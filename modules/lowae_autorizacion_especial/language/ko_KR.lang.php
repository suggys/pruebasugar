<?php
// created: 2018-11-08 09:25:23
$mod_strings = array (
  'LBL_TEAM' => '팀',
  'LBL_TEAMS' => '팀',
  'LBL_TEAM_ID' => '팀 ID',
  'LBL_ASSIGNED_TO_ID' => '지정 사용자 ID',
  'LBL_ASSIGNED_TO_NAME' => '지정자',
  'LBL_TAGS_LINK' => '태그',
  'LBL_TAGS' => '태그',
  'LBL_ID' => 'ID:',
  'LBL_DATE_ENTERED' => '생성일자:',
  'LBL_DATE_MODIFIED' => '수정일자:',
  'LBL_MODIFIED' => '수정자:',
  'LBL_MODIFIED_ID' => '수정자 ID',
  'LBL_MODIFIED_NAME' => '사용자명에 의해 수정',
  'LBL_CREATED' => '생성자',
  'LBL_CREATED_ID' => '생성자 ID',
  'LBL_DOC_OWNER' => '문서소유자',
  'LBL_USER_FAVORITES' => '사용자 즐겨 찾기',
  'LBL_DESCRIPTION' => '설명',
  'LBL_DELETED' => '삭제',
  'LBL_NAME' => '이름',
  'LBL_CREATED_USER' => '사용자에 의해 생성',
  'LBL_MODIFIED_USER' => '사용자에 의해 수정',
  'LBL_LIST_NAME' => '성명',
  'LBL_EDIT_BUTTON' => '수정하기',
  'LBL_REMOVE' => '제거하기',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '이름으로 수정',
  'LBL_LIST_FORM_TITLE' => 'Autorización Especial 목록',
  'LBL_MODULE_NAME' => 'Autorización Especial',
  'LBL_MODULE_TITLE' => 'Autorización Especial',
  'LBL_MODULE_NAME_SINGULAR' => 'Autorización Especial',
  'LBL_HOMEPAGE_TITLE' => '나의 Autorización Especial',
  'LNK_NEW_RECORD' => '새로 만들기 Autorización Especial',
  'LNK_LIST' => '보기 Autorización Especial',
  'LNK_IMPORT_LOWAE_AUTORIZACION_ESPECIAL' => 'Importar Autorización Especial',
  'LBL_SEARCH_FORM_TITLE' => '검색 Autorización Especial',
  'LBL_HISTORY_SUBPANEL_TITLE' => '연혁보기',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '활동내역',
  'LBL_LOWAE_AUTORIZACION_ESPECIAL_SUBPANEL_TITLE' => 'Autorización Especial',
  'LBL_NEW_FORM_TITLE' => '신규 Autorización Especial',
  'LNK_IMPORT_VCARD' => 'Importar Autorización Especial vCard',
  'LBL_IMPORT' => 'Importar Autorización Especial',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Autorización Especial record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_MONTO_VENTA_C' => 'Monto de Venta',
  'LBL_PROMESA_PAGO_C' => 'Autorización por Promesa Pago',
  'LBL_MONTO_PROMESA_C' => 'Monto de la Promesa',
  'LBL_ESTADO_PROMESA_C' => 'Estado de la Promesa',
  'LBL_CREDITO_DIPONIBLE_TEMPORAL_C' => 'Crédito Disponible Temporal',
  'LBL_LIMITE_CREDITO_TEMPORAL_C' => 'Límite de Crédito Temporal',
  'LBL_FECHA_INICIO_C' => 'Fecha de Inicio',
  'LBL_FECHA_FIN_C' => 'Fecha de Fin',
  'LBL_ESTADO_C' => 'Estado',
  'LBL_FECHA_CUMPLIMIENTO_C' => 'Fecha de Cumplimiento de la Promesa',
  'LBL_AUTORIZACION_C' => 'Autorización',
  'LBL_MOTIVO_SOLICITUD_C' => 'Motivo de Solicitud',
  'LBL_MONTO_ABONADO_C' => 'Monto Abonado',
  'LBL_ACCOUNT_ID_C_ACCOUNT_ID' => 'account id c (relacionado Cliente Crédito AR ID)',
  'LBL_ACCOUNT_ID_C' => 'account id c',
);