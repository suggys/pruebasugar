<?php
// created: 2018-11-08 09:25:23
$mod_strings = array (
  'LBL_TEAM' => '小組',
  'LBL_TEAMS' => '小組',
  'LBL_TEAM_ID' => '小組 ID',
  'LBL_ASSIGNED_TO_ID' => '指派的使用者 ID',
  'LBL_ASSIGNED_TO_NAME' => '已指派至',
  'LBL_TAGS_LINK' => '標籤',
  'LBL_TAGS' => '標籤',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '建立日期',
  'LBL_DATE_MODIFIED' => '修改日期',
  'LBL_MODIFIED' => '修改人',
  'LBL_MODIFIED_ID' => '按 ID 修改',
  'LBL_MODIFIED_NAME' => '按名稱修改',
  'LBL_CREATED' => '建立人',
  'LBL_CREATED_ID' => '按 ID 建立',
  'LBL_DOC_OWNER' => '文件擁有者',
  'LBL_USER_FAVORITES' => '最愛的使用者',
  'LBL_DESCRIPTION' => '描述',
  'LBL_DELETED' => '已刪除',
  'LBL_NAME' => '名稱',
  'LBL_CREATED_USER' => '由使用者建立',
  'LBL_MODIFIED_USER' => '由使用者修改',
  'LBL_LIST_NAME' => '名稱',
  'LBL_EDIT_BUTTON' => '編輯',
  'LBL_REMOVE' => '移除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '按名稱修改',
  'LBL_LIST_FORM_TITLE' => 'Autorización Especial 清單',
  'LBL_MODULE_NAME' => 'Autorización Especial',
  'LBL_MODULE_TITLE' => 'Autorización Especial',
  'LBL_MODULE_NAME_SINGULAR' => 'Autorización Especial',
  'LBL_HOMEPAGE_TITLE' => '我的 Autorización Especial',
  'LNK_NEW_RECORD' => '建立 Autorización Especial',
  'LNK_LIST' => '檢視 Autorización Especial',
  'LNK_IMPORT_LOWAE_AUTORIZACION_ESPECIAL' => 'Importar Autorización Especial',
  'LBL_SEARCH_FORM_TITLE' => '搜尋 Autorización Especial',
  'LBL_HISTORY_SUBPANEL_TITLE' => '檢視歷史',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活動流',
  'LBL_LOWAE_AUTORIZACION_ESPECIAL_SUBPANEL_TITLE' => 'Autorización Especial',
  'LBL_NEW_FORM_TITLE' => '新 Autorización Especial',
  'LNK_IMPORT_VCARD' => 'Importar Autorización Especial vCard',
  'LBL_IMPORT' => 'Importar Autorización Especial',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Autorización Especial record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_MONTO_VENTA_C' => 'Monto de Venta',
  'LBL_PROMESA_PAGO_C' => 'Autorización por Promesa Pago',
  'LBL_MONTO_PROMESA_C' => 'Monto de la Promesa',
  'LBL_ESTADO_PROMESA_C' => 'Estado de la Promesa',
  'LBL_CREDITO_DIPONIBLE_TEMPORAL_C' => 'Crédito Disponible Temporal',
  'LBL_LIMITE_CREDITO_TEMPORAL_C' => 'Límite de Crédito Temporal',
  'LBL_FECHA_INICIO_C' => 'Fecha de Inicio',
  'LBL_FECHA_FIN_C' => 'Fecha de Fin',
  'LBL_ESTADO_C' => 'Estado',
  'LBL_FECHA_CUMPLIMIENTO_C' => 'Fecha de Cumplimiento de la Promesa',
  'LBL_AUTORIZACION_C' => 'Autorización',
  'LBL_MOTIVO_SOLICITUD_C' => 'Motivo de Solicitud',
  'LBL_MONTO_ABONADO_C' => 'Monto Abonado',
  'LBL_ACCOUNT_ID_C_ACCOUNT_ID' => 'account id c (relacionado Cliente Crédito AR ID)',
  'LBL_ACCOUNT_ID_C' => 'account id c',
);