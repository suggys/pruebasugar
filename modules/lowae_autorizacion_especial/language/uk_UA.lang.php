<?php
// created: 2018-11-08 09:25:23
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_LIST_FORM_TITLE' => 'Autorización Especial Список',
  'LBL_MODULE_NAME' => 'Autorización Especial',
  'LBL_MODULE_TITLE' => 'Autorización Especial',
  'LBL_MODULE_NAME_SINGULAR' => 'Autorización Especial',
  'LBL_HOMEPAGE_TITLE' => 'Мій Autorización Especial',
  'LNK_NEW_RECORD' => 'Створити Autorización Especial',
  'LNK_LIST' => 'Переглянути Autorización Especial',
  'LNK_IMPORT_LOWAE_AUTORIZACION_ESPECIAL' => 'Importar Autorización Especial',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Autorización Especial',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_LOWAE_AUTORIZACION_ESPECIAL_SUBPANEL_TITLE' => 'Autorización Especial',
  'LBL_NEW_FORM_TITLE' => 'Новий Autorización Especial',
  'LNK_IMPORT_VCARD' => 'Importar Autorización Especial vCard',
  'LBL_IMPORT' => 'Importar Autorización Especial',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Autorización Especial record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_MONTO_VENTA_C' => 'Monto de Venta',
  'LBL_PROMESA_PAGO_C' => 'Autorización por Promesa Pago',
  'LBL_MONTO_PROMESA_C' => 'Monto de la Promesa',
  'LBL_ESTADO_PROMESA_C' => 'Estado de la Promesa',
  'LBL_CREDITO_DIPONIBLE_TEMPORAL_C' => 'Crédito Disponible Temporal',
  'LBL_LIMITE_CREDITO_TEMPORAL_C' => 'Límite de Crédito Temporal',
  'LBL_FECHA_INICIO_C' => 'Fecha de Inicio',
  'LBL_FECHA_FIN_C' => 'Fecha de Fin',
  'LBL_ESTADO_C' => 'Estado',
  'LBL_FECHA_CUMPLIMIENTO_C' => 'Fecha de Cumplimiento de la Promesa',
  'LBL_AUTORIZACION_C' => 'Autorización',
  'LBL_MOTIVO_SOLICITUD_C' => 'Motivo de Solicitud',
  'LBL_MONTO_ABONADO_C' => 'Monto Abonado',
  'LBL_ACCOUNT_ID_C_ACCOUNT_ID' => 'account id c (relacionado Cliente Crédito AR ID)',
  'LBL_ACCOUNT_ID_C' => 'account id c',
);