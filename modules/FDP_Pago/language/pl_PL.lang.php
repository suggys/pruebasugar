<?php
// created: 2018-11-08 09:25:31
$mod_strings = array (
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_ASSIGNED_TO_ID' => 'Przypisano do (ID użytkownika)',
  'LBL_ASSIGNED_TO_NAME' => 'Przydzielono do',
  'LBL_TAGS_LINK' => 'Tagi',
  'LBL_TAGS' => 'Tagi',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_MODIFIED' => 'Zmodyfikowano przez',
  'LBL_MODIFIED_ID' => 'Zmodyfikowano przez (ID)',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowano przez (nazwa)',
  'LBL_CREATED' => 'Utworzono przez',
  'LBL_CREATED_ID' => 'Utworzone przez (ID)',
  'LBL_DOC_OWNER' => 'Właściciel dokumentu',
  'LBL_USER_FAVORITES' => 'Użytkownicy, którzy dodali rekord do ulubionych',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Usunięto',
  'LBL_NAME' => 'Nazwa',
  'LBL_CREATED_USER' => 'Utworzono przez',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_REMOVE' => 'Usuń',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmodyfikowano przez (nazwa)',
  'LBL_LIST_FORM_TITLE' => 'Formas de Pago Lista',
  'LBL_MODULE_NAME' => 'Formas de Pago',
  'LBL_MODULE_TITLE' => 'Formas de Pago',
  'LBL_MODULE_NAME_SINGULAR' => 'Forma de Pago',
  'LBL_HOMEPAGE_TITLE' => 'Moje Formas de Pago',
  'LNK_NEW_RECORD' => 'Utwórz Forma de Pago',
  'LNK_LIST' => 'Widok Formas de Pago',
  'LNK_IMPORT_FDP_PAGO' => 'Importar Formas de Pago',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Forma de Pago',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Wyświetl historię',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Panel aktywności',
  'LBL_FDP_PAGO_SUBPANEL_TITLE' => 'Formas de Pago',
  'LBL_NEW_FORM_TITLE' => 'Nowy Forma de Pago',
  'LNK_IMPORT_VCARD' => 'Importar Forma de Pago vCard',
  'LBL_IMPORT' => 'Importar Formas de Pago',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Forma de Pago record by importing a vCard from your file system.',
);