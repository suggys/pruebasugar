<?php
// created: 2018-11-08 09:25:25
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_LIST_FORM_TITLE' => 'Pagos_Facturas Liste',
  'LBL_MODULE_NAME' => 'Pagos_Facturas',
  'LBL_MODULE_TITLE' => 'Pagos_Facturas',
  'LBL_MODULE_NAME_SINGULAR' => 'Pago Factura',
  'LBL_HOMEPAGE_TITLE' => 'Min Pagos_Facturas',
  'LNK_NEW_RECORD' => 'Opprett Pago Factura',
  'LNK_LIST' => 'Vis Pagos_Facturas',
  'LNK_IMPORT_LOW01_PAGOS_FACTURAS' => 'Importar Pagos_Facturas',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Pago Factura',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_LOW01_PAGOS_FACTURAS_SUBPANEL_TITLE' => 'Pagos_Facturas',
  'LBL_NEW_FORM_TITLE' => 'Ny Pago Factura',
  'LNK_IMPORT_VCARD' => 'Importar Pago Factura vCard',
  'LBL_IMPORT' => 'Importar Pagos_Facturas',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Pago Factura record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_IMPORTE' => 'Importe',
);