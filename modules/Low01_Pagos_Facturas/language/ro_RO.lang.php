<?php
// created: 2018-11-08 09:25:25
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Id Echipă',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuit lui',
  'LBL_TAGS_LINK' => 'Etichete',
  'LBL_TAGS' => 'Etichete',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data creării',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificat după Id',
  'LBL_MODIFIED_NAME' => 'Modificat după Nume',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creat de Id',
  'LBL_DOC_OWNER' => 'Proprietar document',
  'LBL_USER_FAVORITES' => 'Utilizatori cu setări favorite',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_DELETED' => 'Şters',
  'LBL_NAME' => 'Nume',
  'LBL_CREATED_USER' => 'Creat de Utilizator',
  'LBL_MODIFIED_USER' => 'Modificat după Utilizator',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_REMOVE' => 'Eliminare',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat după Nume',
  'LBL_LIST_FORM_TITLE' => 'Pagos_Facturas Lista',
  'LBL_MODULE_NAME' => 'Pagos_Facturas',
  'LBL_MODULE_TITLE' => 'Pagos_Facturas',
  'LBL_MODULE_NAME_SINGULAR' => 'Pago Factura',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Pagos_Facturas',
  'LNK_NEW_RECORD' => 'Creează Pago Factura',
  'LNK_LIST' => 'Vizualizare Pagos_Facturas',
  'LNK_IMPORT_LOW01_PAGOS_FACTURAS' => 'Importar Pagos_Facturas',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Pago Factura',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_LOW01_PAGOS_FACTURAS_SUBPANEL_TITLE' => 'Pagos_Facturas',
  'LBL_NEW_FORM_TITLE' => 'Nou Pago Factura',
  'LNK_IMPORT_VCARD' => 'Importar Pago Factura vCard',
  'LBL_IMPORT' => 'Importar Pagos_Facturas',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Pago Factura record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_IMPORTE' => 'Importe',
);