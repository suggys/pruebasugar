<?php
// created: 2018-11-08 09:25:25
$mod_strings = array (
  'LBL_TEAM' => 'Timovi',
  'LBL_TEAMS' => 'Timovi',
  'LBL_TEAM_ID' => 'ID tima',
  'LBL_ASSIGNED_TO_ID' => 'ID dodijeljenog korisnika',
  'LBL_ASSIGNED_TO_NAME' => 'Dodijeljeno',
  'LBL_TAGS_LINK' => 'Oznake',
  'LBL_TAGS' => 'Oznake',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum stvaranja',
  'LBL_DATE_MODIFIED' => 'Datum izmjene',
  'LBL_MODIFIED' => 'Izmijenio/la',
  'LBL_MODIFIED_ID' => 'Izmijenio ID',
  'LBL_MODIFIED_NAME' => 'Izmijenilo ime',
  'LBL_CREATED' => 'Stvorio/la',
  'LBL_CREATED_ID' => 'Stvorio ID',
  'LBL_DOC_OWNER' => 'Vlasnik dokumenta',
  'LBL_USER_FAVORITES' => 'Korisnici koji su dodali u Favorite',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Izbrisano',
  'LBL_NAME' => 'Ime',
  'LBL_CREATED_USER' => 'Stvorio korisnik',
  'LBL_MODIFIED_USER' => 'Izmijenio korisnik',
  'LBL_LIST_NAME' => 'Ime',
  'LBL_EDIT_BUTTON' => 'Uredi',
  'LBL_REMOVE' => 'Ukloni',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Izmijenilo ime',
  'LBL_LIST_FORM_TITLE' => 'Pagos_Facturas Popis',
  'LBL_MODULE_NAME' => 'Pagos_Facturas',
  'LBL_MODULE_TITLE' => 'Pagos_Facturas',
  'LBL_MODULE_NAME_SINGULAR' => 'Pago Factura',
  'LBL_HOMEPAGE_TITLE' => 'Moja Pagos_Facturas',
  'LNK_NEW_RECORD' => 'Stvori Pago Factura',
  'LNK_LIST' => 'Prikaži Pagos_Facturas',
  'LNK_IMPORT_LOW01_PAGOS_FACTURAS' => 'Importar Pagos_Facturas',
  'LBL_SEARCH_FORM_TITLE' => 'Pretraži Pago Factura',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Prikaži povijest',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Pregled aktivnosti',
  'LBL_LOW01_PAGOS_FACTURAS_SUBPANEL_TITLE' => 'Pagos_Facturas',
  'LBL_NEW_FORM_TITLE' => 'Novo Pago Factura',
  'LNK_IMPORT_VCARD' => 'Importar Pago Factura vCard',
  'LBL_IMPORT' => 'Importar Pagos_Facturas',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Pago Factura record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_IMPORTE' => 'Importe',
);