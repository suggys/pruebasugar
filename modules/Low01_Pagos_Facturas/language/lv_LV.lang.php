<?php
// created: 2018-11-08 09:25:25
$mod_strings = array (
  'LBL_TEAM' => 'Darba grupa',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotāja Id',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_TAGS_LINK' => 'Birkas',
  'LBL_TAGS' => 'Birkas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Izveides datums',
  'LBL_DATE_MODIFIED' => 'Modificēšanas datums',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificēts pēc vārda',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_DOC_OWNER' => 'Dokumenta īpašnieks',
  'LBL_USER_FAVORITES' => 'Lietotāji atzīmēja',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_NAME' => 'Nosaukums',
  'LBL_CREATED_USER' => 'Izveidoja',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificēts pēc vārda',
  'LBL_LIST_FORM_TITLE' => 'Pagos_Facturas Saraksts',
  'LBL_MODULE_NAME' => 'Pagos_Facturas',
  'LBL_MODULE_TITLE' => 'Pagos_Facturas',
  'LBL_MODULE_NAME_SINGULAR' => 'Pago Factura',
  'LBL_HOMEPAGE_TITLE' => 'Mans Pagos_Facturas',
  'LNK_NEW_RECORD' => 'Izveidot Pago Factura',
  'LNK_LIST' => 'Skats Pagos_Facturas',
  'LNK_IMPORT_LOW01_PAGOS_FACTURAS' => 'Importar Pagos_Facturas',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēt Pago Factura',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_LOW01_PAGOS_FACTURAS_SUBPANEL_TITLE' => 'Pagos_Facturas',
  'LBL_NEW_FORM_TITLE' => 'Jauns Pago Factura',
  'LNK_IMPORT_VCARD' => 'Importar Pago Factura vCard',
  'LBL_IMPORT' => 'Importar Pagos_Facturas',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Pago Factura record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_IMPORTE' => 'Importe',
);