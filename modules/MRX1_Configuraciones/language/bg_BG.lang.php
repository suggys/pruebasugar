<?php
// created: 2018-11-08 09:25:28
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_TAGS_LINK' => 'Етикети',
  'LBL_TAGS' => 'Етикети',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от',
  'LBL_MODIFIED_USER' => 'Модифицирано от',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_REMOVE' => 'Премахни',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_LIST_FORM_TITLE' => 'Configuraciones Списък',
  'LBL_MODULE_NAME' => 'Configuraciones',
  'LBL_MODULE_TITLE' => 'Configuraciones',
  'LBL_MODULE_NAME_SINGULAR' => 'Configuración',
  'LBL_HOMEPAGE_TITLE' => 'Мои Configuraciones',
  'LNK_NEW_RECORD' => 'Създай Configuración',
  'LNK_LIST' => 'Изглед Configuraciones',
  'LNK_IMPORT_MRX1_CONFIGURACIONES' => 'Importar Configuración',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Configuración',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_MRX1_CONFIGURACIONES_SUBPANEL_TITLE' => 'Configuraciones',
  'LBL_NEW_FORM_TITLE' => 'Нов Configuración',
  'LNK_IMPORT_VCARD' => 'Importar Configuración vCard',
  'LBL_IMPORT' => 'Importar Configuraciones',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Configuración record by importing a vCard from your file system.',
  'LBL_VALOR' => 'Valor',
);