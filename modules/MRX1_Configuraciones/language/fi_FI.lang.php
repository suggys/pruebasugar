<?php
// created: 2018-11-08 09:25:28
$mod_strings = array (
  'LBL_TEAM' => 'Tiimit',
  'LBL_TEAMS' => 'Tiimit',
  'LBL_TEAM_ID' => 'Tiimin tunnus',
  'LBL_ASSIGNED_TO_ID' => 'Vastuukäyttäjän ID',
  'LBL_ASSIGNED_TO_NAME' => 'Vastuuhenkilö',
  'LBL_TAGS_LINK' => 'Tagit',
  'LBL_TAGS' => 'Tagit',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Luontipäivä',
  'LBL_DATE_MODIFIED' => 'Muokkattu viimeksi',
  'LBL_MODIFIED' => 'Muokannut',
  'LBL_MODIFIED_ID' => 'Muokkaajan ID',
  'LBL_MODIFIED_NAME' => 'Muokkaajan nimi',
  'LBL_CREATED' => 'Luonut',
  'LBL_CREATED_ID' => 'Luojan ID',
  'LBL_DOC_OWNER' => 'Tiedoston omistaja',
  'LBL_USER_FAVORITES' => 'Käyttäjät, jotka ottivat suosikikseen',
  'LBL_DESCRIPTION' => 'Kuvaus',
  'LBL_DELETED' => 'Poistettu',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Luoja',
  'LBL_MODIFIED_USER' => 'Muokkaaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Muokkaa',
  'LBL_REMOVE' => 'Poista',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muokkaajan nimi',
  'LBL_LIST_FORM_TITLE' => 'Configuraciones Lista',
  'LBL_MODULE_NAME' => 'Configuraciones',
  'LBL_MODULE_TITLE' => 'Configuraciones',
  'LBL_MODULE_NAME_SINGULAR' => 'Configuración',
  'LBL_HOMEPAGE_TITLE' => 'Oma Configuraciones',
  'LNK_NEW_RECORD' => 'Luo Configuración',
  'LNK_LIST' => 'Näkymä Configuraciones',
  'LNK_IMPORT_MRX1_CONFIGURACIONES' => 'Importar Configuración',
  'LBL_SEARCH_FORM_TITLE' => 'Etsi Configuración',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Näytä historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteetit',
  'LBL_MRX1_CONFIGURACIONES_SUBPANEL_TITLE' => 'Configuraciones',
  'LBL_NEW_FORM_TITLE' => 'Uusi Configuración',
  'LNK_IMPORT_VCARD' => 'Importar Configuración vCard',
  'LBL_IMPORT' => 'Importar Configuraciones',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Configuración record by importing a vCard from your file system.',
  'LBL_VALOR' => 'Valor',
);