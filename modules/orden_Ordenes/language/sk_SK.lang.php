<?php
// created: 2018-11-08 09:25:22
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Pridelené užívateľské ID',
  'LBL_ASSIGNED_TO_NAME' => 'Pridelený k',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Dátum vytvorenia',
  'LBL_DATE_MODIFIED' => 'Dátum úpravy',
  'LBL_MODIFIED' => 'Zmenil',
  'LBL_MODIFIED_ID' => 'Zmenil podľa ID',
  'LBL_MODIFIED_NAME' => 'Zmenil podľa mena',
  'LBL_CREATED' => 'Vytvoril podľa',
  'LBL_CREATED_ID' => 'Vytvoril podľa ID',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Vymazaný',
  'LBL_NAME' => 'Názov dokumentu',
  'LBL_CREATED_USER' => 'Vytvorené používateľom',
  'LBL_MODIFIED_USER' => 'Zmenené používateľom',
  'LBL_LIST_NAME' => 'Názov',
  'LBL_EDIT_BUTTON' => 'Upraviť',
  'LBL_REMOVE' => 'Odstrániť',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmenil Meno',
  'LBL_TEAM' => 'Tímy',
  'LBL_TEAMS' => 'Tímy',
  'LBL_TEAM_ID' => 'ID tímu',
  'LBL_LIST_FORM_TITLE' => 'Ordenes Zoznam',
  'LBL_MODULE_NAME' => 'Ordenes',
  'LBL_MODULE_TITLE' => 'Ordenes',
  'LBL_MODULE_NAME_SINGULAR' => 'Orden',
  'LBL_HOMEPAGE_TITLE' => 'Moje Ordenes',
  'LNK_NEW_RECORD' => 'Vytvoriť Orden',
  'LNK_LIST' => 'zobrazenie Ordenes',
  'LNK_IMPORT_ORDEN_ORDENES' => 'Importar Orden',
  'LBL_SEARCH_FORM_TITLE' => 'Vyhľadávanie Orden',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobraziť Históriu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_ORDEN_ORDENES_SUBPANEL_TITLE' => 'Ordenes',
  'LBL_NEW_FORM_TITLE' => 'Nový Orden',
  'LNK_IMPORT_VCARD' => 'Importar Orden vCard',
  'LBL_IMPORT' => 'Importar Ordenes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Orden record by importing a vCard from your file system.',
);