<?php
// created: 2018-11-08 09:25:24
$mod_strings = array (
  'LBL_TEAM' => 'Gruppi',
  'LBL_TEAMS' => 'Gruppi',
  'LBL_TEAM_ID' => 'Id Gruppo',
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_TAGS_LINK' => 'Tag',
  'LBL_TAGS' => 'Tag',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DOC_OWNER' => 'Proprietario documento',
  'LBL_USER_FAVORITES' => 'Preferenze Utenti',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DELETED' => 'Cancellato',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Modifica',
  'LBL_REMOVE' => 'Rimuovi',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificato dal Nome',
  'LBL_LIST_FORM_TITLE' => 'Facturas Lista',
  'LBL_MODULE_NAME' => 'Facturas',
  'LBL_MODULE_TITLE' => 'Facturas',
  'LBL_MODULE_NAME_SINGULAR' => 'Factura',
  'LBL_HOMEPAGE_TITLE' => 'Mio Facturas',
  'LNK_NEW_RECORD' => 'Crea Factura',
  'LNK_LIST' => 'Visualizza Facturas',
  'LNK_IMPORT_LF_FACTURAS' => 'Importar Factura',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca Factura',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_LF_FACTURAS_SUBPANEL_TITLE' => 'Facturas',
  'LBL_NEW_FORM_TITLE' => 'Nuovo Factura',
  'LNK_IMPORT_VCARD' => 'Importar Factura vCard',
  'LBL_IMPORT' => 'Importar Facturas',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Factura record by importing a vCard from your file system.',
  'LBL_TIPO_DOCUMENTO' => 'Tipo documento',
  'LBL_ESTADO_TIEMPO' => 'Estado Según Tiempo',
  'LBL_NO_TICKET' => 'Número de Ticket',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_SUB_TOTAL' => 'Sub Total',
  'LBL_IVA' => 'Iva',
  'LBL_IMPORTE' => 'Importe',
  'LBL_REFACTURA' => 'Refactura',
  'LBL_FECHA_ENTREGA' => 'Fecha de entrega de la factura',
  'LBL_DT_CRT' => 'Fecha de emisión',
  'LBL_FECHA_VENCIMIENTO' => 'Fecha de vencimiento',
  'LBL_DIAS_VENCIMIENTO' => 'Días vencido',
  'LBL_ABONO' => 'Abono',
  'LBL_ESTADO' => 'Estado de la Factura',
  'LBL_UUID' => 'Identificador Unico ',
  'LBL_SERIE' => 'Serie',
  'LBL_RFC_EMISOR' => 'RFC Emisior',
  'LBL_NOMBRE_EMISOR' => 'nombre emisor',
  'LBL_RFC_RECEPTOR' => 'RFC Receptor',
  'LBL_NAME_RECEPTOR' => 'Nombre Receptor ',
  'LBL_ID_POS' => 'Cuenta Cliente AR Credit',
  'LBL_SALDO' => 'Saldo',
  'LBL_FOLIO_ORDEN' => 'Número de Folio de la Orden',
  'LBL_CUENTA_CLIENTE_AR' => 'Relación con la cuenta apartir del ID POS',
  'LBL_FOLIO_FACTURA_ORIGINAL_ID' => 'Factura Original /  Inicial ',
);