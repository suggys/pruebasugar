<?php
// created: 2018-11-08 09:25:23
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favourite',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modified By Name',
  'LBL_LIST_FORM_TITLE' => 'Facturas List',
  'LBL_MODULE_NAME' => 'Facturas',
  'LBL_MODULE_TITLE' => 'Facturas',
  'LBL_MODULE_NAME_SINGULAR' => 'Factura',
  'LBL_HOMEPAGE_TITLE' => 'My Facturas',
  'LNK_NEW_RECORD' => 'Create Factura',
  'LNK_LIST' => 'View Facturas',
  'LNK_IMPORT_LF_FACTURAS' => 'Importar Factura',
  'LBL_SEARCH_FORM_TITLE' => 'Search Factura',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_LF_FACTURAS_SUBPANEL_TITLE' => 'Facturas',
  'LBL_NEW_FORM_TITLE' => 'New Factura',
  'LNK_IMPORT_VCARD' => 'Importar Factura vCard',
  'LBL_IMPORT' => 'Importar Facturas',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Factura record by importing a vCard from your file system.',
  'LBL_TIPO_DOCUMENTO' => 'Tipo documento',
  'LBL_ESTADO_TIEMPO' => 'Estado Según Tiempo',
  'LBL_NO_TICKET' => 'Número de Ticket',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_SUB_TOTAL' => 'Sub Total',
  'LBL_IVA' => 'Iva',
  'LBL_IMPORTE' => 'Importe',
  'LBL_REFACTURA' => 'Refactura',
  'LBL_FECHA_ENTREGA' => 'Fecha de entrega de la factura',
  'LBL_DT_CRT' => 'Fecha de emisión',
  'LBL_FECHA_VENCIMIENTO' => 'Fecha de vencimiento',
  'LBL_DIAS_VENCIMIENTO' => 'Días vencido',
  'LBL_ABONO' => 'Abono',
  'LBL_ESTADO' => 'Estado de la Factura',
  'LBL_UUID' => 'Identificador Unico ',
  'LBL_SERIE' => 'Serie',
  'LBL_RFC_EMISOR' => 'RFC Emisior',
  'LBL_NOMBRE_EMISOR' => 'nombre emisor',
  'LBL_RFC_RECEPTOR' => 'RFC Receptor',
  'LBL_NAME_RECEPTOR' => 'Nombre Receptor ',
  'LBL_ID_POS' => 'Cuenta Cliente AR Credit',
  'LBL_SALDO' => 'Saldo',
  'LBL_FOLIO_ORDEN' => 'Número de Folio de la Orden',
  'LBL_CUENTA_CLIENTE_AR' => 'Relación con la cuenta apartir del ID POS',
  'LBL_FOLIO_FACTURA_ORIGINAL_ID' => 'Factura Original /  Inicial ',
);