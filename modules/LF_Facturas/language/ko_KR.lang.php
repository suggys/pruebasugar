<?php
// created: 2018-11-08 09:25:24
$mod_strings = array (
  'LBL_TEAM' => '팀',
  'LBL_TEAMS' => '팀',
  'LBL_TEAM_ID' => '팀 ID',
  'LBL_ASSIGNED_TO_ID' => '지정 사용자 ID',
  'LBL_ASSIGNED_TO_NAME' => '지정자',
  'LBL_TAGS_LINK' => '태그',
  'LBL_TAGS' => '태그',
  'LBL_ID' => 'ID:',
  'LBL_DATE_ENTERED' => '생성일자:',
  'LBL_DATE_MODIFIED' => '수정일자:',
  'LBL_MODIFIED' => '수정자:',
  'LBL_MODIFIED_ID' => '수정자 ID',
  'LBL_MODIFIED_NAME' => '사용자명에 의해 수정',
  'LBL_CREATED' => '생성자',
  'LBL_CREATED_ID' => '생성자 ID',
  'LBL_DOC_OWNER' => '문서소유자',
  'LBL_USER_FAVORITES' => '사용자 즐겨 찾기',
  'LBL_DESCRIPTION' => '설명',
  'LBL_DELETED' => '삭제',
  'LBL_NAME' => '이름',
  'LBL_CREATED_USER' => '사용자에 의해 생성',
  'LBL_MODIFIED_USER' => '사용자에 의해 수정',
  'LBL_LIST_NAME' => '성명',
  'LBL_EDIT_BUTTON' => '수정하기',
  'LBL_REMOVE' => '제거하기',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '이름으로 수정',
  'LBL_LIST_FORM_TITLE' => 'Facturas 목록',
  'LBL_MODULE_NAME' => 'Facturas',
  'LBL_MODULE_TITLE' => 'Facturas',
  'LBL_MODULE_NAME_SINGULAR' => 'Factura',
  'LBL_HOMEPAGE_TITLE' => '나의 Facturas',
  'LNK_NEW_RECORD' => '새로 만들기 Factura',
  'LNK_LIST' => '보기 Facturas',
  'LNK_IMPORT_LF_FACTURAS' => 'Importar Factura',
  'LBL_SEARCH_FORM_TITLE' => '검색 Factura',
  'LBL_HISTORY_SUBPANEL_TITLE' => '연혁보기',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '활동내역',
  'LBL_LF_FACTURAS_SUBPANEL_TITLE' => 'Facturas',
  'LBL_NEW_FORM_TITLE' => '신규 Factura',
  'LNK_IMPORT_VCARD' => 'Importar Factura vCard',
  'LBL_IMPORT' => 'Importar Facturas',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Factura record by importing a vCard from your file system.',
  'LBL_TIPO_DOCUMENTO' => 'Tipo documento',
  'LBL_ESTADO_TIEMPO' => 'Estado Según Tiempo',
  'LBL_NO_TICKET' => 'Número de Ticket',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_SUB_TOTAL' => 'Sub Total',
  'LBL_IVA' => 'Iva',
  'LBL_IMPORTE' => 'Importe',
  'LBL_REFACTURA' => 'Refactura',
  'LBL_FECHA_ENTREGA' => 'Fecha de entrega de la factura',
  'LBL_DT_CRT' => 'Fecha de emisión',
  'LBL_FECHA_VENCIMIENTO' => 'Fecha de vencimiento',
  'LBL_DIAS_VENCIMIENTO' => 'Días vencido',
  'LBL_ABONO' => 'Abono',
  'LBL_ESTADO' => 'Estado de la Factura',
  'LBL_UUID' => 'Identificador Unico ',
  'LBL_SERIE' => 'Serie',
  'LBL_RFC_EMISOR' => 'RFC Emisior',
  'LBL_NOMBRE_EMISOR' => 'nombre emisor',
  'LBL_RFC_RECEPTOR' => 'RFC Receptor',
  'LBL_NAME_RECEPTOR' => 'Nombre Receptor ',
  'LBL_ID_POS' => 'Cuenta Cliente AR Credit',
  'LBL_SALDO' => 'Saldo',
  'LBL_FOLIO_ORDEN' => 'Número de Folio de la Orden',
  'LBL_CUENTA_CLIENTE_AR' => 'Relación con la cuenta apartir del ID POS',
  'LBL_FOLIO_FACTURA_ORIGINAL_ID' => 'Factura Original /  Inicial ',
);