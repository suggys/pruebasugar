<?php
$module_name = 'LF_Facturas';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'tipo_documento',
                'label' => 'LBL_TIPO_DOCUMENTO',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'no_ticket',
                'label' => 'LBL_NO_TICKET',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'estado',
                'label' => 'LBL_ESTADO',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'estado_tiempo',
                'label' => 'LBL_ESTADO_TIEMPO',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'refactura',
                'label' => 'LBL_REFACTURA',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'importe',
                'label' => 'LBL_IMPORTE',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'sub_total',
                'label' => 'LBL_SUB_TOTAL',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'iva',
                'label' => 'LBL_IVA',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'abono',
                'label' => 'LBL_ABONO',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'saldo',
                'label' => 'LBL_SALDO',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'rfc_receptor',
                'label' => 'LBL_RFC_RECEPTOR',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'rfc_emisor',
                'label' => 'LBL_RFC_EMISOR',
                'enabled' => true,
                'default' => true,
              ),
              13 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
              14 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
              15 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              16 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              17 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              18 => 
              array (
                'name' => 'dias_vencimiento',
                'label' => 'LBL_DIAS_VENCIMIENTO',
                'enabled' => true,
                'default' => false,
              ),
              19 => 
              array (
                'name' => 'fecha_entrega',
                'label' => 'LBL_FECHA_ENTREGA',
                'enabled' => true,
                'default' => false,
              ),
              20 => 
              array (
                'name' => 'name_receptor',
                'label' => 'LBL_NAME_RECEPTOR',
                'enabled' => true,
                'default' => false,
              ),
              21 => 
              array (
                'name' => 'uuid',
                'label' => 'LBL_UUID',
                'enabled' => true,
                'default' => false,
              ),
              22 => 
              array (
                'name' => 'dt_crt',
                'label' => 'LBL_DT_CRT',
                'enabled' => true,
                'default' => false,
              ),
              23 => 
              array (
                'name' => 'fecha_vencimiento',
                'label' => 'LBL_FECHA_VENCIMIENTO',
                'enabled' => true,
                'default' => false,
              ),
              24 => 
              array (
                'name' => 'nombre_emisor',
                'label' => 'LBL_NOMBRE_EMISOR',
                'enabled' => true,
                'default' => false,
              ),
              25 => 
              array (
                'name' => 'serie',
                'label' => 'LBL_SERIE',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
