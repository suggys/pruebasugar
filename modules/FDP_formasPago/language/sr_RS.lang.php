<?php
// created: 2018-11-08 09:25:32
$mod_strings = array (
  'LBL_TEAM' => 'Timovi',
  'LBL_TEAMS' => 'Timovi',
  'LBL_TEAM_ID' => 'ID broj tima',
  'LBL_ASSIGNED_TO_ID' => 'ID broj dodeljenog korisnika',
  'LBL_ASSIGNED_TO_NAME' => 'Korisnik',
  'LBL_TAGS_LINK' => 'Oznake',
  'LBL_TAGS' => 'Oznake',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum kreiranja',
  'LBL_DATE_MODIFIED' => 'Datum izmene',
  'LBL_MODIFIED' => 'Promenio',
  'LBL_MODIFIED_ID' => 'ID broj korisnika koji je promenio',
  'LBL_MODIFIED_NAME' => 'Modifikovano prema imenu',
  'LBL_CREATED' => 'Autor',
  'LBL_CREATED_ID' => 'Kreirano prema Id-u',
  'LBL_DOC_OWNER' => 'Vlasnik dokumenta',
  'LBL_USER_FAVORITES' => 'Korisnici koji su označili omiljene',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Obrisan',
  'LBL_NAME' => 'Ime',
  'LBL_CREATED_USER' => 'Kreirao korisnik',
  'LBL_MODIFIED_USER' => 'Promenio korisnik',
  'LBL_LIST_NAME' => 'Ime',
  'LBL_EDIT_BUTTON' => 'Izmeni',
  'LBL_REMOVE' => 'Ukloni',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikovano prema imenu',
  'LBL_LIST_FORM_TITLE' => 'formaPago Lista',
  'LBL_MODULE_NAME' => 'formaPago',
  'LBL_MODULE_TITLE' => 'formaPago',
  'LBL_MODULE_NAME_SINGULAR' => 'formapago',
  'LBL_HOMEPAGE_TITLE' => 'Moja formaPago',
  'LNK_NEW_RECORD' => 'Kreiraj formapago',
  'LNK_LIST' => 'Pregled formaPago',
  'LNK_IMPORT_FDP_FORMASPAGO' => 'Importar formaPago',
  'LBL_SEARCH_FORM_TITLE' => 'Pretraga formapago',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Pregled istorije',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivnosti',
  'LBL_FDP_FORMASPAGO_SUBPANEL_TITLE' => 'formaPago',
  'LBL_NEW_FORM_TITLE' => 'Novo formapago',
  'LNK_IMPORT_VCARD' => 'Importar formapago vCard',
  'LBL_IMPORT' => 'Importar formaPago',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new formapago record by importing a vCard from your file system.',
  'LBL_VALU' => 'valu',
  'LBL_CODE' => 'code',
);