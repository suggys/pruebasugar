<?php
// created: 2018-11-08 09:25:30
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_NUMBER' => 'Номер:',
  'LBL_STATUS' => 'Статус:',
  'LBL_PRIORITY' => 'Пріоритет:',
  'LBL_RESOLUTION' => 'Resolution',
  'LBL_LAST_MODIFIED' => 'Остання зміна',
  'LBL_WORK_LOG' => 'Робочий Лог:',
  'LBL_CREATED_BY' => 'Створено:',
  'LBL_DATE_CREATED' => 'Дата створення:',
  'LBL_MODIFIED_BY' => 'Остання зміна:',
  'LBL_ASSIGNED_USER' => 'Відповідальний (-а):',
  'LBL_ASSIGNED_USER_NAME' => 'Відповідальний (-а)',
  'LBL_SYSTEM_ID' => 'Система (Id):',
  'LBL_TEAM_NAME' => 'Назва команди:',
  'LBL_TYPE' => 'Тип:',
  'LBL_SUBJECT' => 'Тема:',
  'LBL_LIST_FORM_TITLE' => 'Prospectos de Centro de Diseño Список',
  'LBL_MODULE_NAME' => 'Prospectos de Centro de Diseño',
  'LBL_MODULE_TITLE' => 'Prospectos de Centro de Diseño',
  'LBL_MODULE_NAME_SINGULAR' => 'Prospecto de Centro de Diseño',
  'LBL_HOMEPAGE_TITLE' => 'Мій Prospectos de Centro de Diseño',
  'LNK_NEW_RECORD' => 'Створити Prospecto de Centro de Diseño',
  'LNK_LIST' => 'Переглянути Prospectos de Centro de Diseño',
  'LNK_IMPORT_DISE_PROSPECTOS_COCINA' => 'Importar Prospectos de Centro de Diseño',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Prospecto de Centro de Diseño',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_DISE_PROSPECTOS_COCINA_SUBPANEL_TITLE' => 'Prospectos de Centro de Diseño',
  'LBL_NEW_FORM_TITLE' => 'Новий Prospecto de Centro de Diseño',
  'LNK_IMPORT_VCARD' => 'Importar Prospecto de Centro de Diseño vCard',
  'LBL_IMPORT' => 'Importar Prospectos de Centro de Diseño',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Prospecto de Centro de Diseño record by importing a vCard from your file system.',
  'LBL_TIPO_PERSONA' => 'Tipo de persona',
  'LBL_RFC' => 'RFC',
  'LBL_TELEFONO_CELULAR' => 'Teléfono celular',
  'LBL_TELEFONO_OFICINA' => 'Teléfono de oficina',
  'LBL_COLONIA' => 'Colonia',
  'LBL_CIUDAD' => 'Ciudad',
  'LBL_ESTADO' => 'Estado',
  'LBL_CODIGO_POSTAL' => 'Código postal',
  'LBL_NOMBRE_DEL_PROYECTO' => 'Nombre del proyecto',
  'LBL_CATEGORIA' => 'Categoria',
  'LBL_ETAPA_DE_VENTA' => 'Etapa de venta',
  'LBL_FECHA_DE_CIERRE' => 'Fecha de cierre',
  'LBL_ID_SUCURSAL' => 'Sucursal',
);