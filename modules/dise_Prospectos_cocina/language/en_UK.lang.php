<?php
// created: 2018-11-08 09:25:29
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favourite',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modified By Name',
  'LBL_NUMBER' => 'Number:',
  'LBL_STATUS' => 'Status:',
  'LBL_PRIORITY' => 'Priority:',
  'LBL_RESOLUTION' => 'Resolution',
  'LBL_LAST_MODIFIED' => 'Last Modified',
  'LBL_WORK_LOG' => 'Work Log:',
  'LBL_CREATED_BY' => 'Created by:',
  'LBL_DATE_CREATED' => 'Date Created:',
  'LBL_MODIFIED_BY' => 'Last Modified by:',
  'LBL_ASSIGNED_USER' => 'Assigned User:',
  'LBL_ASSIGNED_USER_NAME' => 'Assigned to',
  'LBL_SYSTEM_ID' => 'System Id:',
  'LBL_TEAM_NAME' => 'Team Name:',
  'LBL_TYPE' => 'Type:',
  'LBL_SUBJECT' => 'Subject:',
  'LBL_LIST_FORM_TITLE' => 'Prospectos de Centro de Diseño List',
  'LBL_MODULE_NAME' => 'Prospectos de Centro de Diseño',
  'LBL_MODULE_TITLE' => 'Prospectos de Centro de Diseño',
  'LBL_MODULE_NAME_SINGULAR' => 'Prospecto de Centro de Diseño',
  'LBL_HOMEPAGE_TITLE' => 'My Prospectos de Centro de Diseño',
  'LNK_NEW_RECORD' => 'Create Prospecto de Centro de Diseño',
  'LNK_LIST' => 'View Prospectos de Centro de Diseño',
  'LNK_IMPORT_DISE_PROSPECTOS_COCINA' => 'Importar Prospectos de Centro de Diseño',
  'LBL_SEARCH_FORM_TITLE' => 'Search Prospecto de Centro de Diseño',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_DISE_PROSPECTOS_COCINA_SUBPANEL_TITLE' => 'Prospectos de Centro de Diseño',
  'LBL_NEW_FORM_TITLE' => 'New Prospecto de Centro de Diseño',
  'LNK_IMPORT_VCARD' => 'Importar Prospecto de Centro de Diseño vCard',
  'LBL_IMPORT' => 'Importar Prospectos de Centro de Diseño',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Prospecto de Centro de Diseño record by importing a vCard from your file system.',
  'LBL_TIPO_PERSONA' => 'Tipo de persona',
  'LBL_RFC' => 'RFC',
  'LBL_TELEFONO_CELULAR' => 'Teléfono celular',
  'LBL_TELEFONO_OFICINA' => 'Teléfono de oficina',
  'LBL_COLONIA' => 'Colonia',
  'LBL_CIUDAD' => 'Ciudad',
  'LBL_ESTADO' => 'Estado',
  'LBL_CODIGO_POSTAL' => 'Código postal',
  'LBL_NOMBRE_DEL_PROYECTO' => 'Nombre del proyecto',
  'LBL_CATEGORIA' => 'Categoria',
  'LBL_ETAPA_DE_VENTA' => 'Etapa de venta',
  'LBL_FECHA_DE_CIERRE' => 'Fecha de cierre',
  'LBL_ID_SUCURSAL' => 'Sucursal',
);