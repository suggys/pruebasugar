<?php
// created: 2018-11-08 09:25:29
$mod_strings = array (
  'LBL_TEAM' => 'チーム',
  'LBL_TEAMS' => 'チーム',
  'LBL_TEAM_ID' => 'チームID',
  'LBL_ASSIGNED_TO_ID' => 'アサイン先ID',
  'LBL_ASSIGNED_TO_NAME' => 'アサイン先',
  'LBL_TAGS_LINK' => 'タグ',
  'LBL_TAGS' => 'タグ',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '作成日',
  'LBL_DATE_MODIFIED' => '更新日',
  'LBL_MODIFIED' => '更新者',
  'LBL_MODIFIED_ID' => '更新者ID',
  'LBL_MODIFIED_NAME' => '更新者',
  'LBL_CREATED' => '作成者',
  'LBL_CREATED_ID' => '作成者ID',
  'LBL_DOC_OWNER' => 'ドキュメントの所有者',
  'LBL_USER_FAVORITES' => 'お気に入りのユーザー',
  'LBL_DESCRIPTION' => '詳細',
  'LBL_DELETED' => '削除済み',
  'LBL_NAME' => '名前',
  'LBL_CREATED_USER' => '作成者',
  'LBL_MODIFIED_USER' => '更新者',
  'LBL_LIST_NAME' => '名前',
  'LBL_EDIT_BUTTON' => '編集',
  'LBL_REMOVE' => '削除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '更新者',
  'LBL_NUMBER' => '番号:',
  'LBL_STATUS' => 'ステータス:',
  'LBL_PRIORITY' => '優先度:',
  'LBL_RESOLUTION' => '解決',
  'LBL_LAST_MODIFIED' => '最終更新日',
  'LBL_WORK_LOG' => '作業履歴:',
  'LBL_CREATED_BY' => '作成者:',
  'LBL_DATE_CREATED' => '作成日:',
  'LBL_MODIFIED_BY' => '更新者:',
  'LBL_ASSIGNED_USER' => 'アサイン先:',
  'LBL_ASSIGNED_USER_NAME' => 'アサイン先',
  'LBL_SYSTEM_ID' => 'システムID:',
  'LBL_TEAM_NAME' => 'チーム名:',
  'LBL_TYPE' => 'タイプ:',
  'LBL_SUBJECT' => '件名:',
  'LBL_LIST_FORM_TITLE' => 'Prospectos de Centro de Diseño 一覧',
  'LBL_MODULE_NAME' => 'Prospectos de Centro de Diseño',
  'LBL_MODULE_TITLE' => 'Prospectos de Centro de Diseño',
  'LBL_MODULE_NAME_SINGULAR' => 'Prospecto de Centro de Diseño',
  'LBL_HOMEPAGE_TITLE' => '私の Prospectos de Centro de Diseño',
  'LNK_NEW_RECORD' => '作成 Prospecto de Centro de Diseño',
  'LNK_LIST' => '画面 Prospectos de Centro de Diseño',
  'LNK_IMPORT_DISE_PROSPECTOS_COCINA' => 'Importar Prospectos de Centro de Diseño',
  'LBL_SEARCH_FORM_TITLE' => '検索 Prospecto de Centro de Diseño',
  'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'アクティビティストリーム',
  'LBL_DISE_PROSPECTOS_COCINA_SUBPANEL_TITLE' => 'Prospectos de Centro de Diseño',
  'LBL_NEW_FORM_TITLE' => '新規 Prospecto de Centro de Diseño',
  'LNK_IMPORT_VCARD' => 'Importar Prospecto de Centro de Diseño vCard',
  'LBL_IMPORT' => 'Importar Prospectos de Centro de Diseño',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Prospecto de Centro de Diseño record by importing a vCard from your file system.',
  'LBL_TIPO_PERSONA' => 'Tipo de persona',
  'LBL_RFC' => 'RFC',
  'LBL_TELEFONO_CELULAR' => 'Teléfono celular',
  'LBL_TELEFONO_OFICINA' => 'Teléfono de oficina',
  'LBL_COLONIA' => 'Colonia',
  'LBL_CIUDAD' => 'Ciudad',
  'LBL_ESTADO' => 'Estado',
  'LBL_CODIGO_POSTAL' => 'Código postal',
  'LBL_NOMBRE_DEL_PROYECTO' => 'Nombre del proyecto',
  'LBL_CATEGORIA' => 'Categoria',
  'LBL_ETAPA_DE_VENTA' => 'Etapa de venta',
  'LBL_FECHA_DE_CIERRE' => 'Fecha de cierre',
  'LBL_ID_SUCURSAL' => 'Sucursal',
);