<?php
$popupMeta = array (
    'moduleMain' => 'Low03_AgentesExternos',
    'varName' => 'Low03_AgentesExternos',
    'orderBy' => 'low03_agentesexternos.first_name, low03_agentesexternos.last_name',
    'whereClauses' => array (
  'first_name' => 'low03_agentesexternos.first_name',
  'last_name' => 'low03_agentesexternos.last_name',
),
    'searchInputs' => array (
  0 => 'first_name',
  1 => 'last_name',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'type' => 'fullname',
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
  ),
  'EMAIL' => 
  array (
    'type' => 'email',
    'studio' => 
    array (
      'visible' => true,
      'searchview' => true,
      'editview' => true,
      'editField' => true,
    ),
    'link' => 'email_addresses_primary',
    'label' => 'LBL_ANY_EMAIL',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
),
);
