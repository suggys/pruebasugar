<?php
// created: 2018-11-08 09:25:27
$mod_strings = array (
  'LBL_TEAM' => 'Týmy',
  'LBL_TEAMS' => 'Týmy',
  'LBL_TEAM_ID' => 'ID týmu',
  'LBL_ASSIGNED_TO_ID' => 'Přiřazené uživateli s ID',
  'LBL_ASSIGNED_TO_NAME' => 'Přiřazeno komu',
  'LBL_TAGS_LINK' => 'Tagy',
  'LBL_TAGS' => 'Tagy',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum zahájení',
  'LBL_DATE_MODIFIED' => 'Datum poslední úpravy',
  'LBL_MODIFIED' => 'Modifikováno kym',
  'LBL_MODIFIED_ID' => 'Upraveno podle ID',
  'LBL_MODIFIED_NAME' => 'Upraveno uživatelem',
  'LBL_CREATED' => 'Vytvořil:',
  'LBL_CREATED_ID' => 'Vytvořeno podle ID',
  'LBL_DOC_OWNER' => 'Vlastník dokumentu',
  'LBL_USER_FAVORITES' => 'Uživatelé, jejichž je oblíbeným',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Odstranit',
  'LBL_NAME' => 'Název',
  'LBL_CREATED_USER' => 'Vytvořeno uživatelem',
  'LBL_MODIFIED_USER' => 'Změněno uživatelem',
  'LBL_LIST_NAME' => 'Název',
  'LBL_EDIT_BUTTON' => 'Editace',
  'LBL_REMOVE' => 'Odstranit',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Upraveno uživatelem',
  'LBL_SALUTATION' => 'Oslovení',
  'LBL_FIRST_NAME' => 'Jméno',
  'LBL_LAST_NAME' => 'Příjmení',
  'LBL_TITLE' => 'Titul',
  'LBL_DEPARTMENT' => 'Oddělení',
  'LBL_DO_NOT_CALL' => 'Nevolat',
  'LBL_HOME_PHONE' => 'Telefon domů',
  'LBL_MOBILE_PHONE' => 'Mobilní telefon',
  'LBL_OFFICE_PHONE' => 'Telefon do práce',
  'LBL_OTHER_PHONE' => 'Další telefon',
  'LBL_FAX_PHONE' => 'Fax',
  'LBL_EMAIL_ADDRESS' => 'E-mailová adresa(y)',
  'LBL_PRIMARY_ADDRESS' => 'Hlavní adresa',
  'LBL_PRIMARY_ADDRESS_STREET' => 'Hlavní adresa',
  'LBL_PRIMARY_ADDRESS_STREET_2' => 'Ulice 2:',
  'LBL_PRIMARY_ADDRESS_STREET_3' => 'Ulice 3:',
  'LBL_PRIMARY_ADDRESS_CITY' => 'Primární město',
  'LBL_PRIMARY_ADDRESS_STATE' => 'Primární stát',
  'LBL_PRIMARY_ADDRESS_POSTALCODE' => 'Primární PSČ',
  'LBL_PRIMARY_ADDRESS_COUNTRY' => 'Země:',
  'LBL_ALT_ADDRESS' => 'Alternativní adresa',
  'LBL_ALT_ADDRESS_STREET' => 'Alternativní adresa',
  'LBL_ALT_ADDRESS_STREET_2' => 'Alternativní ulice 2:',
  'LBL_ALT_ADDRESS_STREET_3' => 'Alternativní ulice 3:',
  'LBL_ALT_ADDRESS_CITY' => 'Alternativní město',
  'LBL_ALT_ADDRESS_STATE' => 'Alternativní stát',
  'LBL_ALT_ADDRESS_POSTALCODE' => 'Alternativní PSČ',
  'LBL_ALT_ADDRESS_COUNTRY' => 'Alternativní země',
  'LBL_STREET' => 'Jiný adresa',
  'LBL_PRIMARY_STREET' => 'Adresa',
  'LBL_ALT_STREET' => 'Jiný adresa',
  'LBL_CITY' => 'Město',
  'LBL_STATE' => 'Stát',
  'LBL_POSTALCODE' => 'PSČ',
  'LBL_POSTAL_CODE' => 'PSČ',
  'LBL_COUNTRY' => 'Země',
  'LBL_ADDRESS_INFORMATION' => 'Adresa(y)',
  'LBL_CONTACT_INFORMATION' => 'Kontaktní informace',
  'LBL_OTHER_EMAIL_ADDRESS' => 'Další email:',
  'LBL_ASSISTANT' => 'Asistent',
  'LBL_ASSISTANT_PHONE' => 'Telefon asistenta',
  'LBL_WORK_PHONE' => 'Telefon do práce',
  'LNK_IMPORT_VCARD' => 'Importar Agente Externo vCard',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Agente Externo record by importing a vCard from your file system.',
  'LBL_ANY_EMAIL' => 'E-mail',
  'LBL_PICTURE_FILE' => 'Obrázek',
  'LBL_LINKEDIN' => 'Účet LinkedIn',
  'LBL_FACEBOOK' => 'Účet na Facebooku',
  'LBL_TWITTER' => 'Účet na Twitteru',
  'LBL_GOOGLEPLUS' => 'ID pro Google Plus',
  'NTC_COPY_ALTERNATE_ADDRESS' => 'Zkopírovat další adresu do adresy',
  'NTC_COPY_PRIMARY_ADDRESS' => 'Zkopírovat adresu do další adresy',
  'LBL_LIST_FORM_TITLE' => 'Agente Externo Celk. cena',
  'LBL_MODULE_NAME' => 'Agente Externo',
  'LBL_MODULE_TITLE' => 'Agente Externo',
  'LBL_MODULE_NAME_SINGULAR' => 'Agente Externo',
  'LBL_HOMEPAGE_TITLE' => 'Moje Agente Externo',
  'LNK_NEW_RECORD' => 'Přidat Agente Externo',
  'LNK_LIST' => 'Zobrazit Agente Externo',
  'LNK_IMPORT_LOW03_AGENTESEXTERNOS' => 'Importar Agente Externo',
  'LBL_SEARCH_FORM_TITLE' => 'Hledat Agente Externo',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobrazit historii',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_LOW03_AGENTESEXTERNOS_SUBPANEL_TITLE' => 'Agente Externo',
  'LBL_NEW_FORM_TITLE' => 'Nový Agente Externo',
  'LBL_IMPORT' => 'Importar Agente Externo',
);