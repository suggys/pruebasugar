<?php
$module_name = 'LOW01_SolicitudesCredito';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'LOW01_SolicitudesCredito',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'tienda',
                'label' => 'LBL_TIENDA',
              ),
              1 => 
              array (
                'name' => 'promotor',
                'label' => 'LBL_PROMOTOR',
              ),
              2 => 
              array (
                'name' => 'fecha',
                'label' => 'LBL_FECHA',
              ),
              3 => 
              array (
                'name' => 'tc',
                'label' => 'LBL_TC',
              ),
              4 => 
              array (
                'name' => 'numero_de_cuenta',
                'label' => 'LBL_NUMERO_DE_CUENTA',
              ),
              5 => 
              array (
                'name' => 'autorizo',
                'label' => 'LBL_AUTORIZO',
              ),
              6 => 
              array (
                'name' => 'limite_otorgado',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_LIMITE_OTORGADO',
              ),
              7 => 
              array (
                'name' => 'folio_bc',
                'label' => 'LBL_FOLIO_BC',
              ),
              8 => 
              array (
                'name' => 'low01_solicitudescredito_accounts_name',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
                'name' => 'low01_solicitudescredito_accounts_1_name',
              ),
              11 => 
              array (
              ),
              12 => 
              array (
                'name' => 'low01_solicitudescredito_prospects_1_name',
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'apellido_paterno',
                'label' => 'LBL_APELLIDO_PATERNO',
              ),
              1 => 
              array (
                'name' => 'apellido_materno',
                'label' => 'LBL_APELLIDO_MATERNO',
              ),
              2 => 
              array (
                'name' => 'nombre_razon_social',
                'label' => 'LBL_NOMBRE_RAZON_SOCIAL',
              ),
              3 => 
              array (
                'name' => 'tipo_de_credito',
                'label' => 'LBL_TIPO_DE_CREDITO',
              ),
              4 => 
              array (
                'name' => 'pagina_de_internet',
                'label' => 'LBL_PAGINA_DE_INTERNET',
              ),
              5 => 
              array (
                'name' => 'calle_domicilio_actual_fiscal',
                'label' => 'LBL_CALLE_DOMICILIO_ACTUAL_FISCAL',
              ),
              6 => 
              array (
                'name' => 'numero_exterior',
                'label' => 'LBL_NUMERO_EXTERIOR',
              ),
              7 => 
              array (
                'name' => 'numero_interior',
                'label' => 'LBL_NUMERO_INTERIOR',
              ),
              8 => 
              array (
                'name' => 'cruza_con',
                'label' => 'LBL_CRUZA_CON',
              ),
              9 => 
              array (
                'name' => 'colonia',
                'label' => 'LBL_COLONIA',
              ),
              10 => 
              array (
                'name' => 'municipio',
                'label' => 'LBL_MUNICIPIO',
              ),
              11 => 
              array (
                'name' => 'estado',
                'label' => 'LBL_ESTADO',
              ),
              12 => 
              array (
                'name' => 'cp',
                'label' => 'LBL_CP',
              ),
              13 => 
              array (
                'name' => 'domicilio',
                'label' => 'LBL_DOMICILIO',
              ),
              14 => 
              array (
                'name' => 'pago_mensual',
                'label' => 'LBL_PAGO_MENSUAL',
              ),
              15 => 
              array (
                'name' => 'nombre_del_aval',
                'label' => 'LBL_NOMBRE_DEL_AVAL',
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'anios_domicilio_anterior',
                'label' => 'LBL_ANIOS_DOMICILIO_ANTERIOR',
              ),
              1 => 
              array (
                'name' => 'meses_domicilio_actual',
                'label' => 'LBL_MESES_DOMICILIO_ACTUAL',
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'anios_domicilio_actual',
                'label' => 'LBL_ANIOS_DOMICILIO_ACTUAL',
              ),
              1 => 
              array (
                'name' => 'meses_domicilio_anterior',
                'label' => 'LBL_MESES_DOMICILIO_ANTERIOR',
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'clave_telefono_conmutador',
                'label' => 'LBL_CLAVE_TELEFONO_CONMUTADOR',
              ),
              1 => 
              array (
                'name' => 'numero_telefono_oficina',
                'label' => 'LBL_NUMERO_TELEFONO_OFICINA',
              ),
            ),
          ),
          6 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL5',
            'label' => 'LBL_RECORDVIEW_PANEL5',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'clave_proveedor1',
                'label' => 'LBL_CLAVE_PROVEEDOR1',
              ),
              1 => 
              array (
                'name' => 'numero_telefono_conmutador',
                'label' => 'LBL_NUMERO_TELEFONO_CONMUTADOR',
              ),
              2 => 
              array (
                'name' => 'extension',
                'label' => 'LBL_EXTENSION',
              ),
              3 => 
              array (
              ),
            ),
          ),
          7 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL7',
            'label' => 'LBL_RECORDVIEW_PANEL7',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'fecha_de_constitucion',
                'label' => 'LBL_FECHA_DE_CONSTITUCION',
              ),
              1 => 
              array (
                'name' => 'rfc_construccion',
                'label' => 'LBL_RFC_CONSTRUCCION',
              ),
              2 => 
              array (
                'name' => 'nombre_del_notario',
                'label' => 'LBL_NOMBRE_DEL_NOTARIO',
              ),
              3 => 
              array (
                'name' => 'notaria',
                'label' => 'LBL_NOTARIA',
              ),
              4 => 
              array (
                'name' => 'plaza',
                'label' => 'LBL_PLAZA',
              ),
              5 => 
              array (
                'name' => 'nombre_del_representante_legal',
                'label' => 'LBL_NOMBRE_DEL_REPRESENTANTE_LEGAL',
              ),
              6 => 
              array (
                'name' => 'puesto',
                'label' => 'LBL_PUESTO',
              ),
              7 => 
              array (
                'name' => 'telefono_celular_construccion',
                'label' => 'LBL_TELEFONO_CELULAR_CONSTRUCCION',
              ),
              8 => 
              array (
                'name' => 'email_construccion',
                'label' => 'LBL_EMAIL_CONSTRUCCION',
              ),
              9 => 
              array (
                'name' => 'fecha_de_nacimiento_construc',
                'label' => 'LBL_FECHA_DE_NACIMIENTO_CONSTRUC',
              ),
              10 => 
              array (
                'name' => 'rfc_del_repre_legal_construc',
                'label' => 'LBL_RFC_DEL_REPRE_LEGAL_CONSTRUC',
              ),
              11 => 
              array (
              ),
            ),
          ),
          8 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL8',
            'label' => 'LBL_RECORDVIEW_PANEL8',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'escritura',
                'label' => 'LBL_ESCRITURA',
              ),
              1 => 
              array (
                'name' => 'rpp',
                'label' => 'LBL_RPP',
              ),
            ),
          ),
          9 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL9',
            'label' => 'LBL_RECORDVIEW_PANEL9',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'numero_de_sucursales',
                'label' => 'LBL_NUMERO_DE_SUCURSALES',
              ),
              1 => 
              array (
                'name' => 'numero_de_empleados',
                'label' => 'LBL_NUMERO_DE_EMPLEADOS',
              ),
              2 => 
              array (
                'name' => 'ventas_anuales',
                'label' => 'LBL_VENTAS_ANUALES',
              ),
              3 => 
              array (
                'name' => 'principales_sedes_de_operacion',
                'label' => 'LBL_PRINCIPALES_SEDES_DE_OPERACION',
              ),
              4 => 
              array (
                'name' => 'giro',
                'label' => 'LBL_GIRO',
              ),
              5 => 
              array (
                'name' => 'sector',
                'label' => 'LBL_SECTOR',
              ),
              6 => 
              array (
                'name' => 'caracter',
                'label' => 'LBL_CARACTER',
              ),
              7 => 
              array (
              ),
            ),
          ),
          10 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL10',
            'label' => 'LBL_RECORDVIEW_PANEL10',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'nombre_funcionario3',
                'label' => 'LBL_NOMBRE_FUNCIONARIO3',
              ),
              1 => 
              array (
                'name' => 'puesto_funcionario2',
                'label' => 'LBL_PUESTO_FUNCIONARIO2',
              ),
              2 => 
              array (
                'name' => 'nombre_funcionario2',
                'label' => 'LBL_NOMBRE_FUNCIONARIO2',
              ),
              3 => 
              array (
                'name' => 'puesto_funcionario3',
                'label' => 'LBL_PUESTO_FUNCIONARIO3',
              ),
              4 => 
              array (
                'name' => 'nombre_funcionario1',
                'label' => 'LBL_NOMBRE_FUNCIONARIO1',
              ),
              5 => 
              array (
                'name' => 'puesto_funcionario1',
                'label' => 'LBL_PUESTO_FUNCIONARIO1',
              ),
            ),
          ),
          11 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL11',
            'label' => 'LBL_RECORDVIEW_PANEL11',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'institucion_ref_com2',
                'label' => 'LBL_INSTITUCION_REF_COM2',
              ),
              1 => 
              array (
                'name' => 'tipo_ref_com2',
                'label' => 'LBL_TIPO_REF_COM2',
              ),
              2 => 
              array (
                'name' => 'numero_de_cuenta_ref_com1',
                'label' => 'LBL_NUMERO_DE_CUENTA_REF_COM1',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'name' => 'institucion_ref_com1',
                'label' => 'LBL_INSTITUCION_REF_COM1',
              ),
              5 => 
              array (
                'name' => 'tipo_ref_com1',
                'label' => 'LBL_TIPO_REF_COM1',
              ),
              6 => 
              array (
                'name' => 'numero_de_cuenta_ref_com2',
                'label' => 'LBL_NUMERO_DE_CUENTA_REF_COM2',
              ),
              7 => 
              array (
              ),
            ),
          ),
          12 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL12',
            'label' => 'LBL_RECORDVIEW_PANEL12',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'nombre_proveedor2',
                'label' => 'LBL_NOMBRE_PROVEEDOR2',
              ),
              1 => 
              array (
                'name' => 'clave_proveedor2',
                'label' => 'LBL_CLAVE_PROVEEDOR2',
              ),
              2 => 
              array (
                'name' => 'telefono_proveedor1',
                'label' => 'LBL_TELEFONO_PROVEEDOR1',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'name' => 'nombre_proveedor3',
                'label' => 'LBL_NOMBRE_PROVEEDOR3',
              ),
              5 => 
              array (
                'name' => 'clave_proveedor3',
                'label' => 'LBL_CLAVE_PROVEEDOR3',
              ),
              6 => 
              array (
                'name' => 'telefono_proveedor3',
                'label' => 'LBL_TELEFONO_PROVEEDOR3',
              ),
              7 => 
              array (
              ),
              8 => 
              array (
                'name' => 'nombre_proveedor1',
                'label' => 'LBL_NOMBRE_PROVEEDOR1',
              ),
              9 => 
              array (
                'name' => 'clave_telefono_oficina',
                'label' => 'LBL_CLAVE_TELEFONO_OFICINA',
              ),
              10 => 
              array (
                'name' => 'telefono_proveedor2',
                'label' => 'LBL_TELEFONO_PROVEEDOR2',
              ),
              11 => 
              array (
              ),
            ),
          ),
          13 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL13',
            'label' => 'LBL_RECORDVIEW_PANEL13',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'recepcion_estado_cuenta',
                'label' => 'LBL_RECEPCION_ESTADO_CUENTA',
              ),
              1 => 
              array (
                'name' => 'recepcion_informacion_promo',
                'label' => 'LBL_RECEPCION_INFORMACION_PROMO',
              ),
              2 => 
              array (
                'name' => 'correo_eletronico_terminos',
                'label' => 'LBL_CORREO_ELETRONICO_TERMINOS',
              ),
              3 => 
              array (
              ),
            ),
          ),
          14 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL6',
            'label' => 'LBL_RECORDVIEW_PANEL6',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'doc_acta_constitutiva',
                'label' => 'LBL_DOC_ACTA_CONSTITUTIVA',
              ),
              1 => 
              array (
                'name' => 'doc_acta_constitutiva_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_ACTA_CONSTITUTIVA_D',
              ),
              2 => 
              array (
                'name' => 'doc_aval',
                'label' => 'LBL_DOC_AVAL',
              ),
              3 => 
              array (
                'name' => 'doc_aval_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_AVAL_D',
              ),
              4 => 
              array (
                'name' => 'doc_comprobante_domicilio',
                'label' => 'LBL_DOC_COMPROBANTE_DOMICILIO',
              ),
              5 => 
              array (
                'name' => 'doc_comprobante_domicilio_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COMPROBANTE_DOMICILIO_D',
              ),
              6 => 
              array (
                'name' => 'doc_copia_acta_representante',
                'label' => 'LBL_DOC_COPIA_ACTA_REPRESENTANTE',
              ),
              7 => 
              array (
                'name' => 'doc_copia_acta_representante_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_ACTA_REPRESENTANTE_D',
              ),
              8 => 
              array (
                'name' => 'doc_copia_estado_cuenta',
                'label' => 'LBL_DOC_COPIA_ESTADO_CUENTA',
              ),
              9 => 
              array (
                'name' => 'doc_copia_estado_cuenta_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_ESTADO_CUENTA_D',
              ),
              10 => 
              array (
                'name' => 'doc_copia_estado_finan_rec',
                'label' => 'LBL_DOC_COPIA_ESTADO_FINAN_REC',
              ),
              11 => 
              array (
                'name' => 'doc_copia_estado_finan_rec_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_ESTADO_FINAN_REC_D',
              ),
              12 => 
              array (
                'name' => 'copia_estado_financiero_anter',
                'label' => 'LBL_COPIA_ESTADO_FINANCIERO_ANTER',
              ),
              13 => 
              array (
                'name' => 'copia_estado_financiero_anterd',
                'studio' => 'visible',
                'label' => 'LBL_COPIA_ESTADO_FINANCIERO_ANTERD',
              ),
              14 => 
              array (
                'name' => 'doc_copia_id_aval',
                'label' => 'LBL_DOC_COPIA_ID_AVAL',
              ),
              15 => 
              array (
                'name' => 'doc_copia_id_aval_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_ID_AVAL_D',
              ),
              16 => 
              array (
                'name' => 'doc_copia_id_solicitante',
                'label' => 'LBL_DOC_COPIA_ID_SOLICITANTE',
              ),
              17 => 
              array (
                'name' => 'doc_copia_id_solicitante_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_ID_SOLICITANTE_D',
              ),
              18 => 
              array (
                'name' => 'doc_copia_pago_impuestos',
                'label' => 'LBL_DOC_COPIA_PAGO_IMPUESTOS',
              ),
              19 => 
              array (
                'name' => 'doc_copia_pago_impuestos_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_PAGO_IMPUESTOS_D',
              ),
              20 => 
              array (
                'name' => 'doc_formato_solicitud',
                'label' => 'LBL_DOC_FORMATO_SOLICITUD',
              ),
              21 => 
              array (
                'name' => 'doc_formato_solicitud_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_FORMATO_SOLICITUD_D',
              ),
              22 => 
              array (
                'name' => 'doc_copia_rfc',
                'label' => 'LBL_DOC_COPIA_RFC',
              ),
              23 => 
              array (
                'name' => 'doc_copia_rfc_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_RFC_D',
              ),
              24 => 
              array (
                'name' => 'doc_formato_buro',
                'label' => 'LBL_DOC_FORMATO_BURO',
              ),
              25 => 
              array (
                'name' => 'doc_formato_buro_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_FORMATO_BURO_D',
              ),
            ),
          ),
          15 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              2 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
