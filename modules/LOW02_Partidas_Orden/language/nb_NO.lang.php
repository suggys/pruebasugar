<?php
// created: 2018-11-08 09:25:26
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_LIST_FORM_TITLE' => 'Partidas de Orden Liste',
  'LBL_MODULE_NAME' => 'Partidas de Orden',
  'LBL_MODULE_TITLE' => 'Partidas de Orden',
  'LBL_MODULE_NAME_SINGULAR' => 'Partida de Orden',
  'LBL_HOMEPAGE_TITLE' => 'Min Partidas de Orden',
  'LNK_NEW_RECORD' => 'Opprett Partida de Orden',
  'LNK_LIST' => 'Vis Partidas de Orden',
  'LNK_IMPORT_LOW02_PARTIDAS_ORDEN' => 'Importar Partida de Orden',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Partida de Orden',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_LOW02_PARTIDAS_ORDEN_SUBPANEL_TITLE' => 'Partidas de Orden',
  'LBL_NEW_FORM_TITLE' => 'Ny Partida de Orden',
  'LNK_IMPORT_VCARD' => 'Importar Partida de Orden vCard',
  'LBL_IMPORT' => 'Importar Partidas de Orden',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Partida de Orden record by importing a vCard from your file system.',
  'LBL_FECHA' => 'Fecha',
  'LBL_CANTIDAD' => 'Cantidad',
  'LBL_ESTADO' => 'Estado',
);