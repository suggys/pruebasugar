<?php
// created: 2018-11-08 09:25:26
$mod_strings = array (
  'LBL_TEAM' => 'Equipas',
  'LBL_TEAMS' => 'Equipas',
  'LBL_TEAM_ID' => 'Id Equipa',
  'LBL_ASSIGNED_TO_ID' => 'ID de Utilizador Atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_TAGS_LINK' => 'Etiquetas',
  'LBL_TAGS' => 'Etiquetas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data da Criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'ID do Utilizador que Criou',
  'LBL_DOC_OWNER' => 'Proprietário do Documento',
  'LBL_USER_FAVORITES' => 'Utilizadores que adicionaram aos Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado por Utilizador',
  'LBL_MODIFIED_USER' => 'Modificado por Utilizador',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado Por Nome',
  'LBL_LIST_FORM_TITLE' => 'Partidas de Orden Lista',
  'LBL_MODULE_NAME' => 'Partidas de Orden',
  'LBL_MODULE_TITLE' => 'Partidas de Orden',
  'LBL_MODULE_NAME_SINGULAR' => 'Partida de Orden',
  'LBL_HOMEPAGE_TITLE' => 'Minha Partidas de Orden',
  'LNK_NEW_RECORD' => 'Criar Partida de Orden',
  'LNK_LIST' => 'Vista Partidas de Orden',
  'LNK_IMPORT_LOW02_PARTIDAS_ORDEN' => 'Importar Partida de Orden',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Partida de Orden',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Fluxo de Atividades',
  'LBL_LOW02_PARTIDAS_ORDEN_SUBPANEL_TITLE' => 'Partidas de Orden',
  'LBL_NEW_FORM_TITLE' => 'Novo Partida de Orden',
  'LNK_IMPORT_VCARD' => 'Importar Partida de Orden vCard',
  'LBL_IMPORT' => 'Importar Partidas de Orden',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Partida de Orden record by importing a vCard from your file system.',
  'LBL_FECHA' => 'Fecha',
  'LBL_CANTIDAD' => 'Cantidad',
  'LBL_ESTADO' => 'Estado',
);