<?php
// created: 2018-11-08 09:25:25
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_TAGS_LINK' => 'Етикети',
  'LBL_TAGS' => 'Етикети',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от',
  'LBL_MODIFIED_USER' => 'Модифицирано от',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_REMOVE' => 'Премахни',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_LIST_FORM_TITLE' => 'Partidas de Orden Списък',
  'LBL_MODULE_NAME' => 'Partidas de Orden',
  'LBL_MODULE_TITLE' => 'Partidas de Orden',
  'LBL_MODULE_NAME_SINGULAR' => 'Partida de Orden',
  'LBL_HOMEPAGE_TITLE' => 'Мои Partidas de Orden',
  'LNK_NEW_RECORD' => 'Създай Partida de Orden',
  'LNK_LIST' => 'Изглед Partidas de Orden',
  'LNK_IMPORT_LOW02_PARTIDAS_ORDEN' => 'Importar Partida de Orden',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Partida de Orden',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_LOW02_PARTIDAS_ORDEN_SUBPANEL_TITLE' => 'Partidas de Orden',
  'LBL_NEW_FORM_TITLE' => 'Нов Partida de Orden',
  'LNK_IMPORT_VCARD' => 'Importar Partida de Orden vCard',
  'LBL_IMPORT' => 'Importar Partidas de Orden',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Partida de Orden record by importing a vCard from your file system.',
  'LBL_FECHA' => 'Fecha',
  'LBL_CANTIDAD' => 'Cantidad',
  'LBL_ESTADO' => 'Estado',
);