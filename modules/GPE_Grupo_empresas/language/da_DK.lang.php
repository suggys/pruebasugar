<?php
// created: 2018-11-08 09:25:21
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruger-id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Oprettet den',
  'LBL_DATE_MODIFIED' => 'Ændret den',
  'LBL_MODIFIED' => 'Ændret af',
  'LBL_MODIFIED_ID' => 'Ændret af id',
  'LBL_MODIFIED_NAME' => 'Ændret af navn',
  'LBL_CREATED' => 'Oprettet af',
  'LBL_CREATED_ID' => 'Oprettet af id',
  'LBL_DOC_OWNER' => 'Dokument ejer',
  'LBL_USER_FAVORITES' => 'Brugernes favorit',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Oprettet af bruger',
  'LBL_MODIFIED_USER' => 'Ændret af bruger',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ændret af navn:',
  'LBL_LIST_FORM_TITLE' => 'Grupo de empresas Liste',
  'LBL_MODULE_NAME' => 'Grupo de empresas',
  'LBL_MODULE_TITLE' => 'Grupo de empresas',
  'LBL_MODULE_NAME_SINGULAR' => 'Grupo de empresa',
  'LBL_HOMEPAGE_TITLE' => 'Min Grupo de empresas',
  'LNK_NEW_RECORD' => 'Opret Grupo de empresa',
  'LNK_LIST' => 'Vis Grupo de empresas',
  'LNK_IMPORT_GPE_GRUPO_EMPRESAS' => 'Importar Grupo de empresa',
  'LBL_SEARCH_FORM_TITLE' => 'Søg Grupo de empresa',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vis historik',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_GPE_GRUPO_EMPRESAS_SUBPANEL_TITLE' => 'Grupo de empresas',
  'LBL_NEW_FORM_TITLE' => 'Ny Grupo de empresa',
  'LNK_IMPORT_VCARD' => 'Importar Grupo de empresa vCard',
  'LBL_IMPORT' => 'Importar Grupo de empresas',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Grupo de empresa record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_LIMITE_CREDITO_GRUPO_C' => 'Límite de Crédito de Grupo',
  'LBL_CREDITO_DIPONIBLE_GRUPO_C' => 'Crédito Disponible de Grupo',
  'LBL_SALDO_DEUDOR_C' => 'Saldo Deudor',
  'LBL_ADMINISTRAR_CREDITO_GRUPAL' => 'Sumar / Administrar Límite de Crédito por Grupo',
  'LBL_ESTADO' => 'Estado',
);