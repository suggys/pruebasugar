<?php
// created: 2018-11-08 09:25:21
$mod_strings = array (
  'LBL_TEAM' => 'Tímy',
  'LBL_TEAMS' => 'Tímy',
  'LBL_TEAM_ID' => 'ID tímu',
  'LBL_ASSIGNED_TO_ID' => 'Pridelené užívateľské ID',
  'LBL_ASSIGNED_TO_NAME' => 'Pridelený k',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Dátum vytvorenia',
  'LBL_DATE_MODIFIED' => 'Dátum úpravy',
  'LBL_MODIFIED' => 'Zmenil',
  'LBL_MODIFIED_ID' => 'Zmenil podľa ID',
  'LBL_MODIFIED_NAME' => 'Zmenil podľa mena',
  'LBL_CREATED' => 'Vytvoril podľa',
  'LBL_CREATED_ID' => 'Vytvoril podľa ID',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Vymazaný',
  'LBL_NAME' => 'Názov dokumentu',
  'LBL_CREATED_USER' => 'Vytvorené používateľom',
  'LBL_MODIFIED_USER' => 'Zmenené používateľom',
  'LBL_LIST_NAME' => 'Názov',
  'LBL_EDIT_BUTTON' => 'Upraviť',
  'LBL_REMOVE' => 'Odstrániť',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmenil Meno',
  'LBL_LIST_FORM_TITLE' => 'Grupo de empresas Zoznam',
  'LBL_MODULE_NAME' => 'Grupo de empresas',
  'LBL_MODULE_TITLE' => 'Grupo de empresas',
  'LBL_MODULE_NAME_SINGULAR' => 'Grupo de empresa',
  'LBL_HOMEPAGE_TITLE' => 'Moje Grupo de empresas',
  'LNK_NEW_RECORD' => 'Vytvoriť Grupo de empresa',
  'LNK_LIST' => 'zobrazenie Grupo de empresas',
  'LNK_IMPORT_GPE_GRUPO_EMPRESAS' => 'Importar Grupo de empresa',
  'LBL_SEARCH_FORM_TITLE' => 'Vyhľadávanie Grupo de empresa',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobraziť Históriu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_GPE_GRUPO_EMPRESAS_SUBPANEL_TITLE' => 'Grupo de empresas',
  'LBL_NEW_FORM_TITLE' => 'Nový Grupo de empresa',
  'LNK_IMPORT_VCARD' => 'Importar Grupo de empresa vCard',
  'LBL_IMPORT' => 'Importar Grupo de empresas',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Grupo de empresa record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_LIMITE_CREDITO_GRUPO_C' => 'Límite de Crédito de Grupo',
  'LBL_CREDITO_DIPONIBLE_GRUPO_C' => 'Crédito Disponible de Grupo',
  'LBL_SALDO_DEUDOR_C' => 'Saldo Deudor',
  'LBL_ADMINISTRAR_CREDITO_GRUPAL' => 'Sumar / Administrar Límite de Crédito por Grupo',
  'LBL_ESTADO' => 'Estado',
);