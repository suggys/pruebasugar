<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'lowes_pagos_accounts' => 
  array (
    'rhs_label' => 'Clientes Crédito AR',
    'lhs_label' => 'Pagos',
    'lhs_subpanel' => 'default',
    'lhs_module' => 'lowes_Pagos',
    'rhs_module' => 'Accounts',
    'relationship_type' => 'many-to-one',
    'readonly' => false,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
    'relationship_name' => 'lowes_pagos_accounts',
  ),
  'lowes_pagos_lf_facturas' => 
  array (
    'rhs_label' => 'Facturas',
    'lhs_label' => 'Pagos',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'lowes_Pagos',
    'rhs_module' => 'LF_Facturas',
    'relationship_type' => 'one-to-many',
    'readonly' => false,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
    'relationship_name' => 'lowes_pagos_lf_facturas',
  ),
  'lowes_pagos_lf_facturas_1' => 
  array (
    'lhs_module' => 'lowes_Pagos',
    'rhs_module' => 'LF_Facturas',
    'relationship_type' => 'one-to-one',
    'readonly' => false,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
    'relationship_name' => 'lowes_pagos_lf_facturas_1',
  ),
);