<?php
// created: 2018-11-08 09:25:16
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_LIST_FORM_TITLE' => 'Pagos Liste',
  'LBL_MODULE_NAME' => 'Pagos',
  'LBL_MODULE_TITLE' => 'Pagos',
  'LBL_MODULE_NAME_SINGULAR' => 'Pago',
  'LBL_HOMEPAGE_TITLE' => 'Min Pagos',
  'LNK_NEW_RECORD' => 'Opprett Pago',
  'LNK_LIST' => 'Vis Pagos',
  'LNK_IMPORT_LOWES_PAGOS' => 'Importar Pago',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Pago',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_LOWES_PAGOS_SUBPANEL_TITLE' => 'Pagos',
  'LBL_NEW_FORM_TITLE' => 'Ny Pago',
  'LNK_IMPORT_VCARD' => 'Importar Pago vCard',
  'LBL_IMPORT' => 'Importar Pagos',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Pago record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_MONTO' => 'Monto',
  'LBL_FORMA_PAGO' => 'Forma de pago',
  'LBL_PAGO_ANTICIPO' => 'Pago por anticipo',
  'LBL_FECHA_TRANSACCION' => 'Fecha de Pago',
  'LBL_CHEQUE_DEVUELTO' => 'Cheque devuelto',
  'LBL_NUM_FOLIO_C' => 'Número de Folio',
);