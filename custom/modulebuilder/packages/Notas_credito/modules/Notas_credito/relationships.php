<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'nc_notas_credito_accounts' => 
  array (
    'rhs_label' => 'Clientes Crédito AR',
    'lhs_label' => 'Notas de crédito',
    'lhs_subpanel' => 'default',
    'lhs_module' => 'NC_Notas_credito',
    'rhs_module' => 'Accounts',
    'relationship_type' => 'many-to-one',
    'readonly' => false,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
    'relationship_name' => 'nc_notas_credito_accounts',
  ),
  'nc_notas_credito_orden_ordenes' => 
  array (
    'lhs_module' => 'NC_Notas_credito',
    'rhs_module' => 'orden_Ordenes',
    'relationship_type' => 'one-to-one',
    'readonly' => false,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
    'relationship_name' => 'nc_notas_credito_orden_ordenes',
  ),
);