<?php
// created: 2018-11-08 09:25:12
$config = array (
  'team_security' => true,
  'assignable' => true,
  'taggable' => 1,
  'acl' => true,
  'has_tab' => true,
  'studio' => true,
  'audit' => true,
  'activity_enabled' => 0,
  'templates' => 
  array (
    'basic' => 1,
  ),
  'label' => 'Notas de crédito',
  'label_singular' => 'Nota de crédito',
  'importable' => true,
);