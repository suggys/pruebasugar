<?php
// created: 2018-11-08 09:25:13
$mod_strings = array (
  'LBL_TEAM' => 'Tiimit',
  'LBL_TEAMS' => 'Tiimit',
  'LBL_TEAM_ID' => 'Tiimin tunnus',
  'LBL_ASSIGNED_TO_ID' => 'Vastuukäyttäjän ID',
  'LBL_ASSIGNED_TO_NAME' => 'Vastuuhenkilö',
  'LBL_TAGS_LINK' => 'Tagit',
  'LBL_TAGS' => 'Tagit',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Luontipäivä',
  'LBL_DATE_MODIFIED' => 'Muokkattu viimeksi',
  'LBL_MODIFIED' => 'Muokannut',
  'LBL_MODIFIED_ID' => 'Muokkaajan ID',
  'LBL_MODIFIED_NAME' => 'Muokkaajan nimi',
  'LBL_CREATED' => 'Luonut',
  'LBL_CREATED_ID' => 'Luojan ID',
  'LBL_DOC_OWNER' => 'Tiedoston omistaja',
  'LBL_USER_FAVORITES' => 'Käyttäjät, jotka ottivat suosikikseen',
  'LBL_DESCRIPTION' => 'Kuvaus',
  'LBL_DELETED' => 'Poistettu',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Luoja',
  'LBL_MODIFIED_USER' => 'Muokkaaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Muokkaa',
  'LBL_REMOVE' => 'Poista',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muokkaajan nimi',
  'LBL_LIST_FORM_TITLE' => 'formaPago Lista',
  'LBL_MODULE_NAME' => 'formaPago',
  'LBL_MODULE_TITLE' => 'formaPago',
  'LBL_MODULE_NAME_SINGULAR' => 'formapago',
  'LBL_HOMEPAGE_TITLE' => 'Oma formaPago',
  'LNK_NEW_RECORD' => 'Luo formapago',
  'LNK_LIST' => 'Näkymä formaPago',
  'LNK_IMPORT_FDP_FORMASPAGO' => 'Importar formaPago',
  'LBL_SEARCH_FORM_TITLE' => 'Etsi formapago',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Näytä historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteetit',
  'LBL_FDP_FORMASPAGO_SUBPANEL_TITLE' => 'formaPago',
  'LBL_NEW_FORM_TITLE' => 'Uusi formapago',
  'LNK_IMPORT_VCARD' => 'Importar formapago vCard',
  'LBL_IMPORT' => 'Importar formaPago',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new formapago record by importing a vCard from your file system.',
  'LBL_VALU' => 'valu',
  'LBL_CODE' => 'code',
);