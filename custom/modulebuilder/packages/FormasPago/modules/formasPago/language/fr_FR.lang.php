<?php
// created: 2018-11-08 09:25:13
$mod_strings = array (
  'LBL_TEAM' => 'Équipes',
  'LBL_TEAMS' => 'Équipes',
  'LBL_TEAM_ID' => 'Équipe (ID)',
  'LBL_ASSIGNED_TO_ID' => 'ID utilisateur assigné',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par Nom',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_DOC_OWNER' => 'Propriétaire du document',
  'LBL_USER_FAVORITES' => 'Favoris pour les utilisateurs suivants',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Supprimé',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Éditer',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifié par Nom',
  'LBL_LIST_FORM_TITLE' => 'formaPago Catalogue',
  'LBL_MODULE_NAME' => 'formaPago',
  'LBL_MODULE_TITLE' => 'formaPago',
  'LBL_MODULE_NAME_SINGULAR' => 'formapago',
  'LBL_HOMEPAGE_TITLE' => 'Mes formaPago',
  'LNK_NEW_RECORD' => 'Créer formapago',
  'LNK_LIST' => 'Afficher formaPago',
  'LNK_IMPORT_FDP_FORMASPAGO' => 'Importar formaPago',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche formapago',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_FDP_FORMASPAGO_SUBPANEL_TITLE' => 'formaPago',
  'LBL_NEW_FORM_TITLE' => 'Nouveau formapago',
  'LNK_IMPORT_VCARD' => 'Importar formapago vCard',
  'LBL_IMPORT' => 'Importar formaPago',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new formapago record by importing a vCard from your file system.',
  'LBL_VALU' => 'valu',
  'LBL_CODE' => 'code',
);