<?php
// created: 2018-11-08 09:25:15
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'معرّف المستخدم المعين',
  'LBL_ASSIGNED_TO_NAME' => 'تعيين إلى',
  'LBL_TAGS_LINK' => 'العلامات',
  'LBL_TAGS' => 'العلامات',
  'LBL_ID' => 'المعرّف',
  'LBL_DATE_ENTERED' => 'تاريخ الإنشاء',
  'LBL_DATE_MODIFIED' => 'تاريخ التعديل',
  'LBL_MODIFIED' => 'تم التعديل بواسطة',
  'LBL_MODIFIED_ID' => 'تم التعديل بواسطة المعرّف',
  'LBL_MODIFIED_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_CREATED' => 'تم الإنشاء بواسطة',
  'LBL_CREATED_ID' => 'تم الإنشاء بواسطة المعرّف',
  'LBL_DOC_OWNER' => 'مالك المستند',
  'LBL_USER_FAVORITES' => 'المستخدمون الذي يفضلون',
  'LBL_DESCRIPTION' => 'الوصف',
  'LBL_DELETED' => 'تم الحذف',
  'LBL_NAME' => 'الاسم',
  'LBL_CREATED_USER' => 'تم الإنشاء بواسطة مستخدم',
  'LBL_MODIFIED_USER' => 'تم التعديل بواسطة مستخدم',
  'LBL_LIST_NAME' => 'الاسم',
  'LBL_EDIT_BUTTON' => 'تحرير',
  'LBL_REMOVE' => 'إزالة',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_TEAM' => 'الفرق',
  'LBL_TEAMS' => 'الفرق',
  'LBL_TEAM_ID' => 'معرّف الفريق',
  'LBL_LIST_FORM_TITLE' => 'Ordenes القائمة',
  'LBL_MODULE_NAME' => 'Ordenes',
  'LBL_MODULE_TITLE' => 'Ordenes',
  'LBL_MODULE_NAME_SINGULAR' => 'Orden',
  'LBL_HOMEPAGE_TITLE' => 'الخاص بي Ordenes',
  'LNK_NEW_RECORD' => 'إنشاء Orden',
  'LNK_LIST' => 'عرض Ordenes',
  'LNK_IMPORT_ORDEN_ORDENES' => 'Importar Orden',
  'LBL_SEARCH_FORM_TITLE' => 'بحث Orden',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'عرض السجل',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'سير النشاط الكلي',
  'LBL_ORDEN_ORDENES_SUBPANEL_TITLE' => 'Ordenes',
  'LBL_NEW_FORM_TITLE' => 'جديد Orden',
  'LNK_IMPORT_VCARD' => 'Importar Orden vCard',
  'LBL_IMPORT' => 'Importar Ordenes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Orden record by importing a vCard from your file system.',
);