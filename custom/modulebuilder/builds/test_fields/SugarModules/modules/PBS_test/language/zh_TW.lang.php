<?php
// created: 2018-11-08 09:25:20
$mod_strings = array (
  'LBL_TEAM' => '小組',
  'LBL_TEAMS' => '小組',
  'LBL_TEAM_ID' => '小組 ID',
  'LBL_ASSIGNED_TO_ID' => '指派的使用者 ID',
  'LBL_ASSIGNED_TO_NAME' => '已指派至',
  'LBL_TAGS_LINK' => '標籤',
  'LBL_TAGS' => '標籤',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '建立日期',
  'LBL_DATE_MODIFIED' => '修改日期',
  'LBL_MODIFIED' => '修改人',
  'LBL_MODIFIED_ID' => '按 ID 修改',
  'LBL_MODIFIED_NAME' => '按名稱修改',
  'LBL_CREATED' => '建立人',
  'LBL_CREATED_ID' => '按 ID 建立',
  'LBL_DOC_OWNER' => '文件擁有者',
  'LBL_USER_FAVORITES' => '最愛的使用者',
  'LBL_DESCRIPTION' => '描述',
  'LBL_DELETED' => '已刪除',
  'LBL_NAME' => '名稱',
  'LBL_CREATED_USER' => '由使用者建立',
  'LBL_MODIFIED_USER' => '由使用者修改',
  'LBL_LIST_NAME' => '名稱',
  'LBL_EDIT_BUTTON' => '編輯',
  'LBL_REMOVE' => '移除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '按名稱修改',
  'LBL_LIST_FORM_TITLE' => 'test 清單',
  'LBL_MODULE_NAME' => 'test',
  'LBL_MODULE_TITLE' => 'test',
  'LBL_MODULE_NAME_SINGULAR' => 'test',
  'LBL_HOMEPAGE_TITLE' => '我的 test',
  'LNK_NEW_RECORD' => '建立 test',
  'LNK_LIST' => '檢視 test',
  'LNK_IMPORT_PBS_TEST' => 'Importar test',
  'LBL_SEARCH_FORM_TITLE' => '搜尋 test',
  'LBL_HISTORY_SUBPANEL_TITLE' => '檢視歷史',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活動流',
  'LBL_PBS_TEST_SUBPANEL_TITLE' => 'test',
  'LBL_NEW_FORM_TITLE' => '新 test',
  'LNK_IMPORT_VCARD' => 'Importar test vCard',
  'LBL_IMPORT' => 'Importar test',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new test record by importing a vCard from your file system.',
  'LBL_USER_VENDEDOR_C_USER_ID' => 'user vendedor c (relacionado Usuario ID)',
  'LBL_USER_VENDEDOR_C' => 'user vendedor c',
);