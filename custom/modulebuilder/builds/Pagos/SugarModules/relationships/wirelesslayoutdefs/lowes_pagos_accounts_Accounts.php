<?php
 // created: 2016-10-22 05:02:20
$layout_defs["Accounts"]["subpanel_setup"]['lowes_pagos_accounts'] = array (
  'order' => 100,
  'module' => 'lowes_Pagos',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE',
  'get_subpanel_data' => 'lowes_pagos_accounts',
);
