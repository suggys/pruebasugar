<?php
// created: 2016-10-22 05:02:20
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_lf_facturas"] = array (
  'name' => 'lowes_pagos_lf_facturas',
  'type' => 'link',
  'relationship' => 'lowes_pagos_lf_facturas',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'lowes_pagos_lf_facturaslowes_pagos_ida',
  'link-type' => 'many',
  'side' => 'left',
);
