<?php
// created: 2016-10-22 05:02:20
$dictionary["LF_Facturas"]["fields"]["lowes_pagos_lf_facturas_1"] = array (
  'name' => 'lowes_pagos_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'lowes_pagos_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'lowes_Pagos',
  'bean_name' => 'lowes_Pagos',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'lowes_pagos_lf_facturas_1lowes_pagos_ida',
);
$dictionary["LF_Facturas"]["fields"]["lowes_pagos_lf_facturas_1_name"] = array (
  'name' => 'lowes_pagos_lf_facturas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE',
  'save' => true,
  'id_name' => 'lowes_pagos_lf_facturas_1lowes_pagos_ida',
  'link' => 'lowes_pagos_lf_facturas_1',
  'table' => 'lowes_pagos',
  'module' => 'lowes_Pagos',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["lowes_pagos_lf_facturas_1lowes_pagos_ida"] = array (
  'name' => 'lowes_pagos_lf_facturas_1lowes_pagos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE_ID',
  'id_name' => 'lowes_pagos_lf_facturas_1lowes_pagos_ida',
  'link' => 'lowes_pagos_lf_facturas_1',
  'table' => 'lowes_pagos',
  'module' => 'lowes_Pagos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
