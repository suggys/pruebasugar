<?php
// created: 2016-10-22 05:02:20
$dictionary["Account"]["fields"]["lowes_pagos_accounts"] = array (
  'name' => 'lowes_pagos_accounts',
  'type' => 'link',
  'relationship' => 'lowes_pagos_accounts',
  'source' => 'non-db',
  'module' => 'lowes_Pagos',
  'bean_name' => 'lowes_Pagos',
  'vname' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'lowes_pagos_accountsaccounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
