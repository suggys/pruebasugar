<?php
// created: 2016-10-22 05:02:20
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_accounts"] = array (
  'name' => 'lowes_pagos_accounts',
  'type' => 'link',
  'relationship' => 'lowes_pagos_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'lowes_pagos_accountsaccounts_ida',
  'link-type' => 'one',
);
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_accounts_name"] = array (
  'name' => 'lowes_pagos_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'lowes_pagos_accountsaccounts_ida',
  'link' => 'lowes_pagos_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_accountsaccounts_ida"] = array (
  'name' => 'lowes_pagos_accountsaccounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE_ID',
  'id_name' => 'lowes_pagos_accountsaccounts_ida',
  'link' => 'lowes_pagos_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
