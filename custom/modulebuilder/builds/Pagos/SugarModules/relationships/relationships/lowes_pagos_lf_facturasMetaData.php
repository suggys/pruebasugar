<?php
// created: 2016-10-22 05:02:20
$dictionary["lowes_pagos_lf_facturas"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'lowes_pagos_lf_facturas' => 
    array (
      'lhs_module' => 'lowes_Pagos',
      'lhs_table' => 'lowes_pagos',
      'lhs_key' => 'id',
      'rhs_module' => 'LF_Facturas',
      'rhs_table' => 'lf_facturas',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'lowes_pagos_lf_facturas_c',
      'join_key_lhs' => 'lowes_pagos_lf_facturaslowes_pagos_ida',
      'join_key_rhs' => 'lowes_pagos_lf_facturaslf_facturas_idb',
    ),
  ),
  'table' => 'lowes_pagos_lf_facturas_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'lowes_pagos_lf_facturaslowes_pagos_ida' => 
    array (
      'name' => 'lowes_pagos_lf_facturaslowes_pagos_ida',
      'type' => 'id',
    ),
    'lowes_pagos_lf_facturaslf_facturas_idb' => 
    array (
      'name' => 'lowes_pagos_lf_facturaslf_facturas_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'lowes_pagos_lf_facturasspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'lowes_pagos_lf_facturas_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lowes_pagos_lf_facturaslowes_pagos_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'lowes_pagos_lf_facturas_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'lowes_pagos_lf_facturaslf_facturas_idb',
      ),
    ),
  ),
);