<?php
// created: 2016-10-22 05:02:20
$dictionary["lowes_pagos_accounts"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'lowes_pagos_accounts' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'lowes_Pagos',
      'rhs_table' => 'lowes_pagos',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'lowes_pagos_accounts_c',
      'join_key_lhs' => 'lowes_pagos_accountsaccounts_ida',
      'join_key_rhs' => 'lowes_pagos_accountslowes_pagos_idb',
    ),
  ),
  'table' => 'lowes_pagos_accounts_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'lowes_pagos_accountsaccounts_ida' => 
    array (
      'name' => 'lowes_pagos_accountsaccounts_ida',
      'type' => 'id',
    ),
    'lowes_pagos_accountslowes_pagos_idb' => 
    array (
      'name' => 'lowes_pagos_accountslowes_pagos_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'lowes_pagos_accountsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'lowes_pagos_accounts_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lowes_pagos_accountsaccounts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'lowes_pagos_accounts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'lowes_pagos_accountslowes_pagos_idb',
      ),
    ),
  ),
);