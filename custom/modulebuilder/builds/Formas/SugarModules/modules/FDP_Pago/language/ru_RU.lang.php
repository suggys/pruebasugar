<?php
// created: 2018-11-08 09:25:17
$mod_strings = array (
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'Id Команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный (-ая)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено (по названию)',
  'LBL_LIST_FORM_TITLE' => 'Formas de Pago Список',
  'LBL_MODULE_NAME' => 'Formas de Pago',
  'LBL_MODULE_TITLE' => 'Formas de Pago',
  'LBL_MODULE_NAME_SINGULAR' => 'Forma de Pago',
  'LBL_HOMEPAGE_TITLE' => 'Моя Formas de Pago',
  'LNK_NEW_RECORD' => 'Создать Forma de Pago',
  'LNK_LIST' => 'Просмотр Formas de Pago',
  'LNK_IMPORT_FDP_PAGO' => 'Importar Formas de Pago',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Forma de Pago',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_FDP_PAGO_SUBPANEL_TITLE' => 'Formas de Pago',
  'LBL_NEW_FORM_TITLE' => 'Новый Forma de Pago',
  'LNK_IMPORT_VCARD' => 'Importar Forma de Pago vCard',
  'LBL_IMPORT' => 'Importar Formas de Pago',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Forma de Pago record by importing a vCard from your file system.',
);