<?php
// created: 2018-11-08 09:25:17
$mod_strings = array (
  'LBL_TEAM' => 'ทีม',
  'LBL_TEAMS' => 'ทีม',
  'LBL_TEAM_ID' => 'ID ทีม',
  'LBL_ASSIGNED_TO_ID' => 'ID ผู้ใช้ที่ระบุ',
  'LBL_ASSIGNED_TO_NAME' => 'ระบุให้',
  'LBL_TAGS_LINK' => 'แท็ก',
  'LBL_TAGS' => 'แท็ก',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'วันที่สร้าง',
  'LBL_DATE_MODIFIED' => 'วันที่แก้ไข',
  'LBL_MODIFIED' => 'แก้ไขโดย',
  'LBL_MODIFIED_ID' => 'แก้ไขโดย ID',
  'LBL_MODIFIED_NAME' => 'แก้ไขโดยชื่อ',
  'LBL_CREATED' => 'สร้างโดย',
  'LBL_CREATED_ID' => 'สร้างโดย ID',
  'LBL_DOC_OWNER' => 'เจ้าของเอกสาร',
  'LBL_USER_FAVORITES' => 'ผู้ใช้ที่เพิ่มรายการโปรด',
  'LBL_DESCRIPTION' => 'คำอธิบาย',
  'LBL_DELETED' => 'ลบ',
  'LBL_NAME' => 'ชื่อ',
  'LBL_CREATED_USER' => 'สร้างโดยผู้ใช้',
  'LBL_MODIFIED_USER' => 'แก้ไขโดยผู้ใช้',
  'LBL_LIST_NAME' => 'ชื่อ',
  'LBL_EDIT_BUTTON' => 'แก้ไข',
  'LBL_REMOVE' => 'นำออก',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'แก้ไขโดยชื่อ',
  'LBL_LIST_FORM_TITLE' => 'Formas de Pago รายการ',
  'LBL_MODULE_NAME' => 'Formas de Pago',
  'LBL_MODULE_TITLE' => 'Formas de Pago',
  'LBL_MODULE_NAME_SINGULAR' => 'Forma de Pago',
  'LBL_HOMEPAGE_TITLE' => 'ของฉัน Formas de Pago',
  'LNK_NEW_RECORD' => 'สร้าง Forma de Pago',
  'LNK_LIST' => 'มุมมอง Formas de Pago',
  'LNK_IMPORT_FDP_PAGO' => 'Importar Formas de Pago',
  'LBL_SEARCH_FORM_TITLE' => 'ค้นหา Forma de Pago',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'ดูประวัติ',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'สตรีมกิจกรรม',
  'LBL_FDP_PAGO_SUBPANEL_TITLE' => 'Formas de Pago',
  'LBL_NEW_FORM_TITLE' => 'ใหม่ Forma de Pago',
  'LNK_IMPORT_VCARD' => 'Importar Forma de Pago vCard',
  'LBL_IMPORT' => 'Importar Formas de Pago',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Forma de Pago record by importing a vCard from your file system.',
);