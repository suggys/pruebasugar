<?php
// created: 2018-11-08 09:25:19
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Przydzielono do użytkownika (ID)',
  'LBL_ASSIGNED_TO_NAME' => 'Przydzielono do',
  'LBL_TAGS_LINK' => 'Tagi',
  'LBL_TAGS' => 'Tagi',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_MODIFIED' => 'Zmodyfikowane przez',
  'LBL_MODIFIED_ID' => 'ID modyfikującego',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowane przez (nazwa)',
  'LBL_CREATED' => 'Utworzone przez',
  'LBL_CREATED_ID' => 'ID tworzącego',
  'LBL_DOC_OWNER' => 'Właściciel dokumentu',
  'LBL_USER_FAVORITES' => 'Użytkownicy, którzy dodali rekord do ulubionych',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Usunięto',
  'LBL_NAME' => 'Nazwa',
  'LBL_CREATED_USER' => 'Utworzone przez użytkownika',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez użytkownika',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_REMOVE' => 'Usuń',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmodyfikowane przez (nazwa)',
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_LIST_FORM_TITLE' => 'Ordenes Lista',
  'LBL_MODULE_NAME' => 'Ordenes',
  'LBL_MODULE_TITLE' => 'Ordenes',
  'LBL_MODULE_NAME_SINGULAR' => 'Orden',
  'LBL_HOMEPAGE_TITLE' => 'Moje Ordenes',
  'LNK_NEW_RECORD' => 'Utwórz Orden',
  'LNK_LIST' => 'Widok Ordenes',
  'LNK_IMPORT_ORDEN_ORDENES' => 'Importar Orden',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Orden',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historia zmian',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Wydarzenia',
  'LBL_ORDEN_ORDENES_SUBPANEL_TITLE' => 'Ordenes',
  'LBL_NEW_FORM_TITLE' => 'Nowy Orden',
  'LNK_IMPORT_VCARD' => 'Importar Orden vCard',
  'LBL_IMPORT' => 'Importar Ordenes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Orden record by importing a vCard from your file system.',
);