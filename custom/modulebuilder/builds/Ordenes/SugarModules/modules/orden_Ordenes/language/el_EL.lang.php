<?php
// created: 2018-11-08 09:25:19
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Ταυτότητα Ανατεθειμένου Χρήστη',
  'LBL_ASSIGNED_TO_NAME' => 'Ανατέθηκε σε',
  'LBL_TAGS_LINK' => 'Ετικέτες',
  'LBL_TAGS' => 'Ετικέτες',
  'LBL_ID' => 'Ταυτότητα',
  'LBL_DATE_ENTERED' => 'Ημερομηνία Δημιουργίας',
  'LBL_DATE_MODIFIED' => 'Ημερομηνία Τροποποίησης',
  'LBL_MODIFIED' => 'Τροποποιήθηκε Από',
  'LBL_MODIFIED_ID' => 'Τροποποιήθηκε Από Ταυτότητα',
  'LBL_MODIFIED_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_CREATED' => 'Δημιουργήθηκε Από',
  'LBL_CREATED_ID' => 'Δημιουργήθηκε Από Ταυτότητα',
  'LBL_DOC_OWNER' => 'Κάτοχος του εγγράφου',
  'LBL_USER_FAVORITES' => 'Αγαπημένα Χρηστών',
  'LBL_DESCRIPTION' => 'Περιγραφή',
  'LBL_DELETED' => 'Διαγράφηκε',
  'LBL_NAME' => 'Όνομα',
  'LBL_CREATED_USER' => 'Δημιουργήθηκε Από Χρήστη',
  'LBL_MODIFIED_USER' => 'Τροποποιήθηκε από Χρήστη',
  'LBL_LIST_NAME' => 'Όνομα',
  'LBL_EDIT_BUTTON' => 'Επεξεργασία',
  'LBL_REMOVE' => 'Αφαίρεση',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_TEAM' => 'Ομάδες',
  'LBL_TEAMS' => 'Ομάδες',
  'LBL_TEAM_ID' => 'Ταυτότητα Ομάδας',
  'LBL_LIST_FORM_TITLE' => 'Ordenes Λίστα',
  'LBL_MODULE_NAME' => 'Ordenes',
  'LBL_MODULE_TITLE' => 'Ordenes',
  'LBL_MODULE_NAME_SINGULAR' => 'Orden',
  'LBL_HOMEPAGE_TITLE' => 'Δική Μου Ordenes',
  'LNK_NEW_RECORD' => 'Δημιουργία Orden',
  'LNK_LIST' => 'Προβολή Ordenes',
  'LNK_IMPORT_ORDEN_ORDENES' => 'Importar Orden',
  'LBL_SEARCH_FORM_TITLE' => 'Αναζήτηση Orden',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Προβολή Ιστορικού',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Δραστηριότητες',
  'LBL_ORDEN_ORDENES_SUBPANEL_TITLE' => 'Ordenes',
  'LBL_NEW_FORM_TITLE' => 'Νέα Orden',
  'LNK_IMPORT_VCARD' => 'Importar Orden vCard',
  'LBL_IMPORT' => 'Importar Ordenes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Orden record by importing a vCard from your file system.',
);