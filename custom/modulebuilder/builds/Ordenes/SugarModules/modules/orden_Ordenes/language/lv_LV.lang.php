<?php
// created: 2018-11-08 09:25:19
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotāja Id',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_TAGS_LINK' => 'Birkas',
  'LBL_TAGS' => 'Birkas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Izveides datums',
  'LBL_DATE_MODIFIED' => 'Modificēšanas datums',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificēts pēc vārda',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_DOC_OWNER' => 'Dokumenta īpašnieks',
  'LBL_USER_FAVORITES' => 'Lietotāji atzīmēja',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_NAME' => 'Nosaukums',
  'LBL_CREATED_USER' => 'Izveidoja',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificēts pēc vārda',
  'LBL_TEAM' => 'Darba grupa',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_LIST_FORM_TITLE' => 'Ordenes Saraksts',
  'LBL_MODULE_NAME' => 'Ordenes',
  'LBL_MODULE_TITLE' => 'Ordenes',
  'LBL_MODULE_NAME_SINGULAR' => 'Orden',
  'LBL_HOMEPAGE_TITLE' => 'Mans Ordenes',
  'LNK_NEW_RECORD' => 'Izveidot Orden',
  'LNK_LIST' => 'Skats Ordenes',
  'LNK_IMPORT_ORDEN_ORDENES' => 'Importar Orden',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēt Orden',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_ORDEN_ORDENES_SUBPANEL_TITLE' => 'Ordenes',
  'LBL_NEW_FORM_TITLE' => 'Jauns Orden',
  'LNK_IMPORT_VCARD' => 'Importar Orden vCard',
  'LBL_IMPORT' => 'Importar Ordenes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Orden record by importing a vCard from your file system.',
);