<?php
// created: 2018-11-08 09:25:17
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruger-id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Oprettet den',
  'LBL_DATE_MODIFIED' => 'Ændret den',
  'LBL_MODIFIED' => 'Ændret af',
  'LBL_MODIFIED_ID' => 'Ændret af id',
  'LBL_MODIFIED_NAME' => 'Ændret af navn',
  'LBL_CREATED' => 'Oprettet af',
  'LBL_CREATED_ID' => 'Oprettet af id',
  'LBL_DOC_OWNER' => 'Dokument ejer',
  'LBL_USER_FAVORITES' => 'Brugernes favorit',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Oprettet af bruger',
  'LBL_MODIFIED_USER' => 'Ændret af bruger',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ændret af navn:',
  'LBL_LIST_FORM_TITLE' => 'formaPago Liste',
  'LBL_MODULE_NAME' => 'formaPago',
  'LBL_MODULE_TITLE' => 'formaPago',
  'LBL_MODULE_NAME_SINGULAR' => 'formapago',
  'LBL_HOMEPAGE_TITLE' => 'Min formaPago',
  'LNK_NEW_RECORD' => 'Opret formapago',
  'LNK_LIST' => 'Vis formaPago',
  'LNK_IMPORT_FDP_FORMASPAGO' => 'Importar formaPago',
  'LBL_SEARCH_FORM_TITLE' => 'Søg formapago',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vis historik',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_FDP_FORMASPAGO_SUBPANEL_TITLE' => 'formaPago',
  'LBL_NEW_FORM_TITLE' => 'Ny formapago',
  'LNK_IMPORT_VCARD' => 'Importar formapago vCard',
  'LBL_IMPORT' => 'Importar formaPago',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new formapago record by importing a vCard from your file system.',
  'LBL_VALU' => 'valu',
  'LBL_CODE' => 'code',
);