<?php
// created: 2018-11-08 09:25:18
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Lag-ID',
  'LBL_ASSIGNED_TO_ID' => 'Tilldelat användar-ID',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad användare',
  'LBL_TAGS_LINK' => 'Taggar',
  'LBL_TAGS' => 'Taggar',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum skapat',
  'LBL_DATE_MODIFIED' => 'Modifierat Datum',
  'LBL_MODIFIED' => 'Modifierat Av',
  'LBL_MODIFIED_ID' => 'Modifierat Av Id',
  'LBL_MODIFIED_NAME' => 'Modifierat Av Namn',
  'LBL_CREATED' => 'Skapat Av',
  'LBL_CREATED_ID' => 'Skapat Av',
  'LBL_DOC_OWNER' => 'Ägare av dokument',
  'LBL_USER_FAVORITES' => 'Användare som favorite',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_DELETED' => 'Raderad',
  'LBL_NAME' => 'Namn',
  'LBL_CREATED_USER' => 'Skapat Av Användare',
  'LBL_MODIFIED_USER' => 'Modifierat Av Användare',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_REMOVE' => 'Ta bort',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ändrad av namn',
  'LBL_LIST_FORM_TITLE' => 'formaPago List',
  'LBL_MODULE_NAME' => 'formaPago',
  'LBL_MODULE_TITLE' => 'formaPago',
  'LBL_MODULE_NAME_SINGULAR' => 'formapago',
  'LBL_HOMEPAGE_TITLE' => 'Min formaPago',
  'LNK_NEW_RECORD' => 'Create formapago',
  'LNK_LIST' => 'Visa formaPago',
  'LNK_IMPORT_FDP_FORMASPAGO' => 'Importar formaPago',
  'LBL_SEARCH_FORM_TITLE' => 'Search formapago',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetsström',
  'LBL_FDP_FORMASPAGO_SUBPANEL_TITLE' => 'formaPago',
  'LBL_NEW_FORM_TITLE' => 'Ny formapago',
  'LNK_IMPORT_VCARD' => 'Importar formapago vCard',
  'LBL_IMPORT' => 'Importar formaPago',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new formapago record by importing a vCard from your file system.',
  'LBL_VALU' => 'valu',
  'LBL_CODE' => 'code',
);