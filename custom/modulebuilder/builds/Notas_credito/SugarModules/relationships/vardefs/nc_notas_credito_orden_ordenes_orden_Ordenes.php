<?php
// created: 2016-10-12 00:13:48
$dictionary["orden_Ordenes"]["fields"]["nc_notas_credito_orden_ordenes"] = array (
  'name' => 'nc_notas_credito_orden_ordenes',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_orden_ordenes',
  'source' => 'non-db',
  'module' => 'NC_Notas_credito',
  'bean_name' => false,
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE',
  'id_name' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
);
$dictionary["orden_Ordenes"]["fields"]["nc_notas_credito_orden_ordenes_name"] = array (
  'name' => 'nc_notas_credito_orden_ordenes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE',
  'save' => true,
  'id_name' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
  'link' => 'nc_notas_credito_orden_ordenes',
  'table' => 'nc_notas_credito',
  'module' => 'NC_Notas_credito',
  'rname' => 'name',
);
$dictionary["orden_Ordenes"]["fields"]["nc_notas_credito_orden_ordenesnc_notas_credito_ida"] = array (
  'name' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE_ID',
  'id_name' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
  'link' => 'nc_notas_credito_orden_ordenes',
  'table' => 'nc_notas_credito',
  'module' => 'NC_Notas_credito',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
