<?php
 // created: 2016-10-12 00:13:47
$layout_defs["Accounts"]["subpanel_setup"]['nc_notas_credito_accounts'] = array (
  'order' => 100,
  'module' => 'NC_Notas_credito',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE',
  'get_subpanel_data' => 'nc_notas_credito_accounts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
