<?php
// created: 2016-10-12 00:13:47
$dictionary["nc_notas_credito_orden_ordenes"] = array (
  'true_relationship_type' => 'one-to-one',
  'relationships' => 
  array (
    'nc_notas_credito_orden_ordenes' => 
    array (
      'lhs_module' => 'NC_Notas_credito',
      'lhs_table' => 'nc_notas_credito',
      'lhs_key' => 'id',
      'rhs_module' => 'orden_Ordenes',
      'rhs_table' => 'orden_ordenes',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'nc_notas_credito_orden_ordenes_c',
      'join_key_lhs' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
      'join_key_rhs' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
    ),
  ),
  'table' => 'nc_notas_credito_orden_ordenes_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'nc_notas_credito_orden_ordenesnc_notas_credito_ida' => 
    array (
      'name' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
      'type' => 'id',
    ),
    'nc_notas_credito_orden_ordenesorden_ordenes_idb' => 
    array (
      'name' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'nc_notas_credito_orden_ordenesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'nc_notas_credito_orden_ordenes_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'nc_notas_credito_orden_ordenes_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
      ),
    ),
  ),
);