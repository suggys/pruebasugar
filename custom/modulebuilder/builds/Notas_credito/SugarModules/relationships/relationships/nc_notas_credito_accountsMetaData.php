<?php
// created: 2016-10-12 00:13:47
$dictionary["nc_notas_credito_accounts"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'nc_notas_credito_accounts' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'NC_Notas_credito',
      'rhs_table' => 'nc_notas_credito',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'nc_notas_credito_accounts_c',
      'join_key_lhs' => 'nc_notas_credito_accountsaccounts_ida',
      'join_key_rhs' => 'nc_notas_credito_accountsnc_notas_credito_idb',
    ),
  ),
  'table' => 'nc_notas_credito_accounts_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'nc_notas_credito_accountsaccounts_ida' => 
    array (
      'name' => 'nc_notas_credito_accountsaccounts_ida',
      'type' => 'id',
    ),
    'nc_notas_credito_accountsnc_notas_credito_idb' => 
    array (
      'name' => 'nc_notas_credito_accountsnc_notas_credito_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'nc_notas_credito_accountsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'nc_notas_credito_accounts_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'nc_notas_credito_accountsaccounts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'nc_notas_credito_accounts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'nc_notas_credito_accountsnc_notas_credito_idb',
      ),
    ),
  ),
);