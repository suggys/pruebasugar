<?php
// created: 2018-11-08 09:25:18
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum ingevoerd',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_CREATED' => 'Gemaakt door',
  'LBL_CREATED_ID' => 'Gemaakt door ID',
  'LBL_DOC_OWNER' => 'Documenteigenaar',
  'LBL_USER_FAVORITES' => 'Gebruikers die favoriet gemaakt hebben',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_NAME' => 'Naam',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_EDIT_BUTTON' => 'Wijzig',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Gewijzigd door Naam',
  'LBL_LIST_FORM_TITLE' => 'Notas de crédito List',
  'LBL_MODULE_NAME' => 'Notas de crédito',
  'LBL_MODULE_TITLE' => 'Notas de crédito',
  'LBL_MODULE_NAME_SINGULAR' => 'Nota de crédito',
  'LBL_HOMEPAGE_TITLE' => 'Mijn Notas de crédito',
  'LNK_NEW_RECORD' => 'Create Nota de crédito',
  'LNK_LIST' => 'View Notas de crédito',
  'LNK_IMPORT_NC_NOTAS_CREDITO' => 'Importar Nota de crédito',
  'LBL_SEARCH_FORM_TITLE' => 'Zoeken Nota de crédito',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_NC_NOTAS_CREDITO_SUBPANEL_TITLE' => 'Notas de crédito',
  'LBL_NEW_FORM_TITLE' => 'Nieuw Nota de crédito',
  'LNK_IMPORT_VCARD' => 'Importar Nota de crédito vCard',
  'LBL_IMPORT' => 'Importar Notas de crédito',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Nota de crédito record by importing a vCard from your file system.',
  'LBL_NO_TICKET_C' => 'Número de Ticket',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_SUB_TOTAL_C' => 'Sub Total',
  'LBL_IVA_C' => 'IVA',
  'LBL_IMPORTE_TOTAL' => 'Importe Total',
  'LBL_FECHA_EMISION_C' => 'Fecha de Emisión',
  'LBL_ID_POS_C' => 'ID POS',
  'LBL_SALDO_APLICADO_C' => 'Saldo Aplicado',
  'LBL_SALDO_PENDIENTE_C' => 'Saldo Pendiente por Aplicar',
);