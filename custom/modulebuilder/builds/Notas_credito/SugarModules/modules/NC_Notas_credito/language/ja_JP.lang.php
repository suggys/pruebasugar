<?php
// created: 2018-11-08 09:25:18
$mod_strings = array (
  'LBL_TEAM' => 'チーム',
  'LBL_TEAMS' => 'チーム',
  'LBL_TEAM_ID' => 'チームID',
  'LBL_ASSIGNED_TO_ID' => 'アサイン先ID',
  'LBL_ASSIGNED_TO_NAME' => 'アサイン先',
  'LBL_TAGS_LINK' => 'タグ',
  'LBL_TAGS' => 'タグ',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '作成日',
  'LBL_DATE_MODIFIED' => '更新日',
  'LBL_MODIFIED' => '更新者',
  'LBL_MODIFIED_ID' => '更新者ID',
  'LBL_MODIFIED_NAME' => '更新者',
  'LBL_CREATED' => '作成者',
  'LBL_CREATED_ID' => '作成者ID',
  'LBL_DOC_OWNER' => 'ドキュメントの所有者',
  'LBL_USER_FAVORITES' => 'お気に入りのユーザー',
  'LBL_DESCRIPTION' => '詳細',
  'LBL_DELETED' => '削除済み',
  'LBL_NAME' => '名前',
  'LBL_CREATED_USER' => '作成者',
  'LBL_MODIFIED_USER' => '更新者',
  'LBL_LIST_NAME' => '名前',
  'LBL_EDIT_BUTTON' => '編集',
  'LBL_REMOVE' => '削除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '更新者',
  'LBL_LIST_FORM_TITLE' => 'Notas de crédito 一覧',
  'LBL_MODULE_NAME' => 'Notas de crédito',
  'LBL_MODULE_TITLE' => 'Notas de crédito',
  'LBL_MODULE_NAME_SINGULAR' => 'Nota de crédito',
  'LBL_HOMEPAGE_TITLE' => '私の Notas de crédito',
  'LNK_NEW_RECORD' => '作成 Nota de crédito',
  'LNK_LIST' => '画面 Notas de crédito',
  'LNK_IMPORT_NC_NOTAS_CREDITO' => 'Importar Nota de crédito',
  'LBL_SEARCH_FORM_TITLE' => '検索 Nota de crédito',
  'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'アクティビティストリーム',
  'LBL_NC_NOTAS_CREDITO_SUBPANEL_TITLE' => 'Notas de crédito',
  'LBL_NEW_FORM_TITLE' => '新規 Nota de crédito',
  'LNK_IMPORT_VCARD' => 'Importar Nota de crédito vCard',
  'LBL_IMPORT' => 'Importar Notas de crédito',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Nota de crédito record by importing a vCard from your file system.',
  'LBL_NO_TICKET_C' => 'Número de Ticket',
  'LBL_CURRENCY' => 'Moneda',
  'LBL_SUB_TOTAL_C' => 'Sub Total',
  'LBL_IVA_C' => 'IVA',
  'LBL_IMPORTE_TOTAL' => 'Importe Total',
  'LBL_FECHA_EMISION_C' => 'Fecha de Emisión',
  'LBL_ID_POS_C' => 'ID POS',
  'LBL_SALDO_APLICADO_C' => 'Saldo Aplicado',
  'LBL_SALDO_PENDIENTE_C' => 'Saldo Pendiente por Aplicar',
);