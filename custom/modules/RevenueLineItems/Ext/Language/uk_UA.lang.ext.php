<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Language/uk_UA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_REVENUELINEITEM'] = 'Створити дохід за продукти';
$mod_strings['LBL_MODULE_NAME'] = 'Доходи за продукти';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Дохід за продукт';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Створити дохід за продукти';
$mod_strings['LNK_REVENUELINEITEM_LIST'] = 'Переглянути дохід по продуктах:';
$mod_strings['LNK_IMPORT_REVENUELINEITEMS'] = 'Імпортувати дохід по продуктам';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Список доходів за продукти';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Пошук доходу за продукти';
$mod_strings['LBL_MODULE_TITLE'] = 'Доходи за продуктии: Головна';

?>
