<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Language/hu_HU.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_REVENUELINEITEM'] = 'Bevételi sor tételek létrehozása';
$mod_strings['LBL_MODULE_NAME'] = 'Bevétel sorok';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Bevételi sor tétel';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Új termék létrehozása';
$mod_strings['LNK_REVENUELINEITEM_LIST'] = 'Bevételi sorok tételeinek megtekintése';
$mod_strings['LNK_IMPORT_REVENUELINEITEMS'] = 'Bevételi sorok tételeinek importálása';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Termék lista';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Termékek keresése';

?>
