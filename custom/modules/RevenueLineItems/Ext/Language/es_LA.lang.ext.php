<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_REVENUELINEITEM'] = 'Crear un Artículo de Línea de Ganancia';
$mod_strings['LBL_MODULE_NAME'] = 'Artículos de Línea de Ganancia';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Artículo de Línea de Ganancia';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Crear un Artículo de Línea de Ganancia';
$mod_strings['LNK_REVENUELINEITEM_LIST'] = 'Ver Artículos de Línea de Ganancia';
$mod_strings['LNK_IMPORT_REVENUELINEITEMS'] = 'Importar Artículos de la Línea de Ganancia';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Artículos de Línea de Ganancia';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de una Artículos de Línea de Ganancia';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nombre de Cliente:';
$mod_strings['LBL_LEAD_SOURCE'] = 'Fuente del Lead';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Nombre de Cliente';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de la Cliente';

?>
