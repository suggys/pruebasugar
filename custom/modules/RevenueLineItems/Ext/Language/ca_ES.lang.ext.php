<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Language/ca_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_REVENUELINEITEM'] = 'Crear element de línia d&#039;ingressos';
$mod_strings['LBL_MODULE_NAME'] = 'Línia d&#039;impostos articles';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Element de la línia d&#039;impostos';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Crear element de línia d&#039;ingressos';
$mod_strings['LNK_REVENUELINEITEM_LIST'] = 'Veure elements de línies d&#039;ingressos';
$mod_strings['LNK_IMPORT_REVENUELINEITEMS'] = 'Import de les línies d&#039;ingressos';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Llista d&#039;elements de línia d&#039;ingressos';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cerca d&#039;elements de la línia d&#039;ingressos';

?>
