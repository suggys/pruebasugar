<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/RevenueLineItems/Ext/Language/zh_TW.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_REVENUELINEITEM'] = '建立營收項目';
$mod_strings['LBL_MODULE_NAME'] = '營收項目';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = '營收項目';
$mod_strings['LBL_NEW_FORM_TITLE'] = '建立營收項目';
$mod_strings['LNK_REVENUELINEITEM_LIST'] = '檢視營收項目';
$mod_strings['LNK_IMPORT_REVENUELINEITEMS'] = '匯入營收項目';
$mod_strings['LBL_LIST_FORM_TITLE'] = '營收項目清單';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = '營收項目搜尋';

?>
