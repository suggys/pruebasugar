<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/LeadsVisibility.php

$dictionary['Lead']['visibility']["LeadsVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_account_name.php

 // created: 2017-07-12 21:10:00
$dictionary['Lead']['fields']['account_name']['required']=true;
$dictionary['Lead']['fields']['account_name']['audited']=false;
$dictionary['Lead']['fields']['account_name']['massupdate']=false;
$dictionary['Lead']['fields']['account_name']['comments']='Account name for lead';
$dictionary['Lead']['fields']['account_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['account_name']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['account_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['account_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_alt_address_state.php

 // created: 2017-08-03 15:59:48
$dictionary['Lead']['fields']['alt_address_state']['audited']=false;
$dictionary['Lead']['fields']['alt_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Lead']['fields']['alt_address_state']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_state']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_first_name.php

 // created: 2017-07-20 18:01:38
$dictionary['Lead']['fields']['first_name']['required']=false;
$dictionary['Lead']['fields']['first_name']['audited']=false;
$dictionary['Lead']['fields']['first_name']['massupdate']=false;
$dictionary['Lead']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Lead']['fields']['first_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['first_name']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['first_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['first_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.87',
  'searchable' => true,
);
$dictionary['Lead']['fields']['first_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Lead']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_mobile.php

 // created: 2017-08-04 14:44:26
$dictionary['Lead']['fields']['phone_mobile']['type']='varchar';
$dictionary['Lead']['fields']['phone_mobile']['len']='10';
$dictionary['Lead']['fields']['phone_mobile']['required']=false;
$dictionary['Lead']['fields']['phone_mobile']['audited']=false;
$dictionary['Lead']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Lead']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.01',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_mobile']['calculated']=false;
$dictionary['Lead']['fields']['phone_mobile']['enable_range_search']=false;
$dictionary['Lead']['fields']['phone_mobile']['min']=false;
$dictionary['Lead']['fields']['phone_mobile']['max']=false;
$dictionary['Lead']['fields']['phone_mobile']['disable_num_format']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_last_name.php

 // created: 2018-05-11 13:59:06
$dictionary['Lead']['fields']['last_name']['required']=true;
$dictionary['Lead']['fields']['last_name']['audited']=false;
$dictionary['Lead']['fields']['last_name']['massupdate']=false;
$dictionary['Lead']['fields']['last_name']['comments']='';
$dictionary['Lead']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['last_name']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.85',
  'searchable' => true,
);
$dictionary['Lead']['fields']['last_name']['calculated']=false;
$dictionary['Lead']['fields']['last_name']['len']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_adress_country.php

$dictionary['Lead']['fields']['primary_address_country']['type']='enum';
$dictionary['Lead']['fields']['primary_address_country']['options']='prospect_pais_list';
$dictionary['Lead']['fields']['primary_address_country']['required']=false;

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_email.php

 // created: 2017-08-04 14:45:22
$dictionary['Lead']['fields']['email']['len']='100';
$dictionary['Lead']['fields']['email']['required']=false;
$dictionary['Lead']['fields']['email']['audited']=false;
$dictionary['Lead']['fields']['email']['massupdate']=true;
$dictionary['Lead']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['email']['merge_filter']='disabled';
$dictionary['Lead']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.83',
  'searchable' => true,
);
$dictionary['Lead']['fields']['email']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_state.php

 // created: 2017-08-06 00:18:00
$dictionary['Lead']['fields']['primary_address_state']['audited']=false;
$dictionary['Lead']['fields']['primary_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_state']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_state']['len']=100;
$dictionary['Lead']['fields']['primary_address_state']['dependency']=false;
$dictionary['Lead']['fields']['primary_address_state']['visibility_grid']=array (
  'trigger' => 'primary_address_country',
  'values' => 
  array (
    '' => 
    array (
      0 => '',
    ),
    'US' => 
    array (
      0 => 'OTRO',
    ),
    'CA' => 
    array (
      0 => 'OTRO',
    ),
    'FR' => 
    array (
      0 => 'OTRO',
    ),
    'MX' => 
    array (
      0 => 'AGU',
      1 => 'OTRO',
      2 => 'BCN',
      3 => 'BCS',
      4 => 'CAM',
      5 => 'CHP',
      6 => 'CHH',
      7 => 'CMX',
      8 => 'COL',
      9 => 'DUR',
      10 => 'MEX',
      11 => 'GUA',
      12 => 'GRO',
      13 => 'HID',
      14 => 'JAL',
      15 => 'MIC',
      16 => 'MOR',
      17 => 'NAY',
      18 => 'NLE',
      19 => 'OAX',
      20 => 'PUE',
      21 => 'QUE',
      22 => 'ROO',
      23 => 'SLP',
      24 => 'SIN',
      25 => 'SON',
      26 => 'TAB',
      27 => 'TAM',
      28 => 'TLA',
      29 => 'VER',
      30 => 'YUC',
      31 => 'ZAC',
    ),
    'EU' => 
    array (
      0 => 'OTRO',
    ),
    'DE' => 
    array (
      0 => 'OTRO',
    ),
    'GB' => 
    array (
      0 => 'OTRO',
    ),
    'JP' => 
    array (
      0 => 'OTRO',
    ),
    'PR' => 
    array (
      0 => 'OTRO',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_phone_work.php

 // created: 2017-08-02 21:21:08
$dictionary['Lead']['fields']['phone_work']['type']='varchar';
$dictionary['Lead']['fields']['phone_work']['len']='13';
$dictionary['Lead']['fields']['phone_work']['massupdate']=false;
$dictionary['Lead']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_work']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_work']['calculated']=false;
$dictionary['Lead']['fields']['phone_work']['enable_range_search']=false;
$dictionary['Lead']['fields']['phone_work']['min']=false;
$dictionary['Lead']['fields']['phone_work']['max']=false;
$dictionary['Lead']['fields']['phone_work']['disable_num_format']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_website.php

 // created: 2017-07-20 21:34:16
$dictionary['Lead']['fields']['website']['len']='255';
$dictionary['Lead']['fields']['website']['audited']=false;
$dictionary['Lead']['fields']['website']['massupdate']=false;
$dictionary['Lead']['fields']['website']['comments']='URL of website for the company';
$dictionary['Lead']['fields']['website']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['website']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['website']['merge_filter']='disabled';
$dictionary['Lead']['fields']['website']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['website']['calculated']=false;
$dictionary['Lead']['fields']['website']['gen']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_status.php

 // created: 2017-08-02 15:59:25
$dictionary['Lead']['fields']['status']['len']=100;
$dictionary['Lead']['fields']['status']['required']=true;
$dictionary['Lead']['fields']['status']['massupdate']=true;
$dictionary['Lead']['fields']['status']['comments']='Status of the lead';
$dictionary['Lead']['fields']['status']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['status']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['status']['merge_filter']='disabled';
$dictionary['Lead']['fields']['status']['calculated']=false;
$dictionary['Lead']['fields']['status']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_adress_state.php

$dictionary['Lead']['fields']['primary_address_state']['type']='enum';
$dictionary['Lead']['fields']['primary_address_state']['options']='state_c_list';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_folio_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Lead']['fields']['folio_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['folio_c']['labelValue']='Folio';
$dictionary['Lead']['fields']['folio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['folio_c']['enforced']='';
$dictionary['Lead']['fields']['folio_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_contacto_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Lead']['fields']['contacto_c']['labelValue']='Contacto';
$dictionary['Lead']['fields']['contacto_c']['dependency']='equal($status,"Converted")';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Lead']['fields']['contact_id_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_lead_status_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Lead']['fields']['lead_status_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['lead_status_c']['labelValue']='Estado de la Cuenta';
$dictionary['Lead']['fields']['lead_status_c']['dependency']='';
$dictionary['Lead']['fields']['lead_status_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_id_sucursal_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Lead']['fields']['id_sucursal_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['id_sucursal_c']['labelValue']='Sucursal';
$dictionary['Lead']['fields']['id_sucursal_c']['dependency']='';
$dictionary['Lead']['fields']['id_sucursal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_apellido_materno_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Lead']['fields']['apellido_materno_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['apellido_materno_c']['labelValue']='Apellido Materno';
$dictionary['Lead']['fields']['apellido_materno_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['apellido_materno_c']['enforced']='';
$dictionary['Lead']['fields']['apellido_materno_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_proyecto_id_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Lead']['fields']['proyecto_id_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_tipo_persona_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Lead']['fields']['tipo_persona_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['tipo_persona_c']['labelValue']='Tipo de persona';
$dictionary['Lead']['fields']['tipo_persona_c']['dependency']='';
$dictionary['Lead']['fields']['tipo_persona_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_rfc_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Lead']['fields']['rfc_c']['duplicate_merge_dom_value']=0;
$dictionary['Lead']['fields']['rfc_c']['labelValue']='RFC';
$dictionary['Lead']['fields']['rfc_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['rfc_c']['enforced']='';
$dictionary['Lead']['fields']['rfc_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_proyecto_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Lead']['fields']['proyecto_c']['labelValue']='Proyecto';
$dictionary['Lead']['fields']['proyecto_c']['dependency']='equal($status,"Converted")';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_street.php

 // created: 2018-06-05 04:23:10
$dictionary['Lead']['fields']['primary_address_street']['required']=true;
$dictionary['Lead']['fields']['primary_address_street']['audited']=false;
$dictionary['Lead']['fields']['primary_address_street']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_street']['comments']='The street address used for primary address';
$dictionary['Lead']['fields']['primary_address_street']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.31',
  'searchable' => true,
);
$dictionary['Lead']['fields']['primary_address_street']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_street']['dependency']='equal($status,"Converted")';
$dictionary['Lead']['fields']['primary_address_street']['rows']='4';
$dictionary['Lead']['fields']['primary_address_street']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_city.php

 // created: 2018-06-05 04:23:41
$dictionary['Lead']['fields']['primary_address_city']['audited']=false;
$dictionary['Lead']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_city']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_city']['required']=true;
$dictionary['Lead']['fields']['primary_address_city']['dependency']='equal($status,"Converted")';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_postalcode.php

 // created: 2018-06-05 04:24:34
$dictionary['Lead']['fields']['primary_address_postalcode']['audited']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Lead']['fields']['primary_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_postalcode']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_postalcode']['len']='5';
$dictionary['Lead']['fields']['primary_address_postalcode']['required']=true;
$dictionary['Lead']['fields']['primary_address_postalcode']['dependency']='equal($status,"Converted")';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_colonia_c.php

 // created: 2018-06-05 04:25:12
$dictionary['Lead']['fields']['primary_address_colonia_c']['labelValue']='Colonia';
$dictionary['Lead']['fields']['primary_address_colonia_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_colonia_c']['enforced']='';
$dictionary['Lead']['fields']['primary_address_colonia_c']['dependency']='equal($status,"Converted")';

 
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Vardefs/sugarfield_primary_address_numero_c.php

 // created: 2018-06-05 04:25:46
$dictionary['Lead']['fields']['primary_address_numero_c']['labelValue']='Número';
$dictionary['Lead']['fields']['primary_address_numero_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_numero_c']['enforced']='';
$dictionary['Lead']['fields']['primary_address_numero_c']['dependency']='equal($status,"Converted")';

 
?>
