<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/status_deps.php

$dependencies['Leads']['status'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal($status, "Converted")',
    'triggerFields' => array(''),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'status',
              'value' => 'true', //Formula
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/alt_address_state_deps.php

$dependencies['Leads']['alt_address_state_actions'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal($status, "Converted")',
    'triggerFields' => array('status'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'alt_address_state',
              'value' => 'true', //Formula
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Dependencies/website_deps.php

$dependencies['Leads']['website'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal($status, "Converted")',
    'triggerFields' => array(''),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'website',
              'value' => 'true', //Formula
          ),
      ),
    ),
);

?>
