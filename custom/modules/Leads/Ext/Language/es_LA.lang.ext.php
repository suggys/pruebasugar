<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.CF_Leads_20170802_1.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.CF_lowes_leads_cstm_fields_pkg_v3.php

$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.CF_lowes_leads_cstm_fields_pkg_02.php

$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.CF_lowes_leads_folio_c_20170710.php

$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.CF_Leads_20170728.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.CF_lowes_leads_cstm_fields_pkg_01.php

$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.CF_lowes_lbl_leads_apellido_materno_c_20170712.php

$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20171007.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_FRR.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20170907.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20170922.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20170828.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20171012.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20170817.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20171020.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20171010.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_201710130100.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20171009.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20170831.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MODULE_NAME'] = 'Leads';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Lead';
$mod_strings['LNK_NEW_LEAD'] = 'Nuevo Lead';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Lead desde Vcard';
$mod_strings['LNK_LEAD_LIST'] = 'Ver Leads';
$mod_strings['LNK_LEAD_REPORTS'] = 'Ver Informes de Leads';
$mod_strings['LNK_IMPORT_LEADS'] = 'Importar Leads';
$mod_strings['LBL_ACCOUNT_DESCRIPTION'] = 'Descripción de la Cliente';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nombre o Razón Social';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente';
$mod_strings['LBL_CONVERTED_ACCOUNT'] = 'Cliente Convertido:';
$mod_strings['LNK_SELECT_ACCOUNTS'] = 'O Seleccione una Cliente';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente';
$mod_strings['LBL_STATUS'] = 'Estatus de Lead';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_LAST_NAME'] = 'Nombre o Razón Social';
$mod_strings['LBL_LIST_MY_LEADS'] = 'Mis Leads';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Datos de identificación';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono celular';
$mod_strings['LBL_ANY_EMAIL'] = 'Correo Electrónico:';
$mod_strings['LBL_WEBSITE'] = 'Página de internet';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Dirección';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número de dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio de dirección principal';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Estado Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP de dirección principal';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_CONTACTO'] = 'Contacto';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina:';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Nuevo Panel 4';
$mod_strings['LBL_CONVERTED_OPP'] = 'Proyecto Convertido:';
$mod_strings['LBL_CREATED_OPPORTUNITY'] = 'Creado un nuevo proyecto';
$mod_strings['LBL_EXISTING_OPPORTUNITY'] = 'Se utiliza un proyecto existente';
$mod_strings['LBL_OPPORTUNITIES_SUBPANEL_TITLE'] = 'Proyectos';
$mod_strings['LBL_OPPORTUNITY_AMOUNT'] = 'Cantidad del Proyecto:';
$mod_strings['LBL_OPPORTUNITY_ID'] = 'ID de Proyecto';
$mod_strings['LBL_OPPORTUNITY_NAME'] = 'Nombre del Proyecto:';
$mod_strings['LBL_OPP_NAME'] = 'Nombre del Proyecto:';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nuevo Proyecto';
$mod_strings['NTC_OPPORTUNITY_REQUIRES_ACCOUNT'] = 'La creación de un proyecto requiere un Cliente Crédito AR.\\n Por favor, o bien cree una nueva o seleccione una existente.';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Lead';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20171012.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20171007.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MODULE_NAME'] = 'Leads';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Lead';
$mod_strings['LNK_NEW_LEAD'] = 'Nuevo Lead';
$mod_strings['LNK_IMPORT_VCARD'] = 'Nuevo Lead desde Vcard';
$mod_strings['LNK_LEAD_LIST'] = 'Ver Leads';
$mod_strings['LNK_LEAD_REPORTS'] = 'Ver Informes de Leads';
$mod_strings['LNK_IMPORT_LEADS'] = 'Importar Leads';
$mod_strings['LBL_ACCOUNT_DESCRIPTION'] = 'Descripción de la Cliente';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de Cliente';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nombre o Razón Social';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente';
$mod_strings['LBL_CONVERTED_ACCOUNT'] = 'Cliente Convertido:';
$mod_strings['LNK_SELECT_ACCOUNTS'] = 'O Seleccione una Cliente';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente';
$mod_strings['LBL_STATUS'] = 'Estatus de Lead';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_LAST_NAME'] = 'Nombre o Razón Social';
$mod_strings['LBL_LIST_MY_LEADS'] = 'Mis Leads';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Datos de identificación';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono celular';
$mod_strings['LBL_ANY_EMAIL'] = 'Correo Electrónico:';
$mod_strings['LBL_WEBSITE'] = 'Página de internet';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Dirección';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número de dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio de dirección principal';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Estado Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP de dirección principal';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_CONTACTO'] = 'Contacto';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina:';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Nuevo Panel 4';
$mod_strings['LBL_CONVERTED_OPP'] = 'Proyecto Convertido:';
$mod_strings['LBL_CREATED_OPPORTUNITY'] = 'Creado un nuevo proyecto';
$mod_strings['LBL_EXISTING_OPPORTUNITY'] = 'Se utiliza un proyecto existente';
$mod_strings['LBL_OPPORTUNITIES_SUBPANEL_TITLE'] = 'Proyectos';
$mod_strings['LBL_OPPORTUNITY_AMOUNT'] = 'Cantidad del Proyecto:';
$mod_strings['LBL_OPPORTUNITY_ID'] = 'ID de Proyecto';
$mod_strings['LBL_OPPORTUNITY_NAME'] = 'Nombre del Proyecto:';
$mod_strings['LBL_OPP_NAME'] = 'Nombre del Proyecto:';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nuevo Proyecto';
$mod_strings['NTC_OPPORTUNITY_REQUIRES_ACCOUNT'] = 'La creación de un proyecto requiere un Cliente Crédito AR.\\n Por favor, o bien cree una nueva o seleccione una existente.';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Lead';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_201710130100.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20170828.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_FRR.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20170817.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20171010.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20171020.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20171009.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20170907.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20170831.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
<?php
// Merged from custom/Extension/modules/Leads/Ext/Language/es_LA.Lowes_CRM_20170922.php.cstm_names.lang.php

$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';

?>
