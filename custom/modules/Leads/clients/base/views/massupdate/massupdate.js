({
    extendsFrom: 'MassupdateView',
    initialize: function(options) {
    	this._super("initialize", [options]);
    },
    saveClicked: function(evt) {
        var self = this;
        if(this.$(".btn[name=update_button]").hasClass("disabled") === false) {
            // recuperando la colleccion a actualizar
            var collection = self.context.get('mass_collection').models;
            var finalModels = [];
            var omitRecords = false;
            _.each(collection,function(model){
                if(self._canEditRecord(model)){
                    // removiendo los modelos que son convertidos
                    //self.context.get('mass_collection').remove(model);
                    finalModels.push(model);
                    omitRecords = true;
                }
            });
            self.context.get('mass_collection').remove(finalModels);
            if(omitRecords){
                app.alert.show('noEditables',{
                      level: 'error',
                      messages: 'Algunos registros ser&aacute;n omitidos porque estan convertidos a clientes',
                      autoClose: false
                });
            }
            self.save();
        }
    },
    _canEditRecord: function(model){
      var self = this;
      var roles = app.user.get('roles');
      var result = false;
      // if(!(( _.filter(rolesArray,function(rol) { return rol.toLowerCase() == "av";}).length
      //     || _.filter(rolesArray,function(rol) { return rol.toLowerCase() == "";}).length
      //     || _.filter(rolesArray,function(rol) { return rol.toLowerCase() == "agente de venta";}).length
      //     || _.filter(rolesArray,function(rol) { return rol.toLowerCase() == "agente de venta de campo";}).length)
      //     && model.get('status') === 'Converted')
      // ){
      if( !_.contains(roles,"admin") && model.get('status') === "Converted" ){
          result = true;
      }
      return result;
    },
})
