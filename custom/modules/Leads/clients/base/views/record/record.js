({
    extendsFrom: 'RecordView',
    my_events: {
       'keyup input[name=primary_address_postalcode]': '_keypress_primary_address_postalcode',
       'keypress input[name=primary_address_postalcode]': '_keypress_event_primary_address_postalcode',
    },
    initialize: function (options) {
        var self = this;
        this._super("initialize", arguments);
        this.model.addValidationTask('alt_address_state', _.bind(this._valida_alt_address_state, this));
        this.model.addValidationTask('status', _.bind(this._valida_status, this));
        this.model.addValidationTask('email', _.bind(this._valida_email, this));
        this.model.addValidationTask('phone_mobile', _.bind(this._valida_phone_mobile, this));
        this.model.addValidationTask('validateBeforeSave',_.bind(this._doValidateBeforeSave, this));
        self.model.on('sync', function(model, value){
            self._disableField();
            self.model.on('change:primary_address_state', _.bind(self._valida_otro_estado, self));
        });
    },
    _keypress_event_primary_address_postalcode: function (event) {
        var controlKeys = [8, 9, 13, 35, 36, 37, 39];
        // IE doesn't support indexOf
        var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
        console.log(event.which);
        // Some browsers just don't raise events for control keys. Easy.
        // e.g. Safari backspace.
        if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
            (48 <= event.which && event.which <= 57) || // Always 1 through 9
            /*(48 == event.which && $(this).attr("value")) ||*/ // No 0 first digit
            isControlKey) { // Opera assigns values for control keys.
          return;
        } else {
          event.preventDefault();
        }
    },
    _keypress_primary_address_postalcode: function(evt) {
         var self = this;
         var postalCode = self.$el.find('input[name=primary_address_postalcode]').val() || '';
         var values = [];
         console.log(postalCode);

         if(String(postalCode).length == 5)
         {
             app.alert.show('datos_cp', {
               level: 'process',
               title: 'Cargando datos de direcci&oacute;n.',
               autoClose: false
            });
             var url = app.api.buildURL('Merxbp/Zipcode', 'read',{},{zipcode:postalCode});
             app.api.call('read', url, {}, {
               success: function(data){
                  self._setModeAndDisablefieldsAddress('edit', false);
                  self.model.set({'primary_address_country': 'MX'});
                  if(data.Code == 200) {
                      self.model.set({'primary_address_country': 'MX'});
                      self.model.set({'primary_address_city': data.Address.Ciudad});
                      self.model.set({'primary_address_state': data.Address.Abr_Estado});

                      _.each(data.Address.Colonias, function(col){values.push({id: col.Colonia, text: col.Colonia});});
                      values.push({id : 'Otra', text: 'Otra'});
                      self.$el.find('input[name=primary_address_colonia_c]').select2({
                        width:"100%",
                        containerCssClass: 'select2-choices-pills-close',
                        data: values
                     });

                     if(values.length == 2)
                     {
                        self.model.set({'primary_address_colonia_c': values[0].text});
                        self.$el.find('input[name=primary_address_colonia_c]').val(values[0].text).trigger('change');
                     }
                  }
                  app.alert.dismiss('datos_cp');
               }
           });
        } else {
            self._setModeAndDisablefieldsAddress('readonly', true);
            self._clearFieldsAddress();
          }
    },
    _clearFieldsAddress: function(){
        var self = this;
          self.model.set(
            {
              'primary_address_colonia_c': '',
              'primary_address_city': '',
              'primary_address_state': '',
              'alt_address_state': ''
            }
          );
      },
    _setModeAndDisablefieldsAddress: function(mode, disable){
        var self = this;
        self.getField('primary_address_colonia_c').setMode(mode);
        self.getField('primary_address_colonia_c').setDisabled(disable);

        self.getField('primary_address_city').setMode(mode);
        self.getField('primary_address_city').setDisabled(disable);

        self.getField('primary_address_country').setMode(mode);
        self.getField('primary_address_country').setDisabled(disable);

        self.getField('primary_address_state').setMode(mode);
        self.getField('primary_address_state').setDisabled(disable);

        self.getField('alt_address_state').setMode(mode);
        self.getField('alt_address_state').setDisabled(disable);
        // self.getField('otra_colonia_c').setMode(mode);
        // self.getField('otra_colonia_c').setDisabled(disable);
      },
    editClicked: function() {
   	var self = this;
   	if(self.model.get('status') === 'Converted'){
   	 //  this.setButtonStates(this.STATE.EDIT);
       	this.toggleEdit(false);
       // 	this.setRoute('edit');
        self._showMessageEdit();
      }
      else{
        this.setButtonStates(this.STATE.EDIT);
          	this.toggleEdit(true);
          	this.setRoute('edit');
      }
   },
   _valida_otro_estado: function(){
      var self = this;
      if(self.getField('alt_address_state')){
        if(self.model.get('primary_address_state') === 'OTRO'){
          $('div[data-name="alt_address_state"]').removeClass('vis_action_hidden');
        }
        else {
          $('div[data-name="alt_address_state"]').addClass('vis_action_hidden');
        }
      }
   },
   _renderHtml: function (argument) {
        var self = this;
        this._super('_renderHtml', arguments);
        self._valida_otro_estado();
        this.delegateEvents(_.extend({}, this.events, this.my_events));
    },
   _showMessageEdit:function(){
   	app.alert.show('message-id', {
       level: 'info',
       messages: 'Este Lead ya no se puede editar puesto que ya esta convertido.',
       autoClose: false
    });
   },
    _disableField:function(disableFields){
        var self = this;
        // self.getField('folio_c').setMode('readonly');
        // self.getField('folio_c').setDisabled(true);
        if(self.model.get('status') === 'Converted'){
          self.getField('status').setMode('readonly');
          self.getField('status').setDisabled(true);

          self.getField('last_name').setMode('readonly');
          self.getField('last_name').setDisabled(true);
          self.getField('rfc_c').setMode('readonly');
          self.getField('rfc_c').setDisabled(true);
          self.getField('proyecto_c').setMode('readonly');
          self.getField('proyecto_c').setDisabled(true);
          self.getField('contacto_c').setMode('readonly');
          self.getField('contacto_c').setDisabled(true);
          self.getField('id_sucursal_c').setMode('readonly');
          self.getField('id_sucursal_c').setDisabled(true);
          self.getField('tipo_persona_c').setMode('readonly');
          self.getField('tipo_persona_c').setDisabled(true);
          self.getField('email').setMode('readonly');
          self.getField('email').setDisabled(true);
          self.getField('phone_work').setMode('readonly');
          self.getField('phone_work').setDisabled(true);
          self.getField('phone_mobile').setMode('readonly');
          self.getField('phone_mobile').setDisabled(true);


          self.getField('primary_address_postalcode').setMode('readonly');
          self.getField('primary_address_postalcode').setDisabled(true);
          self.getField('primary_address_country').setMode('readonly');
          self.getField('primary_address_country').setDisabled(true);
          self.getField('primary_address_state').setMode('readonly');
          self.getField('primary_address_state').setDisabled(true);
          self.getField('primary_address_city').setDisabled(true);
          self.getField('primary_address_colonia_c').setDisabled(true);
          self.getField('primary_address_street').setDisabled(true);
          self.getField('primary_address_numero_c').setDisabled(true);
        }
    },
    _valida_alt_address_state: function(fields, errors, callback){
      var self = this;
       if(self.model.get('primary_address_state') === 'OTRO' && _.isEmpty(self.model.get('alt_address_state')) ){
                errors['alt_address_state'] = errors['alt_address_state'] || {};
                errors['alt_address_state'].required = true;
            }
      callback(null, fields, errors);
    },

    _valida_status: function(fields, errors, callback){
      var self = this;
      if(self.model.get('status') === 'Converted'){
        if(_.isEmpty(self.model.get('id_sucursal_c'))){
          errors['id_sucursal_c'] = errors['id_sucursal_c'] || {};
          errors['id_sucursal_c'].required = true;
          errors['id_sucursal_c']['Se requiere la sucursal para convertir a Prospecto.'] = true;
        }
        if(_.isEmpty(self.model.get('rfc_c'))){
          errors['rfc_c'] = errors['rfc_c'] || {};
          errors['rfc_c'].required = true;
          errors['rfc_c']['Se requiere un RFC para convertir a Prospecto.'] = true;
        }

        if(_.isEmpty(errors)){
          callback(null, fields, errors);
          return;
        }

        var filter1 = [{rfc_c:self.model.get('rfc_c')}];
        var url = app.api.buildURL('Accounts', 'read',{},{filter:filter1});
        app.api.call('read', url, {}, {
          success: function(datacta){
            if(datacta.records.length > 0){
              errors['rfc_c'] = errors['rfc_c'] || {};
              errors['rfc_c']['Existe Cliente registrado con el mismo RFC, favor de ponerse en contacto con su Gerente de Ventas Comerciales.'] = true;
              callback(null, fields, errors);
              return;
            }

            var urlp = app.api.buildURL('Prospects', 'read',{},{filter:filter1});
            app.api.call('read', urlp, {}, {
              success: function(datap){
                if(datap.records.length > 0){
                  errors['rfc_c'] = errors['rfc_c'] || {};
                  errors['rfc_c']['Existe un Prospecto registrado con el mismo RFC, favor de ponerse en contacto con su Gerente de Ventas Comerciales.'] = true;
                }
                callback(null, fields, errors);
              }
            });
          }
        });
      }
      callback(null, fields, errors);
    },

    _valida_email: function(fields, errors, callback){
      var self = this;
       if(_.isEmptyValue(self.model.get('phone_mobile')) && _.isEmptyValue(self.model.get('email')) ){
          errors['email'] = errors['email'] || {};
          errors['email'].required = true;
          errors['email']['Se requiere al menos un telefono o un email.'] = true;
      }
      if(self.model.get('status') === 'Converted' && _.isEmptyValue(self.model.get('email'))){
        errors['email'] = {};
        errors['email'].required = true;
      }
      callback(null, fields, errors);
    },
    _valida_phone_mobile: function(fields, errors, callback){
      var self = this;
       if(_.isEmptyValue(self.model.get('phone_mobile')) && _.isEmptyValue(self.model.get('email')) ){
        errors['phone_mobile'] = errors['phone_mobile'] || {};
        errors['phone_mobile'].required = true;
        errors['phone_mobile']['Se requiere al menos un telefono o un email.'] = true;
      }
      if(self.model.get('status') === 'Converted' && _.isEmptyValue(self.model.get('phone_mobile'))){
        errors['phone_mobile'] = {};
        errors['phone_mobile'].required = true;
      }
      callback(null, fields, errors);
    },

    _doValidateBeforeSave: function (fields, errors, callback){
      var self = this;
      var roles = app.user.get('roles');
      var estado = self.model.get('status');
      var modelSynced = app.data.createBean(self.model.module, self.model.getSynced());
      var changedArray = app.utils.compareBeans(self.model, modelSynced);
      if(app.user.get('type') != 'admin'){
        if(
          modelSynced.get('status') === "Converted" &&
          !_.contains(roles,"admin") &&
          !_.isEmpty(changedArray)
        ){
          errors['status'] = errors['status'] || {};
          errors['status']['No se permite modificar, porque ya esta convertido a cliente.'] = true;
        }
      }
      callback(null, fields, errors);
    },
})
