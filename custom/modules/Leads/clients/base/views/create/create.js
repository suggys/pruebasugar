({
    extendsFrom: 'CreateView',
    my_events: {
       'keyup input[name=primary_address_postalcode]': '_keypress_primary_address_postalcode',
       'keypress input[name=primary_address_postalcode]': '_keypress_event_primary_address_postalcode',
    },
    initialize: function (options) {
        this._super('initialize', arguments);
        this.model.on('change:assigned_user_name',_.bind(this._changeAssignedUserName, this));
        this.model.addValidationTask('alt_address_state', _.bind(this._valida_alt_address_state, this));
        this.model.addValidationTask('status', _.bind(this._valida_status, this));
        this.model.addValidationTask('email', _.bind(this._valida_email, this));
        this.model.addValidationTask('phone_mobile', _.bind(this._valida_phone_mobile, this));

        this.model.on('change:primary_address_country', _.bind(this._valida_primary_address_country, this));
        this.model.on('change:primary_address_state', _.bind(this._valida_primary_address_state, this));
        // this.model.on('change:primary_address_postalcode', _.bind(this._searchZipocode, this));

    },
    _keypress_event_primary_address_postalcode: function (event) {
        var controlKeys = [8, 9, 13, 35, 36, 37, 39];
        // IE doesn't support indexOf
        var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
        console.log(event.which);
        // Some browsers just don't raise events for control keys. Easy.
        // e.g. Safari backspace.
        if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
            (48 <= event.which && event.which <= 57) || // Always 1 through 9
            /*(48 == event.which && $(this).attr("value")) ||*/ // No 0 first digit
            isControlKey) { // Opera assigns values for control keys.
          return;
        } else {
          event.preventDefault();
        }
    },
    _keypress_primary_address_postalcode: function(evt) {
         var self = this;
         var postalCode = self.$el.find('input[name=primary_address_postalcode]').val() || '';
         var values = [];
         console.log(postalCode);
         if(String(postalCode).length == 5)
         {
             app.alert.show('datos_cp', {
               level: 'process',
               title: 'Cargando datos de direcci&oacute;n.',
               autoClose: false
            });
             var url = app.api.buildURL('Merxbp/Zipcode', 'read',{},{zipcode:postalCode});
             app.api.call('read', url, {}, {
               success: function(data){
                  self._setModeAndDisablefieldsAddress('edit', false);
                  self.model.set({'primary_address_country': 'MX'});
                  if(data.Code == 200) {
                      self.model.set({'primary_address_country': 'MX'});
                      self.model.set({'primary_address_city': data.Address.Ciudad});
                      self.model.set({'primary_address_state': data.Address.Abr_Estado});

                      _.each(data.Address.Colonias, function(col){values.push({id: col.Colonia, text: col.Colonia});});
                      values.push({id : 'Otra', text: 'Otra'});
                      self.$el.find('input[name=primary_address_colonia_c]').select2({
                        width:"100%",
                        containerCssClass: 'select2-choices-pills-close',
                        data: values
                     });

                     if(values.length == 2)
                     {
                        self.model.set({'primary_address_colonia_c': values[0].text});
                        self.$el.find('input[name=primary_address_colonia_c]').val(values[0].text).trigger('change');
                     }
                  }
                  app.alert.dismiss('datos_cp');
               }
           });
        } else {
            self._setModeAndDisablefieldsAddress('readonly', true);
            self._clearFieldsAddress();
          }
    },
    _clearFieldsAddress: function(){
        var self = this;
          self.model.set(
            {
              'primary_address_colonia_c': '',
              'primary_address_city': '',
              'primary_address_state': '',
              'alt_address_state': ''
            }
          );
      },
    _setModeAndDisablefieldsAddress: function(mode, disable){
        var self = this;
        self.getField('primary_address_colonia_c').setMode(mode);
        self.getField('primary_address_colonia_c').setDisabled(disable);

        self.getField('primary_address_city').setMode(mode);
        self.getField('primary_address_city').setDisabled(disable);

        self.getField('primary_address_country').setMode(mode);
        self.getField('primary_address_country').setDisabled(disable);

        self.getField('primary_address_state').setMode(mode);
        self.getField('primary_address_state').setDisabled(disable);

        self.getField('alt_address_state').setMode(mode);
        self.getField('alt_address_state').setDisabled(disable);
        // self.getField('otra_colonia_c').setMode(mode);
        // self.getField('otra_colonia_c').setDisabled(disable);
      },
    _renderHtml: function (argument) {
        var self = this;
        if(this.model.isNotEmpty){
        if(!_.isEmpty(self.model.get('assigned_user_id'))){
              idUser = self.model.get('assigned_user_id');
              var url_users = app.api.buildURL('Users/'+idUser);
              app.api.call('read', url_users, {}, {
                success: function(data){
                  if(data){
                    self.model.set({id_sucursal_c:data.sucursal_c});
                  }
                }
              });
            }
          }
        self._valida_primary_address_country();
        self._valida_primary_address_state();
        this._super('_renderHtml', arguments);
        this.delegateEvents(_.extend({}, this.events, this.my_events));
        self.$el.find('div[data-name="alt_address_state"]').addClass('vis_action_hidden');

      },
      _valida_primary_address_state: function(){
         var self = this;
         if(self.getField('alt_address_state')){
           if(self.model.get('primary_address_state') === 'OTRO'){
             $('div[data-name="alt_address_state"]').removeClass('vis_action_hidden');
           }
           else {
             $('div[data-name="alt_address_state"]').addClass('vis_action_hidden');
           }
         }
      },
      _valida_primary_address_country: function(){
         var self = this;
         if(self.getField('alt_address_state')){
           if(self.model.get('primary_address_state') === 'OTRO' && !_.isEmpty(self.model.get('primary_address_country')) ){
             $('div[data-name="alt_address_state"]').removeClass('vis_action_hidden');
           }
           else {
             $('div[data-name="alt_address_state"]').addClass('vis_action_hidden');
           }
         }
      },
      _changeAssignedUserName: function (model, value) {
        var self = this;
        idUser = self.model.get('assigned_user_id');
        var url_users = app.api.buildURL('Users/'+idUser);
        app.api.call('read', url_users, {}, {
          success: function(data){
            if(data){
              self.model.set({id_sucursal_c:data.sucursal_c});
            }
          }
        });
      },

      _valida_status: function(fields, errors, callback){
        var self = this;
        if(self.model.get('status') === 'Converted'){
          if(_.isEmpty(self.model.get('id_sucursal_c'))){
            errors['id_sucursal_c'] = errors['id_sucursal_c'] || {};
            errors['id_sucursal_c'].required = true;
            errors['id_sucursal_c']['Se requiere la sucursal para convertir a Prospecto.'] = true;
          }
          if(_.isEmpty(self.model.get('rfc_c'))){
            errors['rfc_c'] = errors['rfc_c'] || {};
            errors['rfc_c'].required = true;
            errors['rfc_c']['Se requiere un RFC para convertir a Prospecto.'] = true;
          }

          var filter1 = [{rfc_c:self.model.get('rfc_c')}];
          var url = app.api.buildURL('Accounts', 'read',{},{filter:filter1});
          app.api.call('read', url, {}, {
            success: function(datacta){
              if(datacta.records.length > 0){
                errors['rfc_c'] = errors['rfc_c'] || {};
                errors['rfc_c']['Existe Cliente registrado con el mismo RFC, favor de ponerse en contacto con su Gerente de Ventas Comerciales.'] = true;
              }
              var urlp = app.api.buildURL('Prospects', 'read',{},{filter:filter1});
              app.api.call('read', urlp, {}, {
                success: function(datap){
                  if(datap.records.length > 0){
                    errors['rfc_c'] = errors['rfc_c'] || {};
                    errors['rfc_c']['Existe un Prospecto registrado con el mismo RFC, favor de ponerse en contacto con su Gerente de Ventas Comerciales.'] = true;
                  }
                  callback(null, fields, errors);
                }
              });
              callback(null, fields, errors);
            }
          });
        }
        callback(null, fields, errors);
      },

    _valida_alt_address_state: function(fields, errors, callback){
      var self = this;
       if(self.model.get('primary_address_state') === 'OTRO' && _.isEmpty(self.model.get('alt_address_state')) ){
                errors['alt_address_state'] = errors['alt_address_state'] || {};
                errors['alt_address_state'].required = true;
            }
      callback(null, fields, errors);
    },
    _valida_email: function(fields, errors, callback){
      var self = this;
       if(_.isEmptyValue(self.model.get('phone_mobile')) && _.isEmptyValue(self.model.get('email')) ){
          errors['email'] = errors['email'] || {};
          errors['email'].required = true;
          errors['email']['Se requiere al menos un telefono o un email.'] = true;
      }
      if(self.model.get('status') === 'Converted' && _.isEmptyValue(self.model.get('email'))){
        errors['email'] = {};
        errors['email'].required = true;
      }
      callback(null, fields, errors);
    },
    _valida_phone_mobile: function(fields, errors, callback){
      var self = this;
      // debugger;
       if(_.isEmptyValue(self.model.get('phone_mobile')) && _.isEmptyValue(self.model.get('email')) ){
          errors['phone_mobile'] = errors['phone_mobile'] || {};
          errors['phone_mobile'].required = true;
          errors['phone_mobile']['Se requiere al menos un telefono o un email.'] = true;
      }

      if(self.model.get('status') === 'Converted' && _.isEmptyValue(self.model.get('phone_mobile'))){
        errors['phone_mobile'] = {};
        errors['phone_mobile'].required = true;
      }
      callback(null, fields, errors);
    }
    // ,
    //
    // _searchZipocode: function () {
    //
    // }
})
