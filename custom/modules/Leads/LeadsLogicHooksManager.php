<?php
require_once 'custom/modules/MRX1_Configuraciones/CustomMRX1Configuraciones.php';
class LeadsLogicHooksManager{
    protected static $fetchedRow = array();
    public function beforeSave($bean, $event, $arguments){
        if ( !empty($bean->id) ) {
            self::$fetchedRow[$bean->id] = $bean->fetched_row;
        }
        // -- Crea folio consecutivo

        require_once 'modules/Configurator/Configurator.php';

        $configKey = '_leads_consecutivo';
        if(self::$fetchedRow[$bean->id]['id'] != $bean->id){
            $folio = 1;
            $admin = new CustomMRX1Configuraciones();
            $admin->retrieveSettings();
            if(isset($admin->settings[$configKey]) && $admin->settings[$configKey] > 0){
              $folio = $admin->settings[$configKey] + 1;
            }
            else{
              $folio = 1;
            }
            $admin->saveSetting("leads_consecutivo", $folio);
            $bean->folio_c = $folio;
            $bean->first_name = $bean->account_name;
        }
        return true;
    }
    public function afterSave($bean, $arguments) {
      if(self::$fetchedRow[$bean->id]['proyecto_id_c'] !== $bean->proyecto_id_c){
            // Se le agrego un proyecto por lo que creamos un prospecto y le añadimos el proyecto.
      }
      if(self::$fetchedRow[$bean->id]['status'] !== $bean->status && $bean->status === 'Converted'){
          $GLOBALS['log']->fatal('entro en la converion a prospectos');
          // LAIMU - HSLR 17-05-2018
          global $db;
        
          $sqlQuery = "SELECT *
            FROM prospects p
            WHERE p.lead_id= '$bean->id';";

          $res = $db->query($sqlQuery);
          $num_row = $res->num_rows;

          if($num_row) {
          while($row = $db->fetchByAssoc($res)){
                $GLOBALS['log']->fatal('Si hay leads dentro');
                $qry="UPDATE prospects p
                    LEFT JOIN prospects_cstm pc ON p.id = pc.id_c
                    LEFT JOIN prospects_accounts_1_c pa ON pa.prospects_accounts_1prospects_ida=p.id
                    LEFT JOIN accounts a ON a.id = pa.prospects_accounts_1accounts_idb
                    LEFT JOIN accounts_cstm ac ON ac.id_c=a.id
                    SET p.deleted=0,
                    p.primary_address_street='$bean->primary_address_street',
                    p.primary_address_city='$bean->primary_address_city',
                    p.primary_address_state='$bean->primary_address_state',
                    p.primary_address_postalcode='$bean->primary_address_postalcode',
                    p.primary_address_country='$bean->primary_address_country',
                    pc.primary_address_colonia_c='$bean->primary_address_colonia_c',
                    pc.primary_address_number_c=$bean->primary_address_numero_c,
                    a.deleted=0,
                    ac.primary_address_street_c='$bean->primary_address_street',
                    ac.primary_address_city_c='$bean->primary_address_city',
                    ac.primary_address_state_c='$bean->primary_address_state',
                    ac.primary_address_postalcode_c='$bean->primary_address_postalcode',
                    ac.primary_address_country_c='$bean->primary_address_country',
                    ac.primary_address_colonia_c='$bean->primary_address_colonia_c',
                    ac.primary_address_numero_c=$bean->primary_address_numero_c
                    where p.lead_id='$bean->id';";
                $res1 = $db->query($qry);
              }
              $GLOBALS['log']->fatal($qry);
          }else{
            $GLOBALS['log']->fatal('no hay leads');
            //Fin

            $prospecto = BeanFactory::newBean('Prospects');
            //$prospecto->first_name = $bean->first_name;
            $prospecto->last_name = $bean->last_name;
            //$prospecto->razon_social_c = $bean->account_name;
            $prospecto->sucursal_c = $bean->id_sucursal_c;
            $prospecto->assigned_user_name = $bean->assigned_user_name;
            $prospecto->assigned_user_id = $bean->assigned_user_id;
            $prospecto->tipo_de_persona_c = $bean->tipo_persona_c;
            $prospecto->rfc_c = $bean->rfc_c;
            $prospecto->phone_work = $bean->phone_work;
            $prospecto->phone_mobile = $bean->phone_mobile;
            $prospecto->email = $bean->email;
            $prospecto->primary_address_postalcode = $bean->primary_address_postalcode;
            $prospecto->primary_address_country = $bean->primary_address_country;
            $prospecto->primary_address_state = $bean->primary_address_state;
            $prospecto->alt_address_state = $bean->alt_address_state;
            $prospecto->primary_address_city = $bean->primary_address_city;
            $prospecto->primary_address_colonia_c = $bean->primary_address_colonia_c;
            $prospecto->primary_address_street = $bean->primary_address_street;
            $prospecto->primary_address_number_c = $bean->primary_address_numero_c;
            $prospecto->web_page_c = $bean->website;
            $prospecto->lead_id = $bean->id;
            $prospecto->save();

            $contacto = BeanFactory::getBean('Contacts',$bean->contact_id_c);
            $prospecto->load_relationship('prospects_contacts_1');
            $prospecto->prospects_contacts_1->add($contacto);

            $proyecto = BeanFactory::getBean('Opportunities',$bean->proyecto_id_c);
            $prospecto->load_relationship('prospects_opportunities_1');
            $prospecto->prospects_opportunities_1->add($proyecto);

            $this->heredaRelatedRecords("calls", $bean, $prospecto);
            $this->heredaRelatedRecords("meetings", $bean, $prospecto);
            $this->heredaRelatedRecords("tasks", $bean, $prospecto);
            $this->heredaRelatedRecords("notes", $bean, $prospecto);
          }
      }

      return true;
    }

    public function getRelatedBeans($bean, $linkName)
    {
      $saldoDeudor = 0;
      $sugarQuery = new SugarQuery();
      $sugarQuery->from($bean, array('team_security'=>false));
      $joinName = $sugarQuery->join($linkName, array('team_security'=>false))->joinName();
      $sugarQuery->where()->equals('id', $bean->id);
      $sugarQuery->select(array("$joinName.id"));
      $results = $sugarQuery->execute();
      return $results;
    }

    private function heredaRelatedRecords($linkName, $lead, $prospect)
    {
      $beans = [];
      $link = null;
      // $beans = $this->getRelatedBeans($lead, $linkName);
      $lead->load_relationship($linkName);
      $prospect->load_relationship($linkName);
      switch ($linkName) {
        case 'calls':
          $link = $prospect->calls;
          $beans = $lead->calls->getBeans();
          break;
        case 'meetings':
          $link = $prospect->meetings;
          $beans = $lead->meetings->getBeans();
          break;
        case 'tasks':
          $link = $prospect->tasks;
          $beans = $lead->tasks->getBeans();
          break;
        case 'notes':
          $link = $prospect->notes;
          $beans = $lead->notes->getBeans();
          break;
      }
      foreach ($beans as $bean) {
        $bean->parent_type = "Prospects";
        $bean->parent_id = $prospect->id;
        $link->add($bean);
      }
    }
}
?>
