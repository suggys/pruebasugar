({
  extendsFrom: "FullnameField",
  initialize: function () {
    this._super("initialize", arguments);
    var meta = app.metadata.getModule(this.module);

    this.def.fields = [
      meta.fields['first_name'],
      meta.fields['last_name'],
      meta.fields['apellido_materno_c']
    ];
  },
})
