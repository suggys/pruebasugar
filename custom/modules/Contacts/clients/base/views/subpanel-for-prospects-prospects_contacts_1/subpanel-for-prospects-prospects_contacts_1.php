<?php
// created: 2017-08-07 21:39:41
$viewdefs['Contacts']['base']['view']['subpanel-for-prospects-prospects_contacts_1'] = array (
  'type' => 'subpanel-list',
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'phone_work',
          'label' => 'LBL_LIST_PHONE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'email',
          'label' => 'LBL_LIST_EMAIL',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'tipo_c',
          'label' => 'LBL_TIPO_C',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
);