<?php
// created: 2018-09-15 05:21:23
$viewdefs['Contacts']['base']['view']['dupecheck-list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'fullname',
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'first_name',
            1 => 'last_name',
            2 => 'apellido_materno_c',
          ),
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
          'related_fields' => 
          array (
            0 => 'apellido_materno_c',
          ),
        ),
        1 => 
        array (
          'name' => 'rfc_representante_legal_c',
          'label' => 'LBL_RFC_REPRESENTANTE_LEGAL',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'email',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'phone_mobile',
          'label' => 'LBL_MOBILE_PHONE',
          'enabled' => true,
          'default' => false,
        ),
        4 => 
        array (
          'name' => 'name',
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'salutation',
            1 => 'first_name',
            2 => 'last_name',
          ),
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
);