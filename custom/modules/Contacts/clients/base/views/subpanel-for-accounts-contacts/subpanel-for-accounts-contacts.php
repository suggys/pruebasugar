<?php
// created: 2017-08-03 01:15:52
$viewdefs['Contacts']['base']['view']['subpanel-for-accounts-contacts'] = array (
  'type' => 'subpanel-list',
  'panels' =>
  array (
    0 =>
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array (
        0 =>
        array (
          'name' => 'first_name',
          'label' => 'LBL_FIRST_NAME',
          'enabled' => true,
          'default' => true,
          'link' => true,
        ),
        1 =>
        array (
          'name' => 'last_name',
          'label' => 'LBL_LAST_NAME',
          'enabled' => true,
          'default' => true,
          'link' => true,
        ),
        2 =>
        array (
          'name' => 'apellido_materno_c',
          'label' => 'LBL_LEADS_APELLIDO_MATERNO_C',
          'enabled' => true,
          'default' => true,
          'link' => true,
        ),
        3 =>
        array (
          'name' => 'phone_work',
          'label' => 'LBL_LIST_PHONE',
          'enabled' => true,
          'default' => true,
        ),
        4 =>
        array (
          'name' => 'email',
          'label' => 'LBL_LIST_EMAIL',
          'enabled' => true,
          'default' => true,
        ),
        5 =>
        array (
          'name' => 'tipo_c',
          'label' => 'LBL_TIPO_C',
          'enabled' => true,
          'default' => true,
        ),
        6 =>
        array (
          'name' => 'primary_address_state',
          'label' => 'LBL_LIST_STATE',
          'enabled' => true,
          'default' => true,
        ),
        7 =>
        array (
          'name' => 'primary_address_city',
          'label' => 'LBL_LIST_CITY',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
);
