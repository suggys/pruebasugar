<?php
// created: 2018-09-15 05:21:23
$viewdefs['Contacts']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'fullname',
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'first_name',
            1 => 'last_name',
            2 => 'apellido_materno_c',
          ),
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
          'related_fields' => 
          array (
            0 => 'apellido_materno_c',
          ),
        ),
        1 => 
        array (
          'name' => 'account_name',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'prospects_contacts_1_name',
          'label' => 'LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE',
          'enabled' => true,
          'id' => 'PROSPECTS_CONTACTS_1PROSPECTS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'rfc_representante_legal_c',
          'label' => 'LBL_RFC_REPRESENTANTE_LEGAL',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'email',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'phone_work',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_USER',
          'id' => 'ASSIGNED_USER_ID',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'date_modified',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'date_entered',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
        ),
        9 => 
        array (
          'name' => 'tipo_c',
          'label' => 'LBL_TIPO_C',
          'enabled' => true,
          'default' => false,
        ),
        10 => 
        array (
          'name' => 'subtipo_c',
          'label' => 'LBL_SUBTIPO_C',
          'enabled' => true,
          'default' => false,
        ),
        11 => 
        array (
          'name' => 'estado_c',
          'label' => 'LBL_SUBTIPO_C',
          'enabled' => true,
          'default' => false,
        ),
        12 => 
        array (
          'name' => 'genre_c',
          'label' => 'LBL_GENRE',
          'enabled' => true,
          'default' => false,
        ),
        13 => 
        array (
          'name' => 'phone_mobile',
          'label' => 'LBL_MOBILE_PHONE',
          'enabled' => true,
          'default' => false,
        ),
        14 => 
        array (
          'name' => 'portal_active',
          'label' => 'LBL_PORTAL_ACTIVE',
          'enabled' => true,
          'default' => false,
        ),
        15 => 
        array (
          'name' => 'portal_name',
          'label' => 'LBL_PORTAL_NAME',
          'enabled' => true,
          'default' => false,
        ),
        16 => 
        array (
          'name' => 'name',
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'salutation',
            1 => 'first_name',
            2 => 'last_name',
          ),
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
);