<?php
// created: 2018-09-15 05:21:23
$viewdefs['Contacts']['portal']['view']['record'] = array (
  'buttons' => 
  array (
    0 => 
    array (
      'type' => 'button',
      'name' => 'cancel_button',
      'label' => 'LBL_CANCEL_BUTTON_LABEL',
      'css_class' => 'btn-invisible btn-link',
      'showOn' => 'edit',
      'events' => 
      array (
        'click' => 'button:cancel_button:click',
      ),
    ),
    1 => 
    array (
      'type' => 'rowaction',
      'event' => 'button:save_button:click',
      'name' => 'save_button',
      'label' => 'LBL_SAVE_BUTTON_LABEL',
      'css_class' => 'btn btn-primary',
      'showOn' => 'edit',
      'acl_action' => 'edit',
    ),
    2 => 
    array (
      'type' => 'actiondropdown',
      'name' => 'main_dropdown',
      'primary' => true,
      'showOn' => 'view',
      'buttons' => 
      array (
        0 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:download_pdf:click',
          'name' => 'download_pdf_report',
          'label' => 'LBL_DOWNLOAD_PDF_REPORT',
          'acl_action' => 'view',
        ),
      ),
    ),
    3 => 
    array (
      'name' => 'sidebar_toggle',
      'type' => 'sidebartoggle',
    ),
  ),
  'hashSync' => false,
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_body',
      'label' => 'LBL_RECORD_BODY',
      'columns' => 2,
      'labelsOnTop' => true,
      'placeholders' => true,
      'newTab' => false,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'cuenta_sucursal_c',
          'label' => 'LBL_CUENTA_SUCURSAL',
        ),
        1 => 
        array (
          'name' => 'cuenta_vendedor_c',
          'label' => 'LBL_CUENTA_VENDEDOR',
        ),
        2 => 
        array (
          'name' => 'cuenta_nombre_c',
          'label' => 'LBL_CUENTA_NOMBRE',
        ),
        3 => 
        array (
          'name' => 'cuenta_rfc_c',
          'label' => 'LBL_CUENTA_RFC',
        ),
        4 => 
        array (
          'name' => 'cuenta_tel_oficina_c',
          'label' => 'LBL_CUENTA_TEL_OFICINA',
        ),
        5 => 
        array (
          'name' => 'cuenta_email_comercial_c',
          'label' => 'LBL_CUENTA_EMAIL_COMERCIAL',
        ),
        6 => 
        array (
          'name' => 'cuenta_direccion_calle_c',
          'label' => 'LBL_CUENTA_DIRECCION_CALLE',
        ),
        7 => 
        array (
          'name' => 'cuenta_direccion_numero_c',
          'label' => 'LBL_CUENTA_DIRECCION_NUMERO',
        ),
        8 => 
        array (
          'name' => 'cuenta_direccion_colonia_c',
          'label' => 'LBL_CUENTA_DIRECCION_COLONIA',
        ),
        9 => 
        array (
          'name' => 'cuenta_direccion_cp_c',
          'label' => 'LBL_CUENTA_DIRECCION_CP',
        ),
        10 => 
        array (
          'name' => 'cuenta_direccion_municipio_c',
          'label' => 'LBL_CUENTA_DIRECCION_MUNICIPIO',
        ),
        11 => 
        array (
          'name' => 'cuenta_direccion_estado_c',
          'label' => 'LBL_CUENTA_DIRECCION_ESTADO',
        ),
        12 => 
        array (
          'name' => 'cuenta_direccion_pais_c',
          'label' => 'LBL_CUENTA_DIRECCION_PAIS',
        ),
        13 => 
        array (
        ),
        14 => 
        array (
        ),
        15 => 
        array (
        ),
        16 => 
        array (
          'name' => 'name',
          'label' => 'LBL_NAME',
          'dismiss_label' => true,
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'salutation',
            1 => 'first_name',
            2 => 'last_name',
          ),
        ),
      ),
    ),
    1 => 
    array (
      'name' => 'panel_info_gral',
      'label' => 'LBL_PANEL_INFO_GRAL',
      'columns' => 2,
      'labelsOnTop' => true,
      'placeholders' => true,
      'newTab' => false,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'cuenta_limite_credito_autorizado_c',
          'label' => 'LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO',
        ),
        1 => 
        array (
          'name' => 'cuenta_id_arcredit_c',
          'label' => 'LBL_CUENTA_ID_ARCREDIT',
        ),
        2 => 
        array (
          'name' => 'cuenta_plazo_pagos_c',
          'label' => 'LBL_CUENTA_PLAZO_PAGOS',
        ),
        3 => 
        array (
          'name' => 'cuenta_credito_disponible_c',
          'label' => 'LBL_CUENTA_CREDITO_DISPONIBLE',
        ),
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'useTabs' => false,
  ),
);