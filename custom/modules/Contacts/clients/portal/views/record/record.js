({
    extendsFrom: 'RecordView',
    initialize: function (options) {
    	this._super('initialize', [options]);
    	//add listener for custom button
        this.context.on('button:download_pdf:click', this.downloadPdf, this);
    },
    downloadPdf: function(templateId){
    	var self = this;
    	var getURL = Backbone.history.location.origin;
    	var idContact = app.user.get('id');
    	console.log(getURL);
    	if(getURL)
    		window.open(getURL+'/index.php?entryPoint=PdfFromPortal&idContacto='+idContact+'&pdf=1');
    	else
    		window.open('https://lowesqa.sugarondemand.com/index.php?entryPoint=PdfFromPortal&idContacto='+idContact+'&pdf=1');
    },
})