<?php
// created: 2017-08-03 01:15:36
$subpanel_layout['list_fields'] = array (
  'first_name' => 
  array (
    'name' => 'first_name',
    'usage' => 'query_only',
  ),
  'last_name' => 
  array (
    'name' => 'last_name',
    'usage' => 'query_only',
  ),
  'apellido_materno_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_LEADS_APELLIDO_MATERNO_C',
    'width' => '10%',
  ),
  'phone_work' => 
  array (
    'name' => 'phone_work',
    'vname' => 'LBL_LIST_PHONE',
    'width' => '10%',
    'default' => true,
  ),
  'email' => 
  array (
    'name' => 'email',
    'vname' => 'LBL_LIST_EMAIL',
    'widget_class' => 'SubPanelEmailLink',
    'width' => '10%',
    'sortable' => false,
    'default' => true,
  ),
  'tipo_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TIPO_C',
    'width' => '10%',
  ),
  'primary_address_state' => 
  array (
    'name' => 'primary_address_state',
    'vname' => 'LBL_LIST_STATE',
    'width' => '10%',
    'default' => true,
  ),
  'primary_address_city' => 
  array (
    'name' => 'primary_address_city',
    'vname' => 'LBL_LIST_CITY',
    'width' => '10%',
    'default' => true,
  ),
  'salutation' => 
  array (
    'name' => 'salutation',
    'usage' => 'query_only',
  ),
);