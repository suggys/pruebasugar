<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Contact']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_tel_oficina_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_tel_oficina_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_tel_oficina_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_tel_oficina_c']['formula']='related($accounts,"phone_office")';
$dictionary['Contact']['fields']['cuenta_tel_oficina_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/low01_solicitudescredito_contacts_1_Contacts.php

// created: 2017-10-12 22:45:07
$dictionary["Contact"]["fields"]["low01_solicitudescredito_contacts_1"] = array (
  'name' => 'low01_solicitudescredito_contacts_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_contacts_1',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => 'LOW01_SolicitudesCredito',
  'side' => 'right',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["low01_solicitudescredito_contacts_1_name"] = array (
  'name' => 'low01_solicitudescredito_contacts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'save' => true,
  'id_name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
  'link' => 'low01_solicitudescredito_contacts_1',
  'table' => 'low01_solicitudescredito',
  'module' => 'LOW01_SolicitudesCredito',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["low01_solicitudescredito_contacts_1low01_solicitudescredito_ida"] = array (
  'name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
  'link' => 'low01_solicitudescredito_contacts_1',
  'table' => 'low01_solicitudescredito',
  'module' => 'LOW01_SolicitudesCredito',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/FilterDuplicateCheck.php

$dictionary['Contact']['duplicate_check']['FilterDuplicateCheck'] = array(
    'filter_template' => array(
        array(
            '$and' => array(
                array('first_name' => array('$equals' => '$first_name')),
                array('last_name' => array('$equals' => '$last_name')),
                array('apellido_materno_c' => array('$equals' => '$apellido_materno_c'))
            )
        ),
    ),
    'ranking_fields' => array(
        array('in_field_name' => 'first_name', 'dupe_field_name' => 'first_name'),
        array('in_field_name' => 'last_name', 'dupe_field_name' => 'last_name'),
        array('in_field_name' => 'apellido_materno_c', 'dupe_field_name' => 'apellido_materno_c'),
    )
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_direccion_colonia_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_direccion_colonia_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_direccion_colonia_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_direccion_colonia_c']['formula']='related($accounts,"primary_address_colonia_c")';
$dictionary['Contact']['fields']['cuenta_direccion_colonia_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_rfc_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_rfc_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_rfc_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_rfc_c']['formula']='related($accounts,"rfc_c")';
$dictionary['Contact']['fields']['cuenta_rfc_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_direccion_pais_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_direccion_pais_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_direccion_pais_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_direccion_pais_c']['formula']='getDropdownValue("primary_address_country_c_list",related($accounts,"primary_address_country_c"))';
$dictionary['Contact']['fields']['cuenta_direccion_pais_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_plazo_pagos_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_plazo_pagos_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_plazo_pagos_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_plazo_pagos_c']['formula']='related($accounts,"plazo_pago_autorizado_c")';
$dictionary['Contact']['fields']['cuenta_plazo_pagos_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_limite_credito_autorizado_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_limite_credito_autorizado_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_limite_credito_autorizado_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_limite_credito_autorizado_c']['formula']='related($accounts,"limite_credito_autorizado_c")';
$dictionary['Contact']['fields']['cuenta_limite_credito_autorizado_c']['enforced']='true';
$dictionary['Contact']['fields']['cuenta_limite_credito_autorizado_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_nombre_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_nombre_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_nombre_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_nombre_c']['formula']='related($accounts,"name")';
$dictionary['Contact']['fields']['cuenta_nombre_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_direccion_calle_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_direccion_calle_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_direccion_calle_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_direccion_calle_c']['formula']='related($accounts,"primary_address_street_c")';
$dictionary['Contact']['fields']['cuenta_direccion_calle_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_direccion_estado_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_direccion_estado_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_direccion_estado_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_direccion_estado_c']['formula']='getDropdownValue("state_c_list",related($accounts,"primary_address_state_c"))';
$dictionary['Contact']['fields']['cuenta_direccion_estado_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_direccion_numero_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_direccion_numero_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_direccion_numero_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_direccion_numero_c']['formula']='related($accounts,"primary_address_numero_c")';
$dictionary['Contact']['fields']['cuenta_direccion_numero_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_direccion_cp_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_direccion_cp_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_direccion_cp_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_direccion_cp_c']['formula']='related($accounts,"primary_address_postalcode_c")';
$dictionary['Contact']['fields']['cuenta_direccion_cp_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_vendedor_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_vendedor_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_vendedor_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_vendedor_c']['formula']='related($accounts,"user_vendedor_c")';
$dictionary['Contact']['fields']['cuenta_vendedor_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2016-09-13 19:28:13

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/prospects_contacts_1_Contacts.php

// created: 2017-07-20 21:33:08
$dictionary["Contact"]["fields"]["prospects_contacts_1"] = array (
  'name' => 'prospects_contacts_1',
  'type' => 'link',
  'relationship' => 'prospects_contacts_1',
  'source' => 'non-db',
  'module' => 'Prospects',
  'bean_name' => 'Prospect',
  'side' => 'right',
  'vname' => 'LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'prospects_contacts_1prospects_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["prospects_contacts_1_name"] = array (
  'name' => 'prospects_contacts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE',
  'save' => true,
  'id_name' => 'prospects_contacts_1prospects_ida',
  'link' => 'prospects_contacts_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["prospects_contacts_1prospects_ida"] = array (
  'name' => 'prospects_contacts_1prospects_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'prospects_contacts_1prospects_ida',
  'link' => 'prospects_contacts_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_credito_disponible_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_credito_disponible_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_credito_disponible_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_credito_disponible_c']['formula']='ifElse(equal(related($accounts,"estado_bloqueo_c"),"Bloqueado"),0,related($accounts,"credito_disponible_c"))';
$dictionary['Contact']['fields']['cuenta_credito_disponible_c']['enforced']='true';
$dictionary['Contact']['fields']['cuenta_credito_disponible_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_email_comercial_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_email_comercial_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_email_comercial_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_email_comercial_c']['formula']='related($accounts,"email1")';
$dictionary['Contact']['fields']['cuenta_email_comercial_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_direccion_municipio_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_direccion_municipio_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_direccion_municipio_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_direccion_municipio_c']['formula']='related($accounts,"primary_address_city_c")';
$dictionary['Contact']['fields']['cuenta_direccion_municipio_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2016-10-07 09:28:37

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/ContactsVisibility.php

$dictionary['Contact']['visibility']["ContactsVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_estado_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['estado_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['estado_c']['labelValue']='Estatus';
$dictionary['Contact']['fields']['estado_c']['dependency']='';
$dictionary['Contact']['fields']['estado_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_sucursal_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_sucursal_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_sucursal_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_sucursal_c']['formula']='getDropdownValue("sucursal_c_list",related($accounts,"id_sucursal_c"))';
$dictionary['Contact']['fields']['cuenta_sucursal_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_cuenta_id_arcredit_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_id_arcredit_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_id_arcredit_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_id_arcredit_c']['formula']='related($accounts,"id_ar_credit_c")';
$dictionary['Contact']['fields']['cuenta_id_arcredit_c']['enforced']='true';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_apellido_materno_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['apellido_materno_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['apellido_materno_c']['labelValue']='Apellido Materno';
$dictionary['Contact']['fields']['apellido_materno_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['apellido_materno_c']['enforced']='';
$dictionary['Contact']['fields']['apellido_materno_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_tipo_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['tipo_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['tipo_c']['labelValue']='Tipo';
$dictionary['Contact']['fields']['tipo_c']['dependency']='';
$dictionary['Contact']['fields']['tipo_c']['visibility_grid']='';
$dictionary['Contact']['fields']['tipo_c']['options']='contacto_tipo_list';
//$dictionary['Contact']['fields']['tipo_c']['required']=1;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_rfc_representante_legal_c.php

 // created: 2017-08-11 21:47:21
$dictionary['Contact']['fields']['rfc_representante_legal_c']['labelValue']='RFC Representante Legal';
$dictionary['Contact']['fields']['rfc_representante_legal_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['rfc_representante_legal_c']['enforced']='';
$dictionary['Contact']['fields']['rfc_representante_legal_c']['dependency']='';
$dictionary['Contact']['fields']['rfc_representante_legal_c']['len']=13;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_genre_c.php

 // created: 2017-08-13 01:04:02
$dictionary['Contact']['fields']['genre_c']['labelValue']='Género';
$dictionary['Contact']['fields']['genre_c']['dependency']='';
$dictionary['Contact']['fields']['genre_c']['visibility_grid']='';
$dictionary['Contact']['fields']['genre_c']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_first_name.php

 // created: 2017-08-01 21:07:02
$dictionary['Contact']['fields']['first_name']['required']=true;
$dictionary['Contact']['fields']['first_name']['audited']=false;
$dictionary['Contact']['fields']['first_name']['massupdate']=false;
$dictionary['Contact']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Contact']['fields']['first_name']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['first_name']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['first_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['first_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.99',
  'searchable' => true,
);
$dictionary['Contact']['fields']['first_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_portal_active.php

 // created: 2017-08-25 16:04:03
$dictionary['Contact']['fields']['portal_active']['default']=false;
$dictionary['Contact']['fields']['portal_active']['audited']=false;
$dictionary['Contact']['fields']['portal_active']['massupdate']=false;
$dictionary['Contact']['fields']['portal_active']['comments']='Indicator whether this contact is a portal user';
$dictionary['Contact']['fields']['portal_active']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['portal_active']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['portal_active']['merge_filter']='disabled';
$dictionary['Contact']['fields']['portal_active']['unified_search']=false;
$dictionary['Contact']['fields']['portal_active']['calculated']=false;
$dictionary['Contact']['fields']['portal_active']['dependency']='not(equal(related($accounts,"id_ar_credit_c"),""))';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_mobile.php

 // created: 2017-08-08 06:22:40
$dictionary['Contact']['fields']['phone_mobile']['len']='10';
$dictionary['Contact']['fields']['phone_mobile']['audited']=false;
$dictionary['Contact']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Contact']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.09',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_mobile']['calculated']=false;
$dictionary['Contact']['fields']['phone_mobile']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_last_name.php

 // created: 2017-08-01 16:55:57
$dictionary['Contact']['fields']['last_name']['audited']=false;
$dictionary['Contact']['fields']['last_name']['massupdate']=false;
$dictionary['Contact']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Contact']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['last_name']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.97',
  'searchable' => true,
);
$dictionary['Contact']['fields']['last_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_phone_work.php

 // created: 2017-08-13 01:05:38
$dictionary['Contact']['fields']['phone_work']['len']='13';
$dictionary['Contact']['fields']['phone_work']['massupdate']=false;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['phone_work']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_work']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.08',
  'searchable' => true,
);
$dictionary['Contact']['fields']['phone_work']['calculated']=false;
$dictionary['Contact']['fields']['phone_work']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_tipo_otro_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['tipo_otro_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['tipo_otro_c']['labelValue']='Otro Tipo';
$dictionary['Contact']['fields']['tipo_otro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['tipo_otro_c']['enforced']='';
$dictionary['Contact']['fields']['tipo_otro_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_subtipo_c.php

 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['subtipo_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['subtipo_c']['labelValue']='Subtipo';
$dictionary['Contact']['fields']['subtipo_c']['dependency']='';
$dictionary['Contact']['fields']['subtipo_c']['visibility_grid']='';

 
?>
