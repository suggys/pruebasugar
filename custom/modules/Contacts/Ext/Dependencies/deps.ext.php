<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/rfc_rep_legal_required.php

$dependencies['Contacts']['rfc_rep_legal_c_required'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'equal($tipo_c,"representante_legal")',
  'triggerFields' => array('tipo_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'rfc_representante_legal_c',
        'label'  => 'rfc_representante_legal_c_label',
        'value' => 'true',
        'label' => 'Requerido',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'rfc_representante_legal_c',
        'label'  => 'rfc_representante_legal_c_label',
        'value' => 'false',
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/SubTipo_required.php

// Obsoleto

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/tipo_otro_c_vicibilidad.php

$dependencies['Contacts']['tipo_otro_c_visibility'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'equal($tipo_c,"otro")',
  'triggerFields' => array('tipo_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'tipo_otro_c',
        'label'  => 'tipo_otro_c_label',
        'value' => 'true',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'tipo_otro_c',
        'label'  => 'tipo_otro_c_label',
        'value' => 'false',
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/prospects_contacts_1_name_readonly.php

$dependencies['Contacts']['prospects_contacts_1_name_readonly'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'not(equal($account_name,""))',
  'triggerFields' => array('account_name'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'prospects_contacts_1_name',
        'label'  => 'prospects_contacts_1_name_label',
        'value' => 'true',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'prospects_contacts_1_name',
        'label'  => 'prospects_contacts_1_name_label',
        'value' => 'false',
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/tipo_otro_c_required.php

$dependencies['Contacts']['tipo_otro_c_required'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'equal($tipo_c,"otro")',
  'triggerFields' => array('tipo_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'tipo_otro_c',
        'label'  => 'tipo_otro_c_label',
        'value' => 'true',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'tipo_otro_c',
        'label'  => 'tipo_otro_c_label',
        'value' => 'false',
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/genre_c_required.php

// Vacío JLHC

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Dependencies/account_name_readonly.php

$dependencies['Contacts']['account_name_readonly'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'not(equal($prospects_contacts_1_name,""))',
  'triggerFields' => array('prospects_contacts_1_name'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'account_name',
        'label'  => 'account_name_label',
        'value' => 'true',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'account_name',
        'label'  => 'account_name_label',
        'value' => 'false',
      ),
    ),
  ),
);

?>
