<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de la Cliente Credito AR:';
$mod_strings['LBL_EXISTING_ACCOUNT'] = 'Cliente existente utilizada';
$mod_strings['LBL_LIST_ACCOUNT_NAME'] = 'Cliente';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccione Cliente';
$mod_strings['NTC_OPPORTUNITY_REQUIRES_ACCOUNT'] = 'La creación de un proyecto requiere una Cliente.\\n Por favor, cree una nueva Cliente o seleccione una existente.';
$mod_strings['LBL_GENRE_C'] = 'Género';
$mod_strings['LBL_RFC_REPRESENTANTE_LEGAL'] = 'RFC Representante Legal';
$mod_strings['LBL_GENRE'] = 'Género';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Información General';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_TIPO'] = 'Tipo';
$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_PANEL_INFO_GRAL'] = 'Información Crediticia';
$mod_strings['LBL_DOWNLOAD_PDF_REPORT'] = 'Estado de Cuenta';
$mod_strings['LBL_CUENTA_RFC'] = 'RFC';
$mod_strings['LBL_CUENTA_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = 'Número';
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = 'Estado';
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = 'País';
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = 'Crédito Disponible';
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = 'Email Comercial';
$mod_strings['LBL_LAST_NAME'] = 'Apellidos Paterno';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE'] = 'Prospecto';
$mod_strings['LBL_ESTADO_C'] = 'Estatus';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Datos de Identificación';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina:';
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = 'Plazo de Pago Autorizado';
$mod_strings['LBL_CREATED_OPPORTUNITY'] = 'Nuevo proyecto creado';
$mod_strings['LBL_EXISTING_OPPORTUNITY'] = 'Proyecto existente utilizado';
$mod_strings['LBL_OPPORTUNITIES'] = 'Proyectos';
$mod_strings['LBL_OPPORTUNITIES_SUBPANEL_TITLE'] = 'Proyectos';
$mod_strings['LBL_OPPORTUNITY_ROLE'] = 'Rol en Proyecto';
$mod_strings['LBL_OPPORTUNITY_ROLE_ID'] = 'ID de Rol en Proyecto:';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Crear Proyecto';
$mod_strings['LBL_TIPO_OTRO_C'] = 'Otro Tipo';
$mod_strings['LBL_PORTAL_ACTIVE'] = 'Portal Activo:';
$mod_strings['LBL_ANY_EMAIL'] = 'Correo Electrónico:';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_20171007.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_FRR.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_20170907.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.CF_Contacts_20170725.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";
?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_20170922.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_20170828.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_20171012.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_20170817.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_20171020.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_20171010.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_201710130100.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_20171009.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.customprospects_contacts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE_ID'] = 'Prospectos ID';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Prospectos';

?>
<?php
// Merged from custom/Extension/modules/Contacts/Ext/Language/es_LA.Lowes_CRM_20170831.php

$mod_strings['LBL_TIPO_C'] = 'Tipo';
$mod_strings['LBL_SUBTIPO_C'] = 'Sub Tipo';
$mod_strings['LBL_CONTACTS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_CUENTA_SUCURSAL'] = "Sucursal";
$mod_strings['LBL_CUENTA_VENDEDOR'] = "Vendedor";
$mod_strings['LBL_CUENTA_NOMBRE'] = "Nombre o Razón Social";
$mod_strings['LBL_CUENTA_RFC'] = "RFC";
$mod_strings['LBL_CUENTA_TEL_OFICINA'] = "Teléfono de Oficina";
$mod_strings['LBL_CUENTA_EMAIL_COMERCIAL'] = "Email Comercial";
$mod_strings['LBL_CUENTA_DIRECCION_CALLE'] = "Calle";
$mod_strings['LBL_CUENTA_DIRECCION_NUMERO'] = "Número";
$mod_strings['LBL_CUENTA_DIRECCION_COLONIA'] = "Colonia";
$mod_strings['LBL_CUENTA_DIRECCION_CP'] = "C. P.";
$mod_strings['LBL_CUENTA_DIRECCION_MUNICIPIO'] = "Municipio";
$mod_strings['LBL_CUENTA_DIRECCION_ESTADO'] = "Estado";
$mod_strings['LBL_CUENTA_DIRECCION_PAIS'] = "País";
$mod_strings['LBL_CUENTA_LIMITE_CREDITO_AUTORIZADO'] = "Límite de Crédito Autorizado";
$mod_strings['LBL_CUENTA_ID_ARCREDIT'] = "ID AR Credit";
$mod_strings['LBL_CUENTA_PLAZO_PAGOS'] = "Plazo de Pago Autorizado";
$mod_strings['LBL_CUENTA_CREDITO_DISPONIBLE'] = "Crédito Disponible";

?>
