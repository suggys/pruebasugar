<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_Importa_Facturas_FTP.php


array_push($job_strings, 'importaFacturasFTP');

function importaFacturasFTP(){
	require_once('custom/modules/Schedulers/FTPGateway.php');
	require_once('custom/modules/Import/CustomImporter.php');
	require_once('include/upload_file.php');
	// descargando cuentas
	$gateway = new FTPGateway();
	try {
		$GLOBALS['log']->debug(" ==== Iniciando =====");
		$gateway->connect();
		$GLOBALS['log']->debug(" ==== listado de archivos =====");
		$response = $gateway->scanFilesystem('bdi_sugarcrm','facturas',false);
		if(count($response)){
			$files = $response;
			$localUrl = 'upload/';
			$GLOBALS['log']->debug(" ==== descargando archivos =====");
			$bean = BeanFactory::getBean('LF_Facturas');
			$stream = new UploadStream();
			foreach ($files as $key => $file) {
				$gateway->downloadFile('bdi_sugarcrm/'.$file,$localUrl.$file);
				importaArchivo($localUrl.$file,$bean);
				// eliminando archivo local
				$stream->unlink('upload://'.$file);
				// moviendo archivo remoto
				$gateway->moveFile('bdi_sugarcrm/'.$file,'bdi_sugarcrm_procesado/'.$file);
			}
		}
	}
	catch (Exception $e){
	    $GLOBALS['log']->debug(" ** Error *** importaFacturasFTP ***** ".$e->getMessage());
	}
	return true;
}
function importaArchivo($localFile,$bean) {
	if( $localFile && $bean ){
		// leyendo archivos
        $_REQUEST = array(
            'colnum_0'    => 'name',
            'colnum_1'    => 'description',
            'colnum_2'    => 'tipo_documento',
            'colnum_3'    => 'estado_tiempo',
            'colnum_4'    => 'no_ticket',
            'colnum_5'    => 'sub_total',
            'colnum_6'    => 'iva',
            'colnum_7'    => 'importe',
            'colnum_8'    => 'refactura',
            'colnum_9'    => 'fecha_entrega',
            'colnum_10'    => 'dt_crt',
            'colnum_11'    => 'fecha_vencimiento',
            'colnum_12'    => 'dias_vencimiento',
            'colnum_13'    => 'abono',
            'colnum_14'    => 'saldo',
            'colnum_15'    => 'estado',
            'colnum_16'    => 'uuid',
            'colnum_17'    => 'serie',
            'colnum_18'    => 'rfc_emisor',
            'colnum_19'    => 'nombre_emisor',
            'colnum_20'    => 'rfc_receptor',
            'colnum_21'    => 'name_receptor',
            'columncount' => '21',
            'importlocale_charset' => 'UTF-8',
            'importlocale_currency' => '-99',
            'importlocale_dateformat' => 'Y-m-d',
            'importlocale_dec_sep' => '.',
            'importlocale_default_currency_significant_digits' => '2',
            //'importlocale_default_locale_name_format' => 's f l',
            'importlocale_num_grp_sep' => ',',
            'importlocale_timeformat' => 'H:i',
            'importlocale_timezone' => 'America/Mexico_City',
            'import_module' => 'LF_Facturas',
        );
		$importFile = new ImportFile( $localFile );
		if($importFile->fileExists()){
			$autoDetectOk = $importFile->autoDetectCSVProperties();
			$importFile->setHeaderRow(true); // indicando que tiene header row
			$totalRecords = $importFile->getTotalRecordCount(); // indicando que tiene header row
			//$GLOBALS['log']->fatal(" ==== Total de registros a importar  =====".$totalRecords);
			$importer = new CustomImporter($importFile, $bean);
    		$importer->import();
    		//$GLOBALS['log']->fatal(" ==== Archivo ===== ".$localFile." importado :) ");
		}
	}
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_clasifica_autorizacion_esp.php

$job_strings[] = 'JOB_clasifica_autorizacion_esp';
array_push($job_strings, 'JOB_clasifica_autorizacion_esp');
// Reporte Promesas de pago clasifica
function JOB_clasifica_autorizacion_esp(){

	global $timedate, $current_user, $db;

    $query1 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = 'vigente'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) = 0
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result1 = $db->query($query1);

    $query2 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = '1a30'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) BETWEEN 1 AND 30
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result2 = $db->query($query2);

    $query3 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = '31a60'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) BETWEEN 31 AND 60
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result3 = $db->query($query3);

    $query4 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = '61a90'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) BETWEEN 61 AND 90
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result4 = $db->query($query4);

    $query5 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = '91a120'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) BETWEEN 91 AND 120
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result5 = $db->query($query5);

    $query6 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = 'mas120'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) > 120
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result6 = $db->query($query6);

    $query7 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = ''
            WHERE a.monto_promesa_c = a.monto_abonado_c
            AND a.deleted = 0;  ";
    $result7 = $db->query($query7);


    //$GLOBALS['log']->fatal("Se ejecutó calculo de vigencia de autorización especial.");
    return true;
}
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/job_calculo_facturas_vencidas.php

$job_strings[] = 'job_calculo_facturas_vencidas';

array_push($job_strings, 'job_calculo_facturas_vencidas');

function job_calculo_facturas_vencidas() {
    return true;
}



?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_clasifica_facturasVigencia.php

$job_strings[] = 'JOB_clasifica_facturas_vigencia';
array_push($job_strings, 'JOB_clasifica_facturas_vigencia');
// Reporte Promesas de pago clasifica
function JOB_clasifica_facturas_vigencia(){

	global $timedate, $db;
	// $GLOBALS["log"]->fatal("antes de UPDATE");
	$query1 ="
	UPDATE lf_facturas_cstm fc
	LEFT JOIN lf_facturas f ON f.id = fc.id_c
	LEFT JOIN (
	 SELECT f0.id, f0.dt_crt, accounts_cstm.plazo_pago_autorizado_c, accounts_cstm.dias_tolerancia_c,
	 accounts_cstm.dias_tolerancia_c + accounts_cstm.plazo_pago_autorizado_c dias,
	 DATEDIFF(NOW(), f0.dt_crt) diffnow,
	 DATEDIFF(NOW(), f0.dt_crt) - (accounts_cstm.dias_tolerancia_c + accounts_cstm.plazo_pago_autorizado_c) dias_vencidos
	 FROM lf_facturas f0
	 INNER JOIN accounts_lf_facturas_1_c ON f0.id = accounts_lf_facturas_1_c.accounts_lf_facturas_1lf_facturas_idb and accounts_lf_facturas_1_c.deleted = 0
	 INNER JOIN accounts_cstm ON accounts_cstm.id_c = accounts_lf_facturas_1_c.accounts_lf_facturas_1accounts_ida and accounts_lf_facturas_1_c.deleted = 0
	 INNER JOIN accounts a ON accounts_cstm.id_c = a.id
	 LEFT JOIN lf_facturas_lf_facturas_c ff ON ff.lf_facturas_lf_facturaslf_facturas_ida = f0.id
	 WHERE f0.deleted = 0
	 AND f0.refactura = 0
	 AND ff.lf_facturas_lf_facturaslf_facturas_ida IS NULL

	 UNION

	 SELECT afc.id_c id, afc.fecha_emision_padre_c dt_crt, accounts_cstm.plazo_pago_autorizado_c, accounts_cstm.dias_tolerancia_c,
	 accounts_cstm.dias_tolerancia_c + accounts_cstm.plazo_pago_autorizado_c dias,
	 DATEDIFF(NOW(), IFNULL(afc.fecha_emision_padre_c,0)) diffnow,
	 DATEDIFF(NOW(), afc.fecha_emision_padre_c) - (accounts_cstm.dias_tolerancia_c + accounts_cstm.plazo_pago_autorizado_c) dias_vencidos
   FROM lf_facturas_cstm afc
	 INNER JOIN lf_facturas f0 ON f0.id = afc.id_c
	 INNER JOIN accounts_lf_facturas_1_c ON f0.id = accounts_lf_facturas_1_c.accounts_lf_facturas_1lf_facturas_idb AND accounts_lf_facturas_1_c.deleted = 0
	 INNER JOIN accounts_cstm ON accounts_cstm.id_c = accounts_lf_facturas_1_c.accounts_lf_facturas_1accounts_ida
	 INNER JOIN accounts a ON accounts_cstm.id_c = a.id
	 INNER JOIN lf_facturas_lf_facturas_c ff ON ff.lf_facturas_lf_facturaslf_facturas_idb = afc.id_c
	 WHERE f0.deleted = 0
	 AND f0.deleted = 0
	) faux ON faux.id = f.id
	SET fc.vigencia_c =
		CASE
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) <= 0 THEN 'vigente'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) BETWEEN 1 AND 15 THEN '1a15'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) BETWEEN 16 AND 30 THEN '16a30'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) BETWEEN 31 AND 60 THEN '31a60'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) BETWEEN 61 AND 90 THEN '61a90'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) BETWEEN 91 AND 120 THEN '91a120'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) > 120 THEN 'mas120'
		END,
	fc.dias_vencidos_sin_tol_c =
		CASE
		WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) > 0 THEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY))
		WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) <= 0 THEN 0
		END,
	f.estado_tiempo =
		CASE
			WHEN f.estado_tiempo = 'devuelta_parcial' THEN 'devuelta_parcial'
			ELSE
				CASE
					WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) > 0 THEN 'vencida'
					WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) <= 0 THEN 'vigente'
				END
		END,
	f.dias_vencimiento =
		CASE
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0) + IFNULL(faux.dias_tolerancia_c, 0) ) DAY)) > 0 THEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0) + IFNULL(faux.dias_tolerancia_c, 0) ) DAY))
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0) + IFNULL(faux.dias_tolerancia_c, 0) ) DAY)) <= 0 THEN 0
		END,
	f.fecha_vencimiento = DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY),
	fc.fecha_vencimiento_flexible_c = DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0) + IFNULL(faux.dias_tolerancia_c, 0) ) DAY)
	WHERE f.tipo_documento = 'factura'
	AND fc.linea_anticipo_c = 0
	AND f.deleted = 0
	AND (f.estado_tiempo IN ('vigente', 'vencida', 'devuelta_parcial' , '') OR f.estado_tiempo IS NULL)
	";
	$result1 = $db->query($query1);

  $query00 = "UPDATE lf_facturas_cstm fc
	    LEFT JOIN lf_facturas f ON f.id = fc.id_c
	    LEFT JOIN lowes_pagos_lf_facturas_c p ON p.lowes_pagos_lf_facturaslf_facturas_idb = f.id
	    LEFT JOIN lowes_pagos lp ON lp.id = p.lowes_pagos_lf_facturaslowes_pagos_ida
	    LEFT JOIN lowes_pagos_accounts_c pa ON pa.lowes_pagos_accountslowes_pagos_idb = lp.id
	    LEFT JOIN accounts_cstm ac ON pa.lowes_pagos_accountsaccounts_ida = ac.id_c
	    SET fc.dias_vencido_pagada_c = DATEDIFF(lp.fecha_transaccion,f.dt_crt)-ac.plazo_pago_autorizado_c-ac.dias_tolerancia_c
	    WHERE f.tipo_documento = 'factura'
	    AND f.deleted = 0
			AND fc.linea_anticipo_c = 0
	    AND dt_crt IS NOT NULL
	    AND p.lowes_pagos_lf_facturaslowes_pagos_ida IS NOT NULL;  ";
    $result00 = $db->query($query00);

	$query01 = "UPDATE lf_facturas_cstm fc
				LEFT JOIN lf_facturas f ON f.id = fc.id_c
				SET fc.dias_trascurridos_c = DATEDIFF(NOW(),f.dt_crt)
				WHERE f.saldo > 0
				AND fc.linea_anticipo_c = 0
				AND f.tipo_documento = 'factura';  ";
    $result01 = $db->query($query01);
    return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/job_clasifica_facturas_segun_tiempo.php

$job_strings[] = 'job_clasifica_facturas_segun_tiempo';

array_push($job_strings, 'job_clasifica_facturas_segun_tiempo');

function job_clasifica_facturas_segun_tiempo() {
    $isDebug = false;
    return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_importa_partidas_JSON.php

require_once 'custom/include/ImportadorPartidas.php';
$job_strings[] = 'JOB_importa_partidas_JSON';
array_push($job_strings, 'JOB_importa_partidas_JSON');
// Reporte Promesas de pago clasifica
function JOB_importa_partidas_JSON(){
  $importadorPartidas = new ImportadorPartidas();
  $importadorPartidas->process();
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_importa_ordenes_JSON.php

require_once "custom/include/ImportadorOrdenes.php";
$job_strings[] = 'importaOrdenesJSON';
array_push($job_strings, 'importaOrdenesJSON');

function importaOrdenesJSON(){
  $importador = new ImportadorOrdenes();
  $importador->process();
  return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_importa_notasDeCredito_JSON.php

require_once 'custom/include/HealthCheckImport.php';
$job_strings[] = 'importaNotasDeCreditoJSON';
array_push($job_strings, 'importaNotasDeCreditoJSON');

function importaNotasDeCreditoJSON()
{
	$saludDeImportaciones = new HealthCheckImport();
	$result = $saludDeImportaciones->obtenerRegistrosJSON("n_credito");

	if (!empty($result))
	{
		if ($result['code'] == "200")
		{
			$registrados = 0;
			$filename = explode('.', $result['filename']);
			$saludDeNotasCredito = $saludDeImportaciones->notasDeCredito($result);

			if (empty($saludDeNotasCredito['n_CreditoConError']))
			{
				$registrados = $saludDeImportaciones->guardarBeans($saludDeNotasCredito['n_CreditoAGuardar'],$result['filename']);
				if ($registrados == count($saludDeNotasCredito['n_CreditoAGuardar']))
				{
					$esProcesado = $saludDeImportaciones->esProcesadoConExito($filename[0]);
				}
			} else {
				if (in_array(5,$saludDeNotasCredito['typeErrors']) || in_array(6,$saludDeNotasCredito['typeErrors'])){
					$saludDeImportaciones->crearNotaDeError("n_credito",$saludDeNotasCredito['typeErrors']);
				} else {
					if(!empty($saludDeNotasCredito['n_CreditoAGuardar'])){
						$registrados = $saludDeImportaciones->guardarBeans($saludDeNotasCredito['n_CreditoAGuardar'],$result['filename']);
					}
					$saludDeImportaciones->crearNotaDeError("n_credito",$saludDeNotasCredito['typeErrors'],$saludDeNotasCredito['n_CreditoConError'],$filename[0],$registrados);
				}
				$esProcesado = $saludDeImportaciones->esProcesadoConErrores($filename[0]);
			}
			return $saludDeImportaciones->logProcesados("NotasDeCredito",$esProcesado);
		} else {
			$GLOBALS['log']->fatal($result['message']);
			return false;
		}
	} else {
		$GLOBALS['log']->fatal('Error con el middleware');
		return false;
	}
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/job_actualiza_estado_autorizacion_especial.php

$job_strings[] = 'job_actualiza_estado_autorizacion_especial';
array_push($job_strings, 'job_actualiza_estado_autorizacion_especial');

function job_actualiza_estado_autorizacion_especial() {
  $whereRaw = "(lae.fecha_fin_c < NOW() AND lae.motivo_solicitud_c IN ('credito_disponible_temporal','otro')) OR (lae.fecha_cumplimiento_c < NOW() AND lae.motivo_solicitud_c IN ('promesa_de_pago'))";
  $sq = new SugarQuery();
	$sq->select(array('id'));
  $sq->from(BeanFactory::newBean('lowae_autorizacion_especial'),array('alias'=>'lae','team_security'=>false));
  $sq->where()->equals('lae_c.estado_autorizacion_c','autorizada');
	$sq->whereRaw($whereRaw);
	$result = $sq->execute();
	foreach ($result as $a) {
		$beanAutEsp = BeanFactory::getBean('lowae_autorizacion_especial',$a['id']);
    $beanAutEsp->estado_c = 'no_aplicado';
    $beanAutEsp->estado_autorizacion_c = 'finalizada';
		$beanAutEsp->save(false);
	}
  return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_importa_facturas_JSON.php

require_once 'custom/include/HealthCheckImport.php';
$job_strings[] = 'importaFacturasJSON';
array_push($job_strings, 'importaFacturasJSON');

function importaFacturasJSON()
{
	$saludDeImportaciones = new HealthCheckImport();
	$result = $saludDeImportaciones->obtenerRegistrosJSON("facturas");

	if (!empty($result))
	{
		if ($result['code'] == "200")
		{
			$registrados = 0;
			$filename = explode('.', $result['filename']);
			$saludDeFacturas = $saludDeImportaciones->facturas($result);

			if (empty($saludDeFacturas['facturasConError']))
			{
				$registrados = $saludDeImportaciones->guardarBeans($saludDeFacturas['facturasAGuardar'],$result['filename']);
				if ($registrados == count($saludDeFacturas['facturasAGuardar']))
				{
					$esProcesado = $saludDeImportaciones->esProcesadoConExito($filename[0]);
				}
			} else {
				if (in_array(5,$saludDeFacturas['typeErrors']) || in_array(6,$saludDeFacturas['typeErrors'])){
					$saludDeImportaciones->crearNotaDeError("facturas",$saludDeFacturas['typeErrors']);
				} else {
					if(!empty($saludDeFacturas['facturasAGuardar'])){
						$registrados = $saludDeImportaciones->guardarBeans($saludDeFacturas['facturasAGuardar'],$result['filename']);
					}
					$saludDeImportaciones->crearNotaDeError("facturas",$saludDeFacturas['typeErrors'],$saludDeFacturas['facturasConError'],$filename[0],$registrados);
				}
				$esProcesado = $saludDeImportaciones->esProcesadoConErrores($filename[0]);
			}
			return $saludDeImportaciones->logProcesados("Facturas",$esProcesado);
		} else {
			$GLOBALS['log']->fatal($result['message']);
			return false;
		}
	} else {
		$GLOBALS['log']->fatal('Error con el middleware');
		return false;
	}
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/job_incumplimiento_promesa_pago.php

$job_strings[] = 'job_incumplimiento_promesa_pago';

array_push($job_strings, 'job_incumplimiento_promesa_pago');

function job_incumplimiento_promesa_pago() {
    global $db, $timedate;

    $query ="UPDATE lowae_autorizacion_especial ae
    INNER JOIN lowae_autorizacion_especial_cstm aec on ae.id = aec.id_c
    INNER JOIN (
      SELECT id, name, fecha_cumplimiento_c, aec.dias_vencimiento_promesa_c,
      DATEDIFF(NOW(), ae.fecha_cumplimiento_c) diffnow
      FROM lowae_autorizacion_especial ae
      INNER JOIN lowae_autorizacion_especial_cstm aec ON ae.id = aec.id_c
      WHERE ae.deleted = 0
      AND ae.motivo_solicitud_c = 'promesa_de_pago'
      AND aec.estado_promesa_segun_monto_c IN ('sin_pago', 'pagada_parcial')
      AND aec.estado_autorizacion_c IN ('autorizada', 'finalizada')
      AND ae.fecha_cumplimiento_c < NOW()
    ) aei ON aei.id = ae.id
    SET ae.estado_promesa_c = 'incumplida'";
    $result1 = $db->query($query);

    $query ="UPDATE lowae_autorizacion_especial ae
    INNER JOIN lowae_autorizacion_especial_cstm aec on ae.id = aec.id_c
    INNER JOIN (
      SELECT id, name, fecha_cumplimiento_c, aec.dias_vencimiento_promesa_c,
      DATEDIFF(NOW(), ae.fecha_cumplimiento_c) diffnow
      FROM lowae_autorizacion_especial ae
      INNER JOIN lowae_autorizacion_especial_cstm aec ON ae.id = aec.id_c
      WHERE ae.deleted = 0
      AND ae.motivo_solicitud_c = 'promesa_de_pago'
      AND ae.monto_abonado_c < ae.monto_promesa_c
      AND ae.estado_promesa_c = 'incumplida'
      AND aec.estado_promesa_segun_monto_c IN ('sin_pago', 'pagada_parcial')
    ) aei ON aei.id = ae.id
    SET aec.dias_vencimiento_promesa_c = aei.diffnow";

    $result1 = $db->query($query);

    return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/job_low_bloqueo_automatico.php

$job_strings[] = 'job_low_bloqueo_automatico';

array_push($job_strings, 'job_low_bloqueo_automatico');

/*************************************************************
FR-11.5. Proceso de bloqueo automático
Escenario 1: Obtener cuentas activas a bloquear por Cartera vencida 1 a 30 días
Dado: Las siguientes cuentas
1. Nombre: [FR-11.5: Cuenta Inactiva], estado de la cuenta: [Inactiva]
2. Nombre: [FR-11.5: Cuenta Activa], estado de la cuenta: [Activa], Estado Según Cartera: [Vigente], Estado de bloqueo:[Desbloqueado]
3. Nombre: [FR-11.5: Cuenta Activa Vencida], estado de la cuenta: [Activa],
 Estado Según Cartera: [Cartera vencida 1 a 30 días], Estado de bloqueo:[Desbloqueado]
Cuando:  Una función filtre las cuentas para obtener las activas con catera vencida
Entonces: Debe de obtener una colección con solamente el registro con Nombre: [FR-11.5: Cuenta Activa Vencida]

Escenario 2: Obtener cuentas activas a bloquear por Cartera vencida 31 a 120 días
Dado: Las siguientes cuentas
1. Nombre: [FR-11.5: Cuenta Inactiva], estado de la cuenta: [Inactiva]
2. Nombre: [FR-11.5: Cuenta Activa], estado de la cuenta: [Activa], Estado Según Cartera: [Vigente], Estado de bloqueo:[Desbloqueado]
3. Nombre: [FR-11.5: Cuenta Activa Vencida], estado de la cuenta: [Activa],
 Estado Según Cartera: [Cartera vencida 31 a 120 días], Estado de bloqueo:[Desbloqueado]
Cuando:  Una función filtre las cuentas para obtener las activas con catera vencida
Entonces: Debe de obtener una colección con solamente el registro con Nombre: [FR-11.5: Cuenta Activa Vencida]

Escenario 3: Obtener cuentas activas a bloquear por Cartera vencida 120 días
Dado: Las siguientes cuentas
1. Nombre: [FR-11.5: Cuenta Inactiva], estado de la cuenta: [Inactiva]
2. Nombre: [FR-11.5: Cuenta Activa], estado de la cuenta: [Activa], Estado Según Cartera: [Vigente], Estado de bloqueo:[Desbloqueado]
3. Nombre: [FR-11.5: Cuenta Activa Vencida], estado de la cuenta: [Activa],
 Estado Según Cartera: [Cartera vencida 120 días], Estado de bloqueo:[Desbloqueado]
Cuando:  Una función filtre las cuentas para obtener las activas con catera vencida
Entonces: Debe de obtener una colección con solamente el registro con Nombre: [FR-11.5: Cuenta Activa Vencida]

Escenario 4: Obtener cuentas activas a bloquear por Cartera vencida 120 días omitiendo Bloquedas
Dado: Las siguientes cuentas
1. Nombre: [FR-11.5: Cuenta Inactiva], estado de la cuenta: [Inactiva]
2. Nombre: [FR-11.5: Cuenta Activa], estado de la cuenta: [Activa], Estado Según Cartera: [Vigente], Estado de bloqueo:[Desbloqueado]
3. Nombre: [FR-11.5: Cuenta Activa Vencida], estado de la cuenta: [Activa],
 Estado Según Cartera: [Cartera vencida 120 días], Estado de bloqueo:[Desbloqueado]
4. Nombre: [FR-11.5: Cuenta Activa Vencida Bloqueada], estado de la cuenta: [Activa],
 Estado Según Cartera: [Cartera vencida 120 días], Estado de bloqueo:[Bloqueado]
Cuando:  Una función filtre las cuentas para obtener las activas con catera vencida
Entonces: Debe de obtener una colección con solamente el registro con Nombre: [FR-11.5: Cuenta Activa Vencida]
**************************************************************/
function job_low_bloqueo_automatico(){
    //$cuentasBloqueadas = fn_bloqueoDeCuentasXCarteraVencida();
    // $GLOBALS['log']->fatal( 'job_low_bloqueo_automatico::cuentasBloqueadas:'. $cuentasBloqueadas);

    //$cuentasBloqueadas = fn_bloqueGpoCuentasCarteraVencida();
    // $GLOBALS['log']->fatal( 'job_low_bloqueo_automatico::cuentasBloqueadas Gpo:'. $cuentasBloqueadas);

    return true;
}

/*
Escenario 6: Bloquear cuentas con Codigo de bloqueo [CV – Cartera vencida] omitiendo Cuentas Bloqueadas
Dado: Un grupo de empresas con Nombre: [FR-11.5-E1: GE], Sumar / Administrar Límite de Crédito por Grupo[Activo]
- Y: Una colección de 3 Cuentas con Estado de bloqueo:[Desbloquedo] Relacionada al registro del modulo Grupo de empresas con Nombre: [FR-11.5-E6: GE]
- Y: Una cuenta con Nombre[FR-11.5-E6: Cuenta 4] Estado de bloqueo:[Desbloquedo]
Cuando:  Se procesen por una función para bloquear por cartera vencida
Entonces: Debe de refrescar la información de cada cuenta y validar Estado de bloqueo igual a [Desbloquedo], si es verdadero Asignar  Estado de bloqueo:[Bloqueado],  Codigo de bloqueo[CV – Cartera vencida]
*/
function fn_bloqueGpoCuentasCarteraVencida() {
  $cuentasBloqueadas = 0;
  require_once('include/SugarQuery/SugarQuery.php');

  $sq = new SugarQuery();
  $sq->select(array('id'));
  $sq->from(BeanFactory::newBean('Accounts'),array('alias'=>'a','team_security'=>false));
  $sq->joinTable('gpe_grupo_empresas_accounts_c', array('alias' => 'gpa'))->on()
  ->equalsField('gpa.gpe_grupo_empresas_accountsaccounts_idb','a.id');
  $sq->joinTable('gpe_grupo_empresas', array('alias' => 'gpe'))->on()
  ->equalsField('gpe.id','gpa.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida');
  $sq->whereRaw("gpe.administrar_credito_grupal = 1");
  $sq->where()->queryAnd()->equals('estado_bloqueo_c','Desbloqueado')
  ->equals('estado_cuenta_c','Activo')
  ->gt('saldo_pendiente_aplicar_c',0);
  $sq->oderBy("id");
  $result = $sq->execute();

  // $sqlQuery = "SELECT accounts.name, IFNULL(gpe_grupo_empresas.name, '') GPO,
  // accounts.id id_emp, gpe_grupo_empresas.id id_gpo, id_c, estado_cuenta_c, estado_bloqueo_c,
  // codigo_bloqueo_c, estado_cartera_c, saldo_pendiente_aplicar_c, administrar_credito_grupal
  // FROM accounts
  // INNER JOIN accounts_cstm ON accounts.id = id_c
  // INNER JOIN gpe_grupo_empresas_accounts_c ON gpe_grupo_empresas_accountsaccounts_idb = accounts.id
  // INNER JOIN gpe_grupo_empresas ON gpe_grupo_empresas.id = gpe_grupo_empresas_accountsgpe_grupo_empresas_ida
  // WHERE accounts.deleted <> 1 AND gpe_grupo_empresas.administrar_credito_grupal = 1
  // AND estado_bloqueo_c='Desbloqueado' AND estado_cuenta_c='Activo' AND saldo_pendiente_aplicar_c > 0
  // ORDER BY accounts.id";

  foreach ($result as $key => $row) {
    $beanAccount = BeanFactory::getBean('Accounts', $row['id_c']);
    $beanAccount->codigo_bloqueo_c = 'CV';
    $beanAccount->estado_bloqueo_c = 'Bloqueado';
    $beanAccount->save();
    $cuentasBloqueadas++;
  }
  return $cuentasBloqueadas;
}

/*
Escenario 5: Bloquear cuentas con Codigo de bloqueo [CV – Cartera vencida]
Dado: Una colección de 3 Cuentas con Estado de bloqueo:[Desbloquedo]
Cuando:  Se procesen por una función para bloquear por cartera vencida
Entonces: La cada una de las cuentas procecadas debe de tener  Estado de bloqueo:[Bloqueado],  Codigo de bloqueo[CV – Cartera vencida]
*/
function fn_bloqueoDeCuentasXCarteraVencida() {
  $cuentasBloqueadas = 0;
  require_once('include/SugarQuery/SugarQuery.php');

  $sq = new SugarQuery();
  $sq->select(array('id'));
  $sq->from(BeanFactory::newBean('Accounts'),array('alias'=>'a','team_security'=>false));
  $sq->joinTable('gpe_grupo_empresas_accounts_c', array('alias' => 'gpa'))->on()
  ->equalsField('gpa.gpe_grupo_empresas_accountsaccounts_idb','a.id');
  $sq->joinTable('gpe_grupo_empresas', array('alias' => 'gpe'))->on()
  ->equalsField('gpe.id','gpa.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida');
  $sq->whereRaw("IFNULL(gpe.administrar_credito_grupal, 0) = 0");
  $sq->where()->queryAnd()->equals('estado_bloqueo_c','Desbloqueado')
  ->equals('estado_cuenta_c','Activo')
  ->gt('saldo_pendiente_aplicar_c',0);
  $sq->oderBy("id");
  $result = $sq->execute();

  // $sqlQuery = "SELECT id
  // FROM accounts
  // INNER JOIN accounts_cstm ON accounts.id = id_c
  // LEFT JOIN gpe_grupo_empresas_accounts_c ON gpe_grupo_empresas_accountsaccounts_idb = accounts.id
  // LEFT JOIN gpe_grupo_empresas ON gpe_grupo_empresas.id = gpe_grupo_empresas_accountsgpe_grupo_empresas_ida
  // WHERE accounts.deleted <> 1 AND IFNULL(gpe_grupo_empresas.administrar_credito_grupal, 0) = 0
  // AND estado_bloqueo_c='Desbloqueado' AND estado_cuenta_c='Activo' AND saldo_pendiente_aplicar_c > 0
  // ORDER BY accounts.id";

  foreach ($result as $key => $row) {
    $beanAccount = BeanFactory::getBean('Accounts', $row['id_c']);
    $beanAccount->codigo_bloqueo_c = 'CV';
    $beanAccount->estado_bloqueo_c = 'Bloqueado';
    $beanAccount->save();
    $cuentasBloqueadas++;
  }
  return $cuentasBloqueadas;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_actualiza_campo_sin_orden_xdias_c.php

$job_strings[] = 'job_actualiza_check_sin_orden_xdias_c';
array_push($job_strings, 'job_actualiza_check_sin_orden_xdias_c');
/**
 * [job_actualiza_check_sin_orden_xdias_c Se consultan la relacion mas reciente de ordenes con cuentas y si han pasado los dias
 * configurados setea el campo sin_orden_xdias_c siempre y cuando la cuenta no tenga estatus inactivo]
 * @return [bool]
 */
function job_actualiza_check_sin_orden_xdias_c() {
    global $db;
    // obteniendo la cantidad de dias parametrizada
    $dias = 120;
    $admin = new Administration();
    $admin->retrieveSettings();
    if (array_key_exists('lowes_settings_accounts_actividad_cliente', $admin->settings))
        $dias = $admin->settings['lowes_settings_accounts_actividad_cliente'];
    // consultando cuentas activas que no tengan mas de x dias sin ordenes y que la orden relacionada mas reciente tenga mas de x dias
    $Ctas = new SugarQuery();
    $Ctas->select('id');
    $beanCuentas = BeanFactory::newBean('Accounts');
    $Ctas->from($beanCuentas, array('team_security' => false));
    $Ctas->join('accounts_orden_ordenes_1',array('alias'=>'o'));
    $Ctas->where()
         ->equals('estado_cuenta_c','Activo')
         ->equals('sin_orden_xdias_c','false');
    $Ctas->groupBy('id');
    $Ctas->havingRaw("datediff(date(now()),max(date(jt0_accounts_orden_ordenes_1_c.date_modified))) > ".$dias);
    $resCuentas = $Ctas->execute();
    actualizaCuentas($resCuentas);
    return true;
}
function actualizaCuentas($resCuentas){
    if(count($resCuentas)>0){
        // encontramos cuentas por actualizar +120 dias sin ordenes
        foreach ($resCuentas as $key => $cuenta) {
            $beanAccount = BeanFactory::getBean('Accounts', $cuenta['id']);
            if($beanAccount->id){
                $beanAccount->sin_orden_xdias_c = true;
                $beanAccount->save(false);
            }
        }
    }
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_importa_pagos_JSON.php

require_once 'custom/include/HealthCheckImport.php';
$job_strings[] = 'importaPagosJSON';
array_push($job_strings, 'importaPagosJSON');

function importaPagosJSON()
{
	$saludDeImportaciones = new HealthCheckImport();
	$result = $saludDeImportaciones->obtenerRegistrosJSON("pagos");

	if (!empty($result))
	{
		if ($result['code'] == "200")
		{
			$registrados = 0;
			$filename = explode('.', $result['filename']);
			$saludDePagos = $saludDeImportaciones->pagos($result);

			if (empty($saludDePagos['pagosConError']))
			{
				$registrados = $saludDeImportaciones->guardarBeans($saludDePagos['pagosAGuardar'],$result['filename']);
				if ($registrados == count($saludDePagos['pagosAGuardar']))
				{
					$esProcesado = $saludDeImportaciones->esProcesadoConExito($filename[0]);
				}
			} else {
				if (in_array(5,$saludDePagos['typeErrors']) || in_array(6,$saludDePagos['typeErrors'])){
					$saludDeImportaciones->crearNotaDeError("pagos",$saludDePagos['typeErrors']);
				} else {
					if(!empty($saludDePagos['pagosAGuardar'])){
						$registrados = $saludDeImportaciones->guardarBeans($saludDePagos['pagosAGuardar'],$result['filename']);
					}
					$saludDeImportaciones->crearNotaDeError("pagos",$saludDePagos['typeErrors'],$saludDePagos['pagosConError'],$filename[0],$registrados);
				}
				$esProcesado = $saludDeImportaciones->esProcesadoConErrores($filename[0]);
			}
			return $saludDeImportaciones->logProcesados("Pagos",$esProcesado);
		} else {
			$GLOBALS['log']->fatal($result['message']);
			return false;
		}
	} else {
		$GLOBALS['log']->fatal('Error con el middleware');
		return false;
	}
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_envia_pagos_konesh.php

require_once 'custom/include/PagosKonesh.php';
$job_strings[] = 'JOB_envia_pagos_kones';
array_push($job_strings, 'JOB_envia_pagos_konesh');
// Reporte Promesas de pago clasifica
function JOB_envia_pagos_konesh(){
  $pagoKonesh = new PagosKonesh();
  $pagoKonesh->process();
  return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/ops_backups_fetch_exports.php


$job_strings[] = 'ops_backups_fetch_exports';

function ops_backups_fetch_exports() {
    global $sugar_config;

    // First lets clean out any old junk
    $Backup = BeanFactory::newBean('ops_Backups');

    if (empty($Backup)) {
        $GLOBALS['log']->error("Failed to instantiate ops_Backups bean");
        return;
    }

    ops_Backups::removeExpired();

    $backups = $Backup->getAppInstanceExports();

    foreach ($backups as $backup) {
        $backup = $Backup->verifyExport($backup);
        if ($backup) {
            $sql = new SugarQuery();
            $sql->select('id');
            $sql->from($Backup);
            $sql->where()->equals('download_url', $backup->download_url);

            $result = $sql->execute();
            $count = count($result);

            if ($count == 0) {
                $newBean = BeanFactory::newBean($Backup->module_name);
                if (empty($newBean)) {
                    $GLOBALS['log']->error("Failed to instantiate a new ops_Backups bean");
                    continue;
                }
                $newBean->name = (isset($backup->name)?$backup->name:$sugar_config['host_name']);
                $newBean->date_entered = $backup->created_at->format('Y-m-d H:i:s');
                $newBean->expires_at = $backup->expires_at->format('Y-m-d H:i:s');
                $newBean->description = sprintf(translate('LBL_CREATED_DESC', 'ops_Backups'),
                    $sugar_config['host_name'],
                    $backup->created_at->format($GLOBALS['timedate']->get_date_format())
                );
                $newBean->description .= sprintf(translate('LBL_EXPIRES_DESC', 'ops_Backups'),
                    $backup->expires_at->format($GLOBALS['timedate']->get_date_format())
                );
                $newBean->download_url = $backup->download_url;
                $newBean->save();
                if ($newBean->id) {
                    $GLOBALS['log']->info(sprintf("opsBackups saved backup: %s", $newBean->id));
                } else {
                    $GLOBALS['log']->fatal(sprintf("opsBackups failed to save backup: %s", $backup->download_url));
                }
            } else {
                $GLOBALS['log']->info(sprintf("opsBackups skipping this export because we already have a record for %s", $backup->download_url));
            }
        }
    }
    return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_copia_info_Pros_Clients.php

$job_strings[] = 'JOB_copia_info_Pros_Clients';
array_push($job_strings, 'JOB_copia_info_Pros_Clients');


//Actualiza el estado de bloqueo si l cliente tiene limite de credito excedido
function JOB_copia_info_Pros_Clients(){
    global $timedate, $db;

    $query='UPDATE accounts a
        INNER JOIN accounts_cstm ac ON a.id=ac.id_c
        INNER JOIN prospects_accounts_1_c pa ON pa.prospects_accounts_1accounts_idb=a.id
        INNER JOIN prospects p ON p.id=pa.prospects_accounts_1prospects_ida
        INNER JOIN prospects_cstm pc ON pc.id_c=p.id
        SET ac.primary_address_postalcode_c=p.primary_address_postalcode,
        ac.primary_address_street_c=p.primary_address_street,
        ac.primary_address_numero_c=pc.primary_address_number_c,
        ac.primary_address_city_c=p.primary_address_city,
        ac.primary_address_state_c=p.primary_address_state,
        ac.primary_address_colonia_c=pc.primary_address_colonia_c,
        ac.primary_address_country_c=p.primary_address_country,
        a.phone_office=p.phone_work
        WHERE ac.id_pos_c IS NOT NULL 
        AND ac.primary_address_postalcode_c = " "
        AND ac.primary_address_postalcode_c IS NOT NULL
        AND p.primary_address_postalcode IS NOT NULL 
        AND p.primary_address_postalcode != " "';

    $result01 = $db->query($query);

    $query2='UPDATE accounts a
        INNER JOIN accounts_cstm ac ON a.id=ac.id_c
        INNER JOIN prospects_accounts_1_c pa ON pa.prospects_accounts_1accounts_idb=a.id
        INNER JOIN prospects p ON p.id=pa.prospects_accounts_1prospects_ida
        INNER JOIN prospects_cstm pc ON pc.id_c=p.id
        SET ac.primary_address_postalcode_c=p.primary_address_postalcode,
        ac.primary_address_street_c=p.primary_address_street,
        ac.primary_address_numero_c=pc.primary_address_number_c,
        ac.primary_address_city_c=p.primary_address_city,
        ac.primary_address_state_c=p.primary_address_state,
        ac.primary_address_colonia_c=pc.primary_address_colonia_c,
        ac.primary_address_country_c=p.primary_address_country,
        a.phone_office=p.phone_work
        WHERE ac.id_pos_c IS NOT NULL 
        AND ac.primary_address_postalcode_c IS NULL 
        AND p.primary_address_postalcode IS NOT NULL  
        AND p.primary_address_postalcode != " "';

    $result02 = $db->query($query2);

    return true;
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_envio_ctas_middleware.php


$job_strings[] = 'envioCtasMiddlewareJOB';

array_push($job_strings, 'envioCtasMiddlewareJOB');
function envioCtasMiddlewareJOB(){
    global $current_user, $timedate, $app_list_strings;
    global $estados, $tipo_telefono, $paises, $url;
    $estados = array('AGU' => 'AGU','BCN' => 'BCN','BCS' => 'BCS','CAM' => 'CAM','CHP' => 'CHP','CHH' => 'CHH','COA' => 'COA','COL' => 'COL','DIF' => 'DIF','CMX' => 'DIF','DUR' => 'DUR','GUA' => 'GUA','GRO' => 'GRO','HID' => 'HID','JAL' => 'JAL','MEX' => 'MEX','MIC' => 'MIC','MOR' => 'MOR','NAY' => 'NAY','NLE' => 'NLE','OAX' => 'OAX','PUE' => 'PUE','QUE' => 'QUE','ROO' => 'ROO','SLP' => 'SLP','SIN' => 'SIN','SON' => 'SON','TAB' => 'TAB','TAM' => 'TAM','TLA' => 'TLA','VER' => 'VER','YUC' => 'YUC','ZAC' => 'ZAC');
    $tipo_telefono = array('' => '-1','0' => '0','1' => '1','2' => '2','3' => '3','4' => '4','5' => '5');
    $estados = array('AGU' => 'AGU','BCN' => 'BCN','BCS' => 'BCS','CAM' => 'CAM','CHP' => 'CHP','CHH' => 'CHH','COA' => 'COA','COL' => 'COL','DIF' => 'DIF','CMX' => 'DIF','DUR' => 'DUR','GUA' => 'GUA','GRO' => 'GRO','HID' => 'HID','JAL' => 'JAL','MEX' => 'MEX','MIC' => 'MIC','MOR' => 'MOR','NAY' => 'NAY','NLE' => 'NLE','OAX' => 'OAX','PUE' => 'PUE','QUE' => 'QUE','ROO' => 'ROO','SLP' => 'SLP','SIN' => 'SIN','SON' => 'SON','TAB' => 'TAB','TAM' => 'TAM','TLA' => 'TLA','VER' => 'VER','YUC' => 'YUC','ZAC' => 'ZAC');
    $tipo_telefono = array('' => '-1','0' => '0','1' => '1','2' => '2','3' => '3','4' => '4','5' => '5');

    $GLOBALS['log']->fatal('Entra a JOB envio de ctas al middleware');
    $sucursales = $app_list_strings['sucursal_c_list'];
    foreach ($sucursales as $suc => $val) {
        if($suc != '0000'){
            $ctas1 = generaCtasxSuc($suc);
            if(count($ctas1)>0){
                $data1 = json_encode($ctas1);
                $respMW = llamaMiddleware($suc, $data1);
                marcaCtasProcesadas($ctas1);
                $vresp = json_decode($respMW);
                // $GLOBALS['log']->fatal($respMW);
                $var1 = $vresp->code;
                if($var1 == '200'){
                    marcaCtasEsperando_IDpos($ctas1);
                }
            }
        }
    }
    return true;
}

function generaCtasxSuc($sucursal){
    global $current_user,$estados, $tipo_telefono;
    $Ctas = new SugarQuery();
    $Ctas->select();
    $beanCuentas = BeanFactory::newBean('Accounts');
    $Ctas->from($beanCuentas, array('team_security' => false));
    $Ctas->where()
         ->equals('deleted',0)
         ->equals('estado_interfaz_pos_c','nuevo')
         ->equals('id_sucursal_c',$sucursal);
    $resCuentas = $Ctas->execute();
    $totCtas = array();

    foreach ($resCuentas as $r) {
        $sea = new SugarEmailAddress;
        $Acc = new Account();
        $Acc->retrieve($r['id']);
        $mail = $sea->getPrimaryAddress($Acc);
        $Con = new Contact();
        $Con->retrieve($r['contact_id_c']);
        $cta['id_sugar'] = $r['id'];
        $cta['ID_CT'] = $r['id_pos_c'];
        $cta['NM_CT'] = $r['name'];
        $cta['ID_LN_ANTC'] = $r['id_linea_anticipo_c'];
        $cta['ID_AR_CRD'] = $r['id_ar_credit_c'];
        $cta['EM_ADS'] = $mail;
        $cta['A1_CNCT'] = $r['primary_address_street_c'] . ", " .$r['primary_address_numero_c'];
        $cta['A2_CNCT'] = $r['primary_address_colonia_c'];
        $cta['CI_CNCT'] = $r['primary_address_city_c'];
        $cta['PC_CNCT'] = $r['primary_address_postalcode_c'];
        $cta['ST_CNCT'] = $estados[$r['primary_address_state_c']]; // Array $estados
        $cta['CO_CNCT'] = $r['primary_address_country_c'];
        $cta['TL_CNCT'] = $r['phone_office'];
        $cta['TY_PHN'] = $tipo_telefono[$r['tipo_telefono_principal_c']]; // Array $tipo_telefono
        $cta['ID_TAX'] = $r['rfc_c'];
        $cta['LU_CNCT_SLN'] = $Con->salutation;
        $nombre_rsocial = [];
        $nombre_rsocial = separa_razon_social($r['name']);
        $cta['FN_CNCT'] = $nombre_rsocial[0];
        $cta['LN_CNCT'] = $nombre_rsocial[1];
        // $cta['FN_CNCT'] = $Con->first_name;
        // $cta['LN_CNCT'] = $Con->last_name;
        $cta['DC_CNCT'] = $Con->birthdate;
        $cta['GNDR_CNCT'] = $Con->genre_c;
        $cta['ID_PRCGP'] = $Acc->tipo_cliente_c ? $Acc->tipo_cliente_c : "1";

        if($r['id']){
            array_push($totCtas,$cta);
        }
    }
    return $totCtas;
}

function separa_razon_social($rsocial){
    $result = [];
    $long = strlen($rsocial);
    if($long > 30){
        $result[0] = substr($rsocial, 0, 29);
        $result[1] = substr($rsocial, 29, $long);
    }
    else{
        $result[0] = $rsocial;
        $result[1] = ".";
    }
    return $result;
}

function llamaMiddleware($suc, $data){
    global $current_user;
    $dom = '';
    $configurator = new Configurator();
    $configurator->loadConfig();
    $urlApi = $configurator->config['host_name'] === 'lowes.sugarondemand.com' ? 'http://sugarconecta.lowes.com.mx/' : 'http://json.lowes.com.mx/';
    $urlApi .= 'convert2json/convert2json/cliente_credito_ar_'.$suc.'_'.date('dmY_Gis');
    $GLOBALS['log']->fatal('--- url: '.$urlApi); //para ver si esta bien la url
    $ch = curl_init($urlApi);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data))
    );
    $result = curl_exec($ch);

    return $result;
}

function marcaCtasProcesadas($aprocesar){
    global $current_user, $timedate;
    if($aprocesar){
        foreach($aprocesar as $cta){
            $Acc = BeanFactory::getBean('Accounts', $cta['id_sugar']);
            if($Acc->id){
              $Acc->estado_interfaz_pos_c = 'procesando';
              $Acc->save(false);
            }
        }
    }
}

function validaRespuestaMiddleware($jsonResp){
    global $current_user, $timedate;
    $rmw = json_decode($jsonResp,true);
    $resp = $rmw[0]['code'];
    if($resp == '200'){
        return true;
    }
    return false;
}

function marcaCtasEsperando_IDpos($aprocesar){
    global $current_user, $timedate;
    if($aprocesar){
        foreach($aprocesar as $cta){
            $Acc = BeanFactory::getBean('Accounts', $cta['id_sugar']);
            if($Acc->id){
              $Acc->estado_interfaz_pos_c = 'esperandoidpos';
              $Acc->save();
            }
        }
    }
}

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_envio_nc_middleware.php


$job_strings[] = 'envioncMiddlewareJOB';

array_push($job_strings, 'envioncMiddlewareJOB');
function envioncMiddlewareJOB(){
    global $current_user, $timedate, $app_list_strings;
    global $estados, $tipo_telefono, $paises, $url;
    global $urlws;

    $urlws = 'http://json.lowes.com.mx/convert2json/convert2json/';
   

    // $GLOBALS['log']->fatal('Entra a JOB envio de Nc al middleware');
   
    $nc1 = generaNotaCredito();
    if(count($nc1)>0){
        $data1 = json_encode($nc1);
        $respMW = llamaMiddlewareNc($data1);
        //$respMW = llamaMiddleware($suc, $data1);
        return true;
    }
    return true;
}

function generaNotaCredito(){
    global $current_user,$estados, $tipo_telefono;

    // creacion de la fecha 
    $anio=date("Y");
    $mes=date("m");
    $dia=date("d")-1;
         
    if (strlen($dia) === 1) {
        $dia = '0'.$dia;
    }

    $fecha=$anio.'-'.$mes.'-'.$dia;
    
    $Nc = new SugarQuery();
    $Nc->select();
    $beanNc = BeanFactory::newBean('NC_Notas_credito');
    $Nc->from($beanNc, array('team_security' => false));
    $Nc->where()->contains('fecha_emision_c',$fecha);
    $resNC = $Nc->execute();
    $GLOBALS['log']->fatal($resNC);
    $totNc = array();

    foreach ($resNC as $r) {
        $NC['id_sugar'] = $r['id'];
        $NC['NT_NC'] = $r['no_ticket_c'];
        $NC['UUID_NC'] = $r['uuid_c'];
        $NC['FOLIO_NC'] = $r['name'];
        $NC['SERIE_NC'] = $r['series_id_c'];
        $NC['IMP_NC'] = $r['importe_total'];
        $NC['ARC_NC'] = $r['importadode_c'];
        $NC['FECHA_NC'] = $r['fecha_emision_c'];
        if($r['id']){
            array_push($totNc,$NC);
        }
    }
    return $totNc;
}


function llamaMiddlewareNc($data){
    global $current_user, $urlws;
    $dom = '';
    $configurator = new Configurator();
    $configurator->loadConfig();
    $urlApi = $configurator->config['host_name'] === 'lowes.sugarondemand.com' ? 'http://sugarconecta.lowes.com.mx/' : 'http://json.lowes.com.mx/';
    $urlApi .= 'convert2json/convert2json/Nc_sugar_'.date('Ymd');
   
    $ch = curl_init($urlApi);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data))
    );
    $result = curl_exec($ch);

    return $result;
}


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/JOB_envio_Facturas_middleware.php


$job_strings[] = 'envioFacturasMiddlewareJOB';

array_push($job_strings, 'envioFacturasMiddlewareJOB');
function envioFacturasMiddlewareJOB(){
    global $current_user, $timedate, $app_list_strings;
    global $estados, $tipo_telefono, $paises, $url;
    global $urlws;

    $urlws = 'http://json.lowes.com.mx/convert2json/convert2json/';
   

    // $GLOBALS['log']->fatal('Entra a JOB envio de Fact al middleware');
   
    $Fact1 = generaFacturas();
    if(count($Fact1)>0){
        $data1 = json_encode($Fact1);
        $respMW = llamaMiddlewareFac($data1);
        //$respMW = llamaMiddleware($suc, $data1);
        return true;
    }
    return true;
}

function generaFacturas(){
    global $current_user,$estados, $tipo_telefono;

    // creacion de la fecha 
    $anio=date("Y");
    $mes=date("m");
    $dia=date("d")-1;
         
    if (strlen($dia) === 1) {
        $dia = '0'.$dia;
    }

    $fecha=$anio.'-'.$mes.'-'.$dia;
    
    $Fact = new SugarQuery();
    $Fact->select();
    $beanFact = BeanFactory::newBean('LF_Facturas');
    $Fact->from($beanFact, array('team_security' => false));
    $Fact->where()->contains('dt_crt',$fecha);
    $resFacturas=$Fact->execute();
    $GLOBALS['log']->fatal($resFacturas);
    $totFact = array();

    foreach ($resFacturas as $r) {
        $fac['id_sugar'] = $r['id'];
        $fac['NT_CDT'] = $r['no_ticket'];
        $fac['UUID_FAC'] = $r['uuid'];
        $fac['NAME_FAC'] = $r['name'];
        $fac['SERIE_FAC'] = $r['serie'];
        $fac['IMP_FAC'] = $r['importe'];
        $fac['ARC_FAC'] = $r['importadode_c'];
        $fac['FECHA_FAC'] = $r['dt_crt'];
        if($r['id']){
            array_push($totFact,$fac);
        }
    }
    return $totFact;
}


function llamaMiddlewareFac($data){
    global $current_user, $urlws;
    $dom = '';
    $configurator = new Configurator();
    $configurator->loadConfig();
    $urlApi = $configurator->config['host_name'] === 'lowes.sugarondemand.com' ? 'http://sugarconecta.lowes.com.mx/' : 'http://json.lowes.com.mx/';
    $urlApi .= 'convert2json/convert2json/fact_sugar_'.date('Ymd');
    $ch = curl_init($urlApi);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data))
    );
    $result = curl_exec($ch);

    return $result;
}


?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/ScheduledTasks/job_clasifica_cliente_cartera_vencida.php


$job_strings[] = 'job_clasifica_cliente_cartera_vencida';

array_push($job_strings, 'job_clasifica_cliente_cartera_vencida');


/*
NOTA:   Hay que tomar en cuenta que para ejecutar este proceso es necesario
        primero ejecutar los job de calculo de cartera vencida
*/
function job_clasifica_cliente_cartera_vencida() {
    //HSLR
    _ppl('dentro de job_cartera vencida');
    $isDebug = false;
    global $db;
  

    $sqlQuery = "SELECT MAX(fc.dias_vencidos_sin_tol_c) dias_vencimiento, a.id, a.name, ac.estado_bloqueo_c, ac.codigo_bloqueo_c
        FROM accounts a
        INNER JOIN accounts_cstm ac ON ac.id_c=a.id
        INNER JOIN accounts_lf_facturas_1_c fa ON fa.accounts_lf_facturas_1accounts_ida=a.id AND fa.deleted <>1
        INNER JOIN lf_facturas f ON f.id=fa.accounts_lf_facturas_1lf_facturas_idb
        INNER JOIN lf_facturas_cstm fc ON f.id=fc.id_c
        WHERE
         f.estado_tiempo NOT IN ('pagada', '', 'devuelta_parcial', 'devuelta_completa') 
         AND f.deleted <> 1 AND a.deleted <> 1 
         AND fc.linea_anticipo_c = 0 
         AND f.tipo_documento='factura' 
         AND f.dt_crt IS NOT NULL
         AND ac.codigo_bloqueo_c NOT IN ('CV','CL', 'Otro','DI')
        GROUP BY id, name;";

    //HSLR
    $GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida::sql:'. $sqlQuery);

    //Primero ejecutamos estea función que habilita las carteras que estaban vencidas pero ya pagaron
    //fnHabilitaCarteraCuentasPagadas();

    $res = $db->query($sqlQuery);
    $num_rows = $res->num_rows;

    $GLOBALS['log']->fatal(' --------------- '. $num_rows);

    //if($isDebug)$GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida::num_rows:'. $num_rows);
    if($num_rows) {
        $GLOBALS['log']->fatal(' -------ENTRO AL IF --------');
        while($row = $db->fetchByAssoc($res)){
            //if($isDebug)$GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida factura::'. $row['name']);
            //if($isDebug)$GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida dias_vencimiento::'. $row['dias_vencimiento']);

            $GLOBALS['log']->fatal(' -----dentro del while --- ');

            $beanAccount = BeanFactory::getBean('Accounts', $row['id']);
            $estadoCarteraPrev = $beanAccount->estado_cartera_c;
            if(intval($row['dias_vencimiento']) > 0){
              if(intval($row['dias_vencimiento']) > 0 && intval($row['dias_vencimiento']) <= 30){
                $beanAccount->estado_cartera_c = 'CV130D';
              }
              else if(intval($row['dias_vencimiento']) > 30 && intval($row['dias_vencimiento']) <= 120){
                $beanAccount->estado_cartera_c = 'CV31120D';
              }
              else if(intval($row['dias_vencimiento']) > 120){
                $beanAccount->estado_cartera_c = 'CV120D';
              }

              if(
                $beanAccount->estado_cartera_c != 'CV'
              ){
                $GLOBALS['log']->fatal('---------- CLIENTES: ------'.$beanAccount->name);
                $beanAccount->estado_bloqueo_c = 'Bloqueado';
                if (empty($beanAccount->codigo_bloqueo_c) || $beanAccount->codigo_bloqueo_c === "LE") {
                  $beanAccount->codigo_bloqueo_c = 'CV';
                }
                $beanAccount->noBloqueante = false;
              }
              //if($isDebug)$GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida account:estado_cartera_c::'. $beanAccount->estado_cartera_c );
              $beanAccount->save(false);
              //if($isDebug)$GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida account:estado_cartera_c:after:'. $beanAccount->estado_cartera_c );
            }
        }
    }

    return true;
}

?>
