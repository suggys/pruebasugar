<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_envio_ctas_middleware.php
 
$mod_strings['LBL_ENVIOCTASMIDDLEWAREJOB'] = 'Envío de cuentas al MiddleWare'; 

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_clasifica_cliente_cartera_vencida.php

$mod_strings['LBL_JOB_CLASIFICA_CLIENTE_CARTERA_VENCIDA'] = 'JOB para Clasificar el cliente por cartera vencida FR-30.2';

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_calculo_facturas_vencidas.php

$mod_strings['LBL_JOB_CALCULO_FACTURAS_VENCIDAS'] = 'JOB para calcula facturas vencidas FR-30.1';

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.JOB_clasifica_facturasVigencia.php

$mod_strings['LBL_JOB_CLASIFICA_FACTURAS_VIGENCIA'] = 'JOB para calculo vigencia de facturas para reportes.';

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_importa_notasDeCredito_JSON.php
 
$mod_strings['LBL_IMPORTANOTASDECREDITOJSON'] = 'Importar Notas de Crédito desde una Base de Datos Intermedia';
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_importa_facturas_ftp.php
 
$mod_strings['LBL_IMPORTAFACTURASFTP'] = 'Importa facturas de FTP'; 
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.JOB_envio_ctas_middleware.php
 
$mod_strings['LBL_ENVIOCTASMIDDLEWAREJOB'] = 'Envío de cuentas al MiddleWare'; 

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_clasifica_facturas_segun_tiempo.php

$mod_strings['LBL_JOB_CLASIFICA_FACTURAS_SEGUN_TIEMPO'] = 'JOB para clasificar facturas  según el tiempo FR-30.5.';

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_importa_pagos_JSON.php
 
$mod_strings['LBL_IMPORTAPAGOSJSON'] = 'Importar Pagos desde CASH EBS'; 
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.JOB_clasifica_autorizacion_esp.php

$mod_strings['LBL_JOB_CLASIFICA_AUTORIZACION_ESP'] = 'JOB para calculo vigencia de aut. especiales para reportes.';

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_importa_facturas_JSON.php
 
$mod_strings['LBL_IMPORTAFACTURASJSON'] = 'Importar Facturas desde una Base de Datos Intermedia'; 
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_low_bloqueo_automatico.php

$mod_strings['LBL_JOB_LOW_BLOQUEO_AUTOMATICO'] = 'Job para bloqueo automatico';

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_incumplimiento_promesa_pago.php

$mod_strings['LBL_JOB_INCUMPLIMIENTO_PROMESA_PAGO'] = 'Incumplimiento de la Promesa de Pago FR-19.8';

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.JOB_importa_partidas_JSON.php

$mod_strings['LBL_JOB_IMPORTA_PARTIDAS_JSON'] = 'Importa Partidas';
 
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_importa_ordenes_JSON.php

$mod_strings['LBL_IMPORTAORDENESJSON'] = 'Importar Ordenes desde SFTP'; 

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_actualiza_check_sin_orden_xdias_c.php

$mod_strings['LBL_JOB_ACTUALIZA_CHECK_SIN_ORDEN_XDIAS_C'] = 'Actualiza cuentas sin ordenes 120 días'; 
?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.job_actualiza_estado_autorizacion_especial.php

$mod_strings['LBL_JOB_ACTUALIZA_ESTADO_AUTORIZACION_ESPECIAL'] = 'Job Actualiza estado de Autorizacion Especial';

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.JOB_envia_pagos_konesh.php

$mod_strings['LBL_JOB_ENVIA_PAGOS_KONESH'] = 'Envia pagos Konesh'; 

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.JOB_copia_info_Pros_Clients.php


$mod_strings['LBL_JOB_COPIA_INFO_PROS_CLIENTS'] = 'Copia informacion de prospecto a cliente';

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.JOB_envio_nc_middleware.php
 
$mod_strings['LBL_ENVIONCMIDDLEWAREJOB'] = 'Envío de NC al MiddleWare'; 

?>
<?php
// Merged from custom/Extension/modules/Schedulers/Ext/Language/es_LA.JOB_envio_Facturas_middleware.php
 
$mod_strings['LBL_ENVIOFACTURASMIDDLEWAREJOB'] = 'Envío de FACTURAS al MiddleWare'; 

?>
