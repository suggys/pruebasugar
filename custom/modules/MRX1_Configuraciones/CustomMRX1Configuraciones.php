<?php
/**
 *
 */
class CustomMRX1Configuraciones
{
  private $moduleName = "MRX1_Configuraciones";
  public $settings = [];

  public function retrieveSettings()
  {
    $this->settings = [];
    $bean = BeanFactory::newBean($this->moduleName);
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $sugarQuery->select(array("name","valor"));
    $results = $sugarQuery->execute();
    foreach ($results as $record) {
      $this->settings[$record['name']] = $record['valor'];
    }
  }

  public function saveSetting($name, $value)
  {
    $config = BeanFactory::newBean($this->moduleName);
    $config->retrieve_by_string_fields( array( 'name' => $name ) );
    if(empty($config->id)){
      $config->name = $name;
    }
    $config->valor = $value;
    $config->save(false);
  }
}

 ?>
