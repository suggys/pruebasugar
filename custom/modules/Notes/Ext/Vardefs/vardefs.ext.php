<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/rli_link_workflow.php

$dictionary['Note']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Note']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/low01_solicitudescredito_activities_notes_Notes.php

// created: 2017-07-21 12:47:08
$dictionary["Note"]["fields"]["low01_solicitudescredito_activities_notes"] = array (
  'name' => 'low01_solicitudescredito_activities_notes',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_notes',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_NOTES_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Notes/Ext/Vardefs/NotesVisibility.php

$dictionary['Note']['visibility']["NotesVisibility"] = true;

?>
