<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Low01_Pagos_Facturas/Ext/Vardefs/low01_pagos_facturas_lf_facturas_Low01_Pagos_Facturas.php

// created: 2017-04-03 22:41:21
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lf_facturas"] = array (
  'name' => 'low01_pagos_facturas_lf_facturas',
  'type' => 'link',
  'relationship' => 'low01_pagos_facturas_lf_facturas',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'side' => 'right',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LOW01_PAGOS_FACTURAS_TITLE',
  'id_name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
  'link-type' => 'one',
);
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lf_facturas_name"] = array (
  'name' => 'low01_pagos_facturas_lf_facturas_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE',
  'save' => true,
  'id_name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
  'link' => 'low01_pagos_facturas_lf_facturas',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'name',
);
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lf_facturaslf_facturas_ida"] = array (
  'name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LOW01_PAGOS_FACTURAS_TITLE_ID',
  'id_name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
  'link' => 'low01_pagos_facturas_lf_facturas',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Low01_Pagos_Facturas/Ext/Vardefs/low01_pagos_facturas_lowes_pagos_Low01_Pagos_Facturas.php

// created: 2017-04-03 22:41:21
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lowes_pagos"] = array (
  'name' => 'low01_pagos_facturas_lowes_pagos',
  'type' => 'link',
  'relationship' => 'low01_pagos_facturas_lowes_pagos',
  'source' => 'non-db',
  'module' => 'lowes_Pagos',
  'bean_name' => 'lowes_Pagos',
  'side' => 'right',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOW01_PAGOS_FACTURAS_TITLE',
  'id_name' => 'low01_pagos_facturas_lowes_pagoslowes_pagos_ida',
  'link-type' => 'one',
);
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lowes_pagos_name"] = array (
  'name' => 'low01_pagos_facturas_lowes_pagos_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOWES_PAGOS_TITLE',
  'save' => true,
  'id_name' => 'low01_pagos_facturas_lowes_pagoslowes_pagos_ida',
  'link' => 'low01_pagos_facturas_lowes_pagos',
  'table' => 'lowes_pagos',
  'module' => 'lowes_Pagos',
  'rname' => 'name',
);
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lowes_pagoslowes_pagos_ida"] = array (
  'name' => 'low01_pagos_facturas_lowes_pagoslowes_pagos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOW01_PAGOS_FACTURAS_TITLE_ID',
  'id_name' => 'low01_pagos_facturas_lowes_pagoslowes_pagos_ida',
  'link' => 'low01_pagos_facturas_lowes_pagos',
  'table' => 'lowes_pagos',
  'module' => 'lowes_Pagos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Low01_Pagos_Facturas/Ext/Vardefs/sugarfield_num_pago_c.php

 // created: 2018-10-04 03:41:05
$dictionary['Low01_Pagos_Facturas']['fields']['num_pago_c']['labelValue']='Número de Pago';
$dictionary['Low01_Pagos_Facturas']['fields']['num_pago_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Low01_Pagos_Facturas']['fields']['num_pago_c']['enforced']='';
$dictionary['Low01_Pagos_Facturas']['fields']['num_pago_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Low01_Pagos_Facturas/Ext/Vardefs/sugarfield_saldo_insoluto_c.php

 // created: 2018-10-04 03:42:13
$dictionary['Low01_Pagos_Facturas']['fields']['saldo_insoluto_c']['labelValue']='Saldo Insoluto';
$dictionary['Low01_Pagos_Facturas']['fields']['saldo_insoluto_c']['enforced']='';
$dictionary['Low01_Pagos_Facturas']['fields']['saldo_insoluto_c']['dependency']='';
$dictionary['Low01_Pagos_Facturas']['fields']['saldo_insoluto_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
