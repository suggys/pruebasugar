<?php
class GrupoEmpresasLogicHooksMananger{
	public function beforeSave($bean, $event, $arguments){
		if(isset($bean->importado_c) && $bean->importado_c){
            return true;
        }
		if($bean->fetched_row['credito_diponible_grupo_c'] != $bean->credito_diponible_grupo_c){
			$bean->credito_disponible_flexible_c = $bean->credito_diponible_grupo_c + ($bean->limite_credito_grupo_c * 0.1);
		}
	}
}
