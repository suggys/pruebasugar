<?php
$popupMeta = array (
    'moduleMain' => 'GPE_Grupo_empresas',
    'varName' => 'GPE_Grupo_empresas',
    'orderBy' => 'gpe_grupo_empresas.name',
    'whereClauses' => array (
  'name' => 'gpe_grupo_empresas.name',
  'administrar_credito_grupal' => 'gpe_grupo_empresas.administrar_credito_grupal',
  'estado' => 'gpe_grupo_empresas.estado',
  'assigned_user_name' => 'gpe_grupo_empresas.assigned_user_name',
  'date_modified' => 'gpe_grupo_empresas.date_modified',
  'date_entered' => 'gpe_grupo_empresas.date_entered',
  'favorites_only' => 'gpe_grupo_empresas.favorites_only',
  'importado_c' => 'gpe_grupo_empresas_cstm.importado_c',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'administrar_credito_grupal',
  5 => 'estado',
  6 => 'assigned_user_name',
  7 => 'date_modified',
  8 => 'date_entered',
  9 => 'favorites_only',
  10 => 'importado_c',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'estado' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_ESTADO',
    'width' => '10%',
    'name' => 'estado',
  ),
  'administrar_credito_grupal' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_ADMINISTRAR_CREDITO_GRUPAL',
    'width' => '10%',
    'name' => 'administrar_credito_grupal',
  ),
  'importado_c' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_IMPORTADO',
    'width' => '10%',
    'name' => 'importado_c',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'name' => 'assigned_user_name',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'name' => 'date_modified',
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'ESTADO' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_ESTADO',
    'width' => '10%',
    'name' => 'estado',
  ),
  'IMPORTADO_C' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_IMPORTADO',
    'width' => '10%',
  ),
  'ADMINISTRAR_CREDITO_GRUPAL' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_ADMINISTRAR_CREDITO_GRUPAL',
    'width' => '10%',
    'name' => 'administrar_credito_grupal',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_entered',
  ),
),
);
