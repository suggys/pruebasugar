<?php
class Updt_InactivaCtasRelClass{
	public function update_ctas_rel_inactivar($bean, $event, $arguments){
		if(isset($bean->importado_c) && $bean->importado_c){
            return true;
        }
		global $timedate, $current_user;
		if($event === "after_save" && $bean->estado === "Inactivo"){
			if($bean->load_relationship('gpe_grupo_empresas_accounts')){
				$cuentas = $bean->gpe_grupo_empresas_accounts->getBeans();
				foreach($cuentas as $cuenta) {
					if(!isset($cuenta->importado_c) || !$cuenta->importado_c){
						$cuenta->estado_cuenta_c = 'Inactivo';
						$cuenta->save();
					}
				}
			}
		}
	}
}