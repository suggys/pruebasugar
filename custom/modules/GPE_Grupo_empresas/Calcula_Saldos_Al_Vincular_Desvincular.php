<?php

/*
*E1
Dato: Un Grupo de Empresas con Nombre: [FR-13.2.- E1 Grupo de Empresa]
Límite de Crédito de Grupo : [$300,000.00 (MXN)]
Crédito Disponible de Grupo : [$100,000.00 (MXN)]
Saldo Deudor : [$ 200,000.00(MXN)]
Sumar / Administrar Límite de Crédito por Grupo : [true]
Estado: [Activo]
con dos cuenta viculada la primera:
nombre : [FR-13.2.- E1 Cuenta 1]
Límite de crédito autorizado:  [$150,000.00 (MXN)]
Crédito disponible: [$50,000.00 (MXN)]
Saldo deudor: [$ 100,000.00(MXN)]
la segunda de
nombre : [FR-13.2.- E1 Cuenta 2]
Límite de crédito autorizado:  [$150,000.00 (MXN)]
Crédito disponible: [$50,000.00 (MXN)]
Saldo deudor: [$ 100,000.00(MXN)]

Cuando Se desvicule la cuenta de nombre:
: [FR-13.2.- E1 Cuenta 2]

Entonces: El registro de Grupo de Empresas con Nombre  [FR-13.2.- E1 Grupo de Empresa] debe tener  los siguientes registros
Límite de Crédito de Grupo : [$150,000.00 (MXN)]
Crédito Disponible de Grupo : [$50,000.00 (MXN)]
Saldo Deudor : [$ 100,000.00(MXN)]

*/
class Calcula_Saldos_Al_Vincular_Desvincular{

    public function fnCalculaSaldoAlVincular($bean, $event, $arguments){
        if(isset($bean->importado_c) && $bean->importado_c){
            return true;
        }
        if(
          $this->fnValidaAplicaCalculo($bean)
          && $arguments['related_module'] === 'Accounts'
        ) {
          // $GLOBALS["log"]->fatal("Entra fnCalculaSaldoAlVincular");
          $this->fnCalculaActualizaSaldos($bean);
        }
    }

    public function fnCalculaSaldoAlDesvincular($bean, $event, $arguments) {
        if(isset($bean->importado_c) && $bean->importado_c){
            return true;
        }
        if($arguments['related_module']==='Accounts') {
          $account = BeanFactory::getBean("Accounts", $arguments['related_id']);
          if($account->id){
            $this->fnCalculaActualizaSaldos($bean, $account, false);
          }
        }
    }

    public function fnValidaAplicaCalculo($bean) {
        if(isset($bean->importado_c) && $bean->importado_c){
            return true;
        }
        return $bean->estado === 'Activo';
    }

    public function fnCalculaActualizaSaldos($bean) {
        if(isset($bean->importado_c) && $bean->importado_c){
            return true;
        }
        $bean->limite_credito_grupo_c = 0;
        $bean->credito_diponible_grupo_c = 0;
        $bean->saldo_deudor_c = 0;
        $bean->saldo_pendiente_aplicar_c = 0;
        $accounts = $this->getAccounts($bean->id);
        foreach ($accounts as $account) {
          $bean->limite_credito_grupo_c += $account['limite_credito_autorizado_c'];
          $bean->credito_diponible_grupo_c += $account['credito_disponible_c'];
          $bean->saldo_deudor_c += $account['saldo_deudor_c'];
          $bean->saldo_pendiente_aplicar_c += $account['saldo_pendiente_aplicar_c'];
        }
        $bean->save(false);
    }

    public function getAccounts($id)
    {
      $bean = BeanFactory::newBean('Accounts');
      $sugarQuery = new SugarQuery();
      $sugarQuery->from($bean, array('team_security'=>false));
      $sugarQuery->join('gpe_grupo_empresas_accounts', array('team_security'=>false, 'alias' => 'grupoEmpresas'));
      $sugarQuery->where()
        ->equals("grupoEmpresas.id", $id);
      $sugarQuery->select(array('credito_disponible_c', 'saldo_deudor_c', 'limite_credito_autorizado_c', 'saldo_pendiente_aplicar_c'));
      $accounts = $result = $sugarQuery->execute();
      return $accounts;
    }
}

?>
