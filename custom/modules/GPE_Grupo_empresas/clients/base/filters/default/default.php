<?php
// created: 2016-11-29 22:45:09
$viewdefs['GPE_Grupo_empresas']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'administrar_credito_grupal' => 
    array (
    ),
    'estado' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);