({
    extendsFrom: 'SubpanelsLayout',

    initialize: function(options) {
        this._super('initialize', arguments);
    },
    showSubpanel: function(linkName) {
        var self = this;
        _.each(self._components, function(component) {
            var link = component.context.get('link');
            if (self._hideButtons(link)) {
                component.$el.find('.subpanel-controls').hide();
            }
            if (link === 'gpe_grupo_empresas_accounts') {
                var filter = [{
                    id: self.model.get('id')
                }];
                var url = app.api.buildURL('GPE_Grupo_empresas', 'read', {}, {
                    filter: filter
                });
                app.api.call('read', url, {}, {
                    success: function(data) {
                        if (data.records[0].estado.toLowerCase() === 'inactivo') {
                            component.$el.find('a[class="rowaction btn"]').hide(); // Esconde el boton de crear
                            component.$el.find('a[class="btn dropdown-toggle"]').hide(); // Esconde el link de vincular.
                        } else {
                            component.$el.find('a[class="rowaction btn"]').show();
                            component.$el.find('a[class="btn dropdown-toggle"]').show();
                        }
                    }
                });
            }
        });


    },
    _hideButtons: function(link) {
        var self = this;
        var response = false;
        if (app.user.get('type') != 'admin') {
            if (link === 'gpe_grupo_empresas_accounts' && !_.filter(app.user.get('roles'), function(rol) {
                    return rol.toLowerCase() == "administrador de credito";
                }).length) {
                response = true;
            }
        }
        return response;
    },
})
