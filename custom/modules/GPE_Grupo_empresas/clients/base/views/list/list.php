<?php
$module_name = 'GPE_Grupo_empresas';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 =>
              array (
                'name' => 'administrar_credito_grupal',
                'label' => 'LBL_ADMINISTRAR_CREDITO_GRUPAL',
                'enabled' => true,
                'default' => true,
              ),
              2 =>
              array (
                'name' => 'estado',
                'label' => 'LBL_ESTADO',
                'enabled' => true,
                'default' => true,
              ),
              3 =>
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              4 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              5 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'limite_credito_grupo_c',
                'label' => 'LBL_LIMITE_CREDITO_GRUPO_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              7 =>
              array (
                'name' => 'credito_diponible_grupo_c',
                'label' => 'LBL_CREDITO_DIPONIBLE_GRUPO_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              8 =>
              array (
                'name' => 'saldo_deudor_c',
                'label' => 'LBL_SALDO_DEUDOR_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              9 =>
              array (
                'name' => 'credito_disponible_flexible_c',
                'label' => 'LBL_CREDITO_DISPONIBLE_FLEXIBLE',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              10 =>
              array (
                'name' => 'saldo_pendiente_aplicar_c',
                'label' => 'LBL_SALDO_PENDIENTE_APLICAR_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              11 =>
              array (
                'name' => 'importado_c',
                'label' => 'LBL_IMPORTADO',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
