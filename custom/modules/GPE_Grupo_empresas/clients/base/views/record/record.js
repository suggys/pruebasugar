({
    extendsFrom: 'RecordView',

    initialize: function(options) {
        this._super('initialize', [options]);
        var self = this;
        self.fieldsDisabled = false;
        self.model.on('sync', function(model, value){
            self._disableFields(app.user.get('roles'));
        });
    },

    // Se deshabilitan los campos cuando el usuario no es Administrador de Crédito FR-16.1
	_disableFields:function(rolesArray){
        var self = this;
        if(app.user.get('type') != 'admin'){
            if( !_.filter(rolesArray, function(rol) { return rol.toLowerCase() == "administrador de credito";}).length ){
                var name = self.getField('name');
                name.setMode('readonly');
                name.setDisabled('true');
                var limiteCreditoGrupo = self.getField('limite_credito_grupo_c');
                limiteCreditoGrupo.setMode('readonly');
                limiteCreditoGrupo.setDisabled('true');
                var creditoDisponibleGrupo = self.getField('credito_diponible_grupo_c');
                creditoDisponibleGrupo.setMode('readonly');
                creditoDisponibleGrupo.setDisabled('true');
                var saldoDeudor = self.getField('saldo_deudor_c');
                saldoDeudor.setMode('readonly');
                saldoDeudor.setDisabled('true');
                var administrarCreditoGrupal = self.getField('administrar_credito_grupal');
                administrarCreditoGrupal.setDisabled('true');
                var estado = self.getField('estado');
                estado.setMode('readonly');
                estado.setDisabled('true');
                var comentarios = self.getField('description');
                comentarios.setMode('readonly');
                comentarios.setDisabled('true');
                var assigned_user_name = self.getField('assigned_user_name');
                assigned_user_name.setMode('readonly');
                assigned_user_name.setDisabled('true');
                self.fieldDisabled = true;
            }
        }
    }
})
