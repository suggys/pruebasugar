<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Vardefs/sugarfield_saldo_deudor_c.php

 // created: 2016-12-06 01:04:12
$dictionary['GPE_Grupo_empresas']['fields']['saldo_deudor_c']['audited']=true;
$dictionary['GPE_Grupo_empresas']['fields']['saldo_deudor_c']['importable']='false';
$dictionary['GPE_Grupo_empresas']['fields']['saldo_deudor_c']['duplicate_merge']='disabled';
$dictionary['GPE_Grupo_empresas']['fields']['saldo_deudor_c']['duplicate_merge_dom_value']='0';
$dictionary['GPE_Grupo_empresas']['fields']['saldo_deudor_c']['calculated']=false;
$dictionary['GPE_Grupo_empresas']['fields']['saldo_deudor_c']['default']=0;

 
?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Vardefs/sugarfield_credito_disponible_flexible_c.php

 // created: 2016-12-13 16:35:53
$dictionary['GPE_Grupo_empresas']['fields']['credito_disponible_flexible_c']['labelValue']='Crédito Disponible Flexible de Grupo';
$dictionary['GPE_Grupo_empresas']['fields']['credito_disponible_flexible_c']['enforced']='1';
$dictionary['GPE_Grupo_empresas']['fields']['credito_disponible_flexible_c']['dependency']='';
$dictionary['GPE_Grupo_empresas']['fields']['credito_disponible_flexible_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Vardefs/sugarfield_importado_c.php

 // created: 2016-12-01 03:57:37
$dictionary['GPE_Grupo_empresas']['fields']['importado_c']['labelValue']='Importado';
$dictionary['GPE_Grupo_empresas']['fields']['importado_c']['enforced']='';
$dictionary['GPE_Grupo_empresas']['fields']['importado_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-16 11:27:49
$dictionary['GPE_Grupo_empresas']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Vardefs/sugarfield_estado.php

 // created: 2016-09-14 18:29:37
$dictionary['GPE_Grupo_empresas']['fields']['estado']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Vardefs/sugarfield_limite_credito_grupo_c.php

 // created: 2016-10-06 18:21:16
$dictionary['GPE_Grupo_empresas']['fields']['limite_credito_grupo_c']['default']=0;
$dictionary['GPE_Grupo_empresas']['fields']['limite_credito_grupo_c']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Vardefs/gpe_grupo_empresas_accounts_GPE_Grupo_empresas.php

// created: 2016-09-14 15:26:16
$dictionary["GPE_Grupo_empresas"]["fields"]["gpe_grupo_empresas_accounts"] = array (
  'name' => 'gpe_grupo_empresas_accounts',
  'type' => 'link',
  'relationship' => 'gpe_grupo_empresas_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE',
  'id_name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Vardefs/sugarfield_credito_diponible_grupo_c.php

 // created: 2016-10-06 18:20:42
$dictionary['GPE_Grupo_empresas']['fields']['credito_diponible_grupo_c']['default']=0;
$dictionary['GPE_Grupo_empresas']['fields']['credito_diponible_grupo_c']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Vardefs/sugarfield_saldo_pendiente_aplicar_c.php

 // created: 2016-12-22 15:21:18
$dictionary['GPE_Grupo_empresas']['fields']['saldo_pendiente_aplicar_c']['labelValue']='Saldo pendiente aplicar';
$dictionary['GPE_Grupo_empresas']['fields']['saldo_pendiente_aplicar_c']['enforced']='';
$dictionary['GPE_Grupo_empresas']['fields']['saldo_pendiente_aplicar_c']['dependency']='';
$dictionary['GPE_Grupo_empresas']['fields']['saldo_pendiente_aplicar_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
