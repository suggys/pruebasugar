<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Layoutdefs/gpe_grupo_empresas_accounts_GPE_Grupo_empresas.php

 // created: 2016-09-14 15:26:16
$layout_defs["GPE_Grupo_empresas"]["subpanel_setup"]['gpe_grupo_empresas_accounts'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'gpe_grupo_empresas_accounts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/Layoutdefs/_overrideGPE_Grupo_empresas_subpanel_gpe_grupo_empresas_accounts.php

//auto-generated file DO NOT EDIT
$layout_defs['GPE_Grupo_empresas']['subpanel_setup']['gpe_grupo_empresas_accounts']['override_subpanel_name'] = 'GPE_Grupo_empresas_subpanel_gpe_grupo_empresas_accounts';

?>
