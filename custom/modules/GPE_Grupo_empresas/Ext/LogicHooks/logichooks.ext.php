<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/LogicHooks/Calcula_Saldos_Al_Vincular_Desvincular.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
$hook_array['after_relationship_delete'][] = array(
    1,
    'Calcular Saldo Deudor, Crédito Disponible y Límite de Crédito al desvincular cta Cliente Crédito AR a Gpo de Empresas',
    'custom/modules/GPE_Grupo_empresas/Calcula_Saldos_Al_Vincular_Desvincular.php',
    'Calcula_Saldos_Al_Vincular_Desvincular',
    'fnCalculaSaldoAlDesvincular'
);
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    1,
    'Calcular Saldo Deudor, Crédito Disponible y Límite de Crédito al vincular cta Cliente Crédito AR a Gpo de Empresas',
    'custom/modules/GPE_Grupo_empresas/Calcula_Saldos_Al_Vincular_Desvincular.php',
    'Calcula_Saldos_Al_Vincular_Desvincular',
    'fnCalculaSaldoAlVincular'
);

$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
    1,
    'Before save',
    'custom/modules/GPE_Grupo_empresas/GrupoEmpresasLogicHooksMananger.php',
    'GrupoEmpresasLogicHooksMananger',
    'beforeSave'
);

?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/LogicHooks/inactiva_ctas_kh_gpe_ctas.php

$hook_version = 1; 
$hook_array = $hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
	'1',
	'Inactiva_Empresas_hijas',
	'custom/modules/GPE_Grupo_empresas/lh_upd_inactivar_ctas_relacionadas.php',
	'Updt_InactivaCtasRelClass',
	'update_ctas_rel_inactivar'
	);

?>
<?php
// Merged from custom/Extension/modules/GPE_Grupo_empresas/Ext/LogicHooks/calcula_lim_cred_gpo_empresas.php


?>
