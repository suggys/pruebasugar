<?php
class UpdateAccountIncobrabilidad {
	public function UpdateAccountIncobrabilidad($bean, $event, $arguments){
		global $timedate, $current_user;
		if($bean->category_id == 'carta_incobrabilidad' && $arguments['related_module'] == 'Accounts'){
			$bean->load_relationship('accounts');
			$cta = $bean->accounts->getBeans();
			foreach ($cta as $cuenta) {
				$cuenta->codigo_bloqueo_c = 'DI';
				$cuenta->estado_bloqueo_c = 'Bloqueado';
				$cuenta->save();
			}
		}
	}
}

class UpdateAccountUnlink {
	public function UpdateAccountUnlink($bean, $event, $arguments){
		global $timedate, $current_user;
		if($arguments['related_module'] == 'Accounts' && $arguments['related_id']){
			$cuenta = BeanFactory::getBean('Accounts',$arguments['related_id']);
			if($cuenta){
				$cuenta->codigo_bloqueo_c = '';
				$cuenta->estado_bloqueo_c = 'Desbloqueado';
				$cuenta->save(false);
			}
		}
	}
}
?>
