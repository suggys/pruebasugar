<?php
require_once ('custom/modules/Accounts/Account_GeneraID_AR.php');

class UpdateAccountIDARCredit
{

  public function UpdateAccountIDARCredit($bean, $event, $arguments)
  {
    if($arguments['related_module'] === "LOW01_SolicitudesCredito")
    {
      $solicitud = BeanFactory::getBean('LOW01_SolicitudesCredito',$arguments['related_id']);
      if(
      !empty($solicitud->id)
      && !empty($solicitud->low01_solicitudescredito_accountsaccounts_ida)
      && (
        $solicitud->estado_solicitud_c === 'Aprobacion_por_Gerente_de_Credito'
        || $solicitud->estado_solicitud_c === 'Aprobacion_por_Director_de_Finanzas'
        || $solicitud->estado_solicitud_c === 'Aprobacion_por_Vicepresidencia_Comercial'
        )
      )
      {
        $cuenta = BeanFactory::getBean('Accounts',$solicitud->low01_solicitudescredito_accountsaccounts_ida);
        if(!empty($cuenta->id) && empty($cuenta->id_ar_credit_c))
        {
          $generador = new Account_GeneraID_AR();
          $cuenta->id_ar_credit_c = $generador->getNewIdArCredit($cuenta);
          $cuenta->document_id_c = $bean->id;
          $cuenta->limite_credito_autorizado_c = $solicitud->limite_otorgado;
          $cuenta->save(false);
        }
      }
    }
  }
}
