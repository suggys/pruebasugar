<?php
class ActCtaxIncobrabilidad {
	public function UpdateCtaIncobrabilidad($bean, $event, $arguments){
		global $timedate, $current_user;
		if($bean->category_id == 'carta_incobrabilidad'){
			$bean->load_relationship('accounts');
			$cta = $bean->accounts->getBeans();
			foreach ($cta as $cuenta) {
				$cuenta->codigo_bloqueo_c = 'DI';
				$cuenta->estado_bloqueo_c = 'Bloqueado';
				$cuenta->save();
			}
		}
	}
}
?>
