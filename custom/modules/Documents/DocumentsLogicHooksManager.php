<?php
require_once 'custom/include/SolicitudCredito/SolicitudCreditoController.php';

class DocumentsLogicHooksManager {
  protected static $fetchedRow = array();

	public function beforeSave($bean, $event, $arguments){
    if(!empty($bean->id)){
      self::$fetchedRow[$bean->id] = $bean->fetched_row;
    }
    // $GLOBALS["log"]->fatal("beforeSave:".print_r($arguments,1));
	}

  public function afterSave($bean, $event, $arguments)
  {
    if(
      isset($_REQUEST['relate_to'])
      && isset($_REQUEST['parent_field_name'])
      && !empty($_REQUEST['relate_to'])
      && !empty($_REQUEST['parent_field_name'])
      && $_REQUEST['relate_to'] === "LOW01_SolicitudesCredito"
    ){
      $solicitud = BeanFactory::getBean($_REQUEST['relate_to'], $_REQUEST['relate_id']);
      $fieldDef = $solicitud->field_defs[$_REQUEST['parent_field_name']];

      $solicitudData = [];
      $solicitudData[$fieldDef['id_name']] = $bean->id;
      $solicitudData[$fieldDef['name']] = $bean->name;

      $sugarApi = new FileApiWrapper;
      $api = new RestService();
      $api->user = $current_user;

      $sugarApi->updateBeanWrapper($solicitud,$api, $solicitudData);
    }

  }
}
?>
