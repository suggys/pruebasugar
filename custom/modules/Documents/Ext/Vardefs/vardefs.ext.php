<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/rli_link_workflow.php

$dictionary['Document']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Document']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_monto_documento_garantia_c.php

 // created: 2016-09-21 15:27:43
$dictionary['Document']['fields']['monto_documento_garantia_c']['labelValue']='Monto del Documento Garantía';
$dictionary['Document']['fields']['monto_documento_garantia_c']['enforced']='';
$dictionary['Document']['fields']['monto_documento_garantia_c']['dependency']='';
$dictionary['Document']['fields']['monto_documento_garantia_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_category_id.php

 // created: 2016-09-21 15:43:59
$dictionary['Document']['fields']['category_id']['audited']=false;
$dictionary['Document']['fields']['category_id']['massupdate']=true;
$dictionary['Document']['fields']['category_id']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['category_id']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['category_id']['merge_filter']='disabled';
$dictionary['Document']['fields']['category_id']['calculated']=false;
$dictionary['Document']['fields']['category_id']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_doc_type.php

 // created: 2016-09-21 15:25:57
$dictionary['Document']['fields']['doc_type']['len']=100;
$dictionary['Document']['fields']['doc_type']['audited']=false;
$dictionary['Document']['fields']['doc_type']['comments']='Document type (ex: Google, box.net, IBM SmartCloud)';
$dictionary['Document']['fields']['doc_type']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['doc_type']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['doc_type']['merge_filter']='disabled';
$dictionary['Document']['fields']['doc_type']['calculated']=false;
$dictionary['Document']['fields']['doc_type']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_description.php

 // created: 2016-09-21 15:25:22
$dictionary['Document']['fields']['description']['audited']=false;
$dictionary['Document']['fields']['description']['massupdate']=false;
$dictionary['Document']['fields']['description']['comments']='Full text of the note';
$dictionary['Document']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['description']['merge_filter']='disabled';
$dictionary['Document']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.61',
  'searchable' => true,
);
$dictionary['Document']['fields']['description']['calculated']=false;
$dictionary['Document']['fields']['description']['rows']='6';
$dictionary['Document']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2016-09-21 15:27:43

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2016-09-21 15:27:44

 
?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/DocumentsVisibility.php

$dictionary['Document']['visibility']["DocumentsVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/Documents/Ext/Vardefs/low01_solicitudescredito_documents_1_Documents.php

// created: 2017-07-26 16:30:16
$dictionary["Document"]["fields"]["low01_solicitudescredito_documents_1"] = array (
  'name' => 'low01_solicitudescredito_documents_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_documents_1',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => 'LOW01_SolicitudesCredito',
  'side' => 'right',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'low01_solicitudescredito_documents_1low01_solicitudescredito_ida',
  'link-type' => 'one',
);
$dictionary["Document"]["fields"]["low01_solicitudescredito_documents_1_name"] = array (
  'name' => 'low01_solicitudescredito_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'save' => true,
  'id_name' => 'low01_solicitudescredito_documents_1low01_solicitudescredito_ida',
  'link' => 'low01_solicitudescredito_documents_1',
  'table' => 'low01_solicitudescredito',
  'module' => 'LOW01_SolicitudesCredito',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["low01_solicitudescredito_documents_1low01_solicitudescredito_ida"] = array (
  'name' => 'low01_solicitudescredito_documents_1low01_solicitudescredito_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_DOCUMENTS_TITLE_ID',
  'id_name' => 'low01_solicitudescredito_documents_1low01_solicitudescredito_ida',
  'link' => 'low01_solicitudescredito_documents_1',
  'table' => 'low01_solicitudescredito',
  'module' => 'LOW01_SolicitudesCredito',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
