({
  extendsFrom: 'SubpanelListView',

	initialize: function(options) {
		this._super('initialize',[options]);
	},
	_initializeMetadata : function(options) {
		console.log('subpanel agentes ext');
        var tmp = this._super('_initializeMetadata', arguments);
        if( this. _hideButtons( app.user.get('roles'), app.user.get('type') ) )
        	{
        		tmp.rowactions.actions = _.without(tmp.rowactions.actions, _.findWhere(tmp.rowactions.actions, {
                type: "unlink-action"
             }));
        	}
        return tmp;
    },
    _hideButtons: function(roles, user_type) {
	    var self = this;
	    var response = false;
	    if( !_.filter(app.user.get('roles'), function(rol) {
            return rol.toLowerCase() == "administrador de credito";
        }).length && user_type !== 'admin'){
	    		response = true;
	    }
	    return response;
	},
})
