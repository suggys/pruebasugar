<?php
// created: 2018-11-08 09:21:51
$viewdefs['Users']['EditView'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '2',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
      1 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'form' => 
    array (
      'headerTpl' => 'modules/Users/tpls/EditViewHeader.tpl',
      'footerTpl' => 'modules/Users/tpls/EditViewFooter.tpl',
    ),
    'useTabs' => false,
    'tabDefs' => 
    array (
      'LBL_USER_INFORMATION' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
      'LBL_EMPLOYEE_INFORMATION' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
    ),
    'syncDetailEditViews' => true,
  ),
  'panels' => 
  array (
    'LBL_USER_INFORMATION' => 
    array (
      0 => 
      array (
        0 => 
        array (
          'name' => 'user_name',
          'displayParams' => 
          array (
            'required' => true,
          ),
        ),
        1 => 'first_name',
      ),
      1 => 
      array (
        0 => 
        array (
          'name' => 'status',
          'displayParams' => 
          array (
            'required' => true,
          ),
        ),
        1 => 'last_name',
      ),
      2 => 
      array (
        0 => 
        array (
          'name' => 'UserType',
          'customCode' => '{if $IS_ADMIN}{$USER_TYPE_DROPDOWN}{else}{$USER_TYPE_READONLY}{/if}',
        ),
      ),
      3 => 
      array (
        0 => 'picture',
      ),
    ),
    'LBL_EMPLOYEE_INFORMATION' => 
    array (
      0 => 
      array (
        0 => 'employee_status',
        1 => 'show_on_employees',
      ),
      1 => 
      array (
        0 => 
        array (
          'name' => 'tipo_usuario_c',
          'label' => 'LBL_TIPO_USUARIO',
        ),
        1 => 
        array (
          'name' => 'sucursal_c',
          'label' => 'LBL_SUCURSAL',
        ),
      ),
      2 => 
      array (
        0 => 'title',
        1 => 'phone_work',
      ),
      3 => 
      array (
        0 => 'department',
        1 => 'phone_mobile',
      ),
      4 => 
      array (
        0 => 'reports_to_name',
        1 => 'phone_other',
      ),
      5 => 
      array (
        0 => '',
        1 => 'phone_fax',
      ),
      6 => 
      array (
        0 => '',
        1 => 'phone_home',
      ),
      7 => 
      array (
        0 => 'messenger_type',
        1 => 'messenger_id',
      ),
      8 => 
      array (
        0 => 'address_street',
        1 => 'address_city',
      ),
      9 => 
      array (
        0 => 'address_state',
        1 => 'address_postalcode',
      ),
      10 => 
      array (
        0 => 'address_country',
      ),
      11 => 
      array (
        0 => 'description',
      ),
    ),
  ),
);