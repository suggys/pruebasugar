<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_sucursal_c.php

 // created: 2017-08-23 18:56:01
$dictionary['User']['fields']['sucursal_c']['duplicate_merge_dom_value']=0;
$dictionary['User']['fields']['sucursal_c']['labelValue']='Sucursal';
$dictionary['User']['fields']['sucursal_c']['dependency']='';
$dictionary['User']['fields']['sucursal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/UsersVisibility.php

$dictionary['User']['visibility']["UsersVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_last_name.php

 // created: 2017-02-08 18:46:51
$dictionary['User']['fields']['last_name']['required']=false;
$dictionary['User']['fields']['last_name']['audited']=false;
$dictionary['User']['fields']['last_name']['massupdate']=false;
$dictionary['User']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['User']['fields']['last_name']['duplicate_merge_dom_value']='1';
$dictionary['User']['fields']['last_name']['merge_filter']='disabled';
$dictionary['User']['fields']['last_name']['unified_search']=false;
$dictionary['User']['fields']['last_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['User']['fields']['last_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Users/Ext/Vardefs/sugarfield_tipo_usuario_c.php

 // created: 2017-09-22 05:01:32
$dictionary['User']['fields']['tipo_usuario_c']['labelValue']='Tipo de Usuario';
$dictionary['User']['fields']['tipo_usuario_c']['dependency']='';
$dictionary['User']['fields']['tipo_usuario_c']['visibility_grid']='';

 
?>
