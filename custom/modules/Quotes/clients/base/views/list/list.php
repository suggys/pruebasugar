<?php
// created: 2018-09-15 05:21:23
$viewdefs['Quotes']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_LIST_QUOTE_NUM',
          'enabled' => true,
          'default' => true,
          'name' => 'quote_num',
        ),
        1 => 
        array (
          'label' => 'LBL_LIST_QUOTE_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        2 => 
        array (
          'name' => 'dise_prospectos_cocina_quotes_name',
          'label' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_DISE_PROSPECTOS_COCINA_TITLE',
          'enabled' => true,
          'id' => 'DISE_PROSPECTOS_COCINA_QUOTESDISE_PROSPECTOS_COCINA_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'date_quote_expected_closed',
          'label' => 'LBL_LIST_DATE_QUOTE_EXPECTED_CLOSED',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'label' => 'LBL_QUOTE_STAGE',
          'enabled' => true,
          'default' => true,
          'name' => 'quote_stage',
        ),
        5 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
);