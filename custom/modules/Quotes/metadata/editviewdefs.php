<?php
// created: 2018-11-08 09:21:51
$viewdefs['Quotes']['EditView'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '2',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
      1 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'form' => 
    array (
      'footerTpl' => 'modules/Quotes/tpls/EditViewFooter.tpl',
    ),
    'useTabs' => false,
    'tabDefs' => 
    array (
      'LBL_QUOTE_INFORMATION' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
    ),
  ),
  'panels' => 
  array (
    'lbl_quote_information' => 
    array (
      0 => 
      array (
        0 => 'name',
        1 => 
        array (
          'name' => 'dise_prospectos_cocina_quotes_name',
        ),
      ),
      1 => 
      array (
        0 => 
        array (
          'name' => 'quote_num',
          'type' => 'readonly',
          'displayParams' => 
          array (
            'required' => false,
          ),
        ),
        1 => 'quote_stage',
      ),
      2 => 
      array (
        0 => 'payment_terms',
        1 => 'date_quote_expected_closed',
      ),
      3 => 
      array (
        0 => 'description',
      ),
    ),
  ),
);