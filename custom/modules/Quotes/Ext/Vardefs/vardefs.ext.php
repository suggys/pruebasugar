<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/dise_prospectos_cocina_quotes_Quotes.php

// created: 2018-01-11 00:47:54
$dictionary["Quote"]["fields"]["dise_prospectos_cocina_quotes"] = array (
  'name' => 'dise_prospectos_cocina_quotes',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_quotes',
  'source' => 'non-db',
  'module' => 'dise_Prospectos_cocina',
  'bean_name' => 'dise_Prospectos_cocina',
  'side' => 'right',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_QUOTES_TITLE',
  'id_name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
  'link-type' => 'one',
);
$dictionary["Quote"]["fields"]["dise_prospectos_cocina_quotes_name"] = array (
  'name' => 'dise_prospectos_cocina_quotes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'save' => true,
  'id_name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_quotes',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'name',
);
$dictionary["Quote"]["fields"]["dise_prospectos_cocina_quotesdise_prospectos_cocina_ida"] = array (
  'name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_QUOTES_TITLE_ID',
  'id_name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_quotes',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Quote']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/sugarfield_description.php

 // created: 2018-01-16 06:06:19
$dictionary['Quote']['fields']['description']['required']=true;
$dictionary['Quote']['fields']['description']['audited']=true;
$dictionary['Quote']['fields']['description']['massupdate']=false;
$dictionary['Quote']['fields']['description']['comments']='Full text of the note';
$dictionary['Quote']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Quote']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Quote']['fields']['description']['merge_filter']='disabled';
$dictionary['Quote']['fields']['description']['unified_search']=false;
$dictionary['Quote']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.57',
  'searchable' => true,
);
$dictionary['Quote']['fields']['description']['calculated']=false;
$dictionary['Quote']['fields']['description']['rows']='6';
$dictionary['Quote']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Quotes/Ext/Vardefs/rli_link_workflow.php

$dictionary['Quote']['fields']['revenuelineitems']['workflow'] = true;
?>
