<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Emails/Ext/Vardefs/rli_link_workflow.php

$dictionary['Email']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Emails/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Email']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Emails/Ext/Vardefs/emailsVisibility.php

$dictionary['Email']['visibility']["customEmailsVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/Emails/Ext/Vardefs/low01_solicitudescredito_activities_emails_Emails.php

// created: 2017-07-21 12:47:08
$dictionary["Email"]["fields"]["low01_solicitudescredito_activities_emails"] = array (
  'name' => 'low01_solicitudescredito_activities_emails',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_emails',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_EMAILS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
);

?>
