<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/Administration/controller.php');

class CustomAdministrationController extends AdministrationController
{
  public function action_lowes_settings() {
    $this->view = 'lowes_settings';
    ///////////////////////////////////////////////////////////////////////////////
    //// HANDLE CHANGES AFTER SUBMIT
    if(isset($_REQUEST['process']) && $_REQUEST['process'] == 'true') {
      $admin = new Administration();
      $admin->retrieveSettings();

      if (!empty($_REQUEST["folio_konesh"])) {
        $admin->saveSetting("lowes_settings", "folio_konesh", $_REQUEST["folio_konesh"]);
      }

      if (!empty($_REQUEST["leads_consecutivo"])) {
        $admin->saveSetting("lowes_settings", "leads_consecutivo", $_REQUEST["leads_consecutivo"]);
      }

      if (!empty($_REQUEST["prospects_consecutivo"])) {
        $admin->saveSetting("lowes_settings", "prospects_consecutivo", $_REQUEST["prospects_consecutivo"]);
      }

      if (!empty($_REQUEST["clientes_consecutivo"])) {
        $admin->saveSetting("lowes_settings", "clientes_consecutivo", $_REQUEST["clientes_consecutivo"]);
      }

      if (!empty($_REQUEST["monto_maximo_proyectos"])) {
        $admin->saveSetting("lowes_settings", "monto_maximo_proyectos", $_REQUEST["monto_maximo_proyectos"]);
      }

      if (!empty($_REQUEST["pdf_manager_solicitud_credito"])) {
        $admin->saveSetting("lowes_settings", "pdf_manager_solicitud_credito", $_REQUEST["pdf_manager_solicitud_credito"]);
      }

      if (!empty($_REQUEST["accounts_actividad_cliente"])) {
        $admin->saveSetting("lowes_settings", "accounts_actividad_cliente", $_REQUEST["accounts_actividad_cliente"]);
      }

      if (!empty($_REQUEST["emailtemplate_monto_maximo_proye"])) {
        $admin->saveSetting("lowes_settings", "emailtemplate_monto_maximo_proye", $_REQUEST["emailtemplate_monto_maximo_proye"]);
      }

      if (!empty($_REQUEST["emailtemplate_notifica_externo"])) {
        $admin->saveSetting("lowes_settings", "emailtemplate_notifica_externo", $_REQUEST["emailtemplate_notifica_externo"]);
      }

      if (!empty($_REQUEST["emailtemplate_notifica_rechazo"])) {
        $admin->saveSetting("lowes_settings", "emailtemplate_notifica_rechazo", $_REQUEST["emailtemplate_notifica_rechazo"]);
      }

      if (!empty($_REQUEST["emailtem_notifica_aprob"])) {
        $admin->saveSetting("lowes_settings", "emailtem_notifica_aprob", $_REQUEST["emailtem_notifica_aprob"]);
      }

      if (!empty($_REQUEST["email_bienvenida_cli"])) {
        $admin->saveSetting("lowes_settings", "email_bienvenida_cli", $_REQUEST["email_bienvenida_cli"]);
      }

      if (!empty($_REQUEST["email_formulario_sol_cre"])) {
        $admin->saveSetting("lowes_settings", "email_formulario_sol_cre", $_REQUEST["email_formulario_sol_cre"]);
      }

      if (!empty($_REQUEST["monto_maximo_gc"])) {
        $admin->saveSetting("lowes_settings", "monto_maximo_gc", $_REQUEST["monto_maximo_gc"]);
      }

      if (!empty($_REQUEST["monto_maximo_df"])) {
        $admin->saveSetting("lowes_settings", "monto_maximo_df", $_REQUEST["monto_maximo_df"]);
      }

      if (!empty($_REQUEST["pdf_manager_estado_cuenta_portal"])) {
        $admin->saveSetting("lowes_settings", "pdf_manager_estado_cuenta_portal", $_REQUEST["pdf_manager_estado_cuenta_portal"]);
      }

      if (!empty($_REQUEST["email_evalua_sol_cred"])) {
        $admin->saveSetting("lowes_settings", "email_evalua_sol_cred", $_REQUEST["email_evalua_sol_cred"]);
      }

      if (!empty($_REQUEST["id_pos"])) {
        $admin->saveSetting("lowes_settings", "id_pos", $_REQUEST["id_pos"]);
      }

      header('Location: index.php?module=Administration&action=lowes_settings');
    }
  }
}
