<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');



global $current_user, $sugar_config;
if (!is_admin($current_user)) sugar_die("Unauthorized access to administration.");


require_once('include/MVC/View/SugarView.php');


class Viewlowes_settings extends SugarView {


  public function __construct() {
    parent::__construct();
  }


  public function preDisplay() {
    $this->dv->tpl = 'custom/modules/Administration/tpl/lowes_settings.tpl';
  }


  public function display() {
    global $sugar_config, $mod_strings, $app_strings;


    $smarty = new Sugar_Smarty();
    $smarty->assign('MOD', $mod_strings);
    $smarty->assign('APP', $app_strings);
    $smarty->assign('config', $sugar_config['lowes_settings']);
    $admin = new Administration();
    $admin->retrieveSettings();
    if (array_key_exists('lowes_settings_folio_konesh', $admin->settings))
    $smarty->assign('FOLIO_KONESH', $admin->settings['lowes_settings_folio_konesh']);
    if (array_key_exists('lowes_settings_leads_consecutivo', $admin->settings))
    $smarty->assign('LEADS_CONSECUTIVO', $admin->settings['lowes_settings_leads_consecutivo']);
    if (array_key_exists('lowes_settings_prospects_consecutivo', $admin->settings))
    $smarty->assign('PROSPECTS_CONSECUTIVO', $admin->settings['lowes_settings_prospects_consecutivo']);
    if (array_key_exists('lowes_settings_clientes_consecutivo', $admin->settings))
    $smarty->assign('CLIENTES_CONSECUTIVO', $admin->settings['lowes_settings_clientes_consecutivo']);
    if (array_key_exists('lowes_settings_monto_maximo_proyectos', $admin->settings))
    $smarty->assign('MONTO_MAXIMO_PROYECTOS', $admin->settings['lowes_settings_monto_maximo_proyectos']);
    if (array_key_exists('lowes_settings_pdf_manager_solicitud_credito', $admin->settings))
    $smarty->assign('PDF_MANAGER_SOLICITUD_CREDITO', $admin->settings['lowes_settings_pdf_manager_solicitud_credito']);
    if (array_key_exists('lowes_settings_accounts_actividad_cliente', $admin->settings))
    $smarty->assign('ACTIVIDAD_CLIENTE', $admin->settings['lowes_settings_accounts_actividad_cliente']);
    if (array_key_exists('lowes_settings_emailtemplate_monto_maximo_proye', $admin->settings))
    $smarty->assign('EMAILTEMPLATE_MONTO_MAXIMO_PROYE', $admin->settings['lowes_settings_emailtemplate_monto_maximo_proye']);
    if (array_key_exists('lowes_settings_emailtemplate_notifica_externo', $admin->settings))
    $smarty->assign('EMAILTEMPLATE_NOTIFICA_EXTERNO', $admin->settings['lowes_settings_emailtemplate_notifica_externo']);
    if (array_key_exists('lowes_settings_emailtem_notifica_aprob', $admin->settings))
    $smarty->assign('EMAILTEMP_NOTIFICA_APROB', $admin->settings['lowes_settings_emailtem_notifica_aprob']);

    if (array_key_exists('lowes_settings_email_bienvenida_cli', $admin->settings))
    $smarty->assign('EMAIL_BIENVENIDA_CLI', $admin->settings['lowes_settings_email_bienvenida_cli']);
    if (array_key_exists('lowes_settings_email_formulario_sol_cre', $admin->settings))
    $smarty->assign('EMAIL_FORMULARIO_SOL_CRED', $admin->settings['lowes_settings_email_formulario_sol_cre']);

    if (array_key_exists('lowes_settings_emailtemplate_notifica_rechazo', $admin->settings))
    $smarty->assign('EMAILTEMPLATE_NOTIFICA_RECHAZO', $admin->settings['lowes_settings_emailtemplate_notifica_rechazo']);
    if (array_key_exists('lowes_settings_monto_maximo_gc', $admin->settings))
    $smarty->assign('MONTO_MAXIMO_GC', $admin->settings['lowes_settings_monto_maximo_gc']);
    if (array_key_exists('lowes_settings_monto_maximo_df', $admin->settings))
    $smarty->assign('MONTO_MAXIMO_DF', $admin->settings['lowes_settings_monto_maximo_df']);
    if (array_key_exists('lowes_settings_pdf_manager_estado_cuenta_portal', $admin->settings))
    $smarty->assign('PDF_MANAGER_ESTADO_CUENTA_PORTAL', $admin->settings['lowes_settings_pdf_manager_estado_cuenta_portal']);
    if (array_key_exists('lowes_settings_email_evalua_sol_cred', $admin->settings))
    $smarty->assign('EMAIL_EVALUA_SOL_CRED', $admin->settings['lowes_settings_email_evalua_sol_cred']);
    if (array_key_exists('lowes_settings_id_pos', $admin->settings))
    $smarty->assign('ID_POS', $admin->settings['lowes_settings_id_pos']);

    $smarty->display($this->dv->tpl);
  }
}
?>
