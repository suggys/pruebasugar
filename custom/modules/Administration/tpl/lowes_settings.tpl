<script type="text/javascript">
var ERR_NO_SINGLE_QUOTE = '{$APP.ERR_NO_SINGLE_QUOTE}';
var cannotEq = "{$APP.ERR_DECIMAL_SEP_EQ_THOUSANDS_SEP}";
{literal}
function verify_data(formName) {
  var f = document.getElementById(formName);


  for(i=0; i<f.elements.length; i++) {
    if(f.elements[i].value == "'") {
      alert(ERR_NO_SINGLE_QUOTE + " " + f.elements[i].name);
      return false;
    }
  }
  return true;
}
</script>
{/literal}
<BR>
  <form id="ConfigureSettings" name="ConfigureSettings" enctype='multipart/form-data' method="POST"
  action="index.php?module=Administration&action=lowes_settings&process=true">
  {sugar_csrf_form_token}

  <span class='error'>{$error.main}</span>


  <table width="100%" cellpadding="0" cellspacing="0" border="0" class="actionsContainer">
    <tr>
      <td>
        <input title="{$APP.LBL_SAVE_BUTTON_TITLE}"
        accessKey="{$APP.LBL_SAVE_BUTTON_KEY}"
        class="button primary"
        type="submit"
        name="save"
        onclick="return verify_data('ConfigureSettings');"
        value="  {$APP.LBL_SAVE_BUTTON_LABEL}  " >
        <input title="{$MOD.LBL_CANCEL_BUTTON_TITLE}"  onclick="document.location.href='index.php?module=Administration&action=index'" class="button"  type="button" name="cancel" value="  {$APP.LBL_CANCEL_BUTTON_LABEL}  " > </td>
      </tr>
    </table>

    <table width="100%" border="0" cellspacing="1" cellpadding="0" class="edit view">
      <tr>
        <th align="left" scope="row" colspan="4"><h4>{$MOD.AUTHENTICATION_SETTINGS}</h4></th>
      </tr>
      <tr>
        <td nowrap width="10%" scope="row">{$MOD.LBL_FOLIO_KONESH}: </td>
        <td width="25%" >
          <input type='text' name='folio_konesh' size="60" value='{$FOLIO_KONESH}'>
        </td>
        <td nowrap width="10%" scope="row">{$MOD.LBL_LEADS_CONSECUTIVO}: </td>
        <td width="25%" >
          <input type='text' name='leads_consecutivo' size="60" value='{$LEADS_CONSECUTIVO}'>
        </td>
      </tr>
      <tr>
        <td nowrap width="10%" scope="row">{$MOD.LBL_ID_POS}: </td>
        <td width="25%" >
          <input type='text' name='id_pos' size="60" value='{$ID_POS}'>
        </td>
        <td nowrap width="10%" scope="row"></td>
        <td width="25%" >
        </td>
      </tr>
      <tr>
        <td nowrap width="10%" scope="row">{$MOD.LBL_PROSPECTS_CONSECUTIVO}: </td>
        <td width="25%" >
          <input type='text' name='prospects_consecutivo' size="60" value='{$PROSPECTS_CONSECUTIVO}'>
        </td>
        <td nowrap width="10%" scope="row">{$MOD.LBL_CLIENTES_CONSECUTIVO}: </td>
        <td width="25%" >
          <input type='text' name='clientes_consecutivo' size="60" value='{$CLIENTES_CONSECUTIVO}'>
        </td>
      </tr>
      <tr>
        <td nowrap width="10%" scope="row">{$MOD.LBL_MONTO_MAXIMO_PROYECTOS}: </td>
        <td width="25%" >
          <input type='text' name='monto_maximo_proyectos' size="60" value='{$MONTO_MAXIMO_PROYECTOS}'>
        </td>
        <td nowrap width="10%" scope="row">{$MOD.LBL_PDF_MANAGER_SOLICITUD_CREDITO}: </td>
        <td width="25%" >
          <input type='text' name='pdf_manager_solicitud_credito' size="60" value='{$PDF_MANAGER_SOLICITUD_CREDITO}'>
        </td>
      </tr>
      <tr>
        <td nowrap width="10%" scope="row">{$MOD.LBL_ACTIVIDAD_CLIENTE}: </td>
        <td width="25%" >
          <input type='text' name='accounts_actividad_cliente' size="60" value='{$ACTIVIDAD_CLIENTE}'>
        </td>
        <td nowrap width="10%" scope="row">{$MOD.LBL_EMAILTEMPLATE_MONTO_MAXIMO_PROYE}: </td>
        <td width="25%" >
          <input type='text' name='emailtemplate_monto_maximo_proye' size="60" value='{$EMAILTEMPLATE_MONTO_MAXIMO_PROYE}'>
        </td>
      </tr>
      <tr>
        <td nowrap width="10%" scope="row">{$MOD.LBL_EMAILTEMPLATE_NOTIFICA_EXTERNO}: </td>
        <td width="25%" >
          <input type='text' name='emailtemplate_notifica_externo' size="60" value='{$EMAILTEMPLATE_NOTIFICA_EXTERNO}'>
        </td>
        <td nowrap width="10%" scope="row">{$MOD.LBL_EMAILTEMPLATE_NOTIFICA_RECHAZO}: </td>
        <td width="25%" >
          <input type='text' name='emailtemplate_notifica_rechazo' size="60" value='{$EMAILTEMPLATE_NOTIFICA_RECHAZO}'>
        </td>
      </tr>
      <tr>
        <td nowrap width="10%" scope="row">{$MOD.LBL_MONTO_MAXIMO_GC}: </td>
        <td width="25%" >
          <input type='text' name='monto_maximo_gc' size="60" value='{$MONTO_MAXIMO_GC}'>
        </td>
        <td nowrap width="10%" scope="row">{$MOD.LBL_MONTO_MAXIMO_DF}: </td>
        <td width="25%" >
          <input type='text' name='monto_maximo_df' size="60" value='{$MONTO_MAXIMO_DF}'>
        </td>
      </tr>
      <tr>
        <td nowrap width="10%" scope="row">{$MOD.LBL_EMAILTEMP_NOTIFICA_APROB}: </td>
        <td width="25%" >
          <input type='text' name='emailtem_notifica_aprob' size="60" value='{$EMAILTEMP_NOTIFICA_APROB}'>
        </td>
        <td nowrap width="10%" scope="row">{$MOD.LBL_PDF_MANAGER_ESTADO_CUENTA_PORTAL}: </td>
        <td width="25%" >
          <input type='text' name='pdf_manager_estado_cuenta_portal' size="60" value='{$PDF_MANAGER_ESTADO_CUENTA_PORTAL}'>
        </td>
      </tr>
      <tr>
        <td nowrap width="10%" scope="row">{$MOD.LBL_EMAIL_BIENVENIDA_CLI}:</td>
        <td width="25%" >
          <input type='text' name='email_bienvenida_cli' size="60" value='{$EMAIL_BIENVENIDA_CLI}'>
        </td>
        <td nowrap width="10%" scope="row">{$MOD.LBL_EMAIL_FORMULARIO_SOL_CRED}</td>
        <td width="25%" >
          <input type='text' name='email_formulario_sol_cre' size="60" value='{$EMAIL_FORMULARIO_SOL_CRED}'>
        </td>
      </tr>
      <tr>
        <td nowrap width="10%" scope="row">{$MOD.LBL_EMAIL_EVALUA_SOL_CRED}:</td>
        <td width="25%" >
          <input type='text' name='email_evalua_sol_cred' size="60" value='{$EMAIL_EVALUA_SOL_CRED}'>
        </td>
      </tr>

    </table>
    <div style="padding-top: 2px;">
      <input title="{$APP.LBL_SAVE_BUTTON_TITLE}" class="button primary"  type="submit" name="save" value="  {$APP.LBL_SAVE_BUTTON_LABEL}  " onclick="return verify_data('ConfigureSettings');"/>
      <input title="{$MOD.LBL_CANCEL_BUTTON_TITLE}"  onclick="document.location.href='index.php?module=Administration&action=index'" class="button"  type="button" name="cancel" value="  {$APP.LBL_CANCEL_BUTTON_LABEL}  " />
    </div>
    {$JAVASCRIPT}
  </form>
