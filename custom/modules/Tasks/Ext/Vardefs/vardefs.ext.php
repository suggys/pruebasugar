<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/rli_link_workflow.php

$dictionary['Task']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Task']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Tasks/Ext/Vardefs/low01_solicitudescredito_activities_tasks_Tasks.php

// created: 2017-07-21 12:47:08
$dictionary["Task"]["fields"]["low01_solicitudescredito_activities_tasks"] = array (
  'name' => 'low01_solicitudescredito_activities_tasks',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_tasks',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_TASKS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
);

?>
