<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

    class CalculaSaldoCuentaPorFacturas
    {
        public function calculaSaldoDeudor($bean, $event, $arguments)
        {
            global $timedate, $current_user;
            if(isset($bean->importado_c) && $bean->importado_c){
                return true;
            }
        	   $afectoSaldo = false;
            if( $arguments['related_module'] == 'LF_Facturas' && isset($arguments['related_id']) && $event=="after_relationship_add" )
            {
                $facturas = BeanFactory::getBean('LF_Facturas', $arguments['related_id']);
                if( $facturas->id )
                {
                    $facturas->fetch($facturas->id);
                    $orden = $this->getOrden($facturas);
                    if($orden && $this->isLineaAncitipo($orden)){
                        $facturas->abono = $facturas->saldo;
                        $facturas->saldo = 0;
                        $facturas->linea_anticipo_c = true;
                        $facturas->save(false);
                    }
                    else{
                        $grupoEmpresas = !empty($bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida) ? BeanFactory::getBean('GPE_Grupo_empresas', $bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida) : null;
                        if($grupoEmpresas){
                            $grupoEmpresas->saldo_deudor_c = $this->getSaldoDeudorGrupo($grupoEmpresas);
                            $grupoEmpresas->save();
                        }
                        $bean->saldo_deudor_c = $this->getSaldoDeudorCuenta($bean);
                        $bean->save();
                        $afectoSaldo = true;
                    }
                }
            }
            else if( $arguments['related_module'] == 'LF_Facturas' && isset($arguments['related_id']) && $event=="after_relationship_delete" )
            {
                $facturas = BeanFactory::getBean('LF_Facturas', $arguments['related_id']);
                if( $facturas->id )
                {
                	$bean->saldo_deudor_c -= $facturas->importe;
                	$bean->save();
                	$afectoSaldo = true;
                }
            }
	    	if( $bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida && $afectoSaldo ){
	    		$this->actualizaSaldoGpoEmpresas( $bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida, $event );
	    	}
      }

      private function actualizaSaldoGpoEmpresas( $idGpoEmpresa, $event ){
        if(isset($bean->importado_c) && $bean->importado_c){
            return true;
        }
      	$gpoEmpresa = BeanFactory::getBean('GPE_Grupo_empresas',$idGpoEmpresa);
      	$gpoEmpresa->load_relationship('gpe_grupo_empresas_accounts');
      	$cuentas = $gpoEmpresa->gpe_grupo_empresas_accounts->getBeans();
      	$saldo_deudor_c = 0;
      	foreach($cuentas as $cuenta) {
              if(!isset($bean->importado_c) || !$bean->importado_c){
               $saldo_deudor_c += $cuenta->saldo_deudor_c;
              }

  	        $gpoEmpresa->saldo_deudor_c = $saldo_deudor_c;
  	        $gpoEmpresa->save();
          }
      }

      private function getOrden($factura)
      {
          if($factura->lf_facturas_orden_ordenesorden_ordenes_ida){
              return BeanFactory::getBean('orden_Ordenes', $factura->lf_facturas_orden_ordenesorden_ordenes_ida);
          }
          return null;
      }

      private function isLineaAncitipo($orden)
      {
          return substr($orden->id_customer_c, -4) >= 7000 ;
      }

      public function getSaldoDeudorCuenta($account)
      {
        $saldoDeudor = 0;
        $facturaBean = BeanFactory::newBean('LF_Facturas');
        $sugarQuery = new SugarQuery();
    		$sugarQuery->from($facturaBean, array('team_security'=>false));
        $sugarQuery->join('accounts_lf_facturas_1', array('alias' => 'accounts' ,'team_security'=>false));
    		$sugarQuery->where()
    			->equals('accounts.id', $account->id)
    			->gt('saldo', 0);
        $sugarQuery->select(array('id', 'saldo'));
        $facturas = $result = $sugarQuery->execute();
        foreach ($facturas as $facturaData) {
          $saldoDeudor += $facturaData['saldo'];
        }
        return $saldoDeudor;
      }

      public function getSaldoDeudorGrupo($grupoEmpresa)
      {
        $saldoDeudor = 0;
        $facturaBean = BeanFactory::newBean('LF_Facturas');
        $sugarQuery = new SugarQuery();
    		$sugarQuery->from($facturaBean, array('team_security'=>false));
        $sugarQuery->join('accounts_lf_facturas_1', array('alias' => 'accounts', 'team_security'=>false));
        $sugarQuery->join('gpe_grupo_empresas_accounts', array('relatedJoin'=>'accounts', 'alias' => 'grupoempresas',  'team_security'=>false));
    		$sugarQuery->where()
    			->equals("grupoempresas.id", $grupoEmpresa->id)
    			->gt('saldo', 0);
        $sugarQuery->select(array('id', 'saldo'));
        $facturas = $result = $sugarQuery->execute();
        foreach ($facturas as $facturaData) {
          $saldoDeudor += $facturaData['saldo'];
        }
        return $saldoDeudor;
      }
  }

?>
