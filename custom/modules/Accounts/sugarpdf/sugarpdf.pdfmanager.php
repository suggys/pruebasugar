<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('include/Sugarpdf/sugarpdf/sugarpdf.pdfmanager.php');
class AccountsSugarpdfPdfmanager extends SugarpdfPdfmanager{
	public $saldoTotalFacturas = 0;
	public $cantFacturas = 0;
	public $totalMontoFacturas = 0;
	private $cstmFooterEdoCuenta = false;
	public function preDisplay(){
		parent::preDisplay();
		global $timedate;
		if (defined('SUGAR_SHADOW_PATH')){
			$this->ss->secure_dir[] = SUGAR_SHADOW_PATH;
		}
		//if(isset($_REQUEST['fromPortal']) && !empty($_REQUEST['fromPortal']) || ){
		$today = $timedate->getInstance()->nowDbDate();
		$fact = $this->getFacturas($this->bean->id);
		$fact = $this->ordenaArray($fact,'dt_crt');
		$fact = $this->procesaFacturas($fact);
		$this->ss->assign('CuentaFacturas', $this->cantFacturas );
		$this->ss->assign('SumatoriaSaldoTotalFacturas', number_format($this->totalMontoFacturas, 2, '.', ',') );
		$this->ss->assign('SumatoriaSaldoTotalFacturas', number_format($this->saldoTotalFacturas, 2, '.', ',') );
		$this->ss->assign('facturas', $fact );
		$this->cstmFooterEdoCuenta = true;
		//}
	}
	public function getFacturas($id){
		$beanFacturas = BeanFactory::newBean('LF_Facturas');
		$query = new SugarQuery();
		$query->from($beanFacturas);
		$accountsJoinName = $query->join('accounts_lf_facturas_1', ['team_security'=>false])->joinName();
    $query->where()
      ->equals("$accountsJoinName.id",$id)
      ->gt("saldo", 0);
		return $query->execute();
	}
	public function procesaFacturas($arrayFacturas){
		$this->cantFacturas = count($arrayFacturas);
		$this->totalMontoFacturas = 0;
		foreach ($arrayFacturas as $key => $factura) {
			$this->totalMontoFacturas += floatval($factura['saldo']);
			$arrayFacturas[$key]['saldo_num'] = number_format($factura['saldo'], 2, '.', ',');
			$arrayFacturas[$key]['sub_total_num'] = number_format($factura['sub_total'], 2, '.', ',');
			$arrayFacturas[$key]['iva_num'] = number_format($factura['iva'], 2, '.', ',');
			$arrayFacturas[$key]['importe_num'] = number_format($factura['importe'], 2, '.', ',');
			$arrayFacturas[$key]['abono_num'] = number_format($factura['abono'], 2, '.', ',');
		}
		$this->saldoTotalFacturas += $this->totalMontoFacturas;
		return $arrayFacturas;
	}
	public function ordenaArray($array,$columna,$tipoOrden=SORT_DESC)
	{
		$tmplLista = array();
		foreach ($array as $clave => $fila) {
			$tmplLista[$clave] = $fila[$columna];
		}
		array_multisort($tmplLista, $tipoOrden, $array);
		return $array;
	}
	public function Footer()
	{
		parent::Footer();
		if($this->cstmFooterEdoCuenta)
		{
			$y = $this->GetY();
			//$y += 2;
			$html = '<p style="font-size:.8em;">';
			$html .= "Lowe's Companies México, S de RL de CV.- Av Gómez Morín Sur No. 955 Col Montebello, San Pedro Garza García, Nuevo León C.P. 66279</p>";
			$this->SetMargins(PDF_MARGIN_LEFT,0,PDF_MARGIN_RIGHT);
			$this->SetX(15);
			$this->SetY($y);
			$this->writeHTML($html,true,false,true,false,'');
		}
	}
}
