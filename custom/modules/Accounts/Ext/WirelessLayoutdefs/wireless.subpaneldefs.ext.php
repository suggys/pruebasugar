<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_lowae_autorizacion_especial_1_Accounts.php

 // created: 2016-10-13 23:48:34
$layout_defs["Accounts"]["subpanel_setup"]['accounts_lowae_autorizacion_especial_1'] = array (
  'order' => 100,
  'module' => 'lowae_autorizacion_especial',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'get_subpanel_data' => 'accounts_lowae_autorizacion_especial_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_lf_facturas_1_Accounts.php

 // created: 2016-10-24 13:40:01
$layout_defs["Accounts"]["subpanel_setup"]['accounts_lf_facturas_1'] = array (
  'order' => 100,
  'module' => 'LF_Facturas',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'get_subpanel_data' => 'accounts_lf_facturas_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/accounts_orden_ordenes_1_Accounts.php

 // created: 2016-10-11 21:41:47
$layout_defs["Accounts"]["subpanel_setup"]['accounts_orden_ordenes_1'] = array (
  'order' => 100,
  'module' => 'orden_Ordenes',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE',
  'get_subpanel_data' => 'accounts_orden_ordenes_1',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/low01_solicitudescredito_accounts_Accounts.php

 // created: 2017-07-21 12:47:09
$layout_defs["Accounts"]["subpanel_setup"]['low01_solicitudescredito_accounts'] = array (
  'order' => 100,
  'module' => 'LOW01_SolicitudesCredito',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_accounts',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/nc_notas_credito_accounts_Accounts.php

 // created: 2016-10-12 00:13:37
$layout_defs["Accounts"]["subpanel_setup"]['nc_notas_credito_accounts'] = array (
  'order' => 100,
  'module' => 'NC_Notas_credito',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE',
  'get_subpanel_data' => 'nc_notas_credito_accounts',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/WirelessLayoutdefs/lowes_pagos_accounts_Accounts.php

 // created: 2016-10-22 15:12:51
$layout_defs["Accounts"]["subpanel_setup"]['lowes_pagos_accounts'] = array (
  'order' => 100,
  'module' => 'lowes_Pagos',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE',
  'get_subpanel_data' => 'lowes_pagos_accounts',
);

?>
