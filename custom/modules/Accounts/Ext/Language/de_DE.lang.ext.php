<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.customlowae_autorizacion_especial_accounts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID'] = 'Autorización Especial ID';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Autorización Especial';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.customaccounts_lowae_autorizacion_especial_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID'] = 'Autorización Especial ID';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.customaccounts_lf_facturas_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID'] = 'Autorización Especial ID';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Facturas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Pagos.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Notas_credito.php.LowesCRM3500.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Grupo_empresas.php.LowesCRM3500.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Pagos.php.LowesCRM3500.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Grupo_empresas.php.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Pagos.php.LowesCRM3500.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Notas_credito.php.LowesCRM3500.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Notas_credito.php.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Grupo_empresas.php.LowesCRM3500.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Pagos.php.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';

?>
