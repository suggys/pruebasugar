<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Nombre o Razón Social:';
$mod_strings['LBL_ID_AR_CREDIT'] = 'Número de Cuenta Crédito';
$mod_strings['LBL_FACTURACION_AUTOMATICA_C'] = 'Facturación automática:';
$mod_strings['LBL_TYPE'] = 'Tipo de Persona:';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Calle:';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'CP:';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Municipio:';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Estado:';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País:';
$mod_strings['LBL_PHONE_ALT'] = 'Teléfono celular:';
$mod_strings['LBL_PHONE_OFFICE'] = 'Teléfono oficina:';
$mod_strings['LBL_ANY_EMAIL'] = 'Email comercial:';
$mod_strings['LBL_WEBSITE'] = 'Página de internet:';
$mod_strings['LBL_ID_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_ID_VENDEDOR_C'] = 'Vendedor';
$mod_strings['LBL_LIMITE_CREDITO_AUTORIZADO_C'] = 'Límite de crédito autorizado:';
$mod_strings['LBL_PLAZO_PAGO_AUTORIZADO_C'] = 'Plazo de pago autorizado:';
$mod_strings['LBL_DIAS_TOLERANCIA_C'] = 'Días de tolerancia:';
$mod_strings['LBL_ID_LINEA_ANTICIPO_C'] = 'ID Línea de Anticipo:';
$mod_strings['LBL_NUMERO_CUENTA_CLABE_C'] = 'Número de Cuenta Clabe Banamex:';
$mod_strings['LBL_ESTADO_CUENTA_C'] = 'Estado de la cuenta:';
$mod_strings['LBL_ESTADO_BLOQUEO_C'] = 'Estado de bloqueo:';
$mod_strings['LBL_CODIGO_BLOQUEO_C'] = 'Código de bloqueo:';
$mod_strings['LBL_ESTADO_CARTERA_C'] = 'Estado según cartera:';
$mod_strings['LBL_CREDITO_DISPONIBLE_C'] = 'Crédito disponible:';
$mod_strings['LBL_SALDO_DEUDOR_C'] = 'Saldo deudor:';
$mod_strings['LBL_SALDO_PENDIENTE_APLICAR_C'] = 'Saldo pendiente por aplicar:';
$mod_strings['LBL_SALDO_FAVOR_C'] = 'Saldo a favor:';
$mod_strings['LBL_ID_POS_C'] = 'ID POS:';
$mod_strings['LBL_OWNERSHIP'] = 'Nombre del representante Legal:';
$mod_strings['LBL_NOMBRE_AVALA_C'] = 'Nombre del Aval:';
$mod_strings['LBL_NOMBRE_DOCUMENTO_GARANTIA_C'] = 'Nombre Documento Garantía:';
$mod_strings['LBL_MONTO_DOCUMENTO_GARANTIA_C'] = 'Monto de documento garantía:';
$mod_strings['LBL_GRUPO_EMPRESAS_ID'] = 'Grupo de Empresas:';
$mod_strings['LBL_ADMINISTRAR_CREDITO_GRUPAL_C'] = 'Sumar / Administrar Límite de Crédito por Grupo:';
$mod_strings['LBL_ESTADO_GRUPO_C'] = 'Estado del Grupo';
$mod_strings['LBL_TIPO_CUENTA_C'] = 'Tipo de Cuenta';
$mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación:';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_COMENTADIOS_C'] = 'Comentarios';
$mod_strings['LBL_TIPO_DOCUMENTO_C'] = 'Catálogo documento garantía:';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_ACCOUNT_TYPE_C'] = 'Tipo de persona:';
$mod_strings['LBL_BILLING_ADDRESS_NUMBER_C'] = 'Número';
$mod_strings['LBL_BILLING_ADDRESS_COL_C'] = 'Colonia:';
$mod_strings['LBL_BILLING_ADDRESS_STATE_C'] = 'Estado:';
$mod_strings['LBL_OWN_TEL_CEL_C'] = 'Teléfono celular:';
$mod_strings['LBL_OWN_EMAIL_C'] = 'Email:';
$mod_strings['LBL_OWN_RFC_C'] = 'RFC de representante Legal:';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Nuevo Panel 2';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Nuevo Panel 3';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Nuevo Panel 4';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.customlowae_autorizacion_especial_accounts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID'] = 'Autorización Especial ID';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Autorización Especial';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.customaccounts_lowae_autorizacion_especial_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID'] = 'Autorización Especial ID';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.customaccounts_lf_facturas_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID'] = 'Autorización Especial ID';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Facturas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Pagos.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Grupo_empresas.php.LowesCRM3500.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Nombre o Razón Social:';
$mod_strings['LBL_ID_AR_CREDIT'] = 'Número de Cuenta Crédito';
$mod_strings['LBL_FACTURACION_AUTOMATICA_C'] = 'Facturación automática:';
$mod_strings['LBL_TYPE'] = 'Tipo de Persona:';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Calle:';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'CP:';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Municipio:';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Estado:';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País:';
$mod_strings['LBL_PHONE_ALT'] = 'Teléfono celular:';
$mod_strings['LBL_PHONE_OFFICE'] = 'Teléfono oficina:';
$mod_strings['LBL_ANY_EMAIL'] = 'Email comercial:';
$mod_strings['LBL_WEBSITE'] = 'Página de internet:';
$mod_strings['LBL_ID_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_ID_VENDEDOR_C'] = 'Vendedor';
$mod_strings['LBL_LIMITE_CREDITO_AUTORIZADO_C'] = 'Límite de crédito autorizado:';
$mod_strings['LBL_PLAZO_PAGO_AUTORIZADO_C'] = 'Plazo de pago autorizado:';
$mod_strings['LBL_DIAS_TOLERANCIA_C'] = 'Días de tolerancia:';
$mod_strings['LBL_ID_LINEA_ANTICIPO_C'] = 'ID Línea de Anticipo:';
$mod_strings['LBL_NUMERO_CUENTA_CLABE_C'] = 'Número de Cuenta Clabe Banamex:';
$mod_strings['LBL_ESTADO_CUENTA_C'] = 'Estado de la cuenta:';
$mod_strings['LBL_ESTADO_BLOQUEO_C'] = 'Estado de bloqueo:';
$mod_strings['LBL_CODIGO_BLOQUEO_C'] = 'Código de bloqueo:';
$mod_strings['LBL_ESTADO_CARTERA_C'] = 'Estado según cartera:';
$mod_strings['LBL_CREDITO_DISPONIBLE_C'] = 'Crédito disponible:';
$mod_strings['LBL_SALDO_DEUDOR_C'] = 'Saldo deudor:';
$mod_strings['LBL_SALDO_PENDIENTE_APLICAR_C'] = 'Saldo pendiente por aplicar:';
$mod_strings['LBL_SALDO_FAVOR_C'] = 'Saldo a favor:';
$mod_strings['LBL_ID_POS_C'] = 'ID POS:';
$mod_strings['LBL_OWNERSHIP'] = 'Nombre del representante Legal:';
$mod_strings['LBL_NOMBRE_AVALA_C'] = 'Nombre del Aval:';
$mod_strings['LBL_NOMBRE_DOCUMENTO_GARANTIA_C'] = 'Nombre Documento Garantía:';
$mod_strings['LBL_MONTO_DOCUMENTO_GARANTIA_C'] = 'Monto de documento garantía:';
$mod_strings['LBL_GRUPO_EMPRESAS_ID'] = 'Grupo de Empresas:';
$mod_strings['LBL_ADMINISTRAR_CREDITO_GRUPAL_C'] = 'Sumar / Administrar Límite de Crédito por Grupo:';
$mod_strings['LBL_ESTADO_GRUPO_C'] = 'Estado del Grupo';
$mod_strings['LBL_TIPO_CUENTA_C'] = 'Tipo de Cuenta';
$mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación:';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_COMENTADIOS_C'] = 'Comentarios';
$mod_strings['LBL_TIPO_DOCUMENTO_C'] = 'Catálogo documento garantía:';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_ACCOUNT_TYPE_C'] = 'Tipo de persona:';
$mod_strings['LBL_BILLING_ADDRESS_NUMBER_C'] = 'Número';
$mod_strings['LBL_BILLING_ADDRESS_COL_C'] = 'Colonia:';
$mod_strings['LBL_BILLING_ADDRESS_STATE_C'] = 'Estado:';
$mod_strings['LBL_OWN_TEL_CEL_C'] = 'Teléfono celular:';
$mod_strings['LBL_OWN_EMAIL_C'] = 'Email:';
$mod_strings['LBL_OWN_RFC_C'] = 'RFC de representante Legal:';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Nuevo Panel 2';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Nuevo Panel 3';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Nuevo Panel 4';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Notas_credito.php.LowesCRM3500.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Pagos.php.LowesCRM3500.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Notas_credito.php.LowesCRM3500.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Grupo_empresas.php.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Grupo_empresas.php.LowesCRM3500.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Nombre o Razón Social:';
$mod_strings['LBL_ID_AR_CREDIT'] = 'Número de Cuenta Crédito';
$mod_strings['LBL_FACTURACION_AUTOMATICA_C'] = 'Facturación automática:';
$mod_strings['LBL_TYPE'] = 'Tipo de Persona:';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Calle:';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'CP:';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Municipio:';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Estado:';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País:';
$mod_strings['LBL_PHONE_ALT'] = 'Teléfono celular:';
$mod_strings['LBL_PHONE_OFFICE'] = 'Teléfono oficina:';
$mod_strings['LBL_ANY_EMAIL'] = 'Email comercial:';
$mod_strings['LBL_WEBSITE'] = 'Página de internet:';
$mod_strings['LBL_ID_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_ID_VENDEDOR_C'] = 'Vendedor';
$mod_strings['LBL_LIMITE_CREDITO_AUTORIZADO_C'] = 'Límite de crédito autorizado:';
$mod_strings['LBL_PLAZO_PAGO_AUTORIZADO_C'] = 'Plazo de pago autorizado:';
$mod_strings['LBL_DIAS_TOLERANCIA_C'] = 'Días de tolerancia:';
$mod_strings['LBL_ID_LINEA_ANTICIPO_C'] = 'ID Línea de Anticipo:';
$mod_strings['LBL_NUMERO_CUENTA_CLABE_C'] = 'Número de Cuenta Clabe Banamex:';
$mod_strings['LBL_ESTADO_CUENTA_C'] = 'Estado de la cuenta:';
$mod_strings['LBL_ESTADO_BLOQUEO_C'] = 'Estado de bloqueo:';
$mod_strings['LBL_CODIGO_BLOQUEO_C'] = 'Código de bloqueo:';
$mod_strings['LBL_ESTADO_CARTERA_C'] = 'Estado según cartera:';
$mod_strings['LBL_CREDITO_DISPONIBLE_C'] = 'Crédito disponible:';
$mod_strings['LBL_SALDO_DEUDOR_C'] = 'Saldo deudor:';
$mod_strings['LBL_SALDO_PENDIENTE_APLICAR_C'] = 'Saldo pendiente por aplicar:';
$mod_strings['LBL_SALDO_FAVOR_C'] = 'Saldo a favor:';
$mod_strings['LBL_ID_POS_C'] = 'ID POS:';
$mod_strings['LBL_OWNERSHIP'] = 'Nombre del representante Legal:';
$mod_strings['LBL_NOMBRE_AVALA_C'] = 'Nombre del Aval:';
$mod_strings['LBL_NOMBRE_DOCUMENTO_GARANTIA_C'] = 'Nombre Documento Garantía:';
$mod_strings['LBL_MONTO_DOCUMENTO_GARANTIA_C'] = 'Monto de documento garantía:';
$mod_strings['LBL_GRUPO_EMPRESAS_ID'] = 'Grupo de Empresas:';
$mod_strings['LBL_ADMINISTRAR_CREDITO_GRUPAL_C'] = 'Sumar / Administrar Límite de Crédito por Grupo:';
$mod_strings['LBL_ESTADO_GRUPO_C'] = 'Estado del Grupo';
$mod_strings['LBL_TIPO_CUENTA_C'] = 'Tipo de Cuenta';
$mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación:';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_COMENTADIOS_C'] = 'Comentarios';
$mod_strings['LBL_TIPO_DOCUMENTO_C'] = 'Catálogo documento garantía:';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_ACCOUNT_TYPE_C'] = 'Tipo de persona:';
$mod_strings['LBL_BILLING_ADDRESS_NUMBER_C'] = 'Número';
$mod_strings['LBL_BILLING_ADDRESS_COL_C'] = 'Colonia:';
$mod_strings['LBL_BILLING_ADDRESS_STATE_C'] = 'Estado:';
$mod_strings['LBL_OWN_TEL_CEL_C'] = 'Teléfono celular:';
$mod_strings['LBL_OWN_EMAIL_C'] = 'Email:';
$mod_strings['LBL_OWN_RFC_C'] = 'RFC de representante Legal:';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Nuevo Panel 2';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Nuevo Panel 3';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Nuevo Panel 4';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Nombre o Razón Social:';
$mod_strings['LBL_ID_AR_CREDIT'] = 'Número de Cuenta Crédito';
$mod_strings['LBL_FACTURACION_AUTOMATICA_C'] = 'Facturación automática:';
$mod_strings['LBL_TYPE'] = 'Tipo de Persona:';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Calle:';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'CP:';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Municipio:';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Estado:';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País:';
$mod_strings['LBL_PHONE_ALT'] = 'Teléfono celular:';
$mod_strings['LBL_PHONE_OFFICE'] = 'Teléfono oficina:';
$mod_strings['LBL_ANY_EMAIL'] = 'Email comercial:';
$mod_strings['LBL_WEBSITE'] = 'Página de internet:';
$mod_strings['LBL_ID_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_ID_VENDEDOR_C'] = 'Vendedor';
$mod_strings['LBL_LIMITE_CREDITO_AUTORIZADO_C'] = 'Límite de crédito autorizado:';
$mod_strings['LBL_PLAZO_PAGO_AUTORIZADO_C'] = 'Plazo de pago autorizado:';
$mod_strings['LBL_DIAS_TOLERANCIA_C'] = 'Días de tolerancia:';
$mod_strings['LBL_ID_LINEA_ANTICIPO_C'] = 'ID Línea de Anticipo:';
$mod_strings['LBL_NUMERO_CUENTA_CLABE_C'] = 'Número de Cuenta Clabe Banamex:';
$mod_strings['LBL_ESTADO_CUENTA_C'] = 'Estado de la cuenta:';
$mod_strings['LBL_ESTADO_BLOQUEO_C'] = 'Estado de bloqueo:';
$mod_strings['LBL_CODIGO_BLOQUEO_C'] = 'Código de bloqueo:';
$mod_strings['LBL_ESTADO_CARTERA_C'] = 'Estado según cartera:';
$mod_strings['LBL_CREDITO_DISPONIBLE_C'] = 'Crédito disponible:';
$mod_strings['LBL_SALDO_DEUDOR_C'] = 'Saldo deudor:';
$mod_strings['LBL_SALDO_PENDIENTE_APLICAR_C'] = 'Saldo pendiente por aplicar:';
$mod_strings['LBL_SALDO_FAVOR_C'] = 'Saldo a favor:';
$mod_strings['LBL_ID_POS_C'] = 'ID POS:';
$mod_strings['LBL_OWNERSHIP'] = 'Nombre del representante Legal:';
$mod_strings['LBL_NOMBRE_AVALA_C'] = 'Nombre del Aval:';
$mod_strings['LBL_NOMBRE_DOCUMENTO_GARANTIA_C'] = 'Nombre Documento Garantía:';
$mod_strings['LBL_MONTO_DOCUMENTO_GARANTIA_C'] = 'Monto de documento garantía:';
$mod_strings['LBL_GRUPO_EMPRESAS_ID'] = 'Grupo de Empresas:';
$mod_strings['LBL_ADMINISTRAR_CREDITO_GRUPAL_C'] = 'Sumar / Administrar Límite de Crédito por Grupo:';
$mod_strings['LBL_ESTADO_GRUPO_C'] = 'Estado del Grupo';
$mod_strings['LBL_TIPO_CUENTA_C'] = 'Tipo de Cuenta';
$mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación:';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_COMENTADIOS_C'] = 'Comentarios';
$mod_strings['LBL_TIPO_DOCUMENTO_C'] = 'Catálogo documento garantía:';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_ACCOUNT_TYPE_C'] = 'Tipo de persona:';
$mod_strings['LBL_BILLING_ADDRESS_NUMBER_C'] = 'Número';
$mod_strings['LBL_BILLING_ADDRESS_COL_C'] = 'Colonia:';
$mod_strings['LBL_BILLING_ADDRESS_STATE_C'] = 'Estado:';
$mod_strings['LBL_OWN_TEL_CEL_C'] = 'Teléfono celular:';
$mod_strings['LBL_OWN_EMAIL_C'] = 'Email:';
$mod_strings['LBL_OWN_RFC_C'] = 'RFC de representante Legal:';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Nuevo Panel 2';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Nuevo Panel 3';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Nuevo Panel 4';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Pagos.php.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Notas_credito.php.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Pagos.php.LowesCRM3500.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';

?>
