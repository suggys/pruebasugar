<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/hu_HU.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_REVENUELINEITEMS'] = 'Bevétel sorokek';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Nombre o Razón Social:';
$mod_strings['LBL_ID_AR_CREDIT'] = 'Número de Cuenta Crédito';
$mod_strings['LBL_FACTURACION_AUTOMATICA_C'] = 'Facturación automática:';
$mod_strings['LBL_TYPE'] = 'Tipo de Persona:';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Calle:';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'CP:';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Municipio:';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Estado:';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País:';
$mod_strings['LBL_PHONE_ALT'] = 'Teléfono celular:';
$mod_strings['LBL_PHONE_OFFICE'] = 'Teléfono oficina:';
$mod_strings['LBL_ANY_EMAIL'] = 'Email comercial:';
$mod_strings['LBL_WEBSITE'] = 'Página de internet:';
$mod_strings['LBL_ID_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_ID_VENDEDOR_C'] = 'Vendedor';
$mod_strings['LBL_LIMITE_CREDITO_AUTORIZADO_C'] = 'Límite de crédito autorizado:';
$mod_strings['LBL_PLAZO_PAGO_AUTORIZADO_C'] = 'Plazo de pago autorizado:';
$mod_strings['LBL_DIAS_TOLERANCIA_C'] = 'Días de tolerancia:';
$mod_strings['LBL_ID_LINEA_ANTICIPO_C'] = 'ID Línea de Anticipo:';
$mod_strings['LBL_NUMERO_CUENTA_CLABE_C'] = 'Número de Cuenta Clabe Banamex:';
$mod_strings['LBL_ESTADO_CUENTA_C'] = 'Estado de la cuenta:';
$mod_strings['LBL_ESTADO_BLOQUEO_C'] = 'Estado de bloqueo:';
$mod_strings['LBL_CODIGO_BLOQUEO_C'] = 'Código de bloqueo:';
$mod_strings['LBL_ESTADO_CARTERA_C'] = 'Estado según cartera:';
$mod_strings['LBL_CREDITO_DISPONIBLE_C'] = 'Crédito disponible:';
$mod_strings['LBL_SALDO_DEUDOR_C'] = 'Saldo deudor:';
$mod_strings['LBL_SALDO_PENDIENTE_APLICAR_C'] = 'Saldo pendiente por aplicar:';
$mod_strings['LBL_SALDO_FAVOR_C'] = 'Saldo a favor:';
$mod_strings['LBL_ID_POS_C'] = 'ID POS:';
$mod_strings['LBL_OWNERSHIP'] = 'Nombre del representante Legal:';
$mod_strings['LBL_NOMBRE_AVALA_C'] = 'Nombre del Aval:';
$mod_strings['LBL_NOMBRE_DOCUMENTO_GARANTIA_C'] = 'Nombre Documento Garantía:';
$mod_strings['LBL_MONTO_DOCUMENTO_GARANTIA_C'] = 'Monto de documento garantía:';
$mod_strings['LBL_GRUPO_EMPRESAS_ID'] = 'Grupo de Empresas:';
$mod_strings['LBL_ADMINISTRAR_CREDITO_GRUPAL_C'] = 'Sumar / Administrar Límite de Crédito por Grupo:';
$mod_strings['LBL_ESTADO_GRUPO_C'] = 'Estado del Grupo';
$mod_strings['LBL_TIPO_CUENTA_C'] = 'Tipo de Cuenta';
$mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación:';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_COMENTADIOS_C'] = 'Comentarios';
$mod_strings['LBL_TIPO_DOCUMENTO_C'] = 'Catálogo documento garantía:';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_ACCOUNT_TYPE_C'] = 'Tipo de persona:';
$mod_strings['LBL_BILLING_ADDRESS_NUMBER_C'] = 'Número';
$mod_strings['LBL_BILLING_ADDRESS_COL_C'] = 'Colonia:';
$mod_strings['LBL_BILLING_ADDRESS_STATE_C'] = 'Estado:';
$mod_strings['LBL_OWN_TEL_CEL_C'] = 'Teléfono celular:';
$mod_strings['LBL_OWN_EMAIL_C'] = 'Email:';
$mod_strings['LBL_OWN_RFC_C'] = 'RFC de representante Legal:';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Nuevo Panel 1';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Nuevo Panel 2';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Nuevo Panel 3';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Nuevo Panel 4';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/lt_LT.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/zh_TW.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ru_RU.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/da_DK.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/uk_UA.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/nl_NL.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/el_EL.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/lv_LV.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/et_EE.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/fi_FI.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/bg_BG.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sr_RS.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_UK.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/cs_CZ.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sv_SE.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ca_ES.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/pt_PT.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/hu_HU.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/pt_BR.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/zh_CN.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/tr_TR.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/he_IL.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ar_SA.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/pl_PL.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ja_JP.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sk_SK.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ro_RO.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/fr_FR.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ko_KR.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_LA.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/nb_NO.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sq_AL.Grupo_empresas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_DATE_ENTERED'] = 'Fecha de creación:';
$mod_strings['LBL_ESTADO_CUENTA_C'] = 'Estado de la Cuenta:';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nuevo Cliente Crédito AR';
$mod_strings['LNK_CREATE'] = 'Crear Compañía';
$mod_strings['LBL_MODULE_NAME'] = 'Clientes Crédito AR';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Cliente Crédito AR';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nueva Cliente Crédito AR';
$mod_strings['LNK_ACCOUNT_LIST'] = 'Visualizar Clientes Crédito AR';
$mod_strings['LNK_ACCOUNT_REPORTS'] = 'Visualizar Informes de Cliente Crédito ARs';
$mod_strings['LNK_IMPORT_ACCOUNTS'] = 'Importar Clientes Crédito AR';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El Cliente Crédito AR que está a punto de crear podría ser un duplicado de otro Cliente Crédito AR existente. Los registros de Cliente Crédito ARs con nombres similares se enumeran a continuación.Haga clic en Guardar para continuar con la creación de esta nuevo Cliente Crédito AR, o en Cancelar para volver al módulo sin crear el Cliente Crédito AR.';
$mod_strings['LBL_SAVE_ACCOUNT'] = 'Guardar Cliente Crédito AR';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Cliente Credito ARs';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Cliente Crédito ARs';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_MEMBER_OF'] = 'Miembro de:';
$mod_strings['LBL_MEMBER_ORG_SUBPANEL_TITLE'] = 'Organizaciones Miembro';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Clientes Crédito AR';
$mod_strings['LBL_ACCOUNT'] = 'Cliente Crédito AR:';
$mod_strings['LBL_BUG_FORM_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_DUPLICATE'] = 'Posible Cliente Crédito AR Duplicada';
$mod_strings['LBL_MODULE_TITLE'] = 'Clientes Crédito AR: Inicio';
$mod_strings['LBL_MODULE_ID'] = 'Clientes Crédito AR';
$mod_strings['LBL_PARENT_ACCOUNT_ID'] = 'ID de Cliente Crédito AR Principal';
$mod_strings['MSG_DUPLICATE'] = 'El Cliente Crédito AR que está a punto de crear podría ser un duplicado de un Cliente Crédito AR ya existente. Los Cliente Crédito AR con nombres similares se incluyen a continuación.Haga clic en Crear Cliente Crédito AR para continuar con la creación de este nuevo Cliente Crédito AR, o seleccione un Cliente Crédito AR existente de la lista a continuación.';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Tipo de Cliente Crédito AR';
$mod_strings['LBL_BILLING_ADDRESS_COL_C'] = 'Colonia';
$mod_strings['LBL_BILLING_ADDRESS_NUMBER_C'] = 'Número';
$mod_strings['LBL_BILLING_ADDRESS_STATE_C'] = 'Estado:';
$mod_strings['LBL_ESTADO_BLOQUEO_C'] = 'Estado de Bloqueo:';
$mod_strings['LBL_CODIGO_BLOQUEO_C'] = 'Código de Bloqueo:';
$mod_strings['LBL_ESTADO_CARTERA_C'] = 'Estado según Cartera:';
$mod_strings['LBL_COMENTADIOS_C'] = 'Comentarios:';
$mod_strings['LBL_TIPO_DOCUMENTO_C'] = 'Catálogo documento garantía';
$mod_strings['LBL_ACCOUNT_TYPE_C'] = 'Tipo de persona:';
$mod_strings['LBL_ADMINISTRAR_CREDITO_GRUPAL_C'] = 'Sumar / Administrar Límite de Crédito por Grupo:';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_CREDITO_DISPONIBLE_C'] = 'Crédito Disponible:';
$mod_strings['LBL_DIAS_TOLERANCIA_C'] = 'Días de Tolerancia:';
$mod_strings['LBL_FACTURACION_AUTOMATICA_C'] = 'Facturación Automática:';
$mod_strings['LBL_GRUPO_EMPRESAS_ID_C'] = 'Grupo de empresas:';
$mod_strings['LBL_ID_AR_CREDIT'] = 'Número de cuenta crédito:';
$mod_strings['LBL_ID_LINEA_ANTICIPO_C'] = 'ID Línea de Anticipo:';
$mod_strings['LBL_ID_POS_C'] = 'ID POS:';
$mod_strings['LBL_ID_SUCURSAL_C'] = 'Sucursal:';
$mod_strings['LBL_ID_VENDEDOR_C'] = 'Vendedor:';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_LIMITE_CREDITO_AUTORIZADO_C'] = 'Límite de Crédito Autorizado:';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_MONTO_DOCUMENTO_GARANTIA_C'] = 'Monto de Documento Garantia:';
$mod_strings['LBL_NOMBRE_AVALA_C'] = 'Nombre del Aval:';
$mod_strings['LBL_NUMERO_CUENTA_CLABE_C'] = 'Número de Cuenta Clabe Banamex:';
$mod_strings['LBL_OWNERSHIP'] = 'Nombre del Aval:';
$mod_strings['LBL_RFC_C'] = 'RFC:';
$mod_strings['LBL_PLAZO_PAGO_AUTORIZADO_C'] = 'Plazo de Pago Autorizado:';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_SALDO_DEUDOR_C'] = 'Saldo Deudor:';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_SALDO_FAVOR_C'] = 'Saldo a Favor:';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_SALDO_PENDIENTE_APLICAR_C'] = 'Saldo Pendiente por Aplicar:';
$mod_strings['LBL_TIPO_CUENTA_C'] = 'Tipo de cuenta:';
$mod_strings['LBL_RECORDVIEW_PANEL5'] = 'Datos de Identifiación';
$mod_strings['LBL_RECORDVIEW_PANEL7'] = 'Nuevo Panel 7';
$mod_strings['LBL_NOMBRE_DOCUMENTO_GARANTIA_C_DOCUMENT_ID'] = 'Nombre documento garantia: (relacionado Documento ID)';
$mod_strings['LBL_NOMBRE_DOCUMENTO_GARANTIA_C'] = 'Nombre Documento Garantia:';
$mod_strings['LBL_RECORDVIEW_PANEL8'] = 'Nuevo Panel 8';
$mod_strings['LBL_RECORDVIEW_PANEL9'] = '';
$mod_strings['LBL_RECORDVIEW_PANEL10'] = '';
$mod_strings['LBL_RECORDVIEW_PANEL11'] = '';
$mod_strings['LBL_RECORDVIEW_PANEL12'] = '';
$mod_strings['LBL_NAME'] = 'Nombre o Razón Social:';
$mod_strings['LBL_PHONE_OFFICE'] = 'Teléfono de Oficina';
$mod_strings['LBL_PHONE_ALT'] = 'Teléfono Celular:';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_6'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_7'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_8'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_9'] = 'LBL_CURRENCY';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet:';
$mod_strings['LBL_DESCRIPTION'] = 'Comentarios:';
$mod_strings['LBL_CURRENCY_10'] = 'LBL_CURRENCY';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Calle:';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Estado:';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'C.P.:';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País:';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Municipio:';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número:';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia:';
$mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de modificación:';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Saldos Crédito AR';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Documento Garantía';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información Adicional';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Condiciones Créditicias';
$mod_strings['LBL_ID_CAJERO_C'] = 'Cajero:';
$mod_strings['LBL_TIPO_TELEFONO_PRINCIPAL_C'] = 'Tipo de Teléfono:';
$mod_strings['LBL_ISBUSINESSCUSTOMER_C'] = 'Cliente de Negocios :';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET_C'] = 'Calle:';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE_C'] = 'Estado:';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE_C'] = 'C. P.:';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY_C'] = 'Municipio:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY_C'] = 'País';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_C_CONTACT_ID'] = 'Nombre del Representante Legal: (relacionado Contacto ID)';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_C'] = 'Nombre del Representante Legal:';
$mod_strings['LBL_ID_AR_CREDIT_C'] = 'ID AR Credit';
$mod_strings['LBL_ESTADO_INTERFAZ_POS_C'] = 'Estado de Envío a POS :';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de empresas';
$mod_strings['LBL_TIPO_TELEFONO_PRINCIPAL'] = 'Tipo de Teléfono Principal';
$mod_strings['LBL_ENVIAR_POS_C'] = 'Enviar a POS :';
$mod_strings['LBL_CREDITO_DISPONIBLE_FLEXIBLE'] = 'Crédito Disponible Flexible :';
$mod_strings['LBL_CALCULA_ID_LINEA_CREDITO'] = 'Caldula ID Línea de Anticipo';
$mod_strings['LBL_CREDITO_DIPONIBLE_GRUPO_C'] = 'Crédito Disponible de Grupo :';
$mod_strings['LBL_LIMITE_CREDITO_GRUPO_C'] = 'Límite de Crédito de Grupo :';
$mod_strings['LBL_SALDO_DEUDOR_GPE_EMPRESAS_C'] = 'Saldo Deudor :';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Saldos Grupo de Empresas';
$mod_strings['LBL_TOTAL_ORDENES_C'] = 'Total de ordenes :';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/uk_UA.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ja_JP.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sv_SE.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/lt_LT.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_UK.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/hu_HU.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/pt_PT.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/zh_TW.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/et_EE.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ko_KR.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/nb_NO.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ar_SA.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/fi_FI.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sr_RS.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_LA.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ru_RU.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/lv_LV.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/cs_CZ.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/el_EL.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/zh_CN.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sk_SK.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/bg_BG.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/pl_PL.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/he_IL.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/pt_BR.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/nl_NL.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ca_ES.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sq_AL.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/fr_FR.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ro_RO.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/da_DK.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/tr_TR.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ar_SA.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/uk_UA.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/de_DE.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/et_EE.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/fi_FI.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/zh_CN.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/fr_FR.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/da_DK.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/cs_CZ.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sk_SK.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ko_KR.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_us.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ro_RO.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ja_JP.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/zh_TW.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/en_UK.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_LA.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ru_RU.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/ca_ES.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/it_it.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/hu_HU.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sr_RS.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/lv_LV.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sq_AL.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/el_EL.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/bg_BG.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/nl_NL.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/pt_PT.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/sv_SE.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/pt_BR.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/tr_TR.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/nb_NO.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/lt_LT.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/he_IL.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/pl_PL.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Language/es_ES.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';

?>
