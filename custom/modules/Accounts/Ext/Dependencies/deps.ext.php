<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/gpe_grupo_empresas_accounts_name_deps.php

$dependencies['Accounts']['gpe_grupo_empresas_accounts_name'] = array(
  'hooks' => array("edit","view"),
  'triggerFields' => array('id_ar_credit_c'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'gpe_grupo_empresas_accounts_name',
        'value' => 'not(equal($id_ar_credit_c,""))',
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/hide_panel_10_deps.php

$dependencies['Accounts']['panel_10_visibility'] = array(
        'hooks' => array("edit","view"),
        'trigger' => 'not(equal($id_ar_credit_c, ""))',
        'triggerFields' => array('id_ar_credit_c'),
        'onload' => true,
        //Actions is a list of actions to fire when the trigger is true
        'actions' => array(
            array(
                'name' => 'SetPanelVisibility',
                'params' => array(
                    'target' => 'LBL_RECORDVIEW_PANEL10',
                    'value' => 'true',
                ),
            )
        ),
        //notActions is a list of actions to fire when the trigger is false
        'notActions' => array(
            array(
                'name' => 'SetPanelVisibility',
                'params' => array(
                    'target' => 'LBL_RECORDVIEW_PANEL10',
                    'value' => 'false',
                ),
            ),
        ),
    );

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/estado_bloqueo_c_deps.php


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/hide_panel_14_deps.php

$dependencies['Accounts']['panel_14_visibility'] = array(
        'hooks' => array("edit","view"),
        'trigger' => 'not(equal($id_ar_credit_c, ""))',
        'triggerFields' => array('id_ar_credit_c'),
        'onload' => true,
        //Actions is a list of actions to fire when the trigger is true
        'actions' => array(
            array(
                'name' => 'SetPanelVisibility',
                'params' => array(
                    'target' => 'LBL_RECORDVIEW_PANEL14',
                    'value' => 'true',
                ),
            )
        ),
        //notActions is a list of actions to fire when the trigger is false
        'notActions' => array(
            array(
                'name' => 'SetPanelVisibility',
                'params' => array(
                    'target' => 'LBL_RECORDVIEW_PANEL14',
                    'value' => 'false',
                ),
            ),
        ),
    );

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/nombre_representante_c_deps.php


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Dependencies/sin_orden_xdias_c_deps.php

// Convirtiendo campo sin_orden_xdias_c como solo lectura siempre
$dependencies['Accounts']['sin_orden_xdias_c'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'sin_orden_xdias_c',
              'value' => 'true',
          ),
      ),
    ),
);

?>
