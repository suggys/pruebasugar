<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_lowae_autorizacion_especial_1_Accounts.php

// created: 2016-10-13 23:48:34
$dictionary["Account"]["fields"]["accounts_lowae_autorizacion_especial_1"] = array (
  'name' => 'accounts_lowae_autorizacion_especial_1',
  'type' => 'link',
  'relationship' => 'accounts_lowae_autorizacion_especial_1',
  'source' => 'non-db',
  'module' => 'lowae_autorizacion_especial',
  'bean_name' => 'lowae_autorizacion_especial',
  'vname' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_lf_facturas_1_Accounts.php

// created: 2016-10-24 13:40:01
$dictionary["Account"]["fields"]["accounts_lf_facturas_1"] = array (
  'name' => 'accounts_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'accounts_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_lf_facturas_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/accounts_orden_ordenes_1_Accounts.php

// created: 2016-10-11 21:41:46
$dictionary["Account"]["fields"]["accounts_orden_ordenes_1"] = array (
  'name' => 'accounts_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'accounts_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'vname' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_orden_ordenes_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/AccountsVisibility.php

$dictionary['Account']['visibility']["AccountsVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_phone_office.php

 // created: 2016-09-26 11:52:40
$dictionary['Account']['fields']['phone_office']['len']='100';
$dictionary['Account']['fields']['phone_office']['massupdate']=false;
$dictionary['Account']['fields']['phone_office']['comments']='The office phone number';
$dictionary['Account']['fields']['phone_office']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['phone_office']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_office']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.05',
  'searchable' => true,
);
$dictionary['Account']['fields']['phone_office']['calculated']=false;
$dictionary['Account']['fields']['phone_office']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_own_email_c.php

 // created: 2016-09-13 20:32:46
$dictionary['Account']['fields']['own_email_c']['labelValue']='Email:';
$dictionary['Account']['fields']['own_email_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['own_email_c']['enforced']='';
$dictionary['Account']['fields']['own_email_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_own_tel_cel_c.php

 // created: 2016-09-13 20:30:52
$dictionary['Account']['fields']['own_tel_cel_c']['labelValue']='Teléfono celular:';
$dictionary['Account']['fields']['own_tel_cel_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['own_tel_cel_c']['enforced']='';
$dictionary['Account']['fields']['own_tel_cel_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/newFilterDuplicateCheck.php

$dictionary['Account']['duplicate_check']['FilterDuplicateCheck'] = array(
    'filter_template' => array(
        array(
            '$or' => array(
                array('id_ar_credit_c' => array('$equals' => '$id_ar_credit_c')),
                array('id_pos_c' => array('$equals' => '$id_pos_c')),
                array('id_linea_anticipo_c' => array('$equals' => '$id_linea_anticipo_c')),
            )
        ),
    ),
    'ranking_fields' => array(
        array('in_field_name' => 'id_ar_credit_c', 'dupe_field_name' => 'id_ar_credit_c'),
        array('in_field_name' => 'id_pos_c', 'dupe_field_name' => 'id_pos_c'),
        array('in_field_name' => 'id_linea_anticipo_c', 'dupe_field_name' => 'id_linea_anticipo_c'),
    )
);
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/prospects_accounts_1_Accounts.php

// created: 2017-07-20 22:48:10
$dictionary["Account"]["fields"]["prospects_accounts_1"] = array (
  'name' => 'prospects_accounts_1',
  'type' => 'link',
  'relationship' => 'prospects_accounts_1',
  'source' => 'non-db',
  'module' => 'Prospects',
  'bean_name' => 'Prospect',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE',
  'id_name' => 'prospects_accounts_1prospects_ida',
);
$dictionary["Account"]["fields"]["prospects_accounts_1_name"] = array (
  'name' => 'prospects_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE',
  'save' => true,
  'id_name' => 'prospects_accounts_1prospects_ida',
  'link' => 'prospects_accounts_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["prospects_accounts_1prospects_ida"] = array (
  'name' => 'prospects_accounts_1prospects_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE_ID',
  'id_name' => 'prospects_accounts_1prospects_ida',
  'link' => 'prospects_accounts_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_contract_id_c.php

 // created: 2016-09-22 18:22:06

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_comentadios_c.php

 // created: 2016-09-13 20:00:05
$dictionary['Account']['fields']['comentadios_c']['labelValue']='Comentarios';
$dictionary['Account']['fields']['comentadios_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['comentadios_c']['enforced']='';
$dictionary['Account']['fields']['comentadios_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_bloqueante_c.php

 // created: 2017-01-13 22:46:16
$dictionary['Account']['fields']['bloqueante_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_estado_grupo_c.php

 // created: 2016-09-13 16:58:25
$dictionary['Account']['fields']['estado_grupo_c']['labelValue']='Estado del Grupo';
$dictionary['Account']['fields']['estado_grupo_c']['dependency']='';
$dictionary['Account']['fields']['estado_grupo_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_ownership.php

 // created: 2018-03-06 00:27:44
$dictionary['Account']['fields']['ownership']['len']='100';
$dictionary['Account']['fields']['ownership']['audited']=true;
$dictionary['Account']['fields']['ownership']['massupdate']=false;
$dictionary['Account']['fields']['ownership']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['ownership']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['ownership']['merge_filter']='disabled';
$dictionary['Account']['fields']['ownership']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['ownership']['calculated']=false;
$dictionary['Account']['fields']['ownership']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_col_c.php

 // created: 2016-09-13 20:21:47
$dictionary['Account']['fields']['billing_address_col_c']['labelValue']='Colonia:';
$dictionary['Account']['fields']['billing_address_col_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_col_c']['enforced']='';
$dictionary['Account']['fields']['billing_address_col_c']['dependency']='';
$dictionary['Account']['fields']['billing_address_col_c']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Account']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_date_modified.php

 // created: 2017-01-22 16:20:24
$dictionary['Account']['fields']['date_modified']['audited']=false;
$dictionary['Account']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Account']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Account']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Account']['fields']['date_modified']['calculated']=false;
$dictionary['Account']['fields']['date_modified']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_description.php

 // created: 2017-08-02 22:46:38
$dictionary['Account']['fields']['description']['audited']=true;
$dictionary['Account']['fields']['description']['massupdate']=false;
$dictionary['Account']['fields']['description']['comments']='Full text of the note';
$dictionary['Account']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['description']['merge_filter']='disabled';
$dictionary['Account']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.72',
  'searchable' => true,
);
$dictionary['Account']['fields']['description']['calculated']=false;
$dictionary['Account']['fields']['description']['rows']='6';
$dictionary['Account']['fields']['description']['cols']='80';
$dictionary['Account']['fields']['description']['dependency']='and(equal($codigo_bloqueo_c,"Otro"), equal($estado_bloqueo_c,"Bloqueado"))';
$dictionary['Account']['fields']['description']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/low01_solicitudescredito_accounts_Accounts.php

// created: 2017-07-21 12:47:08
$dictionary["Account"]["fields"]["low01_solicitudescredito_accounts"] = array (
  'name' => 'low01_solicitudescredito_accounts',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_accounts',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'low01_solicitudescredito_accountsaccounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_administrar_credito_grupal_c.php

 // created: 2016-09-14 22:34:45
$dictionary['Account']['fields']['administrar_credito_grupal_c']['labelValue']='Sumar / Administrar Límite de Crédito por Grupo:';
$dictionary['Account']['fields']['administrar_credito_grupal_c']['enforced']='';
$dictionary['Account']['fields']['administrar_credito_grupal_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/rli_link_workflow.php

$dictionary['Account']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_tipo_documento_c.php

 // created: 2016-09-13 20:12:46
$dictionary['Account']['fields']['tipo_documento_c']['labelValue']='Catálogo documento garantía:';
$dictionary['Account']['fields']['tipo_documento_c']['dependency']='';
$dictionary['Account']['fields']['tipo_documento_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_street.php

 // created: 2017-07-14 19:21:15
$dictionary['Account']['fields']['billing_address_street']['audited']=false;
$dictionary['Account']['fields']['billing_address_street']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_street']['comments']='The street address used for billing address';
$dictionary['Account']['fields']['billing_address_street']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_street']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.35',
  'searchable' => true,
);
$dictionary['Account']['fields']['billing_address_street']['calculated']=false;
$dictionary['Account']['fields']['billing_address_street']['rows']='4';
$dictionary['Account']['fields']['billing_address_street']['cols']='20';
$dictionary['Account']['fields']['billing_address_street']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_date_entered.php

 // created: 2016-09-23 09:41:18
$dictionary['Account']['fields']['date_entered']['audited']=false;
$dictionary['Account']['fields']['date_entered']['comments']='Date record created';
$dictionary['Account']['fields']['date_entered']['importable']='false';
$dictionary['Account']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Account']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Account']['fields']['date_entered']['calculated']=false;
$dictionary['Account']['fields']['date_entered']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_email.php

 // created: 2016-09-26 11:57:33
$dictionary['Account']['fields']['email']['len']='100';
$dictionary['Account']['fields']['email']['audited']=true;
$dictionary['Account']['fields']['email']['massupdate']=true;
$dictionary['Account']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['email']['merge_filter']='disabled';
$dictionary['Account']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.89',
  'searchable' => true,
);
$dictionary['Account']['fields']['email']['calculated']=false;
$dictionary['Account']['fields']['email']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_tipo_cuenta_c.php

 // created: 2016-09-26 18:15:28
$dictionary['Account']['fields']['tipo_cuenta_c']['labelValue']='Tipo de Cuenta';
$dictionary['Account']['fields']['tipo_cuenta_c']['dependency']='';
$dictionary['Account']['fields']['tipo_cuenta_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_city.php

 // created: 2017-07-14 19:19:22
$dictionary['Account']['fields']['billing_address_city']['audited']=false;
$dictionary['Account']['fields']['billing_address_city']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_city']['comments']='The city used for billing address';
$dictionary['Account']['fields']['billing_address_city']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_city']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_city']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_id_vendedor_c.php

 // created: 2016-09-13 19:50:18
$dictionary['Account']['fields']['id_vendedor_c']['labelValue']='Vendedor';
$dictionary['Account']['fields']['id_vendedor_c']['dependency']='';
$dictionary['Account']['fields']['id_vendedor_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/nc_notas_credito_accounts_Accounts.php

// created: 2016-10-12 00:13:37
$dictionary["Account"]["fields"]["nc_notas_credito_accounts"] = array (
  'name' => 'nc_notas_credito_accounts',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_accounts',
  'source' => 'non-db',
  'module' => 'NC_Notas_credito',
  'bean_name' => false,
  'vname' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'nc_notas_credito_accountsaccounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_country.php

 // created: 2017-07-14 19:13:07
$dictionary['Account']['fields']['billing_address_country']['audited']=false;
$dictionary['Account']['fields']['billing_address_country']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_country']['comments']='The country used for the billing address';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_country']['calculated']=false;
$dictionary['Account']['fields']['billing_address_country']['type']='enum';
$dictionary['Account']['fields']['billing_address_country']['options']='primary_address_country_c_list';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_nombre_avala_c.php

 // created: 2016-09-14 22:28:31
$dictionary['Account']['fields']['nombre_avala_c']['labelValue']='Nombre del Aval:';
$dictionary['Account']['fields']['nombre_avala_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['nombre_avala_c']['enforced']='';
$dictionary['Account']['fields']['nombre_avala_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_website.php

 // created: 2016-09-13 20:28:41
$dictionary['Account']['fields']['website']['len']='255';
$dictionary['Account']['fields']['website']['audited']=false;
$dictionary['Account']['fields']['website']['massupdate']=false;
$dictionary['Account']['fields']['website']['comments']='URL of website for the company';
$dictionary['Account']['fields']['website']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['website']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['website']['merge_filter']='disabled';
$dictionary['Account']['fields']['website']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['website']['calculated']=false;
$dictionary['Account']['fields']['website']['gen']='';
$dictionary['Account']['fields']['website']['link_target']='_self';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_postalcode.php

 // created: 2017-07-14 19:12:26
$dictionary['Account']['fields']['billing_address_postalcode']['audited']=false;
$dictionary['Account']['fields']['billing_address_postalcode']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_postalcode']['comments']='The postal code used for billing address';
$dictionary['Account']['fields']['billing_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_postalcode']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_postalcode']['calculated']=false;
$dictionary['Account']['fields']['billing_address_postalcode']['len']='5';
$dictionary['Account']['fields']['billing_address_postalcode']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_account_type.php

 // created: 2016-09-22 18:08:40
$dictionary['Account']['fields']['account_type']['len']=100;
$dictionary['Account']['fields']['account_type']['audited']=false;
$dictionary['Account']['fields']['account_type']['massupdate']=true;
$dictionary['Account']['fields']['account_type']['comments']='The Company is of this type';
$dictionary['Account']['fields']['account_type']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['account_type']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['account_type']['merge_filter']='disabled';
$dictionary['Account']['fields']['account_type']['calculated']=false;
$dictionary['Account']['fields']['account_type']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/lowes_pagos_accounts_Accounts.php

// created: 2016-10-22 15:12:51
$dictionary["Account"]["fields"]["lowes_pagos_accounts"] = array (
  'name' => 'lowes_pagos_accounts',
  'type' => 'link',
  'relationship' => 'lowes_pagos_accounts',
  'source' => 'non-db',
  'module' => 'lowes_Pagos',
  'bean_name' => 'lowes_Pagos',
  'vname' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'lowes_pagos_accountsaccounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_state.php

 // created: 2017-07-14 22:55:42
$dictionary['Account']['fields']['billing_address_state']['audited']=false;
$dictionary['Account']['fields']['billing_address_state']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_state']['comments']='The state used for billing address';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_state']['calculated']=false;
$dictionary['Account']['fields']['billing_address_state']['type']='enum';
$dictionary['Account']['fields']['billing_address_state']['options']='state_c_list';
$dictionary['Account']['fields']['billing_address_state']['visibility_grid']=array (
  'trigger' => 'billing_address_country',
  'values' =>
  array (
    'US' =>
    array (
      0 => 'OTRO',
    ),
    'CA' =>
    array (
      0 => 'OTRO',
    ),
    'FR' =>
    array (
      0 => 'OTRO',
    ),
    'MX' =>
    array (
      0 => '',
      1 => 'AGU',
      2 => 'BCN',
      3 => 'BCS',
      4 => 'CAM',
      5 => 'CHP',
      6 => 'CHH',
      7 => 'CMX',
      8 => 'COA',
      9 => 'COL',
      10 => 'DUR',
      11 => 'MEX',
      12 => 'GUA',
      13 => 'GRO',
      14 => 'HID',
      15 => 'JAL',
      16 => 'MIC',
      17 => 'MOR',
      18 => 'NAY',
      19 => 'NLE',
      20 => 'OAX',
      21 => 'PUE',
      22 => 'QUE',
      23 => 'ROO',
      24 => 'SLP',
      25 => 'SIN',
      26 => 'SON',
      27 => 'TAB',
      28 => 'TAM',
      29 => 'TLA',
      30 => 'VER',
      31 => 'YUC',
      32 => 'ZAC',
    ),
    'EU' =>
    array (
      0 => 'OTRO',
    ),
    'DE' =>
    array (
      0 => 'OTRO',
    ),
    'GB' =>
    array (
      0 => 'OTRO',
    ),
    'JP' =>
    array (
      0 => 'OTRO',
    ),
    'PR' =>
    array (
      0 => 'OTRO',
    ),
  ),
);
$dictionary['Account']['fields']['billing_address_state']['len']=100;
$dictionary['Account']['fields']['billing_address_state']['dependency']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_cod_bloqueo_aux_c.php

 // created: 2017-01-13 22:46:16
$dictionary['Account']['fields']['cod_bloqueo_aux_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['cod_bloqueo_aux_c']['labelValue']='LBL_COD_BLOQUEAO_AUX';
$dictionary['Account']['fields']['cod_bloqueo_aux_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['cod_bloqueo_aux_c']['enforced']='';
$dictionary['Account']['fields']['cod_bloqueo_aux_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_phone_alternate.php

 // created: 2016-09-27 21:52:05
$dictionary['Account']['fields']['phone_alternate']['len']='100';
$dictionary['Account']['fields']['phone_alternate']['audited']=true;
$dictionary['Account']['fields']['phone_alternate']['massupdate']=false;
$dictionary['Account']['fields']['phone_alternate']['comments']='An alternate phone number';
$dictionary['Account']['fields']['phone_alternate']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['phone_alternate']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['phone_alternate']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_alternate']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.03',
  'searchable' => true,
);
$dictionary['Account']['fields']['phone_alternate']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_own_rfc_c.php

 // created: 2016-09-13 20:33:50
$dictionary['Account']['fields']['own_rfc_c']['labelValue']='RFC de representante Legal:';
$dictionary['Account']['fields']['own_rfc_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['own_rfc_c']['enforced']='';
$dictionary['Account']['fields']['own_rfc_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/gpe_grupo_empresas_accounts_Accounts.php

// created: 2016-09-14 15:26:16
$dictionary["Account"]["fields"]["gpe_grupo_empresas_accounts"] = array (
  'name' => 'gpe_grupo_empresas_accounts',
  'type' => 'link',
  'relationship' => 'gpe_grupo_empresas_accounts',
  'source' => 'non-db',
  'module' => 'GPE_Grupo_empresas',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["gpe_grupo_empresas_accounts_name"] = array (
  'name' => 'gpe_grupo_empresas_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE',
  'save' => true,
  'id_name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
  'link' => 'gpe_grupo_empresas_accounts',
  'table' => 'gpe_grupo_empresas',
  'module' => 'GPE_Grupo_empresas',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["gpe_grupo_empresas_accountsgpe_grupo_empresas_ida"] = array (
  'name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
  'link' => 'gpe_grupo_empresas_accounts',
  'table' => 'gpe_grupo_empresas',
  'module' => 'GPE_Grupo_empresas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_name.php

 // created: 2018-05-11 17:29:05
$dictionary['Account']['fields']['name']['len']='80';
$dictionary['Account']['fields']['name']['massupdate']=false;
$dictionary['Account']['fields']['name']['comments']='Name of the Company';
$dictionary['Account']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['name']['merge_filter']='disabled';
$dictionary['Account']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.91',
  'searchable' => true,
);
$dictionary['Account']['fields']['name']['calculated']=false;
$dictionary['Account']['fields']['name']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_state_c.php

 // created: 2016-09-27 21:36:05
$dictionary['Account']['fields']['billing_address_state_c']['labelValue']='Estado:';
$dictionary['Account']['fields']['billing_address_state_c']['dependency']='';
$dictionary['Account']['fields']['billing_address_state_c']['visibility_grid']='';
$dictionary['Account']['fields']['billing_address_state_c']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_alt_address_country_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['alt_address_country_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['alt_address_country_c']['labelValue']='País Dirección Alterna';
$dictionary['Account']['fields']['alt_address_country_c']['dependency']='';
$dictionary['Account']['fields']['alt_address_country_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_account_type_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['account_type_c']['labelValue']='Tipo de persona:';
$dictionary['Account']['fields']['account_type_c']['dependency']='';
$dictionary['Account']['fields']['account_type_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_aprueba_oc_o_cot_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['aprueba_oc_o_cot_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_alt_address_numero_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['alt_address_numero_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['alt_address_numero_c']['labelValue']='Número Dirección Alterna';
$dictionary['Account']['fields']['alt_address_numero_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['alt_address_numero_c']['enforced']='';
$dictionary['Account']['fields']['alt_address_numero_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_alt_address_state_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['alt_address_state_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['alt_address_state_c']['labelValue']='Estado Dirección Alterna';
$dictionary['Account']['fields']['alt_address_state_c']['dependency']='';
$dictionary['Account']['fields']['alt_address_state_c']['visibility_grid']=array (
  'trigger' => 'alt_address_country_c',
  'values' => 
  array (
    'US' => 
    array (
      0 => 'OTRO',
    ),
    'CA' => 
    array (
      0 => 'OTRO',
    ),
    'FR' => 
    array (
      0 => 'OTRO',
    ),
    'MX' => 
    array (
      0 => '',
      1 => 'AGU',
      2 => 'BCN',
      3 => 'BCS',
      4 => 'CAM',
      5 => 'CHP',
      6 => 'CHH',
      7 => 'CMX',
      8 => 'COA',
      9 => 'COL',
      10 => 'DUR',
      11 => 'MEX',
      12 => 'GUA',
      13 => 'GRO',
      14 => 'HID',
      15 => 'JAL',
      16 => 'MIC',
      17 => 'MOR',
      18 => 'NAY',
      19 => 'NLE',
      20 => 'OAX',
      21 => 'PUE',
      22 => 'QUE',
      23 => 'ROO',
      24 => 'SLP',
      25 => 'SIN',
      26 => 'SON',
      27 => 'TAB',
      28 => 'TAM',
      29 => 'TLA',
      30 => 'VER',
      31 => 'YUC',
      32 => 'ZAC',
    ),
    'EU' => 
    array (
      0 => 'OTRO',
    ),
    'DE' => 
    array (
      0 => 'OTRO',
    ),
    'GB' => 
    array (
      0 => 'OTRO',
    ),
    'JP' => 
    array (
      0 => 'OTRO',
    ),
    'PR' => 
    array (
      0 => 'OTRO',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_alt_address_city_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['alt_address_city_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['alt_address_city_c']['labelValue']='Municipio Dirección Alterna';
$dictionary['Account']['fields']['alt_address_city_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['alt_address_city_c']['enforced']='';
$dictionary['Account']['fields']['alt_address_city_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2018-05-14 04:05:37

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_alt_address_colonia_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['alt_address_colonia_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['alt_address_colonia_c']['labelValue']='Colonia Dirección Alterna';
$dictionary['Account']['fields']['alt_address_colonia_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['alt_address_colonia_c']['enforced']='';
$dictionary['Account']['fields']['alt_address_colonia_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_alt_address_postalcode_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['alt_address_postalcode_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['alt_address_postalcode_c']['labelValue']='C. P. Dirección Alterna';
$dictionary['Account']['fields']['alt_address_postalcode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['alt_address_postalcode_c']['enforced']='';
$dictionary['Account']['fields']['alt_address_postalcode_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_alt_address_other_state_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['alt_address_other_state_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['alt_address_other_state_c']['labelValue']='Otro Estado Dirección Alterna';
$dictionary['Account']['fields']['alt_address_other_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['alt_address_other_state_c']['enforced']='';
$dictionary['Account']['fields']['alt_address_other_state_c']['dependency']='equal($alt_address_state_c,"OTRO")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_alt_address_street_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['alt_address_street_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['alt_address_street_c']['labelValue']='Calle Dirección Alterna';
$dictionary['Account']['fields']['alt_address_street_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['alt_address_street_c']['enforced']='';
$dictionary['Account']['fields']['alt_address_street_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_banco_factoraje_c.php

 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['banco_factoraje_c']['labelValue']='Banco de Factoraje';
$dictionary['Account']['fields']['banco_factoraje_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['banco_factoraje_c']['enforced']='';
$dictionary['Account']['fields']['banco_factoraje_c']['dependency']='equal($con_factoraje_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_numero_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['billing_address_numero_c']['labelValue']='billing address numero c';
$dictionary['Account']['fields']['billing_address_numero_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_numero_c']['enforced']='';
$dictionary['Account']['fields']['billing_address_numero_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_cuenta_patron_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['cuenta_patron_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['cuenta_patron_c']['labelValue']='Cuenta del Patrón';
$dictionary['Account']['fields']['cuenta_patron_c']['enforced']='';
$dictionary['Account']['fields']['cuenta_patron_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_credito_disponible_flexible_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['credito_disponible_flexible_c']['labelValue']='Crédito Disponible Flexible :';
$dictionary['Account']['fields']['credito_disponible_flexible_c']['enforced']='';
$dictionary['Account']['fields']['credito_disponible_flexible_c']['dependency']='';
$dictionary['Account']['fields']['credito_disponible_flexible_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_colonia_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['billing_address_colonia_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['billing_address_colonia_c']['labelValue']='Colonia Dirección Fiscal';
$dictionary['Account']['fields']['billing_address_colonia_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_colonia_c']['enforced']='';
$dictionary['Account']['fields']['billing_address_colonia_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_con_factoraje_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['con_factoraje_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_codigo_bloqueo_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['codigo_bloqueo_c']['labelValue']='Código de Bloqueo:';
$dictionary['Account']['fields']['codigo_bloqueo_c']['dependency']='equal($estado_bloqueo_c,"Bloqueado")';
$dictionary['Account']['fields']['codigo_bloqueo_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_club_pro_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['club_pro_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['club_pro_c']['labelValue']='Cuenta con Tarjeta Club PRO';
$dictionary['Account']['fields']['club_pro_c']['enforced']='';
$dictionary['Account']['fields']['club_pro_c']['dependency']='equal($tipo_cliente2_c,"especialista")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_credito_disponible_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['credito_disponible_c']['labelValue']='Crédito disponible:';
$dictionary['Account']['fields']['credito_disponible_c']['enforced']='';
$dictionary['Account']['fields']['credito_disponible_c']['dependency']='';
$dictionary['Account']['fields']['credito_disponible_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_contacto_pagos_konesh_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['contacto_pagos_konesh_c']['labelValue']='Contacto de Pagos Konesh';
$dictionary['Account']['fields']['contacto_pagos_konesh_c']['dependency']='not(equal($id_ar_credit_c,""))';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_other_state_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['billing_address_other_state_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['billing_address_other_state_c']['labelValue']='Otro Estado Dirección Fiscal';
$dictionary['Account']['fields']['billing_address_other_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_other_state_c']['enforced']='';
$dictionary['Account']['fields']['billing_address_other_state_c']['dependency']='equal($billing_address_state,"OTRO")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2018-05-14 04:05:38

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_calcula_id_linea_credito_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['calcula_id_linea_credito_c']['labelValue']='Calcula ID linea de crédito';
$dictionary['Account']['fields']['calcula_id_linea_credito_c']['enforced']='';
$dictionary['Account']['fields']['calcula_id_linea_credito_c']['dependency']='ifElse(greaterThan(strlen($id_linea_anticipo_c),0),false,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_contact_id1_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['contact_id1_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_competidores_directos_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['competidores_directos_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['competidores_directos_c']['labelValue']='Competidores Directos';
$dictionary['Account']['fields']['competidores_directos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['competidores_directos_c']['enforced']='';
$dictionary['Account']['fields']['competidores_directos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_billing_address_number_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['billing_address_number_c']['labelValue']='Número Dirección Fiscal';
$dictionary['Account']['fields']['billing_address_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_number_c']['enforced']='';
$dictionary['Account']['fields']['billing_address_number_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_credito_diponible_grupo_c.php

 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['credito_diponible_grupo_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['credito_diponible_grupo_c']['labelValue']='Crédito Disponible de Grupo :';
$dictionary['Account']['fields']['credito_diponible_grupo_c']['calculated']='1';
$dictionary['Account']['fields']['credito_diponible_grupo_c']['formula']='related($gpe_grupo_empresas_accounts,"credito_diponible_grupo_c")';
$dictionary['Account']['fields']['credito_diponible_grupo_c']['enforced']='1';
$dictionary['Account']['fields']['credito_diponible_grupo_c']['dependency']='greaterThan(strlen(related($gpe_grupo_empresas_accounts,"id")),1)';
$dictionary['Account']['fields']['credito_diponible_grupo_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_dias_tolerancia_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['dias_tolerancia_c']['labelValue']='Días de tolerancia:';
$dictionary['Account']['fields']['dias_tolerancia_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['dias_tolerancia_c']['enforced']='';
$dictionary['Account']['fields']['dias_tolerancia_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_folio_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['folio_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['folio_c']['labelValue']='Folio';
$dictionary['Account']['fields']['folio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['folio_c']['enforced']='';
$dictionary['Account']['fields']['folio_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_estado_cartera_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['estado_cartera_c']['labelValue']='Estado según cartera:';
$dictionary['Account']['fields']['estado_cartera_c']['dependency']='';
$dictionary['Account']['fields']['estado_cartera_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_id_linea_anticipo_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['id_linea_anticipo_c']['labelValue']='ID Línea de anticipo:';
$dictionary['Account']['fields']['id_linea_anticipo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['id_linea_anticipo_c']['enforced']='';
$dictionary['Account']['fields']['id_linea_anticipo_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_importado_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['importado_c']['labelValue']='Importado';
$dictionary['Account']['fields']['importado_c']['enforced']='';
$dictionary['Account']['fields']['importado_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_id_cajero_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['id_cajero_c']['labelValue']='Cajero:';
$dictionary['Account']['fields']['id_cajero_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['id_cajero_c']['enforced']='';
$dictionary['Account']['fields']['id_cajero_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_id_pos_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['id_pos_c']['labelValue']='ID POS:';
$dictionary['Account']['fields']['id_pos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['id_pos_c']['enforced']='';
$dictionary['Account']['fields']['id_pos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_document_id_c.php

 // created: 2018-05-14 04:05:39

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_giro_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['giro_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['giro_c']['labelValue']='Giro / Industria';
$dictionary['Account']['fields']['giro_c']['dependency']='';
$dictionary['Account']['fields']['giro_c']['visibility_grid']=array (
  'trigger' => 'tipo_cliente2_c',
  'values' => 
  array (
    1 => 
    array (
    ),
    2 => 
    array (
    ),
    3 => 
    array (
    ),
    '' => 
    array (
    ),
    'comercial' => 
    array (
      0 => 'arquitecto',
      1 => 'construccion_vertical',
      2 => 'contratista',
      3 => 'decorador',
      4 => 'desarrollador_de_vivienda_residencial',
      5 => 'ingeniero',
      6 => 'mtto_industrial',
      7 => 'obra_civil',
      8 => 'proyecto_temporal_del_hogar',
      9 => 'urbanizador',
      10 => 'otro',
    ),
    'especialista' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_facturacion_automatica_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['facturacion_automatica_c']['labelValue']='Facturación automática:';
$dictionary['Account']['fields']['facturacion_automatica_c']['enforced']='';
$dictionary['Account']['fields']['facturacion_automatica_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_estado_cuenta_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['estado_cuenta_c']['labelValue']='Estado de la cuenta:';
$dictionary['Account']['fields']['estado_cuenta_c']['dependency']='';
$dictionary['Account']['fields']['estado_cuenta_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2018-05-14 04:05:39

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_estado_bloqueo_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['estado_bloqueo_c']['labelValue']='Estado de Bloqueo:';
$dictionary['Account']['fields']['estado_bloqueo_c']['dependency']='';
$dictionary['Account']['fields']['estado_bloqueo_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_id_sucursal_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['id_sucursal_c']['labelValue']='Sucursal';
$dictionary['Account']['fields']['id_sucursal_c']['dependency']='';
$dictionary['Account']['fields']['id_sucursal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_id_ar_credit_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['id_ar_credit_c']['labelValue']='ID AR Credit:';
$dictionary['Account']['fields']['id_ar_credit_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['id_ar_credit_c']['enforced']='';
$dictionary['Account']['fields']['id_ar_credit_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_enviar_pos_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['enviar_pos_c']['labelValue']='Enviar a POS :';
$dictionary['Account']['fields']['enviar_pos_c']['enforced']='';
$dictionary['Account']['fields']['enviar_pos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_estado_interfaz_pos_c.php

 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['estado_interfaz_pos_c']['labelValue']='Estado de envio a POS :';
$dictionary['Account']['fields']['estado_interfaz_pos_c']['dependency']='';
$dictionary['Account']['fields']['estado_interfaz_pos_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_monto_documento_garantia_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['monto_documento_garantia_c']['labelValue']='Monto de documento garantía:';
$dictionary['Account']['fields']['monto_documento_garantia_c']['enforced']='';
$dictionary['Account']['fields']['monto_documento_garantia_c']['dependency']='';
$dictionary['Account']['fields']['monto_documento_garantia_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_nombre_del_aval_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['nombre_del_aval_c']['labelValue']='Nombre del aval';
$dictionary['Account']['fields']['nombre_del_aval_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['nombre_del_aval_c']['enforced']='';
$dictionary['Account']['fields']['nombre_del_aval_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_numero_club_pro_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['numero_club_pro_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['numero_club_pro_c']['labelValue']='Número de Tarjeta Club PRO';
$dictionary['Account']['fields']['numero_club_pro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['numero_club_pro_c']['enforced']='';
$dictionary['Account']['fields']['numero_club_pro_c']['dependency']='equal($club_pro_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_numero_cuenta_clabe_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['numero_cuenta_clabe_c']['labelValue']='Referencia Banamex';
$dictionary['Account']['fields']['numero_cuenta_clabe_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['numero_cuenta_clabe_c']['enforced']='';
$dictionary['Account']['fields']['numero_cuenta_clabe_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_limite_credito_autorizado_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['limite_credito_autorizado_c']['labelValue']='Límite de crédito autorizado:';
$dictionary['Account']['fields']['limite_credito_autorizado_c']['enforced']='';
$dictionary['Account']['fields']['limite_credito_autorizado_c']['dependency']='';
$dictionary['Account']['fields']['limite_credito_autorizado_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_nombre_documento_garantia_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['nombre_documento_garantia_c']['labelValue']='Nombre documento garantía:';
$dictionary['Account']['fields']['nombre_documento_garantia_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_num_cta_clabe_banamex_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['num_cta_clabe_banamex_c']['labelValue']='Número de Cuenta CLABE Banamex';
$dictionary['Account']['fields']['num_cta_clabe_banamex_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['num_cta_clabe_banamex_c']['enforced']='';
$dictionary['Account']['fields']['num_cta_clabe_banamex_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_limite_credito_grupo_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['limite_credito_grupo_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['limite_credito_grupo_c']['labelValue']='Límite de Crédito de Grupo :';
$dictionary['Account']['fields']['limite_credito_grupo_c']['calculated']='1';
$dictionary['Account']['fields']['limite_credito_grupo_c']['formula']='related($gpe_grupo_empresas_accounts,"limite_credito_grupo_c")';
$dictionary['Account']['fields']['limite_credito_grupo_c']['enforced']='1';
$dictionary['Account']['fields']['limite_credito_grupo_c']['dependency']='greaterThan(strlen(related($gpe_grupo_empresas_accounts,"id")),1)';
$dictionary['Account']['fields']['limite_credito_grupo_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_otro_giro_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['otro_giro_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['otro_giro_c']['labelValue']='Otro Giro / Industria';
$dictionary['Account']['fields']['otro_giro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['otro_giro_c']['enforced']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_isbusinesscustomer_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['isbusinesscustomer_c']['labelValue']='Cliente de Negocios :';
$dictionary['Account']['fields']['isbusinesscustomer_c']['enforced']='';
$dictionary['Account']['fields']['isbusinesscustomer_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_oficio_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['oficio_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['oficio_c']['labelValue']='Oficio';
$dictionary['Account']['fields']['oficio_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_metodo_pago_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['metodo_pago_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['metodo_pago_c']['labelValue']='Método de Pago';
$dictionary['Account']['fields']['metodo_pago_c']['dependency']='';
$dictionary['Account']['fields']['metodo_pago_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_nombre_representante_c.php

 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['nombre_representante_c']['labelValue']='Nombre del Representante Legal:';
$dictionary['Account']['fields']['nombre_representante_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_saldo_deudor_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['saldo_deudor_c']['labelValue']='Saldo deudor:';
$dictionary['Account']['fields']['saldo_deudor_c']['enforced']='';
$dictionary['Account']['fields']['saldo_deudor_c']['dependency']='';
$dictionary['Account']['fields']['saldo_deudor_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_rfc_factoraje_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['rfc_factoraje_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['rfc_factoraje_c']['dependency']='equal($con_factoraje_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_primary_address_other_state_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['primary_address_other_state_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['primary_address_other_state_c']['labelValue']='Otro Estado';
$dictionary['Account']['fields']['primary_address_other_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['primary_address_other_state_c']['enforced']='';
$dictionary['Account']['fields']['primary_address_other_state_c']['dependency']='equal($primary_address_state_c,"OTRO")';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_primary_address_street_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['primary_address_street_c']['labelValue']='Calle:';
$dictionary['Account']['fields']['primary_address_street_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['primary_address_street_c']['enforced']='';
$dictionary['Account']['fields']['primary_address_street_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_primary_address_country_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['primary_address_country_c']['labelValue']='País:';
$dictionary['Account']['fields']['primary_address_country_c']['dependency']='';
$dictionary['Account']['fields']['primary_address_country_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_primary_address_city_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['primary_address_city_c']['labelValue']='Municipio:';
$dictionary['Account']['fields']['primary_address_city_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['primary_address_city_c']['enforced']='';
$dictionary['Account']['fields']['primary_address_city_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_otro_oficio_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['otro_oficio_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['otro_oficio_c']['labelValue']='Otro Oficio';
$dictionary['Account']['fields']['otro_oficio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['otro_oficio_c']['enforced']='';
$dictionary['Account']['fields']['otro_oficio_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_primary_address_postalcode_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['primary_address_postalcode_c']['labelValue']='C. P.';
$dictionary['Account']['fields']['primary_address_postalcode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['primary_address_postalcode_c']['enforced']='';
$dictionary['Account']['fields']['primary_address_postalcode_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_rfc_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['rfc_c']['labelValue']='RFC';
$dictionary['Account']['fields']['rfc_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['rfc_c']['enforced']='';
$dictionary['Account']['fields']['rfc_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_saldo_pendiente_aplicar_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['saldo_pendiente_aplicar_c']['labelValue']='Saldo pendiente por aplicar:';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_c']['enforced']='';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_c']['dependency']='';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_primary_address_colonia_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['primary_address_colonia_c']['labelValue']='Colonia:';
$dictionary['Account']['fields']['primary_address_colonia_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['primary_address_colonia_c']['enforced']='';
$dictionary['Account']['fields']['primary_address_colonia_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_saldo_deudor_gpe_empresas_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['labelValue']='Saldo Deudor de Grupo';
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['calculated']='1';
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['formula']='related($gpe_grupo_empresas_accounts,"saldo_deudor_c")';
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['enforced']='1';
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['dependency']='greaterThan(strlen(related($gpe_grupo_empresas_accounts,"id")),1)';
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_primary_address_numero_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['primary_address_numero_c']['labelValue']='Número:';
$dictionary['Account']['fields']['primary_address_numero_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['primary_address_numero_c']['enforced']='';
$dictionary['Account']['fields']['primary_address_numero_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_primary_address_state_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['primary_address_state_c']['labelValue']='Estado:';
$dictionary['Account']['fields']['primary_address_state_c']['dependency']='';
$dictionary['Account']['fields']['primary_address_state_c']['visibility_grid']=array (
  'trigger' => 'primary_address_country_c',
  'values' => 
  array (
    'US' => 
    array (
      0 => 'OTRO',
    ),
    'CA' => 
    array (
      0 => 'OTRO',
    ),
    'FR' => 
    array (
      0 => 'OTRO',
    ),
    'MX' => 
    array (
      0 => '',
      1 => 'AGU',
      2 => 'BCN',
      3 => 'BCS',
      4 => 'CAM',
      5 => 'CHP',
      6 => 'CHH',
      7 => 'CMX',
      8 => 'COA',
      9 => 'COL',
      10 => 'DUR',
      11 => 'MEX',
      12 => 'GUA',
      13 => 'GRO',
      14 => 'HID',
      15 => 'JAL',
      16 => 'MIC',
      17 => 'MOR',
      18 => 'NAY',
      19 => 'NLE',
      20 => 'OAX',
      21 => 'PUE',
      22 => 'QUE',
      23 => 'ROO',
      24 => 'SLP',
      25 => 'SIN',
      26 => 'SON',
      27 => 'TAB',
      28 => 'TAM',
      29 => 'TLA',
      30 => 'VER',
      31 => 'YUC',
      32 => 'ZAC',
    ),
    'EU' => 
    array (
      0 => 'OTRO',
    ),
    'DE' => 
    array (
      0 => 'OTRO',
    ),
    'GB' => 
    array (
      0 => 'OTRO',
    ),
    'JP' => 
    array (
      0 => 'OTRO',
    ),
    'PR' => 
    array (
      0 => 'OTRO',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_plazo_pago_autorizado_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['plazo_pago_autorizado_c']['labelValue']='Plazo de pago autorizado:';
$dictionary['Account']['fields']['plazo_pago_autorizado_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['plazo_pago_autorizado_c']['enforced']='';
$dictionary['Account']['fields']['plazo_pago_autorizado_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_saldo_favor_c.php

 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['saldo_favor_c']['labelValue']='Saldo a favor:';
$dictionary['Account']['fields']['saldo_favor_c']['enforced']='';
$dictionary['Account']['fields']['saldo_favor_c']['dependency']='';
$dictionary['Account']['fields']['saldo_favor_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_user_vendedor_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['user_vendedor_c']['labelValue']='LBL_USER_VENDEDOR_C';
$dictionary['Account']['fields']['user_vendedor_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_vol_ventas_anual_est_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['vol_ventas_anual_est_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['vol_ventas_anual_est_c']['labelValue']='Volumen de Ventas Anuales Estimadas';
$dictionary['Account']['fields']['vol_ventas_anual_est_c']['enforced']='';
$dictionary['Account']['fields']['vol_ventas_anual_est_c']['dependency']='';
$dictionary['Account']['fields']['vol_ventas_anual_est_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_tipo_cliente_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['tipo_cliente_c']['labelValue']='Tipo de Cliente';
$dictionary['Account']['fields']['tipo_cliente_c']['dependency']='';
$dictionary['Account']['fields']['tipo_cliente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_tipo_telefono_principal_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['tipo_telefono_principal_c']['labelValue']='Tipo de Teléfono :';
$dictionary['Account']['fields']['tipo_telefono_principal_c']['dependency']='';
$dictionary['Account']['fields']['tipo_telefono_principal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_user_id_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['user_id_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_saldo_pendiente_aplicar_gpo_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['labelValue']='Saldo Pendiente por Aplicar de Grupo';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['calculated']='1';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['formula']='related($gpe_grupo_empresas_accounts,"saldo_pendiente_aplicar_c")';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['enforced']='1';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['dependency']='greaterThan(strlen(related($gpe_grupo_empresas_accounts,"id")),1)';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_sin_orden_xdias_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['sin_orden_xdias_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['sin_orden_xdias_c']['labelValue']='Sin orden 120 días';
$dictionary['Account']['fields']['sin_orden_xdias_c']['enforced']='';
$dictionary['Account']['fields']['sin_orden_xdias_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_tipo_cliente2_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['tipo_cliente2_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['tipo_cliente2_c']['labelValue']='Tipo Cliente';
$dictionary['Account']['fields']['tipo_cliente2_c']['dependency']='';
$dictionary['Account']['fields']['tipo_cliente2_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_subtipo_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['subtipo_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['subtipo_c']['labelValue']='Subtipo';
$dictionary['Account']['fields']['subtipo_c']['dependency']='';
$dictionary['Account']['fields']['subtipo_c']['visibility_grid']=array (
  'trigger' => 'tipo_cliente2_c',
  'values' => 
  array (
    1 => 
    array (
    ),
    2 => 
    array (
    ),
    3 => 
    array (
    ),
    '' => 
    array (
    ),
    'comercial' => 
    array (
      0 => '',
      1 => 'mostrador',
      2 => 'campo',
    ),
    'especialista' => 
    array (
      0 => '',
      1 => 'mostrador',
      2 => 'campo',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_total_ordenes_c.php

 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['total_ordenes_c']['labelValue']='total ordenes';
$dictionary['Account']['fields']['total_ordenes_c']['enforced']='';
$dictionary['Account']['fields']['total_ordenes_c']['dependency']='';
$dictionary['Account']['fields']['total_ordenes_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
