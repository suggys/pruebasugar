<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/DesbloqueoCuentaCredito_GrupoEmpresas.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : [];
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : [];
$hook_array['before_save'][] = array(
    7,
    'Desbloqueo de Clientes Crédito AR pertenecientes a un Grupo de Empresas',
    'custom/modules/Accounts/DesbloqueoCuentaCredito_GrupoEmpresas.php',
    'DesbloqueoCuentaCredito_GrupoEmpresas',
    'desbloqueoCuentaCreditoGrupoEmpresas'
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/BloqCta_agrega_GpoEmp.php

	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
    $hook_array['after_relationship_add'][] = array(
        1,
        'Bloqueo de Clientes Crédito AR al agregar una cta al Grupo de empresas',
        'custom/modules/Accounts/BloqCta_agrega_GpoEmp.php',
        'BloqCta_agrega_GpoEmpClass',
        'BloqCta_agrega_GpoEmp'
        );

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/GeneraID_AR_Credit.php

// Obsoleto

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/LHCalculaSaldoCuenta.php

	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
    $hook_array['after_relationship_add'][] = array(
        8,
        'Calcula el saldo pendiente al relacionar un pago',
        'custom/modules/Accounts/CalculaSaldoCuentaPendiente.php',
        'CalculaSaldoCuenta',
        'calculaSaldoPendient'
        );
    $hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
    $hook_array['after_relationship_delete'][] = array(
        8,
        'Calcula el saldo pendiente al relacionar un pago',
        'custom/modules/Accounts/CalculaSaldoCuentaPendiente.php',
        'CalculaSaldoCuenta',
        'calculaSaldoPendient'
        );

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/CalculaSaldoDeudorGrupoEmpresas.php

	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        2,
        'Calculo de saldo deudor Grupo de Empresas',
        'custom/modules/Accounts/AccountsLogicHooksManager.php',
        'AccountsLogicHooksManager',
        'calculaSaldoDeudorGrupoEmpresas'
    );

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/LH_GeneraID_LineaCredito.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(3, 'Calcula ID linea de credito de las cuentas', 'custom/modules/Accounts/GeneraID_LineaCredito.php', 'GeneraID_LineaCredito','GeneraID_LineaCredito');

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/CalculaSaldosGpoEmpresas.php

/*
	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        3,
        'Calculo de saldos al guardar Cuenta',
        'custom/modules/Accounts/AccountsLogicHooksManager.php',
        'AccountsLogicHooksManager',
        'calculaSaldosAlGuardarCta'
    );
	*/

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/CalculaSaldoCuentaPorFacturas.php

	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
    $hook_array['after_relationship_add'][] = array(
        2,
        'Calculo de saldo deudor de Empresas a partir de facturas',
        'custom/modules/Accounts/CalculaSaldoCuentaPorFacturas.php',
        'CalculaSaldoCuentaPorFacturas',
        'calculaSaldoDeudor'
    );
    $hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
    $hook_array['after_relationship_delete'][] = array(
        2,
        'Calculo de saldo deudor de Empresas a partir de facturas',
        'custom/modules/Accounts/CalculaSaldoCuentaPorFacturas.php',
        'CalculaSaldoCuentaPorFacturas',
        'calculaSaldoDeudor'
    );

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/LH_Calcula_Lim_cred_Gpo_Empresas.php


?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/BloqCta_quita_GpoEmp.php

	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
    $hook_array['after_relationship_delete'][] = array(
        1,
        'Bloqueo de Clientes Crédito AR al quitar una cta al Grupo de empresas',
        'custom/modules/Accounts/BloqCta_quita_GpoEmp.php',
        'BloqCta_quita_GpoEmpClass',
        'BloqCta_quita_GpoEmp'
        );

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/BloqueoCuentaAR_por_LE.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
    5,
    'FR-11.7. Bloqueo por LE – Limite de crédito excedido',
    'custom/modules/Accounts/BloqueoCuentaAR_por_LE.php',
    'BloqueoCuentaAR_por_LE',
    'BloqueoCuentaAR_por_LE'
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/BloqueoCuentaCredito_GrupoEmpresas.php

	$hook_version = 1;
    $hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        1,
        'Bloqueo de Clientes Crédito AR pertenecientes a un Grupo de Empresas',
        'custom/modules/Accounts/BloqueoCuentasCreditoGrupoEmpresas.php',
        'BloqueoCuentasCreditoGrupoEmpresas',
        'bloqueoCuentaCreditoGrupoEmpresas'
    );
    $hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
    $hook_array['before_save'][] = array(
        1,
        'save fetched row',
        'custom/modules/Accounts/BloqueoCuentasCreditoGrupoEmpresas.php',
        'BloqueoCuentasCreditoGrupoEmpresas',
        'saveFetchedRow'
    );

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/LH_GeneraFolio.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
  6,
  'Calcula Folio de cuentas',
  'custom/modules/Accounts/AccountsLogicHooksManager.php',
  'AccountsLogicHooksManager',
  'generaFolio'
);

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/LH_ActivaClienteSinOrdenes.php

	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
    $hook_array['after_relationship_add'][] = array(
        9,
        'Activar clientes sin ordenes al relacinar una orden, campo sin_orden_xdias_c',
        'custom/modules/Accounts/ActivaClienteSinOrdenes.php',
        'ActivaClienteSinOrdenes',
        'ActivaCliente'
        );

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/CalculoCreditoDisponibleFlexible.php

	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
    $hook_array['before_save'][] = array(
        4,
        'Calculo credito disponible flexible',
        'custom/modules/Accounts/AccountsLogicHooksManager.php',
        'AccountsLogicHooksManager',
        'calculaCreditDisponibleFlexible'
    );

?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/LogicHooks/LH_GeneraIDPOS.php


$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(6, 'Genera IDPOS al guardar la Cuenta', 'custom/modules/Accounts/GeneraIDPOS.php', 'GeneraIDPOS','GeneraIDPOS');

?>
