<?php
// created: 2016-12-22 17:38:21
$subpanel_layout['list_fields'] = array (
  'id_pos_c' =>
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_ID_POS_C',
    'width' => '10%',
  ),
  'id_ar_credit_c' =>
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_ID_AR_CREDIT_C',
    'width' => '10%',
  ),
  'name' =>
  array (
    'vname' => 'LBL_LIST_ACCOUNT_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'account_type_c' =>
  array (
    'type' => 'radioenum',
    'default' => true,
    'vname' => 'LBL_ACCOUNT_TYPE_C',
    'width' => '10%',
  ),
  'rfc_c' =>
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_RFC_C',
    'width' => '10%',
  ),
  'limite_credito_autorizado_c' =>
  array (
    'related_fields' =>
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_LIMITE_CREDITO_AUTORIZADO_C',
    'currency_format' => true,
    'width' => '10%',
    'readonly' => true,
  ),
  'credito_disponible_c' =>
  array (
    'related_fields' =>
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_CREDITO_DISPONIBLE_C',
    'currency_format' => true,
    'width' => '10%',
    'readonly' => true,
  ),
  'credito_disponible_flexible_c' =>
  array (
    'related_fields' =>
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_CREDITO_DISPONIBLE_FLEXIBLE',
    'currency_format' => true,
    'width' => '10%',
    'readonly' => true,
  ),
  'saldo_pendiente_aplicar_c' =>
  array (
    'related_fields' =>
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_SALDO_PENDIENTE_APLICAR_C',
    'currency_format' => true,
    'width' => '10%',
    'readonly' => true,
  ),
  'saldo_deudor_c' =>
  array (
    'related_fields' =>
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_SALDO_DEUDOR_C',
    'currency_format' => true,
    'width' => '10%',
    'readonly' => true,
  ),
  'estado_cuenta_c' =>
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ESTADO_CUENTA_C',
    'width' => '10%',
  ),
  'estado_bloqueo_c' =>
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ESTADO_BLOQUEO_C',
    'width' => '10%',
    'readonly' => true,
  ),
  'codigo_bloqueo_c' =>
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_CODIGO_BLOQUEO_C',
    'width' => '10%',
    'readonly' => true,
  ),
);
