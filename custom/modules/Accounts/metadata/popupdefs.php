<?php
$popupMeta = array (
    'moduleMain' => 'Account',
    'varName' => 'ACCOUNT',
    'orderBy' => 'name',
    'whereClauses' => array (
  'name' => 'accounts.name',
  'id_pos_c' => 'accounts_cstm.id_pos_c',
  'id_ar_credit_c' => 'accounts_cstm.id_ar_credit_c',
  'id_linea_anticipo_c' => 'accounts_cstm.id_linea_anticipo_c',
  'rfc_c' => 'accounts_cstm.rfc_c',
  'id_sucursal_c' => 'accounts_cstm.id_sucursal_c',
  'email' => 'accounts.email',
  'assigned_user_id' => 'accounts.assigned_user_id',
),
    'searchInputs' => array (
  0 => 'name',
  3 => 'id_pos_c',
  4 => 'id_ar_credit_c',
  5 => 'id_linea_anticipo_c',
  6 => 'rfc_c',
  7 => 'id_sucursal_c',
  8 => 'email',
  9 => 'assigned_user_id',
),
    'create' => array (
  'formBase' => 'AccountFormBase.php',
  'formBaseClass' => 'AccountFormBase',
  'getFormBodyParams' => 
  array (
    0 => '',
    1 => '',
    2 => 'AccountSave',
  ),
  'createButton' => 'LNK_NEW_ACCOUNT',
),
    'searchdefs' => array (
  'id_pos_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ID_POS_C',
    'width' => '10%',
    'name' => 'id_pos_c',
  ),
  'id_ar_credit_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ID_AR_CREDIT_C',
    'width' => '10%',
    'name' => 'id_ar_credit_c',
  ),
  'id_linea_anticipo_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ID_LINEA_ANTICIPO_C',
    'width' => '10%',
    'name' => 'id_linea_anticipo_c',
  ),
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'rfc_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_RFC_C',
    'width' => '10%',
    'name' => 'rfc_c',
  ),
  'id_sucursal_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_ID_SUCURSAL_C',
    'width' => '10%',
    'name' => 'id_sucursal_c',
  ),
  'email' => 
  array (
    'name' => 'email',
    'width' => '10%',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'ID_POS_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_ID_POS_C',
    'width' => '10%',
    'name' => 'id_pos_c',
  ),
  'ID_LINEA_ANTICIPO_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_ID_LINEA_ANTICIPO_C',
    'width' => '10%',
    'name' => 'id_linea_anticipo_c',
  ),
  'ID_AR_CREDIT_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_ID_AR_CREDIT_C',
    'width' => '10%',
    'name' => 'id_ar_credit_c',
  ),
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_ACCOUNT_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'ACCOUNT_TYPE_C' => 
  array (
    'type' => 'radioenum',
    'default' => true,
    'label' => 'LBL_ACCOUNT_TYPE_C',
    'width' => '10%',
    'name' => 'account_type_c',
  ),
  'RFC_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_RFC_C',
    'width' => '10%',
    'name' => 'rfc_c',
  ),
  'LIMITE_CREDITO_AUTORIZADO_C' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_LIMITE_CREDITO_AUTORIZADO_C',
    'currency_format' => true,
    'width' => '10%',
    'name' => 'limite_credito_autorizado_c',
  ),
  'GPE_GRUPO_EMPRESAS_ACCOUNTS_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE',
    'id' => 'GPE_GRUPO_EMPRESAS_ACCOUNTSGPE_GRUPO_EMPRESAS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'gpe_grupo_empresas_accounts_name',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'PRIMARY_ADDRESS_STATE_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_PRIMARY_ADDRESS_STATE_C',
    'width' => '10%',
    'name' => 'primary_address_state_c',
  ),
  'NUMERO_CUENTA_CLABE_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_NUMERO_CUENTA_CLABE_C',
    'width' => '10%',
    'name' => 'numero_cuenta_clabe_c',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_modified',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_entered',
  ),
),
);
