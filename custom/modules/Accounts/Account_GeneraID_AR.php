<?php
require_once 'custom/modules/MRX1_Configuraciones/CustomMRX1Configuraciones.php';
/*
*Al crearse una cuenta Cliente Crédito AR nueva, SugarCRM deberá
*generar el ""ID AR Credit"", compuesto de la siguiente manera:_x000D_
*80012301+ número_tienda(4 digitos) + homoclave(4 digitos)._x000D_
*La homoclave generada debe ser diferente a la del ""ID Línea de Anticipo""
*/

class Account_GeneraID_AR{
    public $isDebug = true;

    function getNewIdArCredit($bean){

      $PREX_ACCOUNT_ID    = "80012301";
      $sucursal_id = $bean->id_sucursal_c;
      $id_ar              = "";
      $homo_clave         = "";
      $configKey   = "suc".$sucursal_id;

      $configuratorObj = new CustomMRX1Configuraciones();
      $configuratorObj->retrieveSettings();
      $isNewIndex = false;

      if(array_key_exists($configKey,$configuratorObj->settings)){
        $homo_clave = $configuratorObj->settings[$configKey]*1;
      }
      else{
        $homo_clave = 0;
      }

      while(!$isNewIndex){
        $id_ar = $PREX_ACCOUNT_ID . $sucursal_id .  sprintf('%04d', $homo_clave);
        if(!$this->isUsed($id_ar)){
          $isNewIndex = true;
        }
        $homo_clave++;
      }

      $configuratorObj->saveSetting($configKey, $homo_clave);
      return $id_ar;
    }

    public function isUsed($idArCredit)
    {
      $bean = BeanFactory::newBean('Accounts');
      $sugarQuery = new SugarQuery();
      $sugarQuery->from($bean, array('team_security'=>false));
      $sugarQuery->where()
        ->equals('id_ar_credit_c', $idArCredit);
      $sugarQuery->select(array('id'));

      $records = $sugarQuery->execute();
      return count($records) === 1;
    }
}
?>
