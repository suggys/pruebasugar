<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class BloqueoCuentasCreditoGrupoEmpresas
{
  protected static $fetchedRow = array();

  public function saveFetchedRow($bean, $event, $arguments)
  {
    if(isset($bean->importado_c) && $bean->importado_c){
      return true;
    }
    if ( !empty($bean->id) ) {
      self::$fetchedRow[$bean->id] = $bean->fetched_row;
    }
  }

  public function bloqueoCuentaCreditoGrupoEmpresas($bean, $event, $arguments)
  {
    if(isset($bean->importado_c) && $bean->importado_c){
      return true;
    }

    if($bean->noBloqueante) return false;
    if (
      isset($arguments['isUpdate'])
      && $arguments['isUpdate'] == 1
      && $bean->estado_cuenta_c == "Activo"
      && $bean->estado_bloqueo_c == "Bloqueado"
      && (
        self::$fetchedRow[$bean->id]
        && self::$fetchedRow[$bean->id]['codigo_bloqueo_c'] != $bean->codigo_bloqueo_c
      )
      && !empty($bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida)
      ) {
        $gpe_empresa = BeanFactory::getBean('GPE_Grupo_empresas', $bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida);
        if($gpe_empresa->administrar_credito_grupal){
          if($gpe_empresa->load_relationship('gpe_grupo_empresas_accounts')){
            $cuentas = $gpe_empresa->gpe_grupo_empresas_accounts->getBeans();
            foreach($cuentas as $cuenta) {

              if ($cuenta->id != $bean->id ){
                $cuenta->estado_bloqueo_c = 'Bloqueado';
                $cuenta->codigo_bloqueo_c = $bean->codigo_bloqueo_c;
                if(
                  $bean->estado_bloqueo_c == "Bloqueado"
                  && $bean->codigo_bloqueo_c == "Otro"
                  && !empty($bean->description)
                ){
                  $cuenta->description = $bean->description;
                }
                $cuenta->noBloqueante = 1;
                $cuenta->save(false);
              }
            }
          }
        }
      }
    }
  }
  ?>
