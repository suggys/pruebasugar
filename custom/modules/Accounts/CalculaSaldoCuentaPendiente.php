<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

    class CalculaSaldoCuenta
    {
        public function calculaSaldoPendient($bean, $event, $arguments){


            if(isset($bean->importado_c) && $bean->importado_c){
                return true;
            }

            if( $arguments['related_module'] == 'lowes_Pagos' && isset($arguments['related_id']) && ($event=="after_relationship_add" || $event=="after_relationship_delete") )
            {
                $pago = BeanFactory::getBean('lowes_Pagos',$arguments['related_id']);
                $fieldIndex = "id_ar_credit_c";
                if($pago->id && $pago->numero_cuenta_clabe_c){
                  $fieldIndex = !$pago->linea_anticipo_c ? "id_ar_credit_c" : "id_linea_anticipo_c";
                }
                else{
                  $fieldIndex = substr($pago->id_customer_c, -4) < 7000 ? "id_ar_credit_c" : "id_linea_anticipo_c";
                }
                if($pago->id && $fieldIndex === 'id_ar_credit_c'){
                  if($event=="after_relationship_add"){
                    $this->aplicaPagoPromesaPago($bean, $pago);
                    $bean->saldo_pendiente_aplicar_c += $pago->monto;
                    $bean->credito_disponible_c += $pago->monto;
                  }
                  // else{
                  //   $this->desaplicaPagoPromesaPago($pago);
                  //   $bean->saldo_pendiente_aplicar_c -= $pago->monto;
                  //   $bean->credito_disponible_c -= $pago->monto;
                  // }

                  if($bean->credito_disponible_c > $bean->limite_credito_autorizado_c){
                    $bean->credito_disponible_c = $bean->limite_credito_autorizado_c;
                  }
                  $bean->save(false);
                  $this->aplicaDesbloqueo($bean);
                }
                if($event=="after_relationship_add" && $pago->id && $fieldIndex === 'id_linea_anticipo_c'){
  								$bean->saldo_favor_c += $pago->monto;
                  $bean->save(false);

          				$pago->saldo_aplicado_c  = $pago->monto;
          				$pago->saldo_pendiente_c = 0;
                  $pago->save(false);
                }
            }
        }

        public function aplicaPagoPromesaPago($account, $pago){
          $promesasPago = $this->getPromesaPago($account);
          if($pago->id && $pago->deleted == 0 && !is_null($promesasPago)){
            $saldo_pendiente_c = $pago->saldo_pendiente_c;
            foreach ($promesasPago as $key => $pp) {
              if($saldo_pendiente_c > 0) {
              $promesaPago = BeanFactory::getBean('lowae_autorizacion_especial',$pp['id']);
              $pagoSaldoPendientePago = $promesaPago->monto_promesa_c - $promesaPago->monto_abonado_c;
              $diff = floatval($saldo_pendiente_c) <= floatval($pagoSaldoPendientePago) ? $saldo_pendiente_c : $pagoSaldoPendientePago;
              $promesaPago->monto_abonado_c += $diff;
              $saldo_pendiente_c -= $diff;

              if(floatval($promesaPago->monto_abonado_c) == floatval($promesaPago->monto_promesa_c)
              && $promesaPago->estado_promesa_c === 'vigente'){
                $promesaPago->estado_promesa_c = 'cumplida';
              }
              else{
                $promesaPago->estado_promesa_c = $promesaPago->estado_promesa_c === 'vigente' ? 'vigente' : 'pagada_con_atraso';
              }
              $promesaPago->save(false);
              $promesaPago->load_relationship('lowae_autorizacion_especial_lowes_pagos_2');
              $promesaPago->lowae_autorizacion_especial_lowes_pagos_2->add($pago);
              } else {
                break;
              }
            }
            return true;
          }
          return false;
        }

        public function desaplicaPagoPromesaPago($pago){
          $pago->load_relationship('lowae_autorizacion_especial_lowes_pagos_2');
          $autorizaciones = $pago->lowae_autorizacion_especial_lowes_pagos_2->getBeans();
          foreach ($autorizaciones as $key => $promesaPago) {
            $pago->lowae_autorizacion_especial_lowes_pagos_2->delete($pago->id, $promesaPago->id);
          }
        }

        public function getPromesaPago($account)
        {
          require_once('include/SugarQuery/SugarQuery.php');
          $idAccount = $account->id;

          $sq = new SugarQuery();
          $sq->select(array('id'));
          $sq->from(BeanFactory::newBean('lowae_autorizacion_especial'),array('alias'=>'lae','team_security'=>false));
          $sq->joinTable('accounts_lowae_autorizacion_especial_1_c', array('alias' => 'jt0'))->on()
          ->equalsField('lae.id','jt0.accounts_le69cspecial_idb')
          ->equals('jt0.deleted',0);
          $sq->joinTable('accounts', array('alias' => 'a'))->on()
          ->equalsField('a.id','jt0.accounts_lowae_autorizacion_especial_1accounts_ida')
          ->equals('a.deleted',0);
          $sq->joinTable('accounts_cstm', array('alias' => 'a_c'))->on()
          ->equalsField('a_c.id_c','a.id');
          $sq->where()->queryAnd()->equals('motivo_solicitud_c','promesa_de_pago')
          ->in('estado_promesa_c',array('vigente','incumplida','pagada_con_atraso'))
          ->in('lae_c.estado_promesa_segun_monto_c',array('sin_pago','pagada_parcial'))
          ->in('lae_c.estado_autorizacion_c',array('autorizada','finalizada','terminada_incumplida'));
          $sq->whereRaw("a.id = '$idAccount'");
          $sq->orderBy('date_entered','ASC');
          $result = $sq->execute();

          $results = [];

          $num_rows = count($result);

      		if($num_rows){
            foreach ($result as $key => $row) {
              $results[] = $row;
            }
            return $results;
          } else {
            return null;
          }
        }

        public function aplicaDesbloqueo($bean)
        {
          if(
          	$bean->credito_disponible_c > 0
          	&& $bean->estado_bloqueo_c === "Bloqueado"
          	&& $bean->codigo_bloqueo_c === "LE"
          ){
            if($bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida){
        			$grupoEmpresas = BeanFactory::getBean('GPE_Grupo_empresas', $bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida);
        			if (
        				!empty($grupoEmpresas->id) && $grupoEmpresas->administrar_credito_grupal
        			){
                $grupoEmpresas->load_relationship('gpe_grupo_empresas_accounts');
                $accounts = $grupoEmpresas->gpe_grupo_empresas_accounts->getBeans();
        				foreach ($accounts as $account) {
        				  if($account->id === $bean->id) continue;
                  $account->estado_bloqueo_c = "Desbloqueado";
                  $account->codigo_bloqueo_c = "";
                  $account->save(false);

        				}
                return true;;
        			}
        		}
            $bean->estado_bloqueo_c = "Desbloqueado";
            $bean->codigo_bloqueo_c = "";
            $bean->save(false);
          }

        }
    }
