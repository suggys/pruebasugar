({
  extendsFrom: 'SubpanelsLayout',

  initialize: function (options) {
    this._super('initialize',arguments);
  },

  showSubpanel: function (linkName) {
    var self = this;
    _.each(self._components, function (component) {
      var link = component.context.get('link');
      if(self._hideButtons(link)){
        component.$el.find('.subpanel-controls').hide();
      }
      if(link === "accounts_lowae_autorizacion_especial_1"){
        component.$el.find('.btn.dropdown-toggle').addClass('disabled');
      }

      if(link === 'low01_solicitudescredito_accounts'){
        var filter = [{
          id: self.model.get('id')
        }];
        var url = app.api.buildURL('Accounts', 'read', {}, {
          filter: filter
        });

        app.api.call('read', url, {}, {
          success: function(data) {
            if(!_.isEmpty(data.records[0].id_ar_credit_c)){
              component.$el.find('a[class="rowaction btn"]').hide(); // Esconde el boton de crear
              component.$el.find('a[class="btn dropdown-toggle"]').hide(); // Esconde el link de vincular.
            }
          }
        });
      }

    });
  },

  _hideButtons: function(link) {
    var self = this;
    var response = false;

    if(
      link === 'accounts_lf_facturas_1'
      ||  link === 'accounts_orden_ordenes_1'
      ||  link === 'nc_notas_credito_accounts'
      ||  link === 'lowes_pagos_accounts')
      {
        response = true;
      }

      return response;
    },

    _render: function() {
      this._super('_render', arguments);
      this._initSortablePlugin();
    },

  })
