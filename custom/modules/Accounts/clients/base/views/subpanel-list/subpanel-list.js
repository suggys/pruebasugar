({
    extendsFrom: 'SubpanelListView',

	initialize: function(options) {
		this._super('initialize',[options]);
	},
	_initializeMetadata : function(options) {
        var tmp = this._super('_initializeMetadata', arguments);
        if(this._hideUnlinkButton() && this.options.context.parent.get('module') == "GPE_Grupo_empresas"){
        	tmp.rowactions.actions = _.without(tmp.rowactions.actions, _.findWhere(tmp.rowactions.actions, {
                type: "unlink-action"
            }));
        }
        return tmp;
    },
    _hideUnlinkButton: function() {
        var self = this;
        var response = false;
        if(app.user.get('type') != 'admin'){
            if(!_.filter(app.user.get('roles'), function(rol) { return rol.toLowerCase() == "administrador de credito";}).length )
            {
                response = true;
            }
        }
        return response;
    },
})
