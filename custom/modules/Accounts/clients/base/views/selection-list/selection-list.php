<?php
// created: 2018-04-14 04:18:55
$viewdefs['Accounts']['base']['view']['selection-list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'id_pos_c',
          'label' => 'LBL_ID_POS_C',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'id_linea_anticipo_c',
          'label' => 'LBL_ID_LINEA_ANTICIPO_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'id_ar_credit_c',
          'label' => 'LBL_ID_AR_CREDIT_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'name',
          'link' => true,
          'label' => 'LBL_LIST_ACCOUNT_NAME',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'account_type_c',
          'label' => 'LBL_ACCOUNT_TYPE_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'rfc_c',
          'label' => 'LBL_RFC_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'limite_credito_autorizado_c',
          'label' => 'LBL_LIMITE_CREDITO_AUTORIZADO_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'gpe_grupo_empresas_accounts_name',
          'label' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE',
          'enabled' => true,
          'id' => 'GPE_GRUPO_EMPRESAS_ACCOUNTSGPE_GRUPO_EMPRESAS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_USER',
          'id' => 'ASSIGNED_USER_ID',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'primary_address_state_c',
          'label' => 'LBL_PRIMARY_ADDRESS_STATE_C',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'numero_cuenta_clabe_c',
          'label' => 'LBL_NUMERO_CUENTA_CLABE_C',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'date_modified',
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'date_entered',
          'type' => 'datetime',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
        ),
        13 => 
        array (
          'name' => 'id_sucursal_c',
          'label' => 'LBL_ID_SUCURSAL_C',
          'enabled' => true,
          'default' => false,
        ),
        14 => 
        array (
          'name' => 'estado_cuenta_c',
          'label' => 'LBL_ESTADO_CUENTA_C',
          'enabled' => true,
          'default' => false,
        ),
        15 => 
        array (
          'name' => 'estado_bloqueo_c',
          'label' => 'LBL_ESTADO_BLOQUEO_C',
          'enabled' => true,
          'default' => false,
        ),
        16 => 
        array (
          'name' => 'codigo_bloqueo_c',
          'label' => 'LBL_CODIGO_BLOQUEO_C',
          'enabled' => true,
          'default' => false,
        ),
        17 => 
        array (
          'name' => 'estado_cartera_c',
          'label' => 'LBL_ESTADO_CARTERA_C',
          'enabled' => true,
          'default' => false,
        ),
        18 => 
        array (
          'name' => 'plazo_pago_autorizado_c',
          'label' => 'LBL_PLAZO_PAGO_AUTORIZADO_C',
          'enabled' => true,
          'default' => false,
        ),
        19 => 
        array (
          'name' => 'dias_tolerancia_c',
          'label' => 'LBL_DIAS_TOLERANCIA_C',
          'enabled' => true,
          'default' => false,
        ),
        20 => 
        array (
          'name' => 'credito_disponible_c',
          'label' => 'LBL_CREDITO_DISPONIBLE_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        21 => 
        array (
          'name' => 'credito_disponible_flexible_c',
          'label' => 'LBL_CREDITO_DISPONIBLE_FLEXIBLE',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        22 => 
        array (
          'name' => 'saldo_pendiente_aplicar_c',
          'label' => 'LBL_SALDO_PENDIENTE_APLICAR_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        23 => 
        array (
          'name' => 'saldo_deudor_c',
          'label' => 'LBL_SALDO_DEUDOR_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        24 => 
        array (
          'name' => 'saldo_favor_c',
          'label' => 'LBL_SALDO_FAVOR_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        25 => 
        array (
          'name' => 'limite_credito_grupo_c',
          'label' => 'LBL_LIMITE_CREDITO_GRUPO_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        26 => 
        array (
          'name' => 'credito_diponible_grupo_c',
          'label' => 'LBL_CREDITO_DIPONIBLE_GRUPO_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        27 => 
        array (
          'name' => 'saldo_deudor_gpe_empresas_c',
          'label' => 'LBL_SALDO_DEUDOR_GPE_EMPRESAS_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        28 => 
        array (
          'name' => 'email',
          'label' => 'LBL_EMAIL_ADDRESS',
          'enabled' => true,
          'default' => false,
        ),
      ),
    ),
  ),
);