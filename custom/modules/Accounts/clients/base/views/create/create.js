({
	extendsFrom:"CreateView",

	initialize: function () {
		var self = this;
		this._super("initialize", arguments);
		//this.model.on('change:assigned_user_name',_.bind(this._changeAssignedUserName, this));
		this.model.on('change:tipo_cliente2_c',_.bind(this._changeTipoCliente2, this));
		this.model.on('change:giro_c',_.bind(this._changeGiro, this));
		this.model.on('change:oficio_c',_.bind(this._changeOficio, this));
		this.model.on('change:estado_bloqueo_c',_.bind(this._changeEstadoBloqueoCuenta, this));
		//Lowes CRM
		if(self.getField('tipo_cliente2_c')){
			this.model.addValidationTask('validateAssignedUserName',_.bind(this._doValidateAssignedUserName, this));
			this.model.addValidationTask('validateGiro',_.bind(this._doValidateGiro, this));
			this.model.addValidationTask('validateOtroGiro',_.bind(this._doValidateOtroGiro, this));
			this.model.addValidationTask('validateOficio',_.bind(this._doValidateOficio, this));
			this.model.addValidationTask('validateOtroOficio',_.bind(this._doValidateOtroOficio, this));
		}
		this.model.addValidationTask('validateDuplicate',_.bind(this._doValidateDuplicate, this));
		this.model.addValidationTask('validateMontoGarantia',_.bind(this._doValidateMontoGarantia, this));
		this.model.addValidationTask('validateCP',_.bind(this._doValidateCP, this));
		this.model.addValidationTask('validateContactoPagoKonesh',_.bind(this._doValidateContactoPagoKonesh, this));
	},

	_doValidateDuplicate: function(fields, errors, callback) {
		var self = this;
		if(!self.model) return;
		var filter = [{rfc_c:self.model.get('rfc_c')}];
		var url = app.api.buildURL('Accounts', 'read',{},{filter:filter})
		app.api.call('read', url, {}, {
			success: function(data){
				self._processFilterRFCSuccess(data, function (isValid) {
					if(isValid){
						errors['rfc_c'] = errors['rfc_c'] || {};
						errors['rfc_c']['El RFC puede ser duplicado solo si las empresas que lo tienen pertenecen al mismo Grupo de Empresas.'] = true;
					}
					callback(null, fields, errors);
				})
			}
		});
	},

	_processFilterRFCSuccess: function (data, callback) {
		var isValid = true;
		if(!data.records.length){
			isValid = false;
		}
		else{
			var accountData = data.records[0];
			if(this.model.get('gpe_grupo_empresas_accountsgpe_grupo_empresas_ida') && accountData.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida === this.model.get('gpe_grupo_empresas_accountsgpe_grupo_empresas_ida')){
				isValid = false;
			}
		}
		callback(isValid);
	},

	_renderHtml: function (argument) {
		var self = this;
		this.model.set({
			gpe_grupo_empresas_accounts_name: "",
			gpe_grupo_empresas_accountsgpe_grupo_empresas_ida: "",
		});
		this._super('_renderHtml', arguments);

		//actualización DRI del 23 sep 2016
		if(self.getField('isbusinesscustomer_c')){
			self.getField('isbusinesscustomer_c').setMode('readonly');
			self.getField('isbusinesscustomer_c').setDisabled('true');
		}
		if(self.getField('id_ar_credit_c')){
			self.getField('id_ar_credit_c').setMode('readonly');
			self.getField('id_ar_credit_c').setDisabled('true');
		}
		if(self.getField('id_linea_anticipo_c')){
			self.getField('id_linea_anticipo_c').setMode('readonly');
			self.getField('id_linea_anticipo_c').setDisabled('true');
		}
		self.getField('id_pos_c').setMode('readonly');
		self.getField('id_pos_c').setDisabled('true');
		if(self.getField('saldo_deudor_c')){
			self.getField('saldo_deudor_c').setMode('readonly');
			self.getField('saldo_deudor_c').setDisabled('true');
		}
		if(self.getField('saldo_pendiente_aplicar_c')){
			self.getField('saldo_pendiente_aplicar_c').setMode('readonly');
			self.getField('saldo_pendiente_aplicar_c').setDisabled('true');
		}
		if(self.getField('saldo_favor_c')){
			self.getField('saldo_favor_c').setMode('readonly');
			self.getField('saldo_favor_c').setDisabled('true');
		}
		//Agregando credito_disponible_flexible_c por FR-11.7
		if(self.getField('credito_disponible_flexible_c')){
			self.getField('credito_disponible_flexible_c').setMode('readonly');
			self.getField('credito_disponible_flexible_c').setDisabled('true');
		}
		if(self.getField('estado_cartera_c')){
			self.getField('estado_cartera_c').setMode('readonly');
			self.getField('estado_cartera_c').setDisabled('true');
		}
		if(self.getField('gpe_grupo_empresas_accounts_name')){
			self.getField('gpe_grupo_empresas_accounts_name').setMode('readonly');
			self.getField('gpe_grupo_empresas_accounts_name').setDisabled('true');
		}
		//actualización DRI del 26 sep 2016
		if(self.getField('codigo_bloqueo_c')){
			var codigoField = self.getField('codigo_bloqueo_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
		}
	},

	//actualización DRI del 26 sep 2016
	_doValidateMontoGarantia: function (fields, errors, callback) {
		var self = this;
		if(!self.model) return false;
		if( self.model.hasChanged('nombre_documento_garantia_c') && !_.isEmpty(self.model.get('nombre_documento_garantia_c') && self.getField('nombre_documento_garantia_c')) ){
			errors['monto_documento_garantia_c'] = errors['monto_documento_garantia_c'] || {};
			errors['monto_documento_garantia_c']['Se requiere el monto del documento en garantia.'] = true;
		}
		callback(null, fields, errors);
	},

	_render: function (argument) {
		var self = this;

		// if(this.model.isNotEmpty){
		// 	if(!_.isEmpty(self.model.get('assigned_user_id'))){
		// 		var idUser = self.model.get('assigned_user_id');
		// 		var url_users = app.api.buildURL('Users/'+idUser);
		// 		app.api.call('read', url_users, {}, {
		// 			success: function(data){
		// 				if(data){
		// 					self.model.set({id_sucursal_c:data.sucursal_c});
		// 				}
		// 			}
		// 		});
		// 	}
		// }

		this._super('_render', arguments);
		if(self.getField('codigo_bloqueo_c')){
			var codigoField = self.getField('codigo_bloqueo_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
		}
		if(self.getField('otro_giro_c')){
			var codigoField = self.getField('otro_giro_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
		}
		if(self.getField('oficio_c')){
			var codigoField = self.getField('oficio_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
		}
		if(self.getField('otro_oficio_c')){
			var codigoField = self.getField('otro_oficio_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
		}
		//if(app.user.get('type') !== "admin"){
		//self.$el.find('a[name="save_button"]').hide();
		//}
	},

	_changeEstadoBloqueoCuenta: function (model, value) {
		var self = this;
		var codigoField = self.getField('codigo_bloqueo_c');
		if(codigoField){
			if(value === 'Desbloqueado'){
				self.model.set('codigo_bloqueo_c',null);
				var meta = this.getFieldMeta('codigo_bloqueo_c');
				delete(meta.readonly);
				self.setFieldMeta('codigo_bloqueo_c', meta);
				codigoField.$el.closest('.record-cell').addClass('hidden');
			}
			else if(value === 'Bloqueado'){
				if(self.currentState !== 'create'){
					codigoField.items = app.lang.getAppListStrings('account_codigo_bloqueo_c_list');
					if(_.contains(['DI','CV','LE'], self.model.get('codigo_bloqueo_c'))){
						var meta = self.getFieldMeta('codigo_bloqueo_c');
						meta.readonly = true;
						self.setFieldMeta('codigo_bloqueo_c', meta);
						codigoField.setMode('readonly')
					}
				}
				codigoField.$el.closest('.record-cell').removeClass('hidden');

			}
		}
	},

	_doValidateCP: function (fields, errors, callback) {
		var self = this;
		var exp = /\d{5}/;
		if(self.model.get('primary_address_postalcode_c') && !exp.test(self.model.get('primary_address_postalcode_c'))){
			errors['primary_address_postalcode_c'] = errors['primary_address_postalcode_c'] || {};
			errors['primary_address_postalcode_c']['El codigo postal debe de contener digitos.'] = true;
		}
		callback(null, fields, errors);
	},

	_renderFields: function (){
		var current = this.action;
		this.action = 'create';
		this._super('_renderFields', arguments);
		this.action = current;
		this.$el.find('span[data-fieldname="gpe_grupo_empresas_accounts_name"]').append('<p class="help-block">Para vincular una cuenta a un grupo de empresas, se debe realizar desde el subpanel del modulo Grupo de empresas.</p><br>');
	},

	// _changeAssignedUserName: function (model, value) {
	// 	var self = this;
	// 	idUser = self.model.get('assigned_user_id');
	// 	var url_users = app.api.buildURL('Users/'+idUser);
	// 	App.api.call('read', url_users, {}, {
	// 		success: function(data){
	// 			if(data){
	// 				self.model.set({id_sucursal_c:data.sucursal_c});
	// 			}
	// 		}
	// 	});
	// },

	_doValidateAssignedUserName: function (fields, errors, callback) {
		var self = this;
		if(_.isEmpty(self.model.get('assigned_user_name'))){
			errors['assigned_user_name'] = errors['assigned_user_name'] || {};
			errors['assigned_user_name'].required = true;
		}
		callback(null, fields, errors);
	},

	_changeTipoCliente2: function (model, value){
		var self = this;
		if(value === "especialista"){
			self.model.set({'otro_giro_c':''});
			var codigoField = self.getField('otro_giro_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');

			var codigoField = self.getField('oficio_c');
			if(codigoField.$el.closest('.record-cell').hasClass('hidden')){
				self.model.set({'oficio_c':''});
				codigoField.$el.closest('.record-cell').removeClass('hidden');
			}

		}

		if(value === "comercial" && !_.contains(self.model.get('giro_c'),"contratista")){
			self.model.set({'otro_giro_c':''});
			var codigoField = self.getField('otro_giro_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
			self.model.set({'oficio_c':''});
			var codigoField = self.getField('oficio_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
			self.model.set({'otro_oficio_c':''});
			var codigoField = self.getField('otro_oficio_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
		}

		if(_.isEmpty(value)){
			self.model.set({'otro_giro_c':''});
			var codigoField = self.getField('otro_giro_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
			self.model.set({'oficio_c':''});
			var codigoField = self.getField('oficio_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
			self.model.set({'otro_oficio_c':''});
			var codigoField = self.getField('otro_oficio_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
		}

	},

	_changeGiro: function (model, value){
		var self = this;
		if(_.contains(value,"otro")){
			var codigoField = self.getField('otro_giro_c');
			codigoField.$el.closest('.record-cell').removeClass('hidden');
		}else {
			self.model.set({'otro_giro_c':''});
			var codigoField = self.getField('otro_giro_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
		}
		if(_.contains(value,"contratista")){
			var codigoField = self.getField('oficio_c');
			codigoField.$el.closest('.record-cell').removeClass('hidden');
		}else{
			if(self.model.get('tipo_cliente2_c') != "especialista"){
				self.model.set({'oficio_c':''});
				var codigoField = self.getField('oficio_c');
				codigoField.$el.closest('.record-cell').addClass('hidden');
				self.model.set({'otro_oficio_c':''});
				var codigoField = self.getField('otro_oficio_c');
				codigoField.$el.closest('.record-cell').addClass('hidden');
			}
		}
	},

	_changeOficio: function (model, value){
		var self = this;
		if(_.contains(value,"otro")){
			var codigoField = self.getField('otro_oficio_c');
			codigoField.$el.closest('.record-cell').removeClass('hidden');
		}else {
			self.model.set({'otro_oficio_c':''});
			var codigoField = self.getField('otro_oficio_c');
			codigoField.$el.closest('.record-cell').addClass('hidden');
		}
	},

	_doValidateGiro: function (fields, errors, callback) {
		var self = this;
		if((self.getField('giro_c') && self.model.get('tipo_cliente2_c') === "comercial") && _.isEmpty(self.model.get('giro_c'))){
			errors['giro_c'] = errors['giro_c'] || {};
			errors['giro_c'].required = true;
			errors['giro_c']['Seleccione al menos una opción'] = true;
		}
		callback(null, fields, errors);
	},

	_doValidateOtroGiro: function (fields, errors, callback) {
		var self = this;
		if((self.getField('otro_giro_c') && _.contains(self.model.get('giro_c'),"otro")) && _.isEmpty(self.model.get('otro_giro_c'))){
			errors['otro_giro_c'] = errors['otro_giro_c'] || {};
			errors['otro_giro_c'].required = true;
			errors['otro_giro_c']['Ingrese una descripción del giro'] = true;
		}
		callback(null, fields, errors);
	},

	_doValidateOficio: function (fields, errors, callback) {
		var self = this;
		if((self.getField('oficio_c') && self.model.get('tipo_cliente2_c') === "especialista") && _.isEmpty(self.model.get('oficio_c'))){
			errors['oficio_c'] = errors['oficio_c'] || {};
			errors['oficio_c'].required = true;
			errors['oficio_c']['Seleccione al menos una opción'] = true;
		}

		if(self.getField('oficio_c') &&
		(
			self.model.get('tipo_cliente2_c') === "comercial" &&
			_.contains(self.model.get('giro_c'),"contratista")
		)
		&& _.isEmpty(self.model.get('oficio_c'))
	){
		errors['oficio_c'] = errors['oficio_c'] || {};
		errors['oficio_c'].required = true;
		errors['oficio_c']['Seleccione al menos una opción'] = true;
	}
	callback(null, fields, errors);
},

_doValidateOtroOficio: function (fields, errors, callback) {
	var self = this;
	if((self.getField('otro_oficio_c') && _.contains(self.model.get('oficio_c'),"otro")) && _.isEmpty(self.model.get('otro_oficio_c'))){
		errors['otro_oficio_c'] = errors['otro_oficio_c'] || {};
		errors['otro_oficio_c'].required = true;
		errors['otro_oficio_c']['Ingrese una descripción del oficio'] = true;
	}
	callback(null, fields, errors);
},

_doValidateContactoPagoKonesh: function(fields, errors, callback) {
	var self = this;
	if(self.getField('contacto_pagos_konesh_c') && !_.isEmpty(self.model.get('contacto_pagos_konesh_c'))){
		var idCpk = self.model.get('contact_id1_c');
		var url_contacts = app.api.buildURL('Contacts/'+idCpk);
		app.api.call('read', url_contacts, {}, {
			success: function(data){
				self._processDataSuccess(data, function (isValid) {
					if(!isValid){
						errors['contacto_pagos_konesh_c'] = errors['contacto_pagos_konesh_c'] || {};
						errors['contacto_pagos_konesh_c']['El contacto no cuenta con un email'] = true;
					}
					callback(null, fields, errors);
				})
			}
		});
	}
},

_processDataSuccess: function (data, callback) {
	var isValid = true;
	if(_.isEmpty(data.email)){
		isValid = false;
	}
	callback(isValid);
},

})
