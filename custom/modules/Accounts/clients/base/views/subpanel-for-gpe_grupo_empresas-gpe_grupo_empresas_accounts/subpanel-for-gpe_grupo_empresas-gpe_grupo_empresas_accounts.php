<?php
// created: 2016-12-22 17:38:59
$viewdefs['Accounts']['base']['view']['subpanel-for-gpe_grupo_empresas-gpe_grupo_empresas_accounts'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'id_pos_c',
          'label' => 'LBL_ID_POS_C',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'id_ar_credit_c',
          'label' => 'LBL_ID_AR_CREDIT_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'default' => true,
          'label' => 'LBL_LIST_ACCOUNT_NAME',
          'enabled' => true,
          'name' => 'name',
          'link' => true,
        ),
        3 => 
        array (
          'name' => 'account_type_c',
          'label' => 'LBL_ACCOUNT_TYPE_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'rfc_c',
          'label' => 'LBL_RFC_C',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'limite_credito_autorizado_c',
          'label' => 'LBL_LIMITE_CREDITO_AUTORIZADO_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'credito_disponible_c',
          'label' => 'LBL_CREDITO_DISPONIBLE_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'credito_disponible_flexible_c',
          'label' => 'LBL_CREDITO_DISPONIBLE_FLEXIBLE',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'saldo_pendiente_aplicar_c',
          'label' => 'LBL_SALDO_PENDIENTE_APLICAR_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'saldo_deudor_c',
          'label' => 'LBL_SALDO_DEUDOR_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'estado_cuenta_c',
          'label' => 'LBL_ESTADO_CUENTA_C',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'estado_bloqueo_c',
          'label' => 'LBL_ESTADO_BLOQUEO_C',
          'enabled' => true,
          'default' => true,
        ),
        12 => 
        array (
          'name' => 'codigo_bloqueo_c',
          'label' => 'LBL_CODIGO_BLOQUEO_C',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);