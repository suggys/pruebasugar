({
    extendsFrom: 'RecordView',
    my_events: {
       'keyup input[name=primary_address_postalcode_c]': '_keypress_primary_address_postalcode_c',
       'keypress input[name=primary_address_postalcode_c]': '_keypress_event_primary_address_postalcode_c',
    },
    initialize: function(options) {
        this.plugins = _.union(this.plugins || [], ['HistoricalSummary']);
        this._super('initialize', [options]);
        var self = this;
        self.fieldDisabled = false;
        self.model.on('sync', function(model, value){
            self._disableField(app.user.get('roles'));
        });

        this.model.on('change:estado_bloqueo_c',_.bind(this._changeEstadoBloqueoCuenta, this));
        this.model.on('change:enviar_pos_c', _.bind(this._changeEnvioPOS, this));

        //this.model.on('change:assigned_user_name',_.bind(this._changeAssignedUserName, this));
        this.model.on('change:tipo_cliente2_c',_.bind(this._changeTipoCliente, this));
        this.model.on('change:giro_c',_.bind(this._changeGiro, this));
        this.model.on('change:oficio_c',_.bind(this._changeOficio, this));
        this.model.addValidationTask('validateGiro',_.bind(this._doValidateGiro, this));
        this.model.addValidationTask('validateOtroGiro',_.bind(this._doValidateOtroGiro, this));
        this.model.addValidationTask('validateOficio',_.bind(this._doValidateOficio, this));
        this.model.addValidationTask('validateOtroOficio',_.bind(this._doValidateOtroOficio, this));

        this.model.addValidationTask('validateDuplicate',_.bind(this._doValidateDuplicate, this));
        this.model.addValidationTask('ValidateChangeActiva',_.bind(this._doValidateChangeActiva, this));
        this.model.addValidationTask('ValidateInactivaConFacturaVigente',_.bind(this._doValidateInactivaConFacturaVigente, this));
        this.model.addValidationTask('validateMontoGarantia',_.bind(this._doValidateMontoGarantia, this));
        //this.model.addValidationTask('validateEstadoBloqueo',_.bind(this._doValidateEstadoBloqueo, this));
        this.model.addValidationTask('validateEdoCta',_.bind(this._doValidateEdoCta, this));
        this.model.addValidationTask('validateCodigoBloqueoRequerido',_.bind(this._doValidateCodigoBloqueoRequerido, this));

        // Validaciones por envio a POS
        this.model.addValidationTask('rfc_c',_.bind(this._ValidaRFCCtaEnvioPOS, this));
        this.model.addValidationTask('primary_address_country_c',_.bind(this._ValidaPaisCtaEnvioPOS, this));
        this.model.addValidationTask('primary_address_numero_c',_.bind(this._ValidaNumCtaEnvioPOS, this));
        this.model.addValidationTask('primary_address_state_c',_.bind(this._ValidaEstadoCtaEnvioPOS, this));
        this.model.addValidationTask('primary_address_city_c',_.bind(this._ValidaCiudadCtaEnvioPOS, this));
        this.model.addValidationTask('primary_address_colonia_c',_.bind(this._ValidaColoniaCtaEnvioPOS, this));
        this.model.addValidationTask('primary_address_street_c',_.bind(this._ValidaCalleCtaEnvioPOS, this));
        this.model.addValidationTask('primary_address_postalcode_c',_.bind(this._ValidaCPCtaEnvioPOS, this));
        this.model.addValidationTask('id_linea_anticipo_c',_.bind(this._ValidaLinACtaEnvioPOS, this));
        this.model.addValidationTask('id_pos_c',_.bind(this._ValidaIDPOSCtaEnvioPOS, this));
        this.model.addValidationTask('name',_.bind(this._ValidaNameCtaEnvioPOS, this));
        this.model.addValidationTask('id_ar_credit_c',_.bind(this._ValidaARCredCtaEnvioPOS, this));
        this.model.addValidationTask('email',_.bind(this._ValidaEmailCtaEnvioPOS, this));
        this.model.addValidationTask('phone_office',_.bind(this._ValidaTelOfCtaEnvioPOS, this));
        this.model.addValidationTask('validateCP',_.bind(this._doValidateCP, this));
        this.model.addValidationTask('validateContactoPagoKonesh',_.bind(this._doValidateContactoPagoKonesh, this));
    },
    _keypress_event_primary_address_postalcode_c: function (event) {
        var controlKeys = [8, 9, 13, 35, 36, 37, 39];
        // IE doesn't support indexOf
        var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
        console.log(event.which);
        // Some browsers just don't raise events for control keys. Easy.
        // e.g. Safari backspace.
        if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
            (48 <= event.which && event.which <= 57) || // Always 1 through 9
            /*(48 == event.which && $(this).attr("value")) ||*/ // No 0 first digit
            isControlKey) { // Opera assigns values for control keys.
          return;
        } else {
          event.preventDefault();
        }
    },
    _keypress_primary_address_postalcode_c: function(evt) {
         var self = this;
         var postalCode = self.$el.find('input[name=primary_address_postalcode_c]').val() || '';
         var values = [];
         //console.log(postalCode);
         if(String(postalCode).length == 5)
         {
             app.alert.show('datos_cp', {
               level: 'process',
               title: 'Cargando datos de direcci&oacute;n.',
               autoClose: false
            });
             var url = app.api.buildURL('Merxbp/Zipcode', 'read',{},{zipcode:postalCode});
             app.api.call('read', url, {}, {
               success: function(data){
                  self._setModeAndDisablefieldsAddress('edit', false);
                  self.model.set({'primary_address_country_c': 'MX'});
                  if(data.Code == 200) {
                      self.model.set({'primary_address_country_c': 'MX'});
                      self.model.set({'primary_address_city_c': data.Address.Ciudad});
                      self.model.set({'primary_address_state_c': data.Address.Abr_Estado});

                      _.each(data.Address.Colonias, function(col){values.push({id: col.Colonia, text: col.Colonia});});
                      values.push({id : 'Otra', text: 'Otra'});
                      self.$el.find('input[name=primary_address_colonia_c]').select2({
                        width:"100%",
                        containerCssClass: 'select2-choices-pills-close',
                        data: values
                     });

                     if(values.length == 2)
                     {
                        self.model.set({'primary_address_colonia_c': values[0].text});
                        self.$el.find('input[name=primary_address_colonia_c]').val(values[0].text).trigger('change');
                     }
                  }
                app.alert.dismiss('datos_cp');
               }
           });
        } else {
            self._setModeAndDisablefieldsAddress('readonly', true);
            self._clearFieldsAddress();
          }
    },
    _clearFieldsAddress: function(){
        var self = this;
          self.model.set(
            {
              'primary_address_colonia_c': '',
              'primary_address_city_c': '',
              'primary_address_state_c': '',
              'primary_address_street_c': '',
              'primary_address_numero_c': ''
              // 'alt_address_state_c': ''
            }
          );
      },
    _setModeAndDisablefieldsAddress: function(mode, disable){
        var self = this;
        self.getField('primary_address_colonia_c').setMode(mode);
        self.getField('primary_address_colonia_c').setDisabled(disable);

        self.getField('primary_address_city_c').setMode(mode);
        self.getField('primary_address_city_c').setDisabled(disable);

        self.getField('primary_address_country_c').setMode(mode);
        self.getField('primary_address_country_c').setDisabled(disable);

        self.getField('primary_address_state_c').setMode(mode);
        self.getField('primary_address_state_c').setDisabled(disable);
      },
    _ValidaRFCCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('rfc_c')) && self.model.get('enviar_pos_c')){
                errors['rfc_c'] = errors['rfc_c'] || {};
                errors['rfc_c']['El RFC es requerido para envío POS'] = true;
            }
        callback(null, fields, errors);
    },

    _ValidaPaisCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('primary_address_country_c')) && self.model.get('enviar_pos_c')){
                errors['primary_address_country_c'] = errors['primary_address_country_c'] || {};
                errors['primary_address_country_c']['El país es requerido para envío POS'] = true;
            }
        callback(null, fields, errors);
    },

    _ValidaNumCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('primary_address_numero_c')) && self.model.get('enviar_pos_c')){
                errors['primary_address_numero_c'] = errors['primary_address_numero_c'] || {};
                errors['primary_address_numero_c']['El número es requerido para envío POS'] = true;
            }
        callback(null, fields, errors);
    },

    _ValidaEstadoCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if (self.model.get('primary_address_state_c') == '' && self.model.get('enviar_pos_c')){
                errors['primary_address_state_c'] = errors['primary_address_state_c'] || {};
                errors['primary_address_state_c']['El estado es requerido para envío POS'] = true;
            }
        callback(null, fields, errors);
    },

    _ValidaCiudadCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('primary_address_city_c')) && self.model.get('enviar_pos_c')){
                errors['primary_address_city_c'] = errors['primary_address_city_c'] || {};
                errors['primary_address_city_c']['La colonia es requerida para envío POS'] = true;
            }
        callback(null, fields, errors);
    },

    _ValidaColoniaCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('primary_address_colonia_c')) && self.model.get('enviar_pos_c')){
                errors['primary_address_colonia_c'] = errors['primary_address_colonia_c'] || {};
                errors['primary_address_colonia_c']['La colonia es requerida para envío POS'] = true;
            }
        callback(null, fields, errors);
    },

    _ValidaCalleCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('primary_address_street_c')) && self.model.get('enviar_pos_c')){
                errors['primary_address_street_c'] = errors['primary_address_street_c'] || {};
                errors['primary_address_street_c']['La calle es requerida para envío POS'] = true;
            }
        callback(null, fields, errors);
    },

    _ValidaCPCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('primary_address_postalcode_c')) && self.model.get('enviar_pos_c')){
                errors['primary_address_postalcode_c'] = errors['primary_address_postalcode_c'] || {};
                errors['primary_address_postalcode_c']['El codigo postal es requerido para envío POS'] = true;
            }
        callback(null, fields, errors);
    },

    _ValidaLinACtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('id_linea_anticipo_c')) && self.model.get('enviar_pos_c')){
                errors['id_linea_anticipo_c'] = errors['id_linea_anticipo_c'] || {};
                errors['id_linea_anticipo_c']['La linea de anticipo es requerida para envío POS'] = true;
            }
        callback(null, fields, errors);
    },

    _ValidaIDPOSCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('id_pos_c')) && self.model.get('enviar_pos_c')){
                errors['id_pos_c'] = errors['id_pos_c'] || {};
                errors['id_pos_c']['El ID POS es requerido para envío POS'] = true;
            }
        callback(null, fields, errors);
    },

    _ValidaNameCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('name')) && self.model.get('enviar_pos_c')){
            errors['name'] = errors['name'] || {};
            errors['name']['El Nombre es requerido para envío POS'] = true;
        }
        callback(null, fields, errors);
    },

    _ValidaARCredCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('id_ar_credit_c')) && self.model.get('enviar_pos_c')){
            errors['id_ar_credit_c'] = errors['id_ar_credit_c'] || {};
            errors['id_ar_credit_c']['El ID AR Credit es requerido para envío POS'] = true;
        }
        callback(null, fields, errors);
    },

    _ValidaEmailCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('email')) && self.model.get('enviar_pos_c')){
            errors['email'] = errors['email'] || {};
            errors['email']['El email es requerido para envío POS'] = true;
        }
        callback(null, fields, errors);
    },

    _ValidaTelOfCtaEnvioPOS: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if ( _.isEmpty(self.model.get('phone_office')) && self.model.get('enviar_pos_c')){
            errors['phone_office'] = errors['phone_office'] || {};
            errors['phone_office']['El Teléfono de oficina es requerido para envío POS'] = true;
        }
        callback(null, fields, errors);
    },

    _changeEnvioPOS: function (model, value) {
        var self = this;
        if(!self.model) return false;

        if(self.model.get('estado_interfaz_pos_c') != "vacio" && self.model.get('enviar_pos_c') == false) return false;

        if( self.model.get('enviar_pos_c')
            && self.model.get('estado_interfaz_pos_c') == "vacio"
            && self.model
            ){
                self.model.set('estado_interfaz_pos_c','nuevo');
        }
    },

    _disableField:function(rolesArray){
        var self = this;

        //actualización DRI del 23 sep 2016
        if(self.getField('isbusinesscustomer_c')){
          self.getField('isbusinesscustomer_c').setMode('readonly');
          self.getField('isbusinesscustomer_c').setDisabled('true');
        }
        if(self.getField('id_ar_credit_c')){
          self.getField('id_ar_credit_c').setMode('readonly');
          self.getField('id_ar_credit_c').setDisabled('true');
        }
        if(self.getField('id_linea_anticipo_c')){
          self.getField('id_linea_anticipo_c').setMode('readonly');
          self.getField('id_linea_anticipo_c').setDisabled('true');
        }

        self.getField('id_pos_c').setMode('readonly');
        self.getField('id_pos_c').setDisabled('true');

        if(self.getField('saldo_deudor_c')){
          self.getField('saldo_deudor_c').setMode('readonly');
          self.getField('saldo_deudor_c').setDisabled('true');
        }
        if(self.getField('saldo_pendiente_aplicar_c')){
          self.getField('saldo_pendiente_aplicar_c').setMode('readonly');
          self.getField('saldo_pendiente_aplicar_c').setDisabled('true');
        }
        if(self.getField('saldo_favor_c')){
          self.getField('saldo_favor_c').setMode('readonly');
          self.getField('saldo_favor_c').setDisabled('true');
        }
        //Agregando credito_disponible_flexible_c por FR-11.7
        if(self.getField('credito_disponible_flexible_c')){
          self.getField('credito_disponible_flexible_c').setMode('readonly');
          self.getField('credito_disponible_flexible_c').setDisabled('true');
        }
        if(self.getField('estado_cartera_c')){
          self.getField('estado_cartera_c').setMode('readonly');
          self.getField('estado_cartera_c').setDisabled('true');
        }
    },

    _doValidateEdoCta: function(fields, errors, callback)
    {
        var self = this;
        if(!self.model) return false;
        if ( self.model.get('estado_bloqueo_c') === 'Bloqueado' && self.model.get('estado_cuenta_c') === 'Inactivo'){
            errors['estado_cuenta_c'] = errors['estado_cuenta_c'] || {};
            errors['estado_cuenta_c']['No se puede inactivar la cuenta si el estado es Bloqueado.'] = true;
        }
        callback(null, fields, errors);
    },

    _doValidateEstadoBloqueo: function(fields, errors, callback)
    {
        var self = this;
        if(!self.model) return false;
        var plazo = self.model.get('plazo_pago_autorizado_c');
        var tolerancia = self.model.get('dias_tolerancia_c');
        var saldo_deudor = self.model.get('saldo_deudor_c');
        var estado_actual = self.model.get('estado_cuenta_c');
        var dia_mes = parseInt(moment().format('D'));
        if(parseInt(plazo) +  parseInt(tolerancia) < dia_mes && estado_actual==='Inactivo')
        {
            errors['estado_cuenta_c'] = errors['estado_cuenta_c'] || {};
            errors['estado_cuenta_c']['Esta cuenta tiene Plazo y Tolerancia por debajo del dia actual!, no se puede inactivar.!'] = true;
        }
        callback(null, fields, errors);
    },

    _doValidateDuplicate: function(fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        var modelSynced = app.data.createBean(self.model.module, self.model.getSynced());
        var changedArray = app.utils.compareBeans(self.model, modelSynced);
        if(_.contains(changedArray, "rfc_c") || _.contains(changedArray, "gpe_grupo_empresas_accountsgpe_grupo_empresas_ida")){
            var filter = [{
                rfc_c:self.model.get('rfc_c'),
                id: { $not_in: [self.model.id ]}
            }];
            var url = app.api.buildURL('Accounts', 'read',{},{filter:filter});
            app.api.call('read', url, {}, {
                success: function(data){
                    self._processFilterRFCSuccess(data, function (isValid) {
                        if(isValid){
                            errors['rfc_c'] = errors['rfc_c'] || {};
                            errors['rfc_c']['El RFC puede ser duplicado solo si las empresas que lo tienen pertenecen al mismo Grupo de Empresas.'] = true;
                        }
                        callback(null, fields, errors);
                    })
                }
            });
        }else{
            callback(null, fields, errors);
        }
    },

    _processFilterRFCSuccess: function (data, callback) {
        var isValid = true;
        if(!data.records.length){
            isValid = false;
        }
        else{
            var accountData = data.records[0];
            if(this.model.get('gpe_grupo_empresas_accountsgpe_grupo_empresas_ida') && accountData.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida === this.model.get('gpe_grupo_empresas_accountsgpe_grupo_empresas_ida')){
                isValid = false;
            }
        }
        callback(isValid);
    },

    _doValidateChangeActiva: function (fields, errors, callback) {
        var self = this;
        var modelSynced = app.data.createBean(self.model.module, self.model.getSynced());
        if(
            this.model.get('estado_cuenta_c') === 'Activo'
            && modelSynced.get('estado_cuenta_c') === 'Inactivo'
            && this.model.get('gpe_grupo_empresas_accountsgpe_grupo_empresas_ida')
        ){
          var grupoEmpresa = app.data.createBean('GPE_Grupo_empresas', {id:this.model.get('gpe_grupo_empresas_accountsgpe_grupo_empresas_ida')});
          grupoEmpresa.fetch({success: function(bean){
            if(bean.get('estado') === 'Inactivo'){
              errors['estado_cuenta_c'] = errors['estado_cuenta_c'] || {};
              errors['estado_cuenta_c']['La Cuenta no puede ser Activada por que el Grupo de empresas se encuentra Inactivo.'] = true;
            }
            callback(null, fields, errors);
          }});
          return false;
        }
        var changedArray = app.utils.compareBeans(self.model, modelSynced);
        if(
            this.model.get('estado_cuenta_c') === 'Inactivo' && (
                // Si lo que se cambia es el estado de la cuenta y otro campo
                (_.contains(changedArray, "estado_cuenta_c") && changedArray.length != 1) ||
                // Si el estado de la cuenta esta inactivo y modifican cualquier campo
                (!_.contains(changedArray, "estado_cuenta_c") && changedArray.length)
            )
        ){
            errors['estado_cuenta_c'] = errors['estado_cuenta_c'] || {};
            errors['estado_cuenta_c']['La cuenta no puede ser modificada si se encuentra Inactiva.'] = true;
        }
        callback(null, fields, errors);

    },

    //actualización DRI del 26 sep 2016
    _doValidateMontoGarantia: function (fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if(self.model.hasChanged('nombre_documento_garantia_c') && !_.isEmpty(self.model.get('nombre_documento_garantia_c'))){
            errors['monto_documento_garantia_c'] = errors['monto_documento_garantia_c'] || {};
            errors['monto_documento_garantia_c']['Se requiere el monto del documento en garantia.'] = true;
        }
        callback(null, fields, errors);
    },

    _doValidateCodigoBloqueoRequerido: function(fields, errors, callback){
        var self = this;
        if(!self.model) return false;
        if(self.model.get('estado_bloqueo_c') === 'Bloqueado' && !self.model.get('codigo_bloqueo_c')){
            errors['codigo_bloqueo_c'] = errors['codigo_bloqueo_c'] || {};
            errors['codigo_bloqueo_c']['required'] = true;
        }
        callback(null, fields, errors);
    },

    _changeEstadoBloqueoCuenta: function (model, value) {
        var self = this;
        if(this.currentState === 'view' ) return;
        if(!this.model.isNotEmpty) return;
        var codigoField = self.getField('codigo_bloqueo_c');
        // console.log('value:', value);
        // if(codigoField){
        //   if(value === 'Desbloqueado'){
        //       self.model.set({
        //           codigo_bloqueo_c: '',
        //           description: ''
        //       });
        //
        //       var meta = this.getFieldMeta('codigo_bloqueo_c');
        //       delete(meta.readonly);
        //       self.setFieldMeta('codigo_bloqueo_c', meta);
        //       codigoField.$el.closest('.record-cell').addClass('hidden');
        //       codigoField.setMode(this.currentState);
        //   }
        //   else if(value === 'Bloqueado'){
        //       codigoField.items = app.lang.getAppListStrings('account_codigo_bloqueo_c_list');
        //       if(_.contains(['DI','CV','LE'], self.model.get('codigo_bloqueo_c'))){
        //           var meta = self.getFieldMeta('codigo_bloqueo_c');
        //           meta.readonly = true;
        //           self.setFieldMeta('codigo_bloqueo_c', meta);
        //           codigoField.setMode('readonly');
        //       }
        //       codigoField.render();
        //       codigoField.$el.closest('.record-cell').removeClass('hidden');
        //
        //   }
        // }
    },
    _renderHtml: function (argument) {
         var self = this;
         this._super('_renderHtml', arguments);
         this.delegateEvents(_.extend({}, this.events, this.my_events));
     },
    _render: function (argument) {
        var self = this;
        this._super('_render', arguments);
        if(this.model.isNotEmpty){
            if(self.getField('codigo_bloqueo_c')){
             var codigoField = self.getField('codigo_bloqueo_c');
             if (self.model.get('estado_bloqueo_c') === "Desbloqueado" ){
                 codigoField.$el.closest('.record-cell').addClass('hidden');
             }
           }
           if(self.getField('otro_giro_c') && _.isEmpty(self.model.get('otro_giro_c'))){
             var codigoField = self.getField('otro_giro_c');
             codigoField.$el.closest('.record-cell').addClass('hidden');
           }
           if(self.getField('oficio_c') && _.isEmpty(self.model.get('oficio_c'))){
             var codigoField = self.getField('oficio_c');
             codigoField.$el.closest('.record-cell').addClass('hidden');
           }
           if(self.getField('otro_oficio_c') && _.isEmpty(self.model.get('otro_oficio_c'))){
             var codigoField = self.getField('otro_oficio_c');
             codigoField.$el.closest('.record-cell').addClass('hidden');
           }
        }

    },

    _doValidateCP: function (fields, errors, callback) {
        var self = this;
        var exp = /\d{5}/;
        if(self.model.get('primary_address_postalcode_c') && !exp.test(self.model.get('primary_address_postalcode_c'))){
            errors['primary_address_postalcode_c'] = errors['primary_address_postalcode_c'] || {};
            errors['primary_address_postalcode_c']['El codigo postal debe de contener digitos.'] = true;
        }
        callback(null, fields, errors);
    },

    bindDataChange: function() {
        this._super('bindDataChange', arguments);

        this.model.on("data:sync:complete", function(event, model) {
            if (event === "read") {

               $("[data-subpanel-link='members']").hide();
            }
        }, this);

    },

    _doValidateInactivaConFacturaVigente: function (fields, errors, callback) {
      var self = this;
      if(!this.model.isNotEmpty) callback(null, fields, errors);
      var modelSynced = app.data.createBean(self.model.module, self.model.getSynced());
      if(
          this.model.get('estado_cuenta_c') === 'Inactivo'
          && modelSynced.get('estado_cuenta_c') === 'Activo'
      ){
        var filter = [{
          accounts_lf_facturas_1accounts_ida: self.model.get('id'),
          estado_tiempo: 'vigente'
        }];
        var url = app.api.buildURL('LF_Facturas', 'read',{},{filter:filter});
        app.api.call('read', url, {}, {
          success:function (data) {
            if(data.records.length){
              errors['estado_cuenta_c'] = errors['estado_cuenta_c'] || {};
              errors['estado_cuenta_c']['La cuenta tiene facturas vigentes, no se puede Inactivar'] = true;
            }
            callback(null, fields, errors);
          }
        });
      }
      else{
        callback(null, fields, errors);
      }
    },

    _changeTipoCliente: function (model, value){
      var self = this;
      if(value === "especialista"){
        self.model.set({'otro_giro_c':''});
        if(self.getField('otro_giro_c')){
          var codigoField = self.getField('otro_giro_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
        if(self.getField('oficio_c')){
          var codigoField = self.getField('oficio_c');
          if(codigoField.$el.closest('.record-cell').hasClass('hidden')){
            self.model.set({'oficio_c':''});
            codigoField.$el.closest('.record-cell').removeClass('hidden');
          }
        }
      }

      if(value === "comercial" && !_.contains(self.model.get('giro_c'),"contratista")){
        self.model.set({'otro_giro_c':''});
        if(self.getField('otro_giro_c')){
          var codigoField = self.getField('otro_giro_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
        self.model.set({'oficio_c':''});
        if(self.getField('oficio_c')){
          var codigoField = self.getField('oficio_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
        self.model.set({'otro_oficio_c':''});
        if(self.getField('otro_oficio_c')){
          var codigoField = self.getField('otro_oficio_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
      }

      if(_.isEmpty(value)){
        self.model.set({'otro_giro_c':''});
        if(self.getField('otro_giro_c')){
          var codigoField = self.getField('otro_giro_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }

        self.model.set({'oficio_c':''});
        if(self.getField('oficio_c')){
          var codigoField = self.getField('oficio_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }

        self.model.set({'otro_oficio_c':''});
        if(self.getField('otro_oficio_c')){
          var codigoField = self.getField('otro_oficio_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
      }

    },

    _changeGiro: function (model, value){
      var self = this;
      if(_.contains(value,"otro")){
        if(self.getField('otro_giro_c')){
          var codigoField = self.getField('otro_giro_c');
          codigoField.$el.closest('.record-cell').removeClass('hidden');
        }
      }else{
        self.model.set({'otro_giro_c':''});
        if(self.getField('otro_giro_c')){
          var codigoField = self.getField('otro_giro_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
      }
      if(_.contains(value,"contratista")){
        if(self.getField('oficio_c')){
          var codigoField = self.getField('oficio_c');
          codigoField.$el.closest('.record-cell').removeClass('hidden');
        }
      }else{
        if(self.model.get('tipo_cliente2_c') != "especialista"){
          self.model.set({'oficio_c':''});
          if(self.getField('oficio_c')){
            var codigoField = self.getField('oficio_c');
            codigoField.$el.closest('.record-cell').addClass('hidden');
          }
          self.model.set({'otro_oficio_c':''});
          if(self.getField('otro_oficio_c')){
            var codigoField = self.getField('otro_oficio_c');
            codigoField.$el.closest('.record-cell').addClass('hidden');
          }
        }
      }
    },

    _changeOficio: function (model, value){
      var self = this;
      if(_.contains(value,"otro")){
        if(self.getField('otro_oficio_c')){
          var codigoField = self.getField('otro_oficio_c');
          codigoField.$el.closest('.record-cell').removeClass('hidden');
        }
      }else{
        self.model.set({'otro_oficio_c':''});
        if(self.getField('otro_oficio_c')){
          var codigoField = self.getField('otro_oficio_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
      }
    },

    _doValidateGiro: function (fields, errors, callback) {
        var self = this;
        if((self.getField('giro_c') && self.model.get('tipo_cliente2_c') === "comercial") && _.isEmpty(self.model.get('giro_c'))){
            errors['giro_c'] = errors['giro_c'] || {};
            errors['giro_c'].required = true;
            errors['giro_c']['Seleccione al menos una opción'] = true;
        }
        callback(null, fields, errors);
    },

    _doValidateOtroGiro: function (fields, errors, callback) {
        var self = this;
        if((self.getField('otro_giro_c') && _.contains(self.model.get('giro_c'),"otro")) && _.isEmpty(self.model.get('otro_giro_c'))){
            errors['otro_giro_c'] = errors['otro_giro_c'] || {};
            errors['otro_giro_c'].required = true;
            errors['otro_giro_c']['Ingrese una descripción del giro'] = true;
        }
        callback(null, fields, errors);
    },

    _doValidateOficio: function (fields, errors, callback) {
        var self = this;
        if((self.getField('oficio_c') && self.model.get('tipo_cliente2_c') === "especialista") && _.isEmpty(self.model.get('oficio_c'))){
            errors['oficio_c'] = errors['oficio_c'] || {};
            errors['oficio_c'].required = true;
            errors['oficio_c']['Seleccione al menos una opción'] = true;
        }

        if(self.getField('oficio_c') &&
          (
            self.model.get('tipo_cliente2_c') === "comercial" &&
            _.contains(self.model.get('giro_c'),"contratista")
          )
          && _.isEmpty(self.model.get('oficio_c'))
        ){
            errors['oficio_c'] = errors['oficio_c'] || {};
            errors['oficio_c'].required = true;
            errors['oficio_c']['Seleccione al menos una opción'] = true;
        }
        callback(null, fields, errors);
    },

    _doValidateOtroOficio: function (fields, errors, callback) {
        var self = this;
        if((self.getField('otro_oficio_c') && _.contains(self.model.get('oficio_c'),"otro")) && _.isEmpty(self.model.get('otro_oficio_c'))){
            errors['otro_oficio_c'] = errors['otro_oficio_c'] || {};
            errors['otro_oficio_c'].required = true;
            errors['otro_oficio_c']['Ingrese una descripción del oficio'] = true;
        }
        callback(null, fields, errors);
    },

    _doValidateContactoPagoKonesh: function(fields, errors, callback) {
      var self = this;
      if(self.getField('contacto_pagos_konesh_c') && !_.isEmpty(self.model.get('contacto_pagos_konesh_c'))){
        var idCpk = self.model.get('contact_id1_c');
        var url_contacts = app.api.buildURL('Contacts/'+idCpk);
        app.api.call('read', url_contacts, {}, {
          success: function(data){
            self._processDataSuccess(data, function (isValid) {
              if(!isValid){
                errors['contacto_pagos_konesh_c'] = errors['contacto_pagos_konesh_c'] || {};
                errors['contacto_pagos_konesh_c']['El contacto no cuenta con un email'] = true;
              }
              callback(null, fields, errors);
            });
          }
        });
      } else {
        callback(null, fields, errors);
      }
    },

    _processDataSuccess: function (data, callback) {
      var isValid = true;
      if(_.isEmpty(data.email)){
        isValid = false;
      }
      callback(isValid);
    },

})
