<?php
$viewdefs['Accounts'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'Accounts',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:historical_summary_button:click',
                'name' => 'historical_summary_button',
                'label' => 'LBL_HISTORICAL_SUMMARY',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              9 => 
              array (
                'type' => 'divider',
              ),
              10 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 
              array (
                'name' => 'name',
                'events' => 
                array (
                  'keyup' => 'update:account',
                ),
              ),
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'folio_c',
                'label' => 'LBL_FOLIO',
                'readonly' => true,
              ),
              1 => 
              array (
                'name' => 'id_pos_c',
                'label' => 'LBL_ID_POS_C',
                'readonly' => true,
              ),
              2 => 'assigned_user_name',
              3 => 
              array (
                'name' => 'id_sucursal_c',
                'label' => 'LBL_ID_SUCURSAL_C',
              ),
              4 => 
              array (
                'name' => 'gpe_grupo_empresas_accounts_name',
                'label' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE',
              ),
              5 => 
              array (
                'name' => 'estado_cuenta_c',
                'label' => 'LBL_ESTADO_CUENTA_C',
                'readonly' => true,
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL5',
            'label' => 'LBL_RECORDVIEW_PANEL5',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'tipo_cliente2_c',
                'label' => 'LBL_TIPO_CLIENTE2',
              ),
              1 => 
              array (
              ),
              2 => 
              array (
                'name' => 'giro_c',
                'label' => 'LBL_GIRO',
              ),
              3 => 
              array (
                'name' => 'otro_giro_c',
                'label' => 'LBL_OTRO_GIRO',
              ),
              4 => 
              array (
                'name' => 'oficio_c',
                'label' => 'LBL_OFICIO',
              ),
              5 => 
              array (
                'name' => 'otro_oficio_c',
                'label' => 'LBL_OTRO_OFICIO',
              ),
              6 => 
              array (
                'name' => 'metodo_pago_c',
                'label' => 'LBL_METODO_PAGO',
              ),
              7 => 
              array (
                'name' => 'subtipo_c',
                'label' => 'LBL_SUBTIPO',
              ),
              8 => 
              array (
                'name' => 'club_pro_c',
                'label' => 'LBL_CLUB_PRO',
              ),
              9 => 
              array (
                'name' => 'numero_club_pro_c',
                'label' => 'LBL_NUMERO_CLUB_PRO',
              ),
              10 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'vol_ventas_anual_est_c',
                'label' => 'LBL_VOL_VENTAS_ANUAL_EST',
              ),
              11 => 
              array (
                'name' => 'competidores_directos_c',
                'studio' => 'visible',
                'label' => 'LBL_COMPETIDORES_DIRECTOS',
              ),
              12 => 
              array (
                'name' => 'cuenta_patron_c',
                'label' => 'LBL_CUENTA_PATRON',
              ),
              13 => 
              array (
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL6',
            'label' => 'LBL_RECORDVIEW_PANEL6',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'account_type_c',
                'label' => 'LBL_ACCOUNT_TYPE_C',
              ),
              1 => 
              array (
                'name' => 'rfc_c',
                'label' => 'LBL_RFC_C',
              ),
              2 => 'phone_office',
              3 => 
              array (
                'name' => 'phone_alternate',
                'label' => 'LBL_PHONE_ALT',
              ),
              4 => 'email',
              5 => 'website',
              6 => 
              array (
                'name' => 'nombre_representante_c',
                'studio' => 'visible',
                'label' => 'LBL_NOMBRE_REPRESENTANTE_C',
              ),
              7 => 
              array (
                'name' => 'nombre_del_aval_c',
                'label' => 'LBL_NOMBRE_DEL_AVAL',
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL7',
            'label' => 'LBL_RECORDVIEW_PANEL7',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'primary_address_street_c',
                'studio' => 'visible',
                'label' => 'LBL_PRIMARY_ADDRESS_STREET_C',
              ),
              1 => 
              array (
                'name' => 'primary_address_numero_c',
                'label' => 'LBL_PRIMARY_ADDRESS_NUMERO_C',
              ),
              2 => 
              array (
                'name' => 'primary_address_postalcode_c',
                'label' => 'LBL_PRIMARY_ADDRESS_POSTALCODE_C',
              ),
              3 => 
              array (
                'name' => 'primary_address_state_c',
                'label' => 'LBL_PRIMARY_ADDRESS_STATE_C',
              ),
              4 => 
              array (
                'name' => 'primary_address_other_state_c',
                'label' => 'LBL_PRIMARY_ADDRESS_OTHER_STATE',
              ),
              5 => 
              array (
                'name' => 'primary_address_country_c',
                'label' => 'LBL_PRIMARY_ADDRESS_COUNTRY_C',
              ),
              6 => 
              array (
                'name' => 'primary_address_city_c',
                'label' => 'LBL_PRIMARY_ADDRESS_CITY_C',
              ),
              7 => 
              array (
                'name' => 'primary_address_colonia_c',
                'label' => 'LBL_PRIMARY_ADDRESS_COLONIA_C',
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL8',
            'label' => 'LBL_RECORDVIEW_PANEL8',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'billing_address_street',
                'comment' => 'The street address used for billing address',
                'label' => 'LBL_BILLING_ADDRESS_STREET',
              ),
              1 => 
              array (
                'name' => 'billing_address_number_c',
                'label' => 'LBL_BILLING_ADDRESS_NUMBER',
              ),
              2 => 
              array (
                'name' => 'billing_address_postalcode',
                'comment' => 'The postal code used for billing address',
                'label' => 'LBL_BILLING_ADDRESS_POSTALCODE',
              ),
              3 => 
              array (
                'name' => 'billing_address_state',
                'comment' => 'The state used for billing address',
                'label' => 'LBL_BILLING_ADDRESS_STATE',
              ),
              4 => 
              array (
                'name' => 'billing_address_other_state_c',
                'label' => 'LBL_BILLING_ADDRESS_OTHER_STATE',
              ),
              5 => 
              array (
                'name' => 'billing_address_country',
                'comment' => 'The country used for the billing address',
                'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
              ),
              6 => 
              array (
                'name' => 'billing_address_city',
                'comment' => 'The city used for billing address',
                'label' => 'LBL_BILLING_ADDRESS_CITY',
              ),
              7 => 
              array (
                'name' => 'billing_address_colonia_c',
                'label' => 'LBL_BILLING_ADDRESS_COLONIA',
              ),
            ),
          ),
          6 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL9',
            'label' => 'LBL_RECORDVIEW_PANEL9',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'alt_address_street_c',
                'label' => 'LBL_ALT_ADDRESS_STREET',
              ),
              1 => 
              array (
                'name' => 'alt_address_numero_c',
                'label' => 'LBL_ALT_ADDRESS_NUMERO',
              ),
              2 => 
              array (
                'name' => 'alt_address_postalcode_c',
                'label' => 'LBL_ALT_ADDRESS_POSTALCODE',
              ),
              3 => 
              array (
                'name' => 'alt_address_state_c',
                'label' => 'LBL_ALT_ADDRESS_STATE',
              ),
              4 => 
              array (
                'name' => 'alt_address_other_state_c',
                'label' => 'LBL_ALT_ADDRESS_OTHER_STATE',
              ),
              5 => 
              array (
                'name' => 'alt_address_country_c',
                'label' => 'LBL_ALT_ADDRESS_COUNTRY',
              ),
              6 => 
              array (
                'name' => 'alt_address_city_c',
                'label' => 'LBL_ALT_ADDRESS_CITY',
              ),
              7 => 
              array (
                'name' => 'alt_address_colonia_c',
                'label' => 'LBL_ALT_ADDRESS_COLONIA',
              ),
            ),
          ),
          7 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL10',
            'label' => 'LBL_RECORDVIEW_PANEL10',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'id_ar_credit_c',
                'label' => 'LBL_ID_AR_CREDIT_C',
              ),
              1 => 
              array (
                'name' => 'numero_cuenta_clabe_c',
                'label' => 'LBL_NUMERO_CUENTA_CLABE_C',
              ),
              2 => 
              array (
                'name' => 'estado_bloqueo_c',
                'label' => 'LBL_ESTADO_BLOQUEO_C',
              ),
              3 => 
              array (
                'name' => 'codigo_bloqueo_c',
                'label' => 'LBL_CODIGO_BLOQUEO_C',
              ),
              4 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'limite_credito_autorizado_c',
                'label' => 'LBL_LIMITE_CREDITO_AUTORIZADO_C',
              ),
              5 => 
              array (
                'name' => 'plazo_pago_autorizado_c',
                'label' => 'LBL_PLAZO_PAGO_AUTORIZADO_C',
              ),
              6 => 
              array (
                'name' => 'estado_cartera_c',
                'label' => 'LBL_ESTADO_CARTERA_C',
              ),
              7 => 
              array (
              ),
            ),
          ),
          8 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL11',
            'label' => 'LBL_RECORDVIEW_PANEL11',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'credito_disponible_c',
                'label' => 'LBL_CREDITO_DISPONIBLE_C',
              ),
              1 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'saldo_pendiente_aplicar_c',
                'label' => 'LBL_SALDO_PENDIENTE_APLICAR_C',
              ),
              2 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'saldo_deudor_c',
                'label' => 'LBL_SALDO_DEUDOR_C',
              ),
              3 => 
              array (
              ),
            ),
          ),
          9 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL12',
            'label' => 'LBL_RECORDVIEW_PANEL12',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'credito_diponible_grupo_c',
                'label' => 'LBL_CREDITO_DIPONIBLE_GRUPO_C',
              ),
              1 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'limite_credito_grupo_c',
                'label' => 'LBL_LIMITE_CREDITO_GRUPO_C',
              ),
              2 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'saldo_deudor_gpe_empresas_c',
                'label' => 'LBL_SALDO_DEUDOR_GPE_EMPRESAS_C',
              ),
              3 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'saldo_pendiente_aplicar_gpo_c',
                'label' => 'LBL_SALDO_PENDIENTE_APLICAR_GPO',
              ),
            ),
          ),
          10 => 
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL13',
            'label' => 'LBL_RECORDVIEW_PANEL13',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'id_linea_anticipo_c',
                'label' => 'LBL_ID_LINEA_ANTICIPO_C',
              ),
              1 => 
              array (
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'saldo_favor_c',
                'label' => 'LBL_SALDO_FAVOR_C',
              ),
            ),
          ),
          11 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_RECORD_SHOWMORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'collapsed',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
              1 => 
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' => 
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
              2 => 
              array (
                'name' => 'estado_interfaz_pos_c',
                'label' => 'LBL_ESTADO_INTERFAZ_POS_C',
              ),
              3 => 'ownership',
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
