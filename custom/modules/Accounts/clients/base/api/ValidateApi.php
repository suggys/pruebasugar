<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

require_once 'include/api/SugarApi.php';
require_once('include/SugarQuery/SugarQuery.php');

class ValidateApi extends SugarApi
{

	public function registerApiRest() {
		return array(
            'filterModuleGet' => array(
                'reqType' => 'GET',
                'path' => array('Accounts','validate'),
                'pathVars' => array('',''),
                'method' => 'validateAccount',
                //'jsonParams' => array('filter'),
                'shortHelp' => 'Lists validated accounts to POS.',
                'longHelp' => '',
                'exceptions' => array(
                    // Thrown in filterList
                    'SugarApiExceptionInvalidParameter',
                    // Thrown in filterListSetup and parseArguments
                    'SugarApiExceptionNotAuthorized',
                ),
            ),
        );
    }

	function validateAccount($api, $args)
	{
		global $app_list_strings;

		$error_handler = array();
		$foundError = false;
		$data = 'saludos';
		$id = '';
		$amount = 0;
		$esPago = 0;
		$isIdLineaAnticipo = false;

		if(!isset($args['amount']) || empty($args['amount']))
		{
			$error_handler['code'] = '400';
			$error_handler['status'] = 'SugarCRM: La solicitud contiene sintaxis erronea y no deberia repetirse';
			return $error_handler;
		}

		$amount = $args['amount'];
		$esPago = (isset($args['es_pago']) && !empty($args['es_pago'])) ? $args['es_pago'] : 0;

		// 3) búsqueda de la cuenta Cliente Crédito AR cliente por medio del "ID AR Credit" o "ID Línea de Anticipo"
		if((isset($args['id_ar_credit_c']) && !empty($args['id_ar_credit_c']))
			|| (isset($args['id_linea_anticipo_c']) && !empty($args['id_linea_anticipo_c']))
		){

			$id = (isset($args['id_ar_credit_c']) && !empty($args['id_ar_credit_c'])) ? $args['id_ar_credit_c'] : '';

			if($id === '')
			{
				$id = (isset($args['id_linea_anticipo_c']) && !empty($args['id_linea_anticipo_c'])) ? $args['id_linea_anticipo_c'] : '';
				$isIdLineaAnticipo = true;
			}
			// Se obtiene el id pos si es que llegara a venir
			$idPos = null;
			if(strpos($id, "|") !== false){
				$ids = explode("|", $id);
				$id = $ids[0];
				$idPos = $ids[1];

				if(empty($id) && !empty($idPos)){
					$resp = $this->procesa_cliente_comercial($id,$idPos, $args);
					return $resp;
				}
			}

			// Comprueba que realmente el id_ar_credit_c no sea un id_linea_anticipo_c
			if(!$isIdLineaAnticipo){
				$isIdLineaAnticipo = substr($id, -4) < 7000 ? false : true;
			}
			//4) búsqueda de la cuenta Cliente Crédito AR cliente por medio del "ID AR Credit" o "ID Línea de Anticipo"
			if(empty($id)){
				$error_handler['code'] = '303';
				$error_handler['status'] = 'SugarCRM: No se encontro id_ar_credit_c o id_linea_anticipo_c.';
			}
			else{

				$data = $this->getAccount($id, $isIdLineaAnticipo);
				if(!empty($data[0]))
				{
					$objAccount = $data[0]; //4.a) Existe
					$account = BeanFactory::getBean('Accounts', $objAccount['id']);
					if($idPos){
						if($idPos !== $account->id_pos_c){
							$idText = $isIdLineaAnticipo ? "Linea de Anticipo" : "Ar Credit";
							$error_handler['code'] = '303';
							$error_handler['status'] = 'SugarCRM: El ID '.$idText.' no corresponde al ID POS.';
							return $error_handler;
						}
					}
					//5.a) cuenta Cliente Crédito AR está activa
					if($account->estado_cuenta_c === 'Activo'){

						if($esPago)
						{
							$error_handler['code'] = '200';
							$error_handler['status'] = 'SugarCRM: El proceso de validacion es correcto para la Cuenta '. $account->name . '.';
							return $error_handler;
						}


						if($isIdLineaAnticipo){ //6) Si es "ID Línea de Anticipo"
							return $this->validateLineaAnticipo($amount, $account, $error_handler);
						}
						else //7) Si es "ID AR Credit"
						{
							$grupoEmpresa = null;
							//Pertenece a un grupo de empresas?
							if(!empty($account->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida))
							{
								return $this->validateGrupoEmpresas($amount, $account, $error_handler);
							}
							else //No pertenece a algun grupo de empresas
							{
								return $this->validateAccountAmount($amount, $account, $error_handler);
							}
						}
					}
					else //5.b) Cliente inactivo
					{
						$autorizacionEspecial = $this->getAutorizacionEspecial($account->id, $amount);
						if($autorizacionEspecial){
							return $this->validateAutorizacionEspecial($amount, $account, $autorizacionEspecial,  $error_handler);
						}
						else{
							$error_handler['code'] = '302';
							$error_handler['status'] = 'SugarCRM: Cliente Inactivo.';
						}
					}
				}
				else //4.b) No existe
				{
					$error_handler['code'] = '303';
					$error_handler['status'] = 'SugarCRM: No se encontro id_ar_credit_c o id_linea_anticipo_c.';
				}
			}
		}
		else
		{
			$error_handler['code'] = '400';
			$error_handler['status'] = 'SugarCRM: La solicitud contiene sintaxis erronea y no deberia repetirse.';
		}

		return $error_handler;
	}

	function getAccount($id, $isIdLineaAnticipo)
	{
		$account_bean = BeanFactory::newBean('Accounts');
        $sugarQuery = new SugarQuery();
        $sugarQuery->from($account_bean, array('team_security' => false));
        if($isIdLineaAnticipo)
        {
        	$sugarQuery->where()->equals('id_linea_anticipo_c', $id);
        }
        else
        {
        	$sugarQuery->where()->equals('id_ar_credit_c', $id);
        }
        $sugarQuery->limit(1);

		return $sugarQuery->execute();
	}

	function validaAutEspxFecha($beanAutEsp, $Prom_pago = false){
		global $app_list_strings, $timedate, $current_user;
		if(!$beanAutEsp->fecha_inicio_c)return false;
		$AutEspValida = false;
		$FechaAct = new SugarDateTime('now');
		$FechaAct = $timedate->tzUser($FechaAct, $current_user)->setTime(0,0);//->asDb();
		$F_Inicio = $timedate->fromDb($beanAutEsp->fecha_inicio_c);

		$fechaFinAux = $beanAutEsp->motivo_solicitud_c === "promesa_de_pago" ? $beanAutEsp->fecha_cumplimiento_c : $beanAutEsp->fecha_fin_c;

		$F_fin = $timedate->fromDb($fechaFinAux);
		if(!$F_Inicio)return false;
		if(!$F_fin)return false;
		if($FechaAct >= $F_Inicio && $FechaAct <= $F_fin){
			$AutEspValida = true;
		}
		return $AutEspValida;
	}

	function getAutorizacionEspecial($accountId, $amount)
	{
		global $timedate;
		$now = new SugarDateTime('now');
		$nowDb = $timedate->asDb($now);

		$autorizacionEspecial = BeanFactory::newBean('lowae_autorizacion_especial');
		$sugarQuery = new SugarQuery();
		$sugarQuery->from($autorizacionEspecial, array('team_security'=>false));
		$accounts = $sugarQuery->join('accounts_lowae_autorizacion_especial_1', array('team_security'=>false))->joinName();
		$sugarQuery->joinTable('accounts_cstm', array('alias' => 'ac', 'team_security'=>false))->on()
		->equalsField("$accounts.id","ac.id_c");
		$sugarQuery->where()
			->equals('estado_autorizacion_c', 'autorizada')
			->equals("$accounts.id", $accountId)
			->equals("ac.estado_bloqueo_c","Bloqueado")
			->gt('saldo_pendiente_aplicar_c', 0);

		$sugarQuery->whereRaw("(
				lowae_autorizacion_especial.motivo_solicitud_c = 'promesa_de_pago'
				AND lowae_autorizacion_especial.fecha_inicio_c <= '$nowDb'
				AND lowae_autorizacion_especial.fecha_cumplimiento_c >= '$nowDb'
		 	)
			OR
			(
				lowae_autorizacion_especial.motivo_solicitud_c IN ('credito_disponible_temporal','otro')
				AND lowae_autorizacion_especial.fecha_inicio_c <= '$nowDb'
				AND lowae_autorizacion_especial.fecha_fin_c >= '$nowDb'
			)");
		$sugarQuery->select(array('id', 'name', 'saldo_pendiente_aplicar_c','motivo_solicitud_c'));
		$result = $sugarQuery->execute();
		$autorizacionEspecial = null;
		if(count($result)){
			$otro = null;
			$cuenta = BeanFactory::getBean('Accounts',$accountId);
			$existeAEOtro = null;

			foreach ($result as $key => $autorizacionData) {
				if($autorizacionData['motivo_solicitud_c'] === 'otro'){
					$existeAEOtro = $key;
					break;
				}
			}

			if(!is_null($existeAEOtro)){
				$otro = $result[$existeAEOtro];
			} else {
				switch ($cuenta->codigo_bloqueo_c) {
					case 'CV':
						foreach ($result as $autorizacionData) {
							if($autorizacionData['motivo_solicitud_c'] === 'promesa_de_pago'){
								$otro = $autorizacionData;
								break;
							}
						}
						break;
					case 'LE':
						foreach ($result as $autorizacionData) {
							if($autorizacionData['motivo_solicitud_c'] === 'credito_disponible_temporal'){
								$otro = $autorizacionData;
								break;
							}
						}
						break;
					default:
						break;
				}
			}
			$autorizacionEspecial = BeanFactory::getBean('lowae_autorizacion_especial', $otro['id']);
		}
		return $autorizacionEspecial;
	}

	function validateLineaAnticipo($amount, $account, &$error_handler){
		if($amount <= $account->saldo_favor_c){ //6.a) el importe de la venta es menor o igual al "Saldo a Favor" de la línea de anticipo de la cuenta Cliente Crédito AR
			$error_handler['code'] = '200';
			$error_handler['status'] = 'SugarCRM: El proceso de validacion es correcto.';
			$error_handler['amount'] = strval($account->saldo_favor_c);
		}
		else //6.b) saldo a favor insuficiente
		{
			$error_handler['code'] = '305';
			$error_handler['status'] = 'SugarCRM: Saldo a favor de linea de anticipo insuficiente.';
			$error_handler['amount'] = strval($account->saldo_favor_c);
		}
		$error_handler['status'] .= ' Disponible $'.number_format($account->saldo_favor_c, 2).'.';
		return $error_handler;
	}

	function validateGrupoEmpresas($amount, $account, &$error_handler){
		$creditoDisponibleFelxibleGrupo = 0;
		$grupoEmpresa = BeanFactory::getBean('GPE_Grupo_empresas', $account->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida);

		$autorizacionEspecial = $this->getAutorizacionEspecial($account->id, $amount);
		if($autorizacionEspecial){
			$error_handler = $this->validateAutorizacionEspecial($amount, $account, $autorizacionEspecial,  $error_handler);
			// return $error_handler;
			if(!empty($error_handler['code'])){
				return $error_handler;
			}
		}

		$error_handler = $this->validateAccountBloqueado($account, $error_handler, $grupoEmpresa);
		if(!empty($error_handler['code'])){
			return $error_handler;
		}

		if($grupoEmpresa->administrar_credito_grupal) //Se administra por credito grupal?
		{
			$creditoDisponibleFelxibleGrupo = $grupoEmpresa->credito_disponible_flexible_c;
			$amountMessage = $grupoEmpresa->credito_diponible_grupo_c;
			if($amount <= $creditoDisponibleFelxibleGrupo)
			{
				$error_handler['code'] = '200';
				$error_handler['status'] = 'SugarCRM: El proceso de validacion es correcto.';
			}
			else
			{
				$error_handler['code'] = '304'; //FIXED LOW-310
				$error_handler['status'] = 'SugarCRM: Limite de credito excedido.';
			}
			$error_handler['amount'] = strval($amountMessage);
		}
		else{
			return $this->validateAccountAmount($amount, $account, $error_handler);
		}
		$error_handler['status'] .= ' Disponible $'.number_format($amountMessage, 2).'.';
		return $error_handler;
	}

	public function validateAutorizacionEspecial($amount, $parentBean, $autorizacionEspecial, &$error_handler)
	{
		$availableAmount = $autorizacionEspecial->saldo_pendiente_aplicar_c + ($autorizacionEspecial->motivo_solicitud_c === "promesa_de_pago" || $autorizacionEspecial->motivo_solicitud_c === "otro" ? 0 : $parentBean->credito_disponible_flexible_c );
		if($amount <= $availableAmount){
			$message = "SugarCRM: ";
			switch ($autorizacionEspecial->motivo_solicitud_c) {
				case 'credito_disponible_temporal':
					$message .= 'Autorizacion especial, motivo: Credito disponible temporal.';
					break;
					case 'otro':
					$message .= 'Autorizacion especial, motivo: Otro.';
					break;
					case 'promesa_de_pago':
					$message .= 'El proceso de validacion es correcto.';
					break;
				default:
					$message .= 'El proceso de validacion es correcto.';
					break;
			}
			$error_handler['code'] = '200';
			$error_handler['status'] = $message;
			$error_handler['status'] .= ' Disponible $'.number_format($availableAmount, 2).'.';
			$error_handler['amount'] = strval($availableAmount);
		}
		return $error_handler;
	}

	public function validateAccountAmount($amount, $account, $error_handler)
	{
		$autorizacionEspecial = $this->getAutorizacionEspecial($account->id, $amount);
		if($account->estado_bloqueo_c === 'Bloqueado'){
			if($autorizacionEspecial){
				if(
					$autorizacionEspecial->motivo_solicitud_c === 'promesa_de_pago'
					|| $autorizacionEspecial->motivo_solicitud_c === 'otro'
					|| ($autorizacionEspecial->motivo_solicitud_c === 'credito_disponible_temporal'
					&& $account->codigo_bloqueo_c != "CV")
				){
					$error_handler = $this->validateAutorizacionEspecial($amount, $account, $autorizacionEspecial,  $error_handler);
					if(!empty($error_handler['code'])){
						return $error_handler;
					}
				}
			}
			return $this->validateAccountBloqueado($account, $error_handler);
		}

		if($amount <= $account->credito_disponible_flexible_c) //FIXED LOW-310
		{
			$error_handler['code'] = '200';
			$error_handler['status'] = 'SugarCRM: El proceso de validacion es correcto.';
		}
		else
		{
			if($autorizacionEspecial){
				$error_handler = $this->validateAutorizacionEspecial($amount, $account, $autorizacionEspecial,  $error_handler);
				if(!empty($error_handler['code'])){
					return $error_handler;
				}
			}
			$error_handler['code'] = '304'; //FIXED LOW-310
			$error_handler['status'] = 'SugarCRM: Limite de credito excedido.';
		}
		$error_handler['amount'] = strval($account->credito_disponible_c);
		$error_handler['status'] .= ' Disponible $'.number_format($account->credito_disponible_c, 2).'.';
		return $error_handler;
	}

	public function validateAccountBloqueado($account, &$error_handler, $grupoEmpresa = null)
	{
		global $app_list_strings;
		$message = "";
		$razon = $app_list_strings['account_codigo_bloqueo_c_list'][$account->codigo_bloqueo_c];
		$disponible = (!empty($grupoEmpresa) && $grupoEmpresa->administrar_credito_grupal) ? $grupoEmpresa->credito_diponible_grupo_c : $account->credito_disponible_c;
		switch ($account->codigo_bloqueo_c) {
			case 'CV':
				$message .= "Cartera vencida.";
				break;
			case 'CL':
			case 'CI':
			case 'CD':
			case 'DI':
			case 'LE':
				$message .= "Cliente Bloqueado - Razon ".$razon.".";
				break;
			case 'Otro':
				$razon .= " " . $account->description;
				$message .= 'Cliente Bloqueado - Razon '.$razon . '.';
				break;
			default:
				# code...
				break;
		}
		if($message){
			$error_handler['code'] = '301';
			$error_handler['status'] = "SugarCRM: " . $message. ( $account->codigo_bloqueo_c === "CL" || $account->codigo_bloqueo_c === "Otro" ? '': ' Disponible $'.number_format($disponible, 2).'.');
			$error_handler['amount'] = strval($disponible);
		}
		return $error_handler;
	}

	public function procesa_cliente_comercial($id, $id_pos, $args){
		$account = BeanFactory::GetBean('Accounts');
		$account->retrieve_by_string_fields( array( 'id_pos_c' => $id_pos ) );
		if(!empty($account->id)){
			if($account->estado_cuenta_c === 'Activo'){
				$error_handler['code'] = '200';
				$error_handler['status'] = 'SugarCRM: El proceso de validacion es correcto para la Cuenta.' . $account->name;
			}
			if ($account->estado_cuenta_c != 'Activo') {
				$error_handler['code'] = '302';
				$error_handler['status'] = 'SugarCRM: Cliente Inactivo.';
			}
		}
		else{
				$error_handler['code'] = '303';
				$error_handler['status'] = 'SugarCRM: El cliente comercial no es valido.';
		}
		return $error_handler;
	}

}
