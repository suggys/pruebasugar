<?php
require_once 'custom/modules/Accounts/clients/base/api/CarteraApi.php';

class PagosClientesApi extends SugarApi
{
	public function registerApiRest() {
		return array(
			'getPagoCliente' => array(
				'reqType' => 'GET',
				'path' => array('Accounts','pagocliente'),
				'pathVars' => array('',''),
				'method' => 'pagoCliente',
				'shortHelp' => 'Get pago cliente',
				'longHelp' => '',
			),
			'exportPagoCliente' => array(
				'reqType' => 'GET',
				'path' => array('Accounts','pagocliente','export'),
				'pathVars' => array('','','export'),
				'method' => 'pagoCliente',
				'shortHelp' => 'Export pago cliente',
				'longHelp' => '',
				'rawReply' => true,
				'allowDownloadCookie' => true,
			),
		);
	}

	public function pagoCliente($api, $args){
		$GLOBALS["log"]->fatal("args:". print_r($args, true));
		if(!isset($args['id_sucursal_c']))
		{
			return false;
		}
		$result = [
			'accounts' => []
		];

		$sucursal = $args['id_sucursal_c'];
		$hoy = new SugarDateTime('now');
		$fechaTransaccionInicio = !empty($args['fecha_transaccion_inicio']) ? $args['fecha_transaccion_inicio'] : $hoy->asDbDate();
		$fechaTransaccionFin = !empty($args['fecha_transaccion_fin']) ? $args['fecha_transaccion_fin'] : $hoy->asDbDate();
		$sucursal = $args['id_sucursal_c'];
		if($sucursal){
			$pagosFacturas = $this->getPagosFacturas($sucursal, $fechaTransaccionInicio, $fechaTransaccionFin);
			$result['accounts'] = array_values($pagosFacturas);
		}
		if(!empty($args['export'])){
			$headers = [
				'id_sucursal_c' => 'Tienda',
				'name' => "Nombre o Razón Social",
				'monto_pagos' => "Monto Pagos",
				'modo_pagos' => "Modo Pagos",
				"vigente" => "Vigente",
				"1a30" => "1 a 30",
				"31a60" => "31 a 60",
				"61a90" => "61 a 90",
				"91a120" => "91 a 120",
				"mas120" => "Mas 120",
			];
			$cartera = new CarteraApi();
			$content = $this->formatDataToCsv($headers, $result['accounts']);
			return $cartera->doExport($headers, $content);
		}
		return $result;
	}

	public function getPagosFacturas($sucursal, $fechaTransaccionInicio, $fechaTransaccionFin){
		global $db;
		$clientesPagos = [];

		$a = BeanFactory::getBean("Accounts");
		$sq = new SugarQuery();
		$sq->select()->fieldRaw("a.id, a.name, a_c.id_sucursal_c, pa.monto_pagos, pa.modo_pagos, pf.vigencia_c, pf.importe_facturas");
    $sq->from($a, array('alias'=>'a','team_security'=>false));
		$sq->joinTable("(
		  SELECT pa.lowes_pagos_accountsaccounts_ida account_id, sum(p.monto) monto_pagos, group_concat(
				DISTINCT forma_pago
				ORDER BY forma_pago DESC SEPARATOR ', '
			) modo_pagos FROM lowes_pagos p INNER JOIN lowes_pagos_accounts_c pa ON pa.lowes_pagos_accountslowes_pagos_idb = p.id AND pa.deleted = 0
		  WHERE p.fecha_transaccion BETWEEN '$fechaTransaccionInicio' AND '$fechaTransaccionFin' GROUP BY account_id
		)",array("alias" => "pa"))
		->on()->equalsField("a.id","pa.account_id");
		$sq->joinTable("(
			SELECT pa.lowes_pagos_accountsaccounts_ida account_id, fc.vigencia_c, sum(f.importe) importe_facturas
			FROM low01_pagos_facturas pf
			INNER JOIN low01_pagos_facturas_lf_facturas_c pff on pff.low01_pagos_facturas_lf_facturaslow01_pagos_facturas_idb = pf.id and pff.deleted = 0
			INNER JOIN low01_pagos_facturas_lowes_pagos_c pfp on pfp.low01_pagos_facturas_lowes_pagoslow01_pagos_facturas_idb = pf.id and pfp.deleted = 0
			INNER JOIN lf_facturas f ON f.id = pff.low01_pagos_facturas_lf_facturaslf_facturas_ida AND f.deleted = 0
			INNER JOIN lf_facturas_cstm fc ON f.id = fc.id_c
			INNER JOIN lowes_pagos p on p.id = pfp.low01_pagos_facturas_lowes_pagoslowes_pagos_ida AND p.deleted = 0
			INNER JOIN lowes_pagos_accounts_c pa ON pa.lowes_pagos_accountslowes_pagos_idb = p.id AND pa.deleted = 0
			WHERE pf.deleted = 0
			AND p.fecha_transaccion BETWEEN '$fechaTransaccionInicio' AND '$fechaTransaccionFin'
			AND p.deleted = 0
			GROUP BY account_id, vigencia_c
		)",array("alias"=>"pf","joinType"=>"LEFT OUTER"))
		->on()->equalsField("pf.account_id","a.id");
		$sq->whereRaw("a_c.id_sucursal_c = '$sucursal'");
		$sq->groupByRaw("id, name, id_sucursal_c, monto_pagos, modo_pagos, vigencia_c");
		// $GLOBALS["log"]->fatal("-----------------copia-------------------------------------->");
		// $GLOBALS["log"]->fatal($sq->compileSql());
		// $GLOBALS["log"]->fatal("-----------------copia-------------------------------------->");
		$result = $sq->execute();

		// $query = "SELECT a.id, a.name, ac.id_sucursal_c, pa.monto_pagos, pa.modo_pagos, pf.vigencia_c, pf.importe_facturas
		// FROM accounts a
		// INNER JOIN accounts_cstm ac ON ac.id_c = a.id
		// INNER JOIN (
		//   SELECT pa.lowes_pagos_accountsaccounts_ida account_id, sum(p.monto) monto_pagos, group_concat(
		// 		DISTINCT forma_pago
		// 		ORDER BY forma_pago DESC SEPARATOR ', '
		// 	) modo_pagos FROM lowes_pagos p INNER JOIN lowes_pagos_accounts_c pa ON pa.lowes_pagos_accountslowes_pagos_idb = p.id AND pa.deleted = 0
		//   WHERE p.fecha_transaccion BETWEEN '$fechaTransaccionInicio' AND '$fechaTransaccionFin' GROUP BY account_id
		// ) pa ON a.id = pa.account_id
		// LEFT OUTER JOIN (
		// 	SELECT pa.lowes_pagos_accountsaccounts_ida account_id, fc.vigencia_c, sum(f.importe) importe_facturas
		// 	FROM low01_pagos_facturas pf
		// 	INNER JOIN low01_pagos_facturas_lf_facturas_c pff on pff.low01_pagos_facturas_lf_facturaslow01_pagos_facturas_idb = pf.id and pff.deleted = 0
		// 	INNER JOIN low01_pagos_facturas_lowes_pagos_c pfp on pfp.low01_pagos_facturas_lowes_pagoslow01_pagos_facturas_idb = pf.id and pfp.deleted = 0
		// 	INNER JOIN lf_facturas f ON f.id = pff.low01_pagos_facturas_lf_facturaslf_facturas_ida AND f.deleted = 0
		// 	INNER JOIN lf_facturas_cstm fc ON f.id = fc.id_c
		// 	INNER JOIN lowes_pagos p on p.id = pfp.low01_pagos_facturas_lowes_pagoslowes_pagos_ida AND p.deleted = 0
		// 	INNER JOIN lowes_pagos_accounts_c pa ON pa.lowes_pagos_accountslowes_pagos_idb = p.id AND pa.deleted = 0
		// 	WHERE pf.deleted = 0
		// 	AND p.fecha_transaccion BETWEEN '$fechaTransaccionInicio' AND '$fechaTransaccionFin'
		// 	AND p.deleted = 0
		// 	GROUP BY account_id, vigencia_c
		// ) pf ON pf.account_id = a.id
		// WHERE a.deleted = 0
		// AND ac.id_sucursal_c = '$sucursal'
		// GROUP BY id, name, id_sucursal_c, monto_pagos, modo_pagos, vigencia_c";
		// $GLOBALS["log"]->fatal("-----------------original-------------------------------------->");
		// $GLOBALS["log"]->fatal($query);
		// $GLOBALS["log"]->fatal("-----------------original-------------------------------------->");

		// $result = $db->query($query);
		$clientesPagos = [];
		// while($pago = $db->fetchByAssoc($result)){
		foreach ($result as $key => $pago) {
			if(empty($clientesPagos[$pago['id']])){
				$clientesPagos[$pago['id']] = [
					'id' => $pago['id'],
					'name' => $pago['name'],
					'id_sucursal_c' => $pago['id_sucursal_c'],
					'monto_pagos' => $pago['monto_pagos'],
					'modo_pagos' => $pago['modo_pagos'],
					'vigente' => 0,
					'1a30' => 0,
					'31a60' => 0,
					'61a90' => 0,
					'91a120' => 0,
					'mas120' => 0,
				];
			}
			$clientesPagos[$pago['id']][$pago['vigencia_c']] = $pago['importe_facturas'];
		}
		return $clientesPagos;
	}
}
