<?php

require_once 'include/api/SugarApi.php';
require_once('include/export_utils.php');

class CarteraApi extends SugarApi
{
	public function registerApiRest() {
		return array(
			'filterModuleGet' => array(
				'reqType' => 'GET',
				'path' => array('Accounts','cartera',),
				'pathVars' => array('','',),
				'method' => 'getCarteraAccounts',
				'shortHelp' => 'Obtiene Cartera clientes.',
				'longHelp' => '',
			),
			'exportCarteraTienda' => array(
				'reqType' => 'GET',
				'path' => array('Accounts','cartera','export',),
				'pathVars' => array('','','export',),
				'method' => 'getCarteraAccounts',
				'shortHelp' => 'Exporta Cartera Clientes.',
				'longHelp' => '',
				'rawReply' => true,
				'allowDownloadCookie' => true,
			),

			'getCarteraTiendas' => array(
				'reqType' => 'GET',
				'path' => array('Accounts','cartera-tiendas',),
				'pathVars' => array('','',),
				'method' => 'getCarteraTiendas',
				'shortHelp' => 'Obtiene Cartera tiendas.',
				'longHelp' => '',

			),
			'exportCarteraTiendas' => array(
				'reqType' => 'GET',
				'path' => array('Accounts','cartera-tiendas','export'),
				'pathVars' => array('','','export'),
				'method' => 'getCarteraTiendas',
				'shortHelp' => 'Exporta Cartera tiendas.',
				'longHelp' => '',
				'rawReply' => true,
				'allowDownloadCookie' => true,
			),
		);
	}

	function getCarteraAccounts($api, $args)
	{
		global $app_list_strings;

		$error_handler = array();
		$foundError = false;
		$data = '';
		$id = '';

		$codigoBloqueo = empty($args['codigo_bloqueo_c']) ? 'TODOS' : $args['codigo_bloqueo_c'];
		$vigencia = empty($args['vigencia_c']) ? "" : $args['vigencia_c'];
		$fecha = new SugarDateTime('now');
		$fechaProyeccion = empty($args['fecha_proyeccion']) ? $fecha->asDbDate() : $args['fecha_proyeccion'];

		$sucursalCompra = false;
		if(!isset($args['sucursal']) || empty($args['sucursal']))
		{
			$sucursal = $args['sucursal_compra'];
			$accounts = $this->getAccountsByVentas($sucursal, $codigoBloqueo);
			$sucursalCompra = true;
		} else {
			$sucursal = $args['sucursal'];
			$accounts = $this->getAccountsBySucursal($sucursal, $codigoBloqueo);
		}

		if(count($accounts)){
			$saldos = $this->getSaldoFacturasAccountBySucursal($sucursal, $fechaProyeccion, $sucursalCompra);
			$pendienteFacturar = $this->getPendienteFacturarBySucursal($sucursal, $vigencia, $sucursalCompra);
			$accounts = $this->merge($accounts, $saldos, $pendienteFacturar);
			$accounts = array_values($accounts);
			$this->getTotals($accounts);
		}

		if($vigencia){
			$this->filterByVigencia($accounts, $vigencia);
		}

		$result['accounts'] = $accounts;
		$headers = [
			"name" => "Nombre o Razón Social",
			"id_ar_credit_c" => "ID AR Credit",
			"user_vendedor_c_full_name" => "Vendedor",
			"limite_credito_autorizado_c" => "Límite de Crédito Total Cliente",
			"credito_disponible_c" => "Crédito Disponible Total Cliente",
			"codigo_bloqueo_c" => "Código de Bloqueo",
			"plazo_pago_autorizado_c" => "Plazo de Pago Autorizado",
			"pendiente_facturar" => "Pendiente de Facturar",
			"vigente" => "Vigente",
			"1a15" => "1 a 15",
			"16a30" => "16 a 30",
			"31a60" => "31 a 60",
			"61a90" => "61 a 90",
			"91a120" => "91 a 120",
			"mas120" => "Mas 120",
			"total" => "Total",
			"total_vencido" => "Vencido",
			"porcentaje_vencido" => "Porcentaje Vencido",
			"impacto_cv" => "Impacto CV",
			"impacto_cv_120" => "Impacto CV 120",
		];
		if (isset($args['sucursal_compra'])){
			$headers = [
				"name" => "Nombre o Razón Social",
				"id_ar_credit_c" => "ID AR Credit",
				"id_sucursal_c"=>"Sucursal del Cliente",
				"user_vendedor_c_full_name" => "Vendedor",
				"limite_credito_autorizado_c" => "Límite de Crédito Total Cliente",
				"credito_disponible_c" => "Crédito Disponible Total Cliente",
				"codigo_bloqueo_c" => "Código de Bloqueo",
				"plazo_pago_autorizado_c" => "Plazo de Pago Autorizado",
				"pendiente_facturar" => "Pendiente de Facturar",
				"vigente" => "Vigente",
				"1a15" => "1 a 15",
				"16a30" => "16 a 30",
				"31a60" => "31 a 60",
				"61a90" => "61 a 90",
				"91a120" => "91 a 120",
				"mas120" => "Mas 120",
				"total" => "Total",
				"total_vencido" => "Vencido",
				"porcentaje_vencido" => "Porcentaje Vencido",
				"impacto_cv" => "Impacto CV",
				"impacto_cv_120" => "Impacto CV 120",
			];
		}

		if(!empty($args['export'])){
			$content = $this->formatDataToCsv($headers, $accounts);
			return $this->doExport($api, "cartera", $content);
		}
		else{
			return $result;
		}
	}

	public function getAccountsBySucursal($sucursal, $codigoBloqueo = "TODOS")
	{
		$accounts = array();
		$account = BeanFactory::getBean("Accounts");
		$sugarQuery = new SugarQuery();
		$sugarQuery->from($account, array('team_security'=>false));
		$sugarQuery->where()
		->equals('id_sucursal_c', $sucursal)
		->isNotEmpty('id_ar_credit_c')
		;
		if($codigoBloqueo != "TODOS"){
			$sugarQuery->where()
			->in('codigo_bloqueo_c', [$codigoBloqueo]);
		}

		$sugarQuery->select(array(
			'id',
			'name',
			'id_ar_credit_c',
			'id_pos_c',
			'limite_credito_autorizado_c',
			'credito_disponible_c',
			'saldo_deudor_c',
			'plazo_pago_autorizado_c',
			'estado_cuenta_c',
			'estado_bloqueo_c',
			'codigo_bloqueo_c',
			'user_vendedor_c',
			'id_sucursal_c',
		));

		$sugarQuery->orderBy('name' ,'ASC');
		$accounts = $sugarQuery->execute();
		return $accounts;
	}

	public function getAccountsByVentas($sucursal, $codigoBloqueo = "TODOS")
	{
		$accounts = array();
		$account = BeanFactory::getBean("Accounts");
		$sugarQuery = new SugarQuery();
		$sugarQuery->from($account, array('team_security'=>false));
		$sugarQuery->joinTable("accounts_lf_facturas_1_c")
		->on()->equalsField("accounts_lf_facturas_1_c.accounts_lf_facturas_1accounts_ida","id");
		$sugarQuery->joinTable("lf_facturas")
		->on()->equalsField("lf_facturas.id","accounts_lf_facturas_1_c.accounts_lf_facturas_1lf_facturas_idb")
		->equals("lf_facturas.deleted", 0);

		$sugarQuery->where()
		->isNotEmpty('id_ar_credit_c');
		$sugarQuery->whereRaw("SUBSTR(lf_facturas.no_ticket,2,4) = '$sucursal'");

		if($codigoBloqueo != "TODOS"){
			$sugarQuery->where()
			->in('codigo_bloqueo_c', [$codigoBloqueo]);
		}

		$sugarQuery->select(array(
			'id',
			'name',
			'id_ar_credit_c',
			'id_pos_c',
			'limite_credito_autorizado_c',
			'credito_disponible_c',
			'saldo_deudor_c',
			'plazo_pago_autorizado_c',
			'estado_cuenta_c',
			'estado_bloqueo_c',
			'codigo_bloqueo_c',
			'user_vendedor_c',
			'id_sucursal_c',
		));

		$sugarQuery->orderBy('name' ,'ASC');
		$accounts = $sugarQuery->execute();
		return $accounts;
	}

	public function getSaldoFacturasAccountBySucursal($sucursal, $fechaProyeccion, $sucursalCompra){
		$saldos = array();
		$whereSucursal = $sucursalCompra ? "SUBSTR(lf_facturas.no_ticket,2,4) = '$sucursal'" : "accounts_cstm.id_sucursal_c IN ('$sucursal')";
		$factura = BeanFactory::getBean("LF_Facturas");
		$sq = new SugarQuery();
	  $sq->from($factura, array('team_security'=>false));
		$sq->select()->fieldRaw("account_id id,
		CASE
		WHEN dias_vencidos < 1 THEN 'vigente'
		WHEN dias_vencidos < 15 THEN '1a15'
		WHEN dias_vencidos < 30 THEN '16a30'
		WHEN dias_vencidos < 60 THEN '31a60'
		WHEN dias_vencidos < 90 THEN '61a90'
		WHEN dias_vencidos < 120 THEN '91a120'
		ELSE 'mas120'
		END vigencia_c, SUM(facturas_vencidas.saldo) saldo");
		$sq->joinTable("(
			SELECT accounts_lf_facturas_1_c.accounts_lf_facturas_1accounts_ida account_id,
			lf_facturas.id, saldo, dt_crt,
			accounts_cstm.plazo_pago_autorizado_c dias,
			DATEDIFF('$fechaProyeccion', dt_crt) diffnow,
			DATEDIFF('$fechaProyeccion', dt_crt) - accounts_cstm.plazo_pago_autorizado_c dias_vencidos
			FROM lf_facturas
			INNER JOIN accounts_lf_facturas_1_c
			on lf_facturas.id = accounts_lf_facturas_1_c.accounts_lf_facturas_1lf_facturas_idb
			and accounts_lf_facturas_1_c.deleted = 0
			INNER JOIN accounts_cstm
			on accounts_cstm.id_c = accounts_lf_facturas_1_c.accounts_lf_facturas_1accounts_ida
			and accounts_lf_facturas_1_c.deleted = 0
			WHERE lf_facturas.deleted = 0
			AND lf_facturas.estado_tiempo IN ('vigente', 'vencida')
			AND $whereSucursal
		)",['alias'=>'facturas_vencidas'])->on()
    ->equalsField('facturas_vencidas.id','lf_facturas.id');
		$sq->groupByRaw("account_id, 2",false);
		$result = $sq->execute();

		foreach ($result as $key => $saldo) {

			if(empty($saldos[$saldo["id"]])){
				$saldos[$saldo["id"]] = [
					'id' => $saldo["id"],
					'vigente' => 0,
					'1a15' => 0,
					'16a30' => 0,
					'31a60' => 0,
					'61a90' => 0,
					'91a120' => 0,
					'mas120' => 0,
					'total' => 0,
					'total_vencido' => 0,
					'porcentaje_vencido' => "0.00%",
				];
			}

			$saldos[$saldo["id"]][$saldo["vigencia_c"]] = $saldo["saldo"];
			$saldos[$saldo["id"]]['total'] += $saldo["saldo"];

			if($saldo["vigencia_c"] !== 'vigente'){
				$saldos[$saldo["id"]]['total_vencido'] += $saldo["saldo"];
			}

			$saldos[$saldo["id"]]['porcentaje_vencido'] = number_format(($saldos[$saldo["id"]]['total_vencido']/$saldos[$saldo["id"]]['total'])*100,2)."%";
		}

		return $saldos;
	}

	public function merge($accounts, $saldos, $pendienteFacturar)
	{
		$accountsResult = array();

		foreach ($accounts as $account) {
			$accountsResult[$account['id']] = $account;
			if(!empty($saldos[$account['id']])){
				$saldo = $saldos[$account['id']];
				$accountsResult[$account['id']] = array_merge($account, $saldo);
			}
			else{
				$accountsResult[$account['id']] = array_merge($account, [
					'vigente' => 0,
					'1a15' => 0,
					'16a30' => 0,
					'31a60' => 0,
					'61a90' => 0,
					'91a120' => 0,
					'mas120' => 0,
					'total' => 0,
					'total_vencido' => 0,
					'porcentaje_vencido' => "0.00%",
					'impacto_cv_120' => '0.00%',
					'impacto_cv' => '0.00%',
				]);
			}

			if(!empty($pendienteFacturar[$account['id']])){
				$pendiente = $pendienteFacturar[$account['id']];
				$accountsResult[$account['id']] = array_merge($accountsResult[$account['id']], $pendiente);
			}
			else{
				$accountsResult[$account['id']] = array_merge($accountsResult[$account['id']], [
					'pendiente_facturar' => 0,
				]);
			}
		}
		return $accountsResult;
	}

	public function getPendienteFacturarBySucursal($sucursal, $sucursalCompra)
	{
		$pendienteFacturar = array();
		$pendienteFacturarNuevas = $this->getPendienteFacturarNuevas($sucursal, $sucursalCompra);
		$pendienteFacturarVentas = $this->getPendienteFacturarVentas($sucursal, $sucursalCompra);
		// $GLOBALS["log"]->fatal("getPendienteFacturarBySucursal:pendienteFacturarNuevas:".print_r($pendienteFacturarNuevas,1));
		// $GLOBALS["log"]->fatal("getPendienteFacturarBySucursal:pendienteFacturarVentas:".print_r($pendienteFacturarVentas,1));


		foreach ($pendienteFacturarNuevas as $key => $row) {
			if(empty($pendienteFacturar[$key])){
				$pendienteFacturar[$key] = [
					'id' => $key,
					'pendiente_facturar' => 0,
				];
			}
			$pendienteFacturar[$key]['pendiente_facturar'] += $row['pendiente_facturar'];
		}

		foreach ($pendienteFacturarVentas as $key => $row) {
			if(empty($pendienteFacturar[$key])){
				$pendienteFacturar[$key] = [
					'id' => $key,
					'pendiente_facturar' => 0,
				];
			}
			$pendienteFacturar[$key]['pendiente_facturar'] += $row['pendiente_facturar'];
		}
		// $GLOBALS["log"]->fatal("pendienteFacturar:".print_r($pendienteFacturar,1));
		return $pendienteFacturar;
	}

	public function getTotals(&$accounts){
		$accountTotals = array(
			'id' => '',
			'name' => 'Totales',
			'id_ar_credit_c' => '',
			'id_pos_c' => '',
			'plazo_pago_autorizado_c' => '',
			'estado_cuenta_c' => '',
			'estado_bloqueo_c' => '',
			'codigo_bloqueo_c' => '',
			'limite_credito_autorizado_c' => 0,
			'credito_disponible_c' => 0,
			'saldo_deudor_c' => 0,
			'pendiente_facturar' => 0,
			'vigente' => 0,
			'1a15' => 0,
			'16a30' => 0,
			'31a60' => 0,
			'61a90' => 0,
			'91a120' => 0,
			'mas120' => 0,
			'total' => 0,
			'total_vencido' => 0,
			'porcentaje_vencido' => '0.00%',
			'impacto_cv_120' => '0.00%',
			'impacto_cv' => '0.00%',
		);
		foreach ($accounts as $account) {
			$accountTotals['limite_credito_autorizado_c'] += $account['limite_credito_autorizado_c'];
			$accountTotals['credito_disponible_c'] += $account['credito_disponible_c'];
			$accountTotals['saldo_deudor_c'] += $account['saldo_deudor_c'];
			$accountTotals['saldo_deudor_c'] += $account['saldo_deudor_c'];
			$accountTotals['pendiente_facturar'] += $account['pendiente_facturar'];
			$accountTotals['vigente'] += $account['vigente'];
			$accountTotals['1a15'] += $account['1a15'];
			$accountTotals['16a30'] += $account['16a30'];
			$accountTotals['31a60'] += $account['31a60'];
			$accountTotals['61a90'] += $account['61a90'];
			$accountTotals['91a120'] += $account['91a120'];
			$accountTotals['mas120'] += $account['mas120'];
			$accountTotals['total'] += $account['total'] + $account['pendiente_facturar'];
			$accountTotals['total_vencido'] += $account['total_vencido'];
		}
		$accountTotals['porcentaje_vencido'] = ($accountTotals['total'] ? number_format(($accountTotals['total_vencido']/$accountTotals['total'])*100, 2) : "0.00")."%";

		for ($item=0; $item < count($accounts); $item++) {
			$accounts[$item]['total'] += $accounts[$item]['pendiente_facturar'];
			$accounts[$item]['impacto_cv'] = ($accounts[$item]['total_vencido'] ? number_format(($accounts[$item]['total_vencido']/$accountTotals['total_vencido'])*100, 2) : "0.00")."%";
			$accounts[$item]['impacto_cv_120'] = ($accounts[$item]['mas120'] ? number_format(($accounts[$item]['mas120']/$accountTotals['mas120'])*100, 2) : "0.00")."%";
			$accounts[$item]['user_vendedor_c_full_name'] = trim($accounts[$item]['rel_user_vendedor_c_first_name'] . " ". $accounts[$item]['rel_user_vendedor_c_last_name']);

		}

		$accounts[] = $accountTotals;

		$accountsPorcentajesTotales = array(
			'id' => '',
			'name' => 'Porcentajes totales',
			'id_ar_credit_c' => '',
			'id_pos_c' => '',
			'plazo_pago_autorizado_c' => '',
			'estado_cuenta_c' => '',
			'estado_bloqueo_c' => '',
			'codigo_bloqueo_c' => '',
			'limite_credito_autorizado_c' => '',
			'credito_disponible_c' => '',
			'saldo_deudor_c' => '',
			'pendiente_facturar' => '',
			'vigente' => ($accountTotals['total'] ? number_format(($accountTotals['vigente']/$accountTotals['total'])*100, 2) : "0.00")."%",
			'1a15' => ($accountTotals['total'] ? number_format(($accountTotals['1a15']/$accountTotals['total'])*100, 2) : "0.00")."%",
			'16a30' => ($accountTotals['total'] ? number_format(($accountTotals['16a30']/$accountTotals['total'])*100, 2) : "0.00")."%",
			'31a60' => ($accountTotals['total'] ? number_format(($accountTotals['31a60']/$accountTotals['total'])*100, 2) : "0.00")."%",
			'61a90' => ($accountTotals['total'] ? number_format(($accountTotals['61a90']/$accountTotals['total'])*100, 2) : "0.00")."%",
			'91a120' => ($accountTotals['total'] ? number_format(($accountTotals['91a120']/$accountTotals['total'])*100, 2) : "0.00")."%",
			'mas120' => ($accountTotals['total'] ? number_format(($accountTotals['mas120']/$accountTotals['total'])*100, 2) : "0.00")."%",
			'total' => ($accountTotals['total'] ? '100.00' : '0.00')."%",
			'total_vencido' => '',
			'porcentaje_vencido' => '',
			'impacto_cv_120' => '',
			'impacto_cv' => '',
		);

		$accounts[] = $accountsPorcentajesTotales;
	}

	public function filterByVigencia($accounts, $vigencia){
		$accountsResult = array();
		foreach ($accounts as $account) {
			if($account[$vigencia] > 0){
				$accountsResult[] = $account;
			}
		}
		return $accountsResult;
	}

	function getCarteraTiendas($api, $args){
		global $app_list_strings;
		$result = [];
		$tiendas = [];
		foreach ($app_list_strings['sucursal_c_list'] as $key => $value) {
			if(!$value) continue;
			$tiendas[$key] = [
				'name' => $value,
				'id' => $key,
				'id_sucursal_c' => $key,
				'limite_credito_autorizado_c' => 0,
				'credito_disponible_c' => 0,
				'saldo_deudor_c' => 0,
			];
		}
		$fecha = new SugarDateTime('now');

		$fechaProyeccion = empty($args['fecha_proyeccion']) ? $fecha->asDbDate() : $args['fecha_proyeccion'];

		$tiendas = $this->mergeTiendas($tiendas, $this->getTiendasGenerales(), $this->getTiendasCartera($fechaProyeccion), $this->getPendienteFacturarTienda());
		$tiendas = array_values($tiendas);
		$this->getTiendaTotales($tiendas);

		if(!empty($args['export'])){
			if($args['mode'] === "estatus"){
				$headers = [
					"name" => "Tienda",
					"pendiente_facturar" => "Pendiente de Facturar",
					"porcentaje_pendiente_facturar" => "Porcentaje Pendiente Facturar",
					"vigente" => "Vigente",
					"porcentaje_vigente" => "Porcentaje Vigente",
					"total_vencido" => "Vencido",
					"porcentaje_vencido" => "Porcentaje Vencido",
					"total" => "Total",
					"limite_credito_autorizado_c" => "Límite de Crédito Autorizado",
					"disponible_excedido" => "Disponible Excedido",
				];
			}
			else{
				$headers = [
					"name" => "Tienda",
					"pendiente_facturar" => "Pendiente de Facturar",
					"vigente" => "Vigente",
					"1a15" => "1 a 15",
					"16a30" => "16 a 30",
					"31a60" => "31 a 60",
					"61a90" => "61 a 90",
					"91a120" => "91 a 120",
					"mas120" => "Mas 120",
					"total_vencido" => "Vencido",
					"total" => "Total",
				];
			}
			$content = $this->formatDataToCsv($headers, $tiendas);
			return $this->doExport($api, "cartera-tiendas", $content);
		}
		else{
			$result['tiendas'] = $tiendas;
			return $result;
		}
	}

	public function getTiendasGenerales(){
		$tiendas = [];

		$sq = new SugarQuery();
		$sq->select(array('id_sucursal_c'))
		->fieldRaw("SUM(accounts_cstm.limite_credito_autorizado_c)","limite_credito_autorizado_c")
		->fieldRaw("SUM(accounts_cstm.credito_disponible_c)","credito_disponible_c")
		->fieldRaw("SUM(accounts_cstm.saldo_deudor_c)","saldo_deudor_c");
		$sq->from(BeanFactory::newBean('Accounts'), array('team_security'=>false));
		$sq->where()->notEquals('id_sucursal_c','');
		$sq->whereRaw("accounts_cstm.id_sucursal_c IS NOT NULL");
		$sq->groupBy('id_sucursal_c');
		$result = $sq->execute();

		foreach ($result as $tienda) {
			$tiendas[$tienda['id_sucursal_c']] = $tienda;
		}

		return $tiendas;
	}

	public function getTiendasCartera($fechaProyeccion = "", $sucursal = ""){
		$cartera = [];
		$tiendas = [];

		$whereSucursal = $sucursal ? " AND accounts_cstm.id_sucursal_c IN ('$sucursal') " : "";
		$factura = BeanFactory::getBean("LF_Facturas");
		$sq = new SugarQuery();
	  $sq->from($factura, array('team_security'=>false));
		$sq->select()->fieldRaw("id_sucursal_c,
		CASE
		WHEN dias_vencidos < 1 THEN 'vigente'
		WHEN dias_vencidos < 15 THEN '1a15'
		WHEN dias_vencidos < 30 THEN '16a30'
		WHEN dias_vencidos < 60 THEN '31a60'
		WHEN dias_vencidos < 90 THEN '61a90'
		WHEN dias_vencidos < 120 THEN '91a120'
		ELSE 'mas120'
		END vigencia_c,
		SUM(facturas_vencidas.saldo) saldo");
		$sq->joinTable("(
			SELECT accounts_cstm.id_sucursal_c, lf_facturas.id, saldo, dt_crt,
			accounts_cstm.plazo_pago_autorizado_c dias,
			DATEDIFF('$fechaProyeccion', dt_crt) diffnow,
			DATEDIFF('$fechaProyeccion', dt_crt) - accounts_cstm.plazo_pago_autorizado_c dias_vencidos
			FROM lf_facturas
			INNER JOIN accounts_lf_facturas_1_c on lf_facturas.id = accounts_lf_facturas_1_c.accounts_lf_facturas_1lf_facturas_idb and accounts_lf_facturas_1_c.deleted = 0
			INNER JOIN accounts_cstm on accounts_cstm.id_c = accounts_lf_facturas_1_c.accounts_lf_facturas_1accounts_ida and accounts_lf_facturas_1_c.deleted = 0
			WHERE lf_facturas.deleted = 0
			AND lf_facturas.estado_tiempo IN ('vigente', 'vencida')
			$whereSucursal
			)",['alias'=>'facturas_vencidas'])->on()
    ->equalsField('facturas_vencidas.id','lf_facturas.id');
		$sq->groupByRaw("id_sucursal_c, 2",false);
		$result = $sq->execute();

			foreach ($result as $key => $tienda) {
				if(empty($cartera[$tienda['id_sucursal_c']])){
					$cartera[$tienda['id_sucursal_c']] = [
						'vigente' => 0,
						'1a15' => 0,
						'16a30' => 0,
						'31a60' => 0,
						'61a90' => 0,
						'91a120' => 0,
						'mas120' => 0,
						'total' => 0,
						'total_vencido' => 0,
						'porcentaje_vencido' => "0.00%",
						'porcentaje_vigente' => "0.00%",
					];
				}
				$cartera[$tienda["id_sucursal_c"]][$tienda["vigencia_c"]] = $tienda["saldo"];
				$cartera[$tienda["id_sucursal_c"]]['total'] += $tienda["saldo"];

				if($tienda["vigencia_c"] !== 'vigente'){
					$cartera[$tienda["id_sucursal_c"]]['total_vencido'] += $tienda["saldo"];
				}


			}

			return $cartera;
		}

		public function mergeTiendas($tiendas, $tiendaGenerales,$carteras, $pendienteFacturar){
			$tiendasResult = array();

			foreach ($tiendas as $tienda) {
				$tiendasResult[$tienda['id_sucursal_c']] = $tienda;
				if(!empty($tiendaGenerales[$tienda['id_sucursal_c']])){
					$tiendasResult[$tienda['id_sucursal_c']] = array_merge($tiendasResult[$tienda['id_sucursal_c']], $tiendaGenerales[$tienda['id_sucursal_c']]);
				}
				if(!empty($carteras[$tienda['id_sucursal_c']])){
					$cartera = $carteras[$tienda['id_sucursal_c']];
					$tiendasResult[$tienda['id_sucursal_c']] = array_merge($tiendasResult[$tienda['id_sucursal_c']], $cartera);
				}
				else{
					$tiendasResult[$tienda['id_sucursal_c']] = array_merge($tiendasResult[$tienda['id_sucursal_c']], [
						'vigente' => 0,
						'1a15' => 0,
						'16a30' => 0,
						'31a60' => 0,
						'61a90' => 0,
						'91a120' => 0,
						'mas120' => 0,
						'total' => 0,
						'total_vencido' => 0,
						'porcentaje_vencido' => "0.00%",
						'porcentaje_vigente' => "0.00%",
					]);
				}

				if(!empty($pendienteFacturar[$tienda['id_sucursal_c']])){
					$pendiente = $pendienteFacturar[$tienda['id_sucursal_c']];
					$tiendasResult[$tienda['id_sucursal_c']] = array_merge($tiendasResult[$tienda['id_sucursal_c']], $pendiente);
				}
				else{
					$tiendasResult[$tienda['id_sucursal_c']] = array_merge($tiendasResult[$tienda['id_sucursal_c']], [
						'pendiente_facturar' => 0,
					]);
				}
			}
			return $tiendasResult;
		}

		public function getTiendaTotales(&$tiendas){
			$tiendasTotales = [
				'name' => 'Totales',
				'pendiente_facturar' => 0,
				'porcentaje_pendiente_facturar' => '-',
				'vigente' => 0,
				'porcentaje_vigente' => '-',
				'1a15' => 0,
				'16a30' => 0,
				'31a60' => 0,
				'61a90' => 0,
				'91a120' => 0,
				'mas120' => 0,
				'total' => 0,
				'total_vencido' => 0,
				'porcentaje_vencido' => '-',
				'limite_credito_autorizado_c' => 0,
				'credito_disponible_c' => 0,
			];

			foreach ($tiendas as $tienda) {
				$tiendasTotales['pendiente_facturar'] += $tienda['pendiente_facturar'];
				$tiendasTotales['vigente'] += $tienda['vigente'];
				$tiendasTotales['1a15'] += $tienda['1a15'];
				$tiendasTotales['16a30'] += $tienda['16a30'];
				$tiendasTotales['31a60'] += $tienda['31a60'];
				$tiendasTotales['61a90'] += $tienda['61a90'];
				$tiendasTotales['91a120'] += $tienda['91a120'];
				$tiendasTotales['mas120'] += $tienda['mas120'];
				$tiendasTotales['total'] += $tienda['total'] + $tienda['pendiente_facturar'];
				$tiendasTotales['total_vencido'] += $tienda['total_vencido'];
				$tiendasTotales['limite_credito_autorizado_c'] += $tienda['limite_credito_autorizado_c'];
				$tiendasTotales['credito_disponible_c'] += $tienda['credito_disponible_c'];
			}
			for ($item=0; $item < count($tiendas); $item++) {
				$tiendas[$item]['total'] += $tiendas[$item]['pendiente_facturar'];
				$tiendas[$item]['disponible_excedido'] = $tiendas[$item]['limite_credito_autorizado_c'] - $tiendas[$item]['total'];
				$tiendas[$item]['porcentaje_pendiente_facturar'] = $tiendas[$item]['total'] ? number_format(($tiendas[$item]['pendiente_facturar']/$tiendas[$item]['total'])*100,2)."%" : "0.00%";
				$tiendas[$item]['porcentaje_vigente'] = $tiendas[$item]['total'] ? number_format(($tiendas[$item]['vigente']/$tiendas[$item]['total'])*100,2)."%" : "0.00%";
				$tiendas[$item]['porcentaje_vencido'] = $tiendas[$item]['total'] ? number_format(($tiendas[$item]['total_vencido']/$tiendas[$item]['total'])*100,2)."%" : "0.00%";
			}

			$tiendas[] = $tiendasTotales;

			$tiendas[] = [
				'name' => 'Porcentajes Totales',
				'pendiente_facturar' => $tiendasTotales['total'] ? number_format(($tiendasTotales['pendiente_facturar']/$tiendasTotales['total'])*100,2)."%" : "0.00%",
				'porcentaje_pendiente_facturar' => '-',
				'vigente' => $tiendasTotales['total'] ? number_format(($tiendasTotales['vigente']/$tiendasTotales['total'])*100,2)."%" : "0.00%",
				'porcentaje_vigente' => '-',
				'1a15' => $tiendasTotales['total'] ? number_format(($tiendasTotales['1a15']/$tiendasTotales['total'])*100,2)."%" : "0.00%",
				'16a30' => $tiendasTotales['total'] ? number_format(($tiendasTotales['16a30']/$tiendasTotales['total'])*100,2)."%" : "0.00%",
				'31a60' => $tiendasTotales['total'] ? number_format(($tiendasTotales['31a60']/$tiendasTotales['total'])*100,2)."%" : "0.00%",
				'61a90' => $tiendasTotales['total'] ? number_format(($tiendasTotales['61a90']/$tiendasTotales['total'])*100,2)."%" : "0.00%",
				'91a120' => $tiendasTotales['total'] ? number_format(($tiendasTotales['91a120']/$tiendasTotales['total'])*100,2)."%" : "0.00%",
				'mas120' => $tiendasTotales['total'] ? number_format(($tiendasTotales['mas120']/$tiendasTotales['total'])*100,2)."%" : "0.00%",
				'total_vencido' => $tiendasTotales['total'] ? number_format(($tiendasTotales['total_vencido']/$tiendasTotales['total'])*100,2)."%" : "0.00%",
				'porcentaje_vencido' => '-',
				'total' => $tiendasTotales['total'] ? "100.00%" : "0.00%",
			];

		}

		public function getPendienteFacturarTienda()
		{
			$pendienteFacturar = [];

			$sq = new SugarQuery();
		  $sq->select()->fieldRaw("id_sucursal_c, sum(amount_c) pendiente_facturar");
		  $sq->from(BeanFactory::newBean('Accounts'),array('team_security'=>false));
			$sq->joinTable("(
				SELECT
				accounts_orden_ordenes_1_c.accounts_orden_ordenes_1accounts_ida account_id, orden_ordenes_cstm.amount_c
				FROM orden_ordenes
				INNER JOIN orden_ordenes_cstm ON orden_ordenes.id = orden_ordenes_cstm.id_c
				INNER JOIN accounts_orden_ordenes_1_c ON accounts_orden_ordenes_1_c.accounts_orden_ordenes_1orden_ordenes_idb = orden_ordenes.id AND accounts_orden_ordenes_1_c.deleted = 0
				LEFT OUTER JOIN lf_facturas_orden_ordenes_c on lf_facturas_orden_ordenes_c.lf_facturas_orden_ordenesorden_ordenes_ida = orden_ordenes.id AND lf_facturas_orden_ordenes_c.deleted = 0
				WHERE orden_ordenes.deleted = 0
				AND lf_facturas_orden_ordenes_c.lf_facturas_orden_ordeneslf_facturas_idb IS NULL
				AND (orden_ordenes_cstm.estado_c IN ('') OR orden_ordenes_cstm.estado_c IS NULL)

				UNION ALL

				SELECT
				accounts_orden_ordenes_1_c.accounts_orden_ordenes_1accounts_ida account_id, orden_ordenes_cstm.amount_c
				FROM orden_ordenes
				INNER JOIN orden_ordenes_cstm ON orden_ordenes.id = orden_ordenes_cstm.id_c
				INNER JOIN accounts_orden_ordenes_1_c ON accounts_orden_ordenes_1_c.accounts_orden_ordenes_1orden_ordenes_idb = orden_ordenes.id
				LEFT OUTER JOIN orden_ordenes_orden_ordenes_1_c ON orden_ordenes_orden_ordenes_1_c.orden_ordenes_orden_ordenes_1orden_ordenes_ida = orden_ordenes.id AND orden_ordenes_orden_ordenes_1_c.deleted = 0
				WHERE orden_ordenes.deleted = 0
				AND orden_ordenes_orden_ordenes_1_c.orden_ordenes_orden_ordenes_1orden_ordenes_idb IS NULL
				AND orden_ordenes_cstm.estado_c IN ('0')
				AND accounts_orden_ordenes_1_c.deleted = 0

				UNION ALL

				SELECT aoi.accounts_orden_ordenes_1accounts_ida account_id, pendiente_facturar amount_c
				FROM accounts_orden_ordenes_1_c aoi,
				(
					SELECT f.id, f.name, f.total, sum(p.subtotal), IFNULL(n.facturado, 0) facturado, (f.total+IFNULL(devoluciones.amount_c,0))-sum(IFNULL(n.facturado, 0)) pendiente_facturar
					FROM
					orden_ordenes_orden_ordenes_1_c m
					INNER JOIN (SELECT a.id, a.name, b.amount_c total FROM orden_ordenes a,  orden_ordenes_cstm b WHERE a.id=b.id_c AND b.estado_c IN ('0') AND a.deleted=0) f ON f.id=m.orden_ordenes_orden_ordenes_1orden_ordenes_ida
					INNER JOIN (SELECT a.id,b.amount_c subtotal FROM orden_ordenes a,  orden_ordenes_cstm b WHERE a.id=b.id_c AND b.estado_c IN ('2','5') AND a.deleted = 0 ) p ON p.id=m.orden_ordenes_orden_ordenes_1orden_ordenes_idb
					LEFT OUTER JOIN (
						SELECT fo.lf_facturas_orden_ordenesorden_ordenes_ida, oc.amount_c facturado
						FROM  lf_facturas_orden_ordenes_c fo
						INNER JOIN orden_ordenes_cstm oc ON oc.id_c = fo.lf_facturas_orden_ordenesorden_ordenes_ida
						WHERE fo.deleted = 0
					) n ON m.orden_ordenes_orden_ordenes_1orden_ordenes_idb = n.lf_facturas_orden_ordenesorden_ordenes_ida
					LEFT OUTER JOIN (SELECT oioh.orden_ordenes_orden_ordenes_1orden_ordenes_ida, b.amount_c  FROM orden_ordenes a,  orden_ordenes_cstm b, orden_ordenes_orden_ordenes_1_c ohod, orden_ordenes_orden_ordenes_1_c oioh WHERE a.id=b.id_c AND b.estado_c IN ('6') AND a.deleted = 0 AND ohod.orden_ordenes_orden_ordenes_1orden_ordenes_idb = a.id AND ohod.deleted = 0 AND oioh.orden_ordenes_orden_ordenes_1orden_ordenes_idb = ohod.orden_ordenes_orden_ordenes_1orden_ordenes_ida) as devoluciones ON m.orden_ordenes_orden_ordenes_1orden_ordenes_ida = devoluciones.orden_ordenes_orden_ordenes_1orden_ordenes_ida
					WHERE  m.deleted = 0
					GROUP BY f.id, f.name, f.total
					HAVING pendiente_facturar > 0
				) oi
				WHERE aoi.accounts_orden_ordenes_1orden_ordenes_idb = oi.id
				AND aoi.deleted = 0
			)",['alias'=>'aov'])->on()->equalsField('aov.account_id','accounts.id');
			$sq->groupByRaw("id_sucursal_c",false);
			$result = $sq->execute();

			foreach ($result as $key => $pendiente) {
				$pendienteFacturar[$pendiente["id_sucursal_c"]] = $pendiente;
			}

			return $pendienteFacturar;
		}

		public function getPendienteFacturarNuevas($sucursal, $sucursalCompra = false){
			$data = [];
			$bean = BeanFactory::getBean("orden_Ordenes");
			$sugarQuery = new SugarQuery();
	    $sugarQuery->from($bean, array('team_security'=>false));
	    $accounts = $sugarQuery->join('accounts_orden_ordenes_1', array('team_security'=>false))->joinName();
			$sugarQuery->joinTable("(SELECT orden_ordenes_orden_ordenes_1_c.orden_ordenes_orden_ordenes_1orden_ordenes_ida id
			  FROM orden_ordenes
			  JOIN orden_ordenes_cstm ON orden_ordenes_cstm.id_c = orden_ordenes.id
				JOIN orden_ordenes_orden_ordenes_1_c ON (orden_ordenes_orden_ordenes_1_c.orden_ordenes_orden_ordenes_1orden_ordenes_idb = orden_ordenes.id AND orden_ordenes_orden_ordenes_1_c.deleted = 0)
			  WHERE orden_ordenes.deleted = 0 AND orden_ordenes_cstm.estado_c IN ('4'))", ["alias" => "ordenes_canceladas", 'joinType' => 'LEFT'])
				->on()->equalsField("ordenes_canceladas.id",  "orden_ordenes.id");
			$sugarQuery->joinTable("(SELECT orden_ordenes_orden_ordenes_1_c.orden_ordenes_orden_ordenes_1orden_ordenes_ida id,
				SUM(lf_facturas.importe) facturado
				FROM orden_ordenes
				JOIN orden_ordenes_cstm ON orden_ordenes_cstm.id_c = orden_ordenes.id
				JOIN orden_ordenes_orden_ordenes_1_c ON (orden_ordenes_orden_ordenes_1_c.orden_ordenes_orden_ordenes_1orden_ordenes_idb = orden_ordenes.id AND orden_ordenes_orden_ordenes_1_c.deleted = 0)
				JOIN lf_facturas_orden_ordenes_c ON (lf_facturas_orden_ordenes_c.lf_facturas_orden_ordenesorden_ordenes_ida = orden_ordenes.id AND lf_facturas_orden_ordenes_c.deleted = 0)
				JOIN lf_facturas ON (lf_facturas.id = lf_facturas_orden_ordenes_c.lf_facturas_orden_ordeneslf_facturas_idb AND lf_facturas.deleted = 0)
				WHERE orden_ordenes.deleted = 0 AND orden_ordenes_cstm.estado_c IN ('2','5','8','9')
				GROUP BY 1)",
				["alias" => "ordenes_facturadas", 'joinType' => 'LEFT'])
				->on()->equalsField("ordenes_facturadas.id",  "orden_ordenes.id");
			$sugarQuery->joinTable("(SELECT orden_ordenes_orden_ordenes_1_c.orden_ordenes_orden_ordenes_1orden_ordenes_ida id,
				SUM(orden_ordenes_cstm.sub_total_c) cancelado
				FROM orden_ordenes
				JOIN orden_ordenes_cstm ON orden_ordenes_cstm.id_c = orden_ordenes.id
				JOIN orden_ordenes_orden_ordenes_1_c ON (orden_ordenes_orden_ordenes_1_c.orden_ordenes_orden_ordenes_1orden_ordenes_idb = orden_ordenes.id AND orden_ordenes_orden_ordenes_1_c.deleted = 0)
				WHERE orden_ordenes_cstm.estado_c IN ('8','9')
				GROUP BY 1)",
				["alias" => "ordenes_canceladas_parcial_completada", 'joinType' => 'LEFT'])
				->on()->equalsField("ordenes_canceladas_parcial_completada.id",  "orden_ordenes.id");
			$sugarQuery->where()
	      ->in("estado_c", ["0"]);
			$sugarQuery->whereRaw("ordenes_canceladas.id IS NULL");
			if($sucursalCompra){
				$sugarQuery->whereRaw("SUBSTR(orden_ordenes_cstm.no_ticket_c,2,4) = '$sucursal'");
			}
			else{
				$sugarQuery->where()
		      ->in("$accounts.id_sucursal_c", [$sucursal]);
			}

			$sugarQuery->select([$accounts.".id"]);
			$sugarQuery->select()->fieldRaw(<<<'SQL'
				SUM(orden_ordenes_cstm.amount_c)-IFNULL(SUM(ordenes_facturadas.facturado),0)+IFNULL(SUM(ordenes_canceladas_parcial_completada.cancelado),0) pendiente_facturar
SQL
);
			$sugarQuery->groupByRaw('1');


			$result = $sugarQuery->execute();
			foreach ($result as $record) {
				$data[$record['id']] = $record;
			}
			return $data;
		}

		public function getPendienteFacturarVentas($sucursal, $sucursalCompra = false)
		{
			$data = [];
			$bean = BeanFactory::getBean("orden_Ordenes");
			$sugarQuery = new SugarQuery();
	    $sugarQuery->from($bean, array('team_security'=>false));
	    $accounts = $sugarQuery->join('accounts_orden_ordenes_1', array('team_security'=>false))->joinName();
			$sugarQuery->joinTable("lf_facturas_orden_ordenes_c", ['joinType' => 'LEFT'])
				->on()->equalsField("lf_facturas_orden_ordenes_c.lf_facturas_orden_ordenesorden_ordenes_ida",  "orden_ordenes.id")
				->equals('lf_facturas_orden_ordenes_c.deleted', 0);;
			$sugarQuery->where()
	      ->in("estado_c", [""]);
			$sugarQuery->whereRaw("lf_facturas_orden_ordenes_c.lf_facturas_orden_ordeneslf_facturas_idb IS NULL");
			if($sucursalCompra){
				$sugarQuery->whereRaw("SUBSTR(orden_ordenes_cstm.no_ticket_c,2,4) = '$sucursal'");
			}
			else{
				$sugarQuery->where()
		      ->in("$accounts.id_sucursal_c", [$sucursal]);
			}

			$sugarQuery->select([$accounts.".id"]);
			$sugarQuery->select()->fieldRaw(<<<'SQL'
				SUM(orden_ordenes_cstm.amount_c) pendiente_facturar
SQL
);
			$sugarQuery->groupByRaw('1');

			$result = $sugarQuery->execute();
			foreach ($result as $record) {
				$data[$record['id']] = $record;
			}

			return $data;
		}

		protected function doExport(ServiceBase $api, $filename, $content)
		{
			$api->setHeader("Pragma", "cache");
			$api->setHeader("Content-Type", "application/octet-stream; charset=" . $GLOBALS['locale']->getExportCharset());
			$api->setHeader("Content-Disposition", "attachment; filename={$filename}.csv");
			$api->setHeader("Content-transfer-encoding", "binary");
			$api->setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
			$api->setHeader("Last-Modified", TimeDate::httpTime());
			$api->setHeader("Cache-Control", "post-check=0, pre-check=0");
			return $GLOBALS['locale']->translateCharset(
				$content,
				'UTF-8',
				$GLOBALS['locale']->getExportCharset(),
				false,
				true
			);
		}

		public function formatDataToCsv($headers, $data)
		{
			$delimiter = getDelimiter();
			$result = "\"".implode("\"". $delimiter ."\"", array_values($headers))."\"\r\n";
			foreach ($data as $row) {
				$newRow = [];
				foreach ($headers as $key => $header) {
					$newRow[] = $row[$key];
				}
				$result .= "\"".implode("\"". $delimiter ."\"", array_values($newRow))."\"\r\n";
			}
			return $result;
		}

	}
