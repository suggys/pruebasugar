<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class BloqCta_quita_GpoEmpClass
{
  public function BloqCta_quita_GpoEmp($bean, $event, $arguments)
  {
    if(isset($bean->importado_c) && $bean->importado_c){
      return true;
    }
    if(
      $arguments['module'] == 'Accounts'
      && $arguments['related_module'] == 'GPE_Grupo_empresas'
      && $event === 'after_relationship_delete'
    ){
      $grupoEmpesas = BeanFactory::getBean($arguments['related_module'], $arguments['related_id']);
      $grupoEmpesas->load_relationship('gpe_grupo_empresas_accounts');
      $accounts = $grupoEmpesas->gpe_grupo_empresas_accounts->getBeans();
      if(count($accounts)){
        $account = $this->getCuentaBloqueante($accounts);
        if($account){
          if($account->codigo_bloqueo_c != $account->cod_bloqueo_aux_c){
            $account->codigo_bloqueo_c = $account->cod_bloqueo_aux_c;
            $account->noBloqueante = false;
            $account->save(false);
          }
        }
        else{
          $account = array_shift($accounts);
          $account->estado_bloqueo_c = 'Desbloqueado';
          $account->codigo_bloqueo_c = '';
          $account->bloqueante_c = false;
          $account->save(false);
        }
      }
    }
  }

  public function getCuentaBloqueante($accounts)
  {
    $accountBloqueante = null;
    foreach ($accounts as $account) {
      if($account->bloqueante_c){
        $accountBloqueante = $account;
        break;
      }
    }
    return $accountBloqueante;
  }
}
?>
