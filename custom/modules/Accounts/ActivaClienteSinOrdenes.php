<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class ActivaClienteSinOrdenes
{
  public function ActivaCliente($bean, $event, $arguments){

    // no cosidera los clientes que no tienen encedido el check
    // $GLOBALS['log']->fatal('ActivaCliente -- esta checkeado? '.print_r($bean->sin_orden_xdias_c,1));
    // if(isset($bean->sin_orden_xdias_c) && !$bean->sin_orden_xdias_c){
    //   return true;
    // }

    //$GLOBALS['log']->fatal('ActivaCliente -- validando relate '.$arguments['related_module'].' - '.$arguments['related_id']);
    if( $arguments['related_module'] == 'orden_Ordenes' && isset($arguments['related_id']) && ($event=="after_relationship_add") )
    {
      $orden = BeanFactory::retrieveBean('orden_Ordenes', $arguments['related_id']);
      //$GLOBALS['log']->fatal('ActivaCliente -- recuperamos orden? ');
      if($orden){
        // filtrando ordenes nuevas o iniciadas
        //$GLOBALS['log']->fatal('ActivaCliente -- filtrando ordenes en 0 o 1 '.$orden->estado_c);
        if( ($orden->estado_c == 0 || $orden->estado_c == 1) && $bean->sin_orden_xdias_c){
          $bean->sin_orden_xdias_c=0;
          $bean->save(false);
        }

        if(!empty($bean->prospects_accounts_1prospects_ida)){
          $prospecto = BeanFactory::getBean('Prospects', $bean->prospects_accounts_1prospects_ida);
          if($prospecto->estado_c != "convertido"){
            $prospecto->load_relationship('prospects_orden_ordenes_1');
            $prospecto->prospects_orden_ordenes_1->add($orden);
          }
        }
      }
    }
  }
}
