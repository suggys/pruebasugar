<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class BloqueoCuentaAR_por_LE{
  protected static $fetchedRow = array();
  public function saveFetchedRow($bean, $event, $arguments)
  {
    if(isset($bean->importado_c) && $bean->importado_c){
      return true;
    }
    if ( !empty($bean->id) ) {
      self::$fetchedRow[$bean->id] = $bean->fetched_row;
    }
  }

  public function BloqueoCuentaAR_por_LE($bean, $event, $arguments){
    //valida si fue modificado
    if (isset($arguments['isUpdate']) && $arguments['isUpdate'] == true) {
      
      //si el credito disponible es menor igual a 0
      if ($bean->credito_disponible_c < 0 && $bean->id_ar_credit_c!= "" && $bean->codigo_bloqueo_c != "CI" && $bean->codigo_bloqueo_c != "CL" && $bean->codigo_bloqueo_c != "Otro" && $bean->codigo_bloqueo_c != "DI") {
        //console_log();
      //if ($bean->credito_disponible_c <= 0 && !is_null($bean->id_ar_credit_c)) {
        
        if ($bean->credito_diponible_grupo_c != "") {
          //
          global $db;
          $bloqueo = 0;
          $sugarQuery = new SugarQuery();
          $sqlQuery = "SELECT ac.codigo_bloqueo_c as codigo from gpe_grupo_empresas g join gpe_grupo_empresas_accounts_c gea 
            on g.id=gea.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida join accounts a on 
            a.id=gea.gpe_grupo_empresas_accountsaccounts_idb join accounts_cstm ac on a.id=ac.id_c 
            where gea.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida=(select ag.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida 
            from accounts a join gpe_grupo_empresas_accounts_c ag on a.id=ag.gpe_grupo_empresas_accountsaccounts_idb 
            where ag.gpe_grupo_empresas_accountsaccounts_idb='$bean->id') and 
            g.administrar_credito_grupal = 1 group by gea.id;";

          $res = $db->query($sqlQuery);
          $num_rows = $res->num_rows;

          if($num_rows) {
              while($row = $db->fetchByAssoc($res)){
                  if($row['codigo']==="CV"){
                    //
                    $bloqueo=1;
                    break;
                  }else if ($row['codigo']==="LE") {
                    $bloqueo=2;
                  }
              }
          }

          switch ($bloqueo) {
            case 1:
                
                $bean->estado_bloqueo_c = "Bloqueado";
                $bean->codigo_bloqueo_c = "CV";

              break;
            
            case 2:
                $bean->estado_bloqueo_c = "Bloqueado";
                $bean->codigo_bloqueo_c = "LE";
                $bean->estado_cartera_c = "CV";
              break;

            case 0:
                
                $sugarQuery = new SugarQuery();
                $Query ="SELECT g.administrar_credito_grupal as admin from gpe_grupo_empresas g join gpe_grupo_empresas_accounts_c gea 
                on g.id=gea.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida where gea.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida=(select ag.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida 
                from accounts a join gpe_grupo_empresas_accounts_c ag on a.id=ag.gpe_grupo_empresas_accountsaccounts_idb 
                where ag.gpe_grupo_empresas_accountsaccounts_idb='$bean->id') group by g.id;";
                $res = $db->query($Query);
                $num_row = $res->num_rows;

                if($num_row) {
                    while($row = $db->fetchByAssoc($res)){
                        if($row['admin']==="1"){
                            $bean->estado_bloqueo_c = "Desbloqueado";
                            $bean->codigo_bloqueo_c = "";
                            $bean->estado_cartera_c = "CV";
                          break;
                        }else if ($row['admin']==="0") {
                          $tieneFacturasVencidas = $this->tieneFacturasVencidas($bean);
                          //valida si tiene cartera vencidad si es asi lo broquea por CV
                          if ($tieneFacturasVencidas ) {
                            
                            $bean->estado_bloqueo_c = "Bloqueado";
                            $bean->codigo_bloqueo_c = "CV";
                            $bean->estado_cartera_c = $this->getMaxDiasVencidosFacturas($bean->id);
                          }else{
                            
                           //si no es cartera vencida entonces es por el limite de credito excedido
                            $bean->estado_bloqueo_c = "Bloqueado";
                            $bean->codigo_bloqueo_c = "LE";
                            $bean->estado_cartera_c = "CV";
                          }
                          break;
                        }
                    }
                }

              break;
          }

        }else{
          //
          $tieneFacturasVencidas = $this->tieneFacturasVencidas($bean);
          //valida si tiene cartera vencidad si es asi lo broquea por CV
          if ($tieneFacturasVencidas ) {
            //
            $bean->estado_bloqueo_c = "Bloqueado";
            $bean->codigo_bloqueo_c = "CV";
            $bean->estado_cartera_c = $this->getMaxDiasVencidosFacturas($bean->id);
          }else{
            
           //si no es cartera vencida entonces es por el limite de credito excedido
            $bean->estado_bloqueo_c = "Bloqueado";
            $bean->codigo_bloqueo_c = "LE";
            $bean->estado_cartera_c = "CV";
          }
        }
      } 
    }  
    return true;
  }

  private function tieneFacturasVencidas($bean){
      global $current_user;
      $result = false;
      if(!is_admin($current_user)){
        $rolGerenteDeCredito = BeanFactory::getBean("ACLRoles", '598b02fa-715f-11e7-9cb4-06d48441b777');
        $rolAdministradorDeCredito = BeanFactory::getBean("ACLRoles", '7f4570ec-7aa3-11e6-8d90-06d48441b777');
        $isUserValidGerenteDeCredito = $current_user->check_role_membership($rolGerenteDeCredito->name, $current_user->id);       
        $isUserValidAdministradorDeCredito = $current_user->check_role_membership($rolAdministradorDeCredito->name, $current_user->id);
        if($isUserValidGerenteDeCredito || $isUserValidAdministradorDeCredito){
          $result = $this->getFacturas($bean->id);
        }
      } else {
        $result = $this->getFacturas($bean->id);
      }
      return $result;
    }


    private function getFacturas($id){
      //
      $fecha = new SugarDateTime('now');
      $beanFacturas = BeanFactory::newBean('LF_Facturas');
      $query = new SugarQuery();
      $query->from($beanFacturas,array('team_security'=>false));
      $accountsJoinName = $query->join('accounts_lf_facturas_1', ['team_security'=>false])->joinName();
      $query->where()
      ->equals("$accountsJoinName.id",$id)
      ->equals("linea_anticipo_c",0)
      ->lt("fecha_vencimiento",$fecha->asDbDate())
      ->equals("estado_tiempo", "vencida");

      $query->select(["id","name", "dias_vencimiento", "dias_vencidos_sin_tol_c", "saldo", "estado_tiempo"]);
      $facturas = $query->execute();
      if (count($facturas) > 0) {
        //
        return true;
      } else {
        return false;
      }
    }

    private function getMaxDiasVencidosFacturas($id_account){
      $fecha = new SugarDateTime('now');
      $beanFacturas = BeanFactory::newBean('LF_Facturas');
      $query = new SugarQuery();
      $query->from($beanFacturas,array('team_security'=>false));
      $accountsJoinName = $query->join('accounts_lf_facturas_1', ['team_security'=>false])->joinName();
      $query->where()
      ->equals("$accountsJoinName.id",$id_account)
      ->equals("linea_anticipo_c",0)
      ->lt("fecha_vencimiento",$fecha->asDbDate());

      $query->select(["id","name", "dias_vencimiento", "dias_vencidos_sin_tol_c", "saldo", "estado_tiempo"]);
      $facturas = $query->execute();
      $dv = 0;
      $codigo_bloqueo = '';
      foreach ($facturas as $f) {
        if($f['dias_vencimiento'] >= $dv){
          $dv = $f['dias_vencimiento'];
        }
      }
      if($dv > 0 && $dv <= 30){
          $codigo_bloqueo = 'CV130D';
        }
      else if($dv > 30 && $dv <= 120){
          $codigo_bloqueo = 'CV31120D';
        }
      else if($dv > 120){
          $codigo_bloqueo = 'CV120D';
        }
      else{
          $codigo_bloqueo = '';
        }
      return $codigo_bloqueo;

    }

}
