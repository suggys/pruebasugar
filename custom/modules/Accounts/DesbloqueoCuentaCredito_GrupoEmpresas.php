<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class DesbloqueoCuentaCredito_GrupoEmpresas
{

  public function desbloqueoCuentaCreditoGrupoEmpresas($bean, $event, $arguments)
  {
    if(isset($bean->importado_c) && $bean->importado_c){
      return true;
    }
    // if($bean->skip_grupo_empresas) return false;

    if (
      isset($arguments['isUpdate'])
      && $arguments['isUpdate'] == true
      && $bean->estado_cuenta_c === "Activo"
      && $bean->estado_bloqueo_c === "Desbloqueado"
      && $bean->fetched_row['estado_bloqueo_c'] === "Bloqueado"
      && $bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida
    ) {
      //es una cuenta que esta en BD y esta lista para actualizar
      //no es inactiva y esta para ser desbloqueada
      //pertenece a un grupo de empresas
      $gpe_empresa = BeanFactory::getBean('GPE_Grupo_empresas', $bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida);
      if($gpe_empresa->id && $gpe_empresa->administrar_credito_grupal){
        //Administra límite de crédito por grupo esta chequeado
        if($gpe_empresa->load_relationship('gpe_grupo_empresas_accounts')){
          $cuentas = $gpe_empresa->gpe_grupo_empresas_accounts->getBeans();
          foreach($cuentas as $cuenta) {
            if ($cuenta->id != $bean->id
              && $cuenta->estado_cuenta_c != "Inactivo"
              && $cuenta->estado_bloqueo_c != "Desbloqueado"
             ){
              //cuentas que no esten inactivas y que no sea la propia
              if($bean->fetched_row['codigo_bloqueo_c'] === 'CV'){
                $cuenta->estado_cartera_c = 'CV';
              }
              $cuenta->estado_bloqueo_c = 'Desbloqueado';
              $cuenta->codigo_bloqueo_c = '';
              //$cuenta->skip_grupo_empresas = true;
              $cuenta->save(false);
            }
          }
        }
      }
    }
    else{
      if(
        isset($arguments['isUpdate'])
        && $arguments['isUpdate'] == true
        && $bean->estado_bloqueo_c === "Desbloqueado"
        && $bean->fetched_row['estado_cartera_c'] !=  $bean->estado_cartera_c
        && $bean->fetched_row['estado_cartera_c'] === 'CV'
      ) {
        $bean->estado_bloqueo_c = 'Bloqueado';
        $bean->codigo_bloqueo_c = 'CV';
        $bean->bloqueante_c = true;
      }
    }

  }
}
?>
