<?php
require_once 'custom/modules/MRX1_Configuraciones/CustomMRX1Configuraciones.php';
/*
FR-1.5. Generar número de cliente (ID POS)
Al crearse una cuenta Cliente Crédito AR nueva, SugarCRM deberá generar el "ID POS", compuesto de la siguiente manera:
"99999" + homoclave(9 digitos) Consecutivo.
Ejemplo: 99999000000001
*/

class GeneraIDPOS{
    public $isDebug = true;
    public function GeneraIDPOS($bean, $event, $arguments){
        if(isset($bean->importado_c)){
            if($bean->importado_c) {
                return true;
            }
        }
        $admin = new CustomMRX1Configuraciones();
        $admin->retrieveSettings();
        $configKey = 'id_pos';
        $id_pos = "";
        $homo_clave = "";
        if($bean->id_sucursal_c != '' && $bean->id_pos_c == ''){
            if(array_key_exists($configKey,$admin->settings)){
                $homo_clave = $admin->settings[$configKey];
                $isNewIndex = false;
                while(!$isNewIndex){
                  $id_pos = "99999" . sprintf('%09d', $homo_clave);
                  if(!$this->isUsed($id_pos)){
                    $isNewIndex = true;
                  }
                  else{
                    $homo_clave++;
                  }
                }
            }
            else{
                $homo_clave = '1';
            }
            $id_pos = "99999" . sprintf('%09d', $homo_clave);
            $bean->id_pos_c =  $id_pos;
            $homo_clave++;
            $admin->saveSetting($configKey, $homo_clave);
        }
    }

    public function isUsed($idPos)
    {
      $bean = BeanFactory::newBean('Accounts');
      $sugarQuery = new SugarQuery();
      $sugarQuery->from($bean, array('team_security'=>false));
      $sugarQuery->where()
        ->equals('id_pos_c', $idPos);
      $sugarQuery->select(array('id'));
      $records = $sugarQuery->execute();
      return count($records) === 1;
    }
}

?>
