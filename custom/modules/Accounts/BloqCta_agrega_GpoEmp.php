<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class BloqCta_agrega_GpoEmpClass
{
  public function BloqCta_agrega_GpoEmp($bean, $event, $arguments)
  {
    if(isset($bean->importado_c) && $bean->importado_c){
      return true;
    }
    $flag_cuenta = false;
    $id_cta = $arguments['id'];
    if($arguments['module'] == 'Accounts'){
      $cta = BeanFactory::getBean('Accounts', $arguments['id']);
      $flag_cuenta = true;
      $edo_bloqueo = $cta->estado_bloqueo_c;
      $cod_bloqueo = $cta->codigo_bloqueo_c;
      $desc = $cta->description;
    }
    if (
      $flag_cuenta
      && $event == 'after_relationship_add')
      {
        $gpe_empresa = BeanFactory::getBean('GPE_Grupo_empresas', $arguments['related_id']);
        if($gpe_empresa->administrar_credito_grupal){
          if($gpe_empresa->load_relationship('gpe_grupo_empresas_accounts') && $edo_bloqueo == "Bloqueado"){
            $cuentas = $gpe_empresa->gpe_grupo_empresas_accounts->getBeans();
            foreach($cuentas as $cuenta) {
              if ($cuenta->id != $id_cta && ( $cuenta->estado_cuenta_c != "Inactivo" && $cuenta->estado_bloqueo_c != "Bloqueado")  && !$bean->importado_c ){
                $cuenta->estado_bloqueo_c = 'Bloqueado';
                $cuenta->codigo_bloqueo_c = $cod_bloqueo;
                if(
                  $edo_bloqueo == "Bloqueado"
                  && $cod_bloqueo == "Otro"
                  && !empty($bean->description))
                {
                  $cuenta->description = $desc;
                }
                $cuenta->save(false);
              }
            }
          }
        }
      }
      return true;
    }
  }
  ?>
