<?php
require_once 'custom/modules/MRX1_Configuraciones/CustomMRX1Configuraciones.php';
/*
*Al crearse una cuenta Cliente Crédito AR nueva, SugarCRM deberá
*generar el ""ID AR Credit"", compuesto de la siguiente manera:_x000D_
*80012301+ número_tienda(4 digitos) + homoclave(4 digitos)._x000D_
*La homoclave generada debe ser diferente a la del ""ID Línea de Anticipo""
*/

class GeneraID_LineaCredito
{
  public function GeneraID_LineaCredito($bean, $event, $arguments)
  {
    if(isset($bean->importado_c)){
      if($bean->importado_c){
        return true;
      }
    }

    $PREX_ACCOUNT_ID    = "80012301";
    $id_linea_anticipo  = "";
    $homo_clave         = "";
    $sucursal_id        = "";

    $merxConfigurator = new CustomMRX1Configuraciones();
    $merxConfigurator->retrieveSettings();
    $isNewIndex = false;

    if($bean->id_linea_anticipo_c == '' && $bean->calcula_id_linea_credito_c ){
      $sucursal_id = $bean->id_sucursal_c;
      $configKey   = "IDLA_suc".$sucursal_id;

      if(array_key_exists($configKey,$merxConfigurator->settings)){
        $homo_clave = $merxConfigurator->settings[$configKey];
      } else {
        $homo_clave = '7000';
      }

      while(!$isNewIndex){
        $id_linea_anticipo = $PREX_ACCOUNT_ID . $sucursal_id .  sprintf('%04d', $homo_clave);
        if(!$this->isUsed($id_linea_anticipo)){
          $isNewIndex = true;
        }
        $homo_clave++;
      }
      $merxConfigurator->saveSetting($configKey, $homo_clave);
      $bean->id_linea_anticipo_c = $id_linea_anticipo;
    }
  }

  public function isUsed($idLineaAnticipo)
  {
    $bean = BeanFactory::newBean('Accounts');
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $sugarQuery->where()->equals('id_linea_anticipo_c', $idLineaAnticipo);
    $sugarQuery->select(array('id'));
    $records = $sugarQuery->execute();
    return count($records) === 1;
  }
}
