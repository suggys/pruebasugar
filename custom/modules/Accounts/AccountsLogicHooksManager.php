<?php
//console_log('estoy en AccountsLogicHooksManager');
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once "custom/modules/GPE_Grupo_empresas/Calcula_Saldos_Al_Vincular_Desvincular.php";
require_once 'custom/modules/MRX1_Configuraciones/CustomMRX1Configuraciones.php';

class AccountsLogicHooksManager{

	protected static $fetchedRow = array();

	public function calculaCreditDisponibleFlexible($bean, $event, $arguments){
		
		if ($bean->id_ar_credit_c!= "") {// validacion para que nada mas entre cuando tenga AR_CREDIT HSLR
			//console_log();
			if(isset($bean->importado_c) && $bean->importado_c){
				return true;
			}

			if ( !empty($bean->id) ) {
				self::$fetchedRow[$bean->id] = $bean->fetched_row;
			}

			if($bean->limite_credito_autorizado_c !== $bean->fetched_row['limite_credito_autorizado_c'] ){
				$bean->credito_disponible_flexible_c = $bean->limite_credito_autorizado_c > 0 ? (($bean->limite_credito_autorizado_c * 1.1) - $bean->saldo_deudor_c) : 0;
			}

			if($arguments['isUpdate'] && $bean->credito_disponible_c !== $bean->fetched_row['credito_disponible_c']){
				$bean->credito_disponible_flexible_c = $bean->credito_disponible_c >= 0 ? $bean->credito_disponible_c + ($bean->limite_credito_autorizado_c * 0.1) : 0;
			}

			if(
				!empty($bean->id)
				&& $bean->limite_credito_autorizado_c !== $bean->fetched_row['limite_credito_autorizado_c']
				&& $bean->deleted === 0
			){
				$bean->credito_disponible_c = $bean->limite_credito_autorizado_c;
				$GLOBALS["log"]->fatal("------- calculaCreditDisponibleFlexible ------");
				if(!empty($bean->id)){
					$bean->load_relationship('accounts_orden_ordenes_1');
					$ordenes = $bean->accounts_orden_ordenes_1->getBeans();
					$montoOrdenes = 0;
					foreach ($ordenes as $orden) {
						$orden->retrieve();
						if (!$orden->importado_c){
							if($orden->estado_c === '2') continue;
							if($orden->estado_c === '5' && $orden->orden_ordenes_orden_ordenes_1orden_ordenes_ida) continue;
							//if de hector
							if ( !empty($orden->id_customer_c)) {
								$GLOBALS["log"]->fatal("------- entra a validar ------");
								$montoOrdenes += $orden->amount_c;
							}
							//$montoOrdenes += $orden->amount_c;
						}
					}
					$bean->credito_disponible_c -= $montoOrdenes;
				}
			}

			if(
				!$bean->noBloqueante
				&& $bean->estado_bloqueo_c === 'Bloqueado'
				&& (
					$bean->codigo_bloqueo_c != $bean->fetched_row['codigo_bloqueo_c']
					|| $bean->fetched_row['estado_bloqueo_c'] === 'Desbloqueado'
					)
				){
					//console_log();
					$bean->bloqueante_c = true;
					$bean->cod_bloqueo_aux_c = $bean->codigo_bloqueo_c;
				}

				if(
					$bean->estado_bloqueo_c === 'Desbloqueado'
					&& $bean->estado_bloqueo_c != $bean->fetched_row['estado_bloqueo_c']
				){
					//console_log();
					$bean->codigo_bloqueo_c = "";
					$bean->estado_cartera_c = "CV";
					$tieneFacturasVencidas = $this->tieneFacturasVencidas($bean);
					$tieneCreditoExcedido = $this->tieneCreditoExcedido($bean);
					// $GLOBALS["log"]->fatal("tieneFacturasVencidas:". print_r($tieneFacturasVencidas, true));
					// $GLOBALS["log"]->fatal("tieneCreditoExcedido:". print_r($tieneCreditoExcedido, true));
					if ($tieneFacturasVencidas || $tieneCreditoExcedido) {
						$bean->estado_bloqueo_c = "Bloqueado";
						$bean->codigo_bloqueo_c = $tieneFacturasVencidas ? "CV" : "LE";
						// TODO Conseguir el estado de la cartera si el codigo de bloqueo es CV -- Listo ok
						if($tieneFacturasVencidas){
							$bean->estado_cartera_c = $this->getMaxDiasVencidosFacturas($bean->id);
						}

					} else {

						$bean->bloqueante_c = false;
						$bean->codigo_bloqueo_c = "";
						$bean->cod_bloqueo_aux_c = "";
						$bean->estado_cartera_c = "CV";
						//$bean->estado_bloqueo_c = "Desbloqueado";
					}
				}
		}
	}

		public function calculaSaldoDeudorGrupoEmpresas($bean, $event, $arguments){
			if(isset($bean->importado_c) && $bean->importado_c){
				return true;
			}
			$bean->retrieve();
			if($bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida){
				$changeGrupo = false;
				$grupoEmpresas = BeanFactory::getBean('GPE_Grupo_empresas', $bean->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida);
				if (
					$bean->credito_disponible_c != self::$fetchedRow[$bean->id]['credito_disponible_c']
					|| $bean->saldo_deudor_c != self::$fetchedRow[$bean->id]['saldo_deudor_c']
					|| $bean->saldo_pendiente_aplicar_c != self::$fetchedRow[$bean->id]['saldo_pendiente_aplicar_c']
				){
					$lhCalculaSaldosGrupo = new Calcula_Saldos_Al_Vincular_Desvincular();
					$lhCalculaSaldosGrupo->fnCalculaActualizaSaldos($grupoEmpresas);
				}
			}
		}

		public function calculaSaldosAlGuardarCta($bean, $event, $arguments){
			if(isset($bean->importado_c) && $bean->importado_c){
				return true;
			}
		}

		public function generaFolio($bean, $event, $arguments){
			if(!empty($bean->id)){
				self::$fetchedRow[$bean->id] = $bean->fetched_row;
			}
			$configKey = 'accounts_folio';
			if(self::$fetchedRow[$bean->id]['id'] != $bean->id){
				$folio = 1;
				$configuratorObj = new CustomMRX1Configuraciones();
				$configuratorObj->retrieveSettings();
				if(isset($configuratorObj->settings[$configKey]) && $configuratorObj->settings[$configKey] > 0){
					$folio = $configuratorObj->settings[$configKey];
				}else{
					$folio = 1;
				}
				$bean->folio_c = $folio;
				$folio++;
				$configuratorObj->saveSetting($configKey, $folio);
			}
			return true;
		}

		private function tieneFacturasVencidas($bean){
			global $current_user;
			$result = false;
			if(!is_admin($current_user)){
				$rolGerenteDeCredito = BeanFactory::getBean("ACLRoles", '598b02fa-715f-11e7-9cb4-06d48441b777');
				$rolAdministradorDeCredito = BeanFactory::getBean("ACLRoles", '7f4570ec-7aa3-11e6-8d90-06d48441b777');
				$isUserValidGerenteDeCredito = $current_user->check_role_membership($rolGerenteDeCredito->name, $current_user->id);				
				$isUserValidAdministradorDeCredito = $current_user->check_role_membership($rolAdministradorDeCredito->name, $current_user->id);
				if($isUserValidGerenteDeCredito || $isUserValidAdministradorDeCredito){
					$result = $this->getFacturas($bean->id);
				}
			} else {
				$result = $this->getFacturas($bean->id);
			}
			return $result;
		}


		private function getFacturas($id){
			$fecha = new SugarDateTime('now');
			$beanFacturas = BeanFactory::newBean('LF_Facturas');
			$query = new SugarQuery();
			$query->from($beanFacturas,array('team_security'=>false));
			$accountsJoinName = $query->join('accounts_lf_facturas_1', ['team_security'=>false])->joinName();
			$query->where()
			->equals("$accountsJoinName.id",$id)
			->equals("linea_anticipo_c",0)
			->lt("fecha_vencimiento",$fecha->asDbDate())
			->equals("estado_tiempo", "vencida");

			$query->select(["id","name", "dias_vencimiento", "dias_vencidos_sin_tol_c", "saldo", "estado_tiempo"]);
			$facturas = $query->execute();
			if (count($facturas) > 0) {
				return true;
			} else {
				return false;
			}
		}

		private function getMaxDiasVencidosFacturas($id_account){
			$fecha = new SugarDateTime('now');
			$beanFacturas = BeanFactory::newBean('LF_Facturas');
			$query = new SugarQuery();
			$query->from($beanFacturas,array('team_security'=>false));
			$accountsJoinName = $query->join('accounts_lf_facturas_1', ['team_security'=>false])->joinName();
			$query->where()
			->equals("$accountsJoinName.id",$id_account)
			->equals("linea_anticipo_c",0)
			->lt("fecha_vencimiento",$fecha->asDbDate());

			$query->select(["id","name", "dias_vencimiento", "dias_vencidos_sin_tol_c", "saldo", "estado_tiempo"]);
			$facturas = $query->execute();
			$dv = 0;
			$codigo_bloqueo = '';
			foreach ($facturas as $f) {
				if($f['dias_vencimiento'] >= $dv){
					$dv = $f['dias_vencimiento'];
				}
			}
			if($dv > 0 && $dv <= 30){
			    $codigo_bloqueo = 'CV130D';
			  }
			else if($dv > 30 && $dv <= 120){
			    $codigo_bloqueo = 'CV31120D';
			  }
			else if($dv > 120){
			    $codigo_bloqueo = 'CV120D';
			  }
			else{
					$codigo_bloqueo = '';
				}
			return $codigo_bloqueo;

		}

		private function tieneCreditoExcedido($bean){
			global $current_user;
			$result = false;
			if(!is_admin($current_user)){
				$rolGerenteDeCredito = BeanFactory::getBean("ACLRoles", '598b02fa-715f-11e7-9cb4-06d48441b777');
				$rolAdministradorDeCredito = BeanFactory::getBean("ACLRoles", '7f4570ec-7aa3-11e6-8d90-06d48441b777');
				$isUserValidGerenteDeCredito = $current_user->check_role_membership($rolGerenteDeCredito->name, $current_user->id);
				$isUserValidAdministradorDeCredito = $current_user->check_role_membership($rolAdministradorDeCredito->name, $current_user->id);
				if($isUserValidGerenteDeCredito || $isUserValidAdministradorDeCredito)
				{
					if ($bean->credito_disponible_c < 0) {
						$result = true;
					}
				}
			} else {
				if ($bean->credito_disponible_c < 0) {
					$result = true;
				}
			}
			return $result;
		}
}
