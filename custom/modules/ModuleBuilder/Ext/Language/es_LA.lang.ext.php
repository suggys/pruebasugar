<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/ModuleBuilder/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_JS_DELETE_REQUIRED_DDL_ITEM_NEW'] = '¿Está seguro de que desea borrar el estatus de Nuevas Ventas? Borrar el estatus puede causar un funcionamiento no adecuado de los elementos de la Línea de Ingresos del módulo de Oportunidades.';
$mod_strings['LBL_JS_DELETE_REQUIRED_DDL_ITEM_IN_PROGRESS'] = '¿Está seguro de que desea borrar el estatus de Progreso de las ventas? Borrar el estatus puede causar el funcionamiento no adecuado de los elementos de la Línea de Ingresos del módulo de Oportunidades.';

?>
