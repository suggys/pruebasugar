<?php
// created: 2018-01-11 00:54:52
$viewdefs['dise_Prospectos_cocina']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'dise_prospectos_cocina_number' => 
    array (
    ),
    'name' => 
    array (
    ),
    'tipo_persona' => 
    array (
    ),
    'etapa_de_venta' => 
    array (
    ),
    'fecha_de_cierre' => 
    array (
    ),
    'id_sucursal' => 
    array (
    ),
    'categoria' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);