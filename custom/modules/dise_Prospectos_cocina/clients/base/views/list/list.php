<?php
$module_name = 'dise_Prospectos_cocina';
$_module_name = 'dise_prospectos_cocina';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'dise_prospectos_cocina_number',
                'label' => 'LBL_NUMBER',
                'default' => true,
                'enabled' => true,
              ),
              1 => 
              array (
                'name' => 'name',
                'label' => 'LBL_SUBJECT',
                'link' => true,
                'default' => true,
                'enabled' => true,
              ),
              2 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'telefono_celular',
                'label' => 'LBL_TELEFONO_CELULAR',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'etapa_de_venta',
                'label' => 'LBL_ETAPA_DE_VENTA',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'test_c',
                'label' => 'LBL_TEST',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
              ),
              7 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
