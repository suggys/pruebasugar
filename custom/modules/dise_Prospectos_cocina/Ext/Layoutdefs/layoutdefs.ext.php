<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Layoutdefs/dise_prospectos_cocina_meetings_dise_Prospectos_cocina.php

 // created: 2018-02-01 19:07:34
$layout_defs["dise_Prospectos_cocina"]["subpanel_setup"]['dise_prospectos_cocina_meetings'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DISE_PROSPECTOS_COCINA_MEETINGS_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'dise_prospectos_cocina_meetings',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Layoutdefs/dise_prospectos_cocina_calls_dise_Prospectos_cocina.php

 // created: 2018-02-01 19:07:34
$layout_defs["dise_Prospectos_cocina"]["subpanel_setup"]['dise_prospectos_cocina_calls'] = array (
  'order' => 100,
  'module' => 'Calls',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DISE_PROSPECTOS_COCINA_CALLS_FROM_CALLS_TITLE',
  'get_subpanel_data' => 'dise_prospectos_cocina_calls',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Layoutdefs/dise_prospectos_cocina_quotes_dise_Prospectos_cocina.php

 // created: 2018-01-11 00:47:54
$layout_defs["dise_Prospectos_cocina"]["subpanel_setup"]['dise_prospectos_cocina_quotes'] = array (
  'order' => 100,
  'module' => 'Quotes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_QUOTES_TITLE',
  'get_subpanel_data' => 'dise_prospectos_cocina_quotes',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
