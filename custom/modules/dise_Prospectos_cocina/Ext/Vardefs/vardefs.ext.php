<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Vardefs/dise_prospectos_cocina_meetings_dise_Prospectos_cocina.php

// created: 2018-02-01 19:07:34
$dictionary["dise_Prospectos_cocina"]["fields"]["dise_prospectos_cocina_meetings"] = array (
  'name' => 'dise_prospectos_cocina_meetings',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_MEETINGS_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'id_name' => 'dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Vardefs/dise_prospectos_cocina_calls_dise_Prospectos_cocina.php

// created: 2018-02-01 19:07:34
$dictionary["dise_Prospectos_cocina"]["fields"]["dise_prospectos_cocina_calls"] = array (
  'name' => 'dise_prospectos_cocina_calls',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_CALLS_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'id_name' => 'dise_prospectos_cocina_callsdise_prospectos_cocina_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Vardefs/sugarfield_nombre_del_proyecto.php

 // created: 2018-01-18 16:41:06
$dictionary['dise_Prospectos_cocina']['fields']['nombre_del_proyecto']['required']=false;
$dictionary['dise_Prospectos_cocina']['fields']['nombre_del_proyecto']['audited']=false;
$dictionary['dise_Prospectos_cocina']['fields']['nombre_del_proyecto']['importable']='false';
$dictionary['dise_Prospectos_cocina']['fields']['nombre_del_proyecto']['reportable']=false;

 
?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Vardefs/sugarfield_fecha_de_cierre.php

 // created: 2018-01-16 05:59:54
$dictionary['dise_Prospectos_cocina']['fields']['fecha_de_cierre']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Vardefs/dise_prospectos_cocina_quotes_dise_Prospectos_cocina.php

// created: 2018-01-11 00:47:54
$dictionary["dise_Prospectos_cocina"]["fields"]["dise_prospectos_cocina_quotes"] = array (
  'name' => 'dise_prospectos_cocina_quotes',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_quotes',
  'source' => 'non-db',
  'module' => 'Quotes',
  'bean_name' => 'Quote',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'id_name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Vardefs/sugarfield_id_sucursal.php

 // created: 2018-01-18 18:01:23

 
?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Vardefs/sugarfield_telefono_celular.php

 // created: 2018-01-16 05:59:24
$dictionary['dise_Prospectos_cocina']['fields']['telefono_celular']['required']=true;
$dictionary['dise_Prospectos_cocina']['fields']['telefono_celular']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Vardefs/sugarfield_test_c.php

 // created: 2018-02-06 05:01:56
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['duplicate_merge_dom_value']=0;
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['labelValue']='Sucursal';
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['calculated']='1';
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['formula']='getDropdownValue("sucursal_c_list",related($created_by_link,"sucursal_c"))';
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['enforced']='1';
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/dise_Prospectos_cocina/Ext/Vardefs/sugarfield_categoria_otra_c.php

 // created: 2018-02-06 05:01:56
$dictionary['dise_Prospectos_cocina']['fields']['categoria_otra_c']['labelValue']='Otra categoría';
$dictionary['dise_Prospectos_cocina']['fields']['categoria_otra_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['dise_Prospectos_cocina']['fields']['categoria_otra_c']['enforced']='';
$dictionary['dise_Prospectos_cocina']['fields']['categoria_otra_c']['dependency']='equal($categoria,"Otros")';

 
?>
