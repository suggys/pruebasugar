<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_importado_c.php

 // created: 2016-12-02 16:22:51
$dictionary['NC_Notas_credito']['fields']['importado_c']['labelValue']='Importado';
$dictionary['NC_Notas_credito']['fields']['importado_c']['enforced']='';
$dictionary['NC_Notas_credito']['fields']['importado_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/nc_notas_credito_orden_ordenes_NC_Notas_credito.php

// created: 2016-10-12 00:13:37
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_orden_ordenes"] = array (
  'name' => 'nc_notas_credito_orden_ordenes',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_orden_ordenes',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
);
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_orden_ordenes_name"] = array (
  'name' => 'nc_notas_credito_orden_ordenes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'save' => true,
  'id_name' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
  'link' => 'nc_notas_credito_orden_ordenes',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'name',
);
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_orden_ordenesorden_ordenes_idb"] = array (
  'name' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE_ID',
  'id_name' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
  'link' => 'nc_notas_credito_orden_ordenes',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/nc_notas_credito_accounts_NC_Notas_credito.php

// created: 2016-10-12 00:13:37
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_accounts"] = array (
  'name' => 'nc_notas_credito_accounts',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE',
  'id_name' => 'nc_notas_credito_accountsaccounts_ida',
  'link-type' => 'one',
);
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_accounts_name"] = array (
  'name' => 'nc_notas_credito_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'nc_notas_credito_accountsaccounts_ida',
  'link' => 'nc_notas_credito_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_accountsaccounts_ida"] = array (
  'name' => 'nc_notas_credito_accountsaccounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID',
  'id_name' => 'nc_notas_credito_accountsaccounts_ida',
  'link' => 'nc_notas_credito_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/nc_notas_credito_lf_facturas_1_NC_Notas_credito.php

// created: 2016-10-20 16:38:00
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_lf_facturas_1"] = array (
  'name' => 'nc_notas_credito_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE',
  'id_name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_no_ticket_c.php

 // created: 2016-12-12 22:48:13
$dictionary['NC_Notas_credito']['fields']['no_ticket_c']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/full_text_search_admin.php

 // created: 2017-02-24 18:28:04
$dictionary['NC_Notas_credito']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_importadode_c.php

 // created: 2017-10-09 18:07:32
$dictionary['NC_Notas_credito']['fields']['importadode_c']['labelValue']='Archivo de origen';
$dictionary['NC_Notas_credito']['fields']['importadode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['NC_Notas_credito']['fields']['importadode_c']['enforced']='';
$dictionary['NC_Notas_credito']['fields']['importadode_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_nombre_e_c.php

 // created: 2017-01-18 23:23:19
$dictionary['NC_Notas_credito']['fields']['nombre_e_c']['labelValue']='Nombre Emisor';
$dictionary['NC_Notas_credito']['fields']['nombre_e_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['NC_Notas_credito']['fields']['nombre_e_c']['enforced']='';
$dictionary['NC_Notas_credito']['fields']['nombre_e_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_description.php

 // created: 2016-12-15 19:30:52
$dictionary['NC_Notas_credito']['fields']['description']['required']=false;
$dictionary['NC_Notas_credito']['fields']['description']['unified_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_uuid_c.php

 // created: 2017-01-18 23:24:19
$dictionary['NC_Notas_credito']['fields']['uuid_c']['labelValue']='Identificador Único (UUID)';
$dictionary['NC_Notas_credito']['fields']['uuid_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['NC_Notas_credito']['fields']['uuid_c']['enforced']='';
$dictionary['NC_Notas_credito']['fields']['uuid_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_rfc_e_c.php

 // created: 2017-01-18 23:23:31
$dictionary['NC_Notas_credito']['fields']['rfc_e_c']['labelValue']='RFC Emisor';
$dictionary['NC_Notas_credito']['fields']['rfc_e_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['NC_Notas_credito']['fields']['rfc_e_c']['enforced']='';
$dictionary['NC_Notas_credito']['fields']['rfc_e_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_rfc_r_c.php

 // created: 2017-01-18 23:23:43
$dictionary['NC_Notas_credito']['fields']['rfc_r_c']['labelValue']='RFC Receptor';
$dictionary['NC_Notas_credito']['fields']['rfc_r_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['NC_Notas_credito']['fields']['rfc_r_c']['enforced']='';
$dictionary['NC_Notas_credito']['fields']['rfc_r_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_series_id_c.php

 // created: 2017-01-18 23:24:05
$dictionary['NC_Notas_credito']['fields']['series_id_c']['labelValue']='Serie';
$dictionary['NC_Notas_credito']['fields']['series_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['NC_Notas_credito']['fields']['series_id_c']['enforced']='';
$dictionary['NC_Notas_credito']['fields']['series_id_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_folio_factura_c.php

 // created: 2017-01-18 23:22:37
$dictionary['NC_Notas_credito']['fields']['folio_factura_c']['labelValue']='Folio Factura';
$dictionary['NC_Notas_credito']['fields']['folio_factura_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['NC_Notas_credito']['fields']['folio_factura_c']['enforced']='';
$dictionary['NC_Notas_credito']['fields']['folio_factura_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_name_r_c.php

 // created: 2017-01-18 23:23:02
$dictionary['NC_Notas_credito']['fields']['name_r_c']['labelValue']='Nombre Receptor';
$dictionary['NC_Notas_credito']['fields']['name_r_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['NC_Notas_credito']['fields']['name_r_c']['enforced']='';
$dictionary['NC_Notas_credito']['fields']['name_r_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Vardefs/sugarfield_name.php

 // created: 2016-12-14 01:17:29
$dictionary['NC_Notas_credito']['fields']['name']['len']='255';
$dictionary['NC_Notas_credito']['fields']['name']['audited']=false;
$dictionary['NC_Notas_credito']['fields']['name']['massupdate']=false;
$dictionary['NC_Notas_credito']['fields']['name']['unified_search']=false;
$dictionary['NC_Notas_credito']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['NC_Notas_credito']['fields']['name']['calculated']=false;

 
?>
