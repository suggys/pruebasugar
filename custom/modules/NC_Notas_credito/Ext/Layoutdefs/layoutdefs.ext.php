<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Layoutdefs/nc_notas_credito_lf_facturas_1_NC_Notas_credito.php

 // created: 2016-10-20 16:38:00
$layout_defs["NC_Notas_credito"]["subpanel_setup"]['nc_notas_credito_lf_facturas_1'] = array (
  'order' => 100,
  'module' => 'LF_Facturas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'get_subpanel_data' => 'nc_notas_credito_lf_facturas_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Layoutdefs/_overrideNC_Notas_credito_subpanel_nc_notas_credito_lf_facturas_1.php

//auto-generated file DO NOT EDIT
$layout_defs['NC_Notas_credito']['subpanel_setup']['nc_notas_credito_lf_facturas_1']['override_subpanel_name'] = 'NC_Notas_credito_subpanel_nc_notas_credito_lf_facturas_1';

?>
