<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.customnc_notas_credito_lf_facturas_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Facturas / Notas de Cargo';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_FOLIO_FACTURA_C'] = 'Folio Factura';
$mod_strings['LBL_UUID'] = 'Identificador Único (UUID)';
$mod_strings['LBL_SERIES_ID'] = 'Serie';
$mod_strings['LBL_RFC_E'] = 'RFC Emisor';
$mod_strings['LBL_NOMBRE_E'] = 'Nombre Emisor';
$mod_strings['LBL_RFC_R'] = 'RFC Receptor';
$mod_strings['LBL_NAME_R'] = 'Nombre Receptor';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Facturas / Notas de cargo';
$mod_strings['LBL_NO_TICKET_C'] = 'Número de Ticket';
$mod_strings['LBL_NAME'] = 'Folio';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Cliente';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Orden';
$mod_strings['LBL_SUB_TOTAL_C'] = 'Subtotal';
$mod_strings['LBL_IMPORTADO'] = 'Registro Importado';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Saldos';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_SHOW_MORE'] = 'Información del Sistema';
$mod_strings['LBL_DESCRIPTION'] = 'Comentarios';
$mod_strings['LBL_MODULE_NAME'] = 'Notas de Crédito';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Nota de Crédito';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.Lowes_CRM.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.Lowes_FRR.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Nota de Crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.Lowes_CRM_20171012.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.Lowes_CRM_20171020.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.Lowes_CRM_20171010.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.Lowes_CRM_201710130100.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.Lowes_CRM_20171009.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
