<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/ar_SA.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/uk_UA.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/de_DE.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/et_EE.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/fi_FI.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/zh_CN.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/fr_FR.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/da_DK.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/cs_CZ.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/sk_SK.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/ko_KR.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/en_us.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/ro_RO.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/ja_JP.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/zh_TW.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/en_UK.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_LA.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/ru_RU.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/ca_ES.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/it_it.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/hu_HU.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/sr_RS.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/lv_LV.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/sq_AL.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/el_EL.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/bg_BG.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/nl_NL.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/pt_PT.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/sv_SE.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/pt_BR.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/tr_TR.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/nb_NO.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/lt_LT.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/he_IL.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/pl_PL.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/NC_Notas_credito/Ext/Language/es_ES.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';

?>
