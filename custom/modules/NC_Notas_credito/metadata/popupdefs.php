<?php
$popupMeta = array (
    'moduleMain' => 'NC_Notas_credito',
    'varName' => 'NC_Notas_credito',
    'orderBy' => 'nc_notas_credito.name',
    'whereClauses' => array (
  'name' => 'nc_notas_credito.name',
),
    'searchInputs' => array (
  0 => 'nc_notas_credito_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'NC_NOTAS_CREDITO_ACCOUNTS_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE',
    'id' => 'NC_NOTAS_CREDITO_ACCOUNTSACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'NC_NOTAS_CREDITO_ORDEN_ORDENES_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
    'id' => 'NC_NOTAS_CREDITO_ORDEN_ORDENESORDEN_ORDENES_IDB',
    'width' => '10%',
    'default' => true,
  ),
  'FECHA_EMISION_C' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_FECHA_EMISION_C',
    'width' => '10%',
    'default' => true,
  ),
  'SUB_TOTAL_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_SUB_TOTAL_C',
    'currency_format' => true,
    'width' => '10%',
  ),
  'IVA_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_IVA_C',
    'currency_format' => true,
    'width' => '10%',
  ),
  'IMPORTE_TOTAL' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_IMPORTE_TOTAL',
    'currency_format' => true,
    'width' => '10%',
  ),
  'SALDO_APLICADO_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_SALDO_APLICADO_C',
    'currency_format' => true,
    'width' => '10%',
  ),
  'SALDO_PENDIENTE_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_SALDO_PENDIENTE_C',
    'currency_format' => true,
    'width' => '10%',
  ),
  'RFC_E_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_RFC_E',
    'width' => '10%',
  ),
  'RFC_R_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_RFC_R',
    'width' => '10%',
  ),
  'IMPORTADO_C' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_IMPORTADO',
    'width' => '10%',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
),
);
