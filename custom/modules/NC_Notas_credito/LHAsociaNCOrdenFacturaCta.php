<?php
class LHAsociaNCOrdenFacturaCta{

	protected static $fetchedRow = array();

	public function asociaNCOrdenFacturaCta($bean, $event, $arguments){

		if ( !empty($bean->id) ) {
			self::$fetchedRow[$bean->id] = $bean->fetched_row;
		}

	}

	public function asociaNCFactura($bean, $event, $arguments){
		if(isset($bean->importado_c) && $bean->importado_c){
			return true;
		}
		if(empty(self::$fetchedRow[$bean->id]) && $bean->no_ticket_c){
			$ordenPadre = BeanFactory::newBean("orden_Ordenes");
			$ordenPadre->retrieve_by_string_fields(array('no_ticket_c' => $bean->no_ticket_c));
			$ordenPadre->retrieve();
			if($ordenPadre->id){
				$orden = null;
				$ordenPadre->load_relationship('orden_ordenes_orden_ordenes_1');
				$ordenesHijas = $ordenPadre->orden_ordenes_orden_ordenes_1->getBeans();
				foreach ($ordenesHijas as $ordenHija) {
					if($ordenHija->estado_c == '6'){
						$orden = $ordenHija;
					}
				}
				if($orden && $orden->id){
					$orden->load_relationship('nc_notas_credito_orden_ordenes');
					$orden->nc_notas_credito_orden_ordenes->add($bean);

					$accout = BeanFactory::getBean('Accounts', $orden->accounts_orden_ordenes_1accounts_ida);
					if($accout->id && !$accout->importado_c){
						$accout->load_relationship('nc_notas_credito_accounts');
						$accout->nc_notas_credito_accounts->add($bean);
					}

					$factura = BeanFactory::newBean("LF_Facturas");
					$factura->retrieve_by_string_fields(array('no_ticket' => $orden->folio_orden_inicial_c));
					$factura->retrieve();
					if($factura->id && !$factura->importado_c){
						$bean->load_relationship('nc_notas_credito_lf_facturas_1');
						$bean->nc_notas_credito_lf_facturas_1->add($factura->id);
					}

				}
			}
		}
	}

	public function afterRelationshipAdd($bean, $event, $arguments)
	{
		return true;
	}
}

?>
