<?php
class LHAsignaSaldosInicialesNC {
	private $isDebug = false;
    public function fnAsignaSaldosIniciales($bean, $event, $arguments) {
        if(isset($bean->importado_c) && $bean->importado_c){
            return true;
        }
    	if($this->isDebug)$GLOBALS['log']->fatal('*********** fnAsignaSaldosIniciales ***********');
    	if(!$arguments['isUpdate']) {
    		if($this->isDebug)$GLOBALS['log']->fatal('fnAsignaSaldosIniciales name:'.$bean->name);
    		$bean->saldo_aplicado_c = 0;
    		$bean->saldo_pendiente_c = $bean->importe_total;
    		if($this->isDebug)$GLOBALS['log']->fatal('fnAsignaSaldosIniciales calculado:'.$bean->saldo_pendiente_c);
    	}
    }
}
?>