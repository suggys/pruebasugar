<?php
$module_name = 'NC_Notas_credito';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'readonly' => true,
                'link' => true,
              ),
              1 =>
              array (
                'name' => 'nc_notas_credito_accounts_name',
                'label' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE',
                'enabled' => true,
                'readonly' => true,
                'id' => 'NC_NOTAS_CREDITO_ACCOUNTSACCOUNTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 =>
              array (
                'name' => 'nc_notas_credito_orden_ordenes_name',
                'label' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
                'enabled' => true,
                'readonly' => true,
                'id' => 'NC_NOTAS_CREDITO_ORDEN_ORDENESORDEN_ORDENES_IDB',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 =>
              array (
                'name' => 'fecha_emision_c',
                'label' => 'LBL_FECHA_EMISION_C',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              4 =>
              array (
                'name' => 'sub_total_c',
                'label' => 'LBL_SUB_TOTAL_C',
                'enabled' => true,
                'readonly' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              5 =>
              array (
                'name' => 'iva_c',
                'label' => 'LBL_IVA_C',
                'enabled' => true,
                'readonly' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'importe_total',
                'label' => 'LBL_IMPORTE_TOTAL',
                'enabled' => true,
                'readonly' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              7 =>
              array (
                'name' => 'saldo_aplicado_c',
                'label' => 'LBL_SALDO_APLICADO_C',
                'enabled' => true,
                'readonly' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              8 =>
              array (
                'name' => 'saldo_pendiente_c',
                'label' => 'LBL_SALDO_PENDIENTE_C',
                'enabled' => true,
                'readonly' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              9 =>
              array (
                'name' => 'rfc_e_c',
                'label' => 'LBL_RFC_E',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              10 =>
              array (
                'name' => 'rfc_r_c',
                'label' => 'LBL_RFC_R',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              11 =>
              array (
                'name' => 'importado_c',
                'label' => 'LBL_IMPORTADO',
                'enabled' => true,
                'default' => true,
              ),
              12 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              13 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              14 =>
              array (
                'name' => 'importadode_c',
                'label' => 'LBL_IMPORTADODE',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              15 =>
              array (
                'name' => 'no_ticket_c',
                'label' => 'LBL_NO_TICKET_C',
                'enabled' => true,
                'readonly' => true,
                'default' => false,
              ),
              16 =>
              array (
                'name' => 'series_id_c',
                'label' => 'LBL_SERIES_ID',
                'enabled' => true,
                'readonly' => true,
                'default' => false,
              ),
              17 =>
              array (
                'name' => 'uuid_c',
                'label' => 'LBL_UUID',
                'enabled' => true,
                'readonly' => true,
                'default' => false,
              ),
              18 =>
              array (
                'name' => 'nombre_e_c',
                'label' => 'LBL_NOMBRE_E',
                'enabled' => true,
                'readonly' => true,
                'default' => false,
              ),
              19 =>
              array (
                'name' => 'name_r_c',
                'label' => 'LBL_NAME_R',
                'enabled' => true,
                'readonly' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
