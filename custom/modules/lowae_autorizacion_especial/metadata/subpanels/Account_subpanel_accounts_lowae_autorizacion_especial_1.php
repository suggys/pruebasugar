<?php
// created: 2017-01-11 17:11:52
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'motivo_solicitud_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_MOTIVO_SOLICITUD_C',
    'width' => '10%',
  ),
  'fecha_inicio_c' => 
  array (
    'type' => 'datetimecombo',
    'vname' => 'LBL_FECHA_INICIO_C',
    'width' => '10%',
    'default' => true,
  ),
  'fecha_fin_c' => 
  array (
    'type' => 'datetimecombo',
    'vname' => 'LBL_FECHA_FIN_C',
    'width' => '10%',
    'default' => true,
  ),
  'estado_autorizacion_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ESTADO_AUTORIZACION',
    'width' => '10%',
  ),
  'fecha_cumplimiento_c' => 
  array (
    'type' => 'datetimecombo',
    'vname' => 'LBL_FECHA_CUMPLIMIENTO_C',
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
);