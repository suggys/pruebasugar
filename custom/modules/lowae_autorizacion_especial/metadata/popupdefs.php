<?php
$popupMeta = array (
    'moduleMain' => 'lowae_autorizacion_especial',
    'varName' => 'lowae_autorizacion_especial',
    'orderBy' => 'lowae_autorizacion_especial.name',
    'whereClauses' => array (
  'name' => 'lowae_autorizacion_especial.name',
  'motivo_solicitud_c' => 'lowae_autorizacion_especial.motivo_solicitud_c',
  'assigned_user_id' => 'lowae_autorizacion_especial.assigned_user_id',
  'favorites_only' => 'lowae_autorizacion_especial.favorites_only',
  'monto_venta_c' => 'lowae_autorizacion_especial.monto_venta_c',
  'autorizacion_c' => 'lowae_autorizacion_especial.autorizacion_c',
  'fecha_inicio_c' => 'lowae_autorizacion_especial.fecha_inicio_c',
  'fecha_fin_c' => 'lowae_autorizacion_especial.fecha_fin_c',
  'estado_promesa_c' => 'lowae_autorizacion_especial.estado_promesa_c',
  'date_modified' => 'lowae_autorizacion_especial.date_modified',
  'date_entered' => 'lowae_autorizacion_especial.date_entered',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'motivo_solicitud_c',
  5 => 'assigned_user_id',
  6 => 'favorites_only',
  7 => 'monto_venta_c',
  8 => 'autorizacion_c',
  9 => 'fecha_inicio_c',
  10 => 'fecha_fin_c',
  11 => 'estado_promesa_c',
  12 => 'date_modified',
  13 => 'date_entered',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'motivo_solicitud_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_MOTIVO_SOLICITUD_C',
    'width' => '10%',
    'name' => 'motivo_solicitud_c',
  ),
  'monto_venta_c' => 
  array (
    'type' => 'currency',
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_MONTO_VENTA_C',
    'currency_format' => true,
    'width' => '10%',
    'name' => 'monto_venta_c',
  ),
  'autorizacion_c' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_AUTORIZACION_C',
    'width' => '10%',
    'name' => 'autorizacion_c',
  ),
  'fecha_inicio_c' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_FECHA_INICIO_C',
    'width' => '10%',
    'name' => 'fecha_inicio_c',
  ),
  'fecha_fin_c' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_FECHA_FIN_C',
    'width' => '10%',
    'name' => 'fecha_fin_c',
  ),
  'estado_promesa_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_ESTADO_PROMESA_C',
    'width' => '10%',
    'name' => 'estado_promesa_c',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'name' => 'date_modified',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'name' => 'date_entered',
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => '10%',
  ),
),
);
