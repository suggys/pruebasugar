<?php

use Sugarcrm\Sugarcrm\Util\Arrays\ArrayFunctions\ArrayFunctions;
include_once("include/workflow/alert_utils.php");
include_once("include/workflow/action_utils.php");
include_once("include/workflow/time_utils.php");
include_once("include/workflow/trigger_utils.php");
//BEGIN WFLOW PLUGINS
include_once("include/workflow/custom_utils.php");
//END WFLOW PLUGINS
	class lowae_autorizacion_especial_workflow {
	function process_wflow_triggers(& $focus){
		include("custom/modules/lowae_autorizacion_especial/workflow/triggers_array.php");
		include("custom/modules/lowae_autorizacion_especial/workflow/alerts_array.php");
		include("custom/modules/lowae_autorizacion_especial/workflow/actions_array.php");
		include("custom/modules/lowae_autorizacion_especial/workflow/plugins_array.php");
		if(empty($focus->fetched_row['id']) || (!empty($_SESSION["workflow_cron"]) && $_SESSION["workflow_cron"]=="Yes" && !empty($_SESSION["workflow_id_cron"]) && ArrayFunctions::in_array_access("7b5af0ca-91a4-11e6-a003-060b37ffb723", $_SESSION["workflow_id_cron"]))){ 
 
 if( (isset($focus->motivo_solicitud_c) && $focus->motivo_solicitud_c ==  stripslashes('Promesa de Pago'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['060d0730_91a5_11e6_a2d1_060b37ffb723'])){
		$triggeredWorkflows['060d0730_91a5_11e6_a2d1_060b37ffb723'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "e5ba7d6a-91a3-11e6-85fa-06b20b8677ed"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['lowae_autorizacion_especial0_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 


	//end function process_wflow_triggers
	}

	//end class
	}

?>
