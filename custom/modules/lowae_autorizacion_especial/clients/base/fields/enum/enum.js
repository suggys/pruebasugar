({
	extendsFrom: 'EnumField',

    _filterOptions: function (options) {
        var currentValue,
            syncedVal,
            newOptions = {},
            filter = app.metadata.getEditableDropdownFilter(this.def.options);

        /**
         * Flag to indicate that the options have already been filtered and do
         * not need to be sorted.
         *
         * @type {boolean}
         */
        this.isFiltered = !_.isEmpty(filter);
        // if(this.name === 'estado_autorizacion_c'){
        //     debugger;
        // }
    	if(
            (this.view.currentState === 'record' || this.view.currentState === 'create' || this.view.currentState === 'edit')
            && this.name === 'estado_autorizacion_c'
        ){
					// if(!_.contains(this.value,"finalizada"))
        	// 	options.finalizada && delete(options.finalizada);
					if(!_.contains(this.value,"cancelada_devolucion_cancelacion"))
  					options.cancelada_devolucion_cancelacion && delete(options.cancelada_devolucion_cancelacion);
    	}


        if (!this.isFiltered) {
        	return options;
        }

        if (!_.contains(this.view.plugins, "Editable")) {
            return options;
        }
        //Force the current value(s) into the availible options
				if(this.model){
        syncedVal = this.model.getSynced();
        	currentValue = _.isUndefined(syncedVal[this.name]) ? this.model.get(this.name) : syncedVal[this.name];
        	if (_.isString(currentValue)) {
            currentValue = [currentValue];
        	}
				}

        var currentIndex = {};

        // add current values to the index in case if current model is saved to the server in order to prevent data loss
				if (this.model){
        if (!this.model.isNew()) {
            _.each(currentValue, function(value) {
                currentIndex[value] = true;
            });
        }
				}

        //Now remove the disabled options
        if (!this._keysOrder) {
            this._keysOrder = {};
        }
        _.each(filter, function(val, index) {
            var key = val[0],
                visible = val[1];
            if ((visible || key in currentIndex) && !_.isUndefined(options[key]) && options[key] !== false) {
                this._keysOrder[key] = index;
                newOptions[key] = options[key];
            }
        }, this);

        if(
    		this.name === 'estado_autorizacion_c'
            && (this.view.currentState === 'record' || this.view.currentState === 'create' || this.view.currentState === 'edit')
        ){
					// if(!_.contains(this.value,"finalizada"))
          //   newOptions.finalizada && delete(newOptions.finalizada);
					if(!_.contains(this.value,"cancelada_devolucion_cancelacion"))
    				newOptions.cancelada_devolucion_cancelacion && delete(newOptions.cancelada_devolucion_cancelacion);
        }
        return newOptions;
    },

    // _render: function () {
    //  	this._super('_render', arguments);


    // }

})
