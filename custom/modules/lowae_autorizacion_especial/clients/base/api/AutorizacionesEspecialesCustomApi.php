<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class AutorizacionesEspecialesCustomApi extends SugarApi
{
	public function registerApiRest()
	{
		return array(
			'filterModuleGet' => array(
				'reqType' => 'GET',
				'path' => array('AutorizacionesEspeciales', 'abierta'),
				'pathVars' => array('',''),
				'jsonParams' => array('cliente','motivo'),
				'method' => 'existeAutEspAbierta',
				'shortHelp' => 'Verifica que el cliente sólo tenga una Autorización Especiales abierta.',
				'longHelp' => '',
				'ignoreMetaHash' => true,
				'ignoreSystemStatusError' => true,
			)
		);
	}

	public function existeAutEspAbierta($api, $args)
	{
		global $app_list_strings;
		$error_handler = array();
		$autsesps = $this->consultaAECliente($args['cliente']);

		if(count($autsesps) > 0){
			$error_handler['code'] = '304';
			$motivo = $app_list_strings['Motivo_Solicitud_list'][$autsesps[0]['motivo_solicitud_c']];
			$error_handler['msg'] = "Existe una autorización especial por $motivo válida y vigente, es necesario finalizarla para aprobar una nueva";
		}else {
			$error_handler['code'] = '200';
			$error_handler['msg'] = 'OK';
		}
		return $error_handler;
	}

	private function consultaAECliente($idCliente)
	{
		require_once('include/SugarQuery/SugarQuery.php');
		$autsesps = [];

		$sq = new SugarQuery();
		$sq->select(array('id','motivo_solicitud_c'));
		$sq->from(BeanFactory::newBean('lowae_autorizacion_especial'),array('alias'=>'lae','team_security'=>false));
		$sq->joinTable('accounts_lowae_autorizacion_especial_1_c', array('alias' => 'alae'))->on()
	  ->equalsField('lae.id','alae.accounts_le69cspecial_idb');
		$sq->joinTable('accounts', array('alias' => 'a'))->on()
	  ->equalsField('a.id','alae.accounts_lowae_autorizacion_especial_1accounts_ida')
		->equals('alae.deleted',0);
		$sq->joinTable('lowae_autorizacion_especial_cstm', array('alias'=>'lae1_c'))->on()
		->equalsField('lae1_c.id_c','lae.id');
		$sq->whereRaw("lae.fecha_inicio_c < now() AND (lae.fecha_fin_c >= now() OR lae.fecha_cumplimiento_c >= now())");
		$sq->where()->queryAnd()->equals('a.id',$idCliente)
		->equals('a.deleted',0)
		->equals('lae1_c.estado_autorizacion_c','autorizada');		
		$result = $sq->execute();
	  foreach ($result as $key => $row) {
			$autsesps[] = [
				'motivo_solicitud_c' => $row['motivo_solicitud_c']
			];
		}

		return $autsesps;
	}

}
