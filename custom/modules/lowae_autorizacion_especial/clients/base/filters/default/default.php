<?php
// created: 2016-12-22 22:31:16
$viewdefs['lowae_autorizacion_especial']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' =>
  array (
    'accounts_lowae_autorizacion_especial_1_name' =>
    array (
    ),
    'name' =>
    array (
    ),
    'motivo_solicitud_c' =>
    array (
    ),
    'monto_venta_c' =>
    array (
    ),
    'estado_autorizacion_c' => 
    array (
    ),
    'fecha_inicio_c' =>
    array (
    ),
    'fecha_fin_c' =>
    array (
    ),
    'estado_promesa_c' =>
    array (
    ),
    'assigned_user_name' =>
    array (
    ),
    'date_modified' =>
    array (
    ),
    'date_entered' =>
    array (
    ),
    '$owner' =>
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' =>
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);
