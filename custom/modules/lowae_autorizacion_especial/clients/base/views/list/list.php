<?php
$module_name = 'lowae_autorizacion_especial';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'readonly' => true,
              ),
              1 =>
              array (
                'name' => 'motivo_solicitud_c',
                'label' => 'LBL_MOTIVO_SOLICITUD_C',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              2 =>
              array (
                'name' => 'accounts_lowae_autorizacion_especial_1_name',
                'label' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE',
                'enabled' => true,
                'id' => 'ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1ACCOUNTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
                'readonly' => true,
                'readonly' => true,
              ),
              3 =>
              array (
                'name' => 'monto_venta_c',
                'label' => 'LBL_MONTO_VENTA_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
                'readonly' => true,
              ),
              4 =>
              array (
                'name' => 'fecha_inicio_c',
                'label' => 'LBL_FECHA_INICIO_C',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              5 =>
              array (
                'name' => 'estado_autorizacion_c',
                'label' => 'LBL_ESTADO_AUTORIZACION',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              6 =>
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'readonly' => true,
              ),
              7 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              8 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              9 =>
              array (
                'name' => 'fecha_fin_c',
                'label' => 'LBL_FECHA_FIN_C',
                'enabled' => true,
                'default' => false,
                'readonly' => true,
              ),
              10 =>
              array (
                'name' => 'credito_diponible_temporal_c',
                'label' => 'LBL_CREDITO_DIPONIBLE_TEMPORAL_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              11 =>
              array (
                'name' => 'limite_credito_temporal_c',
                'label' => 'LBL_LIMITE_CREDITO_TEMPORAL_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              12 =>
              array (
                'name' => 'aplica_por_unica_ocacion_c',
                'label' => 'LBL_APLICA_POR_UNICA_OCACION',
                'enabled' => true,
                'default' => false,
                'readonly' => true,
              ),
              13 =>
              array (
                'name' => 'numero_veces_aplicacion_c',
                'label' => 'LBL_NUMERO_VECES_APLICACION',
                'enabled' => true,
                'default' => false,
                'readonly' => true,
              ),
              14 =>
              array (
                'name' => 'monto_promesa_c',
                'label' => 'LBL_MONTO_PROMESA_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              15 =>
              array (
                'name' => 'fecha_cumplimiento_c',
                'label' => 'LBL_FECHA_CUMPLIMIENTO_C',
                'enabled' => true,
                'default' => false,
                'readonly' => true,
              ),
              16 =>
              array (
                'name' => 'estado_promesa_c',
                'label' => 'LBL_ESTADO_PROMESA_C',
                'enabled' => true,
                'default' => false,
                'readonly' => true,
              ),
              17 =>
              array (
                'name' => 'monto_abonado_c',
                'label' => 'LBL_MONTO_ABONADO_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
