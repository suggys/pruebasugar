({
    extendsFrom:"CreateView",

    initialize: function () {
        var self = this;
        this._super("initialize", arguments);
        this.model.addValidationTask('validateFechaFinCumplimiento',_.bind(this._doValidateFechaFinCumplimiento, this));
        this.model.addValidationTask('validatePeriodoEdicion',_.bind(this._doValidatePeriodoEdicion, this));
        this.model.addValidationTask('validateEstadoAutorizacion',_.bind(this._doValidateEstadoAutorizacion, this));
        this.model.addValidationTask('validateMontoDeVenta',_.bind(this._doValidateMontoDeVenta, this));
        this.model.addValidationTask('validateMontoDePromesa',_.bind(this._doValidateMontoDePromesa, this));

        this.model.on('change:aplica_por_unica_ocacion_c',_.bind(this._changeAplicaUnicaOcasion, this));
        self.model.on('change:motivo_solicitud_c', _.bind(this._setEstadoPromesaPago, this));
        self.fieldsDisabled = false;
        self.model.on('sync', function(model, value){
            self._disableFields(app.user.get('roles'));
        });
    },

    _doValidateMontoDeVenta: function (fields, errors, callback){
      var self = this;
      var motivo = self.model.get('motivo_solicitud_c');

      if(parseFloat(self.model.get('monto_venta_c')) == parseFloat(0)){
        errors['monto_venta_c'] = errors['monto_venta_c'] || {};
        errors['monto_venta_c']['El monto de venta no puede ser 0.'] = true;
        callback(null, fields, errors);
        return true;
      }

      if(parseFloat(self.model.get('monto_venta_c')) < parseFloat(0)){
        errors['monto_venta_c'] = errors['monto_venta_c'] || {};
        errors['monto_venta_c']['El monto de venta no puede ser menor a 0.'] = true;
        callback(null, fields, errors);
        return true;
      }

      if(motivo === "credito_disponible_temporal"){
        var credito_diponible_temporal_c = parseFloat(self.model.get('credito_diponible_temporal_c'));
        var msg = 'El Monto Autorizado de Cr\u00E9dito Disponible Temporal no puede ser igual o menor a $0.00. Esto puede suceder si la cuenta tiene suficiente Cr\u00E9dito Disponible para la venta pero es administrada por un grupo de empresas que la bloquea. Favor de realizar una autorizaci\u00F3n especial por motivo "Otro".';

        if(parseFloat(credito_diponible_temporal_c) <= parseFloat(0)){
          errors['monto_venta_c'] = errors['monto_venta_c'] || {};
          errors['monto_venta_c'][msg] = true;
        }
        callback(null, fields, errors);
      } else {
        callback(null, fields, errors);
      }

    },

    _doValidateMontoDePromesa: function (fields, errors, callback){
      var self = this;
      var motivo = self.model.get('motivo_solicitud_c');
      if(motivo === 'promesa_de_pago'){
        if(parseFloat(self.model.get('monto_promesa_c')) == parseFloat(0)){
          errors['monto_promesa_c'] = errors['monto_promesa_c'] || {};
          errors['monto_promesa_c']['El monto de promesa no puede ser 0.'] = true;
        }
      }
      callback(null, fields, errors);
    },

    _changeAplicaUnicaOcasion: function (model, value) {
        var self = this;
        var unicaocasion = self.model.get('aplica_por_unica_ocacion_c');
        var motivo = self.model.get('motivo_solicitud_c');
        if(unicaocasion && (motivo === 'credito_disponible_temporal' || motivo === 'otro')){
            self.model.set('estado_c','por_aplicar');
        }
        else
        {
            self.model.set('estado_c','');
        }
    },

    // Se deshabilitan los campos cuando el usuario no es Administrador de Credito
    _disableFields:function(rolesArray){
        var self = this;
        var estado_promesa_c = self.getField('estado_promesa_c');
        estado_promesa_c.setMode('readonly');
        estado_promesa_c.setDisabled('true');

        if(app.user.get('type') != 'admin'){
            if( !_.filter(rolesArray, function(rol) { return rol.toLowerCase() == "administrador de credito";}).length ){
                var autorizacion = self.getField('estado_autorizacion_c');
                autorizacion.setMode('readonly');
                autorizacion.setDisabled('true');
            }
        }

    },

    _renderHtml: function (argument) {
        var self = this;
        this._super('_renderHtml', arguments);
    },

    _render: function (argument) {
        var self = this;
        this._super('_render', arguments);
    },

    _setEstadoPromesaPago: function (model, value) {
      var self = this;
      if(!this.$el.html()){return false;}
      model.set('estado_promesa_c', (value === 'promesa_de_pago' ? 'vigente' : ""));
      var estadoAutorizacionField = self.getField('estado_autorizacion_c');
      estadoAutorizacionField.items = app.lang.getAppListStrings('estado_autorizacion_list');
      estadoAutorizacionField.render();
    },

    _doValidateFechaFinCumplimiento: function(fields, errors, callback) {
      var fechaInicio = new app.date(this.model.get('fecha_inicio_c'));
      var fechaFinField = 'fecha_fin_c';
      var mensaje = 'La Fecha de Fin debe de ser posterior a la Fecha de Inicio.';
      if(
        this.model.get('motivo_solicitud_c') === 'promesa_de_pago'
      ){
        fechaFinField = "fecha_cumplimiento_c";
        mensaje = "La Fecha de Cumplimiento de la Promesa de Pago debe de ser posterior a la Fecha de Inicio.";
      }
      var fechaFin = new app.date(this.model.get(fechaFinField));
      var diff = fechaInicio.diff(fechaFin);


      if(diff >= 0){
        errors[fechaFinField] = errors[fechaFinField] || {};
        errors[fechaFinField][mensaje] = true;
      }
      callback(null, fields, errors);
    },

    _doValidatePeriodoEdicion: function (fields, errors, callback) {
      var fechaInicio = new app.date(this.model.get('fecha_inicio_c'));
      var fechaInicioMas30 = fechaInicio.clone();
      fechaInicioMas30.add(30,'days');
      var fechaFinField = 'fecha_fin_c';
      var mensaje = 'La Fecha de Fin no debe ser mayor a 30 d\u00edas despues de la Fecha de Inicio.';
      if(
        this.model.get('motivo_solicitud_c') === 'promesa_de_pago'
      ){
        fechaFinField = "fecha_cumplimiento_c";
        mensaje = "La Fecha de Cumplimiento de la Promesa de Pago no debe ser mayor a 30 d\u00edas despues de la Fecha de Inicio.";
      }
      if(!_.isEmpty(errors[fechaFinField])){callback(null, fields, errors);}
      else{
        var fechaFin = new app.date(this.model.get(fechaFinField));
        var diff = fechaFin.diff(fechaInicioMas30);
        if(diff > 0){
          errors[fechaFinField] = errors[fechaFinField] || {};
          errors[fechaFinField][mensaje] = true;
        }
        callback(null, fields, errors);
      }
    },

    _doValidateEstadoAutorizacion: function (fields, errors, callback) {
      var self = this;
      if(self.model.get('estado_autorizacion_c') === 'autorizada'){
        var motivo = self.model.get('motivo_solicitud_c');
        var cliente = self.model.get('accounts_lowae_autorizacion_especial_1accounts_ida');
        var filters = {cliente:cliente,motivo:motivo};
        filters = $.param(filters);
        app.api.call('read','/rest/v10/AutorizacionesEspeciales/abierta?'+filters, {}, {
          success: function(data){
            self._processSuccess(data, function (isValid) {
              if(!isValid){
                errors['estado_autorizacion_c'] = errors['estado_autorizacion_c'] || {};
                errors['estado_autorizacion_c'][data.msg] = true;
              }
              callback(null, fields, errors);
            });
          }
        });
      }else {
        callback(null, fields, errors);
      }
    },

    _processSuccess: function (data, callback) {
      var isValid = false;
      if(data.code == 200){
        isValid = true;
      }
      callback(isValid);
    },
})
