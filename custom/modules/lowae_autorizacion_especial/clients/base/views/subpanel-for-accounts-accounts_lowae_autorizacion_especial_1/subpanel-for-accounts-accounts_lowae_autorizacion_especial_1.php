<?php
// created: 2017-01-11 17:12:18
$viewdefs['lowae_autorizacion_especial']['base']['view']['subpanel-for-accounts-accounts_lowae_autorizacion_especial_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'motivo_solicitud_c',
          'label' => 'LBL_MOTIVO_SOLICITUD_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'fecha_inicio_c',
          'label' => 'LBL_FECHA_INICIO_C',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'fecha_fin_c',
          'label' => 'LBL_FECHA_FIN_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'estado_autorizacion_c',
          'label' => 'LBL_ESTADO_AUTORIZACION',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'fecha_cumplimiento_c',
          'label' => 'LBL_FECHA_CUMPLIMIENTO_C',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);