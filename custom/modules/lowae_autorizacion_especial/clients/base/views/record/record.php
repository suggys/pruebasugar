<?php
$module_name = 'lowae_autorizacion_especial';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'record' =>
      array (
        'buttons' =>
        array (
          0 =>
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' =>
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 =>
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 =>
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' =>
            array (
              0 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 =>
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 =>
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 =>
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 =>
              array (
                'type' => 'divider',
              ),
              5 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'lowae_autorizacion_especial',
                'acl_action' => 'create',
              ),
              7 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 =>
              array (
                'type' => 'divider',
              ),
              9 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 =>
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' =>
        array (
          0 =>
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 =>
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 =>
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 =>
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'motivo_solicitud_c',
                'label' => 'LBL_MOTIVO_SOLICITUD_C',
              ),
              1 =>
              array (
                'name' => 'accounts_lowae_autorizacion_especial_1_name',
                'label' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE',
                'required' => true,
              ),
              2 =>
              array (
                'name' => 'monto_venta_c',
                'studio' => 'visible',
                'label' => 'LBL_MONTO_VENTA_C',
              ),
              3 =>
              array (
                'name' => 'comentarios_c',
                'studio' => 'visible',
                'label' => 'LBL_COMENTARIOS_C',
              ),
            ),
          ),
          2 =>
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'credito_diponible_temporal_c',
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_CREDITO_DIPONIBLE_TEMPORAL_C',
                'readonly' => true,
              ),
              1 =>
              array (
              ),
              2 =>
              array (
                'name' => 'monto_promesa_c',
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_MONTO_PROMESA_C',
              ),
              3 =>
              array (
              ),
              4 =>
              array (
                'name' => 'monto_abonado_c',
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_MONTO_ABONADO_C',
                'readonly' => true,
              ),
              5 =>
              array (
                'name' => 'porcentaje_cumplimiento_promesa_c',
                'label' => 'LBL_PORCENTAJE_CUMPLIMIENTO_PROMESA',
                'readonly' => true,
              ),
              6 =>
              array (
                'name' => 'fecha_inicio_c',
                'label' => 'LBL_FECHA_INICIO_C',
              ),
              7 =>
              array (
                'name' => 'fecha_fin_c',
                'label' => 'LBL_FECHA_FIN_C',
              ),
              8 =>
              array (
                'name' => 'fecha_cumplimiento_c',
                'label' => 'LBL_FECHA_CUMPLIMIENTO_C',
              ),
              9 =>
              array (
                'name' => 'dias_vencimiento_promesa_c',
                'label' => 'LBL_DIAS_VENCIMIENTO_PROMESA',
                'readonly' => true,
              ),
              10 =>
              array (
                'name' => 'aplica_por_unica_ocacion_c',
                'label' => 'LBL_APLICA_POR_UNICA_OCACION',
              ),
              11 =>
              array (
                'name' => 'numero_veces_aplicacion_c',
                'label' => 'LBL_NUMERO_VECES_APLICACION',
                'readonly' => true,
              ),
              12 =>
              array (
                'name' => 'estado_promesa_c',
                'label' => 'LBL_ESTADO_PROMESA_C',
                'readonly' => true,
              ),
              13 =>
              array (
                'name' => 'estado_promesa_segun_monto_c',
                'label' => 'LBL_ESTADO_PROMESA_SEGUN_MONTO',
                'readonly' => true,
              ),
              14 =>
              array (
                'name' => 'estado_c',
                'label' => 'LBL_ESTADO_C'                
              ),
              15 =>
              array (
              ),
              16 =>
              array (
                'name' => 'estado_autorizacion_c',
                'label' => 'LBL_ESTADO_AUTORIZACION',
              ),
              17 =>
              array (
                'name' => 'fecha_autorizacion_c',
                'label' => 'LBL_FECHA_AUTORIZACION',
              ),
              18 =>
              array (
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'saldo_pendiente_aplicar_c',
                'label' => 'LBL_SALDO_PENDIENTE_APLICAR',
                'readonly' => true,
              ),
              19 =>
              array (
              ),
            ),
          ),
          3 =>
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' =>
            array (
              0 => 'assigned_user_name',
              1 => 'team_name',
              2 =>
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' =>
                array (
                  0 =>
                  array (
                    'name' => 'date_modified',
                  ),
                  1 =>
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 =>
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              3 =>
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' =>
                array (
                  0 =>
                  array (
                    'name' => 'date_entered',
                  ),
                  1 =>
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 =>
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' =>
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
