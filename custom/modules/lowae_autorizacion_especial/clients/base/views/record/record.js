({
  extendsFrom: 'RecordView',

  initialize: function(options) {
    this._super('initialize', [options]);
    var self = this;
    self.fieldsDisabled = false;
    self.model.on('sync', function(model, value){
      self._disableFields(app.user.get('roles'));
    });

    this.model.addValidationTask('validateEstadoAutorizacion',_.bind(this._doValidateEstadoAutorizacion, this));
    this.model.addValidationTask('validateMotivoSolicitud',_.bind(this._doValidateMotivoSolicitud, this));
    this.model.addValidationTask('validateFechaAutorizacion',_.bind(this._doFechaAutorizacion, this));
    this.model.addValidationTask('validateFechaFinCumplimiento',_.bind(this._doValidateFechaFinCumplimiento, this));
    this.model.addValidationTask('validatePeriodoEdicion',_.bind(this._doValidatePeriodoEdicion, this));
    this.model.addValidationTask('validateMontoDeVenta',_.bind(this._doValidateMontoDeVenta, this));
    this.model.addValidationTask('validateMontoDePromesa',_.bind(this._doValidateMontoDePromesa, this));

    this.model.on('change:aplica_por_unica_ocacion_c',_.bind(this._changeAplicaUnicaOcasion, this));
    this.model.on('change:motivo_solicitud_c',_.bind(this._changeMotivoSolicitud, this));
  },

  _changeAplicaUnicaOcasion: function () {
    var self = this;
    var unicaocasion = self.model.get('aplica_por_unica_ocacion_c');
    var motivo = self.model.get('motivo_solicitud_c');
    if(unicaocasion && motivo === 'credito_disponible_temporal' && self.model.get('estado_c') ==='' ){
      self.model.set('estado_c','por_aplicar');
    }
  },

  // Se deshabilitan los campos cuando el usuario no es Administrador de Crédito
  _disableFields:function(){
    var self = this;
    var estado_promesa_c = self.getField('estado_promesa_c');
    estado_promesa_c.setMode('readonly');
    estado_promesa_c.setDisabled('true');
    this.render();
  },

  _doValidateEstadoAutorizacion: function (fields, errors, callback) {
    var rolesArray = app.user.get('roles');
    var self = this;

    if(app.user.get('type') != 'admin')
    {
      if(!_.filter(rolesArray, function(rol) { return rol.toLowerCase() == "administrador de credito";}).length
      || !_.filter(rolesArray, function(rol) { return rol.toLowerCase() == "gerente de credito";}).length){
        if(
          (this.model.get('estado_autorizacion_c') === 'rechazada' && this.model.getSynced().estado_autorizacion_c === 'rechazada')
          || (this.model.get('estado_autorizacion_c') === 'finalizada' && this.model.getSynced().estado_autorizacion_c === 'finalizada')
          || (this.model.get('estado_autorizacion_c') === 'terminada_incumplida' && this.model.getSynced().estado_autorizacion_c === 'terminada_incumplida')
          || (this.model.get('estado_autorizacion_c') === 'cancelada_devolucion_cancelacion' && this.model.getSynced().estado_autorizacion_c === 'cancelada_devolucion_cancelacion')
        ){
          estadoLabel = app.lang.getAppListStrings('estado_autorizacion_list')[this.model.get('estado_autorizacion_c')];
          errors['name'] = errors['name'] || {};
          errors['name']['Una vez ' + estadoLabel + ' la autorizaci\u00f3n especial, no es posible modificarla.'] = true;
          callback(null, fields, errors);
          return true;
        }
      } else {
        if(
          (this.model.get('estado_autorizacion_c') === 'autorizada' && this.model.getSynced().estado_autorizacion_c === 'autorizada')
          || (this.model.get('estado_autorizacion_c') === 'rechazada' && this.model.getSynced().estado_autorizacion_c === 'rechazada')
          || (this.model.get('estado_autorizacion_c') === 'finalizada' && this.model.getSynced().estado_autorizacion_c === 'finalizada')
          || (this.model.get('estado_autorizacion_c') === 'terminada_incumplida' && this.model.getSynced().estado_autorizacion_c === 'terminada_incumplida')
          || (this.model.get('estado_autorizacion_c') === 'cancelada_devolucion_cancelacion' && this.model.getSynced().estado_autorizacion_c === 'cancelada_devolucion_cancelacion')
        ){
          estadoLabel = app.lang.getAppListStrings('estado_autorizacion_list')[this.model.get('estado_autorizacion_c')];
          errors['name'] = errors['name'] || {};
          errors['name']['Una vez ' + estadoLabel + ' la autorizaci\u00f3n especial, no es posible modificarla.'] = true;
          callback(null, fields, errors);
          return true;
        }
      }
    }

    if(
      self.model.get('estado_autorizacion_c') === 'autorizada'
      && self.model.getSynced().estado_autorizacion_c != 'autorizada'
    ){
      var motivo = self.model.get('motivo_solicitud_c');
      var cliente = self.model.get('accounts_lowae_autorizacion_especial_1accounts_ida');
      var filters = {cliente:cliente,motivo:motivo};
      filters = $.param(filters);
      app.api.call('read','/rest/v10/AutorizacionesEspeciales/abierta?'+filters, {}, {
        success: function(data){
          self._processSuccess(data, function (isValid) {
            if(!isValid){
              errors['estado_autorizacion_c'] = errors['estado_autorizacion_c'] || {};
              errors['estado_autorizacion_c'][data.msg] = true;
            }
            callback(null, fields, errors);
          });
        }
      });
    }else {
      callback(null, fields, errors);
    }
  },

  _processSuccess: function (data, callback) {
    var isValid = false;
    if(data.code == 200){
      isValid = true;
    }
    callback(isValid);
  },

  _doValidateMotivoSolicitud: function(fields, errors, callback) {
    if(
      this.model.get('motivo_solicitud_c') != this.model.getSynced().motivo_solicitud_c
    ){
      errors['motivo_solicitud_c'] = errors['motivo_solicitud_c'] || {};
      errors['motivo_solicitud_c']['Una vez creada la autorizaci\u00f3n especial, no es posible modificar el motivo.'] = true;
    }
    callback(null, fields, errors);
  },

  _changeMotivoSolicitud: function (model, value) {
    var self = this;
    if(this.currentState === 'view' ){return}
    if(!this.model.isNotEmpty){return}
    var estadoAutorizacionField = self.getField('estado_autorizacion_c');
    estadoAutorizacionField.render();
  },

  _doValidateMontoDeVenta: function (fields, errors, callback){
    var rolesArray = app.user.get('roles');
    var self = this;

    if(parseFloat(self.model.get('monto_venta_c')) == parseFloat(0)){
      errors['monto_venta_c'] = errors['monto_venta_c'] || {};
      errors['monto_venta_c']['El monto de venta no puede ser 0.'] = true;
      callback(null, fields, errors);
      return true;
    }

    // if (self.model.get('motivo_solicitud_c') === "credito_disponible_temporal" || self.model.get('motivo_solicitud_c') === "otro"){
      if(
        _.filter(rolesArray, function(rol) { return rol.toLowerCase() == "administrador de credito";}).length
        || _.filter(rolesArray, function(rol) { return rol.toLowerCase() == "gerente de credito";}).length
        || app.user.get('type') === 'admin'
      ){

      var monto_venta_c_ant = self.model.getSynced().monto_venta_c;
      var credito_diponible_temporal_c_ant = self.model.getSynced().credito_diponible_temporal_c;
      var saldo_pendiente_aplicar_c_ant = self.model.getSynced().saldo_pendiente_aplicar_c;

      var saldo_pendiente_aplicar_c = self.model.get('saldo_pendiente_aplicar_c');
      var credito_diponible_temporal_c = self.model.get('credito_diponible_temporal_c');
      var monto_venta_c = self.model.get('monto_venta_c');

      if(parseFloat(monto_venta_c_ant) != parseFloat(monto_venta_c)){
        if(self.model.get('motivo_solicitud_c') != "otro"){
          if(parseFloat(saldo_pendiente_aplicar_c) != parseFloat(credito_diponible_temporal_c)){
            if(parseFloat(monto_venta_c) < parseFloat(saldo_pendiente_aplicar_c)) {
              errors['monto_venta_c'] = errors['monto_venta_c'] || {};
              errors['monto_venta_c']['El monto de venta no puede ser menor al monto pendiente por aplicar.'] = true;
              callback(null, fields, errors);
              return true;
            } else {
                credito_diponible_temporal_c = parseFloat(credito_diponible_temporal_c_ant) + (parseFloat(monto_venta_c) - parseFloat(monto_venta_c_ant));
                saldo_pendiente_aplicar_c = parseFloat(saldo_pendiente_aplicar_c_ant) + (parseFloat(monto_venta_c) - parseFloat(monto_venta_c_ant));
                self.model.set({'credito_diponible_temporal_c':credito_diponible_temporal_c,'saldo_pendiente_aplicar_c':saldo_pendiente_aplicar_c});
            }
          } else {
            credito_diponible_temporal_c = parseFloat(credito_diponible_temporal_c_ant) + (parseFloat(monto_venta_c) - parseFloat(monto_venta_c_ant));
            saldo_pendiente_aplicar_c = parseFloat(saldo_pendiente_aplicar_c_ant) + (parseFloat(monto_venta_c) - parseFloat(monto_venta_c_ant));
            self.model.set({'credito_diponible_temporal_c':credito_diponible_temporal_c,'saldo_pendiente_aplicar_c':saldo_pendiente_aplicar_c});
          }
        } else {
          //TODO: Revisar formula de otro...
          if(parseFloat(saldo_pendiente_aplicar_c) == parseFloat(monto_venta_c_ant))
          {
            saldo_pendiente_aplicar_c = parseFloat(saldo_pendiente_aplicar_c_ant) + (parseFloat(monto_venta_c) - parseFloat(monto_venta_c_ant));
            self.model.set({'saldo_pendiente_aplicar_c':saldo_pendiente_aplicar_c});
          }
          else
          {
            if(parseFloat(monto_venta_c) < parseFloat(saldo_pendiente_aplicar_c)){
              debugger;
              errors['monto_venta_c'] = errors['monto_venta_c'] || {};
              errors['monto_venta_c']['El monto de venta no puede ser menor al monto pendiente por aplicar.'] = true;
              callback(null, fields, errors);
              return true;
            } else {
              debugger;
              saldo_pendiente_aplicar_c = parseFloat(saldo_pendiente_aplicar_c_ant) + (parseFloat(monto_venta_c) - parseFloat(monto_venta_c_ant));
              if(parseFloat(saldo_pendiente_aplicar_c) < 0){
                errors['monto_venta_c'] = errors['monto_venta_c'] || {};
                errors['monto_venta_c']['El nuevo Monto Pendiente por Aplicar ser\u00E1 $'+saldo_pendiente_aplicar_c+'. No es posible tener un Monto Pendiente por Aplicar menor a $0.00.'] = true;
                callback(null, fields, errors);
                return true;
              } else {
                self.model.set({'saldo_pendiente_aplicar_c':saldo_pendiente_aplicar_c});
              }
            }
          }
        }
      }
    } else {
      if (
        self.model.get('estado_autorizacion_c') == 'autorizada'
        && self.model.getSynced().monto_venta_c != self.model.get('monto_venta_c')
      ){
        errors['monto_venta_c'] = errors['monto_venta_c'] || {};
        errors['monto_venta_c']['El monto de venta no puede ser modificado por usted.'] = true;
        callback(null, fields, errors);
        return true;
      }
    }
  // }
  callback(null, fields, errors);
  },

  _doFechaAutorizacion: function (fields, errors, callback) {
    if(
      this.model.get('estado_autorizacion_c') != this.model.getSynced().estado_autorizacion_c
      && this.model.get('estado_autorizacion_c') === 'autorizada'
    ){
      var today = new app.date();
      var fechaFinField = "fecha_fin_c";
      var mensaje = "La Fecha de Fin debe de ser posterior al d\u00eda de hoy.";
      if(
        this.model.get('motivo_solicitud_c') === 'promesa_de_pago'
      ){
        fechaFinField = "fecha_cumplimiento_c";
        mensaje = "La Fecha de Cumplimiento de la Promesa de Pago debe de ser posterior al d\u00eda de hoy.";
      }
      var fechaFin = new app.date(this.model.get(fechaFinField));
      var diff = today.diff(fechaFin);
      if(diff >= 0){
        errors[fechaFinField] = errors[fechaFinField] || {};
        errors[fechaFinField][mensaje] = true;
      }
    }
    callback(null, fields, errors);
  },

  _doValidateFechaFinCumplimiento: function(fields, errors, callback) {
    var fechaInicio = new app.date(this.model.get('fecha_inicio_c'));
    var fechaFinField = 'fecha_fin_c';
    var mensaje = 'La Fecha de Fin debe de ser posterior a la Fecha de Inicio.';
    if(
      this.model.get('motivo_solicitud_c') === 'promesa_de_pago'
    ){
      fechaFinField = "fecha_cumplimiento_c";
      mensaje = "La Fecha de Cumplimiento de la Promesa de Pago debe de ser posterior a la Fecha de Inicio.";
    }
    var fechaFin = new app.date(this.model.get(fechaFinField));
    var diff = fechaInicio.diff(fechaFin);

    if(!_.isEmpty(errors[fechaFinField])){callback(null, fields, errors);}
    else{
      if(diff >= 0){
        errors[fechaFinField] = errors[fechaFinField] || {};
        errors[fechaFinField][mensaje] = true;
      }
      callback(null, fields, errors);
    }
  },

  _doValidatePeriodoEdicion: function (fields, errors, callback) {
    var fechaInicio = new app.date(this.model.get('fecha_inicio_c'));
    var fechaInicioMas30 = fechaInicio.clone();
    fechaInicioMas30.add(30,'days');
    var fechaFinField = 'fecha_fin_c';
    var mensaje = 'La Fecha de Fin no debe ser mayor a 30 d\u00edas despues de la Fecha de Inicio.';
    if(
      this.model.get('motivo_solicitud_c') === 'promesa_de_pago'
    ){
      fechaFinField = "fecha_cumplimiento_c";
      mensaje = "La Fecha de Cumplimiento de la Promesa de Pago no debe ser mayor a 30 d\u00edas despues de la Fecha de Inicio.";
    }
    if(!_.isEmpty(errors[fechaFinField])){callback(null, fields, errors);}
    else{
      var fechaFin = new app.date(this.model.get(fechaFinField));
      var diff = fechaFin.diff(fechaInicioMas30);
      if(diff > 0){
        errors[fechaFinField] = errors[fechaFinField] || {};
        errors[fechaFinField][mensaje] = true;
      }
      callback(null, fields, errors);
    }
  },

  _doValidateMontoDePromesa: function (fields, errors, callback){
    var self = this;
    var motivo = self.model.get('motivo_solicitud_c');
    if(motivo === 'promesa_de_pago'){
      if(parseFloat(self.model.get('monto_promesa_c')) == parseFloat(0)){
        errors['monto_promesa_c'] = errors['monto_promesa_c'] || {};
        errors['monto_promesa_c']['El monto de promesa no puede ser 0.'] = true;
      }
    }
    callback(null, fields, errors);
  },
})
