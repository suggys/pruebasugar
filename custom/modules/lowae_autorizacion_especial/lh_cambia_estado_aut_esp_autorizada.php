<?php
require_once('custom/modules/EmailTemplates/NotifierHelper.php');
class lh_cambia_estado_aut_esp_auth{
    private $isDebug = false;

    public function fnCambiaEstadoCuandoAutoriza($bean, $event, $arguments) {
        if($this->isDebug)$GLOBALS['log']->fatal('fnCambiaEstadoCuandoAutoriza autorizacion_c::'. print_r($bean->autorizacion_c,1));

        if($bean->autorizacion_c !== $bean->fetched_row['autorizacion_c']
        && $bean->autorizacion_c
        && $bean->estado_promesa_c=='') {
            $bean->estado_promesa_c = 'vigente';
            if($this->isDebug)$GLOBALS['log']->fatal('fnCambiaEstadoCuandoAutoriza estado_promesa_c::'. print_r($bean->estado_promesa_c,1));
        }

        if(
          $bean->estado_autorizacion_c === 'pendiente'
        ){
          // $GLOBALS["log"]->fatal("Credito Disponible temporal --- $bean->credito_diponible_temporal_c");
          // $GLOBALS["log"]->fatal("Monto de venta --- $bean->monto_venta_c");
          $bean->saldo_pendiente_aplicar_c = ($bean->motivo_solicitud_c === "promesa_de_pago" || $bean->motivo_solicitud_c === "otro") ? $bean->monto_venta_c : $bean->credito_diponible_temporal_c;
        }

        if($bean->motivo_solicitud_c === "promesa_de_pago"
        && $bean->fetched_row['motivo_solicitud_c'] != "promesa_de_pago"
        && empty($bean->fetched_row['id']) ){
          $bean->fecha_fin_c = $bean->fecha_cumplimiento_c;
          $this->EnviaNotificacionPromesaPago($bean->id);
        }


        if(
          $bean->saldo_pendiente_aplicar_c === 0
          && $bean->fetched_row['saldo_pendiente_aplicar_c'] > 0
        ){
          $bean->estado_autorizacion_c = 'finalizada';
        }

        if($bean->monto_abonado_c != $bean->fetched_row['monto_abonado_c']){
          $bean->estado_promesa_segun_monto_c = 'sin_pago';
          if($bean->monto_abonado_c > 0){
            $bean->estado_promesa_segun_monto_c = floatval($bean->monto_abonado_c) === floatval($bean->monto_promesa_c) ? 'pagada_completa' : 'pagada_parcial';
          }
        }
    }

    private function EnviaNotificacionPromesaPago($id_aut_esp){
        global $timedate, $current_user, $db;
        $roles = array();
        $roles = ACLRole::getAllRoles(true);
        foreach ($roles as $rol) {
            if(strtolower($rol['name']) === 'administrador de credito'){
                $rolid = $rol['id'];
            }
        }
        $usrs = $db->query("SELECT user_id FROM acl_roles_users WHERE role_id = '" . $rolid . "' AND deleted = 0; ");
        $users = array();
        while($usr = mysqli_fetch_array($usrs)){
            $users[] = BeanFactory::getBean('Users', $usr['user_id']);
        }

        $aut_especial = BeanFactory::getBean('lowae_autorizacion_especial', $id_aut_esp);
        $notifierHelper = new NotifierHelper;
        $template_id = 'e5ba7d6a-91a3-11e6-85fa-06b20b8677ed';
        $notifierHelper->sendMessage($aut_especial, array("to" => $users), $template_id);
        return true;
    }

}
