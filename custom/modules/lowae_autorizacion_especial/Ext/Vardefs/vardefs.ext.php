<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/accounts_lowae_autorizacion_especial_1_lowae_autorizacion_especial.php

// created: 2016-10-13 23:48:34
$dictionary["lowae_autorizacion_especial"]["fields"]["accounts_lowae_autorizacion_especial_1"] = array (
  'name' => 'accounts_lowae_autorizacion_especial_1',
  'type' => 'link',
  'relationship' => 'accounts_lowae_autorizacion_especial_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'id_name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["lowae_autorizacion_especial"]["fields"]["accounts_lowae_autorizacion_especial_1_name"] = array (
  'name' => 'accounts_lowae_autorizacion_especial_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
  'link' => 'accounts_lowae_autorizacion_especial_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["lowae_autorizacion_especial"]["fields"]["accounts_lowae_autorizacion_especial_1accounts_ida"] = array (
  'name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE_ID',
  'id_name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
  'link' => 'accounts_lowae_autorizacion_especial_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_motivo_solicitud_c.php

 // created: 2016-11-23 02:24:12
$dictionary['lowae_autorizacion_especial']['fields']['motivo_solicitud_c']['default']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_limite_credito_temporal_c.php

 // created: 2016-11-23 03:05:36
$dictionary['lowae_autorizacion_especial']['fields']['limite_credito_temporal_c']['default']=0;
$dictionary['lowae_autorizacion_especial']['fields']['limite_credito_temporal_c']['dependency']='equal($motivo_solicitud_c,"limite_de_credito_temporal")';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_estado_promesa_c.php

 // created: 2016-11-23 04:16:37
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_c']['default']='';
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_c']['required']=false;
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_c']['dependency']='equal($motivo_solicitud_c,"promesa_de_pago")';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_monto_abonado_c.php

 // created: 2016-11-23 04:10:29
$dictionary['lowae_autorizacion_especial']['fields']['monto_abonado_c']['default']=0;
$dictionary['lowae_autorizacion_especial']['fields']['monto_abonado_c']['dependency']='equal($motivo_solicitud_c,"promesa_de_pago")';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_comentarios_c.php

 // created: 2016-12-19 17:50:45
$dictionary['lowae_autorizacion_especial']['fields']['comentarios_c']['labelValue']='Comentarios';
$dictionary['lowae_autorizacion_especial']['fields']['comentarios_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowae_autorizacion_especial']['fields']['comentarios_c']['enforced']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_vigencia_c.php

 // created: 2016-11-26 00:45:07
$dictionary['lowae_autorizacion_especial']['fields']['vigencia_c']['labelValue']='vigencia c';
$dictionary['lowae_autorizacion_especial']['fields']['vigencia_c']['dependency']='';
$dictionary['lowae_autorizacion_especial']['fields']['vigencia_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/full_text_search_admin.php

 // created: 2017-02-24 18:28:04
$dictionary['lowae_autorizacion_especial']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/lowae_autorizacion_especial_lowes_pagos_2_lowae_autorizacion_especial.php

// created: 2017-11-06 23:18:19
$dictionary["lowae_autorizacion_especial"]["fields"]["lowae_autorizacion_especial_lowes_pagos_2"] = array (
  'name' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'type' => 'link',
  'relationship' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'source' => 'non-db',
  'module' => 'lowes_Pagos',
  'bean_name' => 'lowes_Pagos',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'lowae_autorizacion_especial_lowes_pagos_2lowes_pagos_idb',
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/lowae_autorizacion_especial_orden_ordenes_1_lowae_autorizacion_especial.php

// created: 2017-02-23 18:24:39
$dictionary["lowae_autorizacion_especial"]["fields"]["lowae_autorizacion_especial_orden_ordenes_1"] = array (
  'name' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'id_name' => 'lowae_auto0644special_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/lowae_autorizacion_especial_lowes_pagos_1_lowae_autorizacion_especial.php

// created: 2017-03-29 15:36:09
// $dictionary["lowae_autorizacion_especial"]["fields"]["lowae_autorizacion_especial_lowes_pagos_1"] = array (
//   'name' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'type' => 'link',
//   'relationship' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'source' => 'non-db',
//   'module' => 'lowes_Pagos',
//   'bean_name' => 'lowes_Pagos',
//   'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
//   'id_name' => 'lowae_auto3868special_ida',
//   'link-type' => 'many',
//   'side' => 'left',
// );

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_dias_vencimiento_promesa_c.php

 // created: 2017-03-18 05:01:24
$dictionary['lowae_autorizacion_especial']['fields']['dias_vencimiento_promesa_c']['duplicate_merge_dom_value']=0;
$dictionary['lowae_autorizacion_especial']['fields']['dias_vencimiento_promesa_c']['dependency']='equal($motivo_solicitud_c,"promesa_de_pago")';
$dictionary['lowae_autorizacion_especial']['fields']['dias_vencimiento_promesa_c']['default']=0;

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_monto_autorizado_promesa_c.php

 // created: 2017-04-28 17:49:54
$dictionary['lowae_autorizacion_especial']['fields']['monto_autorizado_promesa_c']['labelValue']='Monto Autorizado de Promesa de Pago';
$dictionary['lowae_autorizacion_especial']['fields']['monto_autorizado_promesa_c']['formula']='ifElse(and(equal($estado_autorizacion_c,"pendiente"),equal($motivo_solicitud_c,"promesa_de_pago")),subtract($monto_venta_c,related($accounts_lowae_autorizacion_especial_1,"credito_disponible_flexible_c")),$monto_autorizado_promesa_c)';
$dictionary['lowae_autorizacion_especial']['fields']['monto_autorizado_promesa_c']['enforced']='false';
$dictionary['lowae_autorizacion_especial']['fields']['monto_autorizado_promesa_c']['dependency']='equal($motivo_solicitud_c,"promesa_de_pago")';
$dictionary['lowae_autorizacion_especial']['fields']['monto_autorizado_promesa_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_fecha_inicio_c.php

 // created: 2016-12-28 00:15:53
$dictionary['lowae_autorizacion_especial']['fields']['fecha_inicio_c']['required']=true;


?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_numero_veces_aplicacion_c.php

 // created: 2017-11-02 17:23:51
$dictionary['lowae_autorizacion_especial']['fields']['numero_veces_aplicacion_c']['labelValue']='Número de Veces de Aplicación';
$dictionary['lowae_autorizacion_especial']['fields']['numero_veces_aplicacion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowae_autorizacion_especial']['fields']['numero_veces_aplicacion_c']['enforced']='';
$dictionary['lowae_autorizacion_especial']['fields']['numero_veces_aplicacion_c']['dependency']='equal($motivo_solicitud_c,"credito_disponible_temporal")';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_aplica_por_unica_ocacion_c.php

 // created: 2017-11-02 17:22:56
$dictionary['lowae_autorizacion_especial']['fields']['aplica_por_unica_ocacion_c']['labelValue']='Aplica por Única Ocasión';
$dictionary['lowae_autorizacion_especial']['fields']['aplica_por_unica_ocacion_c']['enforced']='';
$dictionary['lowae_autorizacion_especial']['fields']['aplica_por_unica_ocacion_c']['dependency']='equal($motivo_solicitud_c,"credito_disponible_temporal")';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_monto_venta_c.php

 // created: 2017-11-17 00:10:37
$dictionary['lowae_autorizacion_especial']['fields']['monto_venta_c']['default']='';
$dictionary['lowae_autorizacion_especial']['fields']['monto_venta_c']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_autorizacion_c.php

 // created: 2016-11-23 04:20:43
$dictionary['lowae_autorizacion_especial']['fields']['autorizacion_c']['dependency']='or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"promesa_de_pago"),equal($motivo_solicitud_c,"otro"))';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_credito_diponible_temporal_c.php

 // created: 2017-11-02 17:18:41
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['dependency']='equal($motivo_solicitud_c,"credito_disponible_temporal")';
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['importable']='false';
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['duplicate_merge']='disabled';
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['duplicate_merge_dom_value']=0;
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['calculated']='1';
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['formula']='ifElse(and(equal($estado_autorizacion_c,"pendiente"),equal($motivo_solicitud_c,"credito_disponible_temporal")),subtract($monto_venta_c,related($accounts_lowae_autorizacion_especial_1,"credito_disponible_flexible_c")),$credito_diponible_temporal_c)';
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_fecha_cumplimiento_c.php

 // created: 2016-11-23 03:53:22

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_monto_promesa_c.php

 // created: 2017-11-17 00:11:16
$dictionary['lowae_autorizacion_especial']['fields']['monto_promesa_c']['default']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_estado_promesa_segun_monto_c.php

 // created: 2017-03-29 00:03:52
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_segun_monto_c']['labelValue']='Estado de la Promesa según el Monto';
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_segun_monto_c']['dependency']='equal($motivo_solicitud_c,"promesa_de_pago")';
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_segun_monto_c']['visibility_grid']='';
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_segun_monto_c']['default']='sin_pago';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_estado_autorizacion_c.php

 // created: 2017-01-05 17:22:44
$dictionary['lowae_autorizacion_especial']['fields']['estado_autorizacion_c']['labelValue']='Estado de Autorización';
$dictionary['lowae_autorizacion_especial']['fields']['estado_autorizacion_c']['default']='pendiente';
 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_porcentaje_cumplimiento_promesa_c.php

 // created: 2017-04-03 18:17:39
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['duplicate_merge_dom_value']=0;
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['labelValue']='% de Cumplimiento';
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['calculated']='1';
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['formula']='ifElse(greaterThan($monto_promesa_c, 0),round(divide($monto_abonado_c,$monto_promesa_c),2),0)';
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['enforced']='1';
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['dependency']='equal($motivo_solicitud_c,"promesa_de_pago")';
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['len']=5;
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['precision']=2;

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_saldo_pendiente_aplicar_c.php

 // created: 2017-11-02 19:17:54
$dictionary['lowae_autorizacion_especial']['fields']['saldo_pendiente_aplicar_c']['labelValue']='Monto Pendiente por Aplicar';
$dictionary['lowae_autorizacion_especial']['fields']['saldo_pendiente_aplicar_c']['formula']='ifElse(and(equal($motivo_solicitud_c,"otro"),equal($estado_autorizacion_c,"pendiente")),$monto_venta_c,$saldo_pendiente_aplicar_c)';
$dictionary['lowae_autorizacion_especial']['fields']['saldo_pendiente_aplicar_c']['enforced']='false';
$dictionary['lowae_autorizacion_especial']['fields']['saldo_pendiente_aplicar_c']['dependency']='or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"promesa_de_pago"),equal($motivo_solicitud_c,"otro"))';
$dictionary['lowae_autorizacion_especial']['fields']['saldo_pendiente_aplicar_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_sucursal_cliente_c.php

 // created: 2017-07-06 12:22:24
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['duplicate_merge_dom_value']=0;
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['labelValue']='Sucursal de Cliente';
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['calculated']='true';
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['formula']='related($accounts_lowae_autorizacion_especial_1,"id_sucursal_c")';
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['enforced']='true';
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_estado_c.php

 // created: 2017-01-12 00:09:08
$dictionary['lowae_autorizacion_especial']['fields']['estado_c']['default']='';
$dictionary['lowae_autorizacion_especial']['fields']['estado_c']['calculated']='true';
$dictionary['lowae_autorizacion_especial']['fields']['estado_c']['enforced']='true';
$dictionary['lowae_autorizacion_especial']['fields']['estado_c']['dependency']='and(or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"otro")), equal($aplica_por_unica_ocacion_c,1))';

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_fecha_autorizacion_c.php

 // created: 2017-01-30 16:19:55
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['duplicate_merge_dom_value']=0;
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['labelValue']='Fecha de Autorización';
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['calculated']='1';
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['formula']='ifElse(equal($estado_autorizacion_c,"autorizada"),now(),$fecha_autorizacion_c)';
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['enforced']='1';
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['dependency']='or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"promesa_de_pago"),equal($motivo_solicitud_c,"otro"))';

 
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Vardefs/sugarfield_fecha_fin_c.php

 // created: 2016-12-28 00:16:15
$dictionary['lowae_autorizacion_especial']['fields']['fecha_fin_c']['required']=true;


?>
