<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Language/es_LA.customaccounts_lowae_autorizacion_especial_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Cuenta Cliente Credito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MODULE_NAME'] = 'Autorizaciones Especiales';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Autorización Especial';
$mod_strings['LBL_COMENTARIOS_C'] = 'Comentarios';
$mod_strings['LBL_ESTADO_C'] = 'Estado de Aplicación';
$mod_strings['LBL_ESTADO_PROMESA_C'] = 'Estado de la Promesa según el Tiempo';
$mod_strings['LBL_MOTIVO_SOLICITUD_C'] = 'Motivo de Solicitud';
$mod_strings['LBL_APLICA_POR_UNICA_OCACION'] = 'Aplica por Única Ocasión';
$mod_strings['LBL_NUMERO_VECES_APLICACION'] = 'Número de Veces de Aplicación';
$mod_strings['LBL_LIMITE_CREDITO_TEMPORAL_C'] = 'Límite de Crédito Temporal';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_FECHA_INICIO_C'] = 'Fecha de Inicio';
$mod_strings['LBL_FECHA_FIN_C'] = 'Fecha de Fin';
$mod_strings['LBL_AUTORIZACION_C'] = 'Autorización';
$mod_strings['LBL_CREDITO_DIPONIBLE_TEMPORAL_C'] = 'Monto Autorizado de Crédito Disponible Temporal';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_FECHA_CUMPLIMIENTO_C'] = 'Fecha de Cumplimiento de la Promesa de Pago';
$mod_strings['LBL_MONTO_PROMESA_C'] = 'Monto de Promesa de Pago';
$mod_strings['LBL_MONTO_ABONADO_C'] = 'Monto Abonado a Promesa de Pago';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_VIGENCIA_C'] = 'vigencia c';
$mod_strings['LBL_VIGENCIA'] = 'Vigencia';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Detalle de Autorización Especial';
$mod_strings['LBL_SHOW_MORE'] = 'Información de Sistema';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Información de General';
$mod_strings['LBL_SALDO_PENDIENTE_APLICAR'] = 'Monto Pendiente por Aplicar';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_SALDO_PENDIENTE_APLICAR_C'] = 'Monto Pendiente por Aplicar';
$mod_strings['LBL_CURRENCY_6'] = 'LBL_CURRENCY';
$mod_strings['LBL_ESTADO_AUTORIZACION'] = 'Estado de Autorización';
$mod_strings['LBL_FECHA_AUTORIZACION'] = 'Fecha de Autorización';
$mod_strings['LBL_MONTO_VENTA_C'] = 'Monto Venta';
$mod_strings['LBL_MONTO_AUTORIZADO_PROMESA'] = 'Monto Autorizado de Promesa de Pago';
$mod_strings['LBL_ESTADO_PROMESA_SEGUN_MONTO'] = 'Estado de la Promesa según el Monto';
$mod_strings['LBL_DIAS_VENCIMIENTO_PROMESA'] = 'Días de Vencimiento de Promesa';
$mod_strings['LBL_PORCENTAJE_CUMPLIMIENTO_PROMESA'] = '% de Cumplimiento';
$mod_strings['LBL_CURRENCY_7'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_8'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_9'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_10'] = 'LBL_CURRENCY';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Language/es_LA.customlowae_autorizacion_especial_lowes_pagos_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Language/es_LA.customlowae_autorizacion_especial_lowes_pagos_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Pagos';


?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Language/es_LA.customlowae_autorizacion_especial_lowes_pagos_2.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Language/es_LA.customlowae_autorizacion_especial_lowes_pagos_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Pagos';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Pagos';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Pagos';


?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Language/es_LA.customlowae_autorizacion_especial_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';

?>
