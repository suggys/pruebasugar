<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/aplica_por_unica_ocacion_c_readonly.php

$dependencies['lowae_autorizacion_especial']['aplica_por_unica_ocacion_c_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'or(equal($estado_autorizacion_c,"rechazada"),equal($estado_autorizacion_c,"finalizada"),equal($estado_autorizacion_c,"terminada_incumplida"),equal($estado_autorizacion_c,"cancelada_devolucion_cancelacion"),not(equal($numero_veces_aplicacion_c,0)))',
    'triggerFields' => array('estado_autorizacion_c','numero_veces_aplicacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'aplica_por_unica_ocacion_c',
              'value' => 'true',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/accounts_name_readonly.php

$dependencies['lowae_autorizacion_especial']['accounts_name_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'or(equal($estado_autorizacion_c,"autorizada"),equal($estado_autorizacion_c,"finalizada"),equal($estado_autorizacion_c,"rechazada"),equal($estado_autorizacion_c,"terminada_incumplida"))',
    'triggerFields' => array('estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'accounts_lowae_autorizacion_especial_1_name',
              'value' => 'true',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/comentarios_c_readonly.php

$dependencies['lowae_autorizacion_especial']['comentarios_c_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'not(equal($estado_autorizacion_c,"pendiente"))',
    'triggerFields' => array('estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'comentarios_c',
              'value' => 'true',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/estado_autorizacion_c_visibility.php

$dependencies['lowae_autorizacion_especial']['estado_autorizacion_c_visibility'] = array(
  'hooks' => array("edit","view"),
  'triggerFields' => array('motivo_solicitud_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'estado_autorizacion_c',
        'value' => 'or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"promesa_de_pago"),equal($motivo_solicitud_c,"otro"))',
      ),
    )
  ),
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/fechas_c_deps.php


$dependencies['lowae_autorizacion_especial']['fechas_c_deps'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'not(equal($estado_autorizacion_c,"pendiente"))',
    'triggerFields' => array('estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'fecha_inicio_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'fecha_fin_c',
                'value' => 'true',
            ),
        ),
    ),
    'notActions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'fecha_inicio_c',
              'value' => 'false',
          ),
      ),
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'fecha_fin_c',
              'value' => 'false',
          ),
      ),
    )
);


?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/estado_c_deps.php

// $dependencies['lowae_autorizacion_especial']['estado_c'] = array(
//     'hooks' => array("edit"),
//     //Trigger formula for the dependency. Defaults to 'true'.
//     'trigger' => 'true',
//     'triggerFields' => array('aplica_por_unica_ocacion_c'),
//     'onload' => true,
//     //Actions is a list of actions to fire when the trigger is true
//     'actions' => array(
//
//       array(
//         'name' => 'SetVisibility', //Action type
//         //The parameters passed in depend on the action type
//         'params' => array(
//             'target' => 'estado_c',
//             'label'  => 'estado_c_label', //normally <field>_label
//             'value' => 'equal($aplica_por_unica_ocacion_c, 1)', //Formula
//         ),
//       ),
//     ),
// );

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/estado_autorizacion_c_deps.php

$dependencies['lowae_autorizacion_especial']['estado_autorizacion_c_deps'] = array(
    'hooks' => array("edit"),
    'trigger' => 'or(equal($estado_autorizacion_c,"rechazada"),equal($estado_autorizacion_c,"finalizada"),equal($estado_autorizacion_c,"terminada_incumplida"),equal($estado_autorizacion_c,"cancelada_devolucion_cancelacion"))',
    'triggerFields' => array('estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'estado_autorizacion_c',
              'value' => 'true',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/setoptions_estado_autorizacion2_c.php


$dependencies['lowae_autorizacion_especial']['setoptions_estado_autorizacion2_c'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(equal($estado_autorizacion_c,"autorizada"),equal($motivo_solicitud_c,"promesa_de_pago"))',
   'triggerFields' => array('motivo_solicitud_c','estado_autorizacion_c'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'estado_autorizacion_c',
         'keys' => 'createList("autorizada","terminada_incumplida")',
         'labels' => 'createList("Autorizada","Terminada Incumplida")'
       ),
     ),
   ),
);


?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/setoptions_estado_autorizacion_c.php


$dependencies['lowae_autorizacion_especial']['setoptions_estado_autorizacion_c'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'or(and(equal($estado_autorizacion_c,"autorizada"),equal($motivo_solicitud_c,"otro")),and(equal($estado_autorizacion_c,"autorizada"),equal($motivo_solicitud_c,"credito_disponible_temporal")))',
   'triggerFields' => array('motivo_solicitud_c','estado_autorizacion_c'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'estado_autorizacion_c',
         'keys' => 'createList("autorizada","finalizada")',
         'labels' => 'createList("Autorizada","Finalizada")'
       ),
     ),
   ),
);


?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/required_fields_promesa_pago_dep.php

$dependencies['lowae_autorizacion_especial']['required_fields_promesa_pago_dep'] = array(
  'hooks' => array("edit"),
  'trigger' => 'true',
  'triggerFields' => array('motivo_solicitud_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'monto_promesa_c',
        'label' => 'monto_promesa_c_label',
        'value' => 'equal($motivo_solicitud_c,"promesa_de_pago")',
      ),
    ),
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'fecha_cumplimiento_c',
        'label' => 'fecha_cumplimiento_c_label',
        'value' => 'equal($motivo_solicitud_c,"promesa_de_pago")',
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/monto_venta_c_deps.php


$dependencies['lowae_autorizacion_especial']['monto_venta_c_deps'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'isInList($estado_autorizacion_c,createList("rechazada","finalizada","terminada_incumplida","cancelada_devolucion_cancelacion"))',
  'triggerFields' => array('estado_autorizacion_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'monto_venta_c',
        'value' => 'true',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'monto_venta_c',
        'value' => 'false',
      ),
    ),
  )
);


?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/estado_c_readonly.php

$dependencies['lowae_autorizacion_especial']['estado_c_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'equal($aplica_por_unica_ocacion_c,true)',
    'triggerFields' => array('aplica_por_unica_ocacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'estado_c',
              'value' => 'true',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/monto_promesa_c_readonly.php

$dependencies['lowae_autorizacion_especial']['monto_promesa_c_readonly'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'and(equal($motivo_solicitud_c,"promesa_de_pago"),not(equal($estado_autorizacion_c,"pendiente")))',
    'triggerFields' => array('motivo_solicitud_c','estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'monto_promesa_c',
                'value' => 'true',
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/fechas_c_visibility.php


$dependencies['lowae_autorizacion_especial']['fechas_c_visibility'] = array(
  'hooks' => array("edit","view"),
  'triggerFields' => array('motivo_solicitud_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'fecha_inicio_c',
        'value' => 'or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"promesa_de_pago"),equal($motivo_solicitud_c,"otro"))',
      ),
    ),
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'fecha_fin_c',
        'value' => 'or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"otro"))',
      ),
    ),
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'fecha_cumplimiento_c',
        'value' => 'equal($motivo_solicitud_c,"promesa_de_pago")',
      ),
    ),
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'monto_promesa_c',
        'value' => 'equal($motivo_solicitud_c,"promesa_de_pago")',
      ),
    ),
  ),
);


?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/fecha_cumplimiento_c_readonly.php

$dependencies['lowae_autorizacion_especial']['fecha_cumplimiento_c_readonly'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'and(equal($motivo_solicitud_c,"promesa_de_pago"),not(equal($estado_autorizacion_c,"pendiente")))',
    'triggerFields' => array('motivo_solicitud_c','estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'fecha_cumplimiento_c',
                'value' => 'true',
            ),
        ),
    ),
);


?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Dependencies/motivo_solicitud_c_readonly.php

$dependencies['lowae_autorizacion_especial']['motivo_solicitud_c_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'not(equal($estado_autorizacion_c,"pendiente"))',
    'triggerFields' => array('estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'motivo_solicitud_c',
              'value' => 'true',
          ),
      ),
    ),
);

?>
