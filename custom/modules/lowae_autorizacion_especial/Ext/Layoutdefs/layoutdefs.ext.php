<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Layoutdefs/lowae_autorizacion_especial_lowes_pagos_2_lowae_autorizacion_especial.php

 // created: 2017-11-06 23:18:19
$layout_defs["lowae_autorizacion_especial"]["subpanel_setup"]['lowae_autorizacion_especial_lowes_pagos_2'] = array (
  'order' => 100,
  'module' => 'lowes_Pagos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWES_PAGOS_TITLE',
  'get_subpanel_data' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Layoutdefs/lowae_autorizacion_especial_orden_ordenes_1_lowae_autorizacion_especial.php

 // created: 2017-02-23 18:24:39
$layout_defs["lowae_autorizacion_especial"]["subpanel_setup"]['lowae_autorizacion_especial_orden_ordenes_1'] = array (
  'order' => 100,
  'module' => 'orden_Ordenes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE',
  'get_subpanel_data' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Layoutdefs/lowae_autorizacion_especial_lowes_pagos_1_lowae_autorizacion_especial.php

 // created: 2017-03-29 15:36:09
// $layout_defs["lowae_autorizacion_especial"]["subpanel_setup"]['lowae_autorizacion_especial_lowes_pagos_1'] = array (
//   'order' => 100,
//   'module' => 'lowes_Pagos',
//   'subpanel_name' => 'default',
//   'sort_order' => 'asc',
//   'sort_by' => 'id',
//   'title_key' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE',
//   'get_subpanel_data' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'top_buttons' =>
//   array (
//     0 =>
//     array (
//       'widget_class' => 'SubPanelTopButtonQuickCreate',
//     ),
//     1 =>
//     array (
//       'widget_class' => 'SubPanelTopSelectButton',
//       'mode' => 'MultiSelect',
//     ),
//   ),
// );

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Layoutdefs/_overridelowae_autorizacion_especial_subpanel_lowae_autorizacion_especial_lowes_pagos_1.php

//auto-generated file DO NOT EDIT
// $layout_defs['lowae_autorizacion_especial']['subpanel_setup']['lowae_autorizacion_especial_lowes_pagos_1']['override_subpanel_name'] = 'lowae_autorizacion_especial_subpanel_lowae_autorizacion_especial_lowes_pagos_1';

?>
<?php
// Merged from custom/Extension/modules/lowae_autorizacion_especial/Ext/Layoutdefs/_overridelowae_autorizacion_especial_subpanel_lowae_autorizacion_especial_lowes_pagos_2.php

//auto-generated file DO NOT EDIT
$layout_defs['lowae_autorizacion_especial']['subpanel_setup']['lowae_autorizacion_especial_lowes_pagos_2']['override_subpanel_name'] = 'lowae_autorizacion_especial_subpanel_lowae_autorizacion_especial_lowes_pagos_2';

?>
