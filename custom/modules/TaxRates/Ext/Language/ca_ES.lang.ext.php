<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/TaxRates/Ext/Language/ca_ES.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_TAXRATE'] = 'Llista de tipus d&#039;impost';
$mod_strings['LBL_MODULE_NAME'] = 'Tipus d&#039;impostos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Tipus d&#039;impost';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nou tipus d&#039;impost';
$mod_strings['LNK_IMPORT_TAXRATES'] = 'Importar Tipus d&#039;Impostos';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Llista de tipus d&#039;impost';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cerca de tipus d&#039;impostos';

?>
