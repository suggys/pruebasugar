<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Nuevo Panel 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Calle';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Municipio';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Otro estado';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Estado';
$mod_strings['LBL_BILLING_ADDRESS_NUMERO'] = 'Número';
$mod_strings['LBL_SHOW_MORE'] = 'Información de Sistema';
$mod_strings['LBL_LO_OBRAS_OPPORTUNITIES_FROM_OPPORTUNITIES_TITLE'] = 'Proyectos';

?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Language/es_LA.Lowes_Modules_CRM.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LO_OBRAS_OPPORTUNITIES_FROM_LO_OBRAS_TITLE'] = 'Obras';
$mod_strings['LBL_LO_OBRAS_OPPORTUNITIES_FROM_OPPORTUNITIES_TITLE'] = 'Oportunidades';

?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Language/es_LA.Obras.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LO_OBRAS_OPPORTUNITIES_FROM_LO_OBRAS_TITLE'] = 'Obras';
$mod_strings['LBL_LO_OBRAS_OPPORTUNITIES_FROM_OPPORTUNITIES_TITLE'] = 'Proyectos';

?>
