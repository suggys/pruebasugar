<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Layoutdefs/lo_obras_opportunities_LO_Obras.php

 // created: 2017-07-20 15:21:47
$layout_defs["LO_Obras"]["subpanel_setup"]['lo_obras_opportunities'] = array (
  'order' => 100,
  'module' => 'Opportunities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_OPPORTUNITIES_TITLE',
  'get_subpanel_data' => 'lo_obras_opportunities',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Layoutdefs/_overrideLO_Obras_subpanel_lo_obras_opportunities.php

//auto-generated file DO NOT EDIT
$layout_defs['LO_Obras']['subpanel_setup']['lo_obras_opportunities']['override_subpanel_name'] = 'LO_Obras_subpanel_lo_obras_opportunities';

?>
