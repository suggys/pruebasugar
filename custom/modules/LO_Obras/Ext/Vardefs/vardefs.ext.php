<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/sugarfield_shipping_address_street.php

 // created: 2017-07-21 21:56:27
$dictionary['LO_Obras']['fields']['shipping_address_street']['audited']=false;
$dictionary['LO_Obras']['fields']['shipping_address_street']['massupdate']=false;
$dictionary['LO_Obras']['fields']['shipping_address_street']['comments']='The street address used for for shipping purposes';
$dictionary['LO_Obras']['fields']['shipping_address_street']['duplicate_merge']='enabled';
$dictionary['LO_Obras']['fields']['shipping_address_street']['duplicate_merge_dom_value']='1';
$dictionary['LO_Obras']['fields']['shipping_address_street']['merge_filter']='disabled';
$dictionary['LO_Obras']['fields']['shipping_address_street']['unified_search']=false;
$dictionary['LO_Obras']['fields']['shipping_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.25',
  'searchable' => true,
);
$dictionary['LO_Obras']['fields']['shipping_address_street']['calculated']=false;
$dictionary['LO_Obras']['fields']['shipping_address_street']['rows']='4';
$dictionary['LO_Obras']['fields']['shipping_address_street']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/sugarfield_billing_address_numero.php

 // created: 2017-08-02 22:48:51
$dictionary['LO_Obras']['fields']['billing_address_numero']['help']='';

 
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/newFilterDuplicateCheck.php


$dictionary['LO_Obras']['duplicate_check']['FilterDuplicateCheck'] = array(
    'filter_template' => array(
        array(
            '$or' => array(
                array('name' => array('$equals' => '$name')),
                //array('duns_num' => array('$equals' => '$duns_num')),
                // array(
                //     '$and' => array(
                //         array('name' => array('$starts' => '$name')),
                //         array(
                //             '$or' => array(
                //                 array('billing_address_city' => array('$starts' => '$billing_address_city')),
                //             )
                //         ),
                //     )
                // ),
            )
        ),
    ),
    'ranking_fields' => array(
        array('in_field_name' => 'name', 'dupe_field_name' => 'name'),
        //array('in_field_name' => 'billing_address_city', 'dupe_field_name' => 'billing_address_city'),
    )
);

?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/sugarfield_email.php

 // created: 2017-07-16 22:51:12
$dictionary['LO_Obras']['fields']['email']['len']='100';
$dictionary['LO_Obras']['fields']['email']['required']=true;
$dictionary['LO_Obras']['fields']['email']['audited']=false;
$dictionary['LO_Obras']['fields']['email']['massupdate']=true;
$dictionary['LO_Obras']['fields']['email']['duplicate_merge']='enabled';
$dictionary['LO_Obras']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['LO_Obras']['fields']['email']['merge_filter']='disabled';
$dictionary['LO_Obras']['fields']['email']['unified_search']=false;
$dictionary['LO_Obras']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.5',
  'searchable' => true,
);
$dictionary['LO_Obras']['fields']['email']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/sugarfield_shipping_address_state.php

 // created: 2017-09-06 00:22:49
$dictionary['LO_Obras']['fields']['shipping_address_state']['type']='enum';
$dictionary['LO_Obras']['fields']['shipping_address_state']['options']='state_c_list';
$dictionary['LO_Obras']['fields']['shipping_address_state']['len']=100;
$dictionary['LO_Obras']['fields']['shipping_address_state']['dependency']=false;
$dictionary['LO_Obras']['fields']['shipping_address_state']['comments']='';
$dictionary['LO_Obras']['fields']['shipping_address_state']['full_text_search']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/sugarfield_shipping_address_city.php

 // created: 2017-07-21 22:03:06
$dictionary['LO_Obras']['fields']['shipping_address_city']['len']='100';
$dictionary['LO_Obras']['fields']['shipping_address_city']['audited']=false;
$dictionary['LO_Obras']['fields']['shipping_address_city']['massupdate']=false;
$dictionary['LO_Obras']['fields']['shipping_address_city']['comments']='The city used for the shipping address';
$dictionary['LO_Obras']['fields']['shipping_address_city']['duplicate_merge']='enabled';
$dictionary['LO_Obras']['fields']['shipping_address_city']['duplicate_merge_dom_value']='1';
$dictionary['LO_Obras']['fields']['shipping_address_city']['merge_filter']='disabled';
$dictionary['LO_Obras']['fields']['shipping_address_city']['unified_search']=false;
$dictionary['LO_Obras']['fields']['shipping_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LO_Obras']['fields']['shipping_address_city']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/sugarfield_shipping_address_country.php

$dictionary['LO_Obras']['fields']['shipping_address_country']['type']='enum';
$dictionary['LO_Obras']['fields']['shipping_address_country']['options']='prospect_pais_list';

 
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/sugarfield_shipping_address_postalcode.php

 // created: 2017-09-06 00:30:33
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['len']='5';
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['audited']=false;
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['massupdate']=false;
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['comments']='The zip code used for the shipping address';
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['duplicate_merge']='enabled';
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['merge_filter']='disabled';
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['unified_search']=false;
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/sugarfield_billing_address_country.php

$dictionary['LO_Obras']['fields']['billing_address_country']['type']='enum';
$dictionary['LO_Obras']['fields']['billing_address_country']['options']='prospect_pais_list';

?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/sugarfield_billing_address_state.php

 // created: 2017-07-21 22:31:36
$dictionary['LO_Obras']['fields']['billing_address_state']['dependency']='equal($shipping_address_state,"OTRO")';

 
?>
<?php
// Merged from custom/Extension/modules/LO_Obras/Ext/Vardefs/lo_obras_opportunities_LO_Obras.php

// created: 2017-07-20 15:21:47
$dictionary["LO_Obras"]["fields"]["lo_obras_opportunities"] = array (
  'name' => 'lo_obras_opportunities',
  'type' => 'link',
  'relationship' => 'lo_obras_opportunities',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_LO_OBRAS_TITLE',
  'id_name' => 'lo_obras_opportunitieslo_obras_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
