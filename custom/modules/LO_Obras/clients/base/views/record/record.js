({
  extendsFrom: 'RecordView',

  my_events: {
    'keyup input[name=shipping_address_postalcode]': '_keypress_shipping_address_postalcode',
    'keypress input[name=shipping_address_postalcode]': '_keypress_event_shipping_address_postalcode',
  },

  initialize: function (options) {
    var self = this;
    this._super("initialize", arguments);
    this.model.addValidationTask('billing_address_state', _.bind(this._valida_billing_address_state, this));
    //this.model.addValidationTask('beforeSave', _.bind(this._doBeforeSave, this));
    this.model.on('change:shipping_address_country', _.bind(this._CambiaShippingAddressCountry, this));
  },

  _keypress_event_shipping_address_postalcode: function (event) {
    var controlKeys = [8, 9, 13, 35, 36, 37, 39];
    // IE doesn't support indexOf
    var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
    console.log(event.which);
    // Some browsers just don't raise events for control keys. Easy.
    // e.g. Safari backspace.
    if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
      (48 <= event.which && event.which <= 57) || // Always 1 through 9
      /*(48 == event.which && $(this).attr("value")) ||*/ // No 0 first digit
      isControlKey) { // Opera assigns values for control keys.
        return;
      } else {
        event.preventDefault();
      }
    },

    _keypress_shipping_address_postalcode: function(evt) {
      var self = this;
      var postalCode = self.$el.find('input[name=shipping_address_postalcode]').val() || '';
      var values = [];
      console.log(postalCode);
      if(String(postalCode).length == 5)
      {
        app.alert.show('datos_cp', {
          level: 'process',
          title: 'Cargando datos de direcci&oacute;n.',
          autoClose: false
        });
        var url = app.api.buildURL('Merxbp/Zipcode', 'read',{},{zipcode:postalCode});
        app.api.call('read', url, {}, {
          success: function(data){
            self._setModeAndDisablefieldsAddress('edit', false);
            self.model.set({'shipping_address_country': 'MX'});
            if(data.Code == 200) {
              self.model.set({'shipping_address_country': 'MX'});
              self.model.set({'shipping_address_city': data.Address.Ciudad});
              self.model.set({'shipping_address_state': data.Address.Abr_Estado});

              _.each(data.Address.Colonias, function(col){values.push({id: col.Colonia, text: col.Colonia});});
              values.push({id : 'Otra', text: 'Otra'});
              self.$el.find('input[name=billing_address_colonia]').select2({
                width:"100%",
                containerCssClass: 'select2-choices-pills-close',
                data: values
              });

              if(values.length == 2)
              {
                self.model.set({'billing_address_colonia': values[0].text});
                self.$el.find('input[name=billing_address_colonia]').val(values[0].text).trigger('change');
              }
            }
            app.alert.dismiss('datos_cp');
          }
        });
      } else {
        self._setModeAndDisablefieldsAddress('readonly', true);
        self._clearFieldsAddress();
      }
    },

    _clearFieldsAddress: function(){
      var self = this;
      self.model.set(
        {
          'billing_address_colonia': '',
          'shipping_address_city': '',
          'shipping_address_state': '',
          'billing_address_state': '',
          'shipping_address_country':'',
        }
      );
    },

    _setModeAndDisablefieldsAddress: function(mode, disable){
      var self = this;
      self.getField('billing_address_colonia').setMode(mode);
      self.getField('billing_address_colonia').setDisabled(disable);

      self.getField('shipping_address_city').setMode(mode);
      self.getField('shipping_address_city').setDisabled(disable);

      self.getField('shipping_address_country').setMode(mode);
      self.getField('shipping_address_country').setDisabled(disable);

      self.getField('shipping_address_state').setMode(mode);
      self.getField('shipping_address_state').setDisabled(disable);

      self.getField('billing_address_state').setMode(mode);
      self.getField('billing_address_state').setDisabled(disable);
    },

    _renderHtml: function (argument) {
      var self = this;
      this._super('_renderHtml', arguments);
      this.delegateEvents(_.extend({}, this.events, this.my_events));
    },

    _CambiaShippingAddressCountry: function(){
      var self = this;
      if(self.model.get('shipping_address_country') != 'MX' && !_.isEmpty(self.model.get('shipping_address_country'))){
        self.model.set({'shipping_address_state': 'OTRO'});
      }else if (_.isEmpty(self.model.get('shipping_address_country')) || self.model.get('shipping_address_country') === 'MX') {
        self.model.set({'shipping_address_state': ''});
      }
    },

    _valida_billing_address_state: function(fields, errors, callback){
      var self = this;
      if(self.model.get('shipping_address_state') === 'OTRO' && _.isEmpty(self.model.get('billing_address_state')) ){
        errors['billing_address_state'] = errors['billing_address_state'] || {};
        errors['billing_address_state'].required = true;
      }
      callback(null, fields, errors);
    },

    _doBeforeSave: function (fields, errors, callback){
      var self = this;
      var name = self.model.get('name').trim().toLowerCase();
      name = name.replace("  "," ");
      var filter = [{name:name}];
      var url = app.api.buildURL('LO_Obras', 'read',{},{filter:filter})
      app.api.call('read', url, {}, {
        success: function(data){
          self._processFilterNameSuccess(data, function (isValid) {
            if(isValid){
              errors['name'] = errors['name'] || {};
              errors['name']['Ya existe la Obra que quieres crear, busca en el listado de Obras para más detalles.'] = true;
            }
            callback(null, fields, errors);
          });
        }
      });
    },

    _processFilterNameSuccess: function (data, callback) {
      var isValid = true;
      if(!data.records.length){
        isValid = false;
      }
      callback(isValid);
    },
  })
