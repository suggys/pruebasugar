<?php

require_once 'custom/modules/Accounts/Account_GeneraID_AR.php';

require_once 'custom/modules/MRX1_Configuraciones/CustomMRX1Configuraciones.php';

class ProspectsLogicHooksManager

{

  protected static $fetchedRow = array();


  public function generaFolio($bean, $event, $arguments){

    global $current_user;

    if(!empty($bean->id)){

      self::$fetchedRow[$bean->id] = $bean->fetched_row;

    }

    $configKey = 'prospects_folio';

    if(self::$fetchedRow[$bean->id]['id'] != $bean->id){

      $folio = 1;

      $configuratorObj = new CustomMRX1Configuraciones();

      $configuratorObj->retrieveSettings();

      if(isset($configuratorObj->settings[$configKey]) && $configuratorObj->settings[$configKey] > 0){

        $folio = $configuratorObj->settings[$configKey];

      }

      $bean->folio_c = $folio;

      $folio++;

      $configuratorObj->saveSetting($configKey, $folio);



      if(empty($bean->assigned_user_id)){

        $bean->assigned_user_id = $current_user->id;

      }

      $userAssigned = BeanFactory::getBean('Users',$bean->assigned_user_id, array('disable_row_level_security' => true));

      $bean->sucursal_c = $userAssigned->sucursal_c;

    }

  }



  public function AfterSave($bean, $event, $arguments)

  {

    if(self::$fetchedRow[$bean->id]['id'] != $bean->id)

    {

      $contacto = BeanFactory::getBean('Contacts',$bean->contact_id_c, array('disable_row_level_security' => true));

      $proyecto = BeanFactory::getBean('Opportunities',$bean->opportunity_id_c, array('disable_row_level_security' => true));

      $bean->load_relationship('prospects_contacts_1');

      $bean->prospects_contacts_1->add($contacto);

      $bean->load_relationship('prospects_opportunities_1');

      $bean->prospects_opportunities_1->add($proyecto);

    }

    if($bean->aprueba_oc_o_cot_c == 1

      && $bean->fetched_row['aprueba_oc_o_cot_c'] != 0

      && !isset($bean->account_created)

      && empty($bean->prospects_accounts_1accounts_idb)

    ){

      $this->saveCuenta($bean, true);

    }

    if(self::$fetchedRow[$bean->id]['estado_c'] !== $bean->estado_c

    && $bean->estado_c === 'convertido')

    {

      $this->saveCuenta($bean, false);

      $this->envia_email_bienvenida_cliente($bean);

    }

    /*if (!empty($bean->id_pos_c) && !empty($bean->prospects_orden_ordenes_1))  
      {
        //$GLOBALS['log']->fatal('Convirtiendo automaticamente a prospects');
        $guardada = false;
        $bean->estado_c = 'convertido';
        $guardada = $bean->save(false);
        $this->saveCuenta($bean, false);
      }
    */

  }

  public function ConvercionAuto($bean, $event, $arguments)
  {
    //$GLOBALS['log']->fatal('Estamos en la convercion automatica');
    if ($event=="after_relationship_add") {
       
      // creacion de if HSLR @LAIMU validacion para convercion automatica
      if (!empty($bean->id_pos_c) && !empty($bean->prospects_orden_ordenes_1)) 
      {
        //$GLOBALS['log']->fatal('Convirtiendo automaticamente a prospects');
        $guardada = false;
        $bean->estado_c = 'convertido';
        $guardada = $bean->save(false);
        $this->saveCuenta($bean, false);
      }
    }

  }



  private function saveCuenta($bean, $nueva){

    $guardada = false;

    $cuenta = null;

	

	

	$GLOBALS['log']->fatal('Creando la cuenta');

	

    if($nueva){

      $cuenta = BeanFactory::newBean('Accounts');

    }

    else{

      $cuenta = BeanFactory::getBean('Accounts', $bean->prospects_accounts_1accounts_idb);

    }

    $cuenta->name = $bean->last_name;

    $cuenta->assigned_user_name = $bean->assigned_user_name;

    $cuenta->assigned_user_id = $bean->assigned_user_id;

    $cuenta->id_sucursal_c = $bean->sucursal_c;

    $cuenta->estado_cuenta_c = ucwords($bean->estado_cuenta_c);



    $cuenta->account_type_c = $bean->tipo_de_persona_c;

    $cuenta->rfc_c = $bean->rfc_c;

    $cuenta->phone_office = $bean->phone_work;

    

    //Cambio para Xstore Laimu

    //$cuenta->phone_alternate = $bean->phone_mobile;

	$cuenta->phone_alternate = $bean->phone_mobile; 



    $cuenta->website = $bean->web_page_c;

    $cuenta->contact_id_c = $bean->nombre_representante_legal_id_c;

	

	//20180306 #LAIMU

	// BUG: Nuevo campo nombre de aval en modulo de clientes

    $cuenta->nombre_del_aval_c = $bean->nombre_del_aval_c;

    global $db;
    $contacto_obra = "";
    $sugarQuery = new SugarQuery();
    // saca el contacto de la obra para plasmarlo
    $sqlQuery = "SELECT  lo.contacto FROM prospects p
          INNER JOIN prospects_opportunities_1_c po ON po.prospects_opportunities_1prospects_ida = p.id
          INNER JOIN opportunities o ON o.id = po.prospects_opportunities_1opportunities_idb
          INNER JOIN lo_obras_opportunities_c oo ON oo.lo_obras_opportunitiesopportunities_idb = o.id
          INNER JOIN lo_obras lo ON lo.id = oo.lo_obras_opportunitieslo_obras_ida WHERE p.id = '$bean->id';";

    $res = $db->query($sqlQuery);
    $num_row = $res->num_rows;

    if($num_row) {
        while($row = $db->fetchByAssoc($res)){
            if($row['contacto'] != ""){
              $contacto_obra = $row['contacto'] ;
              break;
            }else{
              $contacto_obra = "";
            }
        }
    }

    $cuenta->ownership = $contacto_obra; 



    $cuenta->tipo_cliente2_c = $bean->tipo_cliente_c;

	

	

	// 20180222 #LAIMUAMC

	// BUG: Tipo de Cliente

	$cuenta->tipo_cliente_c = '1'; // Se inicializa el valor del campo para clientes 

	

	$GLOBALS['log']->fatal('Continua con los campos');

	

    $cuenta->giro_c = $bean->giro_c;

    $cuenta->otro_giro_c = $bean->otro_giro_c;

    $cuenta->oficio_c = $bean->oficio_c;

    $cuenta->otro_oficio_c = $bean->otro_oficio_c;

    $cuenta->metodo_pago_c = $bean->metodo_pago_c;

    $cuenta->subtipo_c = $bean->subtipo_c;

    $cuenta->club_pro_c = $bean->club_pro_c;

    $cuenta->numero_club_pro_c = $bean->numero_club_pro_c;

    $cuenta->vol_ventas_anual_est_c = $bean->vol_ventas_anual_est_c;

    $cuenta->competidores_directos_c = $bean->competidores_directos_c;

    $cuenta->cuenta_patron_c = $bean->cuenta_patron_c;



    $cuenta->primary_address_postalcode_c = $bean->primary_address_postalcode;

    $cuenta->primary_address_country_c = $bean->primary_address_country;

    $cuenta->primary_address_state_c = $bean->primary_address_state;

    $cuenta->primary_address_other_state_c = $bean->alt_address_state;

    $cuenta->primary_address_city_c = $bean->primary_address_city;

    $cuenta->primary_address_colonia_c = $bean->primary_address_colonia_c;

    $cuenta->primary_address_street_c = $bean->primary_address_street;

    $cuenta->primary_address_numero_c = $bean->primary_address_number_c;

    $cuenta->estado_interfaz_pos_c = "nuevo";

    $cuenta->estado_cuenta_c = "Activo";



	$GLOBALS['log']->fatal('Validando la solicitud aprobada');

	

    $solicitudAprobada = $this->getSolicitudAprobada($bean);

    if(!empty($solicitudAprobada->id)){

		

		$GLOBALS['log']->fatal('Se encontro la solicitud');

		

      $cuenta->limite_credito_autorizado_c = $solicitudAprobada->limite_otorgado;

      $idArCreditGenerator =  new Account_GeneraID_AR();

      $cuenta->id_ar_credit_c = $idArCreditGenerator->getNewIdArCredit($cuenta);

      if($solicitudAprobada->load_relationship('low01_solicitudescredito_documents_1')){

        foreach ($solicitudAprobada->low01_solicitudescredito_documents_1->getBeans() as $documento) {

          // if($documento->category_id === 'pagare'){

            $cuenta->document_id_c = $documento->id;

          //}

        }

      }

    }

	

	$GLOBALS['log']->fatal('Se guarda la cuenta');

	

    $guardada = $cuenta->save(false);

    $cuenta->retrieve();



	$GLOBALS['log']->fatal('Se obtiene la direccion de prospectos');

	

    $sea = new SugarEmailAddress;

    $addresses = $sea->getAddressesByGUID($bean->id, "Prospects");

    foreach ( $addresses as $address ) {

        $sea->addAddress($address['email_address'],$address['primary_address']);

    }

	

	

	$GLOBALS['log']->fatal('Se guarda la direccion dentro de clientes');

	

    $sea->save($cuenta->id, "Accounts");



	

	$GLOBALS['log']->fatal('Se asigna el ID POS y el AR Credit');	

	

    $bean->retrieve();

    $bean->id_pos_c = $cuenta->id_pos_c;

    $bean->id_ar_credit_c = $cuenta->id_ar_credit_c;

    $bean->account_created = 1;

    $bean->save(false);



	$GLOBALS['log']->fatal('SI es nueve');	

	

    if($nueva){

		

		$GLOBALS['log']->fatal('Se relaciona la cuenta con los prospectos');	

		

      // se relaciona el prospecto y el cliente

      if($cuenta->load_relationship('prospects_accounts_1')){

        $cuenta->prospects_accounts_1->add($bean->id);

        $cuenta->save(false);

      }

    }

    else{

		

		$GLOBALS['log']->fatal('Se heredan las relaciones');	

		

      $this->heredaRelatedRecords("prospects_contacts_1", $bean, $cuenta);

      $this->heredaRelatedRecords("prospects_opportunities_1", $bean, $cuenta);

      $this->heredaRelatedRecords("calls", $bean, $cuenta);

      $this->heredaRelatedRecords("meetings", $bean, $cuenta);

      $this->heredaRelatedRecords("tasks", $bean, $cuenta);

      $this->heredaRelatedRecords("notes", $bean, $cuenta);

      $this->heredaRelatedRecords("low01_solicitudescredito_prospects", $bean, $cuenta);

      $this->heredaRelatedRecords("prospects_orden_ordenes_1", $bean, $cuenta);

    }

	

	$GLOBALS['log']->fatal('Fin del proceso');	

	

    return $guardada;

  }



  private function envia_email_bienvenida_cliente($bean){

    require_once('custom/modules/EmailTemplates/NotifierHelper.php');

    $notifierHelper = new NotifierHelper;

    $admin = new Administration();

    $admin-> retrieveSettings();

    $configKey = 'lowes_settings_email_bienvenida_cli';

    $template_id = $admin->settings[$configKey];

    $notifierHelper->sendMessage($bean, array("to" => array($bean)), $template_id);

  }



  public function generaIDPOS($bean, $event, $arguments){

    return true;

  }



  private function heredaRelatedRecords($linkName, $prospect, $account)

  {

    $beans = [];

    $link = null;

    switch ($linkName) {

      case 'prospects_contacts_1':

        $prospect->load_relationship('prospects_contacts_1');

        $beans = $prospect->prospects_contacts_1->getBeans();

        $account->load_relationship('contacts');

        $link = $account->contacts;

        break;

      case 'prospects_opportunities_1':

        $prospect->load_relationship('prospects_opportunities_1');

        $beans = $prospect->prospects_opportunities_1->getBeans();

        $account->load_relationship('opportunities');

        $link = $account->opportunities;

        break;

      case 'calls':

        $prospect->load_relationship('calls');

        $beans = $prospect->calls->getBeans();

        $account->load_relationship('calls');

        $link = $account->calls;

        break;

      case 'meetings':

        $prospect->load_relationship('meetings');

        $beans = $prospect->meetings->getBeans();

        $account->load_relationship('meetings');

        $link = $account->meetings;

        break;

      case 'tasks':

        $prospect->load_relationship('tasks');

        $beans = $prospect->tasks->getBeans();

        $account->load_relationship('tasks');

        $link = $account->tasks;

        break;

      case 'notes':

        $prospect->load_relationship('notes');

        $beans = $prospect->notes->getBeans();

        $account->load_relationship('notes');

        $link = $account->notes;

        break;

      case 'low01_solicitudescredito_prospects':

        $prospect->load_relationship('low01_solicitudescredito_prospects');

        $beans = $prospect->low01_solicitudescredito_prospects->getBeans();

        $account->load_relationship('low01_solicitudescredito_accounts');

        $link = $account->low01_solicitudescredito_accounts;

        break;

      case 'prospects_orden_ordenes_1':

        $prospect->load_relationship('prospects_orden_ordenes_1');

        $beans = $prospect->prospects_orden_ordenes_1->getBeans();

        $account->load_relationship('accounts_orden_ordenes_1');

        $link = $account->accounts_orden_ordenes_1;

        break;

    }

    if(in_array($linkName, ['calls','meetings','tasks','notes'])){

      foreach ($beans as $bean) {

        $bean->parent_type = "Accounts";

        $bean->parent_id = $account->id;

        $bean->save();

      }

    }

    else{

      foreach ($beans as $bean) {

        $link->add($bean);

      }

    }

  }



  public function getRelatedBeans($bean, $linkName)

  {

    $saldoDeudor = 0;

    $sugarQuery = new SugarQuery();

    $sugarQuery->from($bean, array('team_security'=>false));

    $joinName = $sugarQuery->join($linkName, array('team_security'=>false))->joinName();

    $sugarQuery->where()->equals('id', $bean->id);

    $sugarQuery->select(array("$joinName.id"));

    $results = $sugarQuery->execute();

    return $results;

  }



  public function getSolicitudAprobada($prospect)

  {

    $bean = BeanFactory::newBean('LOW01_SolicitudesCredito');

    $sugarQuery = new SugarQuery();

    $sugarQuery->from($bean, array('team_security'=>false));

    $joinName = $sugarQuery->join('low01_solicitudescredito_prospects', array('team_security'=>false))->joinName();

    $sugarQuery->where()

      ->equals("$joinName.id", $prospect->id)

      ->in('estado_solicitud_c', [

        'Aprobacion_por_Gerente_de_Credito',

        'Aprobacion_por_Director_de_Finanzas',

        'Aprobacion_por_Vicepresidencia_Comercial'

      ]);

    $sugarQuery->select(array("id"));

    $results = $sugarQuery->execute();

    if(count($results)){

      $bean = BeanFactory::getBean('LOW01_SolicitudesCredito', $results[0]['id']);

    }

    return $bean;

  }

}

