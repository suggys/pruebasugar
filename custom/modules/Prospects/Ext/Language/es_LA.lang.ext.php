<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.CF_Prospects_20170801.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CONTACTO'] = 'Contacto';
$mod_strings['LBL_CONTACT_ID'] = 'Contacto';
$mod_strings['LBL_PROYECTO'] = 'Proyecto';
$mod_strings['LBL_OPPORTUNITY_ID'] = 'Proyecto';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.CF_Prospects_20170711.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.CF_Prospects_20170717.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.CF_Prospects_20170724.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_LAST_NAME'] = 'Nombre o Razón Social';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_CONTACTO'] = 'Contacto';
$mod_strings['LBL_PROYECTO'] = 'Proyecto';
$mod_strings['LBL_ESTADO_ENVIO_POS'] = 'Estado Envío a POS';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_CREATED_OPPORTUNITY'] = 'Crear un nuevo Proyecto';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Proyecto:';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Crear Proyecto';
$mod_strings['NTC_OPPORTUNITY_REQUIRES_ACCOUNT'] = 'La creación de un proyecto requiere una Cliente Crédito AR.\\n Por favor, cree una cuenta nueva o seleccione una existente.';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.SolicitudCredito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.customprospects_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171007.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_FRR.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';
$mod_strings['LBL_ESTADO_ENVIO_POS'] = 'Estado Envío a POS';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_Modules_CRM.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170907.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170922.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170828.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171012.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170817.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171020.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171010.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.customprospects_opportunities_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PROSPECTS_OPPORTUNITIES_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE'] = 'Proyectos';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_201710130100.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.customprospects_accounts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171009.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.customprospects_contacts_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170831.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.SolicitudCredito.php.LowesCRM3500.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170922.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171010.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170831.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_LAST_NAME'] = 'Nombre o Razón Social';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_CONTACTO'] = 'Contacto';
$mod_strings['LBL_PROYECTO'] = 'Proyecto';
$mod_strings['LBL_ESTADO_ENVIO_POS'] = 'Estado Envío a POS';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_CREATED_OPPORTUNITY'] = 'Crear un nuevo Proyecto';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Proyecto:';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Crear Proyecto';
$mod_strings['NTC_OPPORTUNITY_REQUIRES_ACCOUNT'] = 'La creación de un proyecto requiere una Cliente Crédito AR.\\n Por favor, cree una cuenta nueva o seleccione una existente.';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';
$mod_strings['LBL_TIPO_CLIENTE_POS'] = 'tipo cliente pos';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_201710130100.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171009.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170828.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171007.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171012.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_Modules_CRM.php.LowesCRM3500.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170907.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170817.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.customprospects_accounts_1.php.LowesCRM3500.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171020.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_FRR.php.LowesCRM3500.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';
$mod_strings['LBL_ESTADO_ENVIO_POS'] = 'Estado Envío a POS';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171009.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170828.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171012.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170817.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_Modules_CRM.php.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_Modules_CRM.php.LowesCRM3500.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_201710130100.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.SolicitudCredito.php.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171012.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_LAST_NAME'] = 'Nombre o Razón Social';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_CONTACTO'] = 'Contacto';
$mod_strings['LBL_PROYECTO'] = 'Proyecto';
$mod_strings['LBL_ESTADO_ENVIO_POS'] = 'Estado Envío a POS';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_CREATED_OPPORTUNITY'] = 'Crear un nuevo Proyecto';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Proyecto:';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Crear Proyecto';
$mod_strings['NTC_OPPORTUNITY_REQUIRES_ACCOUNT'] = 'La creación de un proyecto requiere una Cliente Crédito AR.\\n Por favor, cree una cuenta nueva o seleccione una existente.';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';
$mod_strings['LBL_TIPO_CLIENTE_POS'] = 'tipo cliente pos';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_FRR.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';
$mod_strings['LBL_ESTADO_ENVIO_POS'] = 'Estado Envío a POS';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171007.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.SolicitudCredito.php.LowesCRM3500.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_LAST_NAME'] = 'Nombre o Razón Social';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_CONTACTO'] = 'Contacto';
$mod_strings['LBL_PROYECTO'] = 'Proyecto';
$mod_strings['LBL_ESTADO_ENVIO_POS'] = 'Estado Envío a POS';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_CREATED_OPPORTUNITY'] = 'Crear un nuevo Proyecto';
$mod_strings['LBL_OPP_NAME'] = 'Nombre Proyecto:';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Crear Proyecto';
$mod_strings['NTC_OPPORTUNITY_REQUIRES_ACCOUNT'] = 'La creación de un proyecto requiere una Cliente Crédito AR.\\n Por favor, cree una cuenta nueva o seleccione una existente.';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171007.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_201710130100.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170828.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_FRR.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';
$mod_strings['LBL_ESTADO_ENVIO_POS'] = 'Estado Envío a POS';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170922.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170817.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171010.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171020.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171010.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171009.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20171020.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.customprospects_accounts_1.php.LowesCRM3500.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170907.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.customprospects_accounts_1.php.cstm_names.lang.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170907.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170831.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170831.php.LowesCRM3500.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Language/es_LA.Lowes_CRM_20170922.php.cstm_names.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PROSPECT'] = 'Crear Prospecto';
$mod_strings['LBL_MODULE_NAME'] = 'Prospectos';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Prospecto';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Prospecto';
$mod_strings['LNK_PROSPECT_LIST'] = 'Ver Públicos Objetivo';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear desde vCard';
$mod_strings['LNK_IMPORT_PROSPECTS'] = 'Importar Prospectos';
$mod_strings['MSG_SHOW_DUPLICATES'] = 'El registro de Prospecto que va a crear podría ser un duplicado de otro registro de Prospecto existente. Los registros de Prospecto con nombres y/o direcciones de correo similares se muestran a continuación.Haga clic en Crear Prospecto para continuar la creación de este nuevo Prospecto, o seleccione un destino existente figuran a continuación.';
$mod_strings['LBL_SAVE_PROSPECT'] = 'Guardar Prospecto';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de Prospecto';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda de Prospecto';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ESTADO_CUENTA'] = 'Estado de la Cuenta';
$mod_strings['LBL_TIPO_DE_PERSONA'] = 'Tipo de Persona';
$mod_strings['LBL_RFC'] = 'RFC';
$mod_strings['LBL_OFFICE_PHONE'] = 'Teléfono de Oficina';
$mod_strings['LBL_OTHER_PHONE'] = 'Tel. Alternativo';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial';
$mod_strings['LBL_MOBILE_PHONE'] = 'Teléfono Celular';
$mod_strings['LBL_WEB_PAGE'] = 'Página de Internet';
$mod_strings['LBL_NOMBRE_DEL_AVAL'] = 'Nombre del Aval';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE'] = 'CP Dirección Principal:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Otro Estado';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY'] = 'Municipio Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA'] = 'Colonia Dirección Principal';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMBER'] = 'Número Dirección Principal';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos de Identificación';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_RAZON_SOCIAL'] = 'Razón Social';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Provincia/Estado Dirección Principal:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'País Dirección Principal:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL'] = 'Nombre de Representante Legal';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_LEGAL_ID'] = 'Nombre de Representante Legal';
$mod_strings['LBL_TRACKER_KEY'] = 'Clave de Seguimiento';
$mod_strings['LBL_ESTADO'] = 'Estado';
$mod_strings['LBL_FIRST_NAME'] = 'Nombre:';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO_C'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Perfil del Cliente';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_CONVERTED_LEAD'] = 'Lead Convertido';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['LBL_LEAD_ID'] = 'Id de Lead';
$mod_strings['LBL_TIENEORDEN'] = 'Tiene ordenes';

?>
