<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/prospects_opportunities_1_Prospects.php

// created: 2017-07-14 22:54:49
$dictionary["Prospect"]["fields"]["prospects_opportunities_1"] = array (
  'name' => 'prospects_opportunities_1',
  'type' => 'link',
  'relationship' => 'prospects_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_PROSPECTS_TITLE',
  'id_name' => 'prospects_opportunities_1prospects_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/prospects_contacts_1_Prospects.php

// created: 2017-07-20 21:33:08
$dictionary["Prospect"]["fields"]["prospects_contacts_1"] = array (
  'name' => 'prospects_contacts_1',
  'type' => 'link',
  'relationship' => 'prospects_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE',
  'id_name' => 'prospects_contacts_1prospects_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/ProspectsVisibility.php

$dictionary['Prospect']['visibility']["ProspectsVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/prospects_orden_ordenes_1_Prospects.php

// created: 2017-07-23 02:41:44
$dictionary["Prospect"]["fields"]["prospects_orden_ordenes_1"] = array (
  'name' => 'prospects_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'prospects_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'vname' => 'LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_PROSPECTS_TITLE',
  'id_name' => 'prospects_orden_ordenes_1prospects_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_primary_address_city.php

 // created: 2018-03-07 15:39:33
$dictionary['Prospect']['fields']['primary_address_city']['audited']=false;
$dictionary['Prospect']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Prospect']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Prospect']['fields']['primary_address_city']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['primary_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_city']['unified_search']=false;
$dictionary['Prospect']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['primary_address_city']['calculated']=false;
$dictionary['Prospect']['fields']['primary_address_city']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_alt_address_state.php

 // created: 2017-08-06 00:11:28
$dictionary['Prospect']['fields']['alt_address_state']['audited']=false;
$dictionary['Prospect']['fields']['alt_address_state']['massupdate']=false;
$dictionary['Prospect']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Prospect']['fields']['alt_address_state']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['alt_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['alt_address_state']['unified_search']=false;
$dictionary['Prospect']['fields']['alt_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['alt_address_state']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_first_name.php

 // created: 2018-03-07 15:37:18
$dictionary['Prospect']['fields']['first_name']['required']=true;
$dictionary['Prospect']['fields']['first_name']['audited']=false;
$dictionary['Prospect']['fields']['first_name']['massupdate']=false;
$dictionary['Prospect']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Prospect']['fields']['first_name']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['first_name']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['first_name']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['first_name']['unified_search']=false;
$dictionary['Prospect']['fields']['first_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.37',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['first_name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Prospect']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_phone_mobile.php

 // created: 2017-08-01 07:50:25
$dictionary['Prospect']['fields']['phone_mobile']['len']='10';
$dictionary['Prospect']['fields']['phone_mobile']['audited']=false;
$dictionary['Prospect']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Prospect']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Prospect']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['phone_mobile']['unified_search']=false;
$dictionary['Prospect']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.88',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['phone_mobile']['calculated']=false;
$dictionary['Prospect']['fields']['phone_mobile']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_primary_address_street.php

 // created: 2018-03-07 15:38:54
$dictionary['Prospect']['fields']['primary_address_street']['required']=true;
$dictionary['Prospect']['fields']['primary_address_street']['audited']=false;
$dictionary['Prospect']['fields']['primary_address_street']['massupdate']=false;
$dictionary['Prospect']['fields']['primary_address_street']['comments']='The street address used for primary address';
$dictionary['Prospect']['fields']['primary_address_street']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['primary_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_street']['unified_search']=false;
$dictionary['Prospect']['fields']['primary_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.22',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['primary_address_street']['calculated']=false;
$dictionary['Prospect']['fields']['primary_address_street']['rows']='4';
$dictionary['Prospect']['fields']['primary_address_street']['cols']='20';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/low01_solicitudescredito_prospects_Prospects.php

// created: 2017-07-21 12:47:08
$dictionary["Prospect"]["fields"]["low01_solicitudescredito_prospects"] = array (
  'name' => 'low01_solicitudescredito_prospects',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_prospects',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE',
  'id_name' => 'low01_solicitudescredito_prospectsprospects_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_last_name.php

 // created: 2018-05-11 14:01:26
$dictionary['Prospect']['fields']['last_name']['audited']=false;
$dictionary['Prospect']['fields']['last_name']['massupdate']=false;
$dictionary['Prospect']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Prospect']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['last_name']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['last_name']['unified_search']=false;
$dictionary['Prospect']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.36',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['last_name']['calculated']=false;
$dictionary['Prospect']['fields']['last_name']['len']='80';
$dictionary['Prospect']['fields']['last_name']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_email.php

 // created: 2017-08-01 07:45:51
$dictionary['Prospect']['fields']['email']['len']='100';
$dictionary['Prospect']['fields']['email']['audited']=false;
$dictionary['Prospect']['fields']['email']['massupdate']=true;
$dictionary['Prospect']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['email']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['email']['unified_search']=false;
$dictionary['Prospect']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.35',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['email']['calculated']=false;
$dictionary['Prospect']['fields']['email']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_tracker_key.php

 // created: 2017-07-13 22:29:32
$dictionary['Prospect']['fields']['tracker_key']['required']=false;
$dictionary['Prospect']['fields']['tracker_key']['audited']=false;
$dictionary['Prospect']['fields']['tracker_key']['massupdate']=false;
$dictionary['Prospect']['fields']['tracker_key']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['tracker_key']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['tracker_key']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['tracker_key']['unified_search']=false;
$dictionary['Prospect']['fields']['tracker_key']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['tracker_key']['calculated']=false;
$dictionary['Prospect']['fields']['tracker_key']['enable_range_search']=false;
$dictionary['Prospect']['fields']['tracker_key']['autoinc_next']='1';
$dictionary['Prospect']['fields']['tracker_key']['min']=false;
$dictionary['Prospect']['fields']['tracker_key']['max']=false;
$dictionary['Prospect']['fields']['tracker_key']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/prospects_accounts_1_Prospects.php

// created: 2017-07-20 22:48:10
$dictionary["Prospect"]["fields"]["prospects_accounts_1"] = array (
  'name' => 'prospects_accounts_1',
  'type' => 'link',
  'relationship' => 'prospects_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'prospects_accounts_1accounts_idb',
);
$dictionary["Prospect"]["fields"]["prospects_accounts_1_name"] = array (
  'name' => 'prospects_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'prospects_accounts_1accounts_idb',
  'link' => 'prospects_accounts_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["Prospect"]["fields"]["prospects_accounts_1accounts_idb"] = array (
  'name' => 'prospects_accounts_1accounts_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'prospects_accounts_1accounts_idb',
  'link' => 'prospects_accounts_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_primary_address_state.php

 // created: 2018-03-07 15:40:02
$dictionary['Prospect']['fields']['primary_address_state']['type']='enum';
$dictionary['Prospect']['fields']['primary_address_state']['options']='state_c_list';
$dictionary['Prospect']['fields']['primary_address_state']['len']=100;
$dictionary['Prospect']['fields']['primary_address_state']['audited']=false;
$dictionary['Prospect']['fields']['primary_address_state']['massupdate']=true;
$dictionary['Prospect']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Prospect']['fields']['primary_address_state']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['primary_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_state']['unified_search']=false;
$dictionary['Prospect']['fields']['primary_address_state']['calculated']=false;
$dictionary['Prospect']['fields']['primary_address_state']['dependency']=false;
$dictionary['Prospect']['fields']['primary_address_state']['visibility_grid']=array (
  'trigger' => 'primary_address_country',
  'values' => 
  array (
    '' => 
    array (
      0 => '',
    ),
    'US' => 
    array (
      0 => 'OTRO',
    ),
    'CA' => 
    array (
      0 => 'OTRO',
    ),
    'FR' => 
    array (
      0 => 'OTRO',
    ),
    'MX' => 
    array (
      0 => 'AGU',
      1 => 'BCN',
      2 => 'BCS',
      3 => 'CAM',
      4 => 'CHP',
      5 => 'CHH',
      6 => 'CMX',
      7 => 'COA',
      8 => 'COL',
      9 => 'DUR',
      10 => 'MEX',
      11 => 'GRO',
      12 => 'GUA',
      13 => 'HID',
      14 => 'JAL',
      15 => 'MIC',
      16 => 'MOR',
      17 => 'NAY',
      18 => 'NLE',
      19 => 'OAX',
      20 => 'PUE',
      21 => 'QUE',
      22 => 'ROO',
      23 => 'SLP',
      24 => 'SIN',
      25 => 'SON',
      26 => 'TAB',
      27 => 'TAM',
      28 => 'TLA',
      29 => 'VER',
      30 => 'YUC',
      31 => 'ZAC',
    ),
    'EU' => 
    array (
      0 => 'OTRO',
    ),
    'DE' => 
    array (
      0 => 'OTRO',
    ),
    'GB' => 
    array (
      0 => 'OTRO',
    ),
    'JP' => 
    array (
      0 => 'OTRO',
    ),
    'PR' => 
    array (
      0 => 'OTRO',
    ),
  ),
);
$dictionary['Prospect']['fields']['primary_address_state']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_primary_address_postalcode.php

 // created: 2018-03-07 15:40:44
$dictionary['Prospect']['fields']['primary_address_postalcode']['len']='5';
$dictionary['Prospect']['fields']['primary_address_postalcode']['audited']=false;
$dictionary['Prospect']['fields']['primary_address_postalcode']['massupdate']=false;
$dictionary['Prospect']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Prospect']['fields']['primary_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['primary_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_postalcode']['unified_search']=false;
$dictionary['Prospect']['fields']['primary_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['primary_address_postalcode']['calculated']=false;
$dictionary['Prospect']['fields']['primary_address_postalcode']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_phone_work.php

 // created: 2018-03-07 15:42:38
$dictionary['Prospect']['fields']['phone_work']['len']='13';
$dictionary['Prospect']['fields']['phone_work']['massupdate']=false;
$dictionary['Prospect']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Prospect']['fields']['phone_work']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['phone_work']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['phone_work']['unified_search']=false;
$dictionary['Prospect']['fields']['phone_work']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.87',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['phone_work']['calculated']=false;
$dictionary['Prospect']['fields']['phone_work']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_primary_address_country.php

 // created: 2018-03-07 15:41:20
$dictionary['Prospect']['fields']['primary_address_country']['type']='enum';
$dictionary['Prospect']['fields']['primary_address_country']['options']='prospect_pais_list';
$dictionary['Prospect']['fields']['primary_address_country']['audited']=false;
$dictionary['Prospect']['fields']['primary_address_country']['massupdate']=true;
$dictionary['Prospect']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Prospect']['fields']['primary_address_country']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['primary_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_country']['unified_search']=false;
$dictionary['Prospect']['fields']['primary_address_country']['calculated']=false;
$dictionary['Prospect']['fields']['primary_address_country']['dependency']=false;
$dictionary['Prospect']['fields']['primary_address_country']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_cuenta_patron_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Prospect']['fields']['cuenta_patron_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['cuenta_patron_c']['labelValue']='Cuenta del Patrón';
$dictionary['Prospect']['fields']['cuenta_patron_c']['enforced']='';
$dictionary['Prospect']['fields']['cuenta_patron_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_club_pro_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Prospect']['fields']['club_pro_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['club_pro_c']['labelValue']='Cuenta con Tarjeta Club PRO';
$dictionary['Prospect']['fields']['club_pro_c']['enforced']='';
$dictionary['Prospect']['fields']['club_pro_c']['dependency']='equal($tipo_cliente_c,"especialista")';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_contacto_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Prospect']['fields']['contacto_c']['labelValue']='Contacto';
$dictionary['Prospect']['fields']['contacto_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_contact_id_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Prospect']['fields']['contact_id_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_competidores_directos_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Prospect']['fields']['competidores_directos_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['competidores_directos_c']['labelValue']='Competidores Directos';
$dictionary['Prospect']['fields']['competidores_directos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['competidores_directos_c']['enforced']='';
$dictionary['Prospect']['fields']['competidores_directos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_aprueba_oc_o_cot_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Prospect']['fields']['aprueba_oc_o_cot_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_estado_cuenta_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Prospect']['fields']['estado_cuenta_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['estado_cuenta_c']['labelValue']='Estado de la Cuenta';
$dictionary['Prospect']['fields']['estado_cuenta_c']['dependency']='';
$dictionary['Prospect']['fields']['estado_cuenta_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2018-05-14 04:05:43

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2018-05-14 04:05:43

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_estado_c.php

 // created: 2018-05-14 04:05:43
$dictionary['Prospect']['fields']['estado_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['estado_c']['labelValue']='Estado';
$dictionary['Prospect']['fields']['estado_c']['dependency']='';
$dictionary['Prospect']['fields']['estado_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_nombre_del_aval_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['nombre_del_aval_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['nombre_del_aval_c']['labelValue']='Nombre del Aval';
$dictionary['Prospect']['fields']['nombre_del_aval_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['nombre_del_aval_c']['enforced']='';
$dictionary['Prospect']['fields']['nombre_del_aval_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_folio_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['folio_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['folio_c']['labelValue']='Folio';
$dictionary['Prospect']['fields']['folio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['folio_c']['enforced']='true';
$dictionary['Prospect']['fields']['folio_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_numero_club_pro_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['numero_club_pro_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['numero_club_pro_c']['labelValue']='Número de Tarjeta Club PRO';
$dictionary['Prospect']['fields']['numero_club_pro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['numero_club_pro_c']['enforced']='';
$dictionary['Prospect']['fields']['numero_club_pro_c']['dependency']='equal($club_pro_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_id_pos_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['id_pos_c']['labelValue']='ID POS';
$dictionary['Prospect']['fields']['id_pos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['id_pos_c']['enforced']='';
$dictionary['Prospect']['fields']['id_pos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_otro_oficio_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['otro_oficio_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['otro_oficio_c']['labelValue']='Otro Oficio';
$dictionary['Prospect']['fields']['otro_oficio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['otro_oficio_c']['enforced']='';
$dictionary['Prospect']['fields']['otro_oficio_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_opportunity_id_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['opportunity_id_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_giro_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['giro_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['giro_c']['labelValue']='Giro / Industria';
$dictionary['Prospect']['fields']['giro_c']['visibility_grid']=array (
  'trigger' => 'tipo_cliente_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'comercial' => 
    array (
      0 => 'arquitecto',
      1 => 'construccion_vertical',
      2 => 'contratista',
      3 => 'decorador',
      4 => 'desarrollador_de_vivienda_residencial',
      5 => 'ingeniero',
      6 => 'mtto_industrial',
      7 => 'obra_civil',
      8 => 'proyecto_temporal_del_hogar',
      9 => 'urbanizador',
      10 => 'otro',
    ),
    'especialista' => 
    array (
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_razon_social_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['razon_social_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['razon_social_c']['labelValue']='Razón Social';
$dictionary['Prospect']['fields']['razon_social_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['razon_social_c']['enforced']='';
$dictionary['Prospect']['fields']['razon_social_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_primary_address_number_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['primary_address_number_c']['labelValue']='Número Dirección Principal';
$dictionary['Prospect']['fields']['primary_address_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['primary_address_number_c']['enforced']='';
$dictionary['Prospect']['fields']['primary_address_number_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_otro_giro_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['otro_giro_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['otro_giro_c']['labelValue']='Otro Giro / Industria';
$dictionary['Prospect']['fields']['otro_giro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['otro_giro_c']['enforced']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_estado_envio_pos_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['estado_envio_pos_c']['labelValue']='Estado Envío a POS';
$dictionary['Prospect']['fields']['estado_envio_pos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['estado_envio_pos_c']['enforced']='false';
$dictionary['Prospect']['fields']['estado_envio_pos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_primary_address_colonia_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['primary_address_colonia_c']['labelValue']='Colonia Dirección Principal';
$dictionary['Prospect']['fields']['primary_address_colonia_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['primary_address_colonia_c']['enforced']='';
$dictionary['Prospect']['fields']['primary_address_colonia_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_nombre_representante_legal_id_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['nombre_representante_legal_id_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_id_ar_credit_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['id_ar_credit_c']['labelValue']='ID AR Credit';
$dictionary['Prospect']['fields']['id_ar_credit_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['id_ar_credit_c']['enforced']='';
$dictionary['Prospect']['fields']['id_ar_credit_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_oficio_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['oficio_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['oficio_c']['labelValue']='Oficio';
$dictionary['Prospect']['fields']['oficio_c']['dependency']='';
$dictionary['Prospect']['fields']['oficio_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_metodo_pago_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['metodo_pago_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['metodo_pago_c']['labelValue']='Método de Pago';
$dictionary['Prospect']['fields']['metodo_pago_c']['visibility_grid']=array (
  'trigger' => 'estado_c',
  'values' => 
  array (
    'nuevo' => 
    array (
      0 => '',
    ),
    'convertido' => 
    array (
      0 => '',
      1 => 'cheque_al_dia',
      2 => 'cheque_post-fechado',
      3 => 'credito_AR',
      4 => 'efectivo',
      5 => 'tarjeta_de_regalo',
      6 => 'tc',
      7 => 'otros',
    ),
  ),
);

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_nombre_representante_legal_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['nombre_representante_legal_c']['labelValue']='Nombre de Representante Legal';
$dictionary['Prospect']['fields']['nombre_representante_legal_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_proyecto_c.php

 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['proyecto_c']['labelValue']='Proyecto';
$dictionary['Prospect']['fields']['proyecto_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_sucursal_c.php

 // created: 2018-05-14 04:05:45
$dictionary['Prospect']['fields']['sucursal_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['sucursal_c']['labelValue']='Sucursal';
$dictionary['Prospect']['fields']['sucursal_c']['dependency']='';
$dictionary['Prospect']['fields']['sucursal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_vol_ventas_anual_est_c.php

 // created: 2018-05-14 04:05:45
$dictionary['Prospect']['fields']['vol_ventas_anual_est_c']['labelValue']='Volumen de Ventas Anuales Estimadas';
$dictionary['Prospect']['fields']['vol_ventas_anual_est_c']['enforced']='';
$dictionary['Prospect']['fields']['vol_ventas_anual_est_c']['dependency']='';
$dictionary['Prospect']['fields']['vol_ventas_anual_est_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_tipo_cliente_c.php

 // created: 2018-05-14 04:05:45
$dictionary['Prospect']['fields']['tipo_cliente_c']['labelValue']='Tipo de Cliente';
$dictionary['Prospect']['fields']['tipo_cliente_c']['dependency']='';
$dictionary['Prospect']['fields']['tipo_cliente_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_tieneorden_c.php

 // created: 2018-05-14 04:05:45
$dictionary['Prospect']['fields']['tieneorden_c']['labelValue']='Tiene ordenes';
$dictionary['Prospect']['fields']['tieneorden_c']['enforced']='';
$dictionary['Prospect']['fields']['tieneorden_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_tipo_de_persona_c.php

 // created: 2018-05-14 04:05:45
$dictionary['Prospect']['fields']['tipo_de_persona_c']['labelValue']='Tipo de Persona';
$dictionary['Prospect']['fields']['tipo_de_persona_c']['dependency']='';
$dictionary['Prospect']['fields']['tipo_de_persona_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_rfc_c.php

 // created: 2018-05-14 04:05:45
$dictionary['Prospect']['fields']['rfc_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['rfc_c']['labelValue']='RFC';
$dictionary['Prospect']['fields']['rfc_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['rfc_c']['enforced']='';
$dictionary['Prospect']['fields']['rfc_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_tipo_cliente_pos_c.php

 // created: 2018-05-14 04:05:45
$dictionary['Prospect']['fields']['tipo_cliente_pos_c']['labelValue']='tipo cliente pos';
$dictionary['Prospect']['fields']['tipo_cliente_pos_c']['dependency']='';
$dictionary['Prospect']['fields']['tipo_cliente_pos_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_web_page_c.php

 // created: 2018-05-14 04:05:45
$dictionary['Prospect']['fields']['web_page_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['web_page_c']['labelValue']='Página de Internet';
$dictionary['Prospect']['fields']['web_page_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['web_page_c']['enforced']='';
$dictionary['Prospect']['fields']['web_page_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Vardefs/sugarfield_subtipo_c.php

 // created: 2018-05-14 04:05:45
$dictionary['Prospect']['fields']['subtipo_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['subtipo_c']['labelValue']='Subtipo';
$dictionary['Prospect']['fields']['subtipo_c']['dependency']='not(equal($tipo_cliente_c,""))';
$dictionary['Prospect']['fields']['subtipo_c']['visibility_grid']='';

 
?>
