<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Layoutdefs/prospects_opportunities_1_Prospects.php

 // created: 2017-07-14 22:54:49
$layout_defs["Prospects"]["subpanel_setup"]['prospects_opportunities_1'] = array (
  'order' => 100,
  'module' => 'Opportunities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'get_subpanel_data' => 'prospects_opportunities_1',
  'top_buttons' =>
  array (
    0 =>
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 =>
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Layoutdefs/prospects_contacts_1_Prospects.php

 // created: 2017-07-20 21:33:07
$layout_defs["Prospects"]["subpanel_setup"]['prospects_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'prospects_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Layoutdefs/prospects_orden_ordenes_1_Prospects.php

 // created: 2017-07-23 02:41:44
$layout_defs["Prospects"]["subpanel_setup"]['prospects_orden_ordenes_1'] = array (
  'order' => 100,
  'module' => 'orden_Ordenes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE',
  'get_subpanel_data' => 'prospects_orden_ordenes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Layoutdefs/low01_solicitudescredito_prospects_Prospects.php

 // created: 2017-07-21 12:47:07
$layout_defs["Prospects"]["subpanel_setup"]['low01_solicitudescredito_prospects'] = array (
  'order' => 100,
  'module' => 'LOW01_SolicitudesCredito',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_prospects',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Layoutdefs/_overrideProspect_subpanel_prospects_contacts_1.php

//auto-generated file DO NOT EDIT
$layout_defs['Prospects']['subpanel_setup']['prospects_contacts_1']['override_subpanel_name'] = 'Prospect_subpanel_prospects_contacts_1';

?>
