<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Dependencies/id_ar_credit_c_deps.php

// Id AR credi visibility
$dependencies['Prospects']['id_ar_credit_c_visibility'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'equal($id_ar_credit_c,"")',
    'triggerFields' => array(''),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'SetVisibility',
          'params' => array(
              'target' => 'id_ar_credit_c',
              'value' => 'not(equal($id_ar_credit_c,""))',
          ),
      ),
    ),
    // 'notActions' => array(
    //   array(
    //       'name' => 'SetVisibility',
    //       'params' => array(
    //           'target' => 'id_ar_credit_c',
    //           'value' => 'true',
    //       ),
    //   ),
    // ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Dependencies/weg_page_c_deps.php

$dependencies['Prospects']['web_page_c_readonly'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal($estado_c, "convertido")',
    'triggerFields' => array(''),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'web_page_c',
              'value' => 'true', //Formula
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Dependencies/aprueba_oc_o_cot_c_deps.php

$dependencies['Prospects']['aprueba_oc_o_cot_c_visible'] = array(
    'hooks' => array("edit","view"),
    //Trigger formula for the dependency. Defaults to 'true'.
    // 'trigger' => 'not(equal($name, ""))',
    'triggerFields' => [''],
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'SetVisibility', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'aprueba_oc_o_cot_c',
              'value' => 'not(equal($name, ""))', //Formula
              // 'label' => 'aprueba_oc_o_cot_c_label',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Dependencies/phone_work_deps.php

$dependencies['Prospects']['phone_work_required'] = array(
    'hooks' => array("edit","view"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal($estado_c, "convertido")',
    'triggerFields' => array('estado_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'SetRequired', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'phone_work',
              'value' => 'true', //Formula
              'label' => 'Requerido',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Dependencies/id_pos_c_deps.php

$dependencies['Prospects']['id_pos_c_deps'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'true',
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'id_pos_c',
              'value' => 'true',
          ),
      ),
    ),
);
// Id ar credito visibility.
$dependencies['Prospects']['id_pos_c_visibility'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'equal($id_pos_c,"")',
    // 'triggerFields' => array('id_ar_credit_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'SetVisibility',
          'params' => array(
              'target' => 'id_pos_c',
              'value' => 'false',
          ),
      ),
    ),
    'notActions' => array(
      array(
          'name' => 'SetVisibility',
          'params' => array(
              'target' => 'id_pos_c',
              'value' => 'true',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Dependencies/estado_envio_pos_c_deps.php

$dependencies['Prospects']['estado_envio_pos_c'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'true',
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'estado_envio_pos_c',
              'value' => 'true',
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Dependencies/subtipo_c_required.php


$dependencies['Prospects']['subtipo_c_required'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('estado_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'subtipo_c',
        'label' => 'subtipo_c_label',
        'value' => 'equal($estado_c,"convertido")',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'subtipo_c',
        'label' => 'subtipo_c_label',
        'value' => 'false',
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Dependencies/tipo_cliente_c_required.php


$dependencies['Prospects']['tipo_cliente_c_required'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('estado_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'tipo_cliente_c',
        'label' => 'tipo_cliente_c_label',
        'value' => 'equal($estado_c,"convertido")',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'tipo_cliente_c',
        'label' => 'tipo_cliente_c_label',
        'value' => 'false',
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Dependencies/alt_address_state_deps.php

$dependencies['Prospects']['alt_address_state_actions'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal($estado_c, "convertido")',
    'triggerFields' => array('estado_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'alt_address_state',
              'value' => true, //Formula
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/Prospects/Ext/Dependencies/hide_panel_3_deps.php

$dependencies['Prospects']['hide_panel_3_deps'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'equal($estado_c, "nuevo" )',
  'triggerFields' => array('estado_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetPanelVisibility',
      'params' => array(
        'target' => 'LBL_RECORDVIEW_PANEL3',
        'value' => 'true',
      ),
    )
  ),
  'notActions' => array(
    array(
      'name' => 'SetPanelVisibility',
      'params' => array(
        'target' => 'LBL_RECORDVIEW_PANEL3',
        'value' => 'true',
      ),
    ),
  ),
);

?>
