({
  extendsFrom: "BoolField",

  _render: function () {
    this._super("_render", arguments);
    if(this.name === 'aprueba_oc_o_cot_c' && this.model.get('aprueba_oc_o_cot_c')){
      this.action = "disabled";
    }
  }
})
