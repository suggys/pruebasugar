({
  extendsFrom: "FullnameField",
  initialize: function () {
    this._super("initialize", arguments);
    var meta = app.metadata.getModule(this.module);

    this.def.fields = [
      meta.fields['last_name'],
    ];
  }
})
