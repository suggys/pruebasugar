({
  extendsFrom:"CreateView",

  my_events: {
    'keyup input[name=primary_address_postalcode]': '_keypress_primary_address_postalcode',
    'keypress input[name=primary_address_postalcode]': '_keypress_event_primary_address_postalcode',
  },

  initialize: function () {
    var self = this;
    this._super("initialize", arguments);
    //this.model.on('change:assigned_user_name',_.bind(this._changeAssignedUserName, this));
    this.model.addValidationTask('validateCP',_.bind(this._doValidateCP, this));
    this.model.addValidationTask('validatePhoneWork',_.bind(this._doValidatePhoneWork, this));
    this.model.addValidationTask('validatePhoneMobile',_.bind(this._doValidatePhoneMobile, this));
    this.model.addValidationTask('validateVendedor',_.bind(this._doValidateVendedor, this));
    this.model.addValidationTask('validateSucursal',_.bind(this._doValidateSucursal, this));
    this.model.addValidationTask('validateContacto',_.bind(this._doValidateContacto, this));
    this.model.addValidationTask('validateProyecto',_.bind(this._doValidateProyecto, this));
    this.model.addValidationTask('validateRFC',_.bind(this._doValidateRFC, this));
    // self.model.on('sync', function(model, value){
      self.model.on('change:primary_address_state', _.bind(self._valida_otro_estado, self));
    //});
  },

  _keypress_event_primary_address_postalcode: function (event) {
    var controlKeys = [8, 9, 13, 35, 36, 37, 39];
    // IE doesn't support indexOf
    var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
    console.log(event.which);
    // Some browsers just don't raise events for control keys. Easy.
    // e.g. Safari backspace.
    if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
      (48 <= event.which && event.which <= 57) || // Always 1 through 9
      /*(48 == event.which && $(this).attr("value")) ||*/ // No 0 first digit
      isControlKey) { // Opera assigns values for control keys.
        return;
      } else {
        event.preventDefault();
      }
    },

    _keypress_primary_address_postalcode: function(evt) {
      var self = this;
      var postalCode = self.$el.find('input[name=primary_address_postalcode]').val() || '';
      var values = [];
      console.log(postalCode);
      if(String(postalCode).length == 5)
      {
        app.alert.show('datos_cp', {
          level: 'process',
          title: 'Cargando datos de direcci&oacute;n.',
          autoClose: false
        });
        var url = app.api.buildURL('Merxbp/Zipcode', 'read',{},{zipcode:postalCode});
        app.api.call('read', url, {}, {
          success: function(data){
            self._setModeAndDisablefieldsAddress('edit', false);
            self.model.set({'primary_address_country': 'MX'});
            if(data.Code == 200) {
              self.model.set({'primary_address_country': 'MX'});
              self.model.set({'primary_address_city': data.Address.Ciudad});
              self.model.set({'primary_address_state': data.Address.Abr_Estado});

              _.each(data.Address.Colonias, function(col){values.push({id: col.Colonia, text: col.Colonia});});
              values.push({id : 'Otra', text: 'Otra'});
              self.$el.find('input[name=primary_address_colonia_c]').select2({
                width:"100%",
                containerCssClass: 'select2-choices-pills-close',
                data: values
              });

              if(values.length == 2)
              {
                self.model.set({'primary_address_colonia_c': values[0].text});
                self.$el.find('input[name=primary_address_colonia_c]').val(values[0].text).trigger('change');
              }
            }
            app.alert.dismiss('datos_cp');
          }
        });
      } else {
        self._setModeAndDisablefieldsAddress('readonly', true);
        self._clearFieldsAddress();
      }
    },

    _clearFieldsAddress: function(){
      var self = this;
      self.model.set(
        {
          'primary_address_colonia_c': '',
          'primary_address_city': '',
          'primary_address_state': '',
          'alt_address_state': ''
        }
      );
    },

    _setModeAndDisablefieldsAddress: function(mode, disable){
      var self = this;
      self.getField('primary_address_colonia_c').setMode(mode);
      self.getField('primary_address_colonia_c').setDisabled(disable);

      self.getField('primary_address_city').setMode(mode);
      self.getField('primary_address_city').setDisabled(disable);

      self.getField('primary_address_country').setMode(mode);
      self.getField('primary_address_country').setDisabled(disable);

      self.getField('primary_address_state').setMode(mode);
      self.getField('primary_address_state').setDisabled(disable);

      self.getField('alt_address_state').setMode(mode);
      self.getField('alt_address_state').setDisabled(disable);
      // self.getField('otra_colonia_c').setMode(mode);
      // self.getField('otra_colonia_c').setDisabled(disable);
    },

    // _changeAssignedUserName: function (model, value) {
    //   var self = this;
    //   debugger;
    //   idUser = self.model.get('assigned_user_id');
    //   var url_users = app.api.buildURL('Users/'+idUser);
    //   app.api.call('read', url_users, {}, {
    //     success: function(data){
    //       if(data){
    //         self.model.set({sucursal_c:data.sucursal_c});
    //       }
    //     }
    //   });
    // },

    _doValidateCP: function (fields, errors, callback) {
      var self = this;
      var exp = /\d{5}/;
      if(self.model.get('primary_address_postalcode') && !exp.test(self.model.get('primary_address_postalcode'))){
        errors['primary_address_postalcode'] = errors['primary_address_postalcode'] || {};
        errors['primary_address_postalcode']['El c\u00F3digo postal debe de contener 5 d\u00EDgitos.'] = true;
      }
      callback(null, fields, errors);
    },

    _doValidatePhoneWork: function (fields, errors, callback) {
      var self = this;
      var exp = /\d{13}/;
      if(self.model.get('phone_work') && !exp.test(self.model.get('phone_work'))){
        errors['phone_work'] = errors['phone_work'] || {};
        errors['phone_work']['El tel\u00E9fono de oficina debe contener 13 d\u00EDgitos.'] = true;
      }
      callback(null, fields, errors);
    },

    _doValidatePhoneMobile: function (fields, errors, callback) {
      var self = this;
      var exp = /\d{10}/;
      if(self.model.get('phone_mobile') && !exp.test(self.model.get('phone_mobile'))){
        errors['phone_mobile'] = errors['phone_mobile'] || {};
        errors['phone_mobile']['El tel\u00E9fono celular debe contener 10 d\u00EDgitos.'] = true;
      }
      callback(null, fields, errors);
    },

    _render: function (argument) {
      var self = this;
      if(this.model.isNotEmpty){
        if(!_.isEmpty(self.model.get('assigned_user_id'))){
          idUser = self.model.get('assigned_user_id');
          var url_users = app.api.buildURL('Users/'+idUser);
          app.api.call('read', url_users, {}, {
            success: function(data){
              if(data){
                self.model.set({sucursal_c:data.sucursal_c});
              }
            }
          });
        }
      }
      this._super('_render', arguments);
      this.delegateEvents(_.extend({}, this.events, this.my_events));
      // self.$el.find('div[data-panelname="LBL_RECORDVIEW_PANEL3"]').addClass('hidden');
    },

    _doValidateVendedor: function (fields, errors, callback) {
      var self = this;
      if(_.isEmpty(self.model.get('assigned_user_name'))){
        errors['assigned_user_name'] = errors['assigned_user_name'] || {};
        errors['assigned_user_name'].required = true;
      }
      callback(null, fields, errors);
    },

    _doValidateSucursal: function (fields, errors, callback) {
      var self = this;
      if(_.isEmpty(self.model.get('sucursal_c'))){
        errors['sucursal_c'] = errors['sucursal_c'] || {};
        errors['sucursal_c'].required = true;
      }
      callback(null, fields, errors);
    },

    _renderHtml: function (argument) {
      var self = this;
      this._super('_renderHtml', arguments);
      if(self.getField('estado_c')){
        self.getField('estado_c').setMode('readonly');
        self.getField('estado_c').setDisabled('true');
      }
      self._valida_otro_estado();
    },

    _doValidateContacto: function (fields, errors, callback) {
      var self = this;
      if(_.isEmpty(self.model.get('contacto_c'))){
        errors['contacto_c'] = errors['contacto_c'] || {};
        errors['contacto_c'].required = true;
      }
      callback(null, fields, errors);
    },

    _doValidateProyecto: function (fields, errors, callback) {
      var self = this;
      if(_.isEmpty(self.model.get('proyecto_c'))){
        errors['proyecto_c'] = errors['proyecto_c'] || {};
        errors['proyecto_c'].required = true;
      }
      callback(null, fields, errors);
    },

    _doValidateRFC: function (fields, errors, callback) {
      var self = this;
      if(self.model.get('rfc_c')){
        var filter1 = [{rfc_c:self.model.get('rfc_c')}];
        var url = app.api.buildURL('Accounts', 'read',{},{filter:filter1});
        app.api.call('read', url, {}, {
          success: function(datacta){
            if(datacta.records.length > 0){
              errors['rfc_c'] = errors['rfc_c'] || {};
              errors['rfc_c']['Existe Cliente registrado con el mismo RFC, favor de ponerse en contacto con su Gerente de Ventas Comerciales.'] = true;
              callback(null, fields, errors);
              return;
            }
            var urlp = app.api.buildURL('Prospects', 'read',{},{filter:filter1});
            app.api.call('read', urlp, {}, {
              success: function(datap){
                debugger;
                if(datap.records.length > 0){
                  errors['rfc_c'] = errors['rfc_c'] || {};
                  errors['rfc_c']['Existe un Prospecto registrado con el mismo RFC, favor de ponerse en contacto con su Gerente de Ventas Comerciales.'] = true;
                }
                callback(null, fields, errors);
              }
            });
          }
        });
      }
      else{
        callback(null, fields, errors);
      }
    },

    _valida_otro_estado: function(){
      var self = this;
      if(self.getField('alt_address_state')){
        if(self.model.get('primary_address_state') === 'OTRO'){
          $('div[data-name="alt_address_state"]').removeClass('vis_action_hidden');
        }
        else {
          $('div[data-name="alt_address_state"]').addClass('vis_action_hidden');
        }
      }
    },

    // saveModel: function (success, error) {
    //     var self = this,
    //         options;
    //     options = {
    //         success: success,
    //         error: error,
    //         viewed: true,
    //         relate: (self.model.link) ? true : null,
    //         //Show alerts for this request
    //         showAlerts: {
    //             'process': true,
    //             'success': false,
    //             'error': false //error callback implements its own error handler
    //         },
    //         lastSaveAction: this.context.lastSaveAction
    //     };
    //     this.applyAfterCreateOptions(options);
    //
    //     // Check if this has subpanel create models
    //     if (this.hasSubpanelModels) {
    //         _.each(this.context.children, function(child) {
    //             if (child.get('isCreateSubpanel')) {
    //                 // create the child collection JSON structure to save
    //                 var childCollection = {
    //                     create: []
    //                 };
    //
    //                 // loop through the models in the collection and push each model's JSON
    //                 // data to the 'create' array
    //                 _.each(child.get('collection').models, function(model) {
    //                     childCollection.create.push(model.toJSON())
    //                 }, this)
    //
    //                 // set the child JSON collection data to the model
    //                 this.model.set(child.get('link'), childCollection);
    //             }
    //         }, this);
    //     }
    //
    //     options = _.extend({}, options, self.getCustomSaveOptions(options));
    //     debugger;
    //     self.model.save(null, options);
    // },

  })
