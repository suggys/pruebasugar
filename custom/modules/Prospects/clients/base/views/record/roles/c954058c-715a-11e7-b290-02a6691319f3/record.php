<?php
$viewdefs['Prospects'] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'record' =>
      array (
        'buttons' =>
        array (
          0 =>
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' =>
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 =>
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 =>
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' =>
            array (
              0 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 =>
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 =>
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 =>
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 =>
              array (
                'type' => 'divider',
              ),
              5 =>
              array (
                'type' => 'manage-subscription',
                'name' => 'manage_subscription_button',
                'label' => 'LBL_MANAGE_SUBSCRIPTIONS',
              ),
              6 =>
              array (
                'type' => 'vcard',
                'name' => 'vcard_button',
                'label' => 'LBL_VCARD_DOWNLOAD',
                'acl_action' => 'edit',
              ),
              7 =>
              array (
                'type' => 'divider',
              ),
              8 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              9 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'Prospects',
                'acl_action' => 'create',
              ),
              10 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:historical_summary_button:click',
                'name' => 'historical_summary_button',
                'label' => 'LBL_HISTORICAL_SUMMARY',
                'acl_action' => 'view',
              ),
              11 =>
              array (
                'type' => 'divider',
              ),
              12 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 =>
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' =>
        array (
          0 =>
          array (
            'name' => 'panel_header',
            'header' => true,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'size' => 'large',
                'dismiss_label' => true,
              ),
              1 =>
              array (
                'name' => 'full_name',
                'type' => 'fullname',
                'label' => 'LBL_NAME',
                'dismiss_label' => true,
                'fields' =>
                array (
                ),
                'span' => 12,
              ),
              2 =>
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'dismiss_label' => true,
              ),
              3 =>
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 =>
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labels' => true,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'folio_c',
                'label' => 'LBL_FOLIO',
                'readonly' => true,
              ),
              1 =>
              array (
                'name' => 'aprueba_oc_o_cot_c',
                'label' => 'LBL_APRUEBA_OC_O_COT',
              ),
              2 =>
              array (
                'name' => 'estado_c',
                'label' => 'LBL_ESTADO',
              ),
              3 =>
              array (
                'name' => 'estado_envio_pos_c',
                'label' => 'LBL_ESTADO_ENVIO_POS',
              ),
              4 =>
              array (
                'name' => 'id_pos_c',
                'label' => 'LBL_ID_POS',
                'readonly' => true,
              ),
              5 =>
              array (
                'name' => 'id_ar_credit_c',
                'label' => 'LBL_ID_AR_CREDIT',
                'readonly' => true,
              ),
              6 =>
              array (
                'name' => 'assigned_user_name',
                'studio' => 'visible',
                'readonly' => true,
              ),
              7 =>
              array (
                'name' => 'sucursal_c',
                'label' => 'LBL_SUCURSAL',
                'readonly' => true,
              ),
            ),
          ),
          2 =>
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_RECORD_SHOWMORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'tipo_de_persona_c',
                'label' => 'LBL_TIPO_DE_PERSONA',
              ),
              1 =>
              array (
                'name' => 'rfc_c',
                'label' => 'LBL_RFC',
              ),
              2 =>
              array (
                'name' => 'phone_work',
              ),
              3 => 'phone_mobile',
              4 => 'email',
              5 =>
              array (
                'name' => 'web_page_c',
                'label' => 'LBL_WEB_PAGE',
              ),
              6 =>
              array (
                'name' => 'nombre_representante_legal_c',
                'studio' => 'visible',
                'label' => 'LBL_NOMBRE_REPRESENTANTE_LEGAL',
              ),
              7 =>
              array (
                'name' => 'nombre_del_aval_c',
                'label' => 'LBL_NOMBRE_DEL_AVAL',
              ),
            ),
          ),
          3 =>
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'tipo_cliente_c',
                'label' => 'LBL_TIPO_CLIENTE',
                'span' => 12,
              ),
              1 =>
              array (
                'name' => 'giro_c',
                'label' => 'LBL_GIRO',
              ),
              2 =>
              array (
                'name' => 'otro_giro_c',
                'label' => 'LBL_OTRO_GIRO',
              ),
              3 =>
              array (
                'name' => 'oficio_c',
                'label' => 'LBL_OFICIO',
              ),
              4 =>
              array (
                'name' => 'otro_oficio_c',
                'label' => 'LBL_OTRO_OFICIO',
              ),
              5 =>
              array (
                'name' => 'metodo_pago_c',
                'label' => 'LBL_METODO_PAGO',
              ),
              6 =>
              array (
                'name' => 'subtipo_c',
                'label' => 'LBL_SUBTIPO',
              ),
              7 =>
              array (
                'name' => 'club_pro_c',
                'label' => 'LBL_CLUB_PRO_C',
              ),
              8 =>
              array (
                'name' => 'numero_club_pro_c',
                'label' => 'LBL_NUMERO_CLUB_PRO',
              ),
              9 =>
              array (
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'vol_ventas_anual_est_c',
                'label' => 'LBL_VOL_VENTAS_ANUAL_EST',
              ),
              10 =>
              array (
                'name' => 'competidores_directos_c',
                'studio' => 'visible',
                'label' => 'LBL_COMPETIDORES_DIRECTOS',
              ),
              11 =>
              array (
                'name' => 'cuenta_patron_c',
                'label' => 'LBL_CUENTA_PATRON',
              ),
              12 =>
              array (
              ),
            ),
          ),
          4 =>
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'primary_address_street',
                'comment' => 'The street address used for primary address',
                'label' => 'LBL_PRIMARY_ADDRESS_STREET',
              ),
              1 =>
              array (
                'name' => 'primary_address_number_c',
                'label' => 'LBL_PRIMARY_ADDRESS_NUMBER',
              ),
              2 =>
              array (
                'name' => 'primary_address_postalcode',
                'comment' => 'Postal code for primary address',
                'label' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
              ),
              3 =>
              array (
                'name' => 'primary_address_state',
                'comment' => 'State for primary address',
                'label' => 'LBL_PRIMARY_ADDRESS_STATE',
              ),
              4 =>
              array (
                'name' => 'alt_address_state',
                'comment' => 'State for alternate address',
                'label' => 'LBL_ALT_ADDRESS_STATE',
              ),
              5 =>
              array (
                'name' => 'primary_address_country',
                'comment' => 'Country for primary address',
                'label' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
              ),
              6 =>
              array (
                'name' => 'primary_address_city',
                'comment' => 'City for primary address',
                'label' => 'LBL_PRIMARY_ADDRESS_CITY',
              ),
              7 =>
              array (
                'name' => 'primary_address_colonia_c',
                'label' => 'LBL_PRIMARY_ADDRESS_COLONIA',
              ),
            ),
          ),
          5 =>
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'date_entered',
                'comment' => 'Date record created',
                'studio' =>
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_ENTERED',
              ),
              1 =>
              array (
                'name' => 'date_modified',
                'comment' => 'Date record last modified',
                'studio' =>
                array (
                  'portaleditview' => false,
                ),
                'readonly' => true,
                'label' => 'LBL_DATE_MODIFIED',
              ),
              2 =>
              array (
                'name' => 'tieneorden_c',
                'label' => 'LBL_TIENEORDEN',
                'readonly' => true,
              ),
              3 =>
              array (
              ),
            ),
          ),
        ),
        'templateMeta' =>
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
