({
  extendsFrom: 'RecordView',

  my_events: {
    'keyup input[name=primary_address_postalcode]': '_keypress_primary_address_postalcode',
    'keypress input[name=primary_address_postalcode]': '_keypress_event_primary_address_postalcode',
  },

  initialize: function (options) {
    var self = this;
    this._super("initialize", arguments);
    //this.model.on('change:assigned_user_name',_.bind(this._changeAssignedUserName, this));
    this.model.on('change:tipo_cliente_c',_.bind(this._changeTipoCliente, this));
    this.model.on('change:giro_c',_.bind(this._changeGiro, this));
    this.model.on('change:oficio_c',_.bind(this._changeOficio, this));
    this.model.addValidationTask('validateCP',_.bind(this._doValidateCP, this));
    this.model.addValidationTask('estado_c',_.bind(this._doValidateEstado_c, this));
    //this.model.addValidationTask('validatePhoneWork',_.bind(this._doValidatePhoneWork, this));
    this.model.addValidationTask('validatePhoneMobile',_.bind(this._doValidatePhoneMobile, this));
    this.model.addValidationTask('validateBeforeSave',_.bind(this._doValidateBeforeSave, this));
    this.model.addValidationTask('validateVendedor',_.bind(this._doValidateVendedor, this));
    this.model.addValidationTask('validateSucursal',_.bind(this._doValidateSucursal, this));
    this.model.addValidationTask('validateMetodoPago',_.bind(this._doValidateMetodoPago, this));
    this.model.addValidationTask('validateGiro',_.bind(this._doValidateGiro, this));
    this.model.addValidationTask('validateOtroGiro',_.bind(this._doValidateOtroGiro, this));
    this.model.addValidationTask('validateOficio',_.bind(this._doValidateOficio, this));
    this.model.addValidationTask('validateOtroOficio',_.bind(this._doValidateOtroOficio, this));
    self.model.on('sync', function(model, value){
      self._disableField();
      self.model.on('change:primary_address_state', _.bind(self._valida_otro_estado, self));
      self.deshabilitaElementos();
    });
  },

  _doValidateEstado_c: function (fields, errors, callback) {
    var self = this;
    if(self.model.get('rfc_c')){
      var filter1 = [
        {rfc_c:self.model.get('rfc_c')},
        {prospects_accounts_1prospects_ida:{$not_in:[self.model.id]}}
      ];
      var url = app.api.buildURL('Accounts', 'read',{},{filter:filter1});
      app.api.call('read', url, {}, {
        success: function(datacta){
          if(datacta.records.length > 0){
            errors['rfc_c'] = errors['rfc_c'] || {};
            errors['rfc_c']['Existe Cliente registrado con el mismo RFC, favor de ponerse en contacto con su Gerente de Ventas Comerciales.'] = true;
            callback(null, fields, errors);
            return;
          }
          filter1 = [
            {rfc_c:self.model.get('rfc_c')},
            {id:{$not_equals:self.model.id}}
          ];
          var urlp = app.api.buildURL('Prospects', 'read',{},{filter:filter1});
          app.api.call('read', urlp, {}, {
            success: function(datap){
              if(datap.records.length > 0){
                errors['rfc_c'] = errors['rfc_c'] || {};
                errors['rfc_c']['Existe un Prospecto registrado con el mismo RFC, favor de ponerse en contacto con su Gerente de Ventas Comerciales.'] = true;
              }
              callback(null, fields, errors);
            }
          });
        }
      });
    }
    else{
      callback(null, fields, errors);
    }
  },

  _keypress_event_primary_address_postalcode: function (event) {
    var controlKeys = [8, 9, 13, 35, 36, 37, 39];
    // IE doesn't support indexOf
    var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
    console.log(event.which);
    // Some browsers just don't raise events for control keys. Easy.
    // e.g. Safari backspace.
    if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
      (48 <= event.which && event.which <= 57) || // Always 1 through 9
      /*(48 == event.which && $(this).attr("value")) ||*/ // No 0 first digit
      isControlKey) { // Opera assigns values for control keys.
        return;
      } else {
        event.preventDefault();
      }
    },

    _keypress_primary_address_postalcode: function(evt) {
      var self = this;
      var postalCode = self.$el.find('input[name=primary_address_postalcode]').val() || '';
      var values = [];
      console.log(postalCode);
      if(String(postalCode).length == 5)
      {
        app.alert.show('datos_cp', {
          level: 'process',
          title: 'Cargando datos de direcci&oacute;n.',
          autoClose: false
        });
        var url = app.api.buildURL('Merxbp/Zipcode', 'read',{},{zipcode:postalCode});
        app.api.call('read', url, {}, {
          success: function(data){
            self._setModeAndDisablefieldsAddress('edit', false);
            self.model.set({'primary_address_country': 'MX'});
            if(data.Code == 200) {
              self.model.set({'primary_address_country': 'MX'});
              self.model.set({'primary_address_city': data.Address.Ciudad});
              self.model.set({'primary_address_state': data.Address.Abr_Estado});

              _.each(data.Address.Colonias, function(col){values.push({id: col.Colonia, text: col.Colonia});});
              values.push({id : 'Otra', text: 'Otra'});
              self.$el.find('input[name=primary_address_colonia_c]').select2({
                width:"100%",
                containerCssClass: 'select2-choices-pills-close',
                data: values
              });

              if(values.length == 2)
              {
                self.model.set({'primary_address_colonia_c': values[0].text});
                self.$el.find('input[name=primary_address_colonia_c]').val(values[0].text).trigger('change');
              }
            }
            app.alert.dismiss('datos_cp');
          }
        });
      } else {
        self._setModeAndDisablefieldsAddress('readonly', true);
        self._clearFieldsAddress();
      }
    },

    _clearFieldsAddress: function(){
      var self = this;
      self.model.set(
        {
          'primary_address_colonia_c': '',
          'primary_address_city': '',
          'primary_address_state': '',
          'alt_address_state': ''
        }
      );
    },

    _setModeAndDisablefieldsAddress: function(mode, disable){
      var self = this;
      self.getField('primary_address_colonia_c').setMode(mode);
      self.getField('primary_address_colonia_c').setDisabled(disable);

      self.getField('primary_address_city').setMode(mode);
      self.getField('primary_address_city').setDisabled(disable);

      self.getField('primary_address_country').setMode(mode);
      self.getField('primary_address_country').setDisabled(disable);

      self.getField('primary_address_state').setMode(mode);
      self.getField('primary_address_state').setDisabled(disable);

      self.getField('alt_address_state').setMode(mode);
      self.getField('alt_address_state').setDisabled(disable);
      // self.getField('otra_colonia_c').setMode(mode);
      // self.getField('otra_colonia_c').setDisabled(disable);
    },

    _valida_otro_estado: function(){
      var self = this;
      if(self.getField('alt_address_state')){
        if(self.model.get('primary_address_state') === 'OTRO'){
          $('div[data-name="alt_address_state"]').removeClass('vis_action_hidden');
        }
        else {
          $('div[data-name="alt_address_state"]').addClass('vis_action_hidden');
        }
      }
    },

    _disableField:function(disableFields){
      var self = this;
      if(self.model.get('estado_c') === 'convertido'){
        self.getField('last_name').setDisabled(true);
        self.getField('id_pos_c').setDisabled(true);
        self.getField('assigned_user_name').setDisabled(true);
        self.getField('sucursal_c').setDisabled(true);
        //self.getField('estado_cuenta_c').setDisabled(true);
        self.getField('tipo_de_persona_c').setDisabled(true);
        self.getField('rfc_c').setDisabled(true);
        self.getField('phone_work').setDisabled(true);
        self.getField('phone_mobile').setDisabled(true);
        self.getField('email').setDisabled(true);
        self.getField('web_page_c').setDisabled(true);
        self.getField('nombre_representante_legal_c').setDisabled(true);
        self.getField('nombre_del_aval_c').setDisabled(true);
        self.getField('primary_address_postalcode').setDisabled(true);
        self.getField('primary_address_country').setDisabled(true);
        self.getField('primary_address_state').setDisabled(true);
        self.getField('alt_address_state').setDisabled(true);
        self.getField('primary_address_city').setDisabled(true);
        self.getField('primary_address_colonia_c').setDisabled(true);
        self.getField('primary_address_street').setDisabled(true);
        self.getField('primary_address_number_c').setDisabled(true);

        self.getField('tipo_cliente_c').setDisabled(true);
        self.getField('giro_c').setDisabled(true);
        self.getField('otro_giro_c').setDisabled(true);
        self.getField('oficio_c').setDisabled(true);
        self.getField('otro_oficio_c').setDisabled(true);
        self.getField('metodo_pago_c').setDisabled(true);
        self.getField('subtipo_c').setDisabled(true);
        self.getField('club_pro_c').setDisabled(true);
        self.getField('numero_club_pro_c').setDisabled(true);
        //self.getField('vol_ventas_anual_est_c').setDisabled(true);
        self.getField('competidores_directos_c').setDisabled(true);
        self.getField('cuenta_patron_c').setDisabled(true);
      }
    },

    // _changeAssignedUserName: function (model, value) {
    //   var self = this;
    //   debugger;
    //   var idUser = self.model.get('assigned_user_id');
    //   var url_users = app.api.buildURL('Users/'+idUser);
    //   app.api.call('read', url_users, {}, {
    //     success: function(data){
    //       if(data){
    //         self.model.set({sucursal_c:data.sucursal_c});
    //       }
    //     }
    //   });
    // },

    _doValidateMetodoPago: function (fields, errors, callback) {
      var self = this;
      if(self.model.get('estado_c') === "convertido" && _.isEmpty(self.model.get('metodo_pago_c'))){
        errors['metodo_pago_c'] = errors['metodo_pago_c'] || {};
        errors['metodo_pago_c'].required = true;
        errors['metodo_pago_c']['Para convertir a cliente se necesita por lo menos un m\u00E9todo de pago.'] = true;
      }
      callback(null, fields, errors);
    },

    _doValidateCP: function (fields, errors, callback) {
      var self = this;
      var exp = /\d{5}/;
      if(self.model.get('primary_address_postalcode') && !exp.test(self.model.get('primary_address_postalcode'))){
        errors['primary_address_postalcode'] = errors['primary_address_postalcode'] || {};
        errors['primary_address_postalcode']['El c\u00F3digo postal debe de contener 5 d\u00EDgitos.'] = true;
      }
      callback(null, fields, errors);
    },

    _doValidatePhoneWork: function (fields, errors, callback) {
      var self = this;
      var exp = /\d{+13}/;
      if(self.model.get('phone_work') && !exp.test(self.model.get('phone_work'))){
        errors['phone_work'] = errors['phone_work'] || {};
        errors['phone_work']['El tel\u00E9fono de oficina debe contener 13 d\u00EDgitos.'] = true;
      }
      callback(null, fields, errors);
    },

    _doValidatePhoneMobile: function (fields, errors, callback) {
      var self = this;
      var exp = /\d{10}/;
      if(self.model.get('phone_mobile') && !exp.test(self.model.get('phone_mobile'))){
        errors['phone_mobile'] = errors['phone_mobile'] || {};
        errors['phone_mobile']['El tel\u00E9fono celular debe contener 10 d\u00EDgitos.'] = true;
      }
      callback(null, fields, errors);
    },

    _doValidateBeforeSave: function (fields, errors, callback){
      var self = this;
      var roles = app.user.get('roles');
      var estado = self.model.get('estado_c');
      var modelSynced = app.data.createBean(self.model.module, self.model.getSynced());
      var changedArray = app.utils.compareBeans(self.model, modelSynced);
      if(app.user.get('type') != 'admin'){
        if(
          modelSynced.get('estado_c') === "convertido" &&
          !_.contains(roles,"admin") &&
          !_.isEmpty(changedArray)
        ){
          errors['estado_c'] = errors['estado_c'] || {};
          errors['estado_c']['No se permite modificar, porque ya esta convertido a cliente.'] = true;
        }
      }
      callback(null, fields, errors);
    },

    _doValidateVendedor: function (fields, errors, callback) {
      var self = this;
      if(_.isEmpty(self.model.get('assigned_user_name'))){
        errors['assigned_user_name'] = errors['assigned_user_name'] || {};
        errors['assigned_user_name'].required = true;
      }
      callback(null, fields, errors);
    },

    _doValidateSucursal: function (fields, errors, callback) {
      var self = this;
      if(_.isEmpty(self.model.get('sucursal_c'))){
        errors['sucursal_c'] = errors['sucursal_c'] || {};
        errors['sucursal_c'].required = true;
      }
      callback(null, fields, errors);
    },

    _render: function (arguments) {
      var self = this;
      this._super('_render', arguments);
      this.delegateEvents(_.extend({}, this.events, this.my_events));
      if(!self.model.isNotEmpty){
        var filter = [{estado_c:{$is_null:true}}];
        var url = app.api.buildURL('Prospects/'+self.model.id+'/link/prospects_orden_ordenes_1?filter=', 'read',{},{filter:filter});
        app.api.call('read', url, {}, {
          success:function (data) {
            if(data.records.length == 0){
              // self.$el.find('div[data-panelname="LBL_RECORDVIEW_PANEL3"]').addClass('hidden');
              console.log("");
            }
            else {
              if(self.getField('otro_giro_c') && _.isEmpty(self.model.get('otro_giro_c'))){
                var codigoField = self.getField('otro_giro_c');
                codigoField.$el.closest('.record-cell').addClass('hidden');
              }
              if(self.getField('oficio_c') && _.isEmpty(self.model.get('oficio_c'))){
                var codigoField = self.getField('oficio_c');
                codigoField.$el.closest('.record-cell').addClass('hidden');
              }
              if(self.getField('otro_oficio_c') && _.isEmpty(self.model.get('otro_oficio_c'))){
                var codigoField = self.getField('otro_oficio_c');
                codigoField.$el.closest('.record-cell').addClass('hidden');
              }
            }
          }
        });
      }
    },

    deshabilitaElementos: function (argument) {
      var self = this;
      self._valida_otro_estado();
      if(self.model.get('aprueba_oc_o_cot_c')){
        var filter = {filter:[{estado_c:{$is_null:true}}]};
        var url = app.api.buildURL('Prospects/'+self.model.id+'/link/prospects_orden_ordenes_1?filter=', 'read',{},{filter:filter});
        app.api.call('read', url, {}, {
          success:function (data) {
            if(data.records.length == 0 ){
              if(self.getField('estado_c')){
                self.getField('estado_c').setMode('readonly');
                self.getField('estado_c').setDisabled(true);
              }
            }
          }
        });
      }
      else{
        if(self.getField('estado_c')){
          self.getField('estado_c').setMode('readonly');
          self.getField('estado_c').setDisabled(true);
        }
      }
    },

    _changeTipoCliente: function (model, value){
      var self = this;
      if(value === "especialista"){
        self.model.set({'otro_giro_c':''});
        if(self.getField('otro_giro_c')){
          var codigoField = self.getField('otro_giro_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
        if(self.getField('oficio_c')){
          var codigoField = self.getField('oficio_c');
          if(codigoField.$el.closest('.record-cell').hasClass('hidden')){
            self.model.set({'oficio_c':''});
            codigoField.$el.closest('.record-cell').removeClass('hidden');
          }
        }
      }

      if(value === "comercial" && !_.contains(self.model.get('giro_c'),"contratista")){
        self.model.set({'otro_giro_c':''});
        if(self.getField('otro_giro_c')){
          var codigoField = self.getField('otro_giro_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
        self.model.set({'oficio_c':''});
        if(self.getField('oficio_c')){
          var codigoField = self.getField('oficio_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
        self.model.set({'otro_oficio_c':''});
        if(self.getField('otro_oficio_c')){
          var codigoField = self.getField('otro_oficio_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
      }

      if(_.isEmpty(value)){
        self.model.set({'otro_giro_c':''});
        if(self.getField('otro_giro_c')){
          var codigoField = self.getField('otro_giro_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }

        self.model.set({'oficio_c':''});
        if(self.getField('oficio_c')){
          var codigoField = self.getField('oficio_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }

        self.model.set({'otro_oficio_c':''});
        if(self.getField('otro_oficio_c')){
          var codigoField = self.getField('otro_oficio_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
      }

    },

    _changeGiro: function (model, value){
      var self = this;
      if(_.contains(value,"otro")){
        if(self.getField('otro_giro_c')){
          var codigoField = self.getField('otro_giro_c');
          codigoField.$el.closest('.record-cell').removeClass('hidden');
        }
      }else{
        self.model.set({'otro_giro_c':''});
        if(self.getField('otro_giro_c')){
          var codigoField = self.getField('otro_giro_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
      }
      if(_.contains(value,"contratista")){
        if(self.getField('oficio_c')){
          var codigoField = self.getField('oficio_c');
          codigoField.$el.closest('.record-cell').removeClass('hidden');
        }
      }else{
        if(self.model.get('tipo_cliente_c') != "especialista"){
          self.model.set({'oficio_c':''});
          if(self.getField('oficio_c')){
            var codigoField = self.getField('oficio_c');
            codigoField.$el.closest('.record-cell').addClass('hidden');
          }
          self.model.set({'otro_oficio_c':''});
          if(self.getField('otro_oficio_c')){
            var codigoField = self.getField('otro_oficio_c');
            codigoField.$el.closest('.record-cell').addClass('hidden');
          }
        }
      }
    },

    _changeOficio: function (model, value){
      var self = this;
      if(_.contains(value,"otro")){
        if(self.getField('otro_oficio_c')){
          var codigoField = self.getField('otro_oficio_c');
          codigoField.$el.closest('.record-cell').removeClass('hidden');
        }
      }else{
        self.model.set({'otro_oficio_c':''});
        if(self.getField('otro_oficio_c')){
          var codigoField = self.getField('otro_oficio_c');
          codigoField.$el.closest('.record-cell').addClass('hidden');
        }
      }
    },

    _doValidateGiro: function (fields, errors, callback) {
      var self = this;
      if((self.getField('giro_c') && self.model.get('tipo_cliente_c') === "comercial") && _.isEmpty(self.model.get('giro_c'))){
        errors['giro_c'] = errors['giro_c'] || {};
        errors['giro_c'].required = true;
        errors['giro_c']['Seleccione al menos una opci\u00F3n'] = true;
      }
      callback(null, fields, errors);
    },

    _doValidateOtroGiro: function (fields, errors, callback) {
      var self = this;
      if((self.getField('otro_giro_c') && _.contains(self.model.get('giro_c'),"otro")) && _.isEmpty(self.model.get('otro_giro_c'))){
        errors['otro_giro_c'] = errors['otro_giro_c'] || {};
        errors['otro_giro_c'].required = true;
        errors['otro_giro_c']['Ingrese una descripci\u00F3n del giro'] = true;
      }
      callback(null, fields, errors);
    },

    _doValidateOficio: function (fields, errors, callback) {
      var self = this;
      if((self.getField('oficio_c') && self.model.get('tipo_cliente_c') === "especialista") && _.isEmpty(self.model.get('oficio_c'))){
        errors['oficio_c'] = errors['oficio_c'] || {};
        errors['oficio_c'].required = true;
        errors['oficio_c']['Seleccione al menos una opci\u00F3n'] = true;
      }

      if(self.getField('oficio_c') &&
      (
        self.model.get('tipo_cliente_c') === "comercial" &&
        _.contains(self.model.get('giro_c'),"contratista")
      )
      && _.isEmpty(self.model.get('oficio_c'))
    ){
      errors['oficio_c'] = errors['oficio_c'] || {};
      errors['oficio_c'].required = true;
      errors['oficio_c']['Seleccione al menos una opci\u00F3n'] = true;
    }
    callback(null, fields, errors);
  },

  _doValidateOtroOficio: function (fields, errors, callback) {
    var self = this;
    if((self.getField('otro_oficio_c') && _.contains(self.model.get('oficio_c'),"otro")) && _.isEmpty(self.model.get('otro_oficio_c'))){
      errors['otro_oficio_c'] = errors['otro_oficio_c'] || {};
      errors['otro_oficio_c'].required = true;
      errors['otro_oficio_c']['Ingrese una descripci\u00F3n del oficio'] = true;
    }
    callback(null, fields, errors);
  }
})
