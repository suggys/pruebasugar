<?php

use Sugarcrm\Sugarcrm\Util\Arrays\ArrayFunctions\ArrayFunctions;
include_once("include/workflow/alert_utils.php");
include_once("include/workflow/action_utils.php");
include_once("include/workflow/time_utils.php");
include_once("include/workflow/trigger_utils.php");
//BEGIN WFLOW PLUGINS
include_once("include/workflow/custom_utils.php");
//END WFLOW PLUGINS
	class Prospects_workflow {
	function process_wflow_triggers(& $focus){
		include("custom/modules/Prospects/workflow/triggers_array.php");
		include("custom/modules/Prospects/workflow/alerts_array.php");
		include("custom/modules/Prospects/workflow/actions_array.php");
		include("custom/modules/Prospects/workflow/plugins_array.php");
		if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if( ( !($focus->fetched_row['estado_c'] ==  stripslashes('convertido') )) && 
 (isset($focus->estado_c) && $focus->estado_c ==  stripslashes('convertido'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['a1b900ee_6f08_11e7_b0f0_080027b5ccaa'])){
		$triggeredWorkflows['a1b900ee_6f08_11e7_b0f0_080027b5ccaa'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = ""; 

	 $alertshell_array['source_type'] = "Normal Message"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['Prospects0_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 


	//end function process_wflow_triggers
	}

	//end class
	}

?>