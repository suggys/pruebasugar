<?php
class Calcula_Abono_Por_NC{
    public $isDebug = false;

    public function CalculaAbonoFacturaNC($bean, $event, $arguments){
      global $timedate, $current_user;
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        if($this->isDebug)$GLOBALS['log']->fatal('CalculaAbonoFacturaNC module::'. $arguments['module']);
        if($this->isDebug)$GLOBALS['log']->fatal('CalculaAbonoFacturaNC related_module::'. $arguments['related_module']);

        if((strcmp('NC_Notas_credito', $arguments['related_module'])==0)) {
            $this->fnCalculaAbonoFacturaNC($bean, $arguments);
        }

        if((strcmp('LF_Facturas', $arguments['related_module'])==0)) {
            $this->fnRelacionaFactFact($bean, $arguments);
        }

        if($arguments['related_module'] === 'Accounts'){
          $account = BeanFactory::getBean('Accounts', $arguments['related_id']);

          $facturaAux = BeanFactory::getBean('LF_Facturas', $arguments['id']);
          $facturaAux->fetch($bean->id);

          // $GLOBALS["log"]->fatal("agregando sucursal");
          if($facturaAux->dt_crt){
            $vencimiento = new SugarDateTime($facturaAux->dt_crt);
            $timedate->tzUser($vencimiento, $current_user);
            $vencimiento = $vencimiento->setTime(0,0);

            $vencimientoDateDb = $timedate->asDbDate($vencimiento, true);
            if($account->plazo_pago_autorizado_c){
              $vencimiento = $vencimiento->add(new DateInterval('P'.$account->plazo_pago_autorizado_c.'D'));
              $vencimientoDateDb = $timedate->asDbDate($vencimiento, true);
            }

            $this->updateVencimiento($bean, $vencimientoDateDb);
            $bean->fecha_vencimiento = $vencimientoDateDb;

            $vencimientoDateDb = $timedate->asDbDate($vencimiento, true);
            if($account->dias_tolerancia_c){
              $vencimiento = $vencimiento->add(new DateInterval('P'.$account->dias_tolerancia_c.'D'));
              $vencimientoDateDb = $timedate->asDbDate($vencimiento, true);
            }
            $this->updateVencimientoFlexible($bean, $vencimientoDateDb);
            $bean->fecha_vencimiento_flexible_c = $vencimientoDateDb;
          }
          $this->updateSucursal($bean, $account->id_sucursal_c);
          $bean->sucursal_cliente_c = $account->id_sucursal_c;
        }
    }

    public function fnRelacionaFactFact($bean, $arguments) {
        // $bean->refactura = 1;
        // $bean->save();
    }

    public function fnCalculaAbonoFacturaNC($bean, $arguments) {
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        if($this->isDebug)$GLOBALS['log']->fatal('CalculaAbonoFacturaNC related_id::'. $arguments['related_id']);

        //TODO: Solo se incrementa campo monto_total_nc, lo ideal sería recorrer las relaciones,
        //pero no haymanera de saber cuanto se le aplicó por cada nc
        $beanNC = BeanFactory::getBean('NC_Notas_credito', $arguments['related_id']);
        $credito_disponible= floatval($beanNC->saldo_pendiente_c) | floatval($beanNC->importe_total);

        if($this->isDebug)$GLOBALS['log']->fatal('CalculaAbonoFacturaNC credito_disponible::'. $credito_disponible);
        if($credito_disponible > 0) {
        	if($this->isDebug)$GLOBALS['log']->fatal('CalculaAbonoFacturaNC saldo::'. $bean->saldo);
        	$bean->monto_total_nc_c = $credito_disponible;
          $bean->save(false);
        }
    }

    public function updateVencimiento($bean, $fechaVencimiento){
      global $db;
      $query = "UPDATE lf_facturas SET fecha_vencimiento = '$fechaVencimiento' WHERE id = '$bean->id';";
      $result = $db->query($query);
    }

    public function updateSucursal($bean, $sucursal){
      global $db;
      $query = "UPDATE lf_facturas_cstm SET sucursal_cliente_c = '$sucursal' WHERE id_c = '$bean->id';";
      $result = $db->query($query);
    }

    public function updateVencimientoFlexible($bean, $fechaVencimientoFlexible){
      global $db;
      $query = "UPDATE lf_facturas_cstm SET fecha_vencimiento_flexible_c = '$fechaVencimientoFlexible' WHERE id_c = '$bean->id';";
      $result = $db->query($query);
    }
}
?>
