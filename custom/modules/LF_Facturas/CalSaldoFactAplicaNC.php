<?php
class CalSaldoFactAplicaNC{

    public $isDebug = true;
    public function CalSaldoFactAplicaNC($bean, $event, $arguments){
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        if(
            $arguments['module'] == "LF_Facturas"
            && $arguments['related_module'] == "NC_Notas_credito"
            && $arguments['link'] == "nc_notas_credito_lf_facturas_1"
        ){
            $bean->retrieve();
            $account = BeanFactory::getBean("Accounts",$bean->accounts_lf_facturas_1accounts_ida);
            $notaCredito = BeanFactory::getBean("NC_Notas_credito",$arguments['related_id']);

            if(
              isset($notaCredito->id)
              && !empty($account->id)
            ){
                $notaCredito->retrieve();
                $saldoPendienteNota = $notaCredito->saldo_pendiente_c;

                $saldoFactura = $bean->saldo;
                $abono = ($bean->saldo >= $notaCredito->saldo_pendiente_c) ? $notaCredito->saldo_pendiente_c : $bean->saldo;

                $account->saldo_deudor_c -= $abono;
                $account->save(false);

                $bean->abono += $abono;
                $bean->saldo -= $abono;
                $bean->save();
                $notaCredito->saldo_aplicado_c += $abono;
                $notaCredito->saldo_pendiente_c -= $abono;
                $notaCredito->save(false);
            }
        }

    }
}


?>
