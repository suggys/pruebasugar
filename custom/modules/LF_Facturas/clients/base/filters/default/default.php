<?php
// created: 2017-10-09 18:39:21
$viewdefs['LF_Facturas']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'tipo_documento' => 
    array (
    ),
    'no_ticket' => 
    array (
    ),
    'accounts_lf_facturas_1_name' => 
    array (
    ),
    'estado' => 
    array (
    ),
    'estado_tiempo' => 
    array (
    ),
    'refactura' => 
    array (
    ),
    'serie' => 
    array (
    ),
    'uuid' => 
    array (
    ),
    'dt_crt' => 
    array (
    ),
    'fecha_vencimiento' => 
    array (
    ),
    'dias_vencimiento' => 
    array (
    ),
    'fecha_entrega' => 
    array (
    ),
    'linea_anticipo_c' => 
    array (
    ),
    'importado_c' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'importadode_c' => 
    array (
    ),
  ),
);