<?php
$module_name = 'LF_Facturas';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'LF_Facturas',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'tipo_documento',
                'label' => 'LBL_TIPO_DOCUMENTO',
              ),
              1 => 
              array (
                'name' => 'dt_crt',
                'label' => 'LBL_DT_CRT',
              ),
              2 => 
              array (
                'name' => 'no_ticket',
                'label' => 'LBL_NO_TICKET',
              ),
              3 => 
              array (
                'name' => 'lf_facturas_orden_ordenes_name',
              ),
              4 => 
              array (
                'name' => 'uuid',
                'label' => 'LBL_UUID',
                'span' => 6,
              ),
              5 => 
              array (
                'name' => 'serie',
                'label' => 'LBL_SERIE',
                'span' => 6,
              ),
              6 => 
              array (
                'name' => 'estado',
                'label' => 'LBL_ESTADO',
              ),
              7 => 
              array (
                'name' => 'estado_tiempo',
                'label' => 'LBL_ESTADO_TIEMPO',
                'readonly' => true,
              ),
              8 => 
              array (
                'name' => 'refactura',
                'label' => 'LBL_REFACTURA',
              ),
              9 => 
              array (
                'name' => 'fecha_entrega',
                'label' => 'LBL_FECHA_ENTREGA',
              ),
              10 => 
              array (
                'name' => 'fecha_vencimiento',
                'label' => 'LBL_FECHA_VENCIMIENTO',
                'readonly' => true,
              ),
              11 => 
              array (
                'name' => 'dias_vencidos_sin_tol_c',
                'label' => 'LBL_DIAS_VENCIDOS_SIN_TOL',
              ),
              12 => 
              array (
                'name' => 'fecha_vencimiento_flexible_c',
                'label' => 'LBL_FECHA_VENCIMIENTO_FLEXIBLE',
              ),
              13 => 
              array (
                'name' => 'dias_vencimiento',
                'label' => 'LBL_DIAS_VENCIMIENTO',
                'readonly' => true,
              ),
              14 => 
              array (
                'name' => 'nombre_emisor',
                'label' => 'LBL_NOMBRE_EMISOR',
              ),
              15 => 
              array (
                'name' => 'rfc_emisor',
                'label' => 'LBL_RFC_EMISOR',
              ),
              16 => 
              array (
                'name' => 'name_receptor',
                'label' => 'LBL_NAME_RECEPTOR',
              ),
              17 => 
              array (
                'name' => 'rfc_receptor',
                'label' => 'LBL_RFC_RECEPTOR',
              ),
              18 => 
              array (
                'name' => 'accounts_lf_facturas_1_name',
                'label' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE',
              ),
              19 => 
              array (
                'name' => 'lf_facturas_lf_facturas_name',
                'label' => 'LBL_FACTURA_PADRE',
              ),
              20 => 
              array (
                'name' => 'lowes_pagos_lf_facturas_name',
                'label' => 'LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LOWES_PAGOS_TITLE',
                'readonly' => true,
              ),
              21 => 
              array (
                'name' => 'nc_notas_credito_lf_facturas_1_name',
                'label' => 'LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE',
              ),
              22 => 
              array (
                'name' => 'lowes_pagos_lf_facturas_1_name',
                'label' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE',
              ),
              23 => 
              array (
              ),
              24 => 
              array (
                'name' => 'linea_anticipo_c',
                'label' => 'LBL_LINEA_ANTICIPO',
                'readonly' => true,
              ),
              25 => 
              array (
              ),
              26 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
              ),
              1 => 
              array (
                'name' => 'sub_total',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_SUB_TOTAL',
              ),
              2 => 
              array (
              ),
              3 => 
              array (
                'name' => 'iva',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_IVA',
              ),
              4 => 
              array (
              ),
              5 => 
              array (
                'name' => 'importe',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_IMPORTE',
              ),
              6 => 
              array (
                'name' => 'saldo',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_SALDO',
                'readonly' => true,
              ),
              7 => 
              array (
                'name' => 'abono',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_ABONO',
                'readonly' => true,
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'collapsed',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              1 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              2 => 
              array (
                'name' => 'importado_c',
                'label' => 'LBL_IMPORTADO_C',
                'span' => 12,
              ),
              3 =>
              array (
                'name' => 'importadode_c',
                'label' => 'LBL_IMPORTADODE',
                'readonly' => true,
              ),
              4 =>
              array (
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
