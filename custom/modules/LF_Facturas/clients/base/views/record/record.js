({
	extendsFrom:"RecordView",

	initialize: function () {
        var self = this;
		this._super("initialize", arguments);
		self.model.on('sync', function(model, value){
            self._disableFields();
        });
        self.model.addValidationTask('change:lf_facturas_lf_facturas_name', _.bind(self._doValidateRelFactura, self));
	},
	_disableFields:function(){
        var self = this;
        var field = self.getField('accounts_lf_facturas_1_name');
        if(field)
         	self.model.get('tipo_documento') === 'factura' && field.setDisabled('true');

        field = self.getField('lf_facturas_orden_ordenes_name');
		if(field)
        	field.setDisabled('true');

        field = self.getField('tipo_documento');
        if(field)
            field.setDisabled('true');
    },
    _doValidateRelFactura: function(fields, errors, callback) {
    //console.log('----->');
    var self = this;
    self.fac_cta1 = self.model.get('accounts_lf_facturas_1accounts_ida');
    self.fac_rel = self.model.get('lf_facturas_lf_facturaslf_facturas_ida');
        if(self.model){
            //var modelSynced = app.data.createBean(self.model.module, self.model.getSynced());
            //var changedArray = app.utils.compareBeans(self.model, modelSynced);
            var filter1 = [{id:self.fac_rel}];
            var url = app.api.buildURL('LF_Facturas', 'read',{},{filter:filter1});
            self.setearFact = true;
            app.api.call('read', url, {}, {
                    success: function(dataf){

                        self._doValidateFactRel(dataf, function (MismaCta) {
                        //console.log('Rel-----');
                        if(!MismaCta && self.fac_rel){
                                self.setearFact = false;
                                errors['lf_facturas_lf_facturas_name'] = errors['lf_facturas_lf_facturas_name'] || {};
                                errors['lf_facturas_lf_facturas_name']['La factura no esta relacionada a una cuenta o no esta relacionada a la misma cuenta.'] = true;
                            }
                            else{
                                //debugger;
                                self.setearFact = true;

                                if(self.setearFact && self.model.get('lf_facturas_lf_facturaslf_facturas_ida')){
                                    self.model.set('refactura', '1');
                                    var fact_setear = app.data.createBean('LF_Facturas',{"id":self.model.get('lf_facturas_lf_facturaslf_facturas_ida')});
                                    fact_setear.fetch({
                                            success: _.bind(function() {
                                                if(fact_setear.get('accounts_lf_facturas_1accounts_ida') == self.fac_cta1){
                                                    fact_setear.set('refactura', '1');
                                                    fact_setear.save();
                                                }
                                            }, this)
                                        });
                                }
                                else{
                                    self.model.set('refactura', '0');
                                }

                            }
                        callback(null, fields, errors);
                        })



                    }
                });

        }
        else
        {
            callback(null, fields, errors);
        }

    },

    _doValidateFactRel: function(data, callback) {
        var MismaCta = false;
        console.log('Num:'+ data.records.length);
        if(!data.records.length){
            MismaCta = false;
        }
        else{
            //debugger;
            var FactData = data.records[0];
            if(this.model.get('accounts_lf_facturas_1accounts_ida') == FactData.accounts_lf_facturas_1accounts_ida){
                MismaCta = true;
            }
        }

        callback(MismaCta);
      },

    _renderHtml: function (argument) {
        var self = this;
        this._super('_renderHtml', arguments);
    },
})
