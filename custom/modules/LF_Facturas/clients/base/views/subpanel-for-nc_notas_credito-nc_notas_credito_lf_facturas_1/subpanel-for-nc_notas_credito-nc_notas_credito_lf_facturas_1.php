<?php
// created: 2017-01-25 23:06:01
$viewdefs['LF_Facturas']['base']['view']['subpanel-for-nc_notas_credito-nc_notas_credito_lf_facturas_1'] = array (
  'panels' =>
  array (
    0 =>
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array (
        0 =>
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
          'readonly' => true,
        ),
        1 =>
        array (
          'name' => 'tipo_documento',
          'label' => 'LBL_TIPO_DOCUMENTO',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
        ),
        2 =>
        array (
          'name' => 'dt_crt',
          'label' => 'LBL_DT_CRT',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
        ),
        3 =>
        array (
          'name' => 'importe',
          'label' => 'LBL_IMPORTE',
          'enabled' => true,
          'related_fields' =>
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
          'readonly' => true,
        ),
        4 =>
        array (
          'name' => 'saldo',
          'label' => 'LBL_SALDO',
          'enabled' => true,
          'related_fields' =>
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
          'readonly' => true,
        ),
        5 =>
        array (
          'name' => 'abono',
          'label' => 'LBL_ABONO',
          'enabled' => true,
          'related_fields' =>
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
          'readonly' => true,
        ),
        6 =>
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' =>
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);
