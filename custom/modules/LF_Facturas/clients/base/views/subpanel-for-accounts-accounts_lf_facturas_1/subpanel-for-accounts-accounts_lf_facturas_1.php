<?php
// created: 2016-11-15 23:02:38
$viewdefs['LF_Facturas']['base']['view']['subpanel-for-accounts-accounts_lf_facturas_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'estado',
          'label' => 'LBL_ESTADO',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'serie',
          'label' => 'LBL_SERIE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'tipo_documento',
          'label' => 'LBL_TIPO_DOCUMENTO',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sub_total',
          'label' => 'LBL_SUB_TOTAL',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'iva',
          'label' => 'LBL_IVA',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'importe',
          'label' => 'LBL_IMPORTE',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'abono',
          'label' => 'LBL_ABONO',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        8 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);