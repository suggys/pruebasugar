({
	extendsFrom:"CreateView",

	initialize: function () {
    var self = this;
		this._super("initialize", arguments);
		this.model.addValidationTask('change:lf_facturas_lf_facturas_name', _.bind(this._doValidateRelFactura, this));
		this.model.on('change:tipo_documento', _.bind(this._changeTipoDocumento, this));
        this.model.addValidationTask('validateCta',_.bind(this._doValidateCta, this));
        this.model.addValidationTask('Subtotal',_.bind(this._doValidateSubtotal, this));
        this.model.addValidationTask('iva',_.bind(this._doValidateIva, this));
        this.model.addValidationTask('importe',_.bind(this._doValidateImporte, this));


        // "Cuenta Relacionada", "Fecha de Emisión", "Subtotal", "IVA", "Importe Total"
 	},

    _doValidateImporte: function (fields, errors, callback) {
        var self = this;
        if( (self.model.get('importe') <= 0 || self.model.get('importe') == '') && self.model.get('tipo_documento') === 'nota_de_cargo'){
            errors['importe'] = errors['importe'] || {};
            errors['importe']['Debe de tener importe.'] = true;
        }
        callback(null, fields, errors);
    },

    _doValidateIva: function (fields, errors, callback) {
        var self = this;
        if( (self.model.get('iva') <= 0 || self.model.get('iva') == '') && self.model.get('tipo_documento') === 'nota_de_cargo'){
            errors['iva'] = errors['iva'] || {};
            errors['iva']['Debe de tener IVA.'] = true;
        }
        callback(null, fields, errors);
    },

    _doValidateSubtotal: function (fields, errors, callback) {
        var self = this;
        if( (self.model.get('sub_total') <= 0 || self.model.get('sub_total') == '') && self.model.get('tipo_documento') === 'nota_de_cargo'){
            errors['sub_total'] = errors['sub_total'] || {};
            errors['sub_total']['Debe de tener Subtotal.'] = true;
        }
        callback(null, fields, errors);
    },
    _doValidateCta: function (fields, errors, callback) {
        var self = this;
        if( _.isEmpty(self.model.get('accounts_lf_facturas_1_name'))  && self.model.get('tipo_documento') === 'nota_de_cargo'){
            errors['accounts_lf_facturas_1_name'] = errors['accounts_lf_facturas_1_name'] || {};
            errors['accounts_lf_facturas_1_name']['Debe de seleccionar una cuenta.'] = true;
        }
        callback(null, fields, errors);
    },
  _doValidateRelFactura: function(fields, errors, callback) {
    var self = this;
    self.fac_cta1 = self.model.get('accounts_lf_facturas_1accounts_ida');
    self.fac_rel = self.model.get('lf_facturas_lf_facturaslf_facturas_ida');
        if(self.model){
            var filter1 = [{id:self.fac_rel}];
            var url = app.api.buildURL('LF_Facturas', 'read',{},{filter:filter1});
            self.setearFact = true;
            app.api.call('read', url, {}, {
                    success: function(dataf){
                        self._doValidateFactRel(dataf, function (MismaCta) {
                        if(!MismaCta && self.fac_rel){
                                self.setearFact = false;
                                errors['lf_facturas_lf_facturas_name'] = errors['lf_facturas_lf_facturas_name'] || {};
                                errors['lf_facturas_lf_facturas_name']['La factura no esta relacionada a una cuenta o no esta relacionada a la misma cuenta.'] = true;
                            }
                            else{
                                self.setearFact = true;
                            }
                        callback(null, fields, errors);
                        })

                        if(self.setearFact && self.model.get('lf_facturas_lf_facturaslf_facturas_ida')){
                            var fact_setear = app.data.createBean('LF_Facturas',{"id":self.model.get('lf_facturas_lf_facturaslf_facturas_ida')});
                            fact_setear.fetch({
                                    success: _.bind(function() {
                                        if(fact_setear.get('accounts_lf_facturas_1accounts_ida') == self.fac_cta1){
                                            fact_setear.set('refactura', '1');
                                            fact_setear.save();
                                        }
                                    }, this)
                                });
                        }
                    }
                });
        }
    },

    _doValidateFactRel: function(data, callback) {
        var MismaCta = false;
        if(!data.records.length){
            MismaCta = false;
        }
        else{
            var FactData = data.records[0];
            if(this.model.get('accounts_lf_facturas_1accounts_ida') == FactData.accounts_lf_facturas_1accounts_ida){
                MismaCta = true;
            }
        }

        callback(MismaCta);
      },

    //actualización DRI del 26 sep 2016
	_renderHtml: function (argument) {
        var self = this;
		this._super("_renderHtml", arguments);
		this._disableFields();
	},

	_disableFields: function (){
		var self = this;

        var field2 = self.getField('accounts_lf_facturas_1_name');
            if(self.model.get('tipo_documento') === "factura"){

                field2.setDisabled('true');

            }

        var field = self.getField('lf_facturas_orden_ordenes_name');
    		if(field)
            	field.setDisabled('true');
	},

		_changeTipoDocumento: function(model, value) {
            var self = this;
            var codigoField = self.getField('accounts_lf_facturas_1_name');
            var recordCell = this.$el.find('div.record-label[data-name=lf_facturas_orden_ordenes_name]').closest('.record-cell');
            if(value === "factura"){
                this.model.set('')
                recordCell.addClass('hide');
                codigoField && codigoField.setDisabled('true');
            }
            else{
                recordCell.removeClass('hide');
                 codigoField && codigoField.setDisabled(false);

            }
        }

})
