({
    extendsFrom: 'SubpanelListView',

	initialize: function(options) {
		this._super('initialize',[options]);
	},
	_initializeMetadata : function(options) {
        var tmp = this._super('_initializeMetadata', arguments);
        if( this._hideRowActions(this.options.context.parent.get('module'))) {
        	tmp.rowactions.actions = _.without(tmp.rowactions.actions, _.findWhere(tmp.rowactions.actions, {
                type: "unlink-action"
            }));
        }
        if( this._hideEditAction(this.options.context.parent.get('module'))){
           tmp.rowactions.actions = _.without(tmp.rowactions.actions, _.findWhere(tmp.rowactions.actions, {
              acl_action: "edit"
           }));
        }
        return tmp;
    },
    _hideRowActions: function(link){
        var hideActions = false;
        if( link == "Accounts" || link == "orden_Ordenes" ){
            hideActions = true;
        }
        return hideActions;
    },
    _hideEditAction: function(link){
      var hideEditAction = false;
      if(
         link == "Accounts"
         || link == "orden_Ordenes"
         || link == "LF_Facturas"
         || link == "NC_Notas_credito"
         || link == "lowes_Pagos"){
            hideEditAction = true;
      }
      return hideEditAction;
   }
})
