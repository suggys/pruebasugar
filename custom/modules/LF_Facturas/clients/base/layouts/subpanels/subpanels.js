({
    extendsFrom: 'SubpanelsLayout',

    initialize: function (options) {
        this._super('initialize',arguments);
    },
    showSubpanel: function (linkName) {
        var self = this;
        _.each(self._components, function (component) {
            var link = component.context.get('link');
            if(self._hideButtons(link)){
                component.$el.find('a[class="rowaction btn"]').hide();
            }
        });
    },
    _hideButtons: function(link) {
        var self = this;
        var response = false;
        if( link === 'lf_facturas_lf_facturas' )
        {
            response = true;
        }
        return response;
    },

})
