<?php
class LHAsignaSaldoInicialFactura {
	private $isDebug = false;
    public function fnCopiaSaldoFactura($bean, $event, $arguments) {
    	if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
    	if($this->isDebug)$GLOBALS['log']->fatal('*********** fnCopiaSaldoFactura ***********');
    	if(!$arguments['isUpdate']) {
    		if($this->isDebug)$GLOBALS['log']->fatal('fnCopiaSaldoFactura name:'.$bean->name);
    		$bean->saldo = $bean->importe;
    		if($this->isDebug)$GLOBALS['log']->fatal('fnCopiaSaldoFactura calculado:'.$bean->saldo);
    	}
    }
}
?>
