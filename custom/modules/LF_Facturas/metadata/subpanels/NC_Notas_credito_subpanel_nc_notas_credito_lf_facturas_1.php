<?php
// created: 2017-01-25 23:05:27
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'tipo_documento' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TIPO_DOCUMENTO',
    'width' => '10%',
  ),
  'dt_crt' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DT_CRT',
    'width' => '10%',
    'default' => true,
  ),
  'importe' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_IMPORTE',
    'currency_format' => true,
    'width' => '10%',
  ),
  'saldo' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_SALDO',
    'currency_format' => true,
    'width' => '10%',
  ),
  'abono' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_ABONO',
    'currency_format' => true,
    'width' => '10%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
);