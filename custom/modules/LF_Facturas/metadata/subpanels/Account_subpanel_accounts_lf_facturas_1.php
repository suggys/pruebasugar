<?php
// created: 2016-11-15 23:02:35
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'estado' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ESTADO',
    'width' => '10%',
  ),
  'serie' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SERIE',
    'width' => '10%',
  ),
  'tipo_documento' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TIPO_DOCUMENTO',
    'width' => '10%',
  ),
  'sub_total' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_SUB_TOTAL',
    'currency_format' => true,
    'width' => '10%',
  ),
  'iva' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_IVA',
    'currency_format' => true,
    'width' => '10%',
  ),
  'importe' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_IMPORTE',
    'currency_format' => true,
    'width' => '10%',
  ),
  'abono' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_ABONO',
    'currency_format' => true,
    'width' => '10%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
);