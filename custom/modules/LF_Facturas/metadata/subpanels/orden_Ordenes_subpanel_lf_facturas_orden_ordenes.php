<?php
// created: 2016-12-15 15:42:33
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'tipo_documento' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_TIPO_DOCUMENTO',
    'width' => '10%',
  ),
  'sub_total' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_SUB_TOTAL',
    'currency_format' => true,
    'width' => '10%',
  ),
  'iva' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_IVA',
    'currency_format' => true,
    'width' => '10%',
  ),
  'importe' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_IMPORTE',
    'currency_format' => true,
    'width' => '10%',
  ),
  'dt_crt' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DT_CRT',
    'width' => '10%',
    'default' => true,
  ),
  'fecha_vencimiento' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_FECHA_VENCIMIENTO',
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
);