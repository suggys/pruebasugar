<?php
$popupMeta = array (
    'moduleMain' => 'LF_Facturas',
    'varName' => 'LF_Facturas',
    'orderBy' => 'lf_facturas.name',
    'whereClauses' => array (
  'name' => 'lf_facturas.name',
  'tipo_documento' => 'lf_facturas.tipo_documento',
  'no_ticket' => 'lf_facturas.no_ticket',
  'accounts_lf_facturas_1_name' => 'lf_facturas.accounts_lf_facturas_1_name',
  'estado' => 'lf_facturas.estado',
  'estado_tiempo' => 'lf_facturas.estado_tiempo',
  'refactura' => 'lf_facturas.refactura',
  'serie' => 'lf_facturas.serie',
  'uuid' => 'lf_facturas.uuid',
  'dt_crt' => 'lf_facturas.dt_crt',
  'fecha_vencimiento' => 'lf_facturas.fecha_vencimiento',
  'dias_vencimiento' => 'lf_facturas.dias_vencimiento',
  'fecha_entrega' => 'lf_facturas.fecha_entrega',
  'importado_c' => 'lf_facturas_cstm.importado_c',
  'date_modified' => 'lf_facturas.date_modified',
  'date_entered' => 'lf_facturas.date_entered',
  'favorites_only' => 'lf_facturas.favorites_only',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'tipo_documento',
  5 => 'no_ticket',
  6 => 'accounts_lf_facturas_1_name',
  7 => 'estado',
  8 => 'estado_tiempo',
  9 => 'refactura',
  10 => 'serie',
  11 => 'uuid',
  12 => 'dt_crt',
  13 => 'fecha_vencimiento',
  14 => 'dias_vencimiento',
  15 => 'fecha_entrega',
  16 => 'importado_c',
  17 => 'date_modified',
  18 => 'date_entered',
  19 => 'favorites_only',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'tipo_documento' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TIPO_DOCUMENTO',
    'width' => '10%',
    'name' => 'tipo_documento',
  ),
  'no_ticket' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NO_TICKET',
    'width' => '10%',
    'name' => 'no_ticket',
  ),
  'accounts_lf_facturas_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_LF_FACTURAS_1ACCOUNTS_IDA',
    'width' => '10%',
    'name' => 'accounts_lf_facturas_1_name',
  ),
  'estado' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_ESTADO',
    'width' => '10%',
    'name' => 'estado',
  ),
  'estado_tiempo' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_ESTADO_TIEMPO',
    'width' => '10%',
    'name' => 'estado_tiempo',
  ),
  'refactura' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_REFACTURA',
    'width' => '10%',
    'name' => 'refactura',
  ),
  'serie' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SERIE',
    'width' => '10%',
    'name' => 'serie',
  ),
  'uuid' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_UUID',
    'width' => '10%',
    'name' => 'uuid',
  ),
  'dt_crt' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DT_CRT',
    'width' => '10%',
    'name' => 'dt_crt',
  ),
  'fecha_vencimiento' => 
  array (
    'type' => 'date',
    'label' => 'LBL_FECHA_VENCIMIENTO',
    'width' => '10%',
    'name' => 'fecha_vencimiento',
  ),
  'dias_vencimiento' => 
  array (
    'type' => 'int',
    'label' => 'LBL_DIAS_VENCIMIENTO',
    'width' => '10%',
    'name' => 'dias_vencimiento',
  ),
  'fecha_entrega' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_FECHA_ENTREGA',
    'width' => '10%',
    'name' => 'fecha_entrega',
  ),
  'importado_c' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_IMPORTADO_C',
    'width' => '10%',
    'name' => 'importado_c',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'name' => 'date_modified',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'name' => 'date_entered',
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'TIPO_DOCUMENTO' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_TIPO_DOCUMENTO',
    'width' => '10%',
  ),
  'ACCOUNTS_LF_FACTURAS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_LF_FACTURAS_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'LF_FACTURAS_ORDEN_ORDENES_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
    'id' => 'LF_FACTURAS_ORDEN_ORDENESORDEN_ORDENES_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'ESTADO' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_ESTADO',
    'width' => '10%',
  ),
  'ESTADO_TIEMPO' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_ESTADO_TIEMPO',
    'width' => '10%',
  ),
  'DT_CRT' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DT_CRT',
    'width' => '10%',
    'default' => true,
  ),
  'FECHA_VENCIMIENTO' => 
  array (
    'type' => 'date',
    'label' => 'LBL_FECHA_VENCIMIENTO',
    'width' => '10%',
    'default' => true,
  ),
  'SUB_TOTAL' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_SUB_TOTAL',
    'currency_format' => true,
    'width' => '10%',
    'name' => 'sub_total',
  ),
  'IVA' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_IVA',
    'currency_format' => true,
    'width' => '10%',
    'name' => 'iva',
  ),
  'IMPORTE' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_IMPORTE',
    'currency_format' => true,
    'width' => '10%',
    'name' => 'importe',
  ),
  'ABONO' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_ABONO',
    'currency_format' => true,
    'width' => '10%',
    'name' => 'abono',
  ),
  'SALDO' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_SALDO',
    'currency_format' => true,
    'width' => '10%',
    'name' => 'saldo',
  ),
  'RFC_RECEPTOR' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_RFC_RECEPTOR',
    'width' => '10%',
  ),
  'RFC_EMISOR' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_RFC_EMISOR',
    'width' => '10%',
  ),
),
);
