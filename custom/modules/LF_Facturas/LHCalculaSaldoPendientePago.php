<?php
require_once "custom/modules/Accounts/CalculaSaldoCuentaPendiente.php";

class LHCalculaSaldoPendientePago{
    public $isDebug = false;

    public function fnCalculaSaldoPendienteAplicarDesvincular($bean, $event, $arguments) {
        //$GLOBALS['log']->fatal('>>>>>after_relationship_delete de Facturas');
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        if(strcmp('Low01_Pagos_Facturas', $arguments['related_module'])==0
            && strcmp($arguments['module'], 'LF_Facturas') ==0
            && isset($arguments['related_id'])
            ) {
                $beanPago = BeanFactory::newBean('Low01_Pagos_Facturas');
                $beanPago->fetch($arguments['related_id']);

                $bean->saldo += $beanPago->importe;
                $bean->abono -= $beanPago->importe;

                if($bean->saldo === 0){
                  $bean->estado_tiempo = 'pagada';
                }
                else{
                  $bean->estado_tiempo = 'vigente';
                }

                $bean->save(false);

                $this->fnCalculaSaldoDeudor($bean, $beanPago->importe, 1);
            }
        //$GLOBALS['log']->setLevel('debug');
        //_ppl(' P A G O S U.U ');
    }

    public function fnCalculaSaldoPendienteAplicar($bean, $event, $arguments){
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        //_ppl("C a l c u l a r ".$bean->tag)
        if(strcmp('Low01_Pagos_Facturas', $arguments['related_module'])==0
            && strcmp($arguments['module'], 'LF_Facturas') ==0
            && isset($arguments['related_id'])
            ) {
            if($bean->saldo > 0) {
                $beanPago = BeanFactory::getBean('Low01_Pagos_Facturas', $arguments['related_id']);
                if($beanPago->id) {
                
                      $pago = $beanPago->importe;
                      $bean->saldo -= $pago;
                      $bean->abono += $pago;
                      
                      if($bean->saldo == 0){
                        $bean->estado_tiempo = "pagada";
                      }else{
                        $bean->estado_tiempo = "vigente";
                      }
                      
                      $bean->save(false);

                      $this->fnCalculaSaldoDeudor($bean, $pago, 0);
                }
            }
        }
        //$GLOBALS['log']->setLevel('debug');
        //_ppl(' P E N D I E N T E S ');
    }

    Public function fnCalculaSaldoPendiente($bean) {
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        //$GLOBALS['log']->setLevel('debug');
        //_ppl(' P E N D I E N T E S D O S');
    }

    public function fnCalculaSaldoDeudor($bean, $abono, $suma) {
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }

        if(isset($bean->accounts_lf_facturas_1accounts_ida)) {
            $beanAccount = BeanFactory::getBean('Accounts', $bean->accounts_lf_facturas_1accounts_ida);
            if($beanAccount->id)
            {
              if($suma){
                $beanAccount->saldo_deudor_c += $abono;
                $beanAccount->saldo_pendiente_aplicar_c += $abono;
              }
              else{
                $beanAccount->saldo_deudor_c -= $abono;
                $beanAccount->saldo_pendiente_aplicar_c -= $abono;
              }
              /*_ppl(" D E U D O R ");
              $GLOBALS["log"]->fatal("Importado ".$bean->importado_c);
              $GLOBALS["log"]->fatal("saldo_deudor:".$beanAccount->saldo_deudor_c);
              $GLOBALS["log"]->fatal("saldo_pendiente:".$beanAccount->saldo_pendiente_aplicar_c);*/
              
              // $GLOBALS["log"]->fatal("fnCalculaSaldoDeudor:estado_bloqueo_c:".$beanAccount->estado_bloqueo_c);
              // $GLOBALS["log"]->fatal("fnCalculaSaldoDeudor:codigo_bloqueo_c:".$beanAccount->codigo_bloqueo_c);
              if(
                $beanAccount->estado_bloqueo_c === "Bloqueado"
                && $beanAccount->codigo_bloqueo_c === "CV"
              ){
                $grupoEmpresas = !empty($beanAccount->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida) ? BeanFactory::getBean('GPE_Grupo_empresas', $beanAccount->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida) : null;
                if($grupoEmpresas && $grupoEmpresas->id && $grupoEmpresas->administrar_credito_grupal){
                  if(!$this->getFacturaVencidas($grupoEmpresas)){
                    $beanAccount->estado_bloqueo_c = "Desbloqueado";
                    $beanAccount->codigo_bloqueo_c = "";
                    $beanAccount->estado_cartera_c = "CV";
                  }
                }

                else{
                  $GLOBALS["log"]->fatal("Entra");
                  if(!$this->getFacturaVencidas($beanAccount)){
                    $GLOBALS["log"]->fatal("desbloquea");
                    $beanAccount->estado_bloqueo_c = "Desbloqueado";
                    $beanAccount->codigo_bloqueo_c = "";
                    $beanAccount->estado_cartera_c = "CV";
                  }
                }
              }
              $beanAccount->save(false);
            }
        }
        //$GLOBALS['log']->setLevel('debug');
        //_ppl(' D E U D O R');
    }

    private function getFacturaVencidas($beanValidate)
    {
      $records = [];
      $query = new SugarQuery();
      $bean = BeanFactory::newBean('LF_Facturas');
      $query->from($bean, array('team_security' => false));

      if($beanValidate->module_name === 'Accounts'){
        $query->where()
          ->equals('accounts_lf_facturas_1accounts_ida', $beanValidate->id)
          ->gt('saldo', 0)
          ->notEquals('vigencia_c', 'vigente');
      }
      if($beanValidate->module_name === 'GPE_Grupo_empresas'){
        $query->join('accounts_lf_facturas_1', ["alias" => "accountsFacturas"]);
        $query->joinTable("gpe_grupo_empresas_accounts_c", array(
          "alias" => "jt1_gpe_grupo_empresas_accounts_c"
        ))
          ->on()
            ->equalsField("jt1_gpe_grupo_empresas_accounts_c.gpe_grupo_empresas_accountsaccounts_idb",  "accountsFacturas.id");

        $query->where()
            ->gt('saldo', 0)
            ->notEquals('vigencia_c', 'vigente');
        $query->whereRaw("jt1_gpe_grupo_empresas_accounts_c.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida = '$beanValidate->id'");
      }
      $query->select(['id', 'name']);

      $records = $query->execute();
      return $records;
    }

}

?>
