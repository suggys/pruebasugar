<?php
class FacturasLogicHooksManager{

    protected static $fetchedRow = array();

    public function beforeSave($bean, $event, $arguments){
    	if(isset($bean->importado_c) && $bean->importado_c){
        return true;
      }
      if ( !empty($bean->id) ) {
        self::$fetchedRow[$bean->id] = $bean->fetched_row;
      }
    }

    public function afterSave($bean, $arguments) {
        return true;
    }
}
?>
