<?php
class Relacionafacturas{
    public function fnQuitaRefactura($bean, $event, $arguments) {
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        if(
          $event === 'after_relationship_delete'
          && $arguments['related_id']
          && $arguments['related_module'] === 'LF_Facturas'
        ){
            $beanFact = BeanFactory::getBean('LF_Facturas', $arguments['related_id']);
            if($beanFact->id){
              $beanFact->refactura = 0;
              $beanFact->save();
              $bean->fecha_emision_padre_c = '';
              $bean->save(false);
            }
        }
    }

    public function fnAgregaRefactura($bean, $event, $arguments) {
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        if(
          $event === 'after_relationship_add'
          && $arguments['related_id']
          && $arguments['related_module'] === 'LF_Facturas'
        ){
          $beanFact = BeanFactory::getBean('LF_Facturas', $arguments['related_id']);
          if($beanFact->id){
            $beanFact->refactura = 1;
            $beanFact->save();
            $bean->fecha_emision_padre_c = $beanFact->dt_crt;
            $bean->save(false);
          }
        }
    }
}
