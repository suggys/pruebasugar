<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Dependencies/iva_deps.php

$dependencies['LF_Facturas']['iva'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('tipo_documento'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(

      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'iva',
              'label'  => 'iva_label', //normally <field>_label
              'value' => 'equal($tipo_documento, "factura")', //Formula
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Dependencies/lf_facturas_lf_facturas_name_deps.php

$dependencies['LF_Facturas']['lf_facturas_lf_facturas_name'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('tipo_documento'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(

      array(
          'name' => 'SetVisibility', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'lf_facturas_lf_facturas_name',
              'label'  => 'lf_facturas_lf_facturas_name_label', //normally <field>_label
              'value' => 'equal($tipo_documento, "factura")', //Formula
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Dependencies/importe_deps.php

$dependencies['LF_Facturas']['importe'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('tipo_documento'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(

      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'importe',
              'label'  => 'importe_label', //normally <field>_label
              'value' => 'equal($tipo_documento, "factura")', //Formula
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Dependencies/lf_facturas_orden_ordenes_name_deps.php

$dependencies['LF_Facturas']['lf_facturas_orden_ordenes_name'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('tipo_documento'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(

      array(
          'name' => 'SetVisibility', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'lf_facturas_orden_ordenes_name',
              'label'  => 'lf_facturas_orden_ordenes_name_label', //normally <field>_label
              'value' => 'equal($tipo_documento, "factura")', //Formula
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Dependencies/name_deps.php

$dependencies['LF_Facturas']['name'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('tipo_documento'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(

      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'name',
              'label'  => 'name_label', //normally <field>_label
              'value' => 'equal($tipo_documento, "factura")', //Formula
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Dependencies/lowes_pagos_lf_facturas_1_name_deps.php

$dependencies['LF_Facturas']['lowes_pagos_lf_facturas_1_name'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('tipo_documento'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(

      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'lowes_pagos_lf_facturas_1_name',
              'label'  => 'lowes_pagos_lf_facturas_1_name_label', //normally <field>_label
              'value' => 'equal($tipo_documento, "nota_de_cargo")', //Formula
          ),
      ),
    ),
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Dependencies/sub_total_deps.php

$dependencies['LF_Facturas']['sub_total'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('tipo_documento'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(

      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'sub_total',
              'label'  => 'sub_total_label', //normally <field>_label
              'value' => 'equal($tipo_documento, "factura")', //Formula
          ),
      ),
    ),
);

?>
