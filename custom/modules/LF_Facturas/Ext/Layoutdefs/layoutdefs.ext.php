<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Layoutdefs/lf_facturas_lf_facturas_LF_Facturas.php

 // created: 2016-10-19 00:31:50
$layout_defs["LF_Facturas"]["subpanel_setup"]['lf_facturas_lf_facturas'] = array (
  'order' => 100,
  'module' => 'LF_Facturas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_R_TITLE',
  'get_subpanel_data' => 'lf_facturas_lf_facturas',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Layoutdefs/low01_pagos_facturas_lf_facturas_LF_Facturas.php

 // created: 2017-04-03 22:41:21
$layout_defs["LF_Facturas"]["subpanel_setup"]['low01_pagos_facturas_lf_facturas'] = array (
  'order' => 100,
  'module' => 'Low01_Pagos_Facturas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LOW01_PAGOS_FACTURAS_TITLE',
  'get_subpanel_data' => 'low01_pagos_facturas_lf_facturas',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
