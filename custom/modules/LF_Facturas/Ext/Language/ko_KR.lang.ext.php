<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/ko_KR.Lowes_facturas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_L_TITLE'] = 'Facturas';
$mod_strings['LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_R_TITLE'] = 'Facturas';
$mod_strings['LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_R_TITLE_ID'] = 'Facturas ID';
$mod_strings['LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Ordenes';
$mod_strings['LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE_ID'] = 'Ordenes ID';
$mod_strings['LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE'] = 'Ordenes';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/ko_KR.customnc_notas_credito_lf_facturas_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'Notas de crédito ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/ko_KR.customaccounts_lf_facturas_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'Notas de crédito ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/ko_KR.Pagos.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE_ID'] = 'Pagos ID';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'lowes_Pagos ID';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/ko_KR.Pagos_Facturas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LOW01_PAGOS_FACTURAS_TITLE'] = 'Pagos_Facturas';
$mod_strings['LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE'] = 'Pagos_Facturas';

?>
