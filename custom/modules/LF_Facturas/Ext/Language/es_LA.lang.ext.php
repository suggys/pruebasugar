<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.Lowes_facturas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_L_TITLE'] = 'Facturas / Notas de Cargo';
$mod_strings['LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_R_TITLE'] = 'Facturas / Notas de Cargo';
$mod_strings['LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_R_TITLE_ID'] = 'Facturas / Notas de Cargo ID';
$mod_strings['LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
$mod_strings['LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE_ID'] = 'Órdenes ID';
$mod_strings['LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE'] = 'Órdenes';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.customaccounts_lf_facturas_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'Notas de crédito ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Cheque';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'lowes_Pagos ID';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Pagos';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.PI_dias_transcurridos_desde_emision_c.php


$mod_strings['LBL_DIAS_TRASCURRIDOS_C'] = 'Días transcurridos desde emisión';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MODULE_NAME'] = 'Facturas';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Factura';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_MONTO_TOTAL_NC'] = 'Monto total nc';
$mod_strings['LBL_ESTADO'] = 'Estado por Condición Especial';
$mod_strings['LBL_SALDO'] = 'Saldo';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_VIGENCIA'] = 'Vigencia';
$mod_strings['LBL_IMPORTADO_C'] = 'Registro Importado';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Cheque';
$mod_strings['LBL_SERIE'] = 'Serie';
$mod_strings['LBL_IVA'] = 'IVA';
$mod_strings['LBL_ESTADO_TIEMPO'] = 'Estado según el Tiempo';
$mod_strings['LBL_NO_TICKET'] = 'Número de Ticket';
$mod_strings['LBL_UUID'] = 'Identificador Único (UUID)';
$mod_strings['LBL_DT_CRT'] = 'Fecha de Emisión';
$mod_strings['LBL_FECHA_ENTREGA'] = 'Fecha de Entrega de la Factura';
$mod_strings['LBL_FECHA_VENCIMIENTO'] = 'Fecha de Vencimiento';
$mod_strings['LBL_DIAS_VENCIMIENTO'] = 'Días Vencido Flexible';
$mod_strings['LBL_DIAS_VENCIDOS_SIN_TOL'] = 'Dias Vencidos';
$mod_strings['LBL_NOMBRE_EMISOR'] = 'Nombre de Emisor';
$mod_strings['LBL_NAME_RECEPTOR'] = 'Nombre Receptor ';
$mod_strings['LBL_REFACTURA'] = 'Refactura';
$mod_strings['LBL_RFC_EMISOR'] = 'RFC Emisor';
$mod_strings['LBL_RFC_RECEPTOR'] = 'RFC Receptor';
$mod_strings['LBL_ABONO'] = 'Abono';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mi Facturas';
$mod_strings['LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_R_TITLE'] = 'Facturas';
$mod_strings['LBL_TIPO_DOCUMENTO'] = 'Tipo de documento';
$mod_strings['LBL_SUB_TOTAL'] = 'Subtotal';
$mod_strings['LBL_IMPORTE'] = 'Importe Total';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Nota de Crédito';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Información de Sistema';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Saldos';
$mod_strings['LBL_SHOW_MORE'] = 'Información de Sistema';
$mod_strings['LBL_LINEA_ANTICIPO'] = 'Linea de Anticipo';
$mod_strings['LBL_CURRENCY_6'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_7'] = 'LBL_CURRENCY';
$mod_strings['LBL_NAME'] = 'Folio';
$mod_strings['LBL_CURRENCY_8'] = 'LBL_CURRENCY';
$mod_strings['LBL_FACTURA_PADRE'] = 'Factura Padre';
$mod_strings['LBL_FECHA_VENCIMIENTO_FLEXIBLE'] = 'Fecha de Vencimiento Flexible';
$mod_strings['LBL_SUCURSAL_CLIENTE'] = 'Sucursal de Cliente';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.Lowes_CRM.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';
$mod_strings['LBL_DIAS_VENCIDOS_SIN_TOL'] = 'Dias vencidos';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.PI_Crea_campo_dias_vencido_pagada_c.php


$mod_strings['LBL_DIAS_VENCIDO_PAGADA_C'] = 'Días vencida pagada';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.Lowes_FRR.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';
$mod_strings['LBL_DIAS_VENCIDOS_SIN_TOL'] = 'Dias vencidos';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.customlowes_pagos_lf_facturas_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'Notas de crédito ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Notas de crédito';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Cheque';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'lowes_Pagos ID';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.Pagos.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LOWES_PAGOS_TITLE'] = 'Pago';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE_ID'] = 'Pagos ID';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Cheque';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'lowes_Pagos ID';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Pagos';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.Lowes_CRM_20171012.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.Lowes_CRM_20171020.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.Lowes_CRM_20171010.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.CF_fecha_emision_padre_c_LF_Facturas.php


$mod_strings['LBL_FECHA_EMISION_PADRE_C'] = 'Fecha de emisión factura padre.';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.Lowes_CRM_201710130100.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.customnc_notas_credito_lf_facturas_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Notas de Crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'Notas de Crédito ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Notas de Crédito';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Cheque';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'lowes_Pagos ID';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Pagos';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.Lowes_CRM_20171009.php

$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Language/es_LA.Pagos_Facturas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LOW01_PAGOS_FACTURAS_TITLE'] = 'Pagos_Facturas';
$mod_strings['LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE'] = 'Pagos_Facturas';

?>
