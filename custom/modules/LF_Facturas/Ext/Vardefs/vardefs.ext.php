<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/accounts_lf_facturas_1_LF_Facturas.php

// created: 2016-10-24 13:40:01
$dictionary["LF_Facturas"]["fields"]["accounts_lf_facturas_1"] = array (
  'name' => 'accounts_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'accounts_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'accounts_lf_facturas_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["LF_Facturas"]["fields"]["accounts_lf_facturas_1_name"] = array (
  'name' => 'accounts_lf_facturas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_lf_facturas_1accounts_ida',
  'link' => 'accounts_lf_facturas_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["accounts_lf_facturas_1accounts_ida"] = array (
  'name' => 'accounts_lf_facturas_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID',
  'id_name' => 'accounts_lf_facturas_1accounts_ida',
  'link' => 'accounts_lf_facturas_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/lf_facturas_lf_facturas_LF_Facturas.php

// created: 2016-10-19 00:31:50
$dictionary["LF_Facturas"]["fields"]["lf_facturas_lf_facturas"] = array (
  'name' => 'lf_facturas_lf_facturas',
  'type' => 'link',
  'relationship' => 'lf_facturas_lf_facturas',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_L_TITLE',
  'id_name' => 'lf_facturas_lf_facturaslf_facturas_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["LF_Facturas"]["fields"]["lf_facturas_lf_facturas_right"] = array (
  'name' => 'lf_facturas_lf_facturas_right',
  'type' => 'link',
  'relationship' => 'lf_facturas_lf_facturas',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'side' => 'right',
  'vname' => 'LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_R_TITLE',
  'id_name' => 'lf_facturas_lf_facturaslf_facturas_ida',
  'link-type' => 'one',
);
$dictionary["LF_Facturas"]["fields"]["lf_facturas_lf_facturas_name"] = array (
  'name' => 'lf_facturas_lf_facturas_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_L_TITLE',
  'save' => true,
  'id_name' => 'lf_facturas_lf_facturaslf_facturas_ida',
  'link' => 'lf_facturas_lf_facturas_right',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["lf_facturas_lf_facturaslf_facturas_ida"] = array (
  'name' => 'lf_facturas_lf_facturaslf_facturas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_R_TITLE_ID',
  'id_name' => 'lf_facturas_lf_facturaslf_facturas_ida',
  'link' => 'lf_facturas_lf_facturas_right',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/lowes_pagos_lf_facturas_1_LF_Facturas.php

// created: 2016-10-22 15:12:51
$dictionary["LF_Facturas"]["fields"]["lowes_pagos_lf_facturas_1"] = array (
  'name' => 'lowes_pagos_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'lowes_pagos_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'lowes_Pagos',
  'bean_name' => 'lowes_Pagos',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'lowes_pagos_lf_facturas_1lowes_pagos_ida',
);
$dictionary["LF_Facturas"]["fields"]["lowes_pagos_lf_facturas_1_name"] = array (
  'name' => 'lowes_pagos_lf_facturas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE',
  'save' => true,
  'id_name' => 'lowes_pagos_lf_facturas_1lowes_pagos_ida',
  'link' => 'lowes_pagos_lf_facturas_1',
  'table' => 'lowes_pagos',
  'module' => 'lowes_Pagos',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["lowes_pagos_lf_facturas_1lowes_pagos_ida"] = array (
  'name' => 'lowes_pagos_lf_facturas_1lowes_pagos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE_ID',
  'id_name' => 'lowes_pagos_lf_facturas_1lowes_pagos_ida',
  'link' => 'lowes_pagos_lf_facturas_1',
  'table' => 'lowes_pagos',
  'module' => 'lowes_Pagos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_importado_c.php

 // created: 2016-12-14 02:09:19
$dictionary['LF_Facturas']['fields']['importado_c']['labelValue']='Registro Importado';
$dictionary['LF_Facturas']['fields']['importado_c']['enforced']='';
$dictionary['LF_Facturas']['fields']['importado_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/nc_notas_credito_lf_facturas_1_LF_Facturas.php

// created: 2016-10-20 16:38:00
$dictionary["LF_Facturas"]["fields"]["nc_notas_credito_lf_facturas_1"] = array (
  'name' => 'nc_notas_credito_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'NC_Notas_credito',
  'bean_name' => 'NC_Notas_credito',
  'side' => 'right',
  'vname' => 'LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
  'link-type' => 'one',
);
$dictionary["LF_Facturas"]["fields"]["nc_notas_credito_lf_facturas_1_name"] = array (
  'name' => 'nc_notas_credito_lf_facturas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE',
  'save' => true,
  'id_name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
  'link' => 'nc_notas_credito_lf_facturas_1',
  'table' => 'nc_notas_credito',
  'module' => 'NC_Notas_credito',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["nc_notas_credito_lf_facturas_1nc_notas_credito_ida"] = array (
  'name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID',
  'id_name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
  'link' => 'nc_notas_credito_lf_facturas_1',
  'table' => 'nc_notas_credito',
  'module' => 'NC_Notas_credito',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/lf_facturas_orden_ordenes_LF_Facturas.php

// created: 2016-10-19 00:31:50
$dictionary["LF_Facturas"]["fields"]["lf_facturas_orden_ordenes"] = array (
  'name' => 'lf_facturas_orden_ordenes',
  'type' => 'link',
  'relationship' => 'lf_facturas_orden_ordenes',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'side' => 'right',
  'vname' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
  'link-type' => 'one',
);
$dictionary["LF_Facturas"]["fields"]["lf_facturas_orden_ordenes_name"] = array (
  'name' => 'lf_facturas_orden_ordenes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'save' => true,
  'id_name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
  'link' => 'lf_facturas_orden_ordenes',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["lf_facturas_orden_ordenesorden_ordenes_ida"] = array (
  'name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE_ID',
  'id_name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
  'link' => 'lf_facturas_orden_ordenes',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/lowes_pagos_lf_facturas_LF_Facturas.php

// created: 2016-10-22 15:12:51
$dictionary["LF_Facturas"]["fields"]["lowes_pagos_lf_facturas"] = array (
  'name' => 'lowes_pagos_lf_facturas',
  'type' => 'link',
  'relationship' => 'lowes_pagos_lf_facturas',
  'source' => 'non-db',
  'module' => 'lowes_Pagos',
  'bean_name' => 'lowes_Pagos',
  'side' => 'right',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'lowes_pagos_lf_facturaslowes_pagos_ida',
  'link-type' => 'one',
);
$dictionary["LF_Facturas"]["fields"]["lowes_pagos_lf_facturas_name"] = array (
  'name' => 'lowes_pagos_lf_facturas_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LOWES_PAGOS_TITLE',
  'save' => true,
  'id_name' => 'lowes_pagos_lf_facturaslowes_pagos_ida',
  'link' => 'lowes_pagos_lf_facturas',
  'table' => 'lowes_pagos',
  'module' => 'lowes_Pagos',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["lowes_pagos_lf_facturaslowes_pagos_ida"] = array (
  'name' => 'lowes_pagos_lf_facturaslowes_pagos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE_ID',
  'id_name' => 'lowes_pagos_lf_facturaslowes_pagos_ida',
  'link' => 'lowes_pagos_lf_facturas',
  'table' => 'lowes_pagos',
  'module' => 'lowes_Pagos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_monto_total_nc_c.php

 // created: 2016-10-26 15:03:41
$dictionary['LF_Facturas']['fields']['monto_total_nc_c']['labelValue']='Monto total nc';
$dictionary['LF_Facturas']['fields']['monto_total_nc_c']['enforced']='';
$dictionary['LF_Facturas']['fields']['monto_total_nc_c']['dependency']='';
$dictionary['LF_Facturas']['fields']['monto_total_nc_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_tipo_documento.php

 // created: 2017-01-18 23:01:04
$dictionary['LF_Facturas']['fields']['tipo_documento']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_dias_vencido_pagada_c.php

 // created: 2017-02-07 16:07:36
$dictionary['LF_Facturas']['fields']['dias_vencido_pagada_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_dias_vencidos_sin_tol_c.php

 // created: 2017-10-25 23:59:12
$dictionary['LF_Facturas']['fields']['dias_vencidos_sin_tol_c']['labelValue']='Dias Vencidos';
$dictionary['LF_Facturas']['fields']['dias_vencidos_sin_tol_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LF_Facturas']['fields']['dias_vencidos_sin_tol_c']['enforced']='';
$dictionary['LF_Facturas']['fields']['dias_vencidos_sin_tol_c']['dependency']='';
$dictionary['LF_Facturas']['fields']['dias_vencidos_sin_tol_c']['default']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_dias_vencimiento.php

 // created: 2017-01-18 22:53:52
$dictionary['LF_Facturas']['fields']['dias_vencimiento']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['dias_vencimiento']['audited']=true;
$dictionary['LF_Facturas']['fields']['dias_vencimiento']['default']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/low01_pagos_facturas_lf_facturas_LF_Facturas.php

// created: 2017-04-03 22:41:21
$dictionary["LF_Facturas"]["fields"]["low01_pagos_facturas_lf_facturas"] = array (
  'name' => 'low01_pagos_facturas_lf_facturas',
  'type' => 'link',
  'relationship' => 'low01_pagos_facturas_lf_facturas',
  'source' => 'non-db',
  'module' => 'Low01_Pagos_Facturas',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_saldo.php

 // created: 2017-01-18 22:59:38
$dictionary['LF_Facturas']['fields']['saldo']['default']=0;
$dictionary['LF_Facturas']['fields']['saldo']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['saldo']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_serie.php

 // created: 2017-01-18 23:00:19
$dictionary['LF_Facturas']['fields']['serie']['help']='';
$dictionary['LF_Facturas']['fields']['serie']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['serie']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_rfc_receptor.php

 // created: 2017-01-18 22:59:10
$dictionary['LF_Facturas']['fields']['rfc_receptor']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['rfc_receptor']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_fecha_entrega.php

 // created: 2017-01-18 22:54:58
$dictionary['LF_Facturas']['fields']['fecha_entrega']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['fecha_entrega']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_importadode_c.php

 // created: 2017-10-09 18:22:58
$dictionary['LF_Facturas']['fields']['importadode_c']['labelValue']='Archivo de origen';
$dictionary['LF_Facturas']['fields']['importadode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LF_Facturas']['fields']['importadode_c']['enforced']='';
$dictionary['LF_Facturas']['fields']['importadode_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/full_text_search_admin.php

 // created: 2017-02-24 18:28:04
$dictionary['LF_Facturas']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_dias_trascurridos_c.php

 // created: 2017-02-07 16:13:00
$dictionary['LF_Facturas']['fields']['dias_trascurridos_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_no_ticket.php

 // created: 2017-01-18 22:58:00
$dictionary['LF_Facturas']['fields']['no_ticket']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['no_ticket']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_linea_anticipo_c.php

 // created: 2017-01-26 23:06:49
$dictionary['LF_Facturas']['fields']['linea_anticipo_c']['duplicate_merge_dom_value']=0;
$dictionary['LF_Facturas']['fields']['linea_anticipo_c']['labelValue']='Linea de Anticipo';
$dictionary['LF_Facturas']['fields']['linea_anticipo_c']['enforced']='';
$dictionary['LF_Facturas']['fields']['linea_anticipo_c']['default']=false;
$dictionary['LF_Facturas']['fields']['linea_anticipo_c']['dependency']='equal($tipo_documento,"factura")';

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_fecha_vencimiento.php

 // created: 2017-08-30 22:07:46
$dictionary['LF_Facturas']['fields']['fecha_vencimiento']['dependency']='equal($tipo_documento,"factura")';

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_estado.php

 // created: 2017-01-18 22:54:25
$dictionary['LF_Facturas']['fields']['estado']['default']='';
$dictionary['LF_Facturas']['fields']['estado']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['estado']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_sub_total.php

 // created: 2016-12-14 02:08:09
$dictionary['LF_Facturas']['fields']['sub_total']['default']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_importe.php

 // created: 2017-01-18 22:55:16
$dictionary['LF_Facturas']['fields']['importe']['default']=0;
$dictionary['LF_Facturas']['fields']['importe']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_fecha_vencimiento_flexible_c.php

 // created: 2017-07-13 15:05:55
$dictionary['LF_Facturas']['fields']['fecha_vencimiento_flexible_c']['labelValue']='Fecha de Vencimiento Flexible';
$dictionary['LF_Facturas']['fields']['fecha_vencimiento_flexible_c']['enforced']='';
$dictionary['LF_Facturas']['fields']['fecha_vencimiento_flexible_c']['dependency']='equal($tipo_documento,"factura")';

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_abono.php

 // created: 2017-01-18 22:53:12
$dictionary['LF_Facturas']['fields']['abono']['default']=0;
$dictionary['LF_Facturas']['fields']['abono']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['abono']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_estado_tiempo.php

 // created: 2017-01-18 22:54:43
$dictionary['LF_Facturas']['fields']['estado_tiempo']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['estado_tiempo']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_name_receptor.php

 // created: 2017-01-18 22:57:14
$dictionary['LF_Facturas']['fields']['name_receptor']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['name_receptor']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_dt_crt.php

 // created: 2017-01-18 22:54:08
$dictionary['LF_Facturas']['fields']['dt_crt']['type']='datetime';
$dictionary['LF_Facturas']['fields']['dt_crt']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['dt_crt']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_refactura.php

 // created: 2017-01-18 22:58:33
$dictionary['LF_Facturas']['fields']['refactura']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['refactura']['audited']=true;
$dictionary['LF_Facturas']['fields']['refactura']['default']=0;
 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_sucursal_cliente_c.php

 // created: 2017-08-24 22:53:10
$dictionary['LF_Facturas']['fields']['sucursal_cliente_c']['labelValue']='Sucursal de Cliente';
$dictionary['LF_Facturas']['fields']['sucursal_cliente_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LF_Facturas']['fields']['sucursal_cliente_c']['formula']='related($accounts_lf_facturas_1,"id_sucursal_c")';
$dictionary['LF_Facturas']['fields']['sucursal_cliente_c']['enforced']='false';
$dictionary['LF_Facturas']['fields']['sucursal_cliente_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_fecha_emision_padre_c.php

 // created: 2017-02-11 08:13:38
$dictionary['LF_Facturas']['fields']['fecha_emision_padre_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_rfc_emisor.php

 // created: 2017-01-18 22:58:48
$dictionary['LF_Facturas']['fields']['rfc_emisor']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['rfc_emisor']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_nombre_emisor.php

 // created: 2017-01-18 22:58:18
$dictionary['LF_Facturas']['fields']['nombre_emisor']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['nombre_emisor']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_name.php

 // created: 2017-01-18 22:56:43
$dictionary['LF_Facturas']['fields']['name']['audited']=true;
$dictionary['LF_Facturas']['fields']['name']['unified_search']=false;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_uuid.php

 // created: 2017-01-18 23:01:27
$dictionary['LF_Facturas']['fields']['uuid']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['uuid']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/Vardefs/sugarfield_vigencia_c.php

 // created: 2017-01-18 23:03:46
$dictionary['LF_Facturas']['fields']['vigencia_c']['labelValue']='Vigencia';
$dictionary['LF_Facturas']['fields']['vigencia_c']['dependency']='equal($tipo_documento,"factura")';
$dictionary['LF_Facturas']['fields']['vigencia_c']['visibility_grid']='';

 
?>
