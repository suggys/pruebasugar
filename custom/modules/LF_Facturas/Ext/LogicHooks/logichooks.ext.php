<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/LogicHooks/LHCalculaSaldoPendientePago.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    2,
    'FR-28.1. Aplicar pagos a facturas',
    'custom/modules/LF_Facturas/LHCalculaSaldoPendientePago.php',
    'LHCalculaSaldoPendientePago',
    'fnCalculaSaldoPendienteAplicar'
);

$hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
$hook_array['after_relationship_delete'][] = array(
    2,
    'FR-28.1. Aplicar pagos a facturas',
    'custom/modules/LF_Facturas/LHCalculaSaldoPendientePago.php',
    'LHCalculaSaldoPendientePago',
    'fnCalculaSaldoPendienteAplicarDesvincular'
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/LogicHooks/LH_Calcula_abono_factura_nc.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    1,
    'Calcular monto por concepto de nc a factura',
    'custom/modules/LF_Facturas/LH_Calcula_abono_factura_nc.php',
    'Calcula_Abono_Por_NC',
    'CalculaAbonoFacturaNC'
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/LogicHooks/LH_RelacionaOrdenes_Cuentas_A_Facturas.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : [];
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : [];
$hook_array['before_save'][] = array(
    1,
    'Relaciona Ordenes y cuentas a Factura a partir del num de ticket',
    'custom/modules/LF_Facturas/Rel_Orden_Cuenta_NumTicket.php',
    'Rel_Orden_Cuenta_NumTicket',
    'previousRow'
);
$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : [];
$hook_array['after_save'][] = array(
    1,
    'Relaciona Ordenes y cuentas a Factura a partir del num de ticket',
    'custom/modules/LF_Facturas/Rel_Orden_Cuenta_NumTicket.php',
    'Rel_Orden_Cuenta_NumTicket',
    'relacionaDatos'
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/LogicHooks/LH_CalSaldoFacturaAplicaNC.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    1,
    'FR-23.2. Calcular saldos de la factura por aplicación de nota de crédito',
    'custom/modules/LF_Facturas/CalSaldoFactAplicaNC.php',
    'CalSaldoFactAplicaNC',
    'CalSaldoFactAplicaNC'
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/LogicHooks/LHAsignaSaldoInicialFactura.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : [];
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : [];
$hook_array['before_save'][] = array(
    1,
    'Copia importe a saldo cuando se crea',
    'custom/modules/LF_Facturas/LHAsignaSaldoInicialFactura.php',
    'LHAsignaSaldoInicialFactura',
    'fnCopiaSaldoFactura'
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/LogicHooks/FacturasLogicHooksManager.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
    1,
    'Calcula saldo deudor',
    'custom/modules/LF_Facturas/FacturasLogicHooksManager.php',
    'FacturasLogicHooksManager',
    'afterSave'
);

$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
    2,
    'Guarda fetched_row',
    'custom/modules/LF_Facturas/FacturasLogicHooksManager.php',
    'FacturasLogicHooksManager',
    'beforeSave'
);

$hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
$hook_array['after_relationship_delete'][] = array(
    3,
    'Relaciona Facturas',
    'custom/modules/LF_Facturas/Relacionafacturas.php',
    'Relacionafacturas',
    'fnQuitaRefactura'
);

$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    3,
    'Relaciona Facturas',
    'custom/modules/LF_Facturas/Relacionafacturas.php',
    'Relacionafacturas',
    'fnAgregaRefactura'
);

?>
<?php
// Merged from custom/Extension/modules/LF_Facturas/Ext/LogicHooks/LHAsignaEstadoSegunTiempo.php


$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    2,
    'Facturas Actualiza al Vincular  Estado segun Tiepo',
    'custom/modules/LF_Facturas/LHAsignaEstadoSegunTiempo.php',
    'LHAsignaEstadoSegunTiempo',
    'fnActualizaEstadoalVincular'
);
/*

$hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
$hook_array['after_relationship_delete'][] = array(
    2,
    'Facturas Actuliza al desvinucular Estado segun Tiepo',
    'custom/modules/LF_Facturas/LHAsignaEstadoSegunTiempo.php',
    'LHAsignaEstadoSegunTiempo',
    'fnActualizaEstadoalVincular'
);

$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
    3,
    'Facturas Actuliza al Actualizar Estado segun Tiepo ',
    'custom/modules/LF_Facturas/LHAsignaEstadoSegunTiempo.php',
    'LHAsignaEstadoSegunTiempo',
    'fnEstadoEntragada'
);
*/



?>
