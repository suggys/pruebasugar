<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class Rel_Orden_Cuenta_NumTicket
{
    protected static $fetchedRow = array();

    public function previousRow($bean, $event, $arguments){
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        if ( !empty($bean->id) ) {
            self::$fetchedRow[$bean->id] = $bean->fetched_row;
        }

        if(
          empty($bean->fetched_row['fecha_entrega'])
          && !empty($bean->fecha_entrega)
        ){
          $bean->estado_tiempo = "entregada";
        }
    }

    public function relacionaDatos($bean, $event, $arguments)
    {
      if(isset($bean->importado_c) && $bean->importado_c){
        return true;
      }
      // validando que sea nuevo y que tenga un folio de ticket
      if( $bean->no_ticket && self::$fetchedRow[$bean->id]['no_ticket'] != $bean->no_ticket  ){
        $bean->saldo = $bean->importe;
        $orden = BeanFactory::newBean("orden_Ordenes");
        $orden->retrieve_by_string_fields(array('no_ticket_c' => $bean->no_ticket));
        if($orden->id &&
          (empty($orden->estado_c)
          || $orden->estado_c === "2"
          || $orden->estado_c === "5"
          || $orden->estado_c === "8"
          || $orden->estado_c === "9"
          )
        ){
          $bean->load_relationship('lf_facturas_orden_ordenes');
          $bean->lf_facturas_orden_ordenes->add($orden);
        }
      }
    }
}
?>
