<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/dupe_check.ext.php

$dictionary['Opportunity']['fields']['revenuelineitems']['workflow'] = true;
$dictionary['Opportunity']['duplicate_check']['FilterDuplicateCheck']['filter_template'][0]['$and'][1] = array('sales_status' => array('$not_equals' => 'Closed Lost'));
$dictionary['Opportunity']['duplicate_check']['FilterDuplicateCheck']['filter_template'][0]['$and'][2] = array('sales_status' => array('$not_equals' => 'Closed Won'));
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/rli_link_workflow.php

$dictionary['Opportunity']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Opportunity']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_amount.php

 // created: 2017-07-20 10:22:11
$dictionary['Opportunity']['fields']['amount']['required']=true;
$dictionary['Opportunity']['fields']['amount']['audited']=true;
$dictionary['Opportunity']['fields']['amount']['massupdate']=true;
$dictionary['Opportunity']['fields']['amount']['comments']='Unconverted amount of the opportunity';
$dictionary['Opportunity']['fields']['amount']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['amount']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['amount']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['amount']['calculated']=false;
$dictionary['Opportunity']['fields']['amount']['default']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_best_case.php

 // created: 2017-07-20 10:22:14
$dictionary['Opportunity']['fields']['best_case']['audited']=true;
$dictionary['Opportunity']['fields']['best_case']['massupdate']=true;
$dictionary['Opportunity']['fields']['best_case']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['best_case']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['best_case']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['best_case']['calculated']=false;
$dictionary['Opportunity']['fields']['best_case']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['best_case']['default']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_clase_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['clase_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['clase_c']['labelValue']='Clase/Categorías';
$dictionary['Opportunity']['fields']['clase_c']['dependency']='';
$dictionary['Opportunity']['fields']['clase_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_comision_tienda_colabora_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['comision_tienda_colabora_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['comision_tienda_colabora_c']['labelValue']='Comisión Tienda Colaboradora (%)';
$dictionary['Opportunity']['fields']['comision_tienda_colabora_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['comision_tienda_colabora_c']['enforced']='';
$dictionary['Opportunity']['fields']['comision_tienda_colabora_c']['dependency']='equal(true,false)';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/OpportunitiesVisibility.php

$dictionary['Opportunity']['visibility']["OpportunityVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_comision_tienda_base_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['comision_tienda_base_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['comision_tienda_base_c']['labelValue']='Comisión Tienda Base (%)';
$dictionary['Opportunity']['fields']['comision_tienda_base_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['comision_tienda_base_c']['enforced']='false';
$dictionary['Opportunity']['fields']['comision_tienda_base_c']['dependency']='equal(false,true)';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/prospects_opportunities_1_Opportunities.php

// created: 2017-07-14 22:54:49
$dictionary["Opportunity"]["fields"]["prospects_opportunities_1"] = array (
  'name' => 'prospects_opportunities_1',
  'type' => 'link',
  'relationship' => 'prospects_opportunities_1',
  'source' => 'non-db',
  'module' => 'Prospects',
  'bean_name' => 'Prospect',
  'side' => 'right',
  'vname' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'prospects_opportunities_1prospects_ida',
  'link-type' => 'one',
);
$dictionary["Opportunity"]["fields"]["prospects_opportunities_1_name"] = array (
  'name' => 'prospects_opportunities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_PROSPECTS_TITLE',
  'save' => true,
  'id_name' => 'prospects_opportunities_1prospects_ida',
  'link' => 'prospects_opportunities_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["prospects_opportunities_1prospects_ida"] = array (
  'name' => 'prospects_opportunities_1prospects_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID',
  'id_name' => 'prospects_opportunities_1prospects_ida',
  'link' => 'prospects_opportunities_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_closed_revenue_line_items.php

 // created: 2017-07-20 10:22:16
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['audited']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['massupdate']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['reportable']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['min']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['max']=false;
$dictionary['Opportunity']['fields']['closed_revenue_line_items']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/lo_obras_opportunities_Opportunities.php

// created: 2017-07-20 15:21:47
$dictionary["Opportunity"]["fields"]["lo_obras_opportunities"] = array (
  'name' => 'lo_obras_opportunities',
  'type' => 'link',
  'relationship' => 'lo_obras_opportunities',
  'source' => 'non-db',
  'module' => 'LO_Obras',
  'bean_name' => 'LO_Obras',
  'side' => 'right',
  'vname' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'lo_obras_opportunitieslo_obras_ida',
  'link-type' => 'one',
);
$dictionary["Opportunity"]["fields"]["lo_obras_opportunities_name"] = array (
  'name' => 'lo_obras_opportunities_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_LO_OBRAS_TITLE',
  'save' => true,
  'id_name' => 'lo_obras_opportunitieslo_obras_ida',
  'link' => 'lo_obras_opportunities',
  'table' => 'lo_obras',
  'module' => 'LO_Obras',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["lo_obras_opportunitieslo_obras_ida"] = array (
  'name' => 'lo_obras_opportunitieslo_obras_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_OPPORTUNITIES_TITLE_ID',
  'id_name' => 'lo_obras_opportunitieslo_obras_ida',
  'link' => 'lo_obras_opportunities',
  'table' => 'lo_obras',
  'module' => 'LO_Obras',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_tipo_de_colaboracion_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['tipo_de_colaboracion_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['tipo_de_colaboracion_c']['labelValue']='Tipo de Colaboración';
$dictionary['Opportunity']['fields']['tipo_de_colaboracion_c']['dependency']='';
$dictionary['Opportunity']['fields']['tipo_de_colaboracion_c']['visibility_grid']='';
$dictionary['Opportunity']['fields']['tipo_de_colaboracion_c']['required']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sucursal_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['sucursal_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['sucursal_c']['labelValue']='Sucursal';
$dictionary['Opportunity']['fields']['sucursal_c']['dependency']='';
$dictionary['Opportunity']['fields']['sucursal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_productos_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['productos_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['productos_c']['labelValue']='Productos';
$dictionary['Opportunity']['fields']['productos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['productos_c']['enforced']='';
$dictionary['Opportunity']['fields']['productos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sales_status.php

 // created: 2017-07-20 10:22:15
$dictionary['Opportunity']['fields']['sales_status']['audited']=false;
$dictionary['Opportunity']['fields']['sales_status']['massupdate']=false;
$dictionary['Opportunity']['fields']['sales_status']['importable']=false;
$dictionary['Opportunity']['fields']['sales_status']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sales_status']['reportable']=false;
$dictionary['Opportunity']['fields']['sales_status']['calculated']=false;
$dictionary['Opportunity']['fields']['sales_status']['dependency']=false;
$dictionary['Opportunity']['fields']['sales_status']['studio']=false;
$dictionary['Opportunity']['fields']['sales_status']['default']='activo';
$dictionary['Opportunity']['fields']['sales_status']['len']=100;
$dictionary['Opportunity']['fields']['sales_status']['options']='proyecto_estatus_list';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_moneda_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['moneda_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['moneda_c']['labelValue']='Moneda';
$dictionary['Opportunity']['fields']['moneda_c']['dependency']='';
$dictionary['Opportunity']['fields']['moneda_c']['visibility_grid']='';
$dictionary['Opportunity']['fields']['moneda_c']['required']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_estatus_c.php

 // created: 2017-08-23 18:56:06
$dictionary['Opportunity']['fields']['estatus_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['estatus_c']['labelValue']='Estatus';
$dictionary['Opportunity']['fields']['estatus_c']['dependency']='';
$dictionary['Opportunity']['fields']['estatus_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_date_closed.php

 // created: 2017-08-03 17:19:33
$dictionary['Opportunity']['fields']['date_closed']['required']=false;
$dictionary['Opportunity']['fields']['date_closed']['audited']=true;
$dictionary['Opportunity']['fields']['date_closed']['massupdate']=true;
$dictionary['Opportunity']['fields']['date_closed']['comments']='Expected or actual date the oppportunity will close';
$dictionary['Opportunity']['fields']['date_closed']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['date_closed']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['date_closed']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['date_closed']['calculated']=false;
$dictionary['Opportunity']['fields']['date_closed']['full_text_search']=array (
);
$dictionary['Opportunity']['fields']['date_closed']['related_fields']=array (
);
$dictionary['Opportunity']['fields']['date_closed']['enable_range_search']='1';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_probabilidad_de_venta_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['probabilidad_de_venta_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['probabilidad_de_venta_c']['labelValue']='Probabilidad de venta (%)';
$dictionary['Opportunity']['fields']['probabilidad_de_venta_c']['dependency']='';
$dictionary['Opportunity']['fields']['probabilidad_de_venta_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_commit_stage.php

 // created: 2017-07-20 10:22:15
$dictionary['Opportunity']['fields']['commit_stage']['audited']=false;
$dictionary['Opportunity']['fields']['commit_stage']['massupdate']=true;
$dictionary['Opportunity']['fields']['commit_stage']['options']='';
$dictionary['Opportunity']['fields']['commit_stage']['comments']='Forecast commit ranges: Include, Likely, Omit etc.';
$dictionary['Opportunity']['fields']['commit_stage']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['commit_stage']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['commit_stage']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['commit_stage']['reportable']=true;
$dictionary['Opportunity']['fields']['commit_stage']['enforced']=false;
$dictionary['Opportunity']['fields']['commit_stage']['dependency']=false;
$dictionary['Opportunity']['fields']['commit_stage']['studio']=true;
$dictionary['Opportunity']['fields']['commit_stage']['related_fields']=array (
);

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_description.php

 // created: 2017-07-11 18:42:00
$dictionary['Opportunity']['fields']['description']['audited']=false;
$dictionary['Opportunity']['fields']['description']['massupdate']=false;
$dictionary['Opportunity']['fields']['description']['comments']='Full text of the note';
$dictionary['Opportunity']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['description']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.59',
  'searchable' => true,
);
$dictionary['Opportunity']['fields']['description']['calculated']=false;
$dictionary['Opportunity']['fields']['description']['rows']='6';
$dictionary['Opportunity']['fields']['description']['cols']='80';
$dictionary['Opportunity']['fields']['description']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_etapa_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['etapa_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['etapa_c']['labelValue']='Etapa';
$dictionary['Opportunity']['fields']['etapa_c']['dependency']='';
$dictionary['Opportunity']['fields']['etapa_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_opportunity_type.php

 // created: 2017-07-11 18:41:09
$dictionary['Opportunity']['fields']['opportunity_type']['default']='prospeccion';
$dictionary['Opportunity']['fields']['opportunity_type']['len']=100;
$dictionary['Opportunity']['fields']['opportunity_type']['massupdate']=true;
$dictionary['Opportunity']['fields']['opportunity_type']['options']='proyecto_tipo_list';
$dictionary['Opportunity']['fields']['opportunity_type']['comments']='Type of opportunity (ex: Existing, New)';
$dictionary['Opportunity']['fields']['opportunity_type']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['opportunity_type']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['opportunity_type']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['opportunity_type']['calculated']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['dependency']=false;
$dictionary['Opportunity']['fields']['opportunity_type']['required']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_total_revenue_line_items.php

 // created: 2017-07-20 10:22:15
$dictionary['Opportunity']['fields']['total_revenue_line_items']['audited']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['massupdate']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['total_revenue_line_items']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['total_revenue_line_items']['reportable']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['min']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['max']=false;
$dictionary['Opportunity']['fields']['total_revenue_line_items']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_motivo_de_perdida_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['motivo_de_perdida_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['motivo_de_perdida_c']['labelValue']='Motivo de Pérdida';
$dictionary['Opportunity']['fields']['motivo_de_perdida_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['motivo_de_perdida_c']['enforced']='';
$dictionary['Opportunity']['fields']['motivo_de_perdida_c']['dependency']='equal("perdida",$etapa_c)';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_etapa_inicio_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['etapa_inicio_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['etapa_inicio_c']['labelValue']='Etapa Inicio del Proyecto con respecto a la Obra';
$dictionary['Opportunity']['fields']['etapa_inicio_c']['dependency']='';
$dictionary['Opportunity']['fields']['etapa_inicio_c']['visibility_grid']='';
$dictionary['Opportunity']['fields']['etapa_inicio_c']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sucursal_colaboracion_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['sucursal_colaboracion_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['sucursal_colaboracion_c']['labelValue']='Sucursal';
$dictionary['Opportunity']['fields']['sucursal_colaboracion_c']['dependency']='';
$dictionary['Opportunity']['fields']['sucursal_colaboracion_c']['visibility_grid']='';
$dictionary['Opportunity']['fields']['sucursal_colaboracion_c']['required']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_estado_de_seguimiento_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['estado_de_seguimiento_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['estado_de_seguimiento_c']['labelValue']='Estado de Seguimiento a Prospecto';
$dictionary['Opportunity']['fields']['estado_de_seguimiento_c']['dependency']='';
$dictionary['Opportunity']['fields']['estado_de_seguimiento_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_probability.php

 // created: 2017-07-20 10:22:15
$dictionary['Opportunity']['fields']['probability']['audited']=true;
$dictionary['Opportunity']['fields']['probability']['massupdate']=false;
$dictionary['Opportunity']['fields']['probability']['comments']='The probability of closure';
$dictionary['Opportunity']['fields']['probability']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['probability']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['probability']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['probability']['reportable']=true;
$dictionary['Opportunity']['fields']['probability']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['probability']['min']=false;
$dictionary['Opportunity']['fields']['probability']['max']=false;
$dictionary['Opportunity']['fields']['probability']['disable_num_format']='';
$dictionary['Opportunity']['fields']['probability']['studio']=true;
$dictionary['Opportunity']['fields']['probability']['importable']='required';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_next_step.php

 // created: 2017-07-11 18:40:03
$dictionary['Opportunity']['fields']['next_step']['len']='255';
$dictionary['Opportunity']['fields']['next_step']['audited']=false;
$dictionary['Opportunity']['fields']['next_step']['massupdate']=false;
$dictionary['Opportunity']['fields']['next_step']['comments']='The next step in the sales process';
$dictionary['Opportunity']['fields']['next_step']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['next_step']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['next_step']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['next_step']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.74',
  'searchable' => true,
);
$dictionary['Opportunity']['fields']['next_step']['calculated']=false;
$dictionary['Opportunity']['fields']['next_step']['required']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_sales_stage.php

 // created: 2017-07-20 10:22:15
$dictionary['Opportunity']['fields']['sales_stage']['required']=true;
$dictionary['Opportunity']['fields']['sales_stage']['audited']=true;
$dictionary['Opportunity']['fields']['sales_stage']['massupdate']=true;
$dictionary['Opportunity']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['Opportunity']['fields']['sales_stage']['importable']='required';
$dictionary['Opportunity']['fields']['sales_stage']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['sales_stage']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['sales_stage']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sales_stage']['reportable']=true;
$dictionary['Opportunity']['fields']['sales_stage']['calculated']=false;
$dictionary['Opportunity']['fields']['sales_stage']['dependency']=false;
$dictionary['Opportunity']['fields']['sales_stage']['studio']=true;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_etapa_actual_c.php

 // created: 2017-08-23 18:56:06
$dictionary['Opportunity']['fields']['etapa_actual_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['etapa_actual_c']['labelValue']='Etapa Actual';
$dictionary['Opportunity']['fields']['etapa_actual_c']['dependency']='';
$dictionary['Opportunity']['fields']['etapa_actual_c']['visibility_grid']='';
$dictionary['Opportunity']['fields']['etapa_actual_c']['required']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_date_closed_timestamp.php

 // created: 2017-07-20 10:22:15
$dictionary['Opportunity']['fields']['date_closed_timestamp']['audited']=false;
$dictionary['Opportunity']['fields']['date_closed_timestamp']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['date_closed_timestamp']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['date_closed_timestamp']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['date_closed_timestamp']['formula']='timestamp($date_closed)';
$dictionary['Opportunity']['fields']['date_closed_timestamp']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['date_closed_timestamp']['min']=false;
$dictionary['Opportunity']['fields']['date_closed_timestamp']['max']=false;
$dictionary['Opportunity']['fields']['date_closed_timestamp']['disable_num_format']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_name.php

 // created: 2017-07-11 17:49:40
$dictionary['Opportunity']['fields']['name']['audited']=false;
$dictionary['Opportunity']['fields']['name']['massupdate']=false;
$dictionary['Opportunity']['fields']['name']['comments']='Name of the opportunity';
$dictionary['Opportunity']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.65',
  'searchable' => true,
);
$dictionary['Opportunity']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_vendedor_id_c.php

 // created: 2017-08-23 18:56:06
$dictionary['Opportunity']['fields']['vendedor_id_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_vol_estimado_ventas_c.php

 // created: 2017-08-23 18:56:06
$dictionary['Opportunity']['fields']['vol_estimado_ventas_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['vol_estimado_ventas_c']['labelValue']='Volumen Estimada de Ventas';
$dictionary['Opportunity']['fields']['vol_estimado_ventas_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['vol_estimado_ventas_c']['enforced']='';
$dictionary['Opportunity']['fields']['vol_estimado_ventas_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_user_id_c.php

 // created: 2017-07-11 19:12:27

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_worst_case.php

 // created: 2017-07-20 10:22:14
$dictionary['Opportunity']['fields']['worst_case']['audited']=true;
$dictionary['Opportunity']['fields']['worst_case']['massupdate']=true;
$dictionary['Opportunity']['fields']['worst_case']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['worst_case']['duplicate_merge_dom_value']=1;
$dictionary['Opportunity']['fields']['worst_case']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['worst_case']['calculated']=false;
$dictionary['Opportunity']['fields']['worst_case']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['worst_case']['default']='';

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_vendedor_c.php

 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['vendedor_c']['labelValue']='Vendedor';
$dictionary['Opportunity']['fields']['vendedor_c']['dependency']='';
$dictionary['Opportunity']['fields']['vendedor_c']['required']=0;

 
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Vardefs/sugarfield_monto_estimado_c.php

 // created: 2018-08-22 21:04:16
$dictionary['Opportunity']['fields']['monto_estimado_c']['labelValue']='Volumen estimado de ventas';
$dictionary['Opportunity']['fields']['monto_estimado_c']['enforced']='';
$dictionary['Opportunity']['fields']['monto_estimado_c']['dependency']='';
$dictionary['Opportunity']['fields']['monto_estimado_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
