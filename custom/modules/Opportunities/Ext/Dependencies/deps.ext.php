<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Dependencies/opp_disable_dep.ext.php

if (isset($dependencies['Opportunities'])) {
    foreach(
        array('commit_stage_readonly_set_value','best_worst_sales_stage_read_only','likely_case_copy_when_closed')
        as $dep
    ) {
        if (isset($dependencies['Opportunities'][$dep])) {
            unset($dependencies['Opportunities'][$dep]);
        }
    }

    // the `set_base_rate` dependency needs to use 'sales_status' here
    if (isset($dependencies['Opportunities']['set_base_rate'])) {
        $dependencies['Opportunities']['set_base_rate']['triggerFields'] = array('sales_status');
        $dependencies['Opportunities']['set_base_rate']['actions'][0]['params']['value'] =
            'ifElse(isForecastClosed($sales_status), $base_rate, currencyRate($currency_id))';
    }
}
?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Dependencies/prospects_opportunities_1_name_readonly.php


// $dependencies['Opportunities']['prospects_opportunities_1_name_readonly'] = array(
//   'hooks' => array("edit","view"),
//   'trigger' => 'not(equal($account_name,""))',
//   'triggerFields' => array('account_name'),
//   'onload' => true,
//   'actions' => array(
//     array(
//       'name' => 'ReadOnly',
//       'params' => array(
//         'target' => 'prospects_opportunities_1_name',
//         'value' => 'true',
//       ),
//     ),
//   ),
//   'notActions' => array(
//     array(
//       'name' => 'ReadOnly',
//       'params' => array(
//         'target' => 'prospects_opportunities_1_name',
//         'value' => 'false',
//       ),
//     ),
//   ),
// );

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Dependencies/prospects_opportunities_1_name_required.php


// $dependencies['Opportunities']['prospects_opportunities_1_name_required'] = array(
//   'hooks' => array("edit","view"),
//   'trigger' => 'true',
//   'triggerFields' => array('account_name'),
//   'onload' => true,
//   'actions' => array(
//     array(
//       'name' => 'SetRequired',
//       'params' => array(
//         'target' => 'prospects_opportunities_1_name',
//         'label' => 'prospects_opportunities_1_name_label',
//         'value' => 'equal($account_name,"")',
//       ),
//     ),
//   ),
//   'notActions' => array(
//     array(
//       'name' => 'SetRequired',
//       'params' => array(
//         'target' => 'prospects_opportunities_1_name',
//         'label' => 'prospects_opportunities_1_name_label',
//         'value' => 'false',
//       ),
//     ),
//   ),
// );

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Dependencies/account_name_required.php


// $dependencies['Opportunities']['account_name_required'] = array(
//   'hooks' => array("edit","view"),
//   'trigger' => 'true',
//   'triggerFields' => array('prospects_opportunities_1_name'),
//   'onload' => true,
//   'actions' => array(
//     array(
//       'name' => 'SetRequired',
//       'params' => array(
//         'target' => 'account_name',
//         'label' => 'account_name_label',
//         'value' => 'equal($prospects_opportunities_1_name,"")',
//       ),
//     ),
//   ),
//   'notActions' => array(
//     array(
//       'name' => 'SetRequired',
//       'params' => array(
//         'target' => 'account_name',
//         'label' => 'account_name_label',
//         'value' => 'false',
//       ),
//     ),
//   ),
// );

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Dependencies/lo_obras_opportunities_name_required.php


$dependencies['Opportunities']['lo_obras_opportunities_name_required'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('lo_obras_opportunities_name'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'lo_obras_opportunities_name',
        'label' => 'lo_obras_opportunities_name_label',
        'value' => 'equal($lo_obras_opportunities_name,"")',
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/Opportunities/Ext/Dependencies/account_name_readonly.php


// $dependencies['Opportunities']['account_name_readonly'] = array(
//   'hooks' => array("edit","view"),
//   'trigger' => 'not(equal($prospects_opportunities_1_name,""))',
//   'triggerFields' => array('prospects_opportunities_1_name'),
//   'onload' => true,
//   'actions' => array(
//     array(
//       'name' => 'ReadOnly',
//       'params' => array(
//         'target' => 'account_name',
//         'value' => 'true',
//       ),
//     ),
//   ),
//   'notActions' => array(
//     array(
//       'name' => 'ReadOnly',
//       'params' => array(
//         'target' => 'account_name',
//         'value' => 'false',
//       ),
//     ),
//   ),
// );

?>
