({
  extendsFrom:"CreateView",

  initialize: function () {
    var self = this;
    this._super("initialize", arguments);
    this.model.on('change:assigned_user_name',_.bind(this._changeAssignedUserName, this));
    this.model.on('change:etapa_c',_.bind(this._changeEtapa, this));
    this.model.on('change:vendedor_c',_.bind(this._changeVendedor, this));
    this.model.on('change:tipo_de_colaboracion_c',_.bind(this._changeTipoColaboracion, this));
    this.model.addValidationTask('perdida',_.bind(this._doValidatePerdida, this));
  },

  _changeAssignedUserName: function (model, value) {
    var self = this;
    var idUser = self.model.get('assigned_user_id');
    var url_users =  app.api.buildURL('Users/'+idUser);
    app.api.call('read', url_users, {}, {
      success: function(data){
        if(data){
          self.model.set({sucursal_c:data.sucursal_c});
        }
      }
    });
  },

  _changeEtapa: function (model, value) {
    var self = this;
    var etapa = self.model.get('etapa_c');
    var probabilidad_de_venta_c = '';
    switch (etapa) {
      case 'prospeccion': probabilidad_de_venta_c = 10; break;
      case 'ganado_ventas_pendientes': probabilidad_de_venta_c = 100; break;
      default: probabilidad_de_venta_c = ''; break;
    }
    self.model.set({probabilidad_de_venta_c:probabilidad_de_venta_c});
  },

  _changeVendedor: function (model, value) {
    var self = this;
    var idUser = self.model.get('vendedor_id_c');
    if(!_.isEmpty(idUser)){
      var url_users =  app.api.buildURL('Users/'+idUser);
      app.api.call('read', url_users, {}, {
        success: function(data){
          if(data){
            self.model.set({sucursal_colaboracion_c:data.sucursal_c});
          }
        }
      });
    }
  },

  _changeTipoColaboracion: function (model, value) {
    var self = this;
    var tipo_de_colaboracion = self.model.get('tipo_de_colaboracion_c');
    var comision_tienda_base_c = '';
    var comision_tienda_colabora_c = '';
    switch (tipo_de_colaboracion) {
      case 'tienda_atiende_cliente_vende_y_otra_entrega':
      case 'tienda_atiende_cliente_vende_y_otra_entrega_por_excepcion' :
      comision_tienda_base_c = 100;
      comision_tienda_colabora_c = 100;
      break;
      case 'tienda_atiende_cliente_vende_y_transfiere_proyecto_a_otro_vendedor_en_tienda_2_para_atencion_de_la_c':
      comision_tienda_base_c = 20;
      comision_tienda_colabora_c = 80;
      break;
    }
    self.model.set({comision_tienda_base_c:comision_tienda_base_c,comision_tienda_colabora_c:comision_tienda_colabora_c});
  },

  _doValidatePerdida: function (fields, errors, callback) {
    var self = this;
    if(self.model.get('etapa_c') === 'perdida' && _.isEmpty(self.model.get('motivo_de_perdida_c'))){
      errors['motivo_de_perdida_c'] = errors['motivo_de_perdida_c'] || {};
      errors['motivo_de_perdida_c'].required = true;
      errors['motivo_de_perdida_c']['Debe de tener un motivo'] = true;
    }
    callback(null, fields, errors);
  },

  _render: function (argument) {
    var self = this;
    if(this.model.isNotEmpty){
      if(!_.isEmpty(self.model.get('assigned_user_id'))){
        idUser = self.model.get('assigned_user_id');
        var url_users = app.api.buildURL('Users/'+idUser);
        app.api.call('read', url_users, {}, {
          success: function(data){
            if(data){
              self.model.set({sucursal_c:data.sucursal_c});
            }
          }
        });
      }
    }
    this._super('_render', arguments);
  },

})
