<?php
// created: 2018-09-15 05:21:37
$viewdefs['Opportunities']['base']['view']['subpanel-lo_obras_subpanel_lo_obras_opportunities'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'default' => true,
          'label' => 'LBL_LIST_OPPORTUNITY_NAME',
          'enabled' => true,
          'link' => true,
          'type' => 'name',
        ),
        1 => 
        array (
          'target_record_key' => 'account_id',
          'target_module' => 'Accounts',
          'default' => true,
          'label' => 'LBL_LIST_ACCOUNT_NAME',
          'enabled' => true,
          'name' => 'account_name',
          'link' => true,
          'type' => 'relate',
        ),
        2 => 
        array (
          'name' => 'sales_stage',
          'default' => true,
          'label' => 'LBL_LIST_SALES_STAGE',
          'enabled' => true,
          'type' => 'enum',
        ),
        3 => 
        array (
          'name' => 'date_closed',
          'default' => true,
          'label' => 'LBL_DATE_CLOSED',
          'enabled' => true,
          'type' => 'date',
        ),
        4 => 
        array (
          'type' => 'currency',
          'default' => true,
          'label' => 'LBL_LIKELY',
          'enabled' => true,
          'name' => 'amount',
        ),
        5 => 
        array (
          'name' => 'assigned_user_name',
          'target_record_key' => 'assigned_user_id',
          'target_module' => 'Employees',
          'default' => true,
          'label' => 'LBL_LIST_ASSIGNED_TO_NAME',
          'enabled' => true,
          'link' => true,
          'type' => 'relate',
        ),
        6 => 
        array (
          'type' => 'int',
          'default' => true,
          'label' => 'LBL_PROBABILITY',
          'enabled' => true,
          'name' => 'probability',
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);