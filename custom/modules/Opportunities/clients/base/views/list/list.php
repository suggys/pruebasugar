<?php
// created: 2018-09-15 05:21:23
$viewdefs['Opportunities']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_LIST_OPPORTUNITY_NAME',
          'enabled' => true,
          'default' => true,
          'related_fields' => 
          array (
            0 => 'total_revenue_line_items',
            1 => 'closed_revenue_line_items',
            2 => 'included_revenue_line_items',
          ),
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'lo_obras_opportunities_name',
          'label' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_LO_OBRAS_TITLE',
          'enabled' => true,
          'id' => 'LO_OBRAS_OPPORTUNITIESLO_OBRAS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'vol_estimado_ventas_c',
          'label' => 'LBL_VOL_ESTIMADO_VENTAS',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'etapa_actual_c',
          'label' => 'LBL_ETAPA_ACTUAL',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'date_closed',
          'label' => 'LBL_DATE_CLOSED',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'probabilidad_de_venta_c',
          'label' => 'LBL_PROBABILIDAD_DE_VENTA',
          'enabled' => true,
          'default' => false,
        ),
        6 => 
        array (
          'name' => 'monto_estimado_c',
          'label' => 'LBL_MONTO_ESTIMADO',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        7 => 
        array (
          'name' => 'moneda_c',
          'label' => 'LBL_MONEDA',
          'enabled' => true,
          'default' => false,
        ),
        8 => 
        array (
          'name' => 'etapa_inicio_c',
          'label' => 'LBL_ETAPA_INICIO',
          'enabled' => true,
          'default' => false,
        ),
        9 => 
        array (
          'name' => 'motivo_de_perdida_c',
          'label' => 'LBL_MOTIVO_DE_PERDIDA',
          'enabled' => true,
          'default' => false,
        ),
        10 => 
        array (
          'name' => 'etapa_c',
          'label' => 'LBL_ETAPA',
          'enabled' => true,
          'default' => false,
        ),
        11 => 
        array (
          'name' => 'sucursal_c',
          'label' => 'LBL_SUCURSAL',
          'enabled' => true,
          'default' => false,
        ),
        12 => 
        array (
          'name' => 'vendedor_c',
          'label' => 'LBL_VENDEDOR',
          'enabled' => true,
          'id' => 'VENDEDOR_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => false,
        ),
        13 => 
        array (
          'name' => 'estatus_c',
          'label' => 'LBL_ESTATUS',
          'enabled' => true,
          'default' => false,
        ),
        14 => 
        array (
          'name' => 'productos_c',
          'label' => 'LBL_PRODUCTOS',
          'enabled' => true,
          'sortable' => false,
          'default' => false,
        ),
        15 => 
        array (
          'name' => 'comision_tienda_base_c',
          'label' => 'LBL_COMISION_TIENDA_BASE',
          'enabled' => true,
          'default' => false,
        ),
        16 => 
        array (
          'name' => 'comision_tienda_colabora_c',
          'label' => 'LBL_COMISION_TIENDA_COLABORA',
          'enabled' => true,
          'default' => false,
        ),
        17 => 
        array (
          'name' => 'tipo_de_colaboracion_c',
          'label' => 'LBL_TIPO_DE_COLABORACION',
          'enabled' => true,
          'default' => false,
        ),
        18 => 
        array (
          'name' => 'sucursal_colaboracion_c',
          'label' => 'LBL_SUCURSAL_COLABORACION',
          'enabled' => true,
          'default' => false,
        ),
        19 => 
        array (
          'name' => 'clase_c',
          'label' => 'LBL_CLASE',
          'enabled' => true,
          'default' => false,
        ),
        20 => 
        array (
          'name' => 'account_name',
          'label' => 'LBL_LIST_ACCOUNT_NAME',
          'enabled' => true,
          'default' => false,
          'id' => 'ACCOUNT_ID',
          'link' => true,
          'sortable' => true,
        ),
        21 => 
        array (
          'name' => 'prospects_opportunities_1_name',
          'label' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_PROSPECTS_TITLE',
          'enabled' => true,
          'id' => 'PROSPECTS_OPPORTUNITIES_1PROSPECTS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => false,
        ),
        22 => 
        array (
          'name' => 'modified_by_name',
          'label' => 'LBL_MODIFIED',
          'enabled' => true,
          'default' => false,
          'id' => 'MODIFIED_USER_ID',
          'link' => true,
          'readonly' => true,
          'sortable' => true,
        ),
        23 => 
        array (
          'name' => 'probability',
          'label' => 'LBL_PROBABILITY',
          'enabled' => true,
          'default' => false,
        ),
        24 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'default' => false,
          'readonly' => true,
        ),
        25 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_USER',
          'enabled' => true,
          'default' => false,
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'sortable' => true,
        ),
        26 => 
        array (
          'name' => 'created_by_name',
          'label' => 'LBL_CREATED',
          'enabled' => true,
          'default' => false,
          'id' => 'CREATED_BY',
          'link' => true,
          'readonly' => true,
          'sortable' => true,
        ),
        27 => 
        array (
          'name' => 'next_step',
          'label' => 'LBL_NEXT_STEP',
          'enabled' => true,
          'default' => false,
        ),
        28 => 
        array (
          'name' => 'lead_source',
          'label' => 'LBL_LEAD_SOURCE',
          'enabled' => true,
          'default' => false,
        ),
        29 => 
        array (
          'name' => 'opportunity_type',
          'label' => 'LBL_TYPE',
          'enabled' => true,
          'default' => false,
        ),
        30 => 
        array (
          'name' => 'team_name',
          'label' => 'LBL_LIST_TEAM',
          'enabled' => true,
          'default' => false,
          'type' => 'teamset',
        ),
      ),
    ),
  ),
);