<?php
// created: 2018-09-15 05:21:23
$viewdefs['Opportunities']['base']['view']['record'] = array (
  'buttons' => 
  array (
    0 => 
    array (
      'type' => 'button',
      'name' => 'cancel_button',
      'label' => 'LBL_CANCEL_BUTTON_LABEL',
      'css_class' => 'btn-invisible btn-link',
      'showOn' => 'edit',
      'events' => 
      array (
        'click' => 'button:cancel_button:click',
      ),
    ),
    1 => 
    array (
      'type' => 'rowaction',
      'event' => 'button:save_button:click',
      'name' => 'save_button',
      'label' => 'LBL_SAVE_BUTTON_LABEL',
      'css_class' => 'btn btn-primary',
      'showOn' => 'edit',
      'acl_action' => 'edit',
    ),
    2 => 
    array (
      'type' => 'actiondropdown',
      'name' => 'main_dropdown',
      'primary' => true,
      'showOn' => 'view',
      'buttons' => 
      array (
        0 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:edit_button:click',
          'name' => 'edit_button',
          'label' => 'LBL_EDIT_BUTTON_LABEL',
          'acl_action' => 'edit',
        ),
        1 => 
        array (
          'type' => 'shareaction',
          'name' => 'share',
          'label' => 'LBL_RECORD_SHARE_BUTTON',
          'acl_action' => 'view',
        ),
        2 => 
        array (
          'type' => 'pdfaction',
          'name' => 'download-pdf',
          'label' => 'LBL_PDF_VIEW',
          'action' => 'download',
          'acl_action' => 'view',
        ),
        3 => 
        array (
          'type' => 'pdfaction',
          'name' => 'email-pdf',
          'label' => 'LBL_PDF_EMAIL',
          'action' => 'email',
          'acl_action' => 'view',
        ),
        4 => 
        array (
          'type' => 'divider',
        ),
        5 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:find_duplicates_button:click',
          'name' => 'find_duplicates_button',
          'label' => 'LBL_DUP_MERGE',
          'acl_action' => 'edit',
        ),
        6 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:duplicate_button:click',
          'name' => 'duplicate_button',
          'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
          'acl_module' => 'Opportunities',
          'acl_action' => 'create',
        ),
        7 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:historical_summary_button:click',
          'name' => 'historical_summary_button',
          'label' => 'LBL_HISTORICAL_SUMMARY',
          'acl_action' => 'view',
        ),
        8 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:audit_button:click',
          'name' => 'audit_button',
          'label' => 'LNK_VIEW_CHANGE_LOG',
          'acl_action' => 'view',
        ),
        9 => 
        array (
          'type' => 'divider',
        ),
        10 => 
        array (
          'type' => 'rowaction',
          'event' => 'button:delete_button:click',
          'name' => 'delete_button',
          'label' => 'LBL_DELETE_BUTTON_LABEL',
          'acl_action' => 'delete',
        ),
      ),
    ),
    3 => 
    array (
      'name' => 'sidebar_toggle',
      'type' => 'sidebartoggle',
    ),
  ),
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'header' => true,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'picture',
          'type' => 'avatar',
          'size' => 'large',
          'dismiss_label' => true,
          'readonly' => true,
        ),
        1 => 
        array (
          'name' => 'name',
          'related_fields' => 
          array (
            0 => 'total_revenue_line_items',
            1 => 'closed_revenue_line_items',
            2 => 'included_revenue_line_items',
          ),
        ),
        2 => 
        array (
          'name' => 'favorite',
          'label' => 'LBL_FAVORITE',
          'type' => 'favorite',
          'dismiss_label' => true,
        ),
        3 => 
        array (
          'name' => 'follow',
          'label' => 'LBL_FOLLOW',
          'type' => 'follow',
          'readonly' => true,
          'dismiss_label' => true,
        ),
      ),
    ),
    1 => 
    array (
      'name' => 'panel_body',
      'label' => 'LBL_RECORD_BODY',
      'columns' => 2,
      'labels' => true,
      'labelsOnTop' => true,
      'placeholders' => true,
      'newTab' => true,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'lo_obras_opportunities_name',
        ),
        1 => 
        array (
        ),
        2 => 
        array (
          'name' => 'etapa_actual_c',
          'label' => 'LBL_ETAPA_ACTUAL',
        ),
        3 => 
        array (
          'name' => 'date_closed',
          'related_fields' => 
          array (
            0 => 'date_closed_timestamp',
          ),
        ),
        4 => 
        array (
          'name' => 'clase_c',
          'label' => 'LBL_CLASE',
        ),
        5 => 
        array (
          'name' => 'productos_c',
          'studio' => 'visible',
          'label' => 'LBL_PRODUCTOS',
        ),
        6 => 
        array (
          'name' => 'estatus_c',
          'label' => 'LBL_ESTATUS',
          'readonly' => true,
        ),
        7 => 
        array (
          'name' => 'assigned_user_name',
        ),
        8 => 
        array (
          'name' => 'sucursal_c',
          'label' => 'LBL_SUCURSAL',
          'readonly' => true,
        ),
        9 => 
        array (
          'name' => 'etapa_c',
          'label' => 'LBL_ETAPA',
        ),
        10 => 
        array (
          'name' => 'motivo_de_perdida_c',
          'label' => 'LBL_MOTIVO_DE_PERDIDA',
        ),
        11 => 
        array (
          'name' => 'etapa_inicio_c',
          'label' => 'LBL_ETAPA_INICIO',
        ),
        12 => 
        array (
          'name' => 'moneda_c',
          'label' => 'LBL_MONEDA',
        ),
        13 => 
        array (
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'name' => 'monto_estimado_c',
          'label' => 'LBL_MONTO_ESTIMADO',
        ),
        14 => 
        array (
          'name' => 'probabilidad_de_venta_c',
          'label' => 'LBL_PROBABILIDAD_DE_VENTA',
        ),
        15 => 'next_step',
        16 => 
        array (
          'name' => 'opportunity_type',
        ),
        17 => 
        array (
        ),
        18 => 
        array (
          'name' => 'description',
          'span' => 12,
        ),
      ),
    ),
    2 => 
    array (
      'name' => 'panel_hidden',
      'label' => 'LBL_RECORD_SHOWMORE',
      'hide' => true,
      'labelsOnTop' => true,
      'placeholders' => true,
      'columns' => 2,
      'newTab' => false,
      'panelDefault' => 'expanded',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'vendedor_c',
          'studio' => 'visible',
          'label' => 'LBL_VENDEDOR',
        ),
        1 => 
        array (
          'name' => 'sucursal_colaboracion_c',
          'label' => 'LBL_SUCURSAL_COLABORACION',
        ),
        2 => 
        array (
          'name' => 'tipo_de_colaboracion_c',
          'label' => 'LBL_TIPO_DE_COLABORACION',
        ),
        3 => 
        array (
          'name' => 'comision_tienda_base_c',
          'label' => 'LBL_COMISION_TIENDA_BASE',
          'readonly' => true,
        ),
        4 => 
        array (
          'name' => 'comision_tienda_colabora_c',
          'label' => 'LBL_COMISION_TIENDA_COLABORA',
          'readonly' => true,
        ),
        5 => 
        array (
        ),
      ),
    ),
    3 => 
    array (
      'newTab' => true,
      'panelDefault' => 'expanded',
      'name' => 'LBL_RECORDVIEW_PANEL1',
      'label' => 'LBL_RECORDVIEW_PANEL1',
      'columns' => 2,
      'labelsOnTop' => 1,
      'placeholders' => 1,
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'prospects_opportunities_1_name',
          'label' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_PROSPECTS_TITLE',
        ),
        1 => 
        array (
          'name' => 'account_name',
          'related_fields' => 
          array (
            0 => 'account_id',
          ),
        ),
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'useTabs' => true,
  ),
);