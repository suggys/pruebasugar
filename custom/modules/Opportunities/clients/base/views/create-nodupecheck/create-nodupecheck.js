({
  extendsFrom: 'CreateNodupecheckView',

  initialize: function (options) {
    var self = this;
    this._super('initialize', arguments);
    this.model.on('change:assigned_user_name',_.bind(this._changeAssignedUserName, this));
  },

  _render: function (argument) {
    var self = this;
    if(this.model.isNotEmpty){
      if(!_.isEmpty(self.model.get('assigned_user_id'))){
        idUser = self.model.get('assigned_user_id');
        var url_users = app.api.buildURL('Users/'+idUser);
        app.api.call('read', url_users, {}, {
          success: function(data){
            if(data){
              self.model.set({sucursal_c:data.sucursal_c});
            }
          }
        });
      }
    }
    this._super('_render', arguments);
  },

  _changeAssignedUserName: function (model, value) {
    var self = this;
    var idUser = self.model.get('assigned_user_id');
    var url_users =  app.api.buildURL('Users/'+idUser);
    app.api.call('read', url_users, {}, {
      success: function(data){
        if(data){
          self.model.set({sucursal_c:data.sucursal_c});
        }
      }
    });
  },

})
