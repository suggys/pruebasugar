<?php
class OpportunitiesLogicHooksManager{
  protected static $fetchedRow = array();

  public function beforeSave($bean, $event, $arguments){
    if ( !empty($bean->id) ) {
      self::$fetchedRow[$bean->id] = $bean->fetched_row;
    }
    return true;
  }

  public function afterSave($bean, $event, $arguments) {
    require_once 'modules/Configurator/Configurator.php';
    $envio = false;
    $configKey = 'lowes_settings_monto_maximo_proyectos';
    $admin = new Administration();
    $admin->retrieveSettings();
    if(isset($admin->settings[$configKey]) && $admin->settings[$configKey] > 0){
      if($bean->monto_estimado_c > $admin->settings[$configKey]
      && self::$fetchedRow[$bean->id]['monto_estimado_c'] != $bean->monto_estimado_c)
      {
        $vendedor = BeanFactory::getBean('Users', $bean->assigned_user_id, array('disable_row_level_security' => true));
        $gerente = BeanFactory::getBean('Users', $vendedor->reports_to_id, array('disable_row_level_security' => true));
        $envio = self::enviaEmail($bean,$gerente->email1);
      }
    }
    return $envio;
  }

  private function enviaEmail($bean, $to_email_address){
    require_once('modules/EmailTemplates/EmailTemplate.php');
    require_once 'modules/Configurator/Configurator.php';

    $configuratorObj = new Configurator();
    $configuratorObj->loadConfig();
    $admin = new Administration();
    $admin->retrieveSettings();
    $fromaddress = $admin->settings['notify_fromaddress'];
    $fromname = $admin->settings['notify_fromname'];
    $idEmailTemplate = $admin->settings['lowes_settings_emailtemplate_monto_maximo_proye'];

    $mail = new SugarPHPMailer();
    $mail->CharSet = "UTF-8";
    $mail->prepForOutbound();
    $mail->setMailerForSystem();
    $mail->From = $fromaddress;
    $mail->FromName = $fromname;
    $mail->ClearAllRecipients();
    $mail->ClearReplyTos();

    $emailTemp = BeanFactory::getBean('EmailTemplates',$idEmailTemplate);

    require_once "include/workflow/alert_utils.php";
    $emailTemp->subject = trim(parse_alert_template($bean, $emailTemp->subject));
    $temp = parse_alert_template($bean, $emailTemp->body_html);
    $emailTemp->body_html = str_replace('{$proyecto-name}', $bean->name, $temp);
    $temp = parse_alert_template($bean, $emailTemp->body_html);

    $usr_actual = BeanFactory::getBean("Users",$bean->assigned_user_id);
    $usr_actual->retrieve();
    $Gte_vtas = BeanFactory::getBean("Users",$usr_actual->reports_to_id);
    $Gte_vtas->retrieve();
    $gte_ventas_name = $Gte_vtas->first_name . " " . $Gte_vtas->last_name;

    $url = '<a href="https://'.$configuratorObj->config['host_name'].'/#'.'Opportunities/'.$bean->id.'">aqu&iacute;</a>';
    $emailTemp->body_html = str_replace('{$url-proyecto}', $url, $temp);
    $emailTemp->body_html = str_replace('{$gte_ventas_com}', $gte_ventas_name, $emailTemp->body_html);
    if(!empty($emailTemp->body_html)){
      $htmlBody = $emailTemp->body_html;
      $mail->isHTML(true);
      $mail->Body = from_html($htmlBody);
      $mail->Subject = $emailTemp->subject;
      $mail->AddAddress($to_email_address);
      $success = @$mail->Send();
      return $success;
    }
  }

}
