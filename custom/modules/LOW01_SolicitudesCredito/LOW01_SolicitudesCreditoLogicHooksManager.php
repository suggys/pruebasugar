<?php
require_once ('custom/modules/EmailTemplates/NotifierHelper.php');
require_once ('modules/Configurator/Configurator.php');
require_once 'custom/modules/MRX1_Configuraciones/CustomMRX1Configuraciones.php';

class LOW01_SolicitudesCreditoLogicHooksManager{

  protected static $fetchedRow = array();
  public function beforeSave($bean, $event, $arguments){
    if ( !empty($bean->id) ) {
      self::$fetchedRow[$bean->id] = $bean->fetched_row;
    }

    if(empty($bean->name)){
      $bean->name = $this->createName($bean);
    }

    $notificacionesEmail = new NotifierHelper();
    $admin = new Administration();
    $admin->retrieveSettings();

    $merxConfigurator = new CustomMRX1Configuraciones();
    $merxConfigurator->retrieveSettings();
    // -- Crea folio consecutivo
    $configKey = 'SolCredito_consecutivo';
    if(self::$fetchedRow[$bean->id]['id'] != $bean->id){
      $folio = 1;

      if(isset($merxConfigurator->settings[$configKey]) && $merxConfigurator->settings[$configKey] > 0){
        $folio = $merxConfigurator->settings[$configKey] + 1;
      }
      else{
        $folio = 1;
      }
      $merxConfigurator->saveSetting("SolCredito_consecutivo", $folio);
      $bean->folio_bc = $folio;
    }

    if(self::$fetchedRow[$bean->id]['estado_solicitud_c'] !== $bean->estado_solicitud_c
    && strtolower(substr($bean->estado_solicitud_c,0,6)) === "aproba"
    && (
      $bean->estado_solicitud_c != 'Aprobado_por_Asesor_Comercial'
      || $bean->estado_solicitud_c != 'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito'
    )){
      $configKey = 'lowes_settings_emailtem_notifica_aprob';
      $admin->retrieveSettings();
      $usersTo = $notificacionesEmail->getUsersParentsFromBean($bean, true);
      array_pop($usersTo);
      $bean->field_defs['usuario_notificar'] = [
        'type' => "varchar",
        'name' => "usuario_notificar",
      ];
      foreach ($usersTo as $user) {
        $bean->usuario_notificar = $user->user_name;
        $notificacionesEmail->sendMessage($bean,['to' => [$user]], $admin->settings[$configKey]);
      }
      unset($bean->field_defs['usuario_notificar']);
    }
    if(self::$fetchedRow[$bean->id]['estado_solicitud_c'] !== $bean->estado_solicitud_c
    && strtolower(substr($bean->estado_solicitud_c,0,9)) === "rechazado" && $bean->estado_solicitud_c != 'Rechazado_por_Asesor_Comercial'){

      $bean->assigned_user_id = $bean->created_by;
      /*$bean->obs_asesor_comercial_c = "";
      $bean->obs_analista_credito_c = $bean->estado_solicitud_c === 'Rechazado_por_Analista_de_Credito' ? $bean->obs_analista_credito_c :  "";
      $bean->obs_gerente_vtas_com_c = $bean->estado_solicitud_c === 'Rechazado_por_Gerente_de_Ventas_Comerciales' ? $bean->obs_gerente_vtas_com_c :  "";
      $bean->obs_admon_credito_c = $bean->estado_solicitud_c === 'Rechazado_por_Administrador_de_Credito' ? $bean->obs_admon_credito_c :  "";
      $bean->obs_gerente_credito_c = $bean->estado_solicitud_c === 'Rechazado_por_Gerente_de_Credito' ? $bean->obs_gerente_credito_c :  "";
      $bean->obs_director_finanzas_c = $bean->estado_solicitud_c === 'Rechazado_por_Director_de_Finanzas' ? $bean->obs_director_finanzas_c :  "";
      $bean->obs_vicep_comercial_c = $bean->estado_solicitud_c === 'Rechazado_por_Vicepresidencia_Comercial' ? $bean->obs_vicep_comercial_c :  "";
      $bean->obs_agente_externo_c = "";*/

      $configKey = 'lowes_settings_emailtemplate_notifica_rechazo';
      $admin->retrieveSettings();
      $usersTo = $notificacionesEmail->getUsersParentsFromBean($bean, true);
      array_pop($usersTo);
      $bean->field_defs['usuario_notificar'] = [
        'type' => "varchar",
        'name' => "usuario_notificar",
      ];
      foreach ($usersTo as $user) {
        $bean->usuario_notificar = $user->user_name;
        $notificacionesEmail->sendMessage($bean,['to' => [$user]], $admin->settings[$configKey]);
      }
      unset($bean->field_defs['usuario_notificar']);
    }

    if(
      self::$fetchedRow[$bean->id]['estado_solicitud_c'] !== $bean->estado_solicitud_c
      && (
        $bean->estado_solicitud_c != 'Aprobado_por_Asesor_Comercial'
        || $bean->estado_solicitud_c != 'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito'
      )
      // && strtolower(substr($bean->estado_solicitud_c,0,9)) != "rechazado"
    ){
      $r = $this->cambia_estado($bean, $admin);
      if(!empty($r["estatus"])){
        $bean->estado_solicitud_c = $r["estatus"];
        $bean->assigned_user_id = $r["new_assigned_user_id"];
      }
    }

    if(
      self::$fetchedRow[$bean->id]['estado_solicitud_c'] != $bean->estado_solicitud_c
      &&(
        $bean->estado_solicitud_c === "Aprobacion_por_Gerente_de_Credito"
        || $bean->estado_solicitud_c === "Aprobacion_por_Director_de_Finanzas"
        || $bean->estado_solicitud_c === "Aprobacion_por_Vicepresidencia_Comercial"
      )
    ){
      $roleAdministradorCredito = BeanFactory::getBean("ACLRoles", '7f4570ec-7aa3-11e6-8d90-06d48441b777');
      $es_aministrador_credito = true;
      $dataUsers = $this->getUsersByRole($roleAdministradorCredito,$es_aministrador_credito);
      if(count($dataUsers)){
        $bean->assigned_user_id = $dataUsers[0]['id'];
      }
    }

    $this->checkDocuments($bean);

    if(
    $bean->estado_solicitud_c && "Pendiente_por_Agente_Externo"
    && self::$fetchedRow[$bean->id]['termina_analisis_c'] != $bean->termina_analisis_c
    && $bean->termina_analisis_c == 1){
      $bean->estado_solicitud_c = "Pendiente_por_Administrador_de_Credito";
    }
  }

  public function ultimo_mensaje($bean_scred,$fetched_row){
    $comentarios = '';
    if(self::$fetchedRow[$bean_scred->id]['obs_asesor_comercial_c'] !== $bean_scred->obs_asesor_comercial_c) { $comentarios = $bean_scred->obs_asesor_comercial_c; }
    if(self::$fetchedRow[$bean_scred->id]['obs_gerente_vtas_com_c'] !== $bean_scred->obs_gerente_vtas_com_c) { $comentarios = $bean_scred->obs_gerente_vtas_com_c; }
    if(self::$fetchedRow[$bean_scred->id]['obs_analista_credito_c'] !== $bean_scred->obs_analista_credito_c) { $comentarios = $bean_scred->obs_analista_credito_c; }
    if(self::$fetchedRow[$bean_scred->id]['obs_admon_credito_c'] !== $bean_scred->obs_admon_credito_c) { $comentarios = $bean_scred->obs_admon_credito_c; }
    if(self::$fetchedRow[$bean_scred->id]['obs_gerente_credito_c'] !== $bean_scred->obs_gerente_credito_c) { $comentarios = $bean_scred->obs_gerente_credito_c; }
    if(self::$fetchedRow[$bean_scred->id]['obs_director_finanzas_c'] !== $bean_scred->obs_director_finanzas_c) { $comentarios = $bean_scred->obs_director_finanzas_c; }
    if(self::$fetchedRow[$bean_scred->id]['obs_vicep_comercial_c'] !== $bean_scred->obs_vicep_comercial_c) { $comentarios = $bean_scred->obs_vicep_comercial_c; }
    return $comentarios;
  }

  public function cambia_estado($b,$adm){
    global $current_user;
    $resp = [
      "new_assigned_user_id" => $b->assigned_user_id,
    ];
    if(
      $b->fetched_row['estado_solicitud_c'] != $b->estado_solicitud_c
    ){
      $adm->retrieveSettings();
      $monto_gc = $adm->settings["lowes_settings_monto_maximo_gc"];
      $monto_df = $adm->settings["lowes_settings_monto_maximo_df"];
      $estatus = false;
      if($b->estado_solicitud_c === "Aprobado_por_Asesor_Comercial"){
        $estatus = "Pendiente_por_Gerente_de_Ventas_Comerciales";
      }
      else if($b->estado_solicitud_c === "Aprobado_por_Gerente_de_Ventas_Comerciales"){
        $estatus = "Pendiente_por_Analista_de_Credito";
      }
      else if($b->estado_solicitud_c === "Aprobado_por_Analista_de_Credito"){
        $estatus = "Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito";
      }
      else if($b->estado_solicitud_c === "Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito"){
        $estatus = "Pendiente_por_Agente_Externo";
      }
      else if($b->estado_solicitud_c === "Aprobacion_por_Administrador_de_Credito"){
        $estatus = "Pendiente_por_Gerente_de_Credito";
      }
      else if($b->estado_solicitud_c === "Aprobacion_por_Gerente_de_Credito" && $b->limite_otorgado > $monto_gc ){
        $estatus = "Pendiente_por_Director_de_Finanzas";
      }
      else if($b->estado_solicitud_c === "Aprobacion_por_Director_de_Finanzas"  && $b->limite_otorgado > $monto_df ){
        $estatus = "Pendiente_por_Vicepresidencia_Comercial";
      }

      $resp["estatus"] = $estatus;
      if(
        $estatus != "Pendiente_por_Agente_Externo"
        && $estatus != "Aprobacion_por_Gerente_de_Credito"
        && $estatus != "Aprobacion_por_Director_de_Finanzas"
      ){
        $userx = BeanFactory::getBean('Users',$b->assigned_user_id);
        $supervisor_id = $userx->reports_to_id;
        if($estatus){
          $resp["new_assigned_user_id"] = $supervisor_id;
        }
      }
    }
    return $resp;
  }

  // public function afterRelationshipAdd($bean, $event, $arguments){
  // }

  public function AfterSave($bean, $event, $arguments){
    $notificacionesEmail = new NotifierHelper();
    $admin = new Administration();

    if(self::$fetchedRow[$bean->id]['enviar_pdf_c'] !== $bean->enviar_pdf_c && $bean->enviar_pdf_c == 1){
      $this->envia_email_solicitudCredito($bean);
    }

    if(self::$fetchedRow[$bean->id]['estado_solicitud_c'] !== $bean->estado_solicitud_c
    && $bean->estado_solicitud_c === "Pendiente_por_Agente_Externo")
    {
      $configKey = 'lowes_settings_emailtemplate_notifica_externo';
      $admin->retrieveSettings();
      $configurator = new Configurator();
      $configurator->loadConfig();
      $bean->field_defs['custom_url_c'] = [
        'type' => "varchar",
        'name' => "custom_url_c",
      ];
      $bean->custom_url_c = $configurator->config['site_url'].'/index.php?entryPoint=solicitudCredito&id='.$bean->id;
      // TODO JALAR TODOS LOS AGENTES RELACIONADOS A LA SOLICITUD
      //$externo = BeanFactory::getBean('Low03_AgentesExternos',$bean->low03_agentesexternos_id_c);
      //$externo->retrieve();
      if($bean->load_relationship('low01_solicitudescredito_low03_agentesexternos_1')) {
          $externos = $bean->low01_solicitudescredito_low03_agentesexternos_1->getBeans();
          $notificacionesEmail->sendMessage($bean,['to' => $externos],$admin->settings[$configKey],false);
      }
      unset($bean->field_defs['custom_url_c']);
    }

    if(self::$fetchedRow[$bean->id]['estado_solicitud_c'] !== $bean->estado_solicitud_c
    && $bean->estado_solicitud_c === "Finalizado_por_Solicitante"){
      $configKey = 'lowes_settings_email_evalua_sol_cred';
      $admin->retrieveSettings();
      $usersTo = [BeanFactory::getBean('Users', $bean->assigned_user_id)];
      $notificacionesEmail->sendMessage($bean,['to' => $usersTo], $admin->settings[$configKey]);
    }

    if(self::$fetchedRow[$bean->id] && self::$fetchedRow[$bean->id]['assigned_user_id'] !== $bean->assigned_user_id){
      $configKey = 'lowes_settings_email_evalua_sol_cred';
      $admin->retrieveSettings();
      $usersTo = [BeanFactory::getBean('Users', $bean->assigned_user_id)];
      $notificacionesEmail->sendMessage($bean,['to' => $usersTo], $admin->settings[$configKey]);
    }

  }

  public function envia_email_solicitudCredito($bean){
    $notificacionesEmail = new NotifierHelper();
    $configKey = 'lowes_settings_email_formulario_sol_cre';
    $admin = new Administration();
    $admin->retrieveSettings();
    $configurator = new Configurator();
    $configurator->loadConfig();

    $beans = $this->getBeanSendToSolicitud($bean);
    $bean->field_defs['custom_url_c'] = [
      'type' => "varchar",
      'name' => "custom_url_c",
    ];

    $bean->custom_url_c = $configurator->config['site_url'].'/index.php?entryPoint=solicitudCredito&id='.$bean->id;
    if(count($beans)){
      foreach ($beans as $beanSendTo) {
        $notificacionesEmail->sendMessage($bean,['to' => [$beanSendTo]],$admin->settings[$configKey]);
      }
    }

    unset($bean->field_defs['custom_url_c']);
  }


  public function checkDocuments($bean)
  {
    if(!empty($bean->document_id_c) && empty($bean->fetched_row['document_id_c'])){
      $bean->doc_formato_solicitud = '1';
    }

    if(!empty($bean->document_id1_c) && empty($bean->fetched_row['document_id1_c'])){
      $bean->doc_aval = '1';
    }

    if(!empty($bean->document_id2_c) && empty($bean->fetched_row['document_id2_c'])){
      $bean->doc_acta_constitutiva = '1';
    }

    if(!empty($bean->document_id3_c) && empty($bean->fetched_row['document_id3_c'])){
      $bean->doc_comprobante_domicilio = '1';
    }

    if(!empty($bean->document_id4_c) && empty($bean->fetched_row['document_id4_c'])){
      $bean->doc_copia_acta_representante = '1';
    }

    if(!empty($bean->document_id5_c) && empty($bean->fetched_row['document_id5_c'])){
      $bean->doc_copia_estado_cuenta = '1';
    }

    if(!empty($bean->document_id6_c) && empty($bean->fetched_row['document_id6_c'])){
      $bean->doc_copia_estado_finan_rec = '1';
    }

    if(!empty($bean->document_id7_c) && empty($bean->fetched_row['document_id7_c'])){
      $bean->doc_copia_id_aval = '1';
    }

    if(!empty($bean->document_id8_c) && empty($bean->fetched_row['document_id8_c'])){
      $bean->doc_copia_id_solicitante = '1';
    }

    if(!empty($bean->document_id9_c) && empty($bean->fetched_row['document_id9_c'])){
      $bean->doc_copia_pago_impuestos = '1';
    }

    if(!empty($bean->document_id10_c) && empty($bean->fetched_row['document_id10_c'])){
      $bean->doc_copia_rfc = '1';
    }

    if(!empty($bean->document_id11_c) && empty($bean->fetched_row['document_id11_c'])){
      $bean->doc_formato_buro = '1';
    }

    if(!empty($bean->document_id12_c) && empty($bean->fetched_row['document_id12_c'])){
      $bean->copia_estado_financiero_anter = '1';
    }

  }

  public function createName($bean)
  {
    $name = "";
    // TODO sacar las solicitudes por un sugar query
    if(!empty($bean->low01_solicitudescredito_prospectsprospects_ida)){
      $prospect = BeanFactory::getBean("Prospects", $bean->low01_solicitudescredito_prospectsprospects_ida);
      if($prospect->id && $prospect->id){
        $solicitudes = $this->getSolicitudes($prospect, "low01_solicitudescredito_prospects");
        $name = $prospect->last_name . "-" . (count($solicitudes)+1);
      }
    }
    if(!empty($bean->low01_solicitudescredito_accountsaccounts_ida)){
      $account = BeanFactory::getBean("Accounts", $bean->low01_solicitudescredito_accountsaccounts_ida);
      if($account->id && $account->id){
        $solicitudes = $this->getSolicitudes($account, "low01_solicitudescredito_accounts");
        $name = $account->name . "-" . (count($solicitudes)+1);
      }
    }
    return $name;
  }

  public function getSolicitudes($bean, $linkName)
  {
    $saldoDeudor = 0;
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $joinName = $sugarQuery->join($linkName, array('team_security'=>false))->joinName();
    $sugarQuery->where()->equals('id', $bean->id);
    $sugarQuery->select(array("$joinName.id"));
    $results = $sugarQuery->execute();
    return $results;
  }

  public function getUsersByRole($role, $es_admin_credito = false)
  {
    $sugarQuery = new SugarQuery();
    $bean = BeanFactory::newBean('Users');
    $sugarQuery->from($bean, array('team_security' => false));
    $aclroles = $sugarQuery->join('aclroles', array('team_security' => false))->joinName();

    if($es_admin_credito){
        $sugarQuery->where()
        ->equals( 'status', 'Active' )
        ->equals( 'tipo_usuario_c', 'admincredito' )
        ->equals( "$aclroles.id", $role->id);
    }
    else{
        $sugarQuery->where()
        ->equals( 'status', 'Active' )
        ->equals( "$aclroles.id", $role->id);
    }

    $sugarQuery->select(array('id'));
    $results = $sugarQuery->execute();
    return $results;
  }

  public function getBeanSendToSolicitud($bean)
  {
    $beans = [];
    if($bean->env_email_default_c){
      $mod_email = ($bean->low01_solicitudescredito_accountsaccounts_ida) ? 'Accounts' : 'Prospects';
      $account_id = $bean->low01_solicitudescredito_accountsaccounts_ida;
      $prospect_id = $bean->low01_solicitudescredito_prospectsprospects_ida;
      $solicitante =  null;
      if($mod_email === 'Accounts'){
        $solicitante = BeanFactory::getBean('Accounts',$account_id);
      }
      else{
        $solicitante = BeanFactory::getBean('Prospects',$prospect_id);
      }
      $beans[] = $solicitante;
    }

    if($bean->env_created_by_c && $bean->created_by){
      $user = BeanFactory::getBean('Users', $bean->created_by);
      if($user->id){
        $beans[] = $user;
      }
    }

    if($bean->env_contacts_sel_c){
      $bean->load_relationship('low01_solicitudescredito_contacts_1');
      $contacts = $bean->low01_solicitudescredito_contacts_1->getBeans();
      if(count($contacts)){
          foreach ($contacts as $contact) {
            $beans[] = $contact;
          }
      }
    }
    return $beans;
  }

}
