({
	/* Autor: Obed Gonzalez	-- @kobedinho
	*  Ocultando botondes de subpanel de agentes externos
	*/

    	extendsFrom: 'SubpanelsLayout',

    	showSubpanel: function(linkName) {
    		var self = this;
        	self.currentModel = app.controller.context.get('model');
        	//req = app.api.call('read', '/rest/v10/CMP_Compromisos/'+ currentModel.get('id'))
        	self.currentModel.on('sync',function(){
        		self._super('showSubpanel', [linkName]);
        		_.each(self._components, function(component) {
                	var link = component.context.get('link');
                	if(self._hideButtons(app.user.get('roles'),app.user.get('type'))){
                		//component.remove();
                		component.$el.find('.subpanel-controls').hide();
                 }
            	});
        	}, self)
        
	},
	_dispose: function(){
		this._super('_dispose');
	},
	_hideButtons: function(roles, user_type) {
	    var self = this;
	    var response = false;
	    if( !_.filter(roles, function(rol) {
            return rol.toLowerCase() == "administrador de credito";
        }).length && user_type !== 'admin'){
	    		response = true;
	    }
	    return response;
	},
})