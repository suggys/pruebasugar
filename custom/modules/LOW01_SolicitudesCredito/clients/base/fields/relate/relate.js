({
    extendsFrom: "RelateField",
    openSelectDrawer: function() {
        var self = this;
        if(self.getSearchModule() === "Documents" && !self.def.link){
          if(this.view.createMode){
            app.alert.show("document_field_create_mode",{
              level:'warning',
              messages: 'Para poder agregar el documento debe de guardar el registro.'
            });
          }
          else{
            app.router.navigate('#bwc/index.php?module=Documents&parent_type=LOW01_SolicitudesCredito&parent_name='+self.model.get('name')+'&parent_id='+self.model.get('id')+'&parent_field_name='+self.name+'&return_module=LOW01_SolicitudesCredito&return_id='+self.model.get('id')+'&return_name='+self.model.get('id')+'&action=EditView',{trigger: true})
          }
        }
        else{
          app.drawer.open({
            layout: 'selection-list',
            context: {
              module: self.getSearchModule(),
              fields: self.getSearchFields(),
              filterOptions: self.getFilterOptions(),
              relLink: self.def.link,
              relModel: self.model,
              fieldName: self.name,
            }
          }, _.bind(this.setValue, this));
        }
    }
  })
