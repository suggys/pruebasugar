({
  extendsFrom: 'RecordView',

    events:{
      "click [name=enviar_solicitud_button]:not(.disabled)": "enviarSolicitud"
    },

    initialize: function() {
      var self = this;
      this._super('initialize', arguments);
      self.model.on('sync', function(model, value){
        self._disableField();
      });
      this.model.addValidationTask('doValidateAprobada',_.bind(this._doValidateAprobada, this));
    },

  _doValidateAprobada: function(fields, errors, callback) {
    var self = this;
    var modelSynced = app.data.createBean(self.model.module, self.model.getSynced());
    var changedArray = app.utils.compareBeans(self.model, modelSynced);
    var estadosAprobacion = ["Aprobacion_por_Gerente_de_Credito","Aprobacion_por_Director_de_Finanzas","Aprobacion_por_Vicepresidencia_Comercial"];
    if( _.contains(estadosAprobacion, modelSynced.get('estado_solicitud_c')) && changedArray.length){
      errors['estado_solicitud_c'] = errors['estado_solicitud_c'] || {};
      errors['estado_solicitud_c']['La solicitud no puede ser modificada una vez que ha sido aprobada'] = true;
    }
    callback(null, fields, errors);
  },

  _disableField: function () {
    var self = this;
    var rolesArray = app.user.get('roles');
    if(self.getField('reporte_analisis_agente_ext_c')){
      if(app.user.get('type') != 'admin'){
        if(
          !_.filter(rolesArray, function(rol) { return rol.toLowerCase() == "administrador de credito";}).length
        ){
          var reporte = self.model.get('reporte_analisis_agente_ext_c');
          var cita = self.model.get('cita_cliente_visita_c');
          var fecha = self.model.get('fecha_visita_ae_v_c');
          var analisis = self.model.get('analisis_fin_cre_c');
          var integracion = self.model.get('integracion_foto_c');
          var terminado = self.model.get('termina_analisis_c');
          var otro = self.model.get('otro_ae_c');
          var otro_texto = self.model.get('otro_texto_ae_c');
          var obs_agente = self.model.get('obs_agente_externo_c');
          if(
            !_.isEmpty(reporte) || cita || !_.isEmpty(fecha) || analisis
            || integracion || terminado || otro || !_.isEmpty(otro_texto)
            || !_.isEmpty(obs_agente)
          ) {
            if(
              !_.filter(rolesArray, function(rol) { return rol.toLowerCase() == "director de finanzas";}).length
              || !_.filter(rolesArray, function(rol) { return rol.toLowerCase() == "vicepresidencia comercial";}).length
            ){
              $("[data-panelname='LBL_RECORDVIEW_PANEL14']").hide();
            }
          } else {
            $("[data-panelname='LBL_RECORDVIEW_PANEL14']").hide();
          }
        }
      }
    }
  },

    enviarSolicitud: function () {
      var self = this;
      var url = app.api.buildURL('LOW01_SolicitudesCredito') + '/' + this.model.id + '/enviar_solicitud';
      app.alert.show("enviar_solicitud",{
        level:"process",
      });
      app.api.call('create', url, {} ,{
        success: _.bind(self.solicitudEnviada,self)
      });
    },

    solicitudEnviada: function () {
      app.alert.dismissAll();
      app.alert.show("solicitud_enviada",{
        level:"success",
        messages:"Solicitud enviada.",
        autoClose: true
      });
    }
})
