<?php
$module_name = 'LOW01_SolicitudesCredito';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'rowaction',
                'name' => 'enviar_solicitud_button',
                'label' => 'LBL_ENVIAR_SOLICITUD_BUTTON',
                'acl_action' => 'edit',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              5 => 
              array (
                'type' => 'divider',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'LOW01_SolicitudesCredito',
                'acl_action' => 'create',
              ),
              8 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              9 => 
              array (
                'type' => 'divider',
              ),
              10 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 
              array (
                'name' => 'name',
                'readonly' => true,
              ),
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'low01_solicitudescredito_prospects_name',
                'label' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE',
              ),
              1 => 
              array (
                'name' => 'low01_solicitudescredito_accounts_name',
              ),
              2 => 
              array (
                'name' => 'estado_solicitud_c',
                'label' => 'LBL_ESTADO_SOLICITUD_C',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'name' => 'tienda',
                'label' => 'LBL_TIENDA',
              ),
              5 => 
              array (
                'name' => 'promotor',
                'label' => 'LBL_PROMOTOR',
              ),
              6 => 
              array (
                'name' => 'fecha',
                'label' => 'LBL_FECHA',
              ),
              7 => 
              array (
                'name' => 'tc',
                'label' => 'LBL_TC',
              ),
              8 => 
              array (
                'name' => 'numero_de_cue_v_c',
                'label' => 'LBL_NUMERO_DE_CUE_V',
              ),
              9 => 
              array (
                'name' => 'autorizo',
                'label' => 'LBL_AUTORIZO',
              ),
              10 => 
              array (
                'name' => 'limite_otorgado',
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_LIMITE_OTORGADO',
              ),
              11 => 
              array (
                'name' => 'folio_bc',
                'label' => 'LBL_FOLIO_BC',
              ),
              12 => 
              array (
                'name' => 'ops_adv_env_sol_c',
                'label' => 'LBL_OPS_ADV_ENV_SOL_C',
              ),
              13 => 
              array (
              ),
              14 => 
              array (
                'name' => 'env_email_default_c',
                'label' => 'LBL_ENV_EMAIL_DEFAULT_C',
              ),
              15 => 
              array (
              ),
              16 => 
              array (
                'name' => 'env_contacts_sel_c',
                'label' => 'LBL_ENV_CONTACTS_SEL_C',
              ),
              17 => 
              array (
              ),
              18 => 
              array (
                'name' => 'env_created_by_c',
                'label' => 'LBL_ENV_CREATED_BY_C',
              ),
              19 => 
              array (
              ),
              20 => 
              array (
                'name' => 'documentacion_incompleta_c',
                'label' => 'LBL_DOCUMENTACION_INCOMPLETA',
              ),
              21 => 
              array (
              ),
            ),
          ),
          2 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL15',
            'label' => 'LBL_RECORDVIEW_PANEL15',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'obs_asesor_comercial_c',
                'studio' => 'visible',
                'label' => 'LBL_OBS_ASESOR_COMERCIAL_C',
              ),
              1 => 
              array (
                'name' => 'obs_gerente_vtas_com_c',
                'studio' => 'visible',
                'label' => 'LBL_OBS_GERENTE_VTAS_COM_C',
              ),
              2 => 
              array (
                'name' => 'obs_analista_credito_c',
                'studio' => 'visible',
                'label' => 'LBL_OBS_ANALISTA_CREDITO_C',
              ),
              3 => 
              array (
                'name' => 'obs_admon_credito_c',
                'studio' => 'visible',
                'label' => 'LBL_OBS_ADMON_CREDITO_C',
              ),
              4 => 
              array (
                'name' => 'obs_gerente_credito_c',
                'studio' => 'visible',
                'label' => 'LBL_OBS_GERENTE_CREDITO_C',
              ),
              5 => 
              array (
                'name' => 'obs_director_finanzas_c',
                'studio' => 'visible',
                'label' => 'LBL_OBS_DIRECTOR_FINANZAS_C',
              ),
              6 => 
              array (
                'name' => 'obs_vicep_comercial_c',
                'studio' => 'visible',
                'label' => 'LBL_OBS_VICEP_COMERCIAL_C',
              ),
              7 => 
              array (
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'apellido_paterno',
                'label' => 'LBL_APELLIDO_PATERNO',
              ),
              1 => 
              array (
                'name' => 'apellido_materno',
                'label' => 'LBL_APELLIDO_MATERNO',
              ),
              2 => 
              array (
                'name' => 'nombre_razon_social',
                'label' => 'LBL_NOMBRE_RAZON_SOCIAL',
              ),
              3 => 
              array (
                'name' => 'pagina_de_internet',
                'label' => 'LBL_PAGINA_DE_INTERNET',
              ),
              4 => 
              array (
                'name' => 'escritura',
                'label' => 'LBL_ESCRITURA',
              ),
              5 => 
              array (
                'name' => 'rpp',
                'label' => 'LBL_RPP',
              ),
              6 => 
              array (
                'name' => 'calle_domicilio_actual_fiscal',
                'label' => 'LBL_CALLE_DOMICILIO_ACTUAL_FISCAL',
              ),
              7 => 
              array (
                'name' => 'numero_exterior',
                'label' => 'LBL_NUMERO_EXTERIOR',
              ),
              8 => 
              array (
                'name' => 'numero_interior',
                'label' => 'LBL_NUMERO_INTERIOR',
              ),
              9 => 
              array (
                'name' => 'cruza_con',
                'label' => 'LBL_CRUZA_CON',
              ),
              10 => 
              array (
                'name' => 'colonia',
                'label' => 'LBL_COLONIA',
              ),
              11 => 
              array (
                'name' => 'municipio',
                'label' => 'LBL_MUNICIPIO',
              ),
              12 => 
              array (
                'name' => 'estado',
                'label' => 'LBL_ESTADO',
              ),
              13 => 
              array (
                'name' => 'cp',
                'label' => 'LBL_CP',
              ),
              14 => 
              array (
                'name' => 'domicilio',
                'label' => 'LBL_DOMICILIO',
              ),
              15 => 
              array (
                'name' => 'nombre_del_aval',
                'label' => 'LBL_NOMBRE_DEL_AVAL',
              ),
              16 => 
              array (
                'name' => 'pago_mensual',
                'label' => 'LBL_PAGO_MENSUAL',
              ),
              17 => 
              array (
                'name' => 'monto_deseado_c',
                'label' => 'LBL_MONTO_DESEADO',
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'anios_dom_act_v_c',
                'label' => 'LBL_ANIOS_DOM_ACT_V',
              ),
              1 => 
              array (
                'name' => 'meses_dom_act_v_c',
                'label' => 'LBL_MESES_DOM_ACT_V',
              ),
            ),
          ),
          5 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL3',
            'label' => 'LBL_RECORDVIEW_PANEL3',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'anios_dom_ant_v_c',
                'label' => 'LBL_ANIOS_DOM_ANT_V',
              ),
              1 => 
              array (
                'name' => 'meses_dom_ant_v_c',
                'label' => 'LBL_MESES_DOM_ANT_V',
              ),
            ),
          ),
          6 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL4',
            'label' => 'LBL_RECORDVIEW_PANEL4',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'clave_tel_ofi_v_c',
                'label' => 'LBL_CLAVE_TEL_OFI_V',
              ),
              1 => 
              array (
                'name' => 'numero_tel_ofi_v_c',
                'label' => 'LBL_NUMERO_TEL_OFI_V',
              ),
            ),
          ),
          7 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL5',
            'label' => 'LBL_RECORDVIEW_PANEL5',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'clave_tel_con_v_c',
                'label' => 'LBL_CLAVE_TEL_CON_V',
              ),
              1 => 
              array (
                'name' => 'numero_tel_con_v_c',
                'label' => 'LBL_NUMERO_TEL_CON_V',
              ),
              2 => 
              array (
                'name' => 'extension_v_c',
                'label' => 'LBL_EXTENSION_V',
              ),
              3 => 
              array (
              ),
            ),
          ),
          8 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL7',
            'label' => 'LBL_RECORDVIEW_PANEL7',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'fecha_de_con_v_c',
                'label' => 'LBL_FECHA_DE_CON_V',
              ),
              1 => 
              array (
                'name' => 'rfc_construccion',
                'label' => 'LBL_RFC_CONSTRUCCION',
              ),
              2 => 
              array (
                'name' => 'nombre_del_notario',
                'label' => 'LBL_NOMBRE_DEL_NOTARIO',
              ),
              3 => 
              array (
                'name' => 'notaria',
                'label' => 'LBL_NOTARIA',
              ),
              4 => 
              array (
                'name' => 'plaza',
                'label' => 'LBL_PLAZA',
              ),
              5 => 
              array (
                'name' => 'nombre_del_representante_legal',
                'label' => 'LBL_NOMBRE_DEL_REPRESENTANTE_LEGAL',
              ),
              6 => 
              array (
                'name' => 'puesto',
                'label' => 'LBL_PUESTO',
              ),
              7 => 
              array (
                'name' => 'telefono_cel_con_v_c',
                'label' => 'LBL_TELEFONO_CEL_CON_V',
              ),
              8 => 
              array (
                'name' => 'email_construccion',
                'label' => 'LBL_EMAIL_CONSTRUCCION',
              ),
              9 => 
              array (
                'name' => 'fecha_de_nac_con_v_c',
                'label' => 'LBL_FECHA_DE_NAC_CON_V',
              ),
              10 => 
              array (
                'name' => 'rfc_del_repre_legal_construc',
                'label' => 'LBL_RFC_DEL_REPRE_LEGAL_CONSTRUC',
              ),
              11 => 
              array (
              ),
            ),
          ),
          9 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL9',
            'label' => 'LBL_RECORDVIEW_PANEL9',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'numero_de_suc_v_c',
                'label' => 'LBL_NUMERO_DE_SUC_V',
              ),
              1 => 
              array (
                'name' => 'numero_de_emp_v_c',
                'label' => 'LBL_NUMERO_DE_EMP_V',
              ),
              2 => 
              array (
                'name' => 'ventas_anuales',
                'label' => 'LBL_VENTAS_ANUALES',
              ),
              3 => 
              array (
                'name' => 'principales_sedes_de_operacion',
                'label' => 'LBL_PRINCIPALES_SEDES_DE_OPERACION',
              ),
              4 => 
              array (
                'name' => 'giro',
                'label' => 'LBL_GIRO',
              ),
              5 => 
              array (
                'name' => 'sector',
                'label' => 'LBL_SECTOR',
              ),
              6 => 
              array (
                'name' => 'caracter',
                'label' => 'LBL_CARACTER',
              ),
              7 => 
              array (
              ),
            ),
          ),
          10 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL10',
            'label' => 'LBL_RECORDVIEW_PANEL10',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'nombre_funcionario1',
                'label' => 'LBL_NOMBRE_FUNCIONARIO1',
              ),
              1 => 
              array (
                'name' => 'puesto_funcionario1',
                'label' => 'LBL_PUESTO_FUNCIONARIO1',
              ),
              2 => 
              array (
                'name' => 'nombre_funcionario2',
                'label' => 'LBL_NOMBRE_FUNCIONARIO2',
              ),
              3 => 
              array (
                'name' => 'puesto_funcionario2',
                'label' => 'LBL_PUESTO_FUNCIONARIO2',
              ),
              4 => 
              array (
                'name' => 'nombre_funcionario3',
                'label' => 'LBL_NOMBRE_FUNCIONARIO3',
              ),
              5 => 
              array (
                'name' => 'puesto_funcionario3',
                'label' => 'LBL_PUESTO_FUNCIONARIO3',
              ),
            ),
          ),
          11 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL11',
            'label' => 'LBL_RECORDVIEW_PANEL11',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'institucion_ref_com1',
                'label' => 'LBL_INSTITUCION_REF_COM1',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'tipo_ref_com1',
                'label' => 'LBL_TIPO_REF_COM1',
              ),
              2 => 
              array (
                'name' => 'numero_de_cue_ref_com1_v_c',
                'label' => 'LBL_NUMERO_DE_CUE_REF_COM1_V',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
              ),
              5 => 
              array (
                'name' => 'institucion_ref_com2',
                'label' => 'LBL_INSTITUCION_REF_COM2',
                'span' => 12,
              ),
              6 => 
              array (
                'name' => 'tipo_ref_com2',
                'label' => 'LBL_TIPO_REF_COM2',
              ),
              7 => 
              array (
                'name' => 'numero_de_cue_ref_com2_v_c',
                'label' => 'LBL_NUMERO_DE_CUE_REF_COM2_V',
              ),
            ),
          ),
          12 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL12',
            'label' => 'LBL_RECORDVIEW_PANEL12',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'nombre_proveedor1',
                'label' => 'LBL_NOMBRE_PROVEEDOR1',
              ),
              1 => 
              array (
                'name' => 'clave_pro_1_v_c',
                'label' => 'LBL_CLAVE_PRO_1_V',
              ),
              2 => 
              array (
                'name' => 'telefono_pro1_v_c',
                'label' => 'LBL_TELEFONO_PRO1_V',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'nombre_proveedor2',
                'label' => 'LBL_NOMBRE_PROVEEDOR2',
              ),
              7 => 
              array (
                'name' => 'clave_pro_2_v_c',
                'label' => 'LBL_CLAVE_PRO_2_V',
              ),
              8 => 
              array (
                'name' => 'telefono_pro2_v_c',
                'label' => 'LBL_TELEFONO_PRO2_V',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
              ),
              11 => 
              array (
              ),
              12 => 
              array (
                'name' => 'nombre_proveedor3',
                'label' => 'LBL_NOMBRE_PROVEEDOR3',
              ),
              13 => 
              array (
                'name' => 'clave_pro_3_v_c',
                'label' => 'LBL_CLAVE_PRO_3_V',
              ),
              14 => 
              array (
                'name' => 'telefono_pro3_v_c',
                'label' => 'LBL_TELEFONO_PRO3_V',
              ),
              15 => 
              array (
              ),
            ),
          ),
          13 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL13',
            'label' => 'LBL_RECORDVIEW_PANEL13',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'recepcion_estado_cuenta',
                'label' => 'LBL_RECEPCION_ESTADO_CUENTA',
              ),
              1 => 
              array (
                'name' => 'recepcion_informacion_promo',
                'label' => 'LBL_RECEPCION_INFORMACION_PROMO',
              ),
              2 => 
              array (
                'name' => 'correo_eletronico_terminos',
                'label' => 'LBL_CORREO_ELETRONICO_TERMINOS',
              ),
              3 => 
              array (
              ),
            ),
          ),
          14 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL6',
            'label' => 'LBL_RECORDVIEW_PANEL6',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'doc_acta_constitutiva',
                'label' => 'LBL_DOC_ACTA_CONSTITUTIVA',
                'readonly' => true,
              ),
              1 => 
              array (
                'name' => 'doc_acta_constitutiva_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_ACTA_CONSTITUTIVA_D',
              ),
              2 => 
              array (
                'name' => 'doc_aval',
                'label' => 'LBL_DOC_AVAL',
                'readonly' => true,
              ),
              3 => 
              array (
                'name' => 'doc_aval_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_AVAL_D',
              ),
              4 => 
              array (
                'name' => 'doc_comprobante_domicilio',
                'label' => 'LBL_DOC_COMPROBANTE_DOMICILIO',
                'readonly' => true,
              ),
              5 => 
              array (
                'name' => 'doc_comprobante_domicilio_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COMPROBANTE_DOMICILIO_D',
              ),
              6 => 
              array (
                'name' => 'doc_copia_acta_representante',
                'label' => 'LBL_DOC_COPIA_ACTA_REPRESENTANTE',
                'readonly' => true,
              ),
              7 => 
              array (
                'name' => 'doc_copia_acta_representante_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_ACTA_REPRESENTANTE_D',
              ),
              8 => 
              array (
                'name' => 'doc_copia_estado_cuenta',
                'label' => 'LBL_DOC_COPIA_ESTADO_CUENTA',
                'readonly' => true,
              ),
              9 => 
              array (
                'name' => 'doc_copia_estado_cuenta_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_ESTADO_CUENTA_D',
              ),
              10 => 
              array (
                'name' => 'doc_copia_estado_finan_rec',
                'label' => 'LBL_DOC_COPIA_ESTADO_FINAN_REC',
                'readonly' => true,
              ),
              11 => 
              array (
                'name' => 'doc_copia_estado_finan_rec_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_ESTADO_FINAN_REC_D',
              ),
              12 => 
              array (
                'name' => 'copia_estado_financiero_anter',
                'label' => 'LBL_COPIA_ESTADO_FINANCIERO_ANTER',
                'readonly' => true,
              ),
              13 => 
              array (
                'name' => 'copia_estado_financiero_anterd',
                'studio' => 'visible',
                'label' => 'LBL_COPIA_ESTADO_FINANCIERO_ANTERD',
              ),
              14 => 
              array (
                'name' => 'doc_copia_id_aval',
                'label' => 'LBL_DOC_COPIA_ID_AVAL',
                'readonly' => true,
              ),
              15 => 
              array (
                'name' => 'doc_copia_id_aval_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_ID_AVAL_D',
              ),
              16 => 
              array (
                'name' => 'doc_copia_id_solicitante',
                'label' => 'LBL_DOC_COPIA_ID_SOLICITANTE',
                'readonly' => true,
              ),
              17 => 
              array (
                'name' => 'doc_copia_id_solicitante_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_ID_SOLICITANTE_D',
              ),
              18 => 
              array (
                'name' => 'doc_copia_pago_impuestos',
                'label' => 'LBL_DOC_COPIA_PAGO_IMPUESTOS',
                'readonly' => true,
              ),
              19 => 
              array (
                'name' => 'doc_copia_pago_impuestos_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_PAGO_IMPUESTOS_D',
              ),
              20 => 
              array (
                'name' => 'doc_formato_solicitud',
                'label' => 'LBL_DOC_FORMATO_SOLICITUD',
                'readonly' => true,
              ),
              21 => 
              array (
                'name' => 'doc_formato_solicitud_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_FORMATO_SOLICITUD_D',
              ),
              22 => 
              array (
                'name' => 'doc_copia_rfc',
                'label' => 'LBL_DOC_COPIA_RFC',
                'readonly' => true,
              ),
              23 => 
              array (
                'name' => 'doc_copia_rfc_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_COPIA_RFC_D',
              ),
              24 => 
              array (
                'name' => 'doc_formato_buro',
                'label' => 'LBL_DOC_FORMATO_BURO',
                'readonly' => true,
              ),
              25 => 
              array (
                'name' => 'doc_formato_buro_d',
                'studio' => 'visible',
                'label' => 'LBL_DOC_FORMATO_BURO_D',
              ),
              26 => 
              array (
              ),
              27 => 
              array (
              ),
            ),
          ),
          15 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL14',
            'label' => 'LBL_RECORDVIEW_PANEL14',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'reporte_analisis_agente_ext_c',
                'studio' => 'visible',
                'label' => 'LBL_REPORTE_ANALISIS_AGENTE_EXT',
                'readonly' => true,
              ),
              1 => 
              array (
              ),
              2 => 
              array (
                'name' => 'cita_cliente_visita_c',
                'label' => 'LBL_CITA_CLIENTE_VISITA',
                'readonly' => true,
              ),
              3 => 
              array (
                'name' => 'fecha_visita_ae_v_c',
                'label' => 'LBL_FECHA_VISITA_AE_V',
                'readonly' => true,
              ),
              4 => 
              array (
                'name' => 'analisis_fin_cre_c',
                'label' => 'LBL_ANALISIS_FIN_CRE',
                'readonly' => true,
              ),
              5 => 
              array (
                'name' => 'integracion_foto_c',
                'label' => 'LBL_INTEGRACION_FOTO',
                'readonly' => true,
              ),
              6 => 
              array (
                'name' => 'termina_analisis_c',
                'label' => 'LBL_TERMINA_ANALISIS',
                'readonly' => true,
                'span' => 12,
              ),
              7 => 
              array (
                'name' => 'otro_ae_c',
                'label' => 'LBL_OTRO_AE',
                'readonly' => true,
              ),
              8 => 
              array (
                'name' => 'otro_texto_ae_c',
                'label' => 'LBL_OTRO_TEXTO_AE',
                'readonly' => true,
              ),
              9 => 
              array (
                'name' => 'obs_agente_externo_c',
                'studio' => 'visible',
                'label' => 'LBL_OBS_AGENTE_EXTERNO_C',
                'readonly' => true,
              ),
              10 => 
              array (
              ),
            ),
          ),
          16 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO',
              ),
              2 => 
              array (
              ),
              3 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              4 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
