({
  extendsFrom: 'CreateView',

  initialize: function (options) {
    this._super('initialize', arguments);
  },

  _renderHtml: function (argument) {
    var self = this;
    this._super('_renderHtml', arguments);
    var rolesArray = app.user.get('roles');
    if(self.getField('reporte_analisis_agente_ext_c')){
      if(app.user.get('type') != 'admin'){
        if(!_.filter(rolesArray, function(rol) { return rol.toLowerCase() == "administrador de credito";}).length){
          $("[data-panelname='LBL_RECORDVIEW_PANEL14']").hide();
        }
      }
    }
    this.render();
  },

  _render: function (argument) {
    var self = this;
    this._super('_render', arguments);
    if(self.model.isNotEmpty){
      var idCliente = self.model.get('low01_solicitudescredito_accountsaccounts_ida');
      if(idCliente){
        var url = app.api.buildURL('Accounts/'+idCliente);
      }else {
        var idProspecto = self.model.get('low01_solicitudescredito_prospectsprospects_ida');
        var url = app.api.buildURL('Prospects/'+idProspecto);
      }
      self._preLlenado(url);
    }
  },

  _preLlenado: function (url){
    var self = this;
    app.api.call('read', url, {}, {
      success: function(data){
        if(data && data._module === "Accounts"){
          self.model.set({
            nombre_razon_social:data.name,
            pagina_de_internet:data.website,
            calle_domicilio_actual_fiscal:data.primary_address_street_c,
            numero_exterior:data.primary_address_numero_c,
            // numero_interior:data. Accounts y Prospects no tiene esto
            colonia:data.primary_address_colonia_c,
            municipio:data.primary_address_city_c,
            estado:data.primary_address_state_c,
            cp:data.primary_address_postalcode_c
          });
        }
        if(data && data._module === "Prospects"){
          self.model.set({
            nombre_razon_social:data.last_name,
            pagina_de_internet:data.web_page_c,
            calle_domicilio_actual_fiscal:data.primary_address_street,
            numero_exterior:data.primary_address_number_c,
            // numero_interior:data. Accounts y Prospects no tiene esto
            colonia:data.primary_address_colonia_c,
            municipio:data.primary_address_city,
            estado:data.primary_address_state,
            cp:data.primary_address_postalcode
          });
        }
      }
    });
  },

  _renderFields: function (){
		var current = this.action;
		this.action = 'create';
		this._super('_renderFields', arguments);
		this.action = current;
		this.$el.find('span[data-fieldname="numero_de_cuenta_ref_com1"]').append('<p class="help-block">  </p><br><br>');
		this.$el.find('span[data-fieldname="telefono_proveedor1"]').append('<p class="help-block">  </p><br><br>');
		this.$el.find('span[data-fieldname="telefono_proveedor2"]').append('<p class="help-block">  </p><br><br>');
	},
})
