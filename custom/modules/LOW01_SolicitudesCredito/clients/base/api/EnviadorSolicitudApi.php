<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
require_once "custom/modules/LOW01_SolicitudesCredito/LOW01_SolicitudesCreditoLogicHooksManager.php";
class EnviadorSolicitudApi extends SugarApi
{
  public function registerApiRest() {
    return array(
      'enviarSolicitud' => array(
        'reqType' => 'POST',
        'path' => array('LOW01_SolicitudesCredito','?', 'enviar_solicitud',),
        'pathVars' => array('','record','',),
        'method' => 'enviarSolicitud',
        'shortHelp' => 'Envia solicitud.',
        'longHelp' => '',
      ),
    );
  }

  public function enviarSolicitud($api, $args)
  {
    if(empty($args['record'])){
			throw new SugarApiExceptionMissingParameter('Parameter record is missing');
		}
		$solicitud = BeanFactory::getBean('LOW01_SolicitudesCredito', $args['record']);
    $logicHookManager = new LOW01_SolicitudesCreditoLogicHooksManager();
    $logicHookManager->envia_email_solicitudCredito($solicitud);
    return ["status" => "success"];
  }

}
