<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Layoutdefs/low01_solicitudescredito_contacts_1_LOW01_SolicitudesCredito.php

 // created: 2017-10-12 22:45:07
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_contacts_1',
  'top_buttons' =>
  array (
    0 =>
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 =>
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Layoutdefs/low01_solicitudescredito_low03_agentesexternos_1_LOW01_SolicitudesCredito.php

 // created: 2017-10-17 01:51:44
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_low03_agentesexternos_1'] = array (
  'order' => 100,
  'module' => 'Low03_AgentesExternos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW03_AGENTESEXTERNOS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_low03_agentesexternos_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Layoutdefs/low01_solicitudescredito_documents_1_LOW01_SolicitudesCredito.php

 // created: 2017-07-26 16:30:16
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Layoutdefs/low01_solicitudescredito_activities_calls_LOW01_SolicitudesCredito.php

 // created: 2017-07-21 12:47:07
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['activities'] = array (
  'order' => 10,
  'sort_order' => 'desc',
  'sort_by' => 'date_start',
  'title_key' => 'LBL_ACTIVITIES_SUBPANEL_TITLE',
  'type' => 'collection',
  'subpanel_name' => 'activities',
  'module' => 'Activities',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateTaskButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopScheduleMeetingButton',
    ),
    2 => 
    array (
      'widget_class' => 'SubPanelTopScheduleCallButton',
    ),
    3 => 
    array (
      'widget_class' => 'SubPanelTopComposeEmailButton',
    ),
  ),
  'collection_list' => 
  array (
    'meetings' => 
    array (
      'module' => 'Meetings',
      'subpanel_name' => 'ForActivities',
      'get_subpanel_data' => 'low01_solicitudescredito_activities_meetings',
    ),
    'tasks' => 
    array (
      'module' => 'Tasks',
      'subpanel_name' => 'ForActivities',
      'get_subpanel_data' => 'low01_solicitudescredito_activities_tasks',
    ),
    'calls' => 
    array (
      'module' => 'Calls',
      'subpanel_name' => 'ForActivities',
      'get_subpanel_data' => 'low01_solicitudescredito_activities_calls',
    ),
  ),
  'get_subpanel_data' => 'activities',
);
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['history'] = array (
  'order' => 20,
  'sort_order' => 'desc',
  'sort_by' => 'date_modified',
  'title_key' => 'LBL_HISTORY',
  'type' => 'collection',
  'subpanel_name' => 'history',
  'module' => 'History',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateNoteButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopArchiveEmailButton',
    ),
    2 => 
    array (
      'widget_class' => 'SubPanelTopSummaryButton',
    ),
  ),
  'collection_list' => 
  array (
    'meetings' => 
    array (
      'module' => 'Meetings',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'low01_solicitudescredito_activities_meetings',
    ),
    'tasks' => 
    array (
      'module' => 'Tasks',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'low01_solicitudescredito_activities_tasks',
    ),
    'calls' => 
    array (
      'module' => 'Calls',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'low01_solicitudescredito_activities_calls',
    ),
    'notes' => 
    array (
      'module' => 'Notes',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'low01_solicitudescredito_activities_notes',
    ),
    'emails' => 
    array (
      'module' => 'Emails',
      'subpanel_name' => 'ForHistory',
      'get_subpanel_data' => 'low01_solicitudescredito_activities_emails',
    ),
  ),
  'get_subpanel_data' => 'history',
);

?>
