<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta';
$mod_strings['LBL_OPS_ADV_ENV_SOL_C'] = "Opciones avanzada";
$mod_strings['LBL_ENV_EMAIL_DEFAULT_C'] = "Enviar a la dirección principal del Prospecto / Cliente";
$mod_strings['LBL_ENV_CONTACTS_SEL_C'] = "Enviar a Conctactos seleccionados";
$mod_strings['LBL_ENV_CREATED_BY_C'] = "Enviar al usuario que ha creado el registro ";
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW03_AGENTESEXTERNOS_TITLE'] = 'Agente Externo';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.CF_SolicitudCredito_20170726.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_20171007.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta'

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_20170907.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta'

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_20170922.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta'

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_20170828.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta'

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_20171012.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta'

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_20170817.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta'

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_20171020.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta';
$mod_strings['LBL_OPS_ADV_ENV_SOL_C'] = "Opciones avanzada";
$mod_strings['LBL_ENV_EMAIL_DEFAULT_C'] = "Enviar a la dirección principal del Prospecto / Cliente";
$mod_strings['LBL_ENV_CONTACTS_SEL_C'] = "Enviar a Conctactos seleccionados";
$mod_strings['LBL_ENV_CREATED_BY_C'] = "Enviar al usuario que ha creado el registro ";
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW03_AGENTESEXTERNOS_TITLE'] = 'Agente Externo';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.CF_SolicitudCredito_20170728.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_20171010.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta'

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_201710130100.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta';
$mod_strings['LBL_OPS_ADV_ENV_SOL_C'] = "Opciones avanzada";
$mod_strings['LBL_ENV_EMAIL_DEFAULT_C'] = "Enviar a la dirección principal del Prospecto / Cliente";
$mod_strings['LBL_ENV_CONTACTS_SEL_C'] = "Enviar a Conctactos seleccionados";
$mod_strings['LBL_ENV_CREATED_BY_C'] = "Enviar al usuario que ha creado el registro ";
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_20171009.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta'

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.CF_SolicitudCredito_20170810.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta'

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.CF_SolicitudCredito_20170804.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_CRM_20170831.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta'

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.SolicitudCredito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE_ID'] = 'Prospectos ID';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_CALLS_FROM_CALLS_TITLE'] = 'Llamadas';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_MEETINGS_FROM_MEETINGS_TITLE'] = 'Reuniones';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_NOTES_FROM_NOTES_TITLE'] = 'Notas';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_TASKS_FROM_TASKS_TITLE'] = 'Tareas';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_EMAILS_FROM_EMAILS_TITLE'] = 'Correos Electrónicos';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_FRR.php

$mod_strings['LBL_ESTADO_C'] = 'Estado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentarios Asesor Comercial';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente de Ventas Comercial';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Clave';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Clave';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta';
$mod_strings['LBL_OPS_ADV_ENV_SOL_C'] = "Opciones avanzada";
$mod_strings['LBL_ENV_EMAIL_DEFAULT_C'] = "Enviar a la dirección principal del Prospecto / Cliente";
$mod_strings['LBL_ENV_CONTACTS_SEL_C'] = "Enviar a Conctactos seleccionados";
$mod_strings['LBL_ENV_CREATED_BY_C'] = "Enviar al usuario que ha creado el registro ";
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Contactos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW03_AGENTESEXTERNOS_TITLE'] = 'Agente Externo';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.customlow01_solicitudescredito_low03_agentesexternos_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW03_AGENTESEXTERNOS_TITLE'] = 'Agente Externo';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.customlow01_solicitudescredito_documents_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Solicitudes de Crédito';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_DOCUMENTS_TITLE'] = 'Documentos';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.Lowes_Modules_CRM.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE_ID'] = 'Prospectos ID';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE'] = 'Prospectos';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_CALLS_FROM_CALLS_TITLE'] = 'Llamadas';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_MEETINGS_FROM_MEETINGS_TITLE'] = 'Reuniones';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_NOTES_FROM_NOTES_TITLE'] = 'Notas';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_TASKS_FROM_TASKS_TITLE'] = 'Tareas';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_EMAILS_FROM_EMAILS_TITLE'] = 'Correos Electrónicos';

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ENVIAR_PDF_C'] = 'Enviar Solicitud de Crédito';
$mod_strings['LBL_ESTADO_SOLICITUD'] = 'Estado de Solicitud';
$mod_strings['LBL_REPORTE_ANALISIS_REALIZADO_C'] = 'Reporte de análisis realizado';
$mod_strings['LBL_ESTADO_SOLICITUD_C'] = 'Estado';
$mod_strings['LBL_OBS_ASESOR_COMERCIAL_C'] = 'Comentario Asesor Comercial';
$mod_strings['LBL_OBS_GERENTE_VTAS_COM_C'] = 'Comentarios Gerente Ventas Comercial';
$mod_strings['LBL_REPORTE_ANALISIS_AGENTE_EXT'] = 'Reporte de Análisis';
$mod_strings['LBL_DOC_COMPROBANTE_DOMICILIO'] = ' ';
$mod_strings['LBL_DOC_COMPROBANTE_DOMICILIO_D'] = 'Formato para consulta de Buró de Crédito Aval (Persona física o Moral)';
$mod_strings['LBL_DOC_COPIA_ACTA_REPRESENTANTE'] = ' ';
$mod_strings['LBL_DOC_COPIA_ACTA_REPRESENTANTE_D'] = 'Copia de RFC (Cédula fiscal)';
$mod_strings['LBL_AGENTE_EXTERNO'] = 'Agente Externo';
$mod_strings['LBL_RECORDVIEW_PANEL14'] = 'Agente Externo';
$mod_strings['LBL_CITA_CLIENTE_VISITA'] = 'Cita Cliente Visita';
$mod_strings['LBL_FECHA_VISITA_AE'] = 'Fecha(AE)(no usar)';
$mod_strings['LBL_ANALISIS_FIN_CRE'] = 'Análisis Financiero Crediticio';
$mod_strings['LBL_INTEGRACION_FOTO'] = 'Integración fotografía';
$mod_strings['LBL_TERMINA_ANALISIS'] = 'Termina Análisis';
$mod_strings['LBL_OTRO_AE'] = 'Otro(AE)';
$mod_strings['LBL_OTRO_TEXTO_AE'] = 'Otro texto';
$mod_strings['LBL_LOW03_AGENTESEXTERNOS_ID'] = 'Agente Externo';
$mod_strings['LBL_OBS_ADMON_CREDITO_C'] = 'Comentarios Administrador de Crédito';
$mod_strings['LBL_OBS_ANALISTA_CREDITO_C'] = 'Comentarios Analista de Crédito';
$mod_strings['LBL_OBS_DIRECTOR_FINANZAS_C'] = 'Comentarios Director de Finanzas';
$mod_strings['LBL_OBS_GERENTE_CREDITO_C'] = 'Comentarios Gerente de Crédito';
$mod_strings['LBL_OBS_VICEP_COMERCIAL_C'] = 'Comentarios Vicepresidente Comercial';
$mod_strings['LBL_SOL_CRED_URL_C'] = 'URL de la Solicitud de Credito';
$mod_strings['LBL_PUESTO_FUNCIONARIO1'] = 'Puesto';
$mod_strings['LBL_DOC_ACTA_CONSTITUTIVA'] = ' ';
$mod_strings['LBL_DOC_ACTA_CONSTITUTIVA_D'] = 'Solicitud de Crédito firmada';
$mod_strings['LBL_DOC_AVAL'] = ' ';
$mod_strings['LBL_DOC_AVAL_D'] = 'Formato para consulta de Buró de Crédito Persona Moral';
$mod_strings['LBL_DOC_COPIA_ESTADO_CUENTA'] = ' ';
$mod_strings['LBL_DOC_COPIA_ESTADO_CUENTA_D'] = 'Copia de Comprobante de Domicilio (Luz, Agua, Teléfono fijo)';
$mod_strings['LBL_DOC_COPIA_ESTADO_FINAN_REC'] = ' ';
$mod_strings['LBL_DOC_COPIA_ESTADO_FINAN_REC_D'] = 'Copia de identificación oficial vigente del Solicitante o Representante Legal (INE o pasaporte)';
$mod_strings['LBL_DOC_COPIA_ID_AVAL'] = ' ';
$mod_strings['LBL_DOC_COPIA_ID_AVAL_D'] = 'Copia de los últimos 3 meses de Estados de Cuenta Bancarios';
$mod_strings['LBL_DOC_COPIA_ID_SOLICITANTE'] = ' ';
$mod_strings['LBL_DOC_COPIA_ID_SOLICITANTE_D'] = 'Copia de Estados Financieros de Diciembre del año pasado (Balance General y Estado de Resultados con Relaciones Analíticas)';
$mod_strings['LBL_DOC_COPIA_PAGO_IMPUESTOS'] = ' ';
$mod_strings['LBL_DOC_COPIA_PAGO_IMPUESTOS_D'] = 'Copia de Estados Financieros del año actual. (Balance General y Estado de Resultados con Relaciones Analíticas)';
$mod_strings['LBL_DOC_FORMATO_SOLICITUD'] = ' ';
$mod_strings['LBL_DOC_FORMATO_SOLICITUD_D'] = 'Copia del Acta Constitutiva y Poderes del Representante legal';
$mod_strings['LBL_DOC_COPIA_RFC'] = ' ';
$mod_strings['LBL_DOC_COPIA_RFC_D'] = 'Copia Comprobante del último pago de impuestos realizado';
$mod_strings['LBL_DOC_FORMATO_BURO'] = ' ';
$mod_strings['LBL_DOC_FORMATO_BURO_D'] = 'Otro';
$mod_strings['LBL_COPIA_ESTADO_FINANCIERO_ANTER'] = ' ';
$mod_strings['LBL_COPIA_ESTADO_FINANCIERO_ANTERD'] = 'Copia de identificación oficial vigente del Aval (INE o pasaporte)';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Documentos Anexos';
$mod_strings['LBL_ANIOS_DOMICILIO_ACTUAL'] = 'Año(no usar)';
$mod_strings['LBL_ANIOS_DOMICILIO_ANTERIOR'] = 'Años(no usar)';
$mod_strings['LBL_CLAVE_PROVEEDOR1'] = 'Clave(no usar)';
$mod_strings['LBL_CLAVE_PROVEEDOR2'] = 'Clave(no usar)';
$mod_strings['LBL_CLAVE_PROVEEDOR3'] = 'Clave(no usar)';
$mod_strings['LBL_CLAVE_TELEFONO_CONMUTADOR'] = 'Clave(no usar)';
$mod_strings['LBL_CLAVE_TELEFONO_OFICINA'] = 'Clave(no uar)';
$mod_strings['LBL_EXTENSION'] = 'Extensión(no usar)';
$mod_strings['LBL_MESES_DOMICILIO_ACTUAL'] = 'Meses(no usar)';
$mod_strings['LBL_NUMERO_DE_CUENTA_REF_COM1'] = 'Número de cuenta(no usar)';
$mod_strings['LBL_NUMERO_DE_CUENTA_REF_COM2'] = 'Número de cuenta(no usar)';
$mod_strings['LBL_NUMERO_DE_EMPLEADOS'] = 'Número de empleados(no usar)';
$mod_strings['LBL_NUMERO_DE_SUCURSALES'] = 'Número de sucursales(no usar)';
$mod_strings['LBL_NUMERO_TELEFONO_CONMUTADOR'] = 'Número(no usar)';
$mod_strings['LBL_NUMERO_TELEFONO_OFICINA'] = 'Número(no usar)';
$mod_strings['LBL_TELEFONO_CELULAR_CONSTRUCCION'] = 'Teléfono celular(no usar)';
$mod_strings['LBL_TELEFONO_PROVEEDOR1'] = 'Teléfono(no usar)';
$mod_strings['LBL_TELEFONO_PROVEEDOR2'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PROVEEDOR3'] = 'Teléfono';
$mod_strings['LBL_OBS_AGENTE_EXTERNO_C'] = 'Comentarios Agente Externo';
$mod_strings['LBL_NAME'] = 'Nombre';
$mod_strings['LBL_NOMBRE_RAZON_SOCIAL'] = 'Nombre / Razón Social';
$mod_strings['LBL_PAGINA_DE_INTERNET'] = 'Página de internet';
$mod_strings['LBL_FECHA_DE_CONSTITUCION'] = 'Fecha de constitución(no usar)';
$mod_strings['LBL_MESES_DOMICILIO_ANTERIOR'] = 'Meses(no usar)';
$mod_strings['LBL_NUMERO_DE_CUENTA'] = 'Número de cuenta(no usar)';
$mod_strings['LBL_ANIOS_DOM_ACT_V'] = 'Años';
$mod_strings['LBL_ANIOS_DOM_ANT_V'] = 'Años';
$mod_strings['LBL_MESES_DOM_ACT_V'] = 'Meses';
$mod_strings['LBL_MESES_DOM_ANT_V'] = 'Meses';
$mod_strings['LBL_CLAVE_TEL_OFI_V'] = 'Clave';
$mod_strings['LBL_CLAVE_TEL_CON_V'] = 'Clave';
$mod_strings['LBL_NUMERO_TEL_OFI_V'] = 'Número';
$mod_strings['LBL_NUMERO_TEL_CON_V'] = 'Número';
$mod_strings['LBL_EXTENSION_V'] = 'Extension';
$mod_strings['LBL_FECHA_DE_CON_V'] = 'Fecha de constitución';
$mod_strings['LBL_TELEFONO_CEL_CON_V'] = 'Teléfono celular';
$mod_strings['LBL_NUMERO_DE_SUC_V'] = 'Número de sucursales';
$mod_strings['LBL_NUMERO_DE_EMP_V'] = 'Número de empleados';
$mod_strings['LBL_NUMERO_DE_CUE_V'] = 'Número de Cuenta';
$mod_strings['LBL_CLAVE_PRO_1_V'] = 'Producto o Servicio';
$mod_strings['LBL_CLAVE_PRO_2_V'] = 'Producto o Servicio';
$mod_strings['LBL_CLAVE_PRO_3_V'] = 'Producto o Servicio';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM1_V'] = 'Número de Cuenta';
$mod_strings['LBL_NUMERO_DE_CUE_REF_COM2_V'] = 'Número de Cuenta';
$mod_strings['LBL_MONTO_DESEADO'] = 'Monto Deseado';
$mod_strings['LBL_FECHA_DE_NAC_CON_V'] = 'Fecha de Nacimiento';
$mod_strings['LBL_TELEFONO_PRO1_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO2_V'] = 'Teléfono';
$mod_strings['LBL_TELEFONO_PRO3_V'] = 'Teléfono';
$mod_strings['LBL_FECHA_VISITA_AE_V'] = 'Fecha(AE)';
$mod_strings['LBL_PROMOTOR'] = 'Asesor Comercial';
$mod_strings['LBL_FECHA'] = 'Fecha';
$mod_strings['LBL_DOMICILIO'] = 'Tipo Domicilio';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet(no USAR)';
$mod_strings['LBL_DOCUMENTACION_INCOMPLETA'] = 'Documentación Incompleta';
$mod_strings['LBL_TIENDA'] = 'Sucursal';
$mod_strings['LBL_SHOW_MORE'] = 'Información de Sistema';
$mod_strings['LBL_CALLE_DOMICILIO_ACTUAL_FISCAL'] = 'Calle domicilio actual y/o fiscal';
$mod_strings['LBL_NOMBRE_DEL_NOTARIO'] = 'Nombre el accionista mayoritario';
$mod_strings['LBL_NOTARIA'] = 'Capital Social';
$mod_strings['LBL_RECORDVIEW_PANEL13'] = 'Otros';
$mod_strings['LBL_RFC_CONSTRUCCION'] = 'RFC Accionista';
$mod_strings['LBL_ESCRITURA'] = 'Teléfono celular';
$mod_strings['LBL_RPP'] = 'E-mail';
$mod_strings['LBL_CORREO_ELETRONICO_TERMINOS'] = 'E-mail';
$mod_strings['LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Clientes';
$mod_strings['LBL_RECORDVIEW_PANEL12'] = 'Referencia proveedores';
$mod_strings['LBL_RECORDVIEW_PANEL15'] = 'COMENTARIOS';
$mod_strings['LBL_ENVIAR_SOLICITUD_BUTTON'] = 'Enviar';

?>
