<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/WirelessLayoutdefs/low01_solicitudescredito_contacts_1_LOW01_SolicitudesCredito.php

 // created: 2017-10-12 22:45:07
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_contacts_1',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/WirelessLayoutdefs/low01_solicitudescredito_activities_emails_LOW01_SolicitudesCredito.php

 // created: 2017-07-21 12:47:09
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_activities_emails'] = array (
  'order' => 100,
  'module' => 'Emails',
  'subpanel_name' => 'Por Defecto',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_EMAILS_FROM_EMAILS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_activities_emails',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/WirelessLayoutdefs/low01_solicitudescredito_activities_tasks_LOW01_SolicitudesCredito.php

 // created: 2017-07-21 12:47:09
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_activities_tasks'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'Por Defecto',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_TASKS_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_activities_tasks',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/WirelessLayoutdefs/low01_solicitudescredito_activities_notes_LOW01_SolicitudesCredito.php

 // created: 2017-07-21 12:47:09
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_activities_notes'] = array (
  'order' => 100,
  'module' => 'Notes',
  'subpanel_name' => 'Por Defecto',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_NOTES_FROM_NOTES_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_activities_notes',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/WirelessLayoutdefs/low01_solicitudescredito_documents_1_LOW01_SolicitudesCredito.php

 // created: 2017-07-26 16:30:16
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_documents_1',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/WirelessLayoutdefs/low01_solicitudescredito_activities_calls_LOW01_SolicitudesCredito.php

 // created: 2017-07-21 12:47:09
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_activities_calls'] = array (
  'order' => 100,
  'module' => 'Calls',
  'subpanel_name' => 'Por Defecto',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_CALLS_FROM_CALLS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_activities_calls',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/WirelessLayoutdefs/low01_solicitudescredito_activities_meetings_LOW01_SolicitudesCredito.php

 // created: 2017-07-21 12:47:09
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_activities_meetings'] = array (
  'order' => 100,
  'module' => 'Meetings',
  'subpanel_name' => 'Por Defecto',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_MEETINGS_FROM_MEETINGS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_activities_meetings',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/WirelessLayoutdefs/low01_solicitudescredito_low03_agentesexternos_1_LOW01_SolicitudesCredito.php

 // created: 2017-10-17 01:51:44
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_low03_agentesexternos_1'] = array (
  'order' => 100,
  'module' => 'Low03_AgentesExternos',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW03_AGENTESEXTERNOS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_low03_agentesexternos_1',
);

?>
