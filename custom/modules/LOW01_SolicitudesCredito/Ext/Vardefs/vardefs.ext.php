<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_contacts_1_LOW01_SolicitudesCredito.php

// created: 2017-10-12 22:45:07
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_contacts_1"] = array (
  'name' => 'low01_solicitudescredito_contacts_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'id_name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_activities_emails_LOW01_SolicitudesCredito.php

// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_activities_emails"] = array (
  'name' => 'low01_solicitudescredito_activities_emails',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_emails',
  'source' => 'non-db',
  'module' => 'Emails',
  'bean_name' => 'Email',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_EMAILS_FROM_EMAILS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_activities_tasks_LOW01_SolicitudesCredito.php

// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_activities_tasks"] = array (
  'name' => 'low01_solicitudescredito_activities_tasks',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_tasks',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_TASKS_FROM_TASKS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_prospects_LOW01_SolicitudesCredito.php

// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_prospects"] = array (
  'name' => 'low01_solicitudescredito_prospects',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_prospects',
  'source' => 'non-db',
  'module' => 'Prospects',
  'bean_name' => 'Prospect',
  'side' => 'right',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'id_name' => 'low01_solicitudescredito_prospectsprospects_ida',
  'link-type' => 'one',
);
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_prospects_name"] = array (
  'name' => 'low01_solicitudescredito_prospects_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE',
  'save' => true,
  'id_name' => 'low01_solicitudescredito_prospectsprospects_ida',
  'link' => 'low01_solicitudescredito_prospects',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'account_name',
);
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_prospectsprospects_ida"] = array (
  'name' => 'low01_solicitudescredito_prospectsprospects_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE_ID',
  'id_name' => 'low01_solicitudescredito_prospectsprospects_ida',
  'link' => 'low01_solicitudescredito_prospects',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_activities_notes_LOW01_SolicitudesCredito.php

// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_activities_notes"] = array (
  'name' => 'low01_solicitudescredito_activities_notes',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_notes',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_NOTES_FROM_NOTES_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_low03_agentesexternos_1_LOW01_SolicitudesCredito.php

// created: 2017-10-17 01:51:44
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_low03_agentesexternos_1"] = array (
  'name' => 'low01_solicitudescredito_low03_agentesexternos_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_low03_agentesexternos_1',
  'source' => 'non-db',
  'module' => 'Low03_AgentesExternos',
  'bean_name' => 'Low03_AgentesExternos',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'id_name' => 'low01_soliaf8bcredito_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_accounts_LOW01_SolicitudesCredito.php

// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_accounts"] = array (
  'name' => 'low01_solicitudescredito_accounts',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'id_name' => 'low01_solicitudescredito_accountsaccounts_ida',
  'link-type' => 'one',
);
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_accounts_name"] = array (
  'name' => 'low01_solicitudescredito_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'low01_solicitudescredito_accountsaccounts_ida',
  'link' => 'low01_solicitudescredito_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_accountsaccounts_ida"] = array (
  'name' => 'low01_solicitudescredito_accountsaccounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE_ID',
  'id_name' => 'low01_solicitudescredito_accountsaccounts_ida',
  'link' => 'low01_solicitudescredito_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_documents_1_LOW01_SolicitudesCredito.php

// created: 2017-07-26 16:30:16
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_documents_1"] = array (
  'name' => 'low01_solicitudescredito_documents_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'id_name' => 'low01_solicitudescredito_documents_1low01_solicitudescredito_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_activities_calls_LOW01_SolicitudesCredito.php

// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_activities_calls"] = array (
  'name' => 'low01_solicitudescredito_activities_calls',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_CALLS_FROM_CALLS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_activities_meetings_LOW01_SolicitudesCredito.php

// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_activities_meetings"] = array (
  'name' => 'low01_solicitudescredito_activities_meetings',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_MEETINGS_FROM_MEETINGS_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_cita_cliente_visita_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['cita_cliente_visita_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_acta_constitutiva.php

 // created: 2017-08-11 01:58:54

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_clave_pro_1_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_pro_1_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_anios_dom_ant_v_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['anios_dom_ant_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_clave_tel_con_v_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_tel_con_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_clave_pro_3_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_pro_3_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_clave_pro_2_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_pro_2_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_clave_tel_ofi_v_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_tel_ofi_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_aval.php

 // created: 2017-08-11 01:59:11

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_analisis_fin_cre_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['analisis_fin_cre_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_anios_dom_act_v_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['anios_dom_act_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_agente_externo_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['agente_externo_c']['labelValue']='Agente Externo';
$dictionary['LOW01_SolicitudesCredito']['fields']['agente_externo_c']['dependent']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['agente_externo_c']['required']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['agente_externo_c']['dependency']='equal($estado_solicitud_c,"Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito")';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_copia_estado_financiero_anter.php

 // created: 2017-08-11 01:56:51

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/low01_solicitudescredito_visibility.php

$dictionary['LOW01_SolicitudesCredito']['visibility']["SolCredVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_env_contacts_sel_c.php

 // created: 2017-10-12 14:22:19
$dictionary['LOW01_SolicitudesCredito']['fields']['env_contacts_sel_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['env_contacts_sel_c']['dependency']='equal($ops_adv_env_sol_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_copia_id_solicitante.php

 // created: 2017-08-11 02:00:46

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_env_created_by_c.php

 // created: 2017-10-12 14:22:19
$dictionary['LOW01_SolicitudesCredito']['fields']['env_created_by_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['env_created_by_c']['dependency']='equal($ops_adv_env_sol_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_enviar_pdf_c.php

 // created: 2017-08-23 18:56:02
$dictionary['LOW01_SolicitudesCredito']['fields']['enviar_pdf_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['enviar_pdf_c']['labelValue']='Enviar Solicitud de Crédito';
$dictionary['LOW01_SolicitudesCredito']['fields']['enviar_pdf_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['enviar_pdf_c']['dependency']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['enviar_pdf_c']['audit']=1;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_copia_estado_cuenta.php

 // created: 2017-08-11 01:59:46

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_copia_rfc.php

 // created: 2017-08-11 02:01:29

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_extension_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['extension_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_document_id13_c.php

 // created: 2017-08-23 18:56:02
$dictionary['LOW01_SolicitudesCredito']['fields']['document_id13_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_env_email_default_c.php

 // created: 2017-10-12 14:22:19
$dictionary['LOW01_SolicitudesCredito']['fields']['env_email_default_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['env_email_default_c']['dependency']='equal($ops_adv_env_sol_c,true)';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_copia_pago_impuestos.php

 // created: 2017-08-11 02:01:06

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_domicilio.php

 // created: 2017-08-11 21:27:18

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_comprobante_domicilio_d.php

 // created: 2017-07-26 16:39:30

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_copia_id_aval.php

 // created: 2017-08-11 02:00:26

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_copia_acta_representante.php

 // created: 2017-08-11 01:59:28

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_formato_buro.php

 // created: 2017-08-11 02:03:39

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_estado_solicitud_c.php

 // created: 2017-10-12 15:35:53
$dictionary['LOW01_SolicitudesCredito']['fields']['estado_solicitud_c']['labelValue']='Estado';
$dictionary['LOW01_SolicitudesCredito']['fields']['estado_solicitud_c']['dependency']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['estado_solicitud_c']['visibility_grid']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['estado_solicitud_c']['default']='Solicitud_Nueva';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_comprobante_domicilio.php

 // created: 2017-08-11 01:56:19

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_formato_solicitud.php

 // created: 2017-08-11 02:04:04

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_documentacion_incompleta_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['documentacion_incompleta_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['documentacion_incompleta_c']['labelValue']='Documentación Incompleta';
$dictionary['LOW01_SolicitudesCredito']['fields']['documentacion_incompleta_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['documentacion_incompleta_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_copia_estado_finan_rec.php

 // created: 2017-08-11 02:00:07

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_doc_copia_acta_representante_d.php

 // created: 2017-07-26 16:40:18

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_fecha_de_nac_con_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_nac_con_v_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_nac_con_v_c']['labelValue']='Fecha de nacimiento';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_nac_con_v_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_nac_con_v_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_nac_con_v_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_fecha_visita_ae_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_visita_ae_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_visita_ae_c']['labelValue']='Fecha(AE)(no usar)';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_visita_ae_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_visita_ae_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_fecha.php

 // created: 2017-08-11 16:45:56
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['massupdate']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['importable']='false';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['duplicate_merge']='disabled';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['calculated']='true';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['formula']='now()';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['enforced']=true;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_fecha_de_con_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_con_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_tel_con_v_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_tel_con_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_de_cue_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_cue_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_obs_analista_credito_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_analista_credito_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_analista_credito_c']['labelValue']='Comentarios Analista de Crédito';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_analista_credito_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_analista_credito_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_analista_credito_c']['required']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_analista_credito_c']['audited']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_analista_credito_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobado_por_Analista_de_Credito","Rechazado_por_Analista_de_Credito","Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Pendiente_por_Agente_Externo","Pendiente_por_Administrador_de_Credito","Aprobacion_por_Administrador_de_Credito","Rechazado_por_Administrador_de_Credito","Pendiente_por_Gerente_de_Credito","Aprobacion_por_Gerente_de_Credito","Rechazado_por_Gerente_de_Credito","Pendiente_por_Director_de_Finanzas","Aprobacion_por_Director_de_Finanzas","Rechazado_por_Director_de_Finanzas","Pendiente_por_Vicepresidencia_Comercial","Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_meses_dom_ant_v_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['meses_dom_ant_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_de_cue_ref_com1_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_cue_ref_com1_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_de_suc_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_suc_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_de_cue_ref_com2_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_cue_ref_com2_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_fecha_visita_ae_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_visita_ae_v_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_visita_ae_v_c']['labelValue']='Fecha(AE)';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_visita_ae_v_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_visita_ae_v_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_visita_ae_v_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_meses_dom_act_v_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['meses_dom_act_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_low03_agentesexternos_id_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['low03_agentesexternos_id_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_obs_director_finanzas_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_director_finanzas_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_director_finanzas_c']['labelValue']='Comentarios Director de Finanzas';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_director_finanzas_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_director_finanzas_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_director_finanzas_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobacion_por_Director_de_Finanzas","Rechazado_por_Director_de_Finanzas","Pendiente_por_Vicepresidencia_Comercial","Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_director_finanzas_c']['required']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_director_finanzas_c']['audited']=1;
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_tel_ofi_v_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_tel_ofi_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_obs_agente_externo_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_agente_externo_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_agente_externo_c']['labelValue']='Comentarios Agente Externo';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_agente_externo_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_agente_externo_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_agente_externo_c']['dependency']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_agente_externo_c']['audited']=1;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_obs_admon_credito_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_admon_credito_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_admon_credito_c']['labelValue']='Comentarios Administrador de Crédito';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_admon_credito_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_admon_credito_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_admon_credito_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobacion_por_Administrador_de_Credito","Rechazado_por_Administrador_de_Credito","Pendiente_por_Gerente_de_Credito","Aprobacion_por_Gerente_de_Credito","Rechazado_por_Gerente_de_Credito","Pendiente_por_Director_de_Finanzas","Aprobacion_por_Director_de_Finanzas","Rechazado_por_Director_de_Finanzas","Pendiente_por_Vicepresidencia_Comercial","Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_admon_credito_c']['required']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_admon_credito_c']['audited']=1;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_de_emp_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_emp_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_integracion_foto_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['integracion_foto_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_telefono_pro2_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro2_v_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro2_v_c']['labelValue']='Teléfono';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro2_v_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro2_v_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro2_v_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_telefono_pro1_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro1_v_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro1_v_c']['labelValue']='Teléfono';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro1_v_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro1_v_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro1_v_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_termina_analisis_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['termina_analisis_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_otro_texto_ae_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['otro_texto_ae_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_otro_ae_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['otro_ae_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_telefono_pro3_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro3_v_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro3_v_c']['labelValue']='Teléfono';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro3_v_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro3_v_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_pro3_v_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_sol_cred_url_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['sol_cred_url_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['sol_cred_url_c']['labelValue']='URL de la Solicitud de Credito';
$dictionary['LOW01_SolicitudesCredito']['fields']['sol_cred_url_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['sol_cred_url_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_obs_gerente_credito_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['labelValue']='Comentarios Gerente de Crédito';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobacion_por_Gerente_de_Credito","Rechazado_por_Gerente_de_Credito","Pendiente_por_Director_de_Finanzas","Aprobacion_por_Director_de_Finanzas","Rechazado_por_Director_de_Finanzas","Pendiente_por_Vicepresidencia_Comercial","Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['required']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['audited']=1;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_website_c.php

 // created: 2017-08-12 23:10:19
$dictionary['LOW01_SolicitudesCredito']['fields']['website_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['website_c']['labelValue']='Página de Internet';
$dictionary['LOW01_SolicitudesCredito']['fields']['website_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['website_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['website_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_obs_gerente_vtas_com_c.php

 // created: 2017-08-23 18:56:02
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['labelValue']='Comentarios Gerente Ventas Comercial';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobado_por_Gerente_de_Ventas_Comerciales","Rechazado_por_Gerente_de_Ventas_Comerciales","Pendiente_por_Analista_de_Credito","Aprobado_por_Analista_de_Credito","Rechazado_por_Analista_de_Credito","Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Pendiente_por_Agente_Externo","Pendiente_por_Administrador_de_Credito","Aprobacion_por_Administrador_de_Credito","Rechazado_por_Administrador_de_Credito","Pendiente_por_Gerente_de_Credito","Aprobacion_por_Gerente_de_Credito","Rechazado_por_Gerente_de_Credito","Pendiente_por_Director_de_Finanzas","Aprobacion_por_Director_de_Finanzas","Rechazado_por_Director_de_Finanzas","Pendiente_por_Vicepresidencia_Comercial","Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['required']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['audited']=1;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_reporte_analisis_agente_ext_c.php

 // created: 2017-08-23 18:56:02
$dictionary['LOW01_SolicitudesCredito']['fields']['reporte_analisis_agente_ext_c']['labelValue']='Reporte de Análisis';
$dictionary['LOW01_SolicitudesCredito']['fields']['reporte_analisis_agente_ext_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_obs_vicep_comercial_c.php

 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_vicep_comercial_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_vicep_comercial_c']['labelValue']='Comentarios Vicepresidente Comercial';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_vicep_comercial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_vicep_comercial_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_vicep_comercial_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_vicep_comercial_c']['required']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_vicep_comercial_c']['audited']=1;


 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_telefono_cel_con_v_c.php

 // created: 2017-08-23 18:56:04
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_cel_con_v_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_reporte_analisis_realizado_c.php

 // created: 2017-07-25 22:50:14
$dictionary['LOW01_SolicitudesCredito']['fields']['reporte_analisis_realizado_c']['labelValue']='Reporte de análisis realizado';
$dictionary['LOW01_SolicitudesCredito']['fields']['reporte_analisis_realizado_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_ops_adv_env_sol_c.php

 // created: 2017-10-10 18:53:51
$dictionary['LOW01_SolicitudesCredito']['fields']['ops_adv_env_sol_c']['duplicate_merge_dom_value']=0;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_monto_deseado_c.php

 // created: 2018-08-22 22:12:37
$dictionary['LOW01_SolicitudesCredito']['fields']['monto_deseado_c']['labelValue']='Monto Deseado';
$dictionary['LOW01_SolicitudesCredito']['fields']['monto_deseado_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['monto_deseado_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['monto_deseado_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobado_por_Asesor_Comercial","Rechazado_por_Asesor_Comercial","Pendiente_por_Gerente_de_Ventas_Comerciales","Aprobado_por_Gerente_de_Ventas_Comerciales","Rechazado_por_Gerente_de_Ventas_Comerciales","Pendiente_por_Analista_de_Credito","Aprobado_por_Analista_de_Credito","Rechazado_por_Analista_de_Credito","Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Pendiente_por_Agente_Externo","Pendiente_por_Administrador_de_Credito","Aprobacion_por_Administrador_de_Credito","Rechazado_por_Administrador_de_Credito","Pendiente_por_Gerente_de_Credito","Aprobacion_por_Gerente_de_Credito","Rechazado_por_Gerente_de_Credito","Pendiente_por_Director_de_Finanzas","Aprobacion_por_Director_de_Finanzas","Rechazado_por_Director_de_Finanzas","Pendiente_por_Vicepresidencia_Comercial","Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_obs_asesor_comercial_c.php

 // created: 2018-09-17 21:19:42
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_asesor_comercial_c']['labelValue']='Comentario Asesor Comercial';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_asesor_comercial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_asesor_comercial_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_asesor_comercial_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobado_por_Asesor_Comercial","Rechazado_por_Asesor_Comercial","Pendiente_por_Gerente_de_Ventas_Comerciales","Aprobado_por_Gerente_de_Ventas_Comerciales","Rechazado_por_Gerente_de_Ventas_Comerciales","Pendiente_por_Analista_de_Credito","Aprobado_por_Analista_de_Credito","Rechazado_por_Analista_de_Credito","Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Pendiente_por_Agente_Externo","Pendiente_por_Administrador_de_Credito","Aprobacion_por_Administrador_de_Credito","Rechazado_por_Administrador_de_Credito","Pendiente_por_Gerente_de_Credito","Aprobacion_por_Gerente_de_Credito","Rechazado_por_Gerente_de_Credito","Pendiente_por_Director_de_Finanzas","Aprobacion_por_Director_de_Finanzas","Rechazado_por_Director_de_Finanzas","Pendiente_por_Vicepresidencia_Comercial","Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_name.php

 // created: 2017-08-08 03:36:46
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['len']='85';
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['required']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['audited']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['massupdate']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['unified_search']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['calculated']=false;

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_tienda.php

 // created: 2017-08-21 16:12:53
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['importable']='false';
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['duplicate_merge']='disabled';
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['calculated']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['formula']='getDropdownValue("sucursal_c_list",related($created_by_link,"sucursal_c"))';
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['enforced']=true;
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_promotor.php

 // created: 2017-08-15 23:00:31
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['importable']='false';
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['duplicate_merge']='disabled';
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['calculated']='true';
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['formula']='related($created_by_link,"full_name")';
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['enforced']=true;
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_tc.php

 // created: 2019-01-31 22:13:27
$dictionary['LOW01_SolicitudesCredito']['fields']['tc']['len']='132';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_de_cuenta.php

 // created: 2017-08-10 18:30:42
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_cuenta']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_folio_bc.php

 // created: 2019-01-31 22:15:31
$dictionary['LOW01_SolicitudesCredito']['fields']['folio_bc']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_apellido_paterno.php

 // created: 2019-01-31 22:16:34
$dictionary['LOW01_SolicitudesCredito']['fields']['apellido_paterno']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_apellido_materno.php

 // created: 2019-01-31 22:16:51
$dictionary['LOW01_SolicitudesCredito']['fields']['apellido_materno']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_nombre_razon_social.php

 // created: 2019-01-31 22:19:10
$dictionary['LOW01_SolicitudesCredito']['fields']['nombre_razon_social']['len']='83';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_pagina_de_internet.php

 // created: 2017-08-12 01:12:28
$dictionary['LOW01_SolicitudesCredito']['fields']['pagina_de_internet']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_calle_domicilio_actual_fiscal.php

 // created: 2019-01-31 22:20:13
$dictionary['LOW01_SolicitudesCredito']['fields']['calle_domicilio_actual_fiscal']['len']='65';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_exterior.php

 // created: 2019-01-31 22:21:07
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_exterior']['len']='53';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_interior.php

 // created: 2019-01-31 22:21:21
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_interior']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_cruza_con.php

 // created: 2019-01-31 22:22:43
$dictionary['LOW01_SolicitudesCredito']['fields']['cruza_con']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_colonia.php

 // created: 2019-01-31 22:23:21
$dictionary['LOW01_SolicitudesCredito']['fields']['colonia']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_municipio.php

 // created: 2019-01-31 22:24:05
$dictionary['LOW01_SolicitudesCredito']['fields']['municipio']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_estado.php

 // created: 2019-01-31 22:24:40
$dictionary['LOW01_SolicitudesCredito']['fields']['estado']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_cp.php

 // created: 2019-01-31 22:25:18
$dictionary['LOW01_SolicitudesCredito']['fields']['cp']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_anios_domicilio_actual.php

 // created: 2017-08-10 18:12:32
$dictionary['LOW01_SolicitudesCredito']['fields']['anios_domicilio_actual']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['anios_domicilio_actual']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['anios_domicilio_actual']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_meses_domicilio_actual.php

 // created: 2017-08-10 18:14:20
$dictionary['LOW01_SolicitudesCredito']['fields']['meses_domicilio_actual']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['meses_domicilio_actual']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['meses_domicilio_actual']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_anios_domicilio_anterior.php

 // created: 2017-08-10 18:12:52
$dictionary['LOW01_SolicitudesCredito']['fields']['anios_domicilio_anterior']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['anios_domicilio_anterior']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['anios_domicilio_anterior']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_meses_domicilio_anterior.php

 // created: 2017-08-10 18:14:45
$dictionary['LOW01_SolicitudesCredito']['fields']['meses_domicilio_anterior']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['meses_domicilio_anterior']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_clave_telefono_oficina.php

 // created: 2017-08-10 18:15:57
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_telefono_oficina']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_telefono_oficina']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_telefono_oficina']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_telefono_oficina.php

 // created: 2017-08-10 18:17:40
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_telefono_oficina']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_telefono_oficina']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_telefono_oficina']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_clave_telefono_conmutador.php

 // created: 2017-08-10 18:17:06
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_telefono_conmutador']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_telefono_conmutador']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_telefono_conmutador']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_telefono_conmutador.php

 // created: 2017-08-10 18:18:17
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_telefono_conmutador']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_telefono_conmutador']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_telefono_conmutador']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_pago_mensual.php

 // created: 2019-01-31 22:30:41
$dictionary['LOW01_SolicitudesCredito']['fields']['pago_mensual']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_nombre_del_aval.php

 // created: 2019-01-31 22:31:38
$dictionary['LOW01_SolicitudesCredito']['fields']['nombre_del_aval']['len']='61';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_fecha_de_constitucion.php

 // created: 2017-08-10 18:20:43
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_constitucion']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_constitucion']['len']='50';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_constitucion']['massupdate']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_constitucion']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_rfc_construccion.php

 // created: 2019-01-31 22:33:19
$dictionary['LOW01_SolicitudesCredito']['fields']['rfc_construccion']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_nombre_del_notario.php

 // created: 2019-01-31 22:34:37
$dictionary['LOW01_SolicitudesCredito']['fields']['nombre_del_notario']['len']='80';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_escritura.php

 // created: 2019-01-31 22:35:51
$dictionary['LOW01_SolicitudesCredito']['fields']['escritura']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_rpp.php

 // created: 2019-01-31 22:36:38
$dictionary['LOW01_SolicitudesCredito']['fields']['rpp']['len']='52';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_notaria.php

 // created: 2019-01-31 22:37:25
$dictionary['LOW01_SolicitudesCredito']['fields']['notaria']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_plaza.php

 // created: 2019-01-31 22:38:12
$dictionary['LOW01_SolicitudesCredito']['fields']['plaza']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_nombre_del_representante_legal.php

 // created: 2019-01-31 22:38:59
$dictionary['LOW01_SolicitudesCredito']['fields']['nombre_del_representante_legal']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_puesto.php

 // created: 2019-01-31 22:39:55
$dictionary['LOW01_SolicitudesCredito']['fields']['puesto']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_telefono_celular_construccion.php

 // created: 2017-08-10 18:25:21
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_celular_construccion']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_celular_construccion']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_celular_construccion']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_email_construccion.php

 // created: 2019-01-31 22:41:08
$dictionary['LOW01_SolicitudesCredito']['fields']['email_construccion']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_fecha_de_nacimiento_construc.php

 // created: 2019-01-31 22:42:11
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha_de_nacimiento_construc']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_rfc_del_repre_legal_construc.php

 // created: 2019-01-31 22:43:17
$dictionary['LOW01_SolicitudesCredito']['fields']['rfc_del_repre_legal_construc']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_de_sucursales.php

 // created: 2017-08-10 18:26:39
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_sucursales']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_sucursales']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_sucursales']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_de_empleados.php

 // created: 2017-08-10 18:28:21
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_empleados']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_empleados']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_empleados']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_ventas_anuales.php

 // created: 2019-01-31 22:44:59
$dictionary['LOW01_SolicitudesCredito']['fields']['ventas_anuales']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_principales_sedes_de_operacion.php

 // created: 2019-01-31 22:45:50
$dictionary['LOW01_SolicitudesCredito']['fields']['principales_sedes_de_operacion']['len']='116';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_nombre_funcionario1.php

 // created: 2019-01-31 22:46:44
$dictionary['LOW01_SolicitudesCredito']['fields']['nombre_funcionario1']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_puesto_funcionario1.php

 // created: 2017-07-28 15:54:42
$dictionary['LOW01_SolicitudesCredito']['fields']['puesto_funcionario1']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_nombre_funcionario2.php

 // created: 2019-01-31 22:47:07
$dictionary['LOW01_SolicitudesCredito']['fields']['nombre_funcionario2']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_puesto_funcionario2.php

 // created: 2019-01-31 22:49:30
$dictionary['LOW01_SolicitudesCredito']['fields']['puesto_funcionario2']['len']='52';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_nombre_funcionario3.php

 // created: 2019-01-31 22:47:29
$dictionary['LOW01_SolicitudesCredito']['fields']['nombre_funcionario3']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_puesto_funcionario3.php

 // created: 2019-01-31 22:49:50
$dictionary['LOW01_SolicitudesCredito']['fields']['puesto_funcionario3']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_institucion_ref_com1.php

 // created: 2019-01-31 22:51:19
$dictionary['LOW01_SolicitudesCredito']['fields']['institucion_ref_com1']['len']='54';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_institucion_ref_com2.php

 // created: 2019-01-31 22:51:33
$dictionary['LOW01_SolicitudesCredito']['fields']['institucion_ref_com2']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_de_cuenta_ref_com1.php

 // created: 2017-08-10 20:31:44
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_cuenta_ref_com1']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_cuenta_ref_com1']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_cuenta_ref_com1']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_numero_de_cuenta_ref_com2.php

 // created: 2017-08-10 20:52:18
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_cuenta_ref_com2']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_cuenta_ref_com2']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['numero_de_cuenta_ref_com2']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_nombre_proveedor2.php

 // created: 2019-01-31 22:54:20
$dictionary['LOW01_SolicitudesCredito']['fields']['nombre_proveedor2']['len']='73';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_nombre_proveedor1.php

 // created: 2019-01-31 22:54:00
$dictionary['LOW01_SolicitudesCredito']['fields']['nombre_proveedor1']['len']='71';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_nombre_proveedor3.php

 // created: 2019-01-31 22:54:36
$dictionary['LOW01_SolicitudesCredito']['fields']['nombre_proveedor3']['len']='65';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_clave_proveedor1.php

 // created: 2017-08-10 18:35:27
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_proveedor1']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_proveedor1']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_proveedor1']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_clave_proveedor2.php

 // created: 2017-08-10 18:36:03
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_proveedor2']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_proveedor2']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_proveedor2']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_clave_proveedor3.php

 // created: 2017-08-10 18:36:29
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_proveedor3']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_proveedor3']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['clave_proveedor3']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_telefono_proveedor1.php

 // created: 2017-08-11 01:16:18
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_proveedor1']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_proveedor1']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_proveedor1']['len']='50';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_telefono_proveedor2.php

 // created: 2017-08-02 00:00:19
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_proveedor2']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_proveedor2']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_proveedor2']['len']='50';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_telefono_proveedor3.php

 // created: 2017-08-02 00:02:05
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_proveedor3']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_proveedor3']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['telefono_proveedor3']['len']='50';

 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_correo_eletronico_terminos.php

 // created: 2019-01-31 22:58:18
$dictionary['LOW01_SolicitudesCredito']['fields']['correo_eletronico_terminos']['len']='61';
 
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Vardefs/sugarfield_extension.php

 // created: 2017-08-10 18:19:20
$dictionary['LOW01_SolicitudesCredito']['fields']['extension']['disable_num_format']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['extension']['type']='varchar';
$dictionary['LOW01_SolicitudesCredito']['fields']['extension']['len']='50';
 
?>
