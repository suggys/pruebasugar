<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Dependencies/low01_solicitudescredito_accounts_name_deps.php

$dependencies['LOW01_SolicitudesCredito']['low01_solicitudescredito_accounts_name_deps'] = array(
  'hooks' => array("edit","view"),
  'triggerFields' => array('low01_solicitudescredito_accounts_name'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'low01_solicitudescredito_accounts_name',
        'value' => 'not(equal($low01_solicitudescredito_accounts_name,""))',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'low01_solicitudescredito_accounts_name',
        'value' => 'false',
      ),
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/LOW01_SolicitudesCredito/Ext/Dependencies/folio_bc_deps.php

$dependencies['LOW01_SolicitudesCredito']['folio_bc'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal("1", "1")',
    'triggerFields' => array(''),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'folio_bc',
              'value' => 'true', //Formula
          ),
      ),
    ),
);

?>
