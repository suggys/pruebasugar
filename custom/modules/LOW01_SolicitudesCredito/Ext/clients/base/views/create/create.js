({
    extendsFrom: 'CreateView',
    initialize: function (options) {
        this._super('initialize', arguments);
    },
    _renderHtml: function (argument) {
        var self = this;
        this._super('_renderHtml', arguments);
        self.getField('folio_bc').setMode('readonly');
        self.getField('folio_bc').setDisabled(true);
      }
})
