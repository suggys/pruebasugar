({
    extendsFrom: 'RecordView',
    initialize: function (options) {
        var self = this;
        app.view.invokeParent(this, {type: 'view', name: 'record', method: 'initialize', args:[options]});
        self.model.on('sync', function(model, value){
            self._disableField();
        });
    },
   _renderHtml: function (argument) {
        var self = this;
        this._super('_renderHtml', arguments);
    },
    _disableField:function(disableFields){
        var self = this;
        self.getField('folio_bc').setMode('readonly');
        self.getField('folio_bc').setDisabled(true);
    },

})
