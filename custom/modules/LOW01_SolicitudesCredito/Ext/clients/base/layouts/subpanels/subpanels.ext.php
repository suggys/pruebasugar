<?php
// WARNING: The contents of this file are auto-generated.


// created: 2017-07-21 12:47:08
$viewdefs['LOW01_SolicitudesCredito']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_CALLS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'low01_solicitudescredito_activities_calls',
  ),
);

// created: 2017-07-21 12:47:08
$viewdefs['LOW01_SolicitudesCredito']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_EMAILS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'low01_solicitudescredito_activities_emails',
  ),
);

// created: 2017-07-21 12:47:08
$viewdefs['LOW01_SolicitudesCredito']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_MEETINGS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'low01_solicitudescredito_activities_meetings',
  ),
);

// created: 2017-07-21 12:47:08
$viewdefs['LOW01_SolicitudesCredito']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_NOTES_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'low01_solicitudescredito_activities_notes',
  ),
);

// created: 2017-07-21 12:47:08
$viewdefs['LOW01_SolicitudesCredito']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_TASKS_SUBPANEL_TITLE',
  'context' => 
  array (
    'link' => 'low01_solicitudescredito_activities_tasks',
  ),
);

// created: 2017-10-13 05:54:23
$viewdefs['LOW01_SolicitudesCredito']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE',
  'context' =>
  array (
    'link' => 'low01_solicitudescredito_contacts_1',
  ),
);


// created: 2017-07-26 16:30:16
$viewdefs['LOW01_SolicitudesCredito']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'context' => 
  array (
    'link' => 'low01_solicitudescredito_documents_1',
  ),
);

// created: 2017-10-17 01:51:44
$viewdefs['LOW01_SolicitudesCredito']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW03_AGENTESEXTERNOS_TITLE',
  'context' => 
  array (
    'link' => 'low01_solicitudescredito_low03_agentesexternos_1',
  ),
);