<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/*********************************************************************************

 * Description: Handles getting a list of fields to duplicate check and doing the duplicate checks
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 ********************************************************************************/
require_once 'modules/Import/ImportDuplicateCheck.php';
class CustomImportDuplicateCheck extends ImportDuplicateCheck
{
	/**
     * Private reference to the bean we're dealing with
     *
     * @var SugarBean
     */
    private $_focus;

    /*
     * holds current field when a duplicate has been found
     */
    public $_dupedFields =array();

    /**
     * Constructor
     *
     * @param object $focus bean
     */
    public function __construct($focus)
    {
        $this->_focus = $focus;
    }

    /**
     * Returns an array of indices for the current module
     *
     * @return array
     */
    
	private function _getIndexVardefs()
    {
        $indexes = $this->_focus->getIndices();

        //grab any custom indexes if they exist
        if($this->_focus->hasCustomFields()){
            $custmIndexes = $this->_focus->db->helper->get_indices($this->_focus->table_name.'_cstm');
            $indexes = array_merge($custmIndexes,$indexes);
        }

        // remove any that are datetime or time field as we can't dupe check them correctly since we don't export
        // seconds
        $fields = $this->_focus->getFieldDefinitions();
        foreach ($indexes as $key => $index) {
            foreach ($index['fields'] as $field) {
                if (isset($fields[$field])
                    && (
                        $fields[$field]['type'] == 'datetime'
                        || $fields[$field]['type'] == 'datetimecombo'
                        || $fields[$field]['type'] == 'time')) {
                    unset($indexes[$key]);
                    break 1;
                }
            }
        }

        if ( $this->_focus->getFieldDefinition('email1') )
            $indexes[] = array(
                'name' => 'special_idx_email1',
                'type' => 'index',
                'fields' => array('email1')
                );
        if ( $this->_focus->getFieldDefinition('email2') )
            $indexes[] = array(
                'name' => 'special_idx_email2',
                'type' => 'index',
                'fields' => array('email2')
                );

        return $indexes;
    }

	public function isADuplicateRecord( $indexlist )
    {
        // Bug #51264 : Importing updates to rows prevented by duplicates check
        if ( !empty($this->_focus) && ($this->_focus instanceof SugarBean) && !empty($this->_focus->id) )
        {
            $_focus = clone $this->_focus;
            $_focus->id = null;
            $_focus->retrieve($this->_focus->id);
            if ( !empty($_focus->id) )
            {
                return false;
            }
            unset($_focus);
        }

        //force duplicate check on certain indexes defined in vardefs
        global $dictionary;
        if (isset($dictionary[$this->_focus->object_name]['required_import_indexes'])) {
            $indexlist = array_merge($dictionary[$this->_focus->object_name]['required_import_indexes'], $indexlist);
        }

        //lets strip the indexes of the name field in the value and leave only the index name
        $origIndexList = $indexlist;
        $indexlist=array();
        $fieldlist=array();
        $customIndexlist=array();
        foreach($origIndexList as $iv){
            if(empty($iv)) continue;
            $field_index_array = explode('::',$iv);
            if($field_index_array[0] == 'customfield'){
                //this is a custom field, so place in custom array
                $customIndexlist[] = $field_index_array[1];

            }else{
                //this is not a custom field, so place in index list
                $indexlist[] = $field_index_array[0];
                if(isset($field_index_array[1])) {
                    $fieldlist[] = $field_index_array[1];
                }
            }
        }

        //if full_name is set, then manually search on the first and last name fields before iterating through rest of fields
        //this is a special handling of the name fields on people objects, the rest of the fields are checked individually
        if(in_array('full_name',$indexlist)){
            $newfocus = BeanFactory::getBean($this->_focus->module_dir);
            $result = $newfocus->retrieve_by_string_fields(array('deleted' =>'0', 'first_name'=>$this->_focus->first_name, 'last_name'=>$this->_focus->last_name),true);

            if ( !is_null($result) ){
                //set dupe field to full_name and name fields
                $this->_dupedFields[] = 'full_name';
                $this->_dupedFields[] = 'first_name';
                $this->_dupedFields[] = 'last_name';

            }
        }

        // loop through var def indexes and compare with selected indexes
        foreach ($this->_getIndexVardefs() as $index){
            // if we get an index not in the indexlist, loop
            if ( !in_array($index['name'],$indexlist) )
                continue;

            // This handles the special case of duplicate email checking
            if ( $index['name'] == 'special_idx_email1' || $index['name'] == 'special_idx_email2' ) {
                $emailAddress = BeanFactory::getBean('EmailAddresses');
                $email = $index['fields'][0];
                if ( $emailAddress->getCountEmailAddressByBean(
                        $this->_focus->$email,
                        $this->_focus,
                        ($index['name'] == 'special_idx_email1')
                        ) > 0 ){ foreach($index['fields'] as $field){
                        if($field !='deleted')
                            $this->_dupedFields[] = $field;
                    }
                }
            }
            // Adds a hook so you can define a method in the bean to handle dupe checking
            elseif ( isset($index['dupeCheckFunction']) ) {
                $functionName = substr_replace($index['dupeCheckFunction'],'',0,9);
                if ( method_exists($this->_focus,$functionName) && $this->_focus->$functionName($index) === true)
                    return $this->_focus->$functionName($index);
            }
            else {
                $index_fields = array('deleted' => '0');
                //search only for the field we have selected
                foreach($index['fields'] as $field){
                    if ($field == 'deleted' ||  !in_array($field,$fieldlist))
                        continue;
                    if (!in_array($field,$index_fields))
                        if (isset($this->_focus->$field) && strlen($this->_focus->$field) > 0)
                            $index_fields[$field] = $this->_focus->$field;
                }

                // if there are no valid fields in the index field list, loop
                if ( count($index_fields) <= 1 )
                    continue;

                $newfocus = BeanFactory::getBean($this->_focus->module_dir);
                $result = $newfocus->retrieve_by_string_fields($index_fields,true);

                if ( !is_null($result) ){
                    //remove deleted as a duped field
                    unset($index_fields['deleted']);

                    //create string based on array of dupe fields
                    $this->_dupedFields = array_merge(array_keys($index_fields),$this->_dupedFields);
                }
            }
        }


        //return true if any dupes were found
        if(!empty($this->_dupedFields)){
            return true;
        }

        if($this->_focus->module_dir === 'Accounts' && $this->isDuplicatedRFC()){
            return true;
        }


        return false;
    }

    public function isDuplicatedRFC()
    {
        $accounts = $this->getAccountsByRFC();
        if(!count($accounts)){
            return false;
        }
        else{
            $accountData = $accounts[0];
            if(!$accountData['gpe_grupo_empresas_accountsgpe_grupo_empresas_ida']){
                return true;
            }
            return $accountData['gpe_grupo_empresas_accountsgpe_grupo_empresas_ida'] != $this->_focus->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida;
        }
        return true; 
    }

    public function getAccountsByRFC()
    {
        $records = []; 
        $query = new SugarQuery();
        $query->from($this->_focus, array('team_security' => false));
        $query->where()
            ->equals('rfc_c', $this->_focus->rfc_c);
        $query->select(['id', 'name', 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida']);
        $records = $query->execute();
        return $records;
    }




}