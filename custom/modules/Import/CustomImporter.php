<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

require_once 'modules/Import/Importer.php';
require_once 'custom/modules/Import/CustomImportDuplicateCheck.php';

use CustomImportDuplicateCheck as ImportDuplicateCheck;

class CustomImporter extends Importer
{
	protected function importRow($row)
    {
        global $sugar_config, $mod_strings, $current_user;

        $focus = BeanFactory::getBean($this->bean->module_dir);
        $focus->unPopulateDefaultValues();
        $focus->save_from_post = false;
        $focus->team_id = null;
        $this->ifs->createdBeans = array();
        $this->importSource->resetRowErrorCounter();
        $do_save = true;

        // set the currency for the row, if it has a currency_id in the row
        if ($this->currencyFieldPosition !== false && !empty($row[$this->currencyFieldPosition])) {
            $currency_id = $row[$this->currencyFieldPosition];
            if (!isset($this->cachedCurrencySymbols[$currency_id])) {
                /** @var Currency $currency */
                $currency = BeanFactory::getBean('Currencies', $currency_id);
                $this->cachedCurrencySymbols[$currency_id] = $currency->symbol;
                unset($currency);
            }
            $this->ifs->currency_symbol = $this->cachedCurrencySymbols[$currency_id];
            $this->ifs->currency_id = $currency_id;
        }

        // Collect email addresses, and add them before save
        $emailAddresses = array(
            'non-primary' => array()
        );

        $fields_order = $this->getImportColumnsOrder($focus->getFieldDefinitions());
        foreach ($fields_order as $fieldNum) {
            // loop if this column isn't set
            if ( !isset($this->importColumns[$fieldNum]) )
                continue;

            // get this field's properties
            $field           = $this->importColumns[$fieldNum];
            $fieldDef        = $focus->getFieldDefinition($field);
            $fieldTranslated = translate((isset($fieldDef['vname'])?$fieldDef['vname']:$fieldDef['name']), $focus->module_dir)." (".$fieldDef['name'].")";
            $defaultRowValue = '';
            // Bug 37241 - Don't re-import over a field we already set during the importing of another field
            if ( !empty($focus->$field) )
                continue;

            // translate strings
            global $locale;
            if(empty($locale))
            {
                $locale = Localization::getObject();
            }
            if ( isset($row[$fieldNum]) )
            {
                $rowValue = strip_tags(trim($row[$fieldNum]));
            }
            else if( isset($this->sugarToExternalSourceFieldMap[$field]) && isset($row[$this->sugarToExternalSourceFieldMap[$field]]) )
            {
                $rowValue = strip_tags(trim($row[$this->sugarToExternalSourceFieldMap[$field]]));
            }
            else
            {
                $rowValue = '';
            }

            // If there is an default value then use it instead
            if (!empty($_REQUEST["default_value_$field"])) {

                if ($fieldDef['type'] == 'relate' && !empty($row[$fieldNum]) && $row[$fieldNum] != $_REQUEST["default_value_$field"]) {
                    if (!empty($fieldDef['id_name']) && empty($row[$fieldDef['id_name']])) {
                        $focus->{$fieldDef['id_name']} = '';
                    }
                }

                $defaultRowValue = $this->populateDefaultMapValue($field, $_REQUEST["default_value_$field"], $fieldDef);

                if(!empty($fieldDef['custom_type']) && $fieldDef['custom_type'] == 'teamset' && empty($rowValue))
                {
                    require_once('include/SugarFields/Fields/Teamset/SugarFieldTeamset.php');
                    $rowValue = implode(', ', SugarFieldTeamset::getTeamsFromRequest($field));
                }

                if( empty($rowValue))
                {
                    $rowValue = $defaultRowValue;
                    //reset the default value to empty
                    $defaultRowValue='';
                }
            }

            // Bug 22705 - Don't update the First Name or Last Name value if Full Name is set
            if ( in_array($field, array('first_name','last_name')) && !empty($focus->full_name) )
                continue;

            // loop if this value has not been set
            if ( !isset($rowValue) )
                continue;

            // If the field is required and blank then error out
            if ( array_key_exists($field,$focus->get_import_required_fields()) && empty($rowValue) && $rowValue!='0')
            {
                $this->importSource->writeError( $mod_strings['LBL_REQUIRED_VALUE'],$fieldTranslated,'NULL');
                $do_save = false;
            }

            // Handle the special case 'Sync to Mail Client'
            if ( $focus->object_name == "Contact" && $field == 'sync_contact' )
            {
                /**
                 * Bug #41194 : if true used as value of sync_contact - add curent user to list to sync
                 */
                if ( true == $rowValue || 'true' == strtolower($rowValue)) {
                    $focus->sync_contact = $focus->id;
                } elseif (false == $rowValue || 'false' == strtolower($rowValue)) {
                    $focus->sync_contact = '';
                } else {
                    $bad_names = array();
                    $returnValue = $this->ifs->synctooutlook($rowValue,$fieldDef,$bad_names);
                    // try the default value on fail
                    if ( !$returnValue && !empty($defaultRowValue) )
                        $returnValue = $this->ifs->synctooutlook($defaultRowValue, $fieldDef, $bad_names);
                    if ( !$returnValue )
                    {
                        $this->importSource->writeError($mod_strings['LBL_ERROR_SYNC_USERS'], $fieldTranslated, $bad_names);
                        $do_save = 0;
                    } else {
                        $focus->sync_contact = $returnValue;
                    }
                }
            }

            // Handle email, email1 and email2 fields ( these don't have the type of email )
            if ( $field == 'email' || $field == 'email1' || $field == 'email2' )
            {
                $returnValue = $this->ifs->email($rowValue, $fieldDef, $focus);
                // try the default value on fail
                if ( !$returnValue && !empty($defaultRowValue) )
                    $returnValue = $this->ifs->email( $defaultRowValue, $fieldDef);
                if ( $returnValue === FALSE )
                {
                    $do_save=0;
                    $this->importSource->writeError( $mod_strings['LBL_ERROR_INVALID_EMAIL'], $fieldTranslated, $rowValue);
                }
                else
                {
                    $rowValue = $returnValue;

                    $address = array(
                        'email_address' => $rowValue,
                        'primary_address' => $field == 'email',
                        'invalid_email' => false,
                        'opt_out' => false,
                    );

                    // check for current opt_out and invalid email settings for this email address
                    // if we find any, set them now
                    $emailres = $focus->db->query( "SELECT opt_out, invalid_email FROM email_addresses WHERE email_address = '".$focus->db->quote($rowValue)."'");
                    if ( $emailrow = $focus->db->fetchByAssoc($emailres) )
                    {
                        $address = array_merge($address, $emailrow);
                    }

                    if ($field === 'email') {

                        //flip the array so we can use it to get the key #
                        $flippedVals = array_flip($this->importColumns);

                        //if the opt out column is set, then attempt to retrieve the values
                        if(isset($flippedVals['email_opt_out'])){
                            //if the string for this value has a length, then use it.
                            if(isset($row[$flippedVals['email_opt_out']]) && strlen($row[$flippedVals['email_opt_out']])>0){
                                $address['opt_out'] = $row[$flippedVals['email_opt_out']];
                            }
                        }

                        //if the invalid email column is set, then attempt to retrieve the values
                        if(isset($flippedVals['invalid_email'])){
                            //if the string for this value has a length, then use it.
                            if(isset($row[$flippedVals['invalid_email']]) && strlen($row[$flippedVals['invalid_email']])>0){
                                $address['invalid_email'] = $row[$flippedVals['invalid_email']];
                            }
                        }
                        $emailAddresses['primary'] = $address;
                    } else {
                        $emailAddresses['non-primary'][] = $address;
                    }
                }
            }

            if ($field == 'email_addresses_non_primary') {
                $nonPrimaryAddresses = $this->handleNonPrimaryEmails($rowValue, $defaultRowValue, $fieldTranslated);
                $emailAddresses['non-primary'] = array_merge($emailAddresses['non-primary'], $nonPrimaryAddresses);
            }

            // Handle splitting Full Name into First and Last Name parts
            if ( $field == 'full_name' && !empty($rowValue) )
            {
                $this->ifs->fullname($rowValue,$fieldDef,$focus);
            }

            // to maintain 451 compatiblity
            if(!isset($fieldDef['module']) && $fieldDef['type']=='relate')
                $fieldDef['module'] = ucfirst($fieldDef['table']);

            if (!isset($fieldDef['module']) && $fieldDef['type']=='enum') {
                $fieldDef['module'] = $focus->module_name;
            }

            if(isset($fieldDef['custom_type']) && !empty($fieldDef['custom_type']))
                $fieldDef['type'] = $fieldDef['custom_type'];

            // If the field is empty then there is no need to check the data
            if( !empty($rowValue) )
            {
                //Start
                $rowValue = $this->sanitizeFieldValueByType($rowValue, $fieldDef, $defaultRowValue, $focus, $fieldTranslated);
                if ($rowValue === FALSE) {
					/* BUG 51213 - jeff @ neposystems.com */
                    $do_save = false;
                    continue;
				}
            }

            // if the parent type is in singular form, get the real module name for parent_type
            if (isset($fieldDef['type']) && $fieldDef['type']=='parent_type') {
                $rowValue = get_module_from_singular($rowValue);
            }

            $focus->$field = $rowValue;
            unset($defaultRowValue);
        }

        // Now try to validate flex relate fields
        if ( isset($focus->field_defs['parent_name']) && isset($focus->parent_name) && ($focus->field_defs['parent_name']['type'] == 'parent') )
        {
            // populate values from the picker widget if the import file doesn't have them
            $parent_idField = $focus->field_defs['parent_name']['id_name'];
            if ( empty($focus->$parent_idField) && !empty($_REQUEST[$parent_idField]) )
                $focus->$parent_idField = $_REQUEST[$parent_idField];

            $parent_typeField = $focus->field_defs['parent_name']['type_name'];

            if ( empty($focus->$parent_typeField) && !empty($_REQUEST[$parent_typeField]) )
                $focus->$parent_typeField = $_REQUEST[$parent_typeField];
            // now validate it
            $returnValue = $this->ifs->parent($focus->parent_name,$focus->field_defs['parent_name'],$focus, empty($_REQUEST['parent_name']));
            if ( !$returnValue && !empty($_REQUEST['parent_name']) )
                $returnValue = $this->ifs->parent( $_REQUEST['parent_name'],$focus->field_defs['parent_name'], $focus);
        }

        // check to see that the indexes being entered are unique.
        if (isset($_REQUEST['enabled_dupes']) && $_REQUEST['enabled_dupes'] != "")
        {
            $toDecode = html_entity_decode  ($_REQUEST['enabled_dupes'], ENT_QUOTES);
            $enabled_dupes = json_decode($toDecode);
            $idc = new ImportDuplicateCheck($focus);

            if ( $idc->isADuplicateRecord($enabled_dupes) )
            {
                $this->importSource->markRowAsDuplicate($idc->_dupedFields);
                $this->_undoCreatedBeans($this->ifs->createdBeans);
                return;
            }
        }
        //Allow fields to be passed in for dup check as well (used by external adapters)
        else if( !empty($_REQUEST['enabled_dup_fields']) )
        {
            $toDecode = html_entity_decode  ($_REQUEST['enabled_dup_fields'], ENT_QUOTES);
            $enabled_dup_fields = json_decode($toDecode);
            $idc = new ImportDuplicateCheck($focus);
            if ( $idc->isADuplicateRecordByFields($enabled_dup_fields) )
            {
                $this->importSource->markRowAsDuplicate($idc->_dupedFields);
                $this->_undoCreatedBeans($this->ifs->createdBeans);
                return;
            }
        }

        // if the id was specified
        $newRecord = true;
        if ( !empty($focus->id) )
        {
            $focus->id = $this->_convertId($focus->id);

            // check if it already exists
            $query = "SELECT * FROM {$focus->table_name} WHERE id='".$focus->db->quote($focus->id)."'";
            $result = $focus->db->query($query)
            or sugar_die("Error selecting sugarbean: ");

            $dbrow = $focus->db->fetchByAssoc($result);

            if (isset ($dbrow['id']) && $dbrow['id'] != -1)
            {
                // if it exists but was deleted, just remove it
                // if you import a record, and specify the ID,
                // and the record ID already exists and is deleted... the "old" deleted record
                // should be removed and replaced with the new record you are importing.
                if (isset ($dbrow['deleted']) && $dbrow['deleted'] == 1)
                {
                    $this->removeDeletedBean($focus);
                    $focus->new_with_id = true;
                }
                else
                {
                    if( ! $this->isUpdateOnly )
                    {
                        $this->importSource->writeError($mod_strings['LBL_ID_EXISTS_ALREADY'],'ID',$focus->id);
                        $this->_undoCreatedBeans($this->ifs->createdBeans);
                        return;
                    }

                    $clonedBean = $this->cloneExistingBean($focus);
                    if($clonedBean === FALSE)
                    {
                        $this->importSource->writeError($mod_strings['LBL_RECORD_CANNOT_BE_UPDATED'],'ID',$focus->id);
                        $this->_undoCreatedBeans($this->ifs->createdBeans);
                        return;
                    }
                    else
                    {
                        $focus = $clonedBean;
                        $newRecord = FALSE;
                    }
                }
            }
            else
            {
                $focus->new_with_id = true;
            }
        }

        try {
            // Update e-mails here, because we're calling retrieve, and it overwrites the emailAddress object
            if ($focus->hasEmails()) {
                $this->handleEmailUpdate($focus, $emailAddresses);
            }
        } catch (Exception $e) {
            $this->importSource->writeError(
                $e->getMessage(),
                $fieldTranslated,
                $focus->id
            );
            $do_save = false;
        }

        if ($do_save)
        {
            // Need to ensure the imported record has an ID
            if ($newRecord && empty($focus->id)) {
                $focus->id = create_guid();
                $focus->new_with_id = true;
            }

            $this->handleTagsImport($focus, $row);
            $this->saveImportBean($focus, $newRecord);
            // Update the created/updated counter
            $this->importSource->markRowAsImported($newRecord);
        }
        else
            $this->_undoCreatedBeans($this->ifs->createdBeans);

        unset($defaultRowValue);

    }

}