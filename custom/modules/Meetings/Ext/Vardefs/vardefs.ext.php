<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/rli_link_workflow.php

$dictionary['Meeting']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Meeting']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/low01_solicitudescredito_activities_meetings_Meetings.php

// created: 2017-07-21 12:47:08
$dictionary["Meeting"]["fields"]["low01_solicitudescredito_activities_meetings"] = array (
  'name' => 'low01_solicitudescredito_activities_meetings',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_meetings',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_MEETINGS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/MeetingsVisibility.php

$dictionary['Meeting']['visibility']["MeetingsVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/sugarfield_description.php

 // created: 2018-01-25 15:08:52
$dictionary['Meeting']['fields']['description']['audited']=false;
$dictionary['Meeting']['fields']['description']['massupdate']=false;
$dictionary['Meeting']['fields']['description']['comments']='Full text of the note';
$dictionary['Meeting']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['description']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.55',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['description']['calculated']=false;
$dictionary['Meeting']['fields']['description']['rows']='6';
$dictionary['Meeting']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Meetings/Ext/Vardefs/dise_prospectos_cocina_meetings_Meetings.php

// created: 2018-02-01 19:07:34
$dictionary["Meeting"]["fields"]["dise_prospectos_cocina_meetings"] = array (
  'name' => 'dise_prospectos_cocina_meetings',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_meetings',
  'source' => 'non-db',
  'module' => 'dise_Prospectos_cocina',
  'bean_name' => 'dise_Prospectos_cocina',
  'side' => 'right',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_MEETINGS_FROM_MEETINGS_TITLE',
  'id_name' => 'dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida',
  'link-type' => 'one',
);
$dictionary["Meeting"]["fields"]["dise_prospectos_cocina_meetings_name"] = array (
  'name' => 'dise_prospectos_cocina_meetings_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_MEETINGS_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'save' => true,
  'id_name' => 'dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_meetings',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'name',
);
$dictionary["Meeting"]["fields"]["dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida"] = array (
  'name' => 'dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_MEETINGS_FROM_MEETINGS_TITLE_ID',
  'id_name' => 'dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_meetings',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
