<?php
$popupMeta = array (
    'moduleMain' => 'orden_Ordenes',
    'varName' => 'orden_Ordenes',
    'orderBy' => 'orden_ordenes.name',
    'whereClauses' => array (
  'name' => 'orden_ordenes.name',
  'importado_c' => 'orden_ordenes_cstm.importado_c',
  'id_customer_c' => 'orden_ordenes_cstm.id_customer_c',
  'orden_compra_c' => 'orden_ordenes_cstm.orden_compra_c',
  'id_cajero_c' => 'orden_ordenes_cstm.id_cajero_c',
  'id_caja_c' => 'orden_ordenes_cstm.id_caja_c',
  'id_sucursal_c' => 'orden_ordenes_cstm.id_sucursal_c',
  'accounts_orden_ordenes_1_name' => 'orden_ordenes.accounts_orden_ordenes_1_name',
  'no_ticket_c' => 'orden_ordenes_cstm.no_ticket_c',
  'estado_c' => 'orden_ordenes_cstm.estado_c',
),
    'searchInputs' => array (
  1 => 'name',
  12 => 'importado_c',
  13 => 'id_customer_c',
  14 => 'orden_compra_c',
  15 => 'id_cajero_c',
  16 => 'id_caja_c',
  17 => 'id_sucursal_c',
  18 => 'accounts_orden_ordenes_1_name',
  19 => 'no_ticket_c',
  20 => 'estado_c',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'estado_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_ESTADO_C',
    'width' => '10%',
    'name' => 'estado_c',
  ),
  'no_ticket_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NO_TICKET',
    'width' => '10%',
    'name' => 'no_ticket_c',
  ),
  'accounts_orden_ordenes_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_ORDEN_ORDENES_1ACCOUNTS_IDA',
    'width' => '10%',
    'name' => 'accounts_orden_ordenes_1_name',
  ),
  'id_sucursal_c' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_ID_SUCURSAL',
    'width' => '10%',
    'name' => 'id_sucursal_c',
  ),
  'id_cajero_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ID_CAJERO',
    'width' => '10%',
    'name' => 'id_cajero_c',
  ),
  'id_caja_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ID_CAJA',
    'width' => '10%',
    'name' => 'id_caja_c',
  ),
  'orden_compra_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ORDEN_COMPRA_C',
    'width' => '10%',
    'name' => 'orden_compra_c',
  ),
  'id_customer_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ID_CUSTOMER_C',
    'width' => '10%',
    'name' => 'id_customer_c',
  ),
  'importado_c' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_IMPORTADO',
    'width' => '10%',
    'name' => 'importado_c',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'ESTADO_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_ESTADO_C',
    'width' => '10%',
    'name' => 'estado_c',
  ),
  'NO_TICKET_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_NO_TICKET',
    'width' => '10%',
    'name' => 'no_ticket_c',
  ),
  'ACCOUNTS_ORDEN_ORDENES_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_ORDEN_ORDENES_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'accounts_orden_ordenes_1_name',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_modified',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
    'name' => 'date_entered',
  ),
),
);
