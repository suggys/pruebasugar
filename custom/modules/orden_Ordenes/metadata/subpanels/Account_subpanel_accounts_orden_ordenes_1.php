<?php
// created: 2017-01-23 22:43:57
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'estado_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_ESTADO_C',
    'width' => '10%',
  ),
  'no_ticket_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_NO_TICKET',
    'width' => '10%',
  ),
  'folio_orden_inicial_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_FOLIO_ORDEN_INICIAL_C',
    'width' => '10%',
  ),
  'amount_c' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_AMOUNT',
    'currency_format' => true,
    'width' => '10%',
  ),
  'por_entregar_c' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_POR_ENTREGAR',
    'currency_format' => true,
    'width' => '10%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
);