({
	extendsFrom: 'RecordView',

	initialize: function () {
        this._super('initialize', arguments);
        var self = this;

        self.model.on('sync', function(model, value){
            self._disableField();
        });
	},

    _disableField:function(){
        var self = this;

        if(app.user.get('type') != 'admin'){

            self.getField('importado_c').setMode('readonly');
            self.getField('importado_c').setDisabled('true');

            self.getField('name').setMode('readonly');
            self.getField('name').setDisabled('true');

            self.getField('estado_c').setMode('readonly');
            self.getField('estado_c').setDisabled('true');

            self.getField('no_ticket_c').setMode('readonly');
            self.getField('no_ticket_c').setDisabled('true');

            self.getField('amount_c').setMode('readonly');
            self.getField('amount_c').setDisabled('true');

            self.getField('fecha_transaccion_c').setMode('readonly');
            self.getField('fecha_transaccion_c').setDisabled('true');

            self.getField('id_caja_c').setMode('readonly');
            self.getField('id_caja_c').setDisabled('true');

            self.getField('id_cajero_c').setMode('readonly');
            self.getField('id_cajero_c').setDisabled('true');

            self.getField('id_sucursal_c').setMode('readonly');
            self.getField('id_sucursal_c').setDisabled('true');

            self.getField('orden_ordenes_orden_ordenes_1_name').setMode('readonly');
            self.getField('orden_ordenes_orden_ordenes_1_name').setDisabled('true');

            /*
            self.getField('transactiontypecode_c').setMode('readonly');
            self.getField('transactiontypecode_c').setDisabled('true');
            */

            self.getField('date_entered_by').setMode('readonly');
            self.getField('date_entered_by').setDisabled('true');

            self.getField('date_modified_by').setMode('readonly');
            self.getField('date_modified_by').setDisabled('true');
        }
    }
})
