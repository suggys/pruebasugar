<?php
$module_name = 'orden_Ordenes';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'record' =>
      array (
        'buttons' =>
        array (
          0 =>
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' =>
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 =>
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 =>
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' =>
            array (
              0 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 =>
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 =>
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 =>
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 =>
              array (
                'type' => 'divider',
              ),
              5 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'orden_Ordenes',
                'acl_action' => 'create',
              ),
              7 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 =>
              array (
                'type' => 'divider',
              ),
              9 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 =>
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' =>
        array (
          0 =>
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'readonly' => false,
              ),
              2 =>
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 =>
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 =>
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'id_sucursal_c',
                'label' => 'LBL_ID_SUCURSAL',
              ),
              1 =>
              array (
                'name' => 'id_cajero_c',
                'label' => 'LBL_ID_CAJERO',
              ),
              2 =>
              array (
                'name' => 'id_caja_c',
                'label' => 'LBL_ID_CAJA',
              ),
              3 =>
              array (
                'name' => 'estado_c',
                'label' => 'LBL_ESTADO_C',
              ),
              4 =>
              array (
                'name' => 'no_ticket_c',
                'label' => 'LBL_NO_TICKET',
              ),
              5 =>
              array (
                'name' => 'fecha_transaccion_c',
                'label' => 'LBL_FECHA_TRANSACCION_C',
              ),
              6 =>
              array (
                'name' => 'id_customer_c',
                'label' => 'LBL_ID_CUSTOMER_C',
              ),
              7 =>
              array (
                'name' => 'accounts_orden_ordenes_1_name',
                'label' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE',
                'readonly' => true,
              ),
              8 =>
              array (
                'name' => 'orden_compra_c',
                'label' => 'LBL_ORDEN_COMPRA_C',
              ),
              9 =>
              array (
              ),
              10 =>
              array (
                'name' => 'folio_orden_inicial_c',
                'label' => 'LBL_FOLIO_ORDEN_INICIAL_C',
              ),
              11 =>
              array (
                'name' => 'orden_ordenes_orden_ordenes_1_name',
                'label' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_L_TITLE',
                'readonly' => true,
              ),
              12 =>
              array (
                'name' => 'folio_c',
                'label' => 'LBL_FOLIO_C',
              ),
              13 =>
              array (
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'amount_c',
                'label' => 'LBL_AMOUNT',
              ),
              14 =>
              array (
                'name' => 'nc_notas_credito_orden_ordenes_name',
                'readonly' => true,
              ),
              15 =>
              array (
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'por_entregar_c',
                'label' => 'LBL_POR_ENTREGAR',
                'readonly' => true,
              ),
              16 =>
              array (
                'name' => 'description',
                'span' => 6,
              ),
              17 =>
              array (
                'name' => 'lowae_autorizacion_especial_orden_ordenes_1_name',
                'readonly' => true,
                'span' => 6,
              ),
            ),
          ),
          2 =>
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' =>
                array (
                  0 =>
                  array (
                    'name' => 'date_modified',
                  ),
                  1 =>
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 =>
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              1 =>
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' =>
                array (
                  0 =>
                  array (
                    'name' => 'date_entered',
                  ),
                  1 =>
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 =>
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              2 =>
              array (
                'name' => 'assigned_user_name',
                'span' => 6,
              ),
              3 =>
              array (
                'name' => 'importado_c',
                'label' => 'LBL_IMPORTADO',
                'span' => 6,
              ),
              4 =>
              array (
                'name' => 'importadode_c',
                'label' => 'LBL_IMPORTADODE',
                'readonly' => true,
              ),
              5 =>
              array (
              ),
            ),
          ),
        ),
        'templateMeta' =>
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
