<?php
// created: 2016-11-15 23:48:07
$viewdefs['orden_Ordenes']['base']['view']['subpanel-for-orden_ordenes-orden_ordenes_orden_ordenes_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'estado_c',
          'label' => 'LBL_ESTADO_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'no_ticket_c',
          'label' => 'LBL_NO_TICKET',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'sub_total_c',
          'label' => 'LBL_SUB_TOTAL_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'iva_c',
          'label' => 'LBL_IVA_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'amount_c',
          'label' => 'LBL_AMOUNT',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'fecha_transaccion_c',
          'label' => 'LBL_FECHA_TRANSACCION_C',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
        8 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);