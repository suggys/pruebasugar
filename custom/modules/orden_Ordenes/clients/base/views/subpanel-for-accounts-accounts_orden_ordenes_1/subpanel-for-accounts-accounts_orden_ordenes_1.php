<?php
// created: 2017-01-23 22:44:17
$viewdefs['orden_Ordenes']['base']['view']['subpanel-for-accounts-accounts_orden_ordenes_1'] = array (
  'buttons' => 
  array (
  ),
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'estado_c',
          'label' => 'LBL_ESTADO_C',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'no_ticket_c',
          'label' => 'LBL_NO_TICKET',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'folio_orden_inicial_c',
          'label' => 'LBL_FOLIO_ORDEN_INICIAL_C',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'amount_c',
          'label' => 'LBL_AMOUNT',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'por_entregar_c',
          'label' => 'LBL_POR_ENTREGAR',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        6 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
        7 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);