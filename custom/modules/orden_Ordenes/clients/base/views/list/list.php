<?php
$module_name = 'orden_Ordenes';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'readonly' => false,
              ),
              1 =>
              array (
                'name' => 'estado_c',
                'label' => 'LBL_ESTADO_C',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              2 =>
              array (
                'name' => 'no_ticket_c',
                'label' => 'LBL_NO_TICKET',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              3 =>
              array (
                'name' => 'accounts_orden_ordenes_1_name',
                'label' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE',
                'enabled' => true,
                'id' => 'ACCOUNTS_ORDEN_ORDENES_1ACCOUNTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
                'readonly' => true,
              ),
              4 =>
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'readonly' => true,
              ),
              5 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              7 =>
              array (
                'name' => 'importadode_c',
                'label' => 'LBL_IMPORTADODE',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              8 =>
              array (
                'name' => 'id_sucursal_c',
                'label' => 'LBL_ID_SUCURSAL',
                'enabled' => true,
                'default' => false,
              ),
              9 =>
              array (
                'name' => 'id_cajero_c',
                'label' => 'LBL_ID_CAJERO',
                'enabled' => true,
                'default' => false,
              ),
              10 =>
              array (
                'name' => 'id_caja_c',
                'label' => 'LBL_ID_CAJA',
                'enabled' => true,
                'default' => false,
              ),
              11 =>
              array (
                'name' => 'orden_compra_c',
                'label' => 'LBL_ORDEN_COMPRA_C',
                'enabled' => true,
                'default' => false,
              ),
              12 =>
              array (
                'name' => 'sub_total_c',
                'label' => 'LBL_SUB_TOTAL_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              13 =>
              array (
                'name' => 'iva_c',
                'label' => 'LBL_IVA_C',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              14 =>
              array (
                'name' => 'amount_c',
                'label' => 'LBL_AMOUNT',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              15 =>
              array (
                'name' => 'por_entregar_c',
                'label' => 'LBL_POR_ENTREGAR',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => false,
                'readonly' => true,
              ),
              16 =>
              array (
                'name' => 'id_customer_c',
                'label' => 'LBL_ID_CUSTOMER_C',
                'enabled' => true,
                'default' => false,
                'readonly' => true,
              ),
              17 =>
              array (
                'name' => 'importado_c',
                'label' => 'LBL_IMPORTADO',
                'enabled' => true,
                'default' => false,
                'readonly' => true,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
