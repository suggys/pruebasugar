({
  extendsFrom: "EnumField",

  format: function(value){
    if (this.def.isMultiSelect && _.isArray(value) && _.indexOf(value, '') > -1) {
        value = _.clone(value);

        // Delete empty values from the select list
        delete value[''];
    }
    if(this.def.isMultiSelect && _.isString(value)){
      return this.convertMultiSelectDefaultString(value);
    } else {
      if(this.name === 'estado_c' && value === ""){
        this.estadoVenta = true;
      }
      return value;
    }
  },
})
