<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class VincularyDesvincularOrdenes{
	public function vincularOrdenes($bean, $event, $arguments)
	{
		if(isset($bean->importado_c) && $bean->importado_c){
			return true;
		}
		
		if(
		   $arguments['module'] == "orden_Ordenes"
            && $arguments['related_module'] == "orden_Ordenes"
            && $arguments['link'] == "orden_ordenes_orden_ordenes_1"
            && !empty($arguments['id'])
            && !empty($arguments['related_id'])
        ){
        	
			$id_orden_padre = $arguments['id'];
			$id_orden_hija = $arguments['related_id'];

			$ordenPadre = BeanFactory::getBean('orden_Ordenes', $id_orden_padre);
			$ordenHija = BeanFactory::getBean('orden_Ordenes', $id_orden_hija);
			if(
				(
					$ordenHija->estado_c === "2" || $ordenHija->estado_c === "5" || $ordenHija->estado_c === "4"
					|| $ordenHija->estado_c === "8" || $ordenHija->estado_c === "9"
				)
				&& !empty($ordenHija->folio_orden_inicial_c)
			){

				$ordenPadre->por_entregar_c -= $ordenHija->amount_c;

				$ordenPadre->es_orden_inicial_c = true;
				if($ordenPadre->id){
					$ordenPadre->save(false);
				}
			}
		}

		if(
		   $arguments['module'] == "orden_Ordenes"
        && $arguments['related_module'] == "Accounts"
        && ($bean->estado_c === "0" || (empty($bean->estado_c)))
				&& $bean->deleted === 0 
		){
			$account = BeanFactory::getBean('Accounts', $arguments['related_id']);
			if($account->id){
				if(!$account->importado_c){
					if(!empty($bean->id_customer_c)){
						$fieldIndex = substr($bean->id_customer_c, -4) < 7000 ? "id_ar_credit_c" : "id_linea_anticipo_c";
						if($fieldIndex==="id_linea_anticipo_c"){
							$account->saldo_favor_c -= $bean->amount_c;
							$account->save(false);
						}
						else{
							//ADD: MCA 051220166 , IF
							if($account->credito_disponible_c < $bean->amount_c){
								$this->processAutorizacionEspecial($account, $bean);
							} else {
								//buscamos Aut Especial Otro o Promesa de Pago
								if($this->processAutorizacionEspecial($account, $bean, true)){
									$this->processAutorizacionEspecial($account, $bean);
								}
							}
							
							$account->credito_disponible_c -= $bean->amount_c;
							$account->save(false);

							if($this->validaAplicaDescuento($account)){
								if($account->credito_disponible_c <= 0 && $account->codigo_bloqueo_c != "CV"){
									$account->estado_bloqueo_c = "Bloqueado";
									if(empty($account->codigo_bloqueo_c)){
										$account->codigo_bloqueo_c = "LE";
									}
									$account->bloqueante_c = true;
									$account->save(false);
								}
							}
							else{
								$grupoEmpresas = !empty($account->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida) ? BeanFactory::getBean('GPE_Grupo_empresas', $account->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida) : null;
								if(
									$grupoEmpresas
									&& $grupoEmpresas->id
									&& $grupoEmpresas->administrar_credito_grupal
									&& $grupoEmpresas->credito_diponible_grupo_c <= 0
								){
									$account->estado_bloqueo_c = "Bloqueado";
									if(empty($account->codigo_bloqueo_c)){
										$account->codigo_bloqueo_c = "LE";
									}
									$account->bloqueante_c = true;
									$account->save(false);
								}
							}
						}
					}
				}
			}
		}
		// $GLOBALS["log"]->fatal("vincularOrdenes:estado:".$bean->estado_c);
		if(
				$arguments['module'] == "orden_Ordenes"
      	&& $arguments['related_module'] == "Accounts"
				&& !empty($bean->folio_orden_inicial_c)
        && ($bean->estado_c === "4" || $bean->estado_c === "6" || $bean->estado_c === "8" || $bean->estado_c === "9")
				&& $bean->deleted === 0
		//linea para modificar si es una orden AR 27-04-2018 HSLR
				&& !empty($bean->id_customer_c)  
    ){
			$bean->load_relationship('orden_ordenes_orden_ordenes_1');
			$account = BeanFactory::getBean('Accounts', $arguments['related_id']);
			if($account->id){
				$amount = ($bean->estado_c === "4" || $bean->estado_c === "6") ? $bean->amount_c : $bean->sub_total_c;

				$fieldIndex = substr($bean->id_customer_c, -4) < 7000 ? "id_ar_credit_c" : "id_linea_anticipo_c";
				if($fieldIndex==="id_linea_anticipo_c"){
					$account->saldo_favor_c += abs($bean->amount_c);
				}
				else{
					$amount = abs($amount);
					$account->credito_disponible_c += $amount;
					$this->devuelveAutorizacion($bean, $amount);

					if(
						$account->estado_bloqueo_c === 'Bloqueado'
						&& $account->codigo_bloqueo_c === 'LE'
						&& $account->bloqueante_c
						&& $account->credito_disponible_c > 0
					){
						$account->estado_bloqueo_c = 'Desbloqueado';
						$account->codigo_bloqueo_c = '';
					}
				}
				$account->save(false);

			}
		}

		if(
				$arguments['module'] == "orden_Ordenes"
      	&& $arguments['related_module'] == "orden_Ordenes"
				&& !empty($bean->folio_orden_inicial_c)
        && ($bean->estado_c === "4" || $bean->estado_c === "6" || $bean->estado_c === "8" || $bean->estado_c === "9")
				// && $arguments['link'] == "orden_ordenes_orden_ordenes_1"
    ){
			$ordenPadre = BeanFactory::getBean('orden_Ordenes', $arguments['related_id']);
			$ordenPadre->retrieve();

			if($bean->estado_c === "4"){
					$ordenPadre->por_facturar_c -= $bean->amount_c;
					$ordenPadre->save(false);
			}

			$account = BeanFactory::getBean('Accounts', $ordenPadre->accounts_orden_ordenes_1accounts_ida);
			$account->load_relationship('accounts_orden_ordenes_1');
			$account->accounts_orden_ordenes_1->add($bean);


		}

		if(
				$arguments['module'] == "orden_Ordenes"
      	&& $arguments['related_module'] == "LF_Facturas"
    ){
			$factura = BeanFactory::getBean('LF_Facturas', $arguments['related_id']);
			$bean->retrieve();
			$account = BeanFactory::getBean('Accounts',$bean->accounts_orden_ordenes_1accounts_ida);
			if(!empty($account->id)){
				$account->load_relationship('accounts_lf_facturas_1');
			  $account->accounts_lf_facturas_1->add($factura);
			}
		}

		if(
		   $arguments['module'] == "orden_Ordenes"
        && $arguments['related_module'] == "Prospects"
        && ($bean->estado_c === "0" || (empty($bean->estado_c)))
				&& $bean->deleted === 0
    ){
			$prospecto = BeanFactory::getBean('Prospects', $arguments['related_id']);
			if($prospecto->id && !$prospecto->tieneorden_c){
				$prospecto->tieneorden_c = true;
				$prospecto->save(false);
			}
		}

	}

	private function validaAplicaDescuento($account) {
		if(!empty($account->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida)) {
			$grupoEmpresas = BeanFactory::getBean('GPE_Grupo_empresas', $account->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida);
			if($grupoEmpresas->administrar_credito_grupal)
				return false;
			else
				return true;
		}
		return true;
	}

	public function desvincularOrdenes($bean, $event, $arguments)
	{
		if(
			$arguments['related_module'] === 'Accounts'
		){
			$account = BeanFactory::getBean('Accounts', $arguments['related_id']);
			if($account->id){
				if((empty($bean->estado_c) || $bean->estado_c === "0") && !empty($bean->id_customer_c)){
					$account->credito_disponible_c += $bean->amount_c;
				}
				if(($bean->estado_c === "6" || $bean->estado_c === "8" || $bean->estado_c === "9") && !empty($bean->id_customer_c)){
					$amount = $bean->estado_c === "6" ? $bean->amount_c : $bean->sub_total_c;
					$account->credito_disponible_c += $amount;

				}
				if($bean->estado_c === "4" && !empty($bean->id_customer_c)){
					$account->credito_disponible_c -= $bean->amount_c;
				}
				$account->save(false);
			}
		}

		if(
			$arguments['related_module'] === 'orden_Ordenes'
		){
			$ordenPadre = BeanFactory::getBean('orden_Ordenes', $arguments['related_id']);
			if(
				$bean->estado_c === "8" || $bean->estado_c === "9" && !empty($bean->id_customer_c)
			){
				$ordenPadre->por_entregar_c += $bean->amount_c;
				$ordenPadre->save(false);
			}
		}
		return true;
	}

	public function processAutorizacionEspecial($account, $orden, $buscaAEOtro = false)
	{
		global $current_user;
		require_once "custom/modules/Accounts/clients/base/api/ValidateApi.php";
		$validateApi = new ValidateApi();
		$autorizacion = $validateApi->getAutorizacionEspecial($account->id, null);

		if($buscaAEOtro){
			if($autorizacion){
				if($autorizacion->motivo_solicitud_c === 'otro' || $autorizacion->motivo_solicitud_c === 'promesa_de_pago'){
					return $autorizacion;
				} else {
					return null;
				}
			} else {
				return null;
			}
		}

		if($autorizacion){
			if($autorizacion->motivo_solicitud_c === 'credito_disponible_temporal'
			|| $autorizacion->motivo_solicitud_c === 'otro'){
				$autorizacion->estado_c = "aplicado";
				$autorizacion->numero_veces_aplicacion_c += 1;
			}
			$autorizacion->saldo_pendiente_aplicar_c -= $orden->amount_c > $autorizacion->saldo_pendiente_aplicar_c ? $autorizacion->saldo_pendiente_aplicar_c : $orden->amount_c;

			if(
				intval($autorizacion->saldo_pendiente_aplicar_c) === 0
				|| ($autorizacion->estado_c === "aplicado" && $autorizacion->aplica_por_unica_ocacion_c)
			){
				$autorizacion->estado_autorizacion_c = 'finalizada';
			}

			$autorizacion->save(false);
			$autorizacion->load_relationship('lowae_autorizacion_especial_orden_ordenes_1');
			$autorizacion->lowae_autorizacion_especial_orden_ordenes_1->add($orden);
		}
		return true;
	}

	public function devuelveAutorizacion($orden, $amount){
		$orden->retrieve();
		if($orden->orden_ordenes_orden_ordenes_1orden_ordenes_ida){
			$ordenPadre = BeanFactory::getBean("orden_Ordenes", $orden->orden_ordenes_orden_ordenes_1orden_ordenes_ida);
			if($ordenPadre->id && $ordenPadre->lowae_auto0644special_ida){
				$autorizacion = BeanFactory::getBean('lowae_autorizacion_especial', $ordenPadre->lowae_auto0644special_ida);
				if($autorizacion->id){
					if(
						$autorizacion->motivo_solicitud_c === 'promesa_de_pago'
						&& ($autorizacion->estado_autorizacion_c === 'autorizada' || $autorizacion->estado_autorizacion_c === 'finalizada')
					){
						// $autorizacion->saldo_pendiente_aplicar_c += $amount;
						$autorizacion->estado_autorizacion_c = 'cancelada_devolucion_cancelacion';
					}

					if(($autorizacion->motivo_solicitud_c === 'credito_disponible_temporal' || $autorizacion->motivo_solicitud_c === 'otro')
						&& $autorizacion->estado_autorizacion_c === 'autorizada'
					){
						$autorizacion->estado_autorizacion_c = 'finalizada';
					}
					$autorizacion->save(false);
				}
			}
		}
	}


}
?>
