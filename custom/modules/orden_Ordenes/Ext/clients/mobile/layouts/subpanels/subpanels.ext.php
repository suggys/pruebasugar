<?php
// WARNING: The contents of this file are auto-generated.


// created: 2016-10-19 00:31:50
$viewdefs['orden_Ordenes']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE',
  'context' => 
  array (
    'link' => 'lf_facturas_orden_ordenes',
  ),
);

// created: 2017-07-16 19:22:10
$viewdefs['orden_Ordenes']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_LOW02_PARTIDAS_ORDEN_TITLE',
  'context' => 
  array (
    'link' => 'low02_partidas_orden_orden_ordenes',
  ),
);

// created: 2016-10-11 21:43:30
$viewdefs['orden_Ordenes']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE',
  'context' => 
  array (
    'link' => 'orden_ordenes_orden_ordenes_1',
  ),
);