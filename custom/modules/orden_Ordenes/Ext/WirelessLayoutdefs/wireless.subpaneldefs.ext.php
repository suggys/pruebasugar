<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/WirelessLayoutdefs/lf_facturas_orden_ordenes_orden_Ordenes.php

 // created: 2016-10-19 00:31:50
$layout_defs["orden_Ordenes"]["subpanel_setup"]['lf_facturas_orden_ordenes'] = array (
  'order' => 100,
  'module' => 'LF_Facturas',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE',
  'get_subpanel_data' => 'lf_facturas_orden_ordenes',
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/WirelessLayoutdefs/orden_ordenes_orden_ordenes_1_orden_Ordenes.php

 // created: 2016-10-11 21:43:30
$layout_defs["orden_Ordenes"]["subpanel_setup"]['orden_ordenes_orden_ordenes_1'] = array (
  'order' => 100,
  'module' => 'orden_Ordenes',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE',
  'get_subpanel_data' => 'orden_ordenes_orden_ordenes_1',
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/WirelessLayoutdefs/low02_partidas_orden_orden_ordenes_orden_Ordenes.php

 // created: 2017-07-16 19:22:10
$layout_defs["orden_Ordenes"]["subpanel_setup"]['low02_partidas_orden_orden_ordenes'] = array (
  'order' => 100,
  'module' => 'LOW02_Partidas_Orden',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_LOW02_PARTIDAS_ORDEN_TITLE',
  'get_subpanel_data' => 'low02_partidas_orden_orden_ordenes',
);

?>
