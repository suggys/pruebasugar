<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Layoutdefs/lf_facturas_orden_ordenes_orden_Ordenes.php

 // created: 2016-10-19 00:31:50
$layout_defs["orden_Ordenes"]["subpanel_setup"]['lf_facturas_orden_ordenes'] = array (
  'order' => 100,
  'module' => 'LF_Facturas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE',
  'get_subpanel_data' => 'lf_facturas_orden_ordenes',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Layoutdefs/orden_ordenes_orden_ordenes_1_orden_Ordenes.php

 // created: 2016-10-11 21:43:30
$layout_defs["orden_Ordenes"]["subpanel_setup"]['orden_ordenes_orden_ordenes_1'] = array (
  'order' => 100,
  'module' => 'orden_Ordenes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE',
  'get_subpanel_data' => 'orden_ordenes_orden_ordenes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Layoutdefs/low02_partidas_orden_orden_ordenes_orden_Ordenes.php

 // created: 2017-07-16 19:22:10
$layout_defs["orden_Ordenes"]["subpanel_setup"]['low02_partidas_orden_orden_ordenes'] = array (
  'order' => 100,
  'module' => 'LOW02_Partidas_Orden',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_LOW02_PARTIDAS_ORDEN_TITLE',
  'get_subpanel_data' => 'low02_partidas_orden_orden_ordenes',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Layoutdefs/_overrideorden_Ordenes_subpanel_orden_ordenes_orden_ordenes_1.php

//auto-generated file DO NOT EDIT
$layout_defs['orden_Ordenes']['subpanel_setup']['orden_ordenes_orden_ordenes_1']['override_subpanel_name'] = 'orden_Ordenes_subpanel_orden_ordenes_orden_ordenes_1';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Layoutdefs/_overrideorden_Ordenes_subpanel_lf_facturas_orden_ordenes.php

//auto-generated file DO NOT EDIT
$layout_defs['orden_Ordenes']['subpanel_setup']['lf_facturas_orden_ordenes']['override_subpanel_name'] = 'orden_Ordenes_subpanel_lf_facturas_orden_ordenes';

?>
