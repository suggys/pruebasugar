<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.customaccounts_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Clientes Crédito AR';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_facturas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
$mod_strings['LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE'] = 'Facturas / Notas de Cargo';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.CF_monto_aux_c.php


$mod_strings['LBL_MONTO_AUX_C'] = 'Monto Orden Aux.';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.CF_Ordenes_20170802.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_NAME'] = 'Número de Orden (ID Transacción)';
$mod_strings['LBL_NO_TICKET'] = 'Número de Ticket';
$mod_strings['LBL_ESTADO_C'] = 'Estado de la Orden ';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_AMOUNT'] = 'Importe Total';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_SUB_TOTAL_C'] = 'Subtotal';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_IVA_C'] = 'IVA ';
$mod_strings['LBL_FECHA_TRANSACCION_C'] = 'Fecha de Transacción';
$mod_strings['LBL_CAJA_ID_C'] = 'caja id c';
$mod_strings['LBL_CAJERO_ID_C'] = 'cajero id c';
$mod_strings['LBL_TIENDA_ID_C'] = 'tienda id c';
$mod_strings['LBL_TRANSACTIONTYPECODE_C'] = 'Transaction Type Code';
$mod_strings['LBL_ID_CAJA_C'] = 'Número de caja';
$mod_strings['LBL_ID_CAJERO_C'] = 'Número de cajero';
$mod_strings['LBL_ID_SUCURSAL_C'] = 'Número de Sucursal';
$mod_strings['LBL_POR_ENTREGAR_C'] = 'Por entregar';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Información de Sistema';
$mod_strings['LBL_FOLIO_ORDEN_INICIAL_C'] = 'Número de Orden Padre';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_ID_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_ID_CAJA'] = 'Número de Caja';
$mod_strings['LBL_ID_CAJERO'] = 'Número de Cajero';
$mod_strings['LBL_POR_ENTREGAR'] = 'Por Entregar';
$mod_strings['LBL_ENTREGADA_POR_COMPLETO'] = 'entregada por completo';
$mod_strings['LBL_ES_ORDEN_INICIAL'] = 'es orden inicial';
$mod_strings['LBL_FOLIO_C'] = 'Número de Orden';
$mod_strings['LBL_ORDEN_COMPRA_C'] = 'Orden de Compra del Cliente';
$mod_strings['LBL_ID_CUSTOMER_C'] = 'ID Customer';
$mod_strings['LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE'] = 'Facturas / Notas de Cargo';
$mod_strings['LBL_IMPORTADO'] = 'Registro Importado';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_L_TITLE'] = 'Orden Padre';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Nota de Crédito';
$mod_strings['LBL_IMPORTADO_C'] = 'Importado';
$mod_strings['LBL_POR_FACTURAR'] = 'Por Facturar';
$mod_strings['LBL_MONTO_AUX_C'] = 'Monto Orden Aux.';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_DELETE_REENTRADA_BUTTON_LABEL'] = 'Eliminar Reentrada';
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.customprospects_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_L_TITLE'] = 'Orden Padre';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE'] = 'Ordenes';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE_ID'] = 'Ordenes ID';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID'] = 'Autorización Especial ID';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_PROSPECTS_TITLE'] = 'Prospectos';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID'] = 'Prospectos ID';
$mod_strings['LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Prospectos';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_20171007.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_FRR.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_Modules_CRM.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
$mod_strings['LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_LOW02_PARTIDAS_ORDEN_TITLE'] = 'Partidas de Orden';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_20170907.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_20170922.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_20170828.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Notas_credito.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Nota de Crédito';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE_ID'] = 'NC_Notas_credito ID';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Notas de crédito';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_20171012.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_20170817.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_20171020.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Partidas_Ordenes.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
$mod_strings['LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_LOW02_PARTIDAS_ORDEN_TITLE'] = 'Partidas de Orden';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_20171010.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_201710130100.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_20171009.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.customlowae_autorizacion_especial_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_L_TITLE'] = 'Orden Padre';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE'] = 'Ordenes';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE_ID'] = 'Ordenes ID';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID'] = 'Autorización Especial ID';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Autorización Especial';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.customorden_ordenes_orden_ordenes_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID'] = 'Clientes Crédito AR ID';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_L_TITLE'] = 'Orden Padre';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE'] = 'Órdenes';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE_ID'] = 'Ordenes ID';

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Language/es_LA.Lowes_CRM_20170831.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_POS'] = 'ID POS';

?>
