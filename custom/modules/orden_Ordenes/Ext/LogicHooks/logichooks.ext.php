<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/LogicHooks/CalcularCreditoDisponibleDeClienteCreditoAR.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
    2,
    'Calcular el crédito disponible de la cuenta Cliente Crédito AR',
    'custom/modules/orden_Ordenes/CalcularCreditoDisponibleDeClienteCreditoAR.php',
    'CalcularCreditoDisponibleDeClienteCreditoAR',
    'CalcularCreditoDisponibleDeClienteCreditoAR'
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/LogicHooks/EstableceMontoPorEntregarDeOrden.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
	1, 
	'Establece monto por entrega de una Orden Iniciada o Completada', 
	'custom/modules/orden_Ordenes/EstableceMontoPorEntregarDeOrden.php',
    'EstableceMontoPorEntregarDeOrden',
    'estableceMontoPorEntregar'
);

$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
	3, 
	'After save', 
	'custom/modules/orden_Ordenes/EstableceMontoPorEntregarDeOrden.php',
    'EstableceMontoPorEntregarDeOrden',
    'afterSave'
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/LogicHooks/VinculacionAutomaticaDeOrdenes.php

    $hook_version = 1;
    $hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        1,
        'FR-20.2. Vinculación de órdenes automática',
        'custom/modules/orden_Ordenes/VinculacionAutomaticaDeOrdenes.php',
        'VinculacionAutomaticaDeOrdenes',
        'vinculaAutomaticaDeOrdenes'
    );

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/LogicHooks/VincularyDesvincularOrdenes.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    1,
    'Vincular una Orden',
    'custom/modules/orden_Ordenes/VincularyDesvincularOrdenes.php',
    'VincularyDesvincularOrdenes',
    'vincularOrdenes'
);
$hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
$hook_array['after_relationship_delete'][] = array(
    1,
    'Desvincular una Orden',
    'custom/modules/orden_Ordenes/VincularyDesvincularOrdenes.php',
    'VincularyDesvincularOrdenes',
    'desvincularOrdenes'
);

?>
