<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/accounts_orden_ordenes_1_orden_Ordenes.php

// created: 2016-10-11 21:41:46
$dictionary["orden_Ordenes"]["fields"]["accounts_orden_ordenes_1"] = array (
  'name' => 'accounts_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'accounts_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'accounts_orden_ordenes_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["orden_Ordenes"]["fields"]["accounts_orden_ordenes_1_name"] = array (
  'name' => 'accounts_orden_ordenes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_orden_ordenes_1accounts_ida',
  'link' => 'accounts_orden_ordenes_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["orden_Ordenes"]["fields"]["accounts_orden_ordenes_1accounts_ida"] = array (
  'name' => 'accounts_orden_ordenes_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID',
  'id_name' => 'accounts_orden_ordenes_1accounts_ida',
  'link' => 'accounts_orden_ordenes_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_importado_c.php

 // created: 2016-12-13 17:27:27
$dictionary['orden_Ordenes']['fields']['importado_c']['labelValue']='Registro Importado';
$dictionary['orden_Ordenes']['fields']['importado_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['importado_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_id_cajero_c.php

 // created: 2016-10-17 10:05:10
$dictionary['orden_Ordenes']['fields']['id_cajero_c']['labelValue']='Número de Cajero';
$dictionary['orden_Ordenes']['fields']['id_cajero_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['orden_Ordenes']['fields']['id_cajero_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['id_cajero_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_fecha_transaccion_c.php

 // created: 2016-10-17 10:08:02
$dictionary['orden_Ordenes']['fields']['fecha_transaccion_c']['labelValue']='Fecha de Transacción';
$dictionary['orden_Ordenes']['fields']['fecha_transaccion_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['fecha_transaccion_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_entregada_por_completo_c.php

 // created: 2016-10-18 19:00:56
$dictionary['orden_Ordenes']['fields']['entregada_por_completo_c']['labelValue']='entregada por completo';
$dictionary['orden_Ordenes']['fields']['entregada_por_completo_c']['enforced']='false';
$dictionary['orden_Ordenes']['fields']['entregada_por_completo_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_amount_c.php

 // created: 2016-12-26 18:50:43
$dictionary['orden_Ordenes']['fields']['amount_c']['labelValue']='Importe Total';
$dictionary['orden_Ordenes']['fields']['amount_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['amount_c']['dependency']='';
$dictionary['orden_Ordenes']['fields']['amount_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_currency_id.php

 // created: 2016-10-11 21:57:26

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_id_caja_c.php

 // created: 2016-10-17 10:04:32
$dictionary['orden_Ordenes']['fields']['id_caja_c']['labelValue']='Número de Caja';
$dictionary['orden_Ordenes']['fields']['id_caja_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['orden_Ordenes']['fields']['id_caja_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['id_caja_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_id_sucursal_c.php

 // created: 2016-10-17 10:03:53
$dictionary['orden_Ordenes']['fields']['id_sucursal_c']['labelValue']='Sucursal';
$dictionary['orden_Ordenes']['fields']['id_sucursal_c']['dependency']='';
$dictionary['orden_Ordenes']['fields']['id_sucursal_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_es_orden_inicial_c.php

 // created: 2016-10-18 21:23:29
$dictionary['orden_Ordenes']['fields']['es_orden_inicial_c']['labelValue']='es orden inicial';
$dictionary['orden_Ordenes']['fields']['es_orden_inicial_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['es_orden_inicial_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_base_rate.php

 // created: 2016-10-17 09:59:01

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/lf_facturas_orden_ordenes_orden_Ordenes.php

// created: 2016-10-19 00:31:50
$dictionary["orden_Ordenes"]["fields"]["lf_facturas_orden_ordenes"] = array (
  'name' => 'lf_facturas_orden_ordenes',
  'type' => 'link',
  'relationship' => 'lf_facturas_orden_ordenes',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/nc_notas_credito_orden_ordenes_orden_Ordenes.php

// created: 2016-10-12 00:13:37
$dictionary["orden_Ordenes"]["fields"]["nc_notas_credito_orden_ordenes"] = array (
  'name' => 'nc_notas_credito_orden_ordenes',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_orden_ordenes',
  'source' => 'non-db',
  'module' => 'NC_Notas_credito',
  'bean_name' => false,
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE',
  'id_name' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
);
$dictionary["orden_Ordenes"]["fields"]["nc_notas_credito_orden_ordenes_name"] = array (
  'name' => 'nc_notas_credito_orden_ordenes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE',
  'save' => true,
  'id_name' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
  'link' => 'nc_notas_credito_orden_ordenes',
  'table' => 'nc_notas_credito',
  'module' => 'NC_Notas_credito',
  'rname' => 'name',
);
$dictionary["orden_Ordenes"]["fields"]["nc_notas_credito_orden_ordenesnc_notas_credito_ida"] = array (
  'name' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE_ID',
  'id_name' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
  'link' => 'nc_notas_credito_orden_ordenes',
  'table' => 'nc_notas_credito',
  'module' => 'NC_Notas_credito',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/orden_ordenes_orden_ordenes_1_orden_Ordenes.php

// created: 2016-10-11 21:43:30
$dictionary["orden_Ordenes"]["fields"]["orden_ordenes_orden_ordenes_1"] = array (
  'name' => 'orden_ordenes_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'orden_ordenes_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'vname' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_L_TITLE',
  'id_name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["orden_Ordenes"]["fields"]["orden_ordenes_orden_ordenes_1_right"] = array (
  'name' => 'orden_ordenes_orden_ordenes_1_right',
  'type' => 'link',
  'relationship' => 'orden_ordenes_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'side' => 'right',
  'vname' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE',
  'id_name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
  'link-type' => 'one',
);
$dictionary["orden_Ordenes"]["fields"]["orden_ordenes_orden_ordenes_1_name"] = array (
  'name' => 'orden_ordenes_orden_ordenes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_L_TITLE',
  'save' => true,
  'id_name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
  'link' => 'orden_ordenes_orden_ordenes_1_right',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'name',
);
$dictionary["orden_Ordenes"]["fields"]["orden_ordenes_orden_ordenes_1orden_ordenes_ida"] = array (
  'name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE_ID',
  'id_name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
  'link' => 'orden_ordenes_orden_ordenes_1_right',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_sub_total_c.php

 // created: 2016-12-13 23:48:55
$dictionary['orden_Ordenes']['fields']['sub_total_c']['labelValue']='Subtotal';
$dictionary['orden_Ordenes']['fields']['sub_total_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['sub_total_c']['dependency']='';
$dictionary['orden_Ordenes']['fields']['sub_total_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_por_entregar_c.php

 // created: 2016-10-17 10:05:46
$dictionary['orden_Ordenes']['fields']['por_entregar_c']['labelValue']='Por Entregar';
$dictionary['orden_Ordenes']['fields']['por_entregar_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['por_entregar_c']['dependency']='';
$dictionary['orden_Ordenes']['fields']['por_entregar_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_no_ticket_c.php

 // created: 2016-10-17 17:44:17
$dictionary['orden_Ordenes']['fields']['no_ticket_c']['labelValue']='Número de Ticket';
$dictionary['orden_Ordenes']['fields']['no_ticket_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['orden_Ordenes']['fields']['no_ticket_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['no_ticket_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_iva_c.php

 // created: 2016-10-11 22:05:25
$dictionary['orden_Ordenes']['fields']['iva_c']['labelValue']='IVA :';
$dictionary['orden_Ordenes']['fields']['iva_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['iva_c']['dependency']='';
$dictionary['orden_Ordenes']['fields']['iva_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_name.php

 // created: 2016-12-13 17:23:33
$dictionary['orden_Ordenes']['fields']['name']['unified_search']=false;
$dictionary['orden_Ordenes']['fields']['name']['duplicate_merge']='enabled';
$dictionary['orden_Ordenes']['fields']['name']['duplicate_merge_dom_value']='3';
$dictionary['orden_Ordenes']['fields']['name']['merge_filter']='selected';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/low02_partidas_orden_orden_ordenes_orden_Ordenes.php

// created: 2017-07-16 19:22:10
$dictionary["orden_Ordenes"]["fields"]["low02_partidas_orden_orden_ordenes"] = array (
  'name' => 'low02_partidas_orden_orden_ordenes',
  'type' => 'link',
  'relationship' => 'low02_partidas_orden_orden_ordenes',
  'source' => 'non-db',
  'module' => 'LOW02_Partidas_Orden',
  'bean_name' => false,
  'vname' => 'LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'low02_partidas_orden_orden_ordenesorden_ordenes_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/OrdenesVisibility.php

$dictionary['orden_Ordenes']['visibility']["OrdenesVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/lowae_autorizacion_especial_orden_ordenes_1_orden_Ordenes.php

// created: 2017-02-23 18:24:39
$dictionary["orden_Ordenes"]["fields"]["lowae_autorizacion_especial_orden_ordenes_1"] = array (
  'name' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'lowae_autorizacion_especial',
  'bean_name' => 'lowae_autorizacion_especial',
  'side' => 'right',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'lowae_auto0644special_ida',
  'link-type' => 'one',
);
$dictionary["orden_Ordenes"]["fields"]["lowae_autorizacion_especial_orden_ordenes_1_name"] = array (
  'name' => 'lowae_autorizacion_especial_orden_ordenes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'save' => true,
  'id_name' => 'lowae_auto0644special_ida',
  'link' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'table' => 'lowae_autorizacion_especial',
  'module' => 'lowae_autorizacion_especial',
  'rname' => 'name',
);
$dictionary["orden_Ordenes"]["fields"]["lowae_auto0644special_ida"] = array (
  'name' => 'lowae_auto0644special_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID',
  'id_name' => 'lowae_auto0644special_ida',
  'link' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'table' => 'lowae_autorizacion_especial',
  'module' => 'lowae_autorizacion_especial',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/full_text_search_admin.php

 // created: 2017-02-24 18:28:04
$dictionary['orden_Ordenes']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/prospects_orden_ordenes_1_orden_Ordenes.php

// created: 2017-07-23 02:41:44
$dictionary["orden_Ordenes"]["fields"]["prospects_orden_ordenes_1"] = array (
  'name' => 'prospects_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'prospects_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'Prospects',
  'bean_name' => 'Prospect',
  'side' => 'right',
  'vname' => 'LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'prospects_orden_ordenes_1prospects_ida',
  'link-type' => 'one',
);
$dictionary["orden_Ordenes"]["fields"]["prospects_orden_ordenes_1_name"] = array (
  'name' => 'prospects_orden_ordenes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_PROSPECTS_TITLE',
  'save' => true,
  'id_name' => 'prospects_orden_ordenes_1prospects_ida',
  'link' => 'prospects_orden_ordenes_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'name',
);
$dictionary["orden_Ordenes"]["fields"]["prospects_orden_ordenes_1prospects_ida"] = array (
  'name' => 'prospects_orden_ordenes_1prospects_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID',
  'id_name' => 'prospects_orden_ordenes_1prospects_ida',
  'link' => 'prospects_orden_ordenes_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_estado_c.php

 // created: 2016-10-17 09:58:27
$dictionary['orden_Ordenes']['fields']['estado_c']['labelValue']='Estado de la Orden ';
$dictionary['orden_Ordenes']['fields']['estado_c']['dependency']='';
$dictionary['orden_Ordenes']['fields']['estado_c']['visibility_grid']='';
$dictionary['orden_Ordenes']['fields']['estado_c']['default']='';
 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_folio_c.php

 // created: 2017-01-18 23:04:47
$dictionary['orden_Ordenes']['fields']['folio_c']['labelValue']='Número de Orden';
$dictionary['orden_Ordenes']['fields']['folio_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['orden_Ordenes']['fields']['folio_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['folio_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_monto_aux_c.php

 // created: 2017-02-10 00:35:48
$dictionary['orden_Ordenes']['fields']['monto_aux_c']['duplicate_merge_dom_value']=0;
$dictionary['orden_Ordenes']['fields']['monto_aux_c']['labelValue']='Monto Orden Aux.';
$dictionary['orden_Ordenes']['fields']['monto_aux_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['monto_aux_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_importadode_c.php

 // created: 2017-10-09 21:49:04
$dictionary['orden_Ordenes']['fields']['importadode_c']['labelValue']='Archivo de origen';
$dictionary['orden_Ordenes']['fields']['importadode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['orden_Ordenes']['fields']['importadode_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['importadode_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_id_pos_c.php

 // created: 2017-08-23 18:56:05
$dictionary['orden_Ordenes']['fields']['id_pos_c']['duplicate_merge_dom_value']=0;
$dictionary['orden_Ordenes']['fields']['id_pos_c']['labelValue']='ID POS';
$dictionary['orden_Ordenes']['fields']['id_pos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['orden_Ordenes']['fields']['id_pos_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['id_pos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_id_customer_c.php

 // created: 2017-01-18 23:05:46
$dictionary['orden_Ordenes']['fields']['id_customer_c']['labelValue']='ID Customer';
$dictionary['orden_Ordenes']['fields']['id_customer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['orden_Ordenes']['fields']['id_customer_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['id_customer_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_orden_compra_c.php

 // created: 2017-01-18 23:06:32
$dictionary['orden_Ordenes']['fields']['orden_compra_c']['labelValue']='Orden de Compra del Cliente';
$dictionary['orden_Ordenes']['fields']['orden_compra_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['orden_Ordenes']['fields']['orden_compra_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['orden_compra_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_transactiontypecode_c.php

 // created: 2017-01-18 23:07:14
$dictionary['orden_Ordenes']['fields']['transactiontypecode_c']['labelValue']='Transaction Type Code';
$dictionary['orden_Ordenes']['fields']['transactiontypecode_c']['dependency']='';
$dictionary['orden_Ordenes']['fields']['transactiontypecode_c']['visibility_grid']='';

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_por_facturar_c.php

 // created: 2017-02-03 23:00:27
$dictionary['orden_Ordenes']['fields']['por_facturar_c']['labelValue']='Por Facturar';
$dictionary['orden_Ordenes']['fields']['por_facturar_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['por_facturar_c']['dependency']='';
$dictionary['orden_Ordenes']['fields']['por_facturar_c']['default']='0';
$dictionary['orden_Ordenes']['fields']['por_facturar_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/orden_Ordenes/Ext/Vardefs/sugarfield_folio_orden_inicial_c.php

 // created: 2017-01-18 23:05:06
$dictionary['orden_Ordenes']['fields']['folio_orden_inicial_c']['labelValue']='Número de Orden Padre';
$dictionary['orden_Ordenes']['fields']['folio_orden_inicial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['orden_Ordenes']['fields']['folio_orden_inicial_c']['enforced']='';
$dictionary['orden_Ordenes']['fields']['folio_orden_inicial_c']['dependency']='';

 
?>
