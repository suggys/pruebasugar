<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class EstableceOrdenAEntregaPorCompleto
{
	protected static $fetchedRow = array();

	public function estableceAEntregaPorCompleto($bean, $event, $arguments)
	{
		if ( !empty($bean->id) ) {
            self::$fetchedRow[$bean->id] = $bean->fetched_row;
        }

		if (
			!$arguments['isUpdate']
			&& $bean->estado_c === ""
			&& $bean->entregada_por_completo_c == false  //LOW 178
			&& $bean->es_orden_inicial_c == false // LOW 178
			)
		{
			$bean->entregada_por_completo_c = true;
			$bean->por_entregar_c = 0;
		}

	}
}
