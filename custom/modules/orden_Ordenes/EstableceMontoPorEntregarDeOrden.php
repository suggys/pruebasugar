<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class EstableceMontoPorEntregarDeOrden{
	protected static $fetchedRow = array();
	public function estableceMontoPorEntregar($bean, $event, $arguments){
		if(isset($bean->importado_c) && $bean->importado_c){
			return true;
		}
		if ( !empty($bean->id) ) {
      self::$fetchedRow[$bean->id] = $bean->fetched_row;
    }
		if(!$arguments['isUpdate']){
			if($bean->sub_total_c < 0){
				// $GLOBALS["log"]->fatal("estado:".$bean->estado_c);
				$estado = "";
				switch ($bean->estado_c) {
					case 2:
					case '2':
					case 8:
					case '8':
					$estado = "8";
					break;
					case 5:
					case '5':
					case 9:
					case '9':
					$estado = "9";
					break;
					default:
					$estado = $bean->estado_c;
					break;
				}
				$bean->estado_c = $estado;
			}
		}

		if (
			!$arguments['isUpdate']
			&& ($bean->estado_c === "0" || $bean->estado_c === "5" || $bean->estado_c === "")
			&& empty($bean->folio_orden_inicial_c)
			)
		{
			switch ($bean->estado_c) {
				case '0':
					$bean->es_orden_inicial_c = true;
					$bean->por_entregar_c = $bean->amount_c;
					$bean->por_facturar_c = $bean->amount_c;
					break;
				case '':
					$bean->por_facturar_c = $bean->amount_c;
					break;
				case '5' :
					$bean->entregada_por_completo_c = true;
					$bean->por_entregar_c = 0;
					break;
				default:
					break;
			}
		}

		if($bean->fetched_row['estado_c'] !== $bean->estado_c
		&& (
			$bean->estado_c === '4' || $bean->estado_c === '6' || $bean->estado_c === '7' || $bean->estado_c === '8' || $bean->estado_c === '9'
			)){
				$bean->monto_aux_c = -1 * abs($bean->amount_c);
			  }
			  else {
				$bean->monto_aux_c = abs($bean->amount_c);
			  }



	}

	public function afterSave($bean, $event, $arguments){
    if(isset($bean->importado_c) && $bean->importado_c){
			return true;
		}
		// $GLOBALS["log"]->fatal("afterSave:". $bean->estado_c);
		if(
			!empty($bean->id_customer_c)
			&& (
				empty(self::$fetchedRow[$bean->id])
				|| empty(self::$fetchedRow[$bean->id]['id_customer_c'])
			)
			&& ($bean->estado_c != "4" && $bean->estado_c != "6" && $bean->estado_c != "8" && $bean->estado_c != "9")
		){
			$fieldIndex = substr($bean->id_customer_c, -4) < 7000 ? "id_ar_credit_c" : "id_linea_anticipo_c";
			$value = $bean->id_customer_c;
			if($fieldIndex && $value){
				$account = BeanFactory::newBean('Accounts');
				$retrieveFields = array();
				$retrieveFields[$fieldIndex] = $value;
				$account->retrieve_by_string_fields($retrieveFields);
				if($account->id && !$account->importado_c){
					$account->retrieve();
					$account->load_relationship('accounts_orden_ordenes_1');
					$account->accounts_orden_ordenes_1->add($bean);
				}
			}
		}



		if(
			self::$fetchedRow[$bean->id]['folio_orden_inicial_c'] != $bean->folio_orden_inicial_c
			&& $bean->folio_orden_inicial_c
			&& (
				$bean->estado_c === "5" || $bean->estado_c === "2" || $bean->estado_c === "4" || $bean->estado_c === "6"
				|| $bean->estado_c === "8" || $bean->estado_c === "9"
			)
		){
			$ordenInicial = BeanFactory::newBean('orden_Ordenes');
			if($bean->estado_c === "6"){
				$ordenInicial->retrieve_by_string_fields(array(
					'no_ticket_c' => $bean->folio_orden_inicial_c,
				));
			}
			else{
				$ordenInicial->retrieve_by_string_fields(array(
					'folio_c' => $bean->folio_orden_inicial_c,
					'estado_c' => '0'
				));
			}

			if ($ordenInicial->id){
				$ordenInicial->load_relationship('orden_ordenes_orden_ordenes_1');
				$ordenInicial->orden_ordenes_orden_ordenes_1->add($bean);
			}
		}

		if(
			!empty($bean->id_pos_c)
			&& (
				empty(self::$fetchedRow[$bean->id])
				|| empty(self::$fetchedRow[$bean->id]['id_pos_c'])
			)
		){
			$fieldIndex = "id_pos_c";
			$value = $bean->id_pos_c;
			if($fieldIndex && $value){
				$account = BeanFactory::newBean('Accounts');
				$retrieveFields = array();
				$retrieveFields[$fieldIndex] = $value;
				$account->retrieve_by_string_fields($retrieveFields);
				if($account->id){
					$account->retrieve();
					$account->load_relationship('accounts_orden_ordenes_1');
					$account->accounts_orden_ordenes_1->add($bean);
				}
			}
		}


	}
}
