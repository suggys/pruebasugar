<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/LOW02_Partidas_Orden/Ext/Vardefs/low02_partidas_orden_orden_ordenes_LOW02_Partidas_Orden.php

// created: 2017-07-16 19:22:10
$dictionary["LOW02_Partidas_Orden"]["fields"]["low02_partidas_orden_orden_ordenes"] = array (
  'name' => 'low02_partidas_orden_orden_ordenes',
  'type' => 'link',
  'relationship' => 'low02_partidas_orden_orden_ordenes',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'side' => 'right',
  'vname' => 'LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_LOW02_PARTIDAS_ORDEN_TITLE',
  'id_name' => 'low02_partidas_orden_orden_ordenesorden_ordenes_ida',
  'link-type' => 'one',
);
$dictionary["LOW02_Partidas_Orden"]["fields"]["low02_partidas_orden_orden_ordenes_name"] = array (
  'name' => 'low02_partidas_orden_orden_ordenes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'save' => true,
  'id_name' => 'low02_partidas_orden_orden_ordenesorden_ordenes_ida',
  'link' => 'low02_partidas_orden_orden_ordenes',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'name',
);
$dictionary["LOW02_Partidas_Orden"]["fields"]["low02_partidas_orden_orden_ordenesorden_ordenes_ida"] = array (
  'name' => 'low02_partidas_orden_orden_ordenesorden_ordenes_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_LOW02_PARTIDAS_ORDEN_TITLE_ID',
  'id_name' => 'low02_partidas_orden_orden_ordenesorden_ordenes_ida',
  'link' => 'low02_partidas_orden_orden_ordenes',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/LOW02_Partidas_Orden/Ext/Vardefs/sugarfield_name.php

 // created: 2017-07-18 00:29:44
$dictionary['LOW02_Partidas_Orden']['fields']['name']['len']='100';
$dictionary['LOW02_Partidas_Orden']['fields']['name']['audited']=false;
$dictionary['LOW02_Partidas_Orden']['fields']['name']['massupdate']=false;
$dictionary['LOW02_Partidas_Orden']['fields']['name']['unified_search']=false;
$dictionary['LOW02_Partidas_Orden']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['LOW02_Partidas_Orden']['fields']['name']['calculated']=false;

 
?>
