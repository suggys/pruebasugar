<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/rli_link_workflow.php

$dictionary['Call']['fields']['revenuelineitems']['workflow'] = true;
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/full_text_search_admin.php

 // created: 2016-09-15 19:00:08
$dictionary['Call']['full_text_search']=false;

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/CallsVisibility.php

$dictionary['Call']['visibility']["CallsVisibility"] = true;

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/low01_solicitudescredito_activities_calls_Calls.php

// created: 2017-07-21 12:47:08
$dictionary["Call"]["fields"]["low01_solicitudescredito_activities_calls"] = array (
  'name' => 'low01_solicitudescredito_activities_calls',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_calls',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_CALLS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
);

?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/sugarfield_description.php

 // created: 2018-01-25 15:06:58
$dictionary['Call']['fields']['description']['audited']=false;
$dictionary['Call']['fields']['description']['massupdate']=false;
$dictionary['Call']['fields']['description']['comments']='Full text of the note';
$dictionary['Call']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['description']['merge_filter']='disabled';
$dictionary['Call']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.54',
  'searchable' => true,
);
$dictionary['Call']['fields']['description']['calculated']=false;
$dictionary['Call']['fields']['description']['rows']='6';
$dictionary['Call']['fields']['description']['cols']='80';

 
?>
<?php
// Merged from custom/Extension/modules/Calls/Ext/Vardefs/dise_prospectos_cocina_calls_Calls.php

// created: 2018-02-01 19:07:34
$dictionary["Call"]["fields"]["dise_prospectos_cocina_calls"] = array (
  'name' => 'dise_prospectos_cocina_calls',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_calls',
  'source' => 'non-db',
  'module' => 'dise_Prospectos_cocina',
  'bean_name' => 'dise_Prospectos_cocina',
  'side' => 'right',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_CALLS_FROM_CALLS_TITLE',
  'id_name' => 'dise_prospectos_cocina_callsdise_prospectos_cocina_ida',
  'link-type' => 'one',
);
$dictionary["Call"]["fields"]["dise_prospectos_cocina_calls_name"] = array (
  'name' => 'dise_prospectos_cocina_calls_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_CALLS_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'save' => true,
  'id_name' => 'dise_prospectos_cocina_callsdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_calls',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'name',
);
$dictionary["Call"]["fields"]["dise_prospectos_cocina_callsdise_prospectos_cocina_ida"] = array (
  'name' => 'dise_prospectos_cocina_callsdise_prospectos_cocina_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_CALLS_FROM_CALLS_TITLE_ID',
  'id_name' => 'dise_prospectos_cocina_callsdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_calls',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
