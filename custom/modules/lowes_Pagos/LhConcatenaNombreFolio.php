<?php

class LhConcatenaNombreFolio{
    private $isDebug = false;

    protected static $fetchedRow = array();

    /**
     * Called as before_save logic hook to grab the fetched_row values
     */
    public function saveFetchedRow($bean, $event, $arguments)
    {
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        if ( !empty($bean->id) ) {
            if($this->isDebug)$GLOBALS['log']->fatal('saveFetchedRow ....');
            self::$fetchedRow[$bean->id] = $bean->fetched_row;

        }
        if($this->isDebug)$GLOBALS['log']->fatal('saveFetchedRow');
    }

    public function fnCambiaNombrePago($bean, $event, $arguments) {
        //if($this->isDebug)$GLOBALS['log']->fatal('fnCambiaNombrePago num_folio_c::'. print_r($bean->num_folio_c,1));
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
		if(!$arguments['isUpdate']) {
			//$bean->retrieve();
            $numFolio = $this->getFolio($bean->id);
			$new_name = $numFolio;

        	if($this->isDebug)$GLOBALS['log']->fatal('fnCambiaNombrePago new  name::'. $new_name);

            $bean->name = $new_name;
            
            $bean->save(false);
		}
    }

    public function getFolio($id)
    {
        if(isset($bean->importado_c) && $bean->importado_c){
                return true;
        }
        $sugarQuery = new SugarQuery();
        $sugarQuery->select(array('id', 'num_folio_c'));
        $bean = BeanFactory::newBean('lowes_Pagos');
        $sugarQuery->from($bean, array('team_security' => false));
        $sugarQuery->where()
            ->equals('id', $id);
        $result = $sugarQuery->execute();
        return $result[0]['num_folio_c'];
    }
}
