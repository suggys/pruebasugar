<?php
class LH_calcula_saldo_aplica_nota_cargo{
    public function fnCalculaSaldoxNCargo($bean, $event, $arguments) {
        global $db;
        if(isset($bean->importado_c) && $bean->importado_c){
            return true;
        }
        if($arguments['relationship'] === 'lowes_pagos_lf_facturas' && $arguments['related_module'] === 'LF_Facturas'){
            $Nota_Cargo = BeanFactory::getBean('LF_Facturas', $arguments['related_id']);
            $Nota_Cargo->retrieve();
            if($Nota_Cargo->id){
                $bean->saldo_aplicado_c += $Nota_Cargo->importe;
                $bean->saldo_pendiente_c = $bean->monto - $Nota_Cargo->importe;
                $bean->save(false);
            }
        }
    }
}

?>
