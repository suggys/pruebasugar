<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/LogicHooks/BloquePorChequeDevuelto.php

	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
    $hook_array['after_relationship_add'][] = array(
        1,
        'Bloque pago por cheque devuelto',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'bloqueaPorChequeDevuelto'
        );

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/LogicHooks/LHAsignaSaldosInicialesPago.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();

$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(1, 'LH asigna saldo a NC', 'custom/modules/lowes_Pagos/LHAsignaSaldosInicialesPago.php', 'LHAsignaSaldosInicialesPago','fnAsignaSaldosPago');


?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/LogicHooks/LiberaCreditoDisponible.php

    $hook_version = 1;
    $hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
    $hook_array['before_save'][] = array(
        1,
        'FR-27.5. Liberar Crédito Disponible',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'antesDeliberarCreditoDisponible'
    );
    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        1,
        'FR-27.5. Liberar Crédito Disponible',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'liberaCreditoDisponible'
    );

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/LogicHooks/DesaplicarNotaCargo.php

	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
    $hook_array['after_relationship_delete'][] = array(
        1,
        'Desaplicar nota de cargo',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'desaplicarNotaCargo'
    );

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/LogicHooks/lh_concatena_nombre_folio_pagos.php

$hook_version = 1; 
$hook_array = $hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
	'1',
	'Concatena nombre y folio al campo name',
	'custom/modules/lowes_Pagos/LhConcatenaNombreFolio.php',
	'LhConcatenaNombreFolio',
	'fnCambiaNombrePago'
	);
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
    $hook_array['before_save'][] = array(
        1,
        'save fetched row',
        'custom/modules/lowes_Pagos/LhConcatenaNombreFolio.php',
        'LhConcatenaNombreFolio',
        'saveFetchedRow'
    );

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/LogicHooks/AplicaPagoALineaAncipio.php

	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
    $hook_array['before_save'][] = array(
        1,
        'Aplica pago a linea de anticipo',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'aplicaPagoALineaAncipio',
    );

    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        1,
        'Calcula saldo disponible cliente',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'calculaSaldoDisponibleCuenta',
    );

    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        2,
        'Calcula saldo disponible cliente',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'calculaSaldoFavorCtaPorPagoAticipo',
    );

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/LogicHooks/LH_calcula_abono_promesa_alvincular.php

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    2,
    'Calcular abono promesa autorizacion especial,  al vincular cta Cliente Crédito AR',
    'custom/modules/lowes_Pagos/LH_calcula_abono_promesa_alvincular.php',
    'LH_calcula_abono_promesa_alvincular',
    'fnCalculaAbonoPromesa'
);

?>
