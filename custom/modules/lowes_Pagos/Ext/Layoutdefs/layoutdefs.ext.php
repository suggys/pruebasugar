<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Layoutdefs/lowes_pagos_lf_facturas_lowes_Pagos.php

 // created: 2016-10-22 15:12:51
$layout_defs["lowes_Pagos"]["subpanel_setup"]['lowes_pagos_lf_facturas'] = array (
  'order' => 100,
  'module' => 'LF_Facturas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE',
  'get_subpanel_data' => 'lowes_pagos_lf_facturas',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Layoutdefs/lowes_pagos_lf_facturas_1_lowes_Pagos.php

 // created: 2016-10-21 13:50:29
$layout_defs["lowes_Pagos"]["subpanel_setup"]['lowes_pagos_lf_facturas_1'] = array (
  'order' => 100,
  'module' => 'LF_Facturas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'get_subpanel_data' => 'lowes_pagos_lf_facturas_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Layoutdefs/lowae_autorizacion_especial_lowes_pagos_2_lowes_Pagos.php

 // created: 2017-11-06 23:18:19
$layout_defs["lowes_Pagos"]["subpanel_setup"]['lowae_autorizacion_especial_lowes_pagos_2'] = array (
  'order' => 100,
  'module' => 'lowae_autorizacion_especial',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'get_subpanel_data' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Layoutdefs/low01_pagos_facturas_lowes_pagos_lowes_Pagos.php

 // created: 2017-04-03 22:41:21
$layout_defs["lowes_Pagos"]["subpanel_setup"]['low01_pagos_facturas_lowes_pagos'] = array (
  'order' => 100,
  'module' => 'Low01_Pagos_Facturas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOW01_PAGOS_FACTURAS_TITLE',
  'get_subpanel_data' => 'low01_pagos_facturas_lowes_pagos',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Layoutdefs/_overridelowes_Pagos_subpanel_lowes_pagos_lf_facturas.php

//auto-generated file DO NOT EDIT
$layout_defs['lowes_Pagos']['subpanel_setup']['lowes_pagos_lf_facturas']['override_subpanel_name'] = 'lowes_Pagos_subpanel_lowes_pagos_lf_facturas';

?>
