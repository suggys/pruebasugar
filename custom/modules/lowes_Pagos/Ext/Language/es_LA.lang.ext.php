<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.customlowes_pagos_lf_facturas_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Cheque';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Nota de cargo';

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.lang.php

// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_ID_CUSTOMER_C'] = 'Id Customer';
$mod_strings['LBL_NUMERO_CUENTA_CLABE_C'] = 'Número de Cuenta Clabe Banamex';
$mod_strings['LBL_LINEA_ANTICIPO_C'] = 'Pago por Anticipo';
$mod_strings['LBL_LINEA_ANTICIPO'] = 'Pago por Anticipo';
$mod_strings['LBL_NAME'] = 'Folio';
$mod_strings['LBL_NUM_FOLIO_C'] = 'Número de Folio';
$mod_strings['LBL_CON_NOMBRE'] = 'con nombre';
$mod_strings['LBL_SALDO_APLICADO_C'] = 'Saldo Aplicado';
$mod_strings['LBL_SALDO_PENDIENTE_C'] = 'Saldo Pendiente por Aplicar';
$mod_strings['LBL_IMPORTADO'] = 'Registro Importado';
$mod_strings['LBL_ID_TRAN_POS_C'] = 'Número de Transacción POS';
$mod_strings['LBL_FORMA_PAGO'] = 'Forma de Pago';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Saldos';
$mod_strings['LBL_SHOW_MORE'] = 'Información de Sistema';
$mod_strings['LBL_DESCRIPTION'] = 'Comentarios';
$mod_strings['LBL_MONTO'] = 'Monto Pagado';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Nota de Cargo por Cheque Devuelto';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE'] = 'Aplicación de Facturas / Notas de Cargo';
$mod_strings['LBL_CHEQUE_DEVUELTO'] = 'Cheque devuelto';
$mod_strings['LBL_FECHA_TRANSACCION'] = 'Fecha de Pago';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_NUMERO_CUENTA_CLABE'] = 'Numero de Cuenta CLABE';
$mod_strings['LBL_PAGO_ANTICIPO'] = 'Pago por anticipo';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Infomacion de Sistema';
$mod_strings['LBL_FOLIO_EBS'] = 'Folio EBS';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Cliente';
$mod_strings['LBL_SUCURSAL_CLIENTE'] = 'Sucursal de Cliente';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_COMISION_FACTORAJE'] = 'Comisión de Factoraje';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_6'] = 'LBL_CURRENCY';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_20171007.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_FRR.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_20170907.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_20170922.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_20170828.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Pagos.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Cliente Crédito AR';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE_ID'] = 'Cliente Crédito AR ID';
$mod_strings['LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE'] = 'Cliente Crédito AR';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LOWES_PAGOS_TITLE'] = 'Pago';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE'] = 'Aplicación de Facturas / Notas de Cargo';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Cheque';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Nota de Cargo por Cheque Devuelto';

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.CF_lowes_Pagos_konesh.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_20171012.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.customlowae_autorizacion_especial_lowes_pagos_1.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.customlowae_autorizacion_especial_lowes_pagos_1.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Facturas';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE'] = 'Autorización Especial';

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Facturas';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE_ID'] = 'Autorización Especial ID';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE'] = 'Autorización Especial';


?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_20170817.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_20171020.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_20171010.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.customlowae_autorizacion_especial_lowes_pagos_2.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.customlowae_autorizacion_especial_lowes_pagos_2.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Facturas';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE_ID'] = 'Autorización Especial ID';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWES_PAGOS_TITLE'] = 'Autorización Especial';

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/temp.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE'] = 'Pagos';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Facturas';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE_ID'] = 'Autorización Especial ID';
// $mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWES_PAGOS_TITLE'] = 'Autorización Especial';


?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_201710130100.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_20171009.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Pagos_Facturas.php

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOW01_PAGOS_FACTURAS_TITLE'] = 'Pagos_Facturas';
$mod_strings['LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOWES_PAGOS_TITLE'] = 'Pagos_Facturas';

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Language/es_LA.Lowes_CRM_20170831.php

$mod_strings['LBL_ESTADO_KONESH'] = 'Estado konesh';
$mod_strings['LBL_FOLIO_KONESH'] = 'Folio konesh';
 
?>
