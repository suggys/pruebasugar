<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_con_nombre_c.php

 // created: 2016-11-01 15:45:06
$dictionary['lowes_Pagos']['fields']['con_nombre_c']['labelValue']='con nombre';
$dictionary['lowes_Pagos']['fields']['con_nombre_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['con_nombre_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/lowes_pagos_accounts_lowes_Pagos.php

// created: 2016-10-22 15:12:51
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_accounts"] = array (
  'name' => 'lowes_pagos_accounts',
  'type' => 'link',
  'relationship' => 'lowes_pagos_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'lowes_pagos_accountsaccounts_ida',
  'link-type' => 'one',
);
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_accounts_name"] = array (
  'name' => 'lowes_pagos_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'lowes_pagos_accountsaccounts_ida',
  'link' => 'lowes_pagos_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_accountsaccounts_ida"] = array (
  'name' => 'lowes_pagos_accountsaccounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_LOWES_PAGOS_TITLE_ID',
  'id_name' => 'lowes_pagos_accountsaccounts_ida',
  'link' => 'lowes_pagos_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/lowes_pagos_lf_facturas_lowes_Pagos.php

// created: 2016-10-22 15:12:51
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_lf_facturas"] = array (
  'name' => 'lowes_pagos_lf_facturas',
  'type' => 'link',
  'relationship' => 'lowes_pagos_lf_facturas',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'lowes_pagos_lf_facturaslowes_pagos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/lowes_pagos_lf_facturas_1_lowes_Pagos.php

// created: 2016-10-22 15:12:51
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_lf_facturas_1"] = array (
  'name' => 'lowes_pagos_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'lowes_pagos_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'lowes_pagos_lf_facturas_1lf_facturas_idb',
);
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_lf_facturas_1_name"] = array (
  'name' => 'lowes_pagos_lf_facturas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'save' => true,
  'id_name' => 'lowes_pagos_lf_facturas_1lf_facturas_idb',
  'link' => 'lowes_pagos_lf_facturas_1',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'name',
);
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_lf_facturas_1lf_facturas_idb"] = array (
  'name' => 'lowes_pagos_lf_facturas_1lf_facturas_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID',
  'id_name' => 'lowes_pagos_lf_facturas_1lf_facturas_idb',
  'link' => 'lowes_pagos_lf_facturas_1',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_importado_c.php

 // created: 2016-11-30 16:38:26
$dictionary['lowes_Pagos']['fields']['importado_c']['labelValue']='Importado';
$dictionary['lowes_Pagos']['fields']['importado_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['importado_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_saldo_aplicado_c.php

 // created: 2016-11-14 13:50:22
$dictionary['lowes_Pagos']['fields']['saldo_aplicado_c']['labelValue']='Saldo aplicado';
$dictionary['lowes_Pagos']['fields']['saldo_aplicado_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['saldo_aplicado_c']['dependency']='';
$dictionary['lowes_Pagos']['fields']['saldo_aplicado_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_num_folio_c.php

 // created: 2017-01-18 23:14:55
$dictionary['lowes_Pagos']['fields']['num_folio_c']['help']='Consecutivo';
$dictionary['lowes_Pagos']['fields']['num_folio_c']['auto_increment']=true;
$dictionary['lowes_Pagos']['fields']['num_folio_c']['duplicate_on_record_copy']='no';
$dictionary['lowes_Pagos']['fields']['num_folio_c']['disable_num_format']='1';
$dictionary['lowes_Pagos']['fields']['num_folio_c']['no_default']=true;
$dictionary['lowes_Pagos']['fields']['num_folio_c']['len']='11';
$dictionary['lowes_Pagos']['fields']['num_folio_c']['autoinc_next']='57';
$dictionary['lowes_Pagos']['fields']['num_folio_c']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_numero_cuenta_clabe_c.php

 // created: 2017-09-14 14:49:05
$dictionary['lowes_Pagos']['fields']['numero_cuenta_clabe_c']['labelValue']='Numero de Cuenta CLABE';
$dictionary['lowes_Pagos']['fields']['numero_cuenta_clabe_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowes_Pagos']['fields']['numero_cuenta_clabe_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['numero_cuenta_clabe_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_id_tran_pos_c.php

 // created: 2017-01-18 23:13:42
$dictionary['lowes_Pagos']['fields']['id_tran_pos_c']['labelValue']='Número de Transacción POS';
$dictionary['lowes_Pagos']['fields']['id_tran_pos_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowes_Pagos']['fields']['id_tran_pos_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['id_tran_pos_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_importadode_c.php

 // created: 2017-10-09 17:17:28
$dictionary['lowes_Pagos']['fields']['importadode_c']['labelValue']='Archivo de origen';
$dictionary['lowes_Pagos']['fields']['importadode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowes_Pagos']['fields']['importadode_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['importadode_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/full_text_search_admin.php

 // created: 2017-02-24 18:28:04
$dictionary['lowes_Pagos']['full_text_search']=true;

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_comision_factoraje_c.php

 // created: 2017-10-18 05:35:29
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['labelValue']='Comisión de Factoraje';
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['default']=null;
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['required']=1;
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['dependency']='equal(related($lowes_pagos_accounts,"con_factoraje_c"),true)';
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_id_customer_c.php

 // created: 2017-01-18 23:13:25
$dictionary['lowes_Pagos']['fields']['id_customer_c']['labelValue']='Id Customer';
$dictionary['lowes_Pagos']['fields']['id_customer_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowes_Pagos']['fields']['id_customer_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['id_customer_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/lowae_autorizacion_especial_lowes_pagos_1_lowes_Pagos.php

// created: 2017-03-29 15:36:09
// $dictionary["lowes_Pagos"]["fields"]["lowae_autorizacion_especial_lowes_pagos_1"] = array (
//   'name' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'type' => 'link',
//   'relationship' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'source' => 'non-db',
//   'module' => 'lowae_autorizacion_especial',
//   'bean_name' => 'lowae_autorizacion_especial',
//   'side' => 'right',
//   'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE',
//   'id_name' => 'lowae_auto3868special_ida',
//   'link-type' => 'one',
// );
// $dictionary["lowes_Pagos"]["fields"]["lowae_autorizacion_especial_lowes_pagos_1_name"] = array (
//   'name' => 'lowae_autorizacion_especial_lowes_pagos_1_name',
//   'type' => 'relate',
//   'source' => 'non-db',
//   'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
//   'save' => true,
//   'id_name' => 'lowae_auto3868special_ida',
//   'link' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'table' => 'lowae_autorizacion_especial',
//   'module' => 'lowae_autorizacion_especial',
//   'rname' => 'name',
// );
// $dictionary["lowes_Pagos"]["fields"]["lowae_auto3868special_ida"] = array (
//   'name' => 'lowae_auto3868special_ida',
//   'type' => 'id',
//   'source' => 'non-db',
//   'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE_ID',
//   'id_name' => 'lowae_auto3868special_ida',
//   'link' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'table' => 'lowae_autorizacion_especial',
//   'module' => 'lowae_autorizacion_especial',
//   'rname' => 'id',
//   'reportable' => false,
//   'side' => 'right',
//   'massupdate' => false,
//   'duplicate_merge' => 'disabled',
//   'hideacl' => true,
// );

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_fecha_transaccion.php

 // created: 2017-01-18 23:12:46
$dictionary['lowes_Pagos']['fields']['fecha_transaccion']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_saldo_pendiente_c.php

 // created: 2017-03-04 01:30:57
$dictionary['lowes_Pagos']['fields']['saldo_pendiente_c']['labelValue']='Saldo Pendiente por Aplicar';
$dictionary['lowes_Pagos']['fields']['saldo_pendiente_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['saldo_pendiente_c']['dependency']='';
$dictionary['lowes_Pagos']['fields']['saldo_pendiente_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_linea_anticipo_c.php

 // created: 2017-01-18 23:13:55
$dictionary['lowes_Pagos']['fields']['linea_anticipo_c']['labelValue']='Pago por Anticipo';
$dictionary['lowes_Pagos']['fields']['linea_anticipo_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['linea_anticipo_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_folio_ebs_c.php

 // created: 2017-02-11 19:42:19
$dictionary['lowes_Pagos']['fields']['folio_ebs_c']['labelValue']='Folio EBS';
$dictionary['lowes_Pagos']['fields']['folio_ebs_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowes_Pagos']['fields']['folio_ebs_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['folio_ebs_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/lowae_autorizacion_especial_lowes_pagos_2_lowes_Pagos.php

// created: 2017-11-06 23:18:19
$dictionary["lowes_Pagos"]["fields"]["lowae_autorizacion_especial_lowes_pagos_2"] = array (
  'name' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'type' => 'link',
  'relationship' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'source' => 'non-db',
  'module' => 'lowae_autorizacion_especial',
  'bean_name' => 'lowae_autorizacion_especial',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'id_name' => 'lowae_auto4a14special_ida',
);

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/low01_pagos_facturas_lowes_pagos_lowes_Pagos.php

// created: 2017-04-03 22:41:21
$dictionary["lowes_Pagos"]["fields"]["low01_pagos_facturas_lowes_pagos"] = array (
  'name' => 'low01_pagos_facturas_lowes_pagos',
  'type' => 'link',
  'relationship' => 'low01_pagos_facturas_lowes_pagos',
  'source' => 'non-db',
  'module' => 'Low01_Pagos_Facturas',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'low01_pagos_facturas_lowes_pagoslowes_pagos_ida',
  'link-type' => 'many',
  'side' => 'left',
);

?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_sucursal_cliente_c.php

 // created: 2017-09-14 14:48:43
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['duplicate_merge_dom_value']=0;
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['labelValue']='Sucursal de Cliente';
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['calculated']='1';
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['formula']='ifElse(not(equal($sucursal_cliente_c,"")),related($lowes_pagos_accounts,"id_sucursal_c"),"")';
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['enforced']='1';
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_forma_pago.php

 // created: 2017-01-18 23:13:09
$dictionary['lowes_Pagos']['fields']['forma_pago']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_folio_konesh_c.php

 // created: 2017-08-23 18:56:06
$dictionary['lowes_Pagos']['fields']['folio_konesh_c']['duplicate_merge_dom_value']=0;
$dictionary['lowes_Pagos']['fields']['folio_konesh_c']['labelValue']='Folio konesh';
$dictionary['lowes_Pagos']['fields']['folio_konesh_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowes_Pagos']['fields']['folio_konesh_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['folio_konesh_c']['dependency']='';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_monto.php

 // created: 2017-01-18 23:14:18
$dictionary['lowes_Pagos']['fields']['monto']['default']=0;
$dictionary['lowes_Pagos']['fields']['monto']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_cheque_devuelto.php

 // created: 2017-01-31 17:46:04
$dictionary['lowes_Pagos']['fields']['cheque_devuelto']['audited']=true;
$dictionary['lowes_Pagos']['fields']['cheque_devuelto']['dependency']='equal($forma_pago,"Check")';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_name.php

 // created: 2017-01-18 23:14:35
$dictionary['lowes_Pagos']['fields']['name']['len']='255';
$dictionary['lowes_Pagos']['fields']['name']['required']=false;
$dictionary['lowes_Pagos']['fields']['name']['audited']=true;
$dictionary['lowes_Pagos']['fields']['name']['massupdate']=false;
$dictionary['lowes_Pagos']['fields']['name']['unified_search']=false;
$dictionary['lowes_Pagos']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['lowes_Pagos']['fields']['name']['calculated']=false;
$dictionary['lowes_Pagos']['fields']['name']['importable']='false';

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_pago_anticipo.php

 // created: 2017-01-18 23:15:50
$dictionary['lowes_Pagos']['fields']['pago_anticipo']['audited']=true;

 
?>
<?php
// Merged from custom/Extension/modules/lowes_Pagos/Ext/Vardefs/sugarfield_estado_konesh_c.php

 // created: 2018-10-10 23:52:34
$dictionary['lowes_Pagos']['fields']['estado_konesh_c']['labelValue']='Estado konesh';
$dictionary['lowes_Pagos']['fields']['estado_konesh_c']['dependency']='';
$dictionary['lowes_Pagos']['fields']['estado_konesh_c']['visibility_grid']='';

 
?>
