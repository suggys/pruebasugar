<?php
// created: 2017-10-09 17:26:35
$viewdefs['lowes_Pagos']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' =>
  array (
    'name' =>
    array (
    ),
    'forma_pago' =>
    array (
    ),
    'fecha_transaccion' =>
    array (
    ),
    'linea_anticipo_c' =>
    array (
    ),
    'saldo_pendiente_c' =>
    array (
    ),
    'cheque_devuelto' =>
    array (
    ),
    'lowes_pagos_accounts_name' =>
    array (
    ),
    'id_tran_pos_c' =>
    array (
    ),
    'date_entered' =>
    array (
    ),
    'folio_konesh_c' =>
    array (
    ),
    'estado_konesh_c' =>
    array (
    ),
    'date_modified' =>
    array (
    ),
    'importado_c' =>
    array (
    ),
    'importadode_c' =>
    array (
    ),
    '$owner' =>
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' =>
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);
