({
    extendsFrom: 'SubpanelsLayout',

    initialize: function (options) {
        this._super('initialize',arguments);
    },
    showSubpanel: function (linkName) {
        var self = this;
        _.each(self._components, function (component) {
            var link = component.context.get('link');
            if(_.contains(['lowes_pagos_lf_facturas_1', 'lowes_pagos_lf_facturas'], link)){
                component.hide();
            }
            if(link === 'lowes_pagos_lf_facturas'){
                component.$el.find('a[name="create_button"]').addClass('disabled');
            }
        });
    }
})
