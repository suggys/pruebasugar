({
	extendsFrom:"CreateView",
	initialize: function () {
        var self = this;
		this._super("initialize", arguments);
        this.model.addValidationTask('validatePagoAnticipo',_.bind(this._doValidatePagoPorAnticipo, this));
	},
    _render: function (argument) {
        var self = this;
        this._super('_render', arguments);
    },
    _doValidatePagoPorAnticipo: function(fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        if(self.model.get('linea_anticipo_c')){
			if(self.model.get('id_customer_c')){
	            var filter = [{
	                id_linea_anticipo_c: self.model.get('id_customer_c'),
	            }];
	            var url = app.api.buildURL('Accounts', 'read',{},{filter:filter});
	            app.api.call('read', url, {}, {
	                success: function(data){
	                    if(!data.records.length){
	                        errors['linea_anticipo_c'] = errors['linea_anticipo_c'] || {};
	                        errors['linea_anticipo_c']['No existe un cliente con ese id de linea de anticipo.'] = true;
	                    }
	                    callback(null, fields, errors);
	                }
	            });
			}
			else{
				errors['linea_anticipo_c'] = errors['linea_anticipo_c'] || {};
				errors['linea_anticipo_c']['Debe de ingresar el ID Customer.'] = true;
				callback(null, fields, errors);
			}
        }else {
        	callback(null, fields, errors);
        }
    },
})
