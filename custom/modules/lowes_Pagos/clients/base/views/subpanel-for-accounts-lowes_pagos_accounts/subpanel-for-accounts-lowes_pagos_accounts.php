<?php
// created: 2016-11-15 22:59:43
$viewdefs['lowes_Pagos']['base']['view']['subpanel-for-accounts-lowes_pagos_accounts'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'id_tran_pos_c',
          'label' => 'LBL_ID_TRAN_POS_C',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        2 => 
        array (
          'name' => 'forma_pago',
          'label' => 'LBL_FORMA_PAGO',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'fecha_transaccion',
          'label' => 'LBL_FECHA_TRANSACCION',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'monto',
          'label' => 'LBL_MONTO',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'saldo_pendiente_c',
          'label' => 'LBL_SALDO_PENDIENTE_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'saldo_aplicado_c',
          'label' => 'LBL_SALDO_APLICADO_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'linea_anticipo_c',
          'label' => 'LBL_LINEA_ANTICIPO',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        9 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);