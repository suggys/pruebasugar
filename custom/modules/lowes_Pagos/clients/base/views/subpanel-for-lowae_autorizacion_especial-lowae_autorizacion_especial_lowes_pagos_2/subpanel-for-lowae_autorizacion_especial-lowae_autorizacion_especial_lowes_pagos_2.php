<?php
// created: 2017-11-06 23:37:09
$viewdefs['lowes_Pagos']['base']['view']['subpanel-for-lowae_autorizacion_especial-lowae_autorizacion_especial_lowes_pagos_2'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'forma_pago',
          'label' => 'LBL_FORMA_PAGO',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'fecha_transaccion',
          'label' => 'LBL_FECHA_TRANSACCION',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'monto',
          'label' => 'LBL_MONTO',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'saldo_aplicado_c',
          'label' => 'LBL_SALDO_APLICADO_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'saldo_pendiente_c',
          'label' => 'LBL_SALDO_PENDIENTE_C',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'linea_anticipo_c',
          'label' => 'LBL_LINEA_ANTICIPO',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'id_customer_c',
          'label' => 'LBL_ID_CUSTOMER_C',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'lowes_pagos_accounts_name',
          'label' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
          'enabled' => true,
          'id' => 'LOWES_PAGOS_ACCOUNTSACCOUNTS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'cheque_devuelto',
          'label' => 'LBL_CHEQUE_DEVUELTO',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'importado_c',
          'label' => 'LBL_IMPORTADO',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);