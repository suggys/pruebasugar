({
  extendsFrom: 'SubpanelListView',

	initialize: function(options) {
		this._super('initialize',[options]);
	},
	_initializeMetadata : function(options) {
        var tmp = this._super('_initializeMetadata', arguments);
           tmp.rowactions.actions = _.without(tmp.rowactions.actions, _.findWhere(tmp.rowactions.actions, {
              acl_action: "edit"
           }));
        return tmp;
    },

})
