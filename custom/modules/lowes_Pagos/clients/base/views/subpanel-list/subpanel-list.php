<?php
// created: 2016-11-15 22:59:43
$viewdefs['lowes_Pagos']['base']['view']['subpanel-list'] = array (
  'template' => 'flex-list',
  'favorite' => true,
  'rowactions' => array(
      'actions' => array(
          array(
              'type' => 'rowaction',
              'css_class' => 'btn',
              'tooltip' => 'LBL_PREVIEW',
              'event' => 'list:preview:fire',
              'icon' => 'fa-eye',
              'acl_action' => 'view',
              'allow_bwc' => false,
          ),
          array(
              'type' => 'rowaction',
              'name' => 'edit_button',
              'icon' => 'fa-pencil',
              'label' => 'LBL_EDIT_BUTTON',
              'event' => 'list:editrow:fire',
              'acl_action' => 'edit',
              'allow_bwc' => true,
          ),
      ),
  ),
  'last_state' => array(
      'id' => 'subpanel-list',
  ),
);
