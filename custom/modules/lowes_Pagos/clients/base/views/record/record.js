({
    extendsFrom: 'RecordView',

    initialize: function(options) {
        this._super('initialize', [options]);
        var self = this;
        self.model.on('sync', function(model, value){
            self._hideLineaAnt();
        });
        self.model.on('change:saldo_pendiente_c', function(model, value){
            self._hideLineaAnt();
        });

        self.model.addValidationTask('validatePagoAplicado',_.bind(self._validatePagoAplicado, this));
        this.model.addValidationTask('validatePagoAnticipo',_.bind(this._doValidatePagoAnticipo, this));
    },
    _doValidatePagoAnticipo: function(fields, errors, callback) {
        var self = this;
        if(!self.model) return false;
        // var modelSynced = app.data.createBean(self.model.module, self.model.getSynced());
        // var changedArray = app.utils.compareBeans(self.model, modelSynced);
        // if(_.contains(changedArray, "linea_anticipo_c")){
        if(self.model.get('linea_anticipo_c')){
            var filter = [{
                id:self.model.get('lowes_pagos_accountsaccounts_ida'),
                id_linea_anticipo_c: self.model.get('id_customer_c'),
            }];
            var url = app.api.buildURL('Accounts', 'read',{},{filter:filter});
            app.api.call('read', url, {}, {
                success: function(data){
                    if(!data.records.length){
                        errors['linea_anticipo_c'] = errors['linea_anticipo_c'] || {};
                        errors['linea_anticipo_c']['El cliente: ' + self.model.get('lowes_pagos_accounts_name') + ' no tiene un id de linea de anticipo o el id customer no es de linea de anticipo.'] = true;
                    }
                    callback(null, fields, errors);
                }
            });
        }else{
            callback(null, fields, errors);
        }
    },
    _hideLineaAnt:function(){
        var self = this;
        if(
          !self.model.get('linea_anticipo_c')
          && self.model.get('monto') > 0
          && self.model.get('monto') !== self.model.get('saldo_pendiente_c')
        ){
          self.$el.find('.record-cell[data-name="linea_anticipo_c"]').addClass('hide');
        }
        else{
          self.$el.find('.record-cell[data-name="linea_anticipo_c"]').removeClass('hide');
        }
    },
    _validatePagoAplicado: function(fields, errors, callback) {
        var self = this;
        var self = this;
        var modelSynced = app.data.createBean(self.model.module, self.model.getSynced());
        var changedArray = app.utils.compareBeans(self.model, modelSynced);

        //Si esta chequeado
        if(self.model.get('linea_anticipo_c')) {
                if(changedArray && changedArray.indexOf('linea_anticipo_c') > -1) {
                    var Factura = app.data.createRelatedCollection(self.model, 'lowes_pagos_lf_facturas');
                    Factura.fetch({
                        success: function(arguments) {
                            //console.log(arguments);
                            self._processPagoSuccess(arguments,
                                function (isValid) {
                                    if(!isValid){
                                        errors['linea_anticipo_c'] = errors['linea_anticipo_c'] || {};
                                        errors['linea_anticipo_c']['No se puede cambiar a Linea anticipo, porque tiene una factura relacionada'] = true;
                                    }
                                    callback(null, fields, errors);
                                }
                            );
                        }
                    });
                } else
                    callback(null, fields, errors);
        } else {
            callback(null, fields, errors);
        }
    },

    _processPagoSuccess: function (data, callback) {
        var isValid = true;
        var self = this;

        if(data) {
            if(data.models.length > 0)
                isValid = false;
        }

        callback(isValid);
    },
    /*
    _renderHtml: function (argument) {
        var self = this;
        this._super('_renderHtml', arguments);
    },
    */

    _render: function (argument) {
        var self = this;
        this._super('_render', arguments);
    },

    // _doValidateChequeDevueltoAplicado: function (fields, errors, callback) {
    //     var modelSynced = app.data.createBean(this.model.module, this.model.getSynced());
    //     if(
    //         modelSynced.get('cheque_devuelto') === false
    //         && this.model.get('cheque_devuelto') === true
    //     ){
    //         errors['cheque_devuelto'] = errors['cheque_devuelto'] || {};
    //         errors['cheque_devuelto']['El cheque ya ha sido aplicado, no se permite cambiar a Cheque Devuelto.'] = true;
    //     }
    //     callback(null, fields, errors);
    // }
})
