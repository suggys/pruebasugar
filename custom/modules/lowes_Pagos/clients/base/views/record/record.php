<?php
$module_name = 'lowes_Pagos';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'record' =>
      array (
        'buttons' =>
        array (
          0 =>
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' =>
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 =>
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 =>
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' =>
            array (
              0 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 =>
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 =>
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 =>
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 =>
              array (
                'type' => 'divider',
              ),
              5 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'lowes_Pagos',
                'acl_action' => 'create',
              ),
              7 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 =>
              array (
                'type' => 'divider',
              ),
              9 =>
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 =>
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' =>
        array (
          0 =>
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 =>
              array (
                'name' => 'name',
                'type' => 'name',
                'readonly' => true,
              ),
              2 =>
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 =>
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 =>
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => true,
            'panelDefault' => 'expanded',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'id_tran_pos_c',
                'label' => 'LBL_ID_TRAN_POS_C',
                'readonly' => true,
              ),
              1 =>
              array (
                'name' => 'forma_pago',
                'label' => 'LBL_FORMA_PAGO',
              ),
              2 =>
              array (
                'name' => 'fecha_transaccion',
                'label' => 'LBL_FECHA_TRANSACCION',
              ),
              3 =>
              array (
                'name' => 'linea_anticipo_c',
                'label' => 'LBL_LINEA_ANTICIPO_C',
              ),
              4 =>
              array (
                'name' => 'cheque_devuelto',
                'label' => 'LBL_CHEQUE_DEVUELTO',
              ),
              5 =>
              array (
                'name' => 'id_customer_c',
                'label' => 'LBL_ID_CUSTOMER_C',
              ),
              6 =>
              array (
                'name' => 'numero_cuenta_clabe_c',
                'label' => 'LBL_NUMERO_CUENTA_CLABE_C',
              ),
              7 =>
              array (
                'name' => 'lowes_pagos_accounts_name',
                'readonly' => true,
              ),
              8 =>
              array (
                'name' => 'lowes_pagos_lf_facturas_1_name',
              ),
              9 =>
              array (
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'comision_factoraje_c',
                'label' => 'LBL_COMISION_FACTORAJE',
              ),
              10 =>
              array (
                'name' => 'description',
                'span' => 12,
              ),
            ),
          ),
          2 =>
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' =>
            array (
              0 =>
              array (
              ),
              1 =>
              array (
                'name' => 'monto',
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'label' => 'LBL_MONTO',
              ),
              2 =>
              array (
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'saldo_pendiente_c',
                'label' => 'LBL_SALDO_PENDIENTE_C',
                'readonly' => true,
              ),
              3 =>
              array (
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'name' => 'saldo_aplicado_c',
                'label' => 'LBL_SALDO_APLICADO_C',
                'readonly' => true,
              ),
            ),
          ),
          3 =>
          array (
            'newTab' => true,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'estado_konesh_c',
                'label' => 'LBL_ESTADO_KONESH',
              ),
              1 =>
              array (
                'name' => 'folio_konesh_c',
                'label' => 'LBL_FOLIO_KONESH',
              ),
              2 =>
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' =>
                array (
                  0 =>
                  array (
                    'name' => 'date_modified',
                  ),
                  1 =>
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 =>
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              3 =>
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' =>
                array (
                  0 =>
                  array (
                    'name' => 'date_entered',
                  ),
                  1 =>
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 =>
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              4 =>
              array (
                'name' => 'importadode_c',
                'label' => 'LBL_IMPORTADODE',
                'readonly' => true,
              ),
              5 =>
              array (
              ),
            ),
          ),
        ),
        'templateMeta' =>
        array (
          'useTabs' => true,
        ),
      ),
    ),
  ),
);
