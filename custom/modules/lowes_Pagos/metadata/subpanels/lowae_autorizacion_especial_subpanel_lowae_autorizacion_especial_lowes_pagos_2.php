<?php
// created: 2017-11-06 23:36:34
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'forma_pago' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_FORMA_PAGO',
    'width' => '10%',
  ),
  'fecha_transaccion' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_FECHA_TRANSACCION',
    'width' => '10%',
    'default' => true,
  ),
  'monto' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_MONTO',
    'currency_format' => true,
    'width' => '10%',
  ),
  'saldo_aplicado_c' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_SALDO_APLICADO_C',
    'currency_format' => true,
    'width' => '10%',
  ),
  'saldo_pendiente_c' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_SALDO_PENDIENTE_C',
    'currency_format' => true,
    'width' => '10%',
  ),
  'linea_anticipo_c' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_LINEA_ANTICIPO',
    'width' => '10%',
  ),
  'id_customer_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_ID_CUSTOMER_C',
    'width' => '10%',
  ),
  'lowes_pagos_accounts_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
    'id' => 'LOWES_PAGOS_ACCOUNTSACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Accounts',
    'target_record_key' => 'lowes_pagos_accountsaccounts_ida',
  ),
  'cheque_devuelto' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_CHEQUE_DEVUELTO',
    'width' => '10%',
  ),
  'importado_c' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_IMPORTADO',
    'width' => '10%',
  ),
);