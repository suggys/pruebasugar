<?php
// created: 2016-11-15 22:59:38
$subpanel_layout['list_fields'] = array (
  'id_tran_pos_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_ID_TRAN_POS_C',
    'width' => '10%',
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'forma_pago' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_FORMA_PAGO',
    'width' => '10%',
  ),
  'fecha_transaccion' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_FECHA_TRANSACCION',
    'width' => '10%',
    'default' => true,
  ),
  'monto' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'vname' => 'LBL_MONTO',
    'currency_format' => true,
    'width' => '10%',
  ),
  'saldo_pendiente_c' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_SALDO_PENDIENTE_C',
    'currency_format' => true,
    'width' => '10%',
  ),
  'saldo_aplicado_c' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_SALDO_APLICADO_C',
    'currency_format' => true,
    'width' => '10%',
  ),
  'linea_anticipo_c' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_LINEA_ANTICIPO',
    'width' => '10%',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
);