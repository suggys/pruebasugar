<?php
$popupMeta = array (
    'moduleMain' => 'lowes_Pagos',
    'varName' => 'lowes_Pagos',
    'orderBy' => 'lowes_pagos.name',
    'whereClauses' => array (
  'name' => 'lowes_pagos.name',
  'forma_pago' => 'lowes_pagos.forma_pago',
  'fecha_transaccion' => 'lowes_pagos.fecha_transaccion',
  'linea_anticipo_c' => 'lowes_pagos_cstm.linea_anticipo_c',
  'cheque_devuelto' => 'lowes_pagos.cheque_devuelto',
  'lowes_pagos_accounts_name' => 'lowes_pagos.lowes_pagos_accounts_name',
  'id_tran_pos_c' => 'lowes_pagos_cstm.id_tran_pos_c',
  'date_entered' => 'lowes_pagos.date_entered',
  'date_modified' => 'lowes_pagos.date_modified',
  'importado_c' => 'lowes_pagos_cstm.importado_c',
  'favorites_only' => 'lowes_pagos.favorites_only',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'forma_pago',
  5 => 'fecha_transaccion',
  6 => 'linea_anticipo_c',
  7 => 'cheque_devuelto',
  8 => 'lowes_pagos_accounts_name',
  9 => 'id_tran_pos_c',
  10 => 'date_entered',
  11 => 'date_modified',
  12 => 'importado_c',
  13 => 'favorites_only',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'forma_pago' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_FORMA_PAGO',
    'width' => '10%',
    'name' => 'forma_pago',
  ),
  'fecha_transaccion' => 
  array (
    'type' => 'date',
    'label' => 'LBL_FECHA_TRANSACCION',
    'width' => '10%',
    'name' => 'fecha_transaccion',
  ),
  'linea_anticipo_c' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_LINEA_ANTICIPO',
    'width' => '10%',
    'name' => 'linea_anticipo_c',
  ),
  'cheque_devuelto' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_CHEQUE_DEVUELTO',
    'width' => '10%',
    'name' => 'cheque_devuelto',
  ),
  'lowes_pagos_accounts_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
    'id' => 'LOWES_PAGOS_ACCOUNTSACCOUNTS_IDA',
    'width' => '10%',
    'name' => 'lowes_pagos_accounts_name',
  ),
  'id_tran_pos_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ID_TRAN_POS_C',
    'width' => '10%',
    'name' => 'id_tran_pos_c',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'name' => 'date_modified',
  ),
  'importado_c' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_IMPORTADO',
    'width' => '10%',
    'name' => 'importado_c',
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'FORMA_PAGO' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_FORMA_PAGO',
    'width' => '10%',
  ),
  'FECHA_TRANSACCION' => 
  array (
    'type' => 'date',
    'label' => 'LBL_FECHA_TRANSACCION',
    'width' => '10%',
    'default' => true,
  ),
  'MONTO' => 
  array (
    'type' => 'currency',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'label' => 'LBL_MONTO',
    'currency_format' => true,
    'width' => '10%',
  ),
  'SALDO_APLICADO_C' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_SALDO_APLICADO_C',
    'currency_format' => true,
    'width' => '10%',
  ),
  'SALDO_PENDIENTE_C' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_SALDO_PENDIENTE_C',
    'currency_format' => true,
    'width' => '10%',
  ),
  'LOWES_PAGOS_ACCOUNTS_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_LOWES_PAGOS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
    'id' => 'LOWES_PAGOS_ACCOUNTSACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'LINEA_ANTICIPO_C' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_LINEA_ANTICIPO',
    'width' => '10%',
  ),
  'CHEQUE_DEVUELTO' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_CHEQUE_DEVUELTO',
    'width' => '10%',
  ),
  'IMPORTADO_C' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_IMPORTADO',
    'width' => '10%',
  ),
),
);
