<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/SugarQuery/SugarQuery.php');
require_once('include/TimeDate.php');

class PagosLogicHooksManager{
	protected static $fetchedRow = array();
	protected static $saved = array();

	public function bloqueaPorChequeDevuelto($bean, $event, $arguments){
		if(isset($bean->importado_c) && $bean->importado_c){
			return true;
		}
		if(
			$arguments['related_module'] === "Low01_Pagos_Facturas"
		){
			$beanPagoFactura = BeanFactory::getBean('Low01_Pagos_Facturas', $arguments['related_id']);
			if($beanPagoFactura->id){
				$bean->saldo_pendiente_c -= $beanPagoFactura->importe;
				$bean->saldo_aplicado_c += $beanPagoFactura->importe;
				$bean->save(false);
			}
		}
	}

	public function desaplicarNotaCargo($bean, $event, $arguments)
	{
		if(isset($bean->importado_c) && $bean->importado_c){return true;}

		if(
			$arguments['related_module'] === "Low01_Pagos_Facturas"
		){
			$beanPagoFactura = BeanFactory::newBean('Low01_Pagos_Facturas');
            $beanPagoFactura->fetch($arguments['related_id']);
			if($beanPagoFactura->id){
				$bean->saldo_pendiente_c += $beanPagoFactura->importe;
				$bean->save(false);
			}
		}

	if(
		$arguments['related_module'] === "Accounts"
		&& $bean->deleted
	){
		$account = BeanFactory::getBean("Accounts", $arguments['related_id']);
		$account->saldo_pendiente_aplicar_c -= $bean->monto;
		$account->credito_disponible_c -= $bean->monto;
		$account->save(false);
	}

	// if(
	// 	$arguments['related_module'] === "lowae_autorizacion_especial"
	// ){
	// 	$promesaPago = BeanFactory::getBean("lowae_autorizacion_especial", $arguments['related_id']);
	// 	$promesaPago->monto_abonado_c -= $bean->monto;
	// 	$promesaPago->estado_promesa_c = 'vigente';
	// 	$promesaPago->estado_promesa_segun_monto_c = $promesaPago->monto_abonado_c > 0 ? "pagada_parcial" : "sin_pago";
	// 	$promesaPago->save();
	// }
}

public function getChequesDevueltos($account)
{
	if(isset($bean->importado_c) && $bean->importado_c){
		return true;
	}
	$sugarQuery = new SugarQuery();
	$bean = BeanFactory::newBean('lowes_Pagos');
	$sugarQuery->from($bean, array('team_security' => false));
	$sugarQuery->where()
	->equals('lowes_pagos_accountsaccounts_ida', $account->id)
	->equals('forma_pago', 'Check')
	->equals('cheque_devuelto', 1);
	$sugarQuery->select(array('id'));
	return $sugarQuery->execute();
}

public function getAccountsChequeDevuleto($account)
{
	if(isset($bean->importado_c) && $bean->importado_c){
		return true;
	}
	$sugarQuery = new SugarQuery();
	$bean = BeanFactory::newBean('Accounts');
	$sugarQuery->from($bean, array('team_security' => false));
	$pagos = $sugarQuery->join('lowes_pagos_accounts')->joinName();
	$sugarQuery->where()
	->equals('gpe_grupo_empresas_accountsgpe_grupo_empresas_ida', $account->gpe_grupo_empresas_accountsgpe_grupo_empresas_ida)
	->equals("$pagos.forma_pago", 'Check')
	->equals("$pagos.cheque_devuelto", 1);
	$sugarQuery->select(array('id'));

	return $sugarQuery->execute();
}

public function antesDeliberarCreditoDisponible($bean, $event, $arguments){
	// if ( !empty($bean->id) ) {
	// 	self::$fetchedRow[$bean->id] = $bean->fetched_row;
	// }
}

public function liberaCreditoDisponible($bean, $event, $arguments)
{
	if(isset($bean->importado_c) && $bean->importado_c){
		return true;
	}

	if(
		!$arguments['isUpdate']
		&& empty($bean->fetched_row['name'])
		&& !empty($bean->id_customer_c)
		&& empty($bean->numero_cuenta_clabe_c)
		&& empty($bean->linea_anticipo_c)
		)
		{
			$sugarQuery = new SugarQuery();
			$sugarQuery->select(array('id'));
			$cuenta = BeanFactory::newBean('Accounts');
			$sugarQuery->from($cuenta, array('team_security' => false));
			$sugarQuery->where()
			->queryOr()
			->equals('id_ar_credit_c', $bean->id_customer_c)
			->equals('id_linea_anticipo_c', $bean->id_customer_c);
			$result = $sugarQuery->execute();
			if (!empty($result))
			{
				$account = BeanFactory::getBean('Accounts', $result[0]['id']);
				$account->retrieve();
				$account->load_relationship('lowes_pagos_accounts');
				$account->lowes_pagos_accounts->add($bean);

			}
		}
		return true;
	}

	public function aplicaPagoALineaAncipio($bean, $event, $arguments)
	{
		if(isset($bean->importado_c) && $bean->importado_c){
			return true;
		}
		if (empty($bean->fetched_row['id']) ) {
			$bean->estado_konesh_c = "Activo";
		}
		if ( !empty($bean->id) ) {
			self::$fetchedRow[$bean->id] = $bean->fetched_row;
		}

		$fieldIndex = null;
		$value = null;

		if(!$arguments['isUpdate']){
			if( $bean->id_customer_c ){

				$fieldIndex = substr($bean->id_customer_c, -4) < 7000 ? "id_ar_credit_c" : "id_linea_anticipo_c";
				$value = $bean->id_customer_c;
			}

			if( $bean->numero_cuenta_clabe_c )
			{
				$fieldIndex = "numero_cuenta_clabe_c";
				$value = $bean->numero_cuenta_clabe_c;
			}

			if($fieldIndex === "id_linea_anticipo_c")
			{
				$bean->linea_anticipo_c = '1';
			}

			$bean->saldo_aplicado_c  = 0;
			$bean->saldo_pendiente_c = $bean->monto;
		}

		if(
			$arguments['isUpdate']
			&& $bean->linea_anticipo_c != self::$fetchedRow[$bean->id]['linea_anticipo_c']
			&& self::$fetchedRow[$bean->id]['id']
			)
			{
				if($bean->linea_anticipo_c == '1')
				{
					$bean->saldo_aplicado_c = $bean->monto;
					$bean->saldo_pendiente_c = 0;
				}
				else
				{
					$bean->saldo_aplicado_c = 0;
					$bean->saldo_pendiente_c = $bean->monto;
				}
			}

			if(
				$arguments['isUpdate']
				&& $bean->cheque_devuelto == true && $bean->fetched_row['cheque_devuelto'] == false
			){
				$bean->saldo_pendiente_c -= $bean->monto;
			}

			if(
				$arguments['isUpdate']
				&& $bean->saldo_pendiente_c == 0
				&& $bean->fetched_row['saldo_pendiente_c'] > 0
				&& $bean->folio_konesh_c == 0
			){
				$bean->estado_konesh_c = "Aplicado";
			}

			if(
				$arguments['isUpdate']
				&& $bean->saldo_pendiente_c == 0
				&& $bean->fetched_row['saldo_pendiente_c'] > 0
				&& $bean->folio_konesh_c > 0
			){
				$bean->estado_konesh_c = "Reaplicado";
			}

			if(
				$arguments['isUpdate']
				&& $bean->saldo_pendiente_c > 0
				&& $bean->fetched_row['saldo_pendiente_c'] == 0
			){
				$bean->estado_konesh_c = "Activo";
			}
		}

		public function calculaSaldoDisponibleCuenta($bean, $event, $arguments)
		{
			if(isset($bean->importado_c) && $bean->importado_c){
				return true;
			}
			$fieldIndex = null;
			$value = null;
			if(!$arguments['isUpdate']){
				self::$saved[$bean->id] = true;
				if(
					$bean->id_customer_c
				){
					$fieldIndex = substr($bean->id_customer_c, -4) < 7000 ? "id_ar_credit_c" : "id_linea_anticipo_c";
					$value = $bean->id_customer_c;
				}
				else if(
					$bean->numero_cuenta_clabe_c
				){
					$fieldIndex = "numero_cuenta_clabe_c";
					$value = $bean->numero_cuenta_clabe_c;
				}

				if( $fieldIndex && $value){
					$account = BeanFactory::newBean('Accounts');
					$retrieveFields = array();
					$retrieveFields[$fieldIndex] = $value;
					$account->retrieve_by_string_fields($retrieveFields);
					if($account->id){
						$account->load_relationship('lowes_pagos_accounts');
						$account->lowes_pagos_accounts->add($bean->id);
					}
				}
			}

			if($bean->cheque_devuelto == true && self::$fetchedRow[$bean->id]['cheque_devuelto'] == false){
					$bean->retrieve();
					$account = BeanFactory::getBean('Accounts', $bean->lowes_pagos_accountsaccounts_ida);
					if($account->id){
						$account->retrieve();
						$account->estado_bloqueo_c = 'Bloqueado';
						$account->codigo_bloqueo_c = 'CD';
	          // $account->saldo_deudor_c += $bean->monto;
						$account->credito_disponible_c -= $bean->monto;
	          $account->saldo_pendiente_aplicar_c -= $bean->monto;
			      $account->save(false);
						$bean->load_relationship('lowae_autorizacion_especial_lowes_pagos_2');
						$autorizaciones = $bean->lowae_autorizacion_especial_lowes_pagos_2->getBeans();
						if(count($autorizaciones)){
							$promesaPago = current($autorizaciones);
							$promesaPago->retrieve();
							$promesaPago->monto_abonado_c -= $bean->monto;
							$promesaPago->estado_promesa_c = 'vigente';
							$promesaPago->save(false);
						}
						$bean->load_relationship('low01_pagos_facturas_lowes_pagos');
						$pagosFacturas = $bean->low01_pagos_facturas_lowes_pagos->getBeans();
						if(count($pagosFacturas)){
							foreach ($pagosFacturas as $pagoFactura) {
								$pagoFactura->mark_deleted($pagoFactura->id);
				        $pagoFactura->save();
							}
						}
						else{
							$bean->saldo_aplicado_c += $bean->monto;
							$bean->save(false);
						}
					}
				}


		}

		public function calculaSaldoFavorCtaPorPagoAticipo($bean, $event, $arguments){
			if(isset($bean->importado_c) && $bean->importado_c){
				return true;
			}
			if (
				$arguments['isUpdate']
				&& $bean->linea_anticipo_c != self::$fetchedRow[$bean->id]['linea_anticipo_c']
				&& self::$fetchedRow[$bean->id]['id']
			) {
				$Cta = BeanFactory::getBean('Accounts', $bean->lowes_pagos_accountsaccounts_ida);
				$Cta->retrieve();
				if(isset($Cta->importado_c) && $Cta->importado_c){
					return true;
				}

				if($bean->linea_anticipo_c == '1'){
					$Cta->saldo_favor_c += $bean->monto;
					$Cta->saldo_pendiente_aplicar_c -= $bean->monto;
					$Cta->credito_disponible_c -= $bean->monto;
					$this->recalculaAbonoEnAEporPromesaDePago($Cta->id, $bean->monto);
				}
				else{
					if($Cta->saldo_favor_c >= $bean->monto){
						$Cta->saldo_favor_c -= $bean->monto;
					}
					$Cta->credito_disponible_c += $bean->monto;
					$this->recalculaAbonoEnAEporPromesaDePago($Cta->id, $bean->monto, false);
				}
				$Cta->save();
			}
		}

		function recalculaAbonoEnAEporPromesaDePago($idCuenta, $monto, $noEsLineaAnticipo = true)
		{
			$sq = new SugarQuery();
		  $sq->select()->fieldRaw("au.id, au.name, au.monto_promesa_c, au.monto_abonado_c, au.date_modified, au.estado_promesa_c, au.fecha_cumplimiento_c");
		  $sq->from(BeanFactory::newBean('lowae_autorizacion_especial'),array('alias'=>'au','team_security'=>false));
			$sq->joinTable('accounts_lowae_autorizacion_especial_1_c', array('alias' => 'ac_au'))->on()
		  ->equalsField('ac_au.accounts_le69cspecial_idb','au.id')
			->equals('ac_au.deleted',0);
			$sq->joinTable('lowes_pagos_accounts_c', array('alias' => 'pa'))->on()
		  ->equalsField('pa.lowes_pagos_accountsaccounts_ida','ac_au.accounts_lowae_autorizacion_especial_1accounts_ida')
			->equals('pa.deleted',0);
			$sq->joinTable('lowes_pagos', array('alias' => 'p'))->on()
		  ->equalsField('p.id','pa.lowes_pagos_accountslowes_pagos_idb')
			->equals('p.deleted',0);
			$sq->where()->queryAnd()->equals('motivo_solicitud_c','promesa_de_pago')
			->equals('autorizacion_c',1);
			$sq->whereRaw("ac_au.accounts_lowae_autorizacion_especial_1accounts_ida= '$idCuenta'");
			$sq->orderByRaw("au.date_entered");
			$sq->limit(1);

			$res = $sq->execute();
			$num_rows = count($res);
			return true;
		}
	}
