<?php
require_once('modules/EmailTemplates/EmailTemplate.php');
require_once('modules/Users/User.php');
require_once("include/SugarPHPMailer.php");
require_once('modules/Administration/Administration.php');
class NotifierHelper {
    var $success = false;
    var $admin ;
    var $emailTemp;
    var $usr;
    var $htmlBody;
    var $mail;
    var $subject;
    var $cpmail;
    function __construct() {
        $admin = new Administration();
        $admin-> retrieveSettings();
        $this->fromaddress = $admin->settings['notify_fromaddress'];
        $this->fromname = $admin->settings['notify_fromname'];
    }
    function sendMessage($bean, $sendToBeans, $template_id, $onlyPrimaryAddress = true ){
        $mail = new SugarPHPMailer();
        $mail->CharSet = "UTF-8";
        $mail->prepForOutbound();
        $mail->setMailerForSystem();
        $mail->From = $this->fromaddress;
        $mail->FromName = $this->fromname;
        $mail->ClearAllRecipients();
        $mail->ClearReplyTos();
        $emailTemp = new EmailTemplate();
        $emailTemp->retrieve($template_id);
        require_once "include/workflow/alert_utils.php";
        $emailTemp->subject = trim(parse_alert_template($bean, $emailTemp->subject));
        $emailTemp->body_html = parse_alert_template($bean, $emailTemp->body_html);
        if(!empty($emailTemp->body_html)){
            $htmlBody = $emailTemp->body_html;
            $mail->isHTML(true);
            $mail->Body = from_html($htmlBody);
            $mail->Subject = $emailTemp->subject;
            //Obtiene la direcciones a las que va a notificar
            $to_email_addresses = $this->getEmailAddressesFromBeans($sendToBeans['to'],$onlyPrimaryAddress);
            foreach($to_email_addresses as $email_address){
              if(!empty($email_address)){
                $mail->AddAddress($email_address);
              }
            }
            $success = @$mail->Send();
            return $success;
        }
    }
    /*
     * Obtiene las direcciones correos de las personas a las que se les
     * enviara la notificacion
     * @param $target <array> Contiene la relación de objetivos
     * @return <array> Array que contiene las direcciones de correo
     */
    private function getEmailAddressesFromBeans($targets, $onlyPrimaryAddress = true){
        $email_addresses = array();
        $email_all_addresses = array();
        $sea = new SugarEmailAddress;
        foreach($targets as $bean){
            if($onlyPrimaryAddress)
                $email_addresses[] = $sea->getPrimaryAddress($bean);
            else{
                $email_all_addresses = array_merge($email_all_addresses, $sea->getAddressesForBean($bean,true));
            }
        }
        if(count($email_all_addresses)){
            foreach ($email_all_addresses as $key => $address){
                $email_addresses[] = $address['email_address'];
            }
        }
        return $email_addresses;
    }

    public function getUsersParentsFromBean($bean, $returnFirstUser = false, $createdBy = true, $stopCurrentRole = true){
      global $current_user;
      $users = [];
      $userId = $createdBy ? $bean->created_by : $bean->assigned_user_id;
      $user = BeanFactory::getBean("Users", $userId);
      if($returnFirstUser){
        $users[] = $user;
      }
      if($stopCurrentRole){
        $roleAcl = BeanFactory::newBean("ACLRoles");
        $currentUserRoles = $roleAcl->getUserRoles($current_user->id);
        $userBeanRoles = $roleAcl->getUserRoles($user->id);

        $intersec = array_intersect($currentUserRoles, $userBeanRoles);
        if(count($intersec) == 0){
          $users = $this->getUsersReportsTo($user, $users);
        }
      }
      else{
        $users = $this->getUsersReportsTo($user, $users, false);
      }
      return $users;
    }

    public function getUsersReportsTo($user, &$users, $stopCurrentRole = true)
    {
      global $current_user;
      if($user->reports_to_id){
        $reportsTo = BeanFactory::getBean("Users", $user->reports_to_id);
        $users[] = $reportsTo;

        $roleAcl = BeanFactory::newBean("ACLRoles");
        $currentUserRoles = $roleAcl->getUserRoles($current_user->id);
        $reportsToRoles = $roleAcl->getUserRoles($reportsTo->id);
        $intersec = array_intersect($currentUserRoles, $reportsToRoles);

        if(count($intersec) === 0){
          return $this->getUsersReportsTo($reportsTo, $users, $stopCurrentRole);
        }
        return $users;
      }
      else{
        return $users;
      }
    }
}
