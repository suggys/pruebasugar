({
    extendsFrom: "FilterLayout",
    /**
     * Applies filter on current contexts
     * @param {String} query search string
     * @param {Object} dynamicFilterDef(optional)
     */


    applyFilter: function(query, dynamicFilterDef) {
        var self = this;
        if(this.module === 'GPE_Grupo_empresas'  && app.controller.context.get('module') === 'Accounts'){
            dynamicFilterDef = dynamicFilterDef || [];
            dynamicFilterDef.push({
                estado: "Activo"
            });
        }
        if(this.module === 'LF_Facturas' && app.controller.context.get('module') === 'LF_Facturas'  && this.context.get('relModel') ){
            var filter_ini = '-1';
            var id_fact = this.context.get('relModel').get('id');
            if(this.context.get('relModel').get('accounts_lf_facturas_1accounts_ida') != ''){
                filter_ini = this.context.get('relModel').get('accounts_lf_facturas_1accounts_ida');
            }

            dynamicFilterDef = dynamicFilterDef || [];
            dynamicFilterDef.push({
                accounts_lf_facturas_1accounts_ida:   filter_ini
            });
            dynamicFilterDef.push({
                id: {$not_in : [id_fact]}
            });
        }

        if(this.module === 'LF_Facturas' &&  this.context.get('recLink') === 'lf_facturas_lf_facturas'){
            filter_ini = '-1';
            id_fact = this.context.get('recParentModel').get('id');
            if(this.context.get('recParentModel').get('accounts_lf_facturas_1accounts_ida') != ''){
                filter_ini = this.context.get('recParentModel').get('accounts_lf_facturas_1accounts_ida')
            }
            dynamicFilterDef = dynamicFilterDef || [];
            dynamicFilterDef.push({
                accounts_lf_facturas_1accounts_ida:   filter_ini
            });

            dynamicFilterDef.push({
               id: {$not_in : [id_fact]}
           });

        }


        if(
            this.module === 'LF_Facturas'
            && (
                this.context.get('relLink')
                && this.context.get('relLink') === 'lowes_pagos_lf_facturas_1'
            )
        ){
            dynamicFilterDef = dynamicFilterDef || [];
            dynamicFilterDef.push({
                accounts_lf_facturas_1accounts_ida: this.context.get('relModel').get('lowes_pagos_accountsaccounts_ida')
            });
            dynamicFilterDef.push({
                tipo_documento: 'nota_de_cargo'
            });
            dynamicFilterDef.push({
                lowes_pagos_lf_facturas_1lowes_pagos_ida: {
                    $is_null:true
                }
            });

        }

        if(this.module === 'NC_Notas_credito'  && app.controller.context.get('module') === 'LF_Facturas'){
            var id_cta = self.context.get('relModel').get('accounts_lf_facturas_1accounts_ida');
            dynamicFilterDef = dynamicFilterDef || [];
            dynamicFilterDef.push({
                nc_notas_credito_accountsaccounts_ida: id_cta
            });
        }

        if(
           this.module === 'LF_Facturas'
           && app.controller.context.get('module') === 'NC_Notas_credito'
           && this.context.get('recParentModel')
        ){
           id_cta = self.context.get('recParentModel').get('nc_notas_credito_accountsaccounts_ida');
           var id_nota = self.context.get('recParentModel').get('id');
           dynamicFilterDef = dynamicFilterDef || [];
           dynamicFilterDef.push({
               nc_notas_credito_accountsaccounts_ida: id_cta
           });
           dynamicFilterDef.push({
               id: {$not_in : [id_nota]}
           });
           dynamicFilterDef.push({
               accounts_lf_facturas_1accounts_ida : id_cta
           });
        }

        if(
            this.module === 'orden_Ordenes' && app.controller.context.get('module') === 'Accounts'
        ){
            id_cta = this.context.get('recParentModel').id;

            dynamicFilterDef = dynamicFilterDef || [];
            dynamicFilterDef.push({
                accounts_orden_ordenes_1accounts_ida: id_cta
            });
        }

        if(
            this.module === 'orden_Ordenes'
            && app.controller.context.get('module') === 'orden_Ordenes'
            && this.context.get('recParentModel')
        ){
            id_cta = self.context.get('recParentModel').get('accounts_orden_ordenes_1accounts_ida');
            var id_orden = self.context.get('recParentModel').get('id');

            dynamicFilterDef = dynamicFilterDef || [];
            dynamicFilterDef.push({
                accounts_orden_ordenes_1accounts_ida: id_cta
            });

            dynamicFilterDef.push({
                id: {$not_in : [id_orden]}
            });
        }

        if(
            this.module === 'orden_Ordenes'
            && (
                this.context.get('relLink')
                && this.context.get('relLink') === 'orden_ordenes_orden_ordenes_1_right'
            )
        ){
            id_cta = self.context.get('relModel').get('accounts_orden_ordenes_1accounts_ida');
            id_orden = self.context.get('relModel').get('id');

            dynamicFilterDef = dynamicFilterDef || [];
            dynamicFilterDef.push({
                accounts_orden_ordenes_1accounts_ida: id_cta
            });

            dynamicFilterDef.push({
                id: {$not_in : [id_orden]}
            });
        }

        if(
            this.module === 'LF_Facturas'
            && (
              this.context.get('recLink') === 'lowes_pagos_lf_facturas'
              || this.context.get('recLink') === 'nc_notas_credito_lf_facturas_1'
            )
        ){
            dynamicFilterDef = dynamicFilterDef || [];
            dynamicFilterDef.push({
                accounts_lf_facturas_1accounts_ida: this.context.get('recParentModel').get('lowes_pagos_accountsaccounts_ida')
            });

            dynamicFilterDef.push({
                saldo: {
                    $between: [0.01,this.context.get('recParentModel').get('saldo_pendiente_c')]
                }
            });
        }

        if(
          this.module === 'Users'
          && this.context.get('fieldName') === 'user_vendedor_c'
        ){
          dynamicFilterDef = dynamicFilterDef || [];
          dynamicFilterDef.push({
              tipo_usuario_c: 'Vendedor'
          });
        }

        if(this.module === 'Contacts'
          && this.context.get('fieldName') === "nombre_representante_legal_c"){
           dynamicFilterDef = dynamicFilterDef || [];
           dynamicFilterDef.push({
              tipo_c: "representante_legal"
           });
        }

        //En Leads y Prospectos verifica que los contactos no esten asignados a una cuenta o prospecto.
        if(this.module === 'Contacts'
        && this.context.get('fieldName') === 'contacto_c'){
          dynamicFilterDef = dynamicFilterDef || [];
          dynamicFilterDef.push({
            account_id:{$is_null:true},
            prospects_contacts_1prospects_ida:{$is_null:true}
          });
        }

        //En Accounts verifica que los contactos no esten asignados a una cuenta o prospecto.
        if(
          this.module === 'Contacts'
          && (
            this.context.get('recLink')
            && (this.context.get('recLink') === 'contacts' || this.context.get('recLink') === 'prospects_contacts_1')
          )
        ){
          dynamicFilterDef = dynamicFilterDef || [];
          dynamicFilterDef.push({
            account_id:{$is_null:true},
            prospects_contacts_1prospects_ida:{$is_null:true}
          });
        }

        //bug interno 28 donde no se puede asignar un proyecto a otro prospecto o cliente

        if(this.module === "Opportunities"  && this.context.get('fieldName') === 'proyecto_c'){
          dynamicFilterDef = dynamicFilterDef || [];
          dynamicFilterDef.push({
            account_id:{$is_null:true},
            prospects_opportunities_1prospects_ida:{$is_null:true}
          });
        }

        if(
          this.module === "Opportunities"
          && (
            this.context.get('recLink')
            && (this.context.get('recLink') === 'prospects_opportunities_1' || this.context.get('recLink') === 'opportunities')
          )
        ){
          dynamicFilterDef = dynamicFilterDef || [];
          dynamicFilterDef.push({
            account_id:{$is_null:true},
            prospects_opportunities_1prospects_ida:{$is_null:true}
          });
        }

        if(this.module === "Contacts" &&
        this.context.get('recLink') && this.context.get('recLink') === "low01_solicitudescredito_contacts_1"){
          dynamicFilterDef = dynamicFilterDef || [];

          if(this.context.get('recParentModel').get('low01_solicitudescredito_accountsaccounts_ida')){
            dynamicFilterDef.push({
              account_id:this.context.get('recParentModel').get('low01_solicitudescredito_accountsaccounts_ida')
            });
          }

          if(this.context.get('recParentModel').get('low01_solicitudescredito_prospectsprospects_ida')){
            dynamicFilterDef.push({
              prospects_contacts_1prospects_ida:this.context.get('recParentModel').get('low01_solicitudescredito_prospectsprospects_ida')
            });
          }
        }

        var filterOptions = this.context.get('filterOptions') || {};
        if (filterOptions.auto_apply === false) {
            return;
        }

        // to make sure quick filter is handled properly
        if (_.isEmpty(query)) {
            var filterQuicksearchView = this.getComponent('filter-quicksearch');
            query = filterQuicksearchView && filterQuicksearchView.$el.val() || '';
        }

        //If the quicksearch field is not empty, append a remove icon so the user can clear the search easily
        this._toggleClearQuickSearchIcon(!_.isEmpty(query));
        var ctxList = this.getRelevantContextList();

        // Here we split the relevant contexts into two groups, 'count', and
        // 'fetch'. For the 'count' contexts, we do a 'fetchOnlyIds' on their
        // collection so we can update the count and highlight the subpanel
        // icon, even though they are collapsed. For the 'fetch' group, we do a
        // full collection fetch so the subpanel can render its list view.
        var relevantCtx = _.groupBy(ctxList, function(ctx) {
            return ctx.get('collapsed') ? 'count' : 'fetch';
        });

        var batchId = relevantCtx.count && relevantCtx.count.length > 1 ? _.uniqueId() : false;
        _.each(relevantCtx.count, function(ctx) {
            var ctxCollection = ctx.get('collection');
            var origFilterDef = dynamicFilterDef || ctxCollection.origFilterDef || [];
            var filterDef = self.buildFilterDef(origFilterDef, query, ctx);
            var options = {
                //Show alerts for this request
                showAlerts: true,
                apiOptions: {
                    bulk: batchId
                }
            };

            ctxCollection.filterDef = filterDef;
            ctxCollection.origFilterDef = origFilterDef;
            ctxCollection.resetPagination();

            options = _.extend(options, ctx.get('collectionOptions'));
            ctx.resetLoadFlag({recursive:false});
            ctx.set('skipFetch', true);
            ctx.loadData(options);

            // We need to reset twice so we can trigger the other bulk call.
            ctx.resetLoadFlag({recursive:false});
            options.success = _.bind(function(hasAmount, properties) {
                if (!this.disposed) {
                    ctx.trigger('refresh:count', hasAmount, properties);
                }
            }, this);
            ctxCollection.hasAtLeast(ctx.get('limit'), options);
        });

        // FIXME: Filters should not be triggering the bulk request and should
        // be moved to subpanels instead. Will be fixed as part of SC-4533.
        if (batchId) {
            app.api.triggerBulkCall(batchId);
        }

        batchId = relevantCtx.fetch && relevantCtx.fetch.length > 1 ? _.uniqueId() : false;
        _.each(relevantCtx.fetch, function(ctx) {

            var ctxCollection = ctx.get('collection');
            var origFilterDef = dynamicFilterDef || ctxCollection.origFilterDef || [];
            var filterDef = self.buildFilterDef(origFilterDef, query, ctx);
            var options = {
                //Show alerts for this request
                showAlerts: true,
                apiOptions: {
                    bulk: batchId
                },
                success: function() {
                    // Close the preview pane to ensure that the preview
                    // collection is in sync with the list collection.
                    app.events.trigger('preview:close');
                }
            };

            ctxCollection.filterDef = filterDef;
            ctxCollection.origFilterDef = origFilterDef;

            ctx.resetLoadFlag({recursive:false});
            if (!_.isEmpty(ctx._recordListFields)) {
                ctx.set('fields', ctx._recordListFields);
            }
            ctx.set('skipFetch', false);
            ctx.loadData(options);
        });
        if (batchId) {
            app.api.triggerBulkCall(batchId);
        }
    },

})
