<?php
$viewdefs['base']['layout']['dashlet-aapf-fullscreen'] = array(
	'components' => array(
    	array(
            'view' => 'dashlet-aapf',
            'primary' => true,
        ),
    ),
    'type' => 'simple',
    'name' => 'base',
);
