({
    extendsFrom: 'SubpanelLayout',
    initialize: function () {
      this._super('initialize', arguments);
      // debugger;
      if(
        this.options.context.parent.get('module') === "Prospects"
        && _.contains([
          "Calls",
          "Meetings",
          "Tasks",
          "Notes",
          "LOW01_SolicitudesCredito",
          "Contacts",
          "orden_Ordenes",
          "Opportunities",
          "Emails",
        ], this.options.context.get('module'))
      ){
        var parentModel = this.options.context.get('parentModel');
        parentModel.on('sync', _.bind(this._hideControls, this));
      }
    },

    _hideControls: function () {
      var parentModel = this.options.context.get('parentModel');
      if( parentModel.get('estado_c') == "convertido"){
          this.$el.find('.subpanel-controls').hide();
      };
    }
});
