<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

require_once 'include/api/SugarApi.php';
require_once('include/export_utils.php');

class ZipcodeApi extends SugarApi
{

	public function registerApiRest() {
		return array(
            'filterModuleGet' => array(
                'reqType' => 'GET',
                'path' => array('Merxbp','Zipcode',),
                'pathVars' => array('','',),
                'method' => 'Zipcode',
                'shortHelp' => 'Obtiene colonia, ciudad de un CP.',
                'longHelp' => '',
            )
        );
	}

	function Zipcode($api, $args)
	{
        $_URL =  "http://api.merxbp.com/zipcode/";
        if(isset($args['zipcode']))
		{
            $GLOBALS['log']->fatal('Zipcode args::'.print_r($args, 1));

            $ch = curl_init($_URL.$args['zipcode']);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json'
                )
            );
            $result = curl_exec($ch);

            //$GLOBALS['log']->fatal('Zipcode >>> $result:'. print_r($result, 1));

            return json_decode($result);
        } else {
            return json_encode(array('Code'=> 500, 'Msg' => 'invalid parameter'));
        }
	}
}
