<?php

require_once 'custom/modules/Accounts/clients/base/api/CarteraApi.php';

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');



class ConversionApi extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            //GET
            'MyGetEndpoint' => array(
                //request type
                'reqType' => 'GET',
                //set authentication
                'noLoginRequired' => false,
                //endpoint path
                'path' => array('Prospects', 'obtencion',),
                //endpoint variables
                'pathVars' => array('', ''),
                //method to call
                'method' => 'MyGetMethod',
                //short help string to be displayed in the help documentation
                'shortHelp' => 'Obtencion de Prospects',
                //long help to be displayed in the help documentation
                'longHelp' => '',
            ),
            //GETEXPORT
            'exportMyGetEndpoint' => array(
                //request type
                'reqType' => 'GET',
                //set authentication
                'noLoginRequired' => false,
                //endpoint path
                'path' => array('Prospects', 'obtencion','export'),
                //endpoint variables
                'pathVars' => array('','','export'),
                //method to call
                'method' => 'MyGetMethod',
                //short help string to be displayed in the help documentation
                'shortHelp' => 'Exporta Obtencion de Prospects',
                //long help to be displayed in the help documentation
                'longHelp' => '',
                'rawReply' => true,
                'allowDownloadCookie' => true,
            ),
        );
    }

    /**
     * Method to be used for my MyEndpoint/GetExample endpoint
     */
    public function MyGetMethod($api, $args)
    {
        $GLOBALS["log"]->fatal("args:". print_r($args, true));

        if(!isset($args['sucursal_c']))
        {
            return false;
        }

        $result = [
            'users' => []
        ];

        $sucursal=$args['sucursal_c'];
        $f_inicio=$args['fecha_inicio'];
        $f_fin=$args['fecha_fin'];

        if ($sucursal) {
            $data = $this->getData($sucursal, $f_inicio, $f_fin);
            $result['users']=array_values($data);
        }


        if(!empty($args['export'])){
            
            $headers = [
                'id' => 'ID',
                'usuario' => "Usuario",
                'name' => "Nombre",
                'clientes' => "Clientes",
                'creados' => "Pros_Creados",
                "convertidos" => "Pros_Convertidos",
                "porcentaje" => "Porcentaje",
                "estimado" => "Estimado_Ventas",
            ];
            $cartera = new CarteraApi();
            $content = $this->formatDataToCsv($headers, $result['users']);
            //$GLOBALS["log"]->fatal($content);

            return $this->doExport($api, "Convercion_Prospectos_porcentaje", $content);
        }
        
        return $result;
    }

    public function getData($sucursal,$f_inicio,$f_fin){
        
        global $current_user;
        $usuarios = [];
        $prospect = BeanFactory::getBean("Prospects");
        $Query = new SugarQuery();
        $Query->from($prospect, array('team_security'=>false));
        $Query->joinTable('users')->on()->equalsField('prospects.created_by','users.id');
        $Query->joinTable("(SELECT users.id,
        CONCAT_WS(' ', users.first_name, users.last_name) vendedor,
        count(*) convertidos, sum(opc.monto_estimado_c) estimado FROM prospects INNER JOIN users users ON prospects.created_by = users.id INNER JOIN (
        SELECT pros.id
        FROM prospects pros
        INNER JOIN prospects_orden_ordenes_1_c prospects_orden_ordenes_1 ON (pros.id = prospects_orden_ordenes_1.prospects_orden_ordenes_1prospects_ida) AND (prospects_orden_ordenes_1.deleted = 0)
        INNER JOIN orden_ordenes jt0_prospects_orden_ordenes_1 ON (jt0_prospects_orden_ordenes_1.id = prospects_orden_ordenes_1.prospects_orden_ordenes_1orden_ordenes_idb) AND (jt0_prospects_orden_ordenes_1.deleted = 0)
        LEFT JOIN orden_ordenes_cstm jt0_prospects_orden_ordenes_1_cstm ON jt0_prospects_orden_ordenes_1_cstm.id_c = jt0_prospects_orden_ordenes_1.id
        LEFT JOIN prospects_cstm prospects_cstm ON prospects_cstm.id_c = pros.id
        WHERE ((prospects_cstm.sucursal_c = $sucursal) AND (pros.date_entered >= '$f_inicio') AND (pros.date_entered <= '$f_fin')) AND (pros.deleted = 0)
        GROUP BY pros.id ) pros ON pros.id = prospects.id 
        LEFT JOIN prospects_cstm prospects_cstm ON prospects_cstm.id_c = prospects.id 

        join prospects_opportunities_1_c opor on pros.id=opor.prospects_opportunities_1prospects_ida
        join opportunities op on opor.prospects_opportunities_1opportunities_idb=op.id
        join opportunities_cstm opc on op.id= opc.id_c

        WHERE prospects.deleted = 0 GROUP BY users.id)
        ",["alias" => "us"])->on()->equalsField('us.id','users.id');

        $Query->joinTable("( SELECT u.id,u.last_name, count(*) clientes
                            FROM users u
                            INNER JOIN accounts a ON a.assigned_user_id=u.id
                            inner join accounts_cstm ac on a.id=ac.id_c
                            WHERE  u.deleted = 0
                            group by u.id)
        ",["alias" => "usc"])->on()->equalsField('usc.id','users.id');


        $Query->whereRaw("prospects_cstm.sucursal_c = '$sucursal'");
        $Query->whereRaw("prospects.date_entered >= '$f_inicio'");
        $Query->whereRaw("prospects.date_entered <= '$f_fin'");
        $Query->groupBy("users.id");
        $Query->select()->fieldRaw(" users.id,
        users.user_name usuario,
        CONCAT_WS(' ', users.first_name, users.last_name) vendedor,
        usc.clientes,
        count(*) creados,
        us.convertidos, 
        CONCAT_WS(' ',(us.convertidos*100)/count(*), '%') as porcentaje,
        us.estimado"
        );
        //$SugarQuery->select("prospects.last_name");
        $GLOBALS["log"]->fatal("----------Query de dashlet convercion de prospectos----------------");    
        $GLOBALS["log"]->fatal("Query ------>:". $Query->compile() );
        //$GLOBALS["log"]->fatal("--------------------------");
        $usuarios=$Query->execute();
        //$GLOBALS["log"]->fatal($usuarios);


        $prospectos = [];
        // while($pros = $db->fetchByAssoc($result)){
        foreach ($usuarios as $key => $pros) {
            
                $prospectos[$pros['id']] = [
                    'id' => $pros['id'],
                    'usuario' => $pros['usuario'],
                    'name' => $pros['vendedor'],
                    'clientes' => $pros['clientes'],
                    'creados' => $pros['creados'],
                    'convertidos' => $pros['convertidos'],
                    'porcentaje' => $pros['porcentaje'],
                    'estimado' => $pros['estimado'],
                ];
           
        }


        return $prospectos;
    }

    protected function doExport(ServiceBase $api, $filename, $content)
        {
            $api->setHeader("Pragma", "cache");
            $api->setHeader("Content-Type", "application/octet-stream; charset=" . $GLOBALS['locale']->getExportCharset());
            $api->setHeader("Content-Disposition", "attachment; filename={$filename}.csv");
            $api->setHeader("Content-transfer-encoding", "binary");
            $api->setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
            $api->setHeader("Last-Modified", TimeDate::httpTime());
            $api->setHeader("Cache-Control", "post-check=0, pre-check=0");
            return $GLOBALS['locale']->translateCharset(
                $content,
                'UTF-8',
                $GLOBALS['locale']->getExportCharset(),
                false,
                true
            );
        }


    public function formatDataToCsv($headers, $data)
        {
            $delimiter = getDelimiter();
            $result = "\"".implode("\"". $delimiter ."\"", array_values($headers))."\"\r\n";
            foreach ($data as $row) {
                $newRow = [];
                foreach ($headers as $key => $header) {
                    $newRow[] = $row[$key];
                }
                $result .= "\"".implode("\"". $delimiter ."\"", array_values($newRow))."\"\r\n";
            }
            return $result;
        }

}

