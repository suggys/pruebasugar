<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
require_once('include/export_utils.php');
class IncobrablesApi extends SugarApi
{
  public function registerApiRest() {
    return array(
      'filterModuleGet' => array(
        'reqType' => 'GET',
        'path' => array('Accounts','incobrables',),
        'pathVars' => array('','',),
        'method' => 'getIncobrables',
        'shortHelp' => 'Obtiene Cartera clientes.',
        'longHelp' => '',
      ),
      'exportCarteraTienda' => array(
        'reqType' => 'GET',
        'path' => array('Accounts','incobrables','export',),
        'pathVars' => array('','','export',),
        'method' => 'getIncobrables',
        'shortHelp' => 'Exporta Cartera Clientes.',
        'longHelp' => '',
        'rawReply' => true,
        'allowDownloadCookie' => true,
      ),
    );
  }

  public function getIncobrables($api, $args)
  {
    $fechaProyeccion = $args['fecha_proyeccion'];
    $data = $this->getData($fechaProyeccion);
    if(!empty($args['export'])){
      $headers = [
        'sucursal' => 'Sucursal',
        'nombre_razon_social' => 'Nombre_razon_social',
        'vendedor' => 'Vendedor',
        'serie' => 'Serie',
        'folio' => 'Folio',
        'fecha_emision' => 'Fecha_emision',
        'monto' => 'Monto',
        'abono' => 'Nota_de_credito',
        'saldo' => 'Saldo',
        'fecha_vencimiento' => 'Fecha_vencimiento',
        'dias_vencidos' => 'Dias_vencidos',
        'refactura' => 'Refactura',
        'comentarios' => 'Comentarios',
  		];
			$content = $this->formatDataToCsv($headers, $data);
			return $this->doExport($api, "Reserva-de-incobrables", $content);
		}
		else{
      $formatedData = $this->formatData($data);
      return $formatedData;
		}
  }


  public function getData($fechaProyeccion = null)
  {
    global $timedate, $current_user;
    $fecha = new SugarDateTime('now');
    $fechaProyeccionToday = $timedate->tzUser($fecha, $current_user)->setTime(0,0)->asDbDate();
    $fechaProyeccion = $fechaProyeccion ? $fechaProyeccion : $fechaProyeccionToday;
    global $current_user;
    $timezone = new DateTimeZone($current_user->user_preferences['global']['timezone']);
    $date = new DateTime("now", $timezone);
    $timeOffset = $timezone->getOffset($date)/3600;
    $timeZoneFormat = sprintf("%03d:00", $timeOffset);
    $data = [];
    $factura = BeanFactory::getBean("LF_Facturas");
		$sugarQuery = new SugarQuery();
    $sugarQuery->from($factura, array('team_security'=>false));
    $sugarQuery->join("accounts_lf_facturas_1", array('team_security'=>false));
    $sugarQuery->joinTable("users")
      ->on()->equalsField("jt0_accounts_lf_facturas_1.assigned_user_id",  "users.id");
		$sugarQuery->joinTable("(
      SELECT lf_facturas.name refactura, lf_facturas_lf_facturas_c.lf_facturas_lf_facturaslf_facturas_idb
      FROM lf_facturas
      JOIN lf_facturas_lf_facturas_c ON lf_facturas_lf_facturas_c.lf_facturas_lf_facturaslf_facturas_ida = lf_facturas.id
      WHERE lf_facturas.deleted = 0
      AND lf_facturas_lf_facturas_c.deleted = 0
    )", array( 'joinType' => 'LEFT', "alias" => "refacturas"))
      ->on()->equalsField("refacturas.lf_facturas_lf_facturaslf_facturas_idb",  "lf_facturas.id");

    $sugarQuery->joinTable("(
      SELECT lf_facturas.id,
      DATEDIFF('$fechaProyeccion', dt_crt) - accounts_cstm.plazo_pago_autorizado_c dias_vencidos
      FROM lf_facturas
      INNER JOIN accounts_lf_facturas_1_c on lf_facturas.id = accounts_lf_facturas_1_c.accounts_lf_facturas_1lf_facturas_idb and accounts_lf_facturas_1_c.deleted = 0
      INNER JOIN accounts_cstm on accounts_cstm.id_c = accounts_lf_facturas_1_c.accounts_lf_facturas_1accounts_ida and accounts_lf_facturas_1_c.deleted = 0
      INNER JOIN lf_facturas_cstm ON lf_facturas_cstm.id_c = lf_facturas.id
      WHERE lf_facturas.deleted = 0
      AND lf_facturas.saldo > 0
      HAVING dias_vencidos > 120
    )",["alias" => "facturas_proyectadas"])
      ->on()->equalsField("facturas_proyectadas.id",  "lf_facturas.id");

    $sugarQuery->select()->fieldRaw("
      lf_facturas.serie, SUBSTR(no_ticket,2,4) as sucursal,
      CONCAT_WS(' ', users.first_name, users.last_name) vendedor,
      jt0_accounts_lf_facturas_1.id account_id,
      jt0_accounts_lf_facturas_1.name nombre_razon_social,
      lf_facturas.importe monto,
      lf_facturas.abono,
      lf_facturas.saldo,
      lf_facturas.serie,
      lf_facturas.name folio,
      lf_facturas.dt_crt fecha_emision,
      lf_facturas.fecha_vencimiento,
      facturas_proyectadas.dias_vencidos,
      lf_facturas.description comentarios,
      refacturas.refactura"
    );

    /* $sugarQuery->orderByRaw("sucursal ASC, nombre_razon_social ASC", false); */
    
    $sugarQuery->orderByRaw("sucursal ASC, nombre_razon_social ", false); 

    $GLOBALS["log"]->fatal("Query:". $sugarQuery->compile() );

    $data = $sugarQuery->execute();
    
   // $GLOBALS["log"]->fatal("data:".print_r($data, 1));

    for ($item=0; $item < count($data) ; $item++) {
      $date = new SugarDateTime($data[$item]['fecha_emision']);
      $data[$item]['fecha_emision'] = $timedate->tzUser($date, $current_user)->asDbDate();
    }
    // $GLOBALS['log']->fatal($sugarQuery->compileSql());
    return $data;
  }

  public function formatData($data)
  {
    $formatedData = [];
    $sucursales = [];
    foreach ($data as $itemData) {

      if(!isset($sucursales[$itemData['sucursal']])){
        $sucursales[$itemData['sucursal']] = [
          "sucursal" => $itemData['sucursal'],
          "saldo" => $itemData['saldo'],
          "accounts" => [],
          "accounts_aux" => [],
        ];
      }

      if(!isset($sucursales[$itemData['sucursal']]['accounts_aux'][$itemData['account_id']])){
        $sucursales[$itemData['sucursal']]['accounts_aux'][$itemData['account_id']] = [
          "id" => $itemData['account_id'],
          "name" => $itemData['nombre_razon_social'],
          "saldo" => 0,
          "facturas" =>[]
        ];
      }

      $sucursales[$itemData['sucursal']]['accounts_aux'][$itemData['account_id']]["saldo"] += $itemData['saldo'];
      $facturaData = $itemData;
      unset($facturaData['sucursal']);
      unset($facturaData['account_id']);
      unset($facturaData['nombre_razon_social']);
      unset($facturaData['vendedor']);
      $sucursales[$itemData['sucursal']]['accounts_aux'][$itemData['account_id']]["facturas"] []= $facturaData;
      $sucursales[$itemData['sucursal']]['accounts'] = array_values($sucursales[$itemData['sucursal']]['accounts_aux']);
    }

    $formatedData = array_values($sucursales);
    return $formatedData;
  }

  public function formatDataToCsv($headers, $data)
	{
		$delimiter = getDelimiter();
		$result = "\"".implode("\"". $delimiter ."\"", array_values($headers))."\"\r\n";
 		foreach ($data as $row) {
			$newRow = [];
			foreach ($headers as $key => $header) {
				$newRow[] = $row[$key];
			}
			$result .= "\"".implode("\"". $delimiter ."\"", array_values($newRow))."\"\r\n";
		}
		return $result;
	}

  protected function doExport(ServiceBase $api, $filename, $content)
	{
		$api->setHeader("Pragma", "cache");
		$api->setHeader("Content-Type", "application/octet-stream; charset=" . $GLOBALS['locale']->getExportCharset());
		$api->setHeader("Content-Disposition", "attachment; filename={$filename}.csv");
		$api->setHeader("Content-transfer-encoding", "binary");
		$api->setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
		$api->setHeader("Last-Modified", TimeDate::httpTime());
		$api->setHeader("Cache-Control", "post-check=0, pre-check=0");
		return $GLOBALS['locale']->translateCharset(
				$content,
				'UTF-8',
				$GLOBALS['locale']->getExportCharset(),
				false,
				true
		);
	}
}
