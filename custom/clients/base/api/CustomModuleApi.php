<?php
class CustomModuleApi extends ModuleApi{

  public function createRecord(ServiceBase $api, array $args)
  {
      if(
        $args['module'] === "orden_Ordenes"
        && empty($args['id_customer_c'])
        && !empty($args['id_customer'])
      ){
        $args['id_customer_c'] = $args['id_customer'];
        $args['transactiontypecode_c'] = $args['TransactionTypeCode_c'];
        switch ($args['transactiontypecode_c']) {
          case '1':
          case 1:
              $args['estado_c'] = '';
            break;
          case '2':
          case 2:
            $args['estado_c'] = '6';
            break;
          case '23':
          case 23:
              $args['estado_c'] = '0';
            break;
          case '26':
          case 26:
              // if($args['sub_total_c'] < 0){
              //   $args['estado_c'] = '8';
              // }
              // else{
                $args['estado_c'] = '2';
              // }
            break;
          case '24':
          case 24:
              // if($args['sub_total_c'] < 0){
              //   $args['estado_c'] = '9';
              // }
              // else{
                $args['estado_c'] = '5';
              // }
            break;
          case '25':
          case 25:
              $args['estado_c'] = '4';
            break;
        }
      }
      $bean = $this->createBean($api, $args);
      $data = $this->formatBeanAfterSave($api, $args, $bean);
      return $data;
  }

  public function retrieveRecord($api, $args) {
      $this->requireArgs($args,array('module','record'));

      $bean = $this->loadBean($api, $args, 'view');

      // formatBean is soft on view so that creates without view access will still work
      if (!$bean->ACLAccess('view', $this->aclCheckOptions)) {
          throw new SugarApiExceptionNotAuthorized('SUGAR_API_EXCEPTION_RECORD_NOT_AUTHORIZED',array('view'));
      }

      $api->action = 'view';
      $data = $this->formatBean($api, $args, $bean);

      if($args['module'] === "LOW01_SolicitudesCredito"){
        if(!empty($data['low01_solicitudescredito_prospectsprospects_ida'])){
          $prospecto = BeanFactory::getBean('Prospects', $data['low01_solicitudescredito_prospectsprospects_ida']);
          $data['low01_solicitudescredito_prospects_name'] = $prospecto->name;
        }
      }

      return $data;

  }
}
?>
