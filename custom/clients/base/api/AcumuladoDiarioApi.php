<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
require_once('include/export_utils.php');
class AcumuladoDiarioApi extends SugarApi
{
  public function registerApiRest() {
    return array(
      'getAcumuladoDiario' => array(
        'reqType' => 'GET',
        'path' => array('Accounts','acumulado-diario',),
        'pathVars' => array('','',),
        'method' => 'getAcumuladoDiario',
        'shortHelp' => 'Obtiene Acumulado Diario.',
        'longHelp' => '',
      ),
      'exportAcumuladoDiario' => array(
        'reqType' => 'GET',
        'path' => array('Accounts','acumulado-diario','export',),
        'pathVars' => array('','','export',),
        'method' => 'getAcumuladoDiario',
        'shortHelp' => 'Exporta Acumulado Diario.',
        'longHelp' => '',
        'rawReply' => true,
        'allowDownloadCookie' => true,
      ),
    );
  }

  public function getAcumuladoDiario($api, $args)
  {
    global $timedate, $current_user;
    $sucursal = $args['sucursal'];

    $fechaFinAux = new SugarDateTime('now');
    $fechaFinAux = $fechaFinAux->add(new DateInterval('P1D'));

    $fechaInicio = $args['fecha_inicio'] ? new SugarDateTime($args['fecha_inicio']) :  new SugarDateTime('now');
    $fechaFin = $args['fecha_fin'] ? new SugarDateTime($args['fecha_fin']) : $fechaFinAux;
    $data = array_values($this->getData($sucursal, $fechaInicio, $fechaFin));
    if(!empty($args['export'])){
      $headers = [
        'fecha' => 'fecha',
        'venta' => 'venta',
        'devolucion' => 'devolucion',
        'pago' => 'pago',
        'total_diario' => 'total_diario',
        'acumulado_total' => 'acumulado_total',
  		];
			$content = $this->formatDataToCsv($headers, $data);
			return $this->doExport($api, "acumulado_diario", $content);
		}
		else{
      $formatedData = $data;
      return $formatedData;
		}
  }


  public function getOrdenes($sucursal = null, $fechaInicio, $fechaFin)
  {
    $ordenes = [];
    $bean = BeanFactory::getBean("orden_Ordenes");
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $accounts = $sugarQuery->join('accounts_orden_ordenes_1', array('team_security'=>false))->joinName();
    $sugarQuery->where()
      ->in("estado_c", ["","2","4","5","6","8","9"])
      ->dateBetween('fecha_transaccion_c',array($fechaInicio,$fechaFin));

    if(!empty($sucursal)){
      $sugarQuery->where()
        ->equals("$accounts.id_sucursal_c", $sucursal);
    }
    $sugarQuery->select()->fieldRaw(<<<'SQL'
      DATE_FORMAT(fecha_transaccion_c, "%Y-%m-%d") fecha_transaccion,
      estado_c,
      SUM(amount_c) amount,
      SUM(sub_total_c) subtotal
SQL
);
    $sugarQuery->groupByRaw('1, estado_c');

    $ordenes = $sugarQuery->execute();
    $ordenesAux = [];
    foreach ($ordenes as $orden) {
      if(!empty($orden['fecha_transaccion'])){
        $ordenesAux[] = $orden;
      }
    }
    // $GLOBALS["log"]->fatal("ordenes:". print_r($ordenes, 1));
    return $ordenesAux;
  }

  public function getPagos($sucursal = null, $fechaInicio, $fechaFin)
  {
    $pagos = [];
    $bean = BeanFactory::getBean("lowes_Pagos");
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $accounts = $sugarQuery->join('lowes_pagos_accounts', array('team_security'=>false))->joinName();
    $sugarQuery->where()
      ->dateBetween('fecha_transaccion',array($fechaInicio,$fechaFin));

    if(!empty($sucursal)){
      $sugarQuery->where()
        ->equals("$accounts.id_sucursal_c", $sucursal);
    }
    $sugarQuery->select()->fieldRaw('
      DATE_FORMAT(fecha_transaccion, "%Y-%m-%d") fecha_transaccion,
      SUM(monto) monto');
    $sugarQuery->groupByRaw('1');

    $pagos = $sugarQuery->execute();
    // $GLOBALS["log"]->fatal("pagos:". print_r($pagos, 1));
    $pagoAux = [];
    foreach ($pagos as $pago) {
      if(!empty($pago['fecha_transaccion'])){
        $pagoAux[$pago['fecha_transaccion']] = $pago;
      }
    }
    return $pagoAux;
  }

  public function getResumenOrdenesSumarizadas($resumenOrdenes)
  {
    $ordenesSumarizadas = [];
    foreach ($resumenOrdenes as $resumenData) {
      if(empty($ordenesSumarizadas[$resumenData['fecha_transaccion']])){
        $ordenesSumarizadas[$resumenData['fecha_transaccion']] = [
          "venta" => 0,
          "devolucion" => 0,
        ];
      }
      switch ($resumenData['estado_c']) {
        case '':
        case '2':
        case '5':
            $ordenesSumarizadas[$resumenData['fecha_transaccion']]["venta"] += $resumenData['amount'];
          break;
        case '4':
            $ordenesSumarizadas[$resumenData['fecha_transaccion']]["devolucion"] -= $resumenData['amount'];
          break;
        case '6':
            $ordenesSumarizadas[$resumenData['fecha_transaccion']]["devolucion"] += ($resumenData['amount']);
          break;
        case '8':
        case '9':
            $ordenesSumarizadas[$resumenData['fecha_transaccion']]["devolucion"] += ($resumenData['subtotal']);
          break;
        default:
          # code...
          break;
      }

    }
    return $ordenesSumarizadas;
  }

  public function getData($sucursal = null, $fechaInicio, $fechaFin)
  {
    // $GLOBALS["log"]->fatal("getData:fechaInicio:".$fechaInicio);
    // $GLOBALS["log"]->fatal("getData:fechaFin:".$fechaFin);
    $data = $this->getContainerDays($fechaInicio, $fechaFin);
    $resumenOrdenes = $this->getOrdenes($sucursal, $fechaInicio, $fechaFin);
    $ordenesSumarizadas = $this->getResumenOrdenesSumarizadas($resumenOrdenes);
    $resumenPagos = $this->getPagos($sucursal, $fechaInicio, $fechaFin);
    // $GLOBALS["log"]->fatal("data:". print_r($data, 1));
    // $GLOBALS["log"]->fatal("resumenPagos:". print_r($resumenPagos, 1));
    $acumuladoTotal = 0;
    foreach ($data as $key => $dataItem) {
      if(!empty($ordenesSumarizadas[$key])){
        $resumen = $ordenesSumarizadas[$key];
        $data[$key]["venta"] += $resumen['venta'];
        $data[$key]["devolucion"] += $resumen['devolucion'];
      }

      if(!empty($resumenPagos[$key])){
        $data[$key]["pago"] += $resumenPagos[$key]['monto']*-1;
      }

      $diff = floatval($data[$key]["venta"]) + floatval($data[$key]["devolucion"]) + floatval($data[$key]["pago"]);
      $data[$key]["total_diario"] += $diff;
      $acumuladoTotal+= $diff;

      $data[$key]["acumulado_total"] = $acumuladoTotal;
    }
    return $data;
  }


  public function getContainerDays($fechaInicio, $fechaFin)
  {
    $container = [];
    // $GLOBALS["log"]->fatal("getContainerDays:fechaInicio:".$fechaInicio);
    // $GLOBALS["log"]->fatal("getContainerDays:fechaFin:".$fechaFin);
    $fechaInicioClone = clone $fechaInicio;
    $interval = $fechaInicio->diff($fechaFin);
    $diffDays = $interval->format('%a');
    for ($item=0; $item < $diffDays; $item++) {
      $container[$fechaInicioClone->format('Y-m-d')] = [
        "fecha" => $fechaInicioClone->format('Y-m-d'),
        "venta" => 0,
        "devolucion" => 0,
        "pago" => 0,
        "total_diario" => 0,
        "acumulado_total" => 0,
      ];
      $fechaInicioClone->add(new DateInterval('P1D'));
    }
    return $container;
  }

  public function formatData($data)
  {
    $formatedData = [];
    $sucursales = [];
    foreach ($data as $itemData) {

      if(!isset($sucursales[$itemData['sucursal']])){
        $sucursales[$itemData['sucursal']] = [
          "sucursal" => $itemData['sucursal'],
          "saldo" => $itemData['saldo'],
          "accounts" => [],
          "accounts_aux" => [],
        ];
      }

      if(!isset($sucursales[$itemData['sucursal']]['accounts_aux'][$itemData['account_id']])){
        $sucursales[$itemData['sucursal']]['accounts_aux'][$itemData['account_id']] = [
          "id" => $itemData['account_id'],
          "name" => $itemData['nombre_razon_social'],
          "saldo" => 0,
          "facturas" =>[]
        ];
      }

      $sucursales[$itemData['sucursal']]['accounts_aux'][$itemData['account_id']]["saldo"] += $itemData['saldo'];
      $facturaData = $itemData;
      unset($facturaData['sucursal']);
      unset($facturaData['account_id']);
      unset($facturaData['nombre_razon_social']);
      unset($facturaData['vendedor']);
      $sucursales[$itemData['sucursal']]['accounts_aux'][$itemData['account_id']]["facturas"] []= $facturaData;
      $sucursales[$itemData['sucursal']]['accounts'] = array_values($sucursales[$itemData['sucursal']]['accounts_aux']);
    }

    $formatedData = array_values($sucursales);
    return $formatedData;
  }

  public function formatDataToCsv($headers, $data)
	{
		$delimiter = getDelimiter();
		$result = "\"".implode("\"". $delimiter ."\"", array_values($headers))."\"\r\n";
 		foreach ($data as $row) {
			$newRow = [];
			foreach ($headers as $key => $header) {
				$newRow[] = $row[$key];
			}
			$result .= "\"".implode("\"". $delimiter ."\"", array_values($newRow))."\"\r\n";
		}
		return $result;
	}

  protected function doExport(ServiceBase $api, $filename, $content)
  {
    $api->setHeader("Pragma", "cache");
    $api->setHeader("Content-Type", "application/octet-stream; charset=" . $GLOBALS['locale']->getExportCharset());
    $api->setHeader("Content-Disposition", "attachment; filename={$filename}.csv");
    $api->setHeader("Content-transfer-encoding", "binary");
    $api->setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
    $api->setHeader("Last-Modified", TimeDate::httpTime());
    $api->setHeader("Cache-Control", "post-check=0, pre-check=0");
    return $GLOBALS['locale']->translateCharset(
      $content,
      'UTF-8',
      $GLOBALS['locale']->getExportCharset(),
      false,
      true
    );
  }
}
