<?php
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$viewdefs['base']['view']['dashlet-conversion-prospectos'] = array(
    'dashlets' => array(
        array(
            'label' => 'Conversion de Prospectos',
            'description' => 'Obtiene la convercion de prospectos por sucursal y asesor',
            'config' => array(
              'sucursal_c' => '',
            ),
            'filter' => array(
                'module' => array(
                    'Home',
                ),
                'view' => 'record',
            )
        ),
    ),
    'config' => array(
        'fields' => array(
            array(
              'name' => 'sucursal_c',
              'label' => 'Sucursal',
              'type' => 'enum',
              'searchBarThreshold' => "",
              'options' => 'sucursal_c_list',
            ),
            array(
              'name' => 'fecha_inicio',
              'label' => 'Fecha Inicio',
              'type' => 'date',
              'searchBarThreshold' => "",
            ),
             array(
              'name' => 'fecha_fin',
              'label' => 'Fecha Fin',
              'type' => 'date',
              'searchBarThreshold' => "",
            ),
        ),
    ),
);
