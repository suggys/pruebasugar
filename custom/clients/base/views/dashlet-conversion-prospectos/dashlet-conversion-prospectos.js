({
  plugins: ['Dashlet'],

  events:{
    'click button[name=download]': "_downloadCsv"
  },

  loadData: function () {
      var self = this;
      self.sucursal_c = self.settings.get('sucursal_c') ? self.settings.get('sucursal_c') : "";
      this.meta = {
        panels:[
          {
            fields: self.getFields()
          }
        ]
      };
       var url = app.api.buildURL('Prospects/obtencion', 'read', null, {
        sucursal_c:self.sucursal_c,
        fecha_inicio: self.settings.get('fecha_inicio'),
        fecha_fin: self.settings.get('fecha_fin')
      });
      app.api.call('get',url, null ,{
        success:function(data){
          //console.log('estamos en pruebas');
          //console.log(data);
          self.collection=app.data.createBeanCollection("Users", data.users);
          //console.log('Dale --------');
          //console.log(self.collection);
          self.render();
        }
      });
  },

  getFields: function (mode, vigencia) {
    var fields = [
      
      {"name": "usuario","type": "text", "label":"Usuario"},
      {"name": "name","type": "text", "label":"Nombre"},
      {"name": "clientes","type": "text", "label":"No. de clientes"},
      {"name": "creados","type": "text", "label":"No. de creados"},
      {"name": "convertidos","type": "text", "label":"No. de convertidos"},
      {"name": "porcentaje","type": "text", "label":"Porcentaje"},
      {"name": "estimado","type": "text", "label":"Venta Estimada"},
    ];
    return fields;
  },
  
  _downloadCsv: function () {
    var self = this;
    var url = app.api.buildURL('Prospects/obtencion/export', 'read', null, {
      sucursal_c:self.sucursal_c,
      fecha_inicio: self.settings.get('fecha_inicio'),
      fecha_fin: self.settings.get('fecha_fin'),
      force_download:1,
      platform:"base"
    });
    app.api.fileDownload(url, {
        error: function(data) {
            app.error.handleHttpError(data, {});
        }
    }, {iframe: this.$el});
  }
})
