({
    extendsFrom: 'SubpanelListView',
    initialize: function(options) {
        this._super('initialize',[options]);
    },
    _initializeMetadata : function(optionsContext) {
        var tmp = this._super('_initializeMetadata', arguments);
        if( this._hideRowActions(this.options.context.parent.get('module'),this.options.context.get('module')) ) {
            tmp.rowactions.actions = _.without(tmp.rowactions.actions, _.findWhere(tmp.rowactions.actions, {
                type: "unlink-action"
            }));
        }
        if( this._hideEditAction(this.options.context.parent.get('module'),this.options.context.get('module')) ){
           tmp.rowactions.actions = _.without(tmp.rowactions.actions, _.findWhere(tmp.rowactions.actions, {
              acl_action: "edit"
           }));
        }
        return tmp;
    },
    _hideRowActions: function(parentModule,sub_modulo){
        var modulo_lower = sub_modulo.toLowerCase();
        switch(sub_modulo) {
              case 'LOW01_SolicitudesCredito':
                  modulo_lower = 'low01_solicitudescredito_prospects';
                  break;
              case 'Contacts':
                  modulo_lower = 'prospects_contacts_1';
                  break;
              case 'orden_Ordenes':
                  modulo_lower = 'prospects_orden_ordenes_1';
                  break;
          }
        var hideActions = false;

        if( parentModule == "Leads" && (sub_modulo === 'Calls' || sub_modulo === 'Meetings' || sub_modulo === 'Tasks' || sub_modulo === 'Notes')){
            hideActions = true;
            var id_lead = this.options.context.parent.get('modelId');
            var req_lead = app.api.call( 'read','/rest/v10/Leads?filter[][id]=' + id_lead + '&fields=status' );
            setTimeout( function(){
                req_lead.xhr.done( function( data ){
                    if(data.records.length && data.records[0].status == "Converted"){
                        $("div[data-subpanel-link='" + modulo_lower + "']").find('.subpanel-controls').css("display","none");
                    }
                });
            }, 100 );
        }
        return hideActions;
    },
    _hideEditAction: function(parentModule,sub_modulo){
        var modulo_lower = sub_modulo.toLowerCase();
        switch(sub_modulo) {
              case 'LOW01_SolicitudesCredito':
                  modulo_lower = 'low01_solicitudescredito_prospects';
                  break;
              case 'Contacts':
                  modulo_lower = 'prospects_contacts_1';
                  break;
              case 'orden_Ordenes':
                  modulo_lower = 'prospects_orden_ordenes_1';
                  break;
          }
        var hideEditAction = false
        if( parentModule == "Leads" && (sub_modulo === 'Calls' || sub_modulo === 'Meetings' || sub_modulo === 'Tasks' || sub_modulo === 'Notes')){
            hideEditAction = true;
            var id_lead = this.options.context.parent.get('modelId');
            var req_lead = app.api.call('read','/rest/v10/Leads?filter[][id]=' + id_lead + '&fields=status');
            setTimeout( function(){
                req_lead.xhr.done( function( data ){
                    if(data.records.length && data.records[0].status == "Converted"){
                        $("div[data-subpanel-link='" + modulo_lower + "']").find('.subpanel-controls').css("display","none");
                    }
                });
            }, 100 );
        }
    }
})
