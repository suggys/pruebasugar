({
  plugins: ['Dashlet'],

  events:{
    'click button[name=download]': "_downloadCsv"
  },

  loadData: function () {
      var self = this;
      self.sucursal = self.settings.get('sucursal') ? self.settings.get('sucursal') : "";
      self.fecha_inicio = self.settings.get('fecha_inicio') ? self.settings.get('fecha_inicio') : "";
      self.fecha_fin = self.settings.get('fecha_fin') ? self.settings.get('fecha_fin') : "";

      this.meta = {
        panels:[
          {
            fields: self.getMetaFields()
          }
        ]
      };

      app.api.call('get','/rest/v10/Accounts/acumulado-diario?sucursal='+self.sucursal+'&fecha_inicio='+self.fecha_inicio+'&fecha_fin='+self.fecha_fin, null ,{
        success:function(data){
          self.data = data;
          self.collection= app.data.createBeanCollection("Accounts", data);
          self.render();
        }
      });
  },
  getMetaFields: function () {
    fields = [
      {"name": "fecha","type": "date","link": false,  "label":"Fecha"},
      {"name": "venta","type": "currency","label":"Venta","align":"right"},
      {"name": "devolucion","type": "currency","label":"Devoluci\u00f3n","align":"right"},
      {"name": "pago","type": "currency","label":"Pago","align":"right"},
      {"name": "total_diario","type": "currency","label":"Total Diario","align":"right"},
      {"name": "acumulado_total","type": "currency","label":"Acumulado Total","align":"right"},
    ];

    return fields;
  },
  _downloadCsv: function () {
    var self = this;
    self.sucursal = self.settings.get('sucursal') ? self.settings.get('sucursal') : "";
    self.fecha_inicio = self.settings.get('fecha_inicio') ? self.settings.get('fecha_inicio') : "";
    self.fecha_fin = self.settings.get('fecha_fin') ? self.settings.get('fecha_fin') : "";
    app.api.fileDownload('/rest/v10/Accounts/acumulado-diario/export?sucursal='+self.sucursal+'&fecha_inicio='+self.fecha_inicio+'&fecha_fin='+self.fecha_fin+'&force_download=1&platform=base', {
      error: function(data) {
        app.error.handleHttpError(data, {});
      }
    }, {iframe: this.$el});
  }

})
