<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$viewdefs['base']['view']['dashlet-acumulado-diario'] = array(
    'dashlets' => array(
        array(
            'label' => 'Reporte acumulado diario AR',
            'description' => 'Reporte acumulado diario AR',
                'config' => array(
                  'sucursal' => '',
                  'fecha_inicio' => '',
                  'fecha_fin' => '',
                ),
                'preview' => array(),
                'filter' => array(
                  'module' => array(
                      'Home',
                  ),
                  'view' => 'record',
                ),
        ),
    ),
    'config' => array(
        'fields' => array(
            array(
                'name' => 'sucursal',
                'label' => 'Sucursal',
                'type' => 'enum',
                'searchBarThreshold' => "",
                'options' => 'sucursal_c_list',
            ),
            array(
                'name' => 'fecha_inicio',
                'label' => 'Fecha de inicio',
                'type' => 'date',
                'searchBarThreshold' => "",
            ),
            array(
                'name' => 'fecha_fin',
                'label' => 'Fecha de fin',
                'type' => 'date',
                'searchBarThreshold' => "",
            ),
        ),
    ),
);
