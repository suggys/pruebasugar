<?php
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$viewdefs['base']['view']['dashlet-entrega-orden'] = array(
    'dashlets' => array(
        array(
            'label' => 'Estado de Entrega de Orden',
            'description' => 'Muestra el estado de entrega de las partidas de la orden',
            'config' => array(

            ),
            'filter' => array(
                'module' => array(
                    'orden_Ordenes',
                ),
                'view' => 'record',
            )
        ),
    ),
    'config' => array(
    ),
);
