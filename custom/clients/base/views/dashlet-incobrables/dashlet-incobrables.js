({
  plugins: ['Dashlet'],
  events:{
    'click button[name=download]': "_downloadCsv"
  },

  loadData: function () {
      var self = this;
      self.fecha_proyeccion = self.settings.get('fecha_proyeccion') ? self.settings.get('fecha_proyeccion') : "";
      var date_proyeccion = self.fecha_proyeccion ? new app.date(self.fecha_proyeccion) : new app.date();
      self.fecha_proyeccion_formated = date_proyeccion.format(app.date.getUserDateFormat());
      this.meta = {
        panels:[
          {
            fields: [
              {"name": "serie","type": "text","link": false,  "label":"Serie"},
              {"name": "folio","type": "text","link": false,  "label":"Folio"},
              {"name": "fecha_emision","type": "date","link": false,  "label":"Fecha de Emision"},
              {"name": "monto","type": "currency","link": false,  "label":"Monto"},
              {"name": "abono","type": "currency","link": false,  "label":"Nota de cr\u00E9dito"},
              {"name": "saldo","type": "currency","link": false,  "label":"Saldo"},
              {"name": "fecha_vencimiento","type": "date","link": false,  "label":"Fecha de Vencimiento"},
              {"name": "dias_vencidos","type": "int","link": false,  "label":"Dias Vencidos"},
              {"name": "refactura","type": "text","link": false,  "label":"Refactura"},
              {"name": "comentarios","type": "text","link": false,  "label":"Comentarios"}
            ]
          }
        ]
      };
      app.api.call('get','/rest/v10/Accounts/incobrables?fecha_proyeccion='+self.fecha_proyeccion, {} ,{
        success:function(data){
          self.data = data;
          if(self.data.length){
            var sucursal_list = App.lang.getAppListStrings('sucursal_c_list');
            _.each(self.data, function (sucursal) {
              sucursal.name = sucursal_list[sucursal.sucursal];
              sucursal.saldo = 0;
              _.each(sucursal.accounts, function (account) {
                sucursal.saldo += account.saldo
                account.models = account.facturas;
                account.saldo = app.currency.formatAmount(account.saldo)
              });
              sucursal.saldo = app.currency.formatAmount(sucursal.saldo)
            });
          }

          self.render();
          if(self.data.length){
            this.$el.find('.accordion').collapse();
          }
        }
      });
      // }
  },
  _downloadCsv: function () {
    var self = this;
    self.fecha_proyeccion = self.settings.get('fecha_proyeccion') ? self.settings.get('fecha_proyeccion') : "";
    app.api.fileDownload('/rest/v10/Accounts/incobrables/export?force_download=1&platform=base&fecha_proyeccion='+self.fecha_proyeccion, {
        error: function(data) {
            app.error.handleHttpError(data, {});
        }
    }, {iframe: this.$el});
  }
})
