({
  plugins: ['Dashlet'],

  events:{
    'click button[name=download]': "_downloadCsv"
  },

  loadData: function (options) {
      var self = this;
      self.id_sucursal_c = self.settings.get('id_sucursal_c') ? self.settings.get('id_sucursal_c') : "";
      this.meta = {
        panels:[
          {
            fields: self.getFields()
          }
        ]
      };
      var url = app.api.buildURL('Accounts/pagocliente', 'read', null, {
        id_sucursal_c:self.id_sucursal_c,
        fecha_transaccion_inicio: self.settings.get('fecha_transaccion_inicio'),
        fecha_transaccion_fin: self.settings.get('fecha_transaccion_fin')
      });
      app.api.call('get',url, null ,{
        success:function(data){
          self.collection= app.data.createBeanCollection("Accounts", data.accounts);
          self.render();
        }
      });
  },

  getFields: function (mode, vigencia) {
    var fields = [
      {"name": "name","type": "name", "label":"LBL_NAME"},
      {"name": "monto_pagos","type": "currency", "label":"Monto Pagos"},
      {"name": "modo_pagos","type": "text", "label":"Modo Pagos"},
      {"name": "vigente","type": "currency","label":"Vigente"},
      {"name": "1a30","type": "currency", "label":"1 a 30"},
      {"name": "31a60","type": "currency", "label":"31 a 60"},
      {"name": "61a90","type": "currency", "label":"61 a 90"},
      {"name": "91a120","type": "currency", "label":"91 a 120"},
      {"name": "mas120","type": "currency", "label":"Mas 120"},
    ];
    return fields;
  },
  _downloadCsv: function () {
    var self = this;
    var url = app.api.buildURL('Accounts/pagocliente', 'read', null, {
      id_sucursal_c:self.id_sucursal_c,
      fecha_transaccion_inicio: self.settings.get('fecha_transaccion_inicio'),
      fecha_transaccion_fin: self.settings.get('fecha_transaccion_fin'),
      force_download:1,
      platform:"base"
    });
    app.api.fileDownload(url, {
        error: function(data) {
            app.error.handleHttpError(data, {});
        }
    }, {iframe: this.$el});
  }
})
