({
  plugins: ['Dashlet'],

  events:{
    'click button[name=download]': "_downloadCsv"
  },

  loadData: function () {
      var self = this;
      self.mode = self.settings.get('mode') ? self.settings.get('mode') : "";
      self.vigencia_c = self.settings.get('vigencia_c') ? self.settings.get('vigencia_c') : "";
      self.fecha_proyeccion = self.settings.get('fecha_proyeccion') ? self.settings.get('fecha_proyeccion') : "";
      this.meta = {
        panels:[
          {
            fields: self.getFields(self.mode,self.vigencia_c)
          }
        ]
      };
      app.api.call('get','/rest/v10/Accounts/cartera-tiendas?mode='+self.mode+'&vigencia_c='+self.vigencia_c+'&fecha_proyeccion='+self.fecha_proyeccion, null ,{
        success:function(data){
          self.data = data;
          self.collection= app.data.createBeanCollection("Accounts", data.tiendas);
          self.render();
        }
      });
  },

  getFields: function (mode, vigencia) {
    var fields;
    if(mode === "estatus"){
      fields = [
        {"name": "name","type": "name", "label":"LBL_NAME_TIENDA"},
        {"name": "pendiente_facturar","type": "currency","label":"LBL_CARTERA_PENDIENTE_FACTURAR","align":"right"},
        {"name": "porcentaje_pendiente_facturar","type": "currency","label":"LBL_CARTERA_PORCENTAJE_PENDIENTE_FACTURAR","align":"right"},
        {"name": "vigente","type": "currency","label":"LBL_CARTERA_VIGENTE","align":"right"},
        {"name": "porcentaje_vigente","type": "currency","label":"LBL_CARTERA_PORCENTAJE_VIGENTE","align":"right"},
        {"name": "total_vencido","type": "currency","label":"LBL_CARTERA_VENCIDO","align":"right"},
        {"name": "porcentaje_vencido","type": "text","label":"LBL_CARTERA_PORCENTAJE_VENCIDO","align":"right"},
        {"name": "total","type": "currency","label":"LBL_CARTERA_TOTAL","align":"right"},
        {"name": "limite_credito_autorizado_c","type": "currency","label":"LBL_LIMITE_CREDITO_AUTORIZADO_C","align":"right"},
        {"name": "disponible_excedido","type": "currency","label":"LBL_CARTERA_DISPONIBLE_EXCEDIDO","align":"right"},
      ];
    }
    else{
      if(vigencia){
        fields = [
          {"name": "name","type": "name","label":"LBL_NAME_TIENDA"},
          {"name": "pendiente_facturar","type": "currency","label":"LBL_CARTERA_PENDIENTE_FACTURAR","align":"right"},
          {"name": vigencia,"type": "currency","label":"LBL_CARTERA_"+vigencia.toUpperCase(),"align":"right"},
          {"name": "total_vencido","type": "currency","label":"LBL_CARTERA_VENCIDO","align":"right"},
          {"name": "total","type": "currency","label":"LBL_CARTERA_TOTAL","align":"right"},
        ];
      }
      else{
        fields = [
          {"name": "name","type": "name","label":"LBL_NAME_TIENDA"},
          {"name": "pendiente_facturar","type": "currency","label":"LBL_CARTERA_PENDIENTE_FACTURAR","align":"right"},
          {"name": "vigente","type": "currency","label":"LBL_CARTERA_VIGENTE","align":"right"},
          {"name": "1a15","type": "currency","label":"LBL_CARTERA_1A15","align":"right"},
          {"name": "16a30","type": "currency","label":"LBL_CARTERA_16A30","align":"right"},
          {"name": "31a60","type": "currency","label":"LBL_CARTERA_31A60","align":"right"},
          {"name": "61a90","type": "currency","label":"LBL_CARTERA_61A90","align":"right"},
          {"name": "91a120","type": "currency","label":"LBL_CARTERA_91A120","align":"right"},
          {"name": "mas120","type": "currency","label":"LBL_CARTERA_MAS120","align":"right"},
          {"name": "total_vencido","type": "currency","label":"LBL_CARTERA_VENCIDO","align":"right"},
          {"name": "total","type": "currency","label":"LBL_CARTERA_TOTAL","align":"right"},

        ];
      }
    }
    return fields;
  },
  _downloadCsv: function () {
    var self = this;
    app.api.fileDownload('/rest/v10/Accounts/cartera-tiendas/export?mode='+self.mode+'&vigencia_c='+self.vigencia_c+'&fecha_proyeccion='+self.fecha_proyeccion+'&force_download=1&platform=base', {
        error: function(data) {
            app.error.handleHttpError(data, {});
        }
    }, {iframe: this.$el});
  }
})
