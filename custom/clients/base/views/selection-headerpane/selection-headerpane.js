({
    extendsFrom: 'SelectionHeaderpaneView',
    initialize: function () {
    	this._super('initialize', arguments);
    },
    _renderHtml: function() {
    	var self = this;
    	this.title = !_.isUndefined(this._title) ? this._formatTitle(this._title) : this.title;
        this.meta.fields = _.map(this.meta.fields, function(field) {
            if (field.name === 'title') {
                field['formatted_value'] = this.title || this._formatTitle(field['default_value']);
            }
            return field;
        }, this);
        this._super('_renderHtml', arguments);

        //this._super('_renderHtml');
        this.layout.on('selection:closedrawer:fire', _.once(_.bind(function() {
            this.$el.off();
            app.drawer.close();
            app.drawer.reset();
        }, this)));

        if (this.isMultiLink) {
        	this.layout.off('selection:link:fire');
            this.layout.on('selection:link:fire', function() {
            	if(
                self.module === "LF_Facturas"
                && (
                  self.context.get('recLink') === 'lowes_pagos_lf_facturas'
                  || self.context.get('recLink') === 'nc_notas_credito_lf_facturas_1'
                )
              ){
            		if(self._validateSaldoPendiente()){
            			this.context.trigger('selection-list:link:multi');
            		}
            	}

            	else{
                	this.context.trigger('selection-list:link:multi');
            	}
            });
        }
    },

    _validateSaldoPendiente: function () {
    	var self = this;
    	var saldoTotal = 0;
    	self.context.get('mass_collection').each(function(factura) {
    		saldoTotal += Number(factura.get('saldo'));
    	});
    	var saldoPendiente = Number(self.context.get('recParentModel').get('saldo_pendiente_c'));
    	var result = saldoPendiente >= saldoTotal;
    	if(!result){
        var medio = self.context.get('recLink') === 'lowes_pagos_lf_facturas' ? "del pago" : "de la nota de credito";
    		app.alert.show("facturas_suma_saldo_mayor",{
    			level: 'error',
    			messages: "El saldo de las facturas, " + app.currency.formatAmountLocale(saldoTotal) + ", supera el saldo pendiente "+medio+", " + app.currency.formatAmountLocale(saldoPendiente),
    			autoClose: true
    		});
    	}
   		return result;
    }


})
