({
  plugins: ['Dashlet'],

  events:{
    'click button[name=download]': "_downloadCsv"
  },

  loadData: function () {
    var self = this;
    self.sucursal = self.settings.get('sucursal') ? self.settings.get('sucursal') : "";
    self.codigo_bloqueo_c = self.settings.get('codigo_bloqueo_c') ? self.settings.get('codigo_bloqueo_c') : "";
    self.vigencia_c = self.settings.get('vigencia_c') ? self.settings.get('vigencia_c') : "";
    self.fecha_proyeccion = self.settings.get('fecha_proyeccion') ? self.settings.get('fecha_proyeccion') : "";
    self.sucursal_compra = self.settings.get('sucursal_compra') ? self.settings.get('sucursal_compra') : "";
    this.meta = {
      panels:[
        {
          fields: self.getMetaFieldsByVigencia(self.vigencia_c)
        }
      ]
    };
    this.sucursal_c_list = app.lang.getAppListStrings("sucursal_c_list");
    if(self.sucursal && _.isEmpty(self.sucursal_compra)){
      app.api.call('get','/rest/v10/Accounts/cartera?sucursal='+self.sucursal+'&codigo_bloqueo_c='+self.codigo_bloqueo_c+'&vigencia_c='+self.vigencia_c+'&fecha_proyeccion='+self.fecha_proyeccion, null ,{
        success:function(data){
          self.data = data;
          var accounts = [];
          _.each(data.accounts,function(account){
            account.id_sucursal_c = self.sucursal_c_list[account.id_sucursal_c];
            accounts.push(account);
          });
          data.accounts = accounts;
          self.collection= app.data.createBeanCollection("Accounts", data.accounts);
          self.render();
        }
      });
    } else if (_.isEmpty(self.sucursal) && !_.isEmpty(self.sucursal_compra)) {
      self.sucursal = self.sucursal_compra;
      app.api.call('get','/rest/v10/Accounts/cartera?sucursal_compra='+self.sucursal_compra+'&codigo_bloqueo_c='+self.codigo_bloqueo_c+'&vigencia_c='+self.vigencia_c+'&fecha_proyeccion='+self.fecha_proyeccion, null ,{
        success:function(data){
          self.data = data;
          var accounts = [];
          _.each(data.accounts,function(account){
            account.id_sucursal_c = self.sucursal_c_list[account.id_sucursal_c];
            accounts.push(account);
          });
          data.accounts = accounts;
          self.collection= app.data.createBeanCollection("Accounts", data.accounts);
          self.render();
          // var width = this.meta.panels[0].fields.length*170;
          // self.$el.find('.dashlet-cartera-list table').css({
          //   width: width
          // });
        }
      });
    }
  },

  getMetaFieldsByVigencia: function (vigencia) {
    if(vigencia){
      fields = [
        {"name": "name","type": "name","link": true, "label":"LBL_NAME"},
        {"name": "id_ar_credit_c","type": "text","label":"LBL_ID_AR_CREDIT"},
        {"name": "id_sucursal_c","type": "text","label":"LBL_ID_SUCURSAL"},
        {"name": "user_vendedor_c_full_name","type": "text","label":"LBL_USER_VENDEDOR"},
        {"name": "limite_credito_autorizado_c","type": "currency","label":"L\u00EDmite de Cr\u00E9dito Total Cliente","align":"right"},
        {"name": "credito_disponible_c","type": "currency","label":"Cr\u00E9dito Disponible Total Cliente","align":"right"},
        {"name": "codigo_bloqueo_c","type": "enum","label":"LBL_CODIGO_BLOQUEO_C", "options":"dashlet_codigo_bloqueo_c_list"},
        {"name": "plazo_pago_autorizado_c","type": "int","label":"LBL_PLAZO_PAGO_AUTORIZADO_C","align":"right"},
        {"name": "pendiente_facturar","type": "currency","label":"LBL_CARTERA_PENDIENTE_FACTURAR","align":"right"},
        {"name": vigencia,"type": "currency","label":"LBL_CARTERA_"+vigencia.toUpperCase(),"align":"right"},
        {"name": "total_vencido","type": "currency","label":"LBL_CARTERA_VENCIDO","align":"right"},
        {"name": "porcentaje_vencido","type": "text","label":"LBL_CARTERA_PORCENTAJE_VENCIDO","align":"right"},
        {"name": "impacto_cv","type": "text","label":"LBL_CARTERA_IMPACTO_CV","align":"right"},
        {"name": "impacto_cv_120","type": "text","label":"LBL_CARTERA_IMPACTO_CV_120","align":"right"},
      ];
    }
    else{
      fields = [
        {"name": "name","type": "name","link": true, "label":"LBL_NAME"},
        {"name": "id_ar_credit_c","type": "text","label":"LBL_ID_AR_CREDIT"},
        {"name": "id_sucursal_c","type": "text","label":"LBL_ID_SUCURSAL"},
        {"name": "user_vendedor_c_full_name","type": "text","label":"LBL_USER_VENDEDOR"},
        {"name": "limite_credito_autorizado_c","type": "currency","label":"L\u00EDmite de Cr\u00E9dito Total Cliente","align":"right"},
        {"name": "credito_disponible_c","type": "currency","label":"Cr\u00E9dito Disponible Total Cliente","align":"right"},
        {"name": "codigo_bloqueo_c","type": "enum","label":"LBL_CODIGO_BLOQUEO_C", "options":"dashlet_codigo_bloqueo_c_list"},
        {"name": "plazo_pago_autorizado_c","type": "int","label":"LBL_PLAZO_PAGO_AUTORIZADO_C","align":"right"},
        {"name": "pendiente_facturar","type": "currency","label":"LBL_CARTERA_PENDIENTE_FACTURAR","align":"right"},
        {"name": "vigente","type": "currency","label":"LBL_CARTERA_VIGENTE","align":"right"},
        {"name": "1a15","type": "currency","label":"LBL_CARTERA_1A15","align":"right"},
        {"name": "16a30","type": "currency","label":"LBL_CARTERA_16A30","align":"right"},
        {"name": "31a60","type": "currency","label":"LBL_CARTERA_31A60","align":"right"},
        {"name": "61a90","type": "currency","label":"LBL_CARTERA_61A90","align":"right"},
        {"name": "91a120","type": "currency","label":"LBL_CARTERA_91A120","align":"right"},
        {"name": "mas120","type": "currency","label":"LBL_CARTERA_MAS120","align":"right"},
        {"name": "total","type": "currency","label":"LBL_CARTERA_TOTAL","align":"right"},
        {"name": "total_vencido","type": "currency","label":"LBL_CARTERA_VENCIDO","align":"right"},
        {"name": "porcentaje_vencido","type": "text","label":"LBL_CARTERA_PORCENTAJE_VENCIDO","align":"right"},
        {"name": "impacto_cv","type": "text","label":"LBL_CARTERA_IMPACTO_CV","align":"right"},
        {"name": "impacto_cv_120","type": "text","label":"LBL_CARTERA_IMPACTO_CV_120","align":"right"},
      ];
    }
    return fields;
  },

  _downloadCsv: function () {
    var self = this;
    if (_.isEmpty(self.sucursal_compra)){
      app.api.fileDownload('/rest/v10/Accounts/cartera/export?sucursal='+self.sucursal+'&codigo_bloqueo_c='+self.codigo_bloqueo_c+'&vigencia_c='+self.vigencia_c+'&fecha_proyeccion='+self.fecha_proyeccion+'&force_download=1&platform=base', {
        error: function(data) {
          app.error.handleHttpError(data, {});
        }
      }, {iframe: this.$el});
    } else {
      app.api.fileDownload('/rest/v10/Accounts/cartera/export?sucursal_compra='+self.sucursal_compra+'&codigo_bloqueo_c='+self.codigo_bloqueo_c+'&vigencia_c='+self.vigencia_c+'&fecha_proyeccion='+self.fecha_proyeccion+'&force_download=1&platform=base', {
        error: function(data) {
          app.error.handleHttpError(data, {});
        }
      }, {iframe: this.$el});
    }
  }
})
