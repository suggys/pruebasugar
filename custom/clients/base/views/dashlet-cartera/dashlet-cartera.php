<?php
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$viewdefs['base']['view']['dashlet-cartera'] = array(
    'dashlets' => array(
        array(
            'label' => 'Informe de Cuentas por Cobrar',
            'description' => 'Informe de Cuentas por Cobrar',
            'config' => array(
              'sucursal' => '',
              'codigo_bloqueo_c' => 'TODOS',
              'vigencia_c' => '',
              'fecha_proyeccion' => '',
            ),
            'filter' => array(
                'module' => array(
                    'Home',
                ),
                'view' => 'record',
            )
        ),
    ),
    'config' => array(
        'fields' => array(
            array(
                'name' => 'sucursal',
                'label' => 'Sucursal',
                'type' => 'enum',
                'searchBarThreshold' => "",
                'options' => 'sucursal_c_list',
            ),
            array(
                'name' => 'codigo_bloqueo_c',
                'label' => 'Codigo de bloqueo',
                'type' => 'enum',
                'searchBarThreshold' => "",
                'options' => 'dashlet_codigo_bloqueo_c_list',
            ),
            array(
                'name' => 'vigencia_c',
                'label' => 'Antigüedad',
                'type' => 'enum',
                'searchBarThreshold' => "",
                'options' => 'vigencia_autesp_rango_list',
            ),
            array(
                'name' => 'fecha_proyeccion',
                'label' => 'Fecha de proyeccion',
                'type' => 'date',
                'searchBarThreshold' => "",
                'options' => 'vigencia_autesp_rango_list',
            ),
            array(
                'name' => 'sucursal_compra',
                'label' => 'Sucursal de compra',
                'type' => 'enum',
                'searchBarThreshold' => "",
                'options' => 'sucursal_c_list',
            ),
        ),
    ),
);
