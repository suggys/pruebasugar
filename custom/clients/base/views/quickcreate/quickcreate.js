({
    extendsFrom: "QuickcreateView",
    _renderHtml: function() {
        if (!app.api.isAuthenticated() || app.config.appStatus == 'offline') {
            return;
        }
        // loadAdditionalComponents fires render before the private metadata is ready, check for this
        if (app.isSynced) {
            var modules = _.without(app.metadata.getModuleNames({filter: ['visible', 'quick_create'], access: 'create'}), "Accounts", "Opportunities", "Contacts");
            this.createMenuItems = this._getMenuMeta(modules);
            app.view.View.prototype._renderHtml.call(this);
        }
    },

})
