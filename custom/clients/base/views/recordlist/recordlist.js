({
   extendsFrom: 'RecordlistView',

   initialize: function(options) {
      this._super("initialize", [options]);
   },

   editClicked: function(model, field) {
      if(field.module === "Leads" || field.module === "Prospects"){
         this._canEditRecord(model,field);
      }else {
         this.edit(model,field);
      }
   },

   _canEditRecord: function(model,field){
      var self = this;
      var idRegistro = model.get('id');
      var roles = app.user.get('roles');
      var registro = app.data.createBean(model.get('_module'), {id:idRegistro});
      registro.fetch({
         success: function(bean){
           if(app.user.get('type') != 'admin'){
            if((bean.get('status') === "Converted" || bean.get('estado_c') === "convertido") && !_.contains(roles,"admin")){
               app.alert.show('noEditable',{
                  level: 'error',
                  messages: 'Este registro esta convertido a cliente, por lo que no puede ser editado.',
                  autoClose: false
               });
            }else {
               self.edit(model, field);
            }
          }else {
             self.edit(model, field);
          }
         }
      });
   },

   edit: function(model, field){
      if (!_.isEmpty(model.get('locked_fields'))) {
          this._showLockedFieldWarning(model);
          return;
      }
      if (field.def.full_form) {
          var parentModel = this.context.parent.get('model');
          var link = this.context.get('link');

          // `app.bwc.createRelatedRecord` navigates to the BWC EditView if an
          // id is passed to it.
          app.bwc.createRelatedRecord(this.module, parentModel, link, model.id);
      } else {
          this.toggleRow(model.id, true);
          //check to see if horizontal scrolling needs to be enabled
          this.resize();
      }
      if (!_.isEqual(model.attributes, model._syncedAttributes)) {
          model.setSyncedAttributes(model.attributes);
      }
   },

})
