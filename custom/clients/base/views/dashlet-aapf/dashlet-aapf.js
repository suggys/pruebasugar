({
  plugins: ['Dashlet'],

  events: {
    'click a[name=save_button]:not(.disabled)': "_save",
    'click a[name=close_button]': "_close",
    'click a.full-screen': "_fullScreen",
    'change select[name=tipo_operacion]': "_changeTipoOperacion",
    'click .facturas-container input[name=factura]': "_selectFactura",
    'click .pagos-container input[name=pago]': "_selectPago",
    'input .importe_pago_factura': "_selectPago",
    //'click input[name=factoraje]': "_showFields",
  },

  initialize: function functionName() {
    var self = this;
    this._super("initialize", arguments);
    self.listFacturasTemplate = app.template.getView(self.name+'.list-facturas');
    self.listPagosTemplate = app.template.getView(self.name+'.list-pagos');

    self.facturasCollection = app.data.createBeanCollection('LF_Facturas');
    self.pagosCollection = app.data.createBeanCollection('lowes_Pagos');

    self.dateDef = { hash: { dateOnly: true } };
    self.tipo_operacion = "1facturaNpagos";
    if(self.layout.options.layout.name == "drawer"){
      self.layout.options.layout.on('drawer:resize', _.bind(self._resizeDrawer, self));
    }
  },

  loadData: function (options) {
    var self = this;
    self.render();
    self._getData();
  },

  _getData: function () {
    var self = this;
    self.facturasCollection = app.data.createRelatedCollection(this.model, 'accounts_lf_facturas_1');
    self.facturasCollection.orderBy = {
        field: "fecha_vencimiento",
        direction: "asc"
    };
    self.pagosCollection = app.data.createRelatedCollection(this.model, 'lowes_pagos_accounts');
    async.series([
        function (callback) {
          self.facturasCollection.fetch({
            filter:{
              saldo:{$gt:0}
            },
            limit:300,
            order_by:"fecha_vencimiento:asc",
            success: function () {
              callback(null, arguments[0]);
            }
          });
        },
        function (callback) {
          self._getPagos(function () {
            callback(null, arguments[0]);
          });
        }
      ],
      _.bind(self._processData, self)
    );
  },
  _getPagos: function (callback) {
    var self = this;
    self.pagosCollection.fetch({
      filter:{
        saldo_pendiente_c:{$gt:0},
        cheque_devuelto:0
      },
      limit:150,
      success: callback
    });
  },
  _processData: function () {
    var self = this;
    var tabFacturas = self.$el.find("#tab-facturas");
    tabFacturas.html(self.listFacturasTemplate(self));

    var tabPagos = self.$el.find("#tab-pagos");
    tabPagos.html(self.listPagosTemplate(self));

    self.$el.find('.tabbable').tabs();

    if(self.pagosCollection.models.length && self.facturasCollection.models.length){
      self.$el.find('a[name="save_button"]').removeClass('disabled');
    }
  },

  _validatePagosSelected: function(errors, callback){
    var self = this;
    var pagosSelected = this.$el.find('table.pagos-container tbody tr input:' + (this.tipo_operacion === '1facturaNpagos' ? 'checkbox' : 'radio') +':checked');
    if(pagosSelected.length){
      _.each(pagosSelected, function (input, index) {
        tr = $(input).closest('tr');
        model = self.pagosCollection.get(tr.data('id'));
        pagoMonto = parseFloat(app.utils.unformatNumberStringLocale(tr.find('input:text').val()));

        if(_.isNaN(pagoMonto) || pagoMonto === 0){
          errors["pago_selected"] = errors["pago_selected"] || {};
          errors['pago_selected']['El monto del pago ' + model.get('name') + ' no es valido.'] = true;
        }
        if(pagoMonto <= 0){
          errors["pago_selected"] = errors["pago_selected"] || {};
          errors['pago_selected']['El monto del pago ' + model.get('name') + ' no es valido.'] = true;
        }
        if(pagoMonto > model.get('saldo_pendiente_c') && _.isEmpty(errors['pago_selected'])){
          errors["pago_selected"] = errors["pago_selected"] || {};
          errors['pago_selected']['El monto del pago ' + model.get('name') + ' es mayor a su importe.'] = true;
        }
      });
    }
    else{
      errors["pago_selected"] = errors["pago_selected"] || {};
      errors['pago_selected']['Debe de seleccionar al menos un pago.'] = true;
    }
    callback(null, errors);
  },
  _validatePagosFactura: function(errors, callback){
    var self = this;
    var facturas = self._getFacturasSelected();
    var pagos = self._getPagosSelected();

    if(this.tipo_operacion === '1facturaNpagos'){
      var importe = 0;
      var factura = facturas[0];
      _.map(pagos, function (pago) {
        if(
          parseFloat(factura.get('saldo')) <= parseFloat(pago.get('saldo_pendiente_c'))
          && pagos.length > 1
          && _.isEmpty(errors)
        ){
          debugger;
          errors["pago_selected"] = errors["pago_selected"] || {};
          errors['pago_selected']['Se recomienda saldar la factura con el pago '+pago.get('name')+'.'] = true;
        }
        importe += pago.get('pago_factura_importe');
      });
      importe = Number(App.math.round(importe, 2));
      if(_.isEmpty(errors) && importe > factura.get('saldo')){
        errors["pago_selected"] = errors["pago_selected"] || {};
        errors['pago_selected']['El importe total de los pagos seleccionados es mayor al saldo de la factura.'] = true;
      }
      /*if(_.isEmpty(errors) && importe < factura.get('saldo')){
        errors["pago_selected"] = errors["pago_selected"] || {};
        errors['pago_selected']['Importe insuficiente.'] = true;
      }*/
    }
    else{
      var saldo = 0;
      var pago = pagos[0]
      _.map(facturas, function (factura) {
        saldo+= parseFloat(factura.get('saldo'));
      });
      saldo = Number(App.math.round(saldo,2));
      importe = Number(App.math.round(pago.get('pago_factura_importe'),2));
      /*if(_.isEmpty(errors) &&  importe < saldo){
        errors["pago_selected"] = errors["pago_selected"] || {};
        errors['pago_selected']['Importe insuficiente.'] = true;
      }*/
    }
    callback(null, errors);
  },

  _getFacturasSelected: function () {
    var self = this;
    var models = [];
    var selected = this.$el.find('table.facturas-container tbody tr input:' + (this.tipo_operacion === '1facturaNpagos' ? 'radio' : 'checkbox') +':checked');
    if(selected.length){
      _.each(selected, function (row, index) {
        tr = $(row).closest('tr');
        model = self.facturasCollection.get(tr.data('id'));
        models.push(model);
      });
    }
    return models;
  },

  _getPagosSelected: function () {
    var self = this;
    var models = [];
    var selected = this.$el.find('table.pagos-container tbody tr input:' + (this.tipo_operacion === '1facturaNpagos' ? 'checkbox' : 'radio') +':checked');
    if(selected.length){
      if(selected.length == 1){
        selected = [selected];
      }
      _.each(selected, function (field, index) {
        tr = $(field).closest('tr');
        model = self.pagosCollection.get(tr.data('id'));
        pagoMonto = parseFloat(app.utils.unformatNumberStringLocale(tr.find('input:text').val()));
        model.set('pago_factura_importe', pagoMonto);
        models.push(model);
      });
    }
    return models;
  },

  _save: function () {
    var self = this;
    var errors = {};
    self.$el.find('a[name="save_button"]').addClass('disabled');
    async.series([
        function (callback) {
          self._validatePagosSelected(errors,callback);
        },
        function (callback) {
          self._validatePagosFactura(errors,callback);
        }
      ],
      function () {
        if(_.isEmpty(errors)){
          var facturas = self._getFacturasSelected();
          var pagos = self._getPagosSelected();
          var data = {
            requests:[]
          };
         
          var saldo_pago=0;
          var saldo_factura=0;
          var index = 0;
          var saldo_insoluto = 0.0;
        
          var honorarios =  $("#h1").val() !==" " ?  $("#h1").val() : 0.00; 
          var aforo = $("#a1").val() !==" " ?  $("#a1").val() : 0.00;
          var intereses = $("#i1").val() !==" " ?  $("#i1").val() : 0.00;
          var status = "Sin factoraje";
          var fac=$("#status_fac").val();
          if(fac==="1"){
            status= "Con factoraje";
          }

          console.log("|>.<| hola we"+status);

            _.each(pagos, function(pago) {

              saldo_pago = pago.get('pago_factura_importe');
              
              _.each(facturas, function (factura) {
                
                if(saldo_pago > 0){
                  saldo_factura = factura.get('saldo');

                  if(saldo_pago <= saldo_factura){
                    saldo_factura=saldo_pago;
                    //saldo_pago = 0;
                  }

                }


                if(saldo_pago > 0){

                  index++;
                  //saldo_insoluto = saldo_factura + 1.0;
                  /*
                      Solo vamos a enviar el pago, 
                      cuando el valor del saldo del pago sea mayor que 0
                    */
                  data.requests.push({
                    "url": "v10/Low01_Pagos_Facturas",
                    "method": "POST",
                    "data": JSON.stringify({
                      name: factura.get('name') + '_' + pago.get('name'),
                      low01_pagos_facturas_lf_facturaslf_facturas_ida: factura.id,
                      low01_pagos_facturas_lowes_pagoslowes_pagos_ida: pago.id,
                      importe: saldo_factura,
                      num_pago_c: index,
                      saldo_insoluto_c: saldo_insoluto,
                      honorarios_c: honorarios,
                      aforo_c: aforo,
                      intereses_c: intereses,
                      estatus_fac_c: status
                    })
                  });
                  saldo_pago = saldo_pago - saldo_factura;

                }
              });
            });
          
          app.api.call('create', '/rest/v10/bulk', data, {
            success: _.bind(self._afterSave, self)
          });
        }
        else{
          message = _.keys(errors[_.keys(errors)[0]])[0];
          app.alert.show('pagos_facturas_error', {
            level: "error",
            messages: message
          });
          self.$el.find('a[name="save_button"]').removeClass('disabled');
        }
      }
    );
  },

  _afterSave: function () {
    var self = this;
    app.alert.show("pagos_facturas_success", {
      level: "success",
      messages: "El pago se ha aplicado con exito."
    });
    if(self.layout.options.layout.name == "drawer"){
      self.loadData();
    }
    else{
      this.layout.reloadDashlet();
    }
  },

  _renderHtml: function () {
    this._super("_renderHtml", arguments);
    this.$el.find('select[name=tipo_operacion]').select2({
      width: "100%"
    });
    if(this.context.get('fullScreen')){
      this.$el.find('.full-screen').addClass('hide');
    }
    else{
      this.$el.find('a[name=close_button]').addClass('hide');
    }
    console.log(this.model);
    var fac = this.model.attributes.con_factoraje_c;
    if(fac){
      var sel = document.getElementsByName("tipo_operacion");
      var opt = document.createElement("option");
      var txt = document.createTextNode("Factoraje");
      opt.setAttribute("value","Nfactura1pagosF");
      opt.appendChild(txt);
      sel[0].appendChild(opt);
    }
    $("#fac_fields").hide();
  },

  _changeTipoOperacion: function (event) {
    var self = this;
    this.tipo_operacion = $(event.target).val();
    if(this.tipo_operacion==="Nfactura1pagosF"){
      console.log("entro a mi if");
      this.tipo_operacion = "Nfactura1pagos";
      $("#fac_fields").show();
      $("#status_fac").val("1");
    }else{
      $("#fac_fields").hide();
      $("#status_fac").val("0");
    }
    

    this._processData();
    self.$el.find('.resumen').find('span[data-name=facturas-saldo] .saldo').text("");
    self.$el.find('.resumen').find('span[data-name=pagos-monto] .monto').text("");
  },

  _fullScreen: function () {
    var self = this;
    app.drawer.open({
      layout: 'dashlet-aapf-fullscreen',
      context: {
        model: self.model,
        fullScreen: true
      }
    }, function () {
      self.layout.reloadDashlet();
    });
  },

  _close: function () {
    app.drawer.close();
  },

  _selectFactura: function () {
    var self = this;
    var saldo = 0;
    self.$el.find('.facturas-container input[name=factura]:checked').each(function(index, item){
      saldo += parseFloat(self.facturasCollection.get($(item).closest('tr').data('id')).get('saldo'));
    });
    self.$el.find('.resumen').find('span[data-name=facturas-saldo] .saldo').text(app.currency.formatAmount(saldo));
  },

  _selectPago: function () {
    var self = this;
    var monto = 0;
    self.$el.find('.pagos-container input[name=pago]:checked').each(function(index, item){
      montoFila = $(item).closest('tr').find('input.importe_pago_factura').val() || 0;
      monto += parseFloat(app.currency.unformatAmountLocale(montoFila));
    });
    self.$el.find('.resumen').find('span[data-name=pagos-monto] .monto').text(app.currency.formatAmount(monto));
  },

  _resizeDrawer: function (height) {
    this.$el.find('.tab-pane').height(height-150);
  },

  _dispose: function() {
    var self = this;
    if(self.layout.options.layout.name == "drawer"){
      self.layout.options.layout.off('drawer:resize', _.bind(self._resizeDrawer, self));
    }
    this._super('_dispose', arguments);
  }

})
