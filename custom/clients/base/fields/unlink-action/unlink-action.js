({
    extendsFrom: 'UnlinkActionField',

    initialize: function(options) {
        this._super('initialize', [options]);
    },

    rowActionSelect: function(evt) {
        var self = this;
        var eventName = $(evt.currentTarget).data('event') || this.def.event;

        if (eventName) {
            self._compareAccountsByRFC(function (isValidate) {
              isValidate && self.getTarget().trigger(eventName, self.model, self, evt);
            });
        }
    },

    _compareAccountsByRFC: function(callback) {
      var self = this;
      var url = app.api.buildURL('Accounts', 'read',{}, {filter:[
        {
          gpe_grupo_empresas_accountsgpe_grupo_empresas_ida: self.view.layout.layout.model.get('id'),
          id:{
            $not_equals: self.model.id
          },
          rfc_c: self.model.get('rfc_c')
        }
      ]});
      app.api.call('read', url, {}, {
          success: function(data){
            var isValid = data.records.length === 0
            if(!isValid){
              app.alert.show("accounts_rfc_c_invalid",{
                level: 'error',
                autoClose: false,
                messages: 'No se puede realizar la desvinculaci\u00f3n, ya que el RFC de la cuenta coincide con el RFC de otra cuenta perteneciente a este Grupo de Empresas'
              });
            }
            callback(isValid);
          }
     });

    }

})
