({
    extendsFrom: "RelateField",
    openSelectDrawer: function() {
        var self = this;
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: self.getSearchModule(),
                fields: self.getSearchFields(),
                filterOptions: self.getFilterOptions(),
                relLink: self.def.link,
                relModel: self.model,
                fieldName: self.name,
            }
        }, _.bind(this.setValue, this));
    },

    format: function(value) {

        var parentCtx = this.context && this.context.parent,
            setFromCtx;

        if (value) {
            /**
             * Flag to indicate that the value has been set from the context
             * once, so if later the value is unset, we don't set it again on
             * {@link #format}.
             *
             * @type {boolean}
             * @protected
             */
            this._valueSetOnce = true;
        }
        setFromCtx = value === null && !this._valueSetOnce && parentCtx && _.isEmpty(this.context.get('model').link) &&
            this.view instanceof app.view.views.BaseCreateView &&
            parentCtx.get('module') === this.def.module &&
            this.module !== this.def.module;

        if (setFromCtx) {
            this._valueSetOnce = true;
            var model = parentCtx.get('model');
            // FIXME we need a method to prevent us from doing this
            this.def.auto_populate = true;
            // FIXME the setValue receives a model but not a backbone model...
            this.setValue(model.toJSON());
            // FIXME we need to iterate over the populated_ that is causing
            // unsaved warnings when doing the auto populate.
        }
        if (!this.def.isMultiSelect) {
            this._buildRoute();
        }

        var idList = this.model.get(this.def.id_name);
        if (_.isArray(value)) {
            this.formattedRname = value.join(this._separator);
            this.formattedIds = idList.join(this._separator);
        } else {
            this.formattedRname = value;
            this.formattedIds = idList;
        }

        if(this.name == "low01_solicitudescredito_prospects_name" ){
          if(this.view.currentState == "create"){
            this.formattedRname = this.context.parent.get('model').get('last_name');
            //debugger;
          }
        }
        return value;
    },

    search: _.debounce(function (query) {
        var term = query.term || '',
            self = this, searchCollection,
            searchModule = this.getSearchModule(),
            params = {},
            limit = self.def.limit || 5,
            relatedModuleField = this.getRelatedModuleField();

        searchCollection = query.context || app.data.createBeanCollection(searchModule);

        if (query.context) {
            params.offset = searchCollection.next_offset;
        }

        params.filter = self.buildFilterDefinition(term);
        if(
            this.module === 'Accounts'
            && self.def.link === 'gpe_grupo_empresas_accounts'
        ){
            params.filter.push({
                estado: 'Activo'
            });
        }

        if(
            this.module === 'LF_Facturas'
            && self.def.link === 'nc_notas_credito_lf_facturas_1'
        ){
            params.filter.push({
                nc_notas_credito_accountsaccounts_ida: self.model.get('accounts_lf_facturas_1accounts_ida')
            });
        }


        if(
            this.module === 'LF_Facturas'
            && self.def.link === 'LF_Facturas'
        ){
            params.filter.push({
                accounts_lf_facturas_1accounts_ida: self.model.get('accounts_lf_facturas_1accounts_ida')
            });
        }

        // LOW-184
        if(
            this.module === 'lowes_Pagos'
            && self.def.link === 'lowes_pagos_lf_facturas_1'
        ){
            params.filter.push({
                accounts_lf_facturas_1accounts_ida: self.model.get('lowes_pagos_accountsaccounts_ida')
            });
            params.filter.push({
                tipo_documento: 'nota_de_cargo'
            });
            params.filter.push({
                lowes_pagos_lf_facturas_1lowes_pagos_ida: {
                    $empty:""
                }
            });
        }

        if(
          this.module === 'Accounts'
          && this.name === 'user_vendedor_c'
        ){
          params.filter.push({
              tipo_usuario_c: 'Vendedor'
          });
        }

        if(this.module === "Prospects" && this.name === "nombre_representante_legal_c"){
          params.filter.push({
              tipo_c: 'representante_legal'
          });
        }

        if(this.module === "Prospects" && this.name === "contacto_c"){
          params.filter.push({
            account_id:{$is_null:true},
            prospects_contacts_1prospects_ida:{$is_null:true}
          });
        }

        //bug interno 28 donde no se puede asignar un proyecto a otro prospecto o cliente
        if(this.module === "Prospects" && this.name === "proyecto_c"){
          params.filter.push({
            account_id:{$is_null:true},
            prospects_opportunities_1prospects_ida:{$is_null:true}
          });
        }

        searchCollection.fetch({
            //Don't show alerts for this request
            showAlerts: false,
            update: true,
            remove: _.isUndefined(params.offset),
            fields: this.getSearchFields(),
            context: self,
            params: params,
            limit: limit,
            success: function (data) {
                var fetch = {results: [], more: data.next_offset > 0, context: searchCollection};
                if (fetch.more) {
                    var fieldEl = self.$(self.fieldTag),
                    //For teamset widget, we should specify which index element to be filled in
                        plugin = (fieldEl.length > 1) ? $(fieldEl.get(self.currentIndex)).data("select2") : fieldEl.data("select2"),
                        height = plugin.searchmore.children("li:first").children(":first").outerHeight(),
                    //0.2 makes scroll not to touch the bottom line which avoid fetching next record set
                        maxHeight = height * (limit - .2);
                    plugin.results.css("max-height", maxHeight);
                }
                _.each(data.models, function (model, index) {
                    if (params.offset && index < params.offset) {
                        return;
                    }
                    fetch.results.push({
                        id: model.id,
                        text: model.get(relatedModuleField) + ''
                    });
                });
                query.callback(fetch);
            },
            error: function () {
                query.callback({results: []});
                app.logger.error("Unable to fetch the bean collection.");
            }
        });
    }, app.config.requiredElapsed || 500),
})
