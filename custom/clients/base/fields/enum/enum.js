({
	extendsFrom: 'EnumField',

	initialize: function (argument) {
		this._super('initialize', arguments);
		if (_.isString(this.def.options)) {
			var self = this;
			this.listenTo(this.model, "sync", function(model) {
				var options = app.lang.getAppListStrings(self.def.options);
				if (options) {
					self.items = self._filterOptions(options);
					self.render();
				}
			});
		}
	},

	_filterOptions: function(options) {
		var currentValue, syncedVal, newOptions = {}, filter = app.metadata.getEditableDropdownFilter(this.def.options);
		this.isFiltered = !_.isEmpty(filter);
		if (!this.isFiltered) {
			return options;
		}
		if (!_.contains(this.view.plugins, "Editable")) {
			return options;
		}
		if(this.model){
			syncedVal = this.model.getSynced();
			currentValue = _.isUndefined(syncedVal[this.name]) ? this.model.get(this.name) : syncedVal[this.name];
			if (_.isString(currentValue)) {
				currentValue = [currentValue];
			}
		}
		var currentIndex = {};
		if(this.model){
			if (!this.model.isNew()) {
				_.each(currentValue, function(value) {
					currentIndex[value] = true;
				});
			}
		}
		if (!this._keysOrder) {
			this._keysOrder = {};
		}
		_.each(filter, function(val, index) {
			var key = val[0]
			, visible = val[1];
			if ((visible || key in currentIndex) && !_.isUndefined(options[key]) && options[key] !== false) {
				this._keysOrder[key] = index;
				newOptions[key] = options[key];
			}
		}, this);
		return newOptions;
	},

});
