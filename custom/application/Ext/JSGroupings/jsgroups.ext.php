<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/lowes-helpers.php


//Loop through the groupings to find include/javascript/sugar_grp7.min.js
foreach ($js_groupings as $key => $groupings)
{
    foreach  ($groupings as $file => $target)
    {
        if ($target == 'include/javascript/sugar_grp7.min.js')
        {
            //append the custom helper file
            $js_groupings[$key]['custom/JavaScript/lowes-helpers.js'] = 'include/javascript/sugar_grp7.min.js';
        }

        break;
    }
}

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/lowes-solicitu-credito.php


//creates the file cache/include/javascript/newGrouping.js
$js_groupings[] = $newGrouping = array(
    'custom/include/SolicitudCredito/src/client/public/bundle.js' => 'include/javascript/solicitud_credito.js',
);

?>
<?php
// Merged from custom/Extension/application/Ext/JSGroupings/acl_hide_create.php

foreach ($js_groupings as $key => $groupings) {
    foreach  ($groupings as $file => $target) {
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            $js_groupings[$key]['custom/JavaScript/acl_hide_create.js'] = 'include/javascript/sugar_grp7.min.js';
        }
        break;
    }
}
?>
