<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/4f945b64-7157-11e7-abb4-060b37ffb723/solicitud_credito_estado_list.php

// created: 2017-08-11 02:55:19
$role_dropdown_filters['solicitud_credito_estado_list'] = array (
  'Solicitud_Nueva' => true,
  'Finalizado_por_Solicitante' => false,
  'Aprobado_por_Asesor_Comercial' => false,
  'Rechazado_por_Asesor_Comercial' => false,
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => false,
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => false,
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => false,
  'Pendiente_por_Analista_de_Credito' => false,
  'Aprobado_por_Analista_de_Credito' => false,
  'Rechazado_por_Analista_de_Credito' => false,
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => false,
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => false,
  'Pendiente_por_Agente_Externo' => false,
  'Rechazado_por_Administrador_de_Credito' => false,
  'Rechazado_por_Gerente_de_Credito' => false,
  'Pendiente_por_Director_de_Finanzas' => false,
  'Rechazado_por_Director_de_Finanzas' => false,
  'Pendiente_por_Vicepresidencia_Comercial' => false,
  'Rechazado_por_Vicepresidencia_Comercial' => false,
  'Pendiente_por_Administrador_de_Credito' => false,
  'Aprobacion_por_Administrador_de_Credito' => false,
  'Pendiente_por_Gerente_de_Credito' => false,
  'Aprobacion_por_Gerente_de_Credito' => false,
  'Aprobacion_por_Director_de_Finanzas' => false,
  'Aprobacion_por_Vicepresidencia_Comercial' => false,
);

?>
