<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/7f4570ec-7aa3-11e6-8d90-06d48441b777/account_codigo_bloqueo_c_list.php

// created: 2017-10-06 16:57:49
$role_dropdown_filters['account_codigo_bloqueo_c_list'] = array (
  '' => true,
  'CL' => true,
  'CI' => true,
  'Otro' => true,
  'DI' => false,
  'CV' => false,
  'CD' => false,
  'LE' => false,
);

?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/7f4570ec-7aa3-11e6-8d90-06d48441b777/solicitud_credito_estado_list.php

// created: 2017-08-08 03:36:23
$role_dropdown_filters['solicitud_credito_estado_list'] = array (
  'Solicitud_Nueva' => false,
  'Finalizado_por_Solicitante' => false,
  'Aprobado_por_Asesor_Comercial' => false,
  'Rechazado_por_Asesor_Comercial' => false,
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => false,
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => false,
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => false,
  'Pendiente_por_Analista_de_Credito' => false,
  'Aprobado_por_Analista_de_Credito' => false,
  'Rechazado_por_Analista_de_Credito' => false,
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => false,
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => true,
  'Pendiente_por_Agente_Externo' => false,
  'Pendiente_por_Administrador_de_Credito' => false,
  'Aprobacion_por_Administrador_de_Credito' => true,
  'Rechazado_por_Administrador_de_Credito' => true,
  'Pendiente_por_Gerente_de_Credito' => false,
  'Aprobacion_por_Gerente_de_Credito' => true,
  'Rechazado_por_Gerente_de_Credito' => false,
  'Pendiente_por_Director_de_Finanzas' => false,
  'Aprobacion_por_Director_de_Finanzas' => false,
  'Rechazado_por_Director_de_Finanzas' => false,
  'Pendiente_por_Vicepresidencia_Comercial' => false,
  'Aprobacion_por_Vicepresidencia_Comercial' => false,
  'Rechazado_por_Vicepresidencia_Comercial' => false,
);

?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/7f4570ec-7aa3-11e6-8d90-06d48441b777/estado_autorizacion_list.php

// created: 2017-11-09 00:36:11
$role_dropdown_filters['estado_autorizacion_list'] = array (
  '' => true,
  'pendiente' => true,
  'autorizada' => true,
  'rechazada' => true,
  'finalizada' => false,
  'terminada_incumplida' => false,
  'cancelada_devolucion_cancelacion' => false,
);

?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/7f4570ec-7aa3-11e6-8d90-06d48441b777/account_codigo_bloqueo_c_list.LowesCRM3500.php
 
$role_dropdown_filters['account_codigo_bloqueo_c_list'] = array (
  '' => true,
  'CL' => true,
  'CI' => true,
  'Otro' => true,
  'DI' => false,
  'CV' => false,
  'CD' => false,
  'LE' => false,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/7f4570ec-7aa3-11e6-8d90-06d48441b777/account_codigo_bloqueo_c_list.cstm_names.php
 
$role_dropdown_filters['account_codigo_bloqueo_c_list'] = array (
  '' => true,
  'CL' => true,
  'CI' => true,
  'Otro' => true,
  'DI' => false,
  'CV' => false,
  'CD' => false,
  'LE' => false,
);
?>
<?php
// Merged from custom/Extension/application/Ext/DropdownFilters/roles/7f4570ec-7aa3-11e6-8d90-06d48441b777/account_codigo_bloqueo_c_list.LowesCRM3500.cstm_names.php
 
$role_dropdown_filters['account_codigo_bloqueo_c_list'] = array (
  '' => true,
  'CL' => true,
  'CI' => true,
  'Otro' => true,
  'DI' => false,
  'CV' => false,
  'CD' => false,
  'LE' => false,
);
?>
