<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:03

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => '取引先',
  'Contacts' => '取引先担当者',
  'Opportunities' => '商談',
  'Tasks' => 'タスク',
  'ProductTemplates' => '商品カタログ',
  'Quotes' => '見積',
  'Products' => '見積済商品',
  'Contracts' => '契約',
  'Emails' => 'Eメール',
  'Bugs' => 'バグトラッカー',
  'Project' => 'プロジェクト',
  'ProjectTask' => 'プロジェクトタスク',
  'Prospects' => 'ターゲット',
  'Cases' => 'ケース',
  'Leads' => 'リード',
  'Meetings' => '会議',
  'Calls' => '電話',
  'KBContents' => 'ナレッジベース',
  'RevenueLineItems' => '商談品目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_moduleList.php

 //created: 2016-07-25 21:54:03

$app_list_strings['moduleList']['RevenueLineItems']='商談品目';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:03

$app_list_strings['parent_type_display']=array (
  'Accounts' => '取引先',
  'Contacts' => '取引先担当者',
  'Tasks' => 'タスク',
  'Opportunities' => '商談',
  'Products' => '見積済商品',
  'Quotes' => '見積',
  'Bugs' => 'バグトラッカー',
  'Cases' => 'ケース',
  'Leads' => 'リード',
  'Project' => 'プロジェクト',
  'ProjectTask' => 'プロジェクトタスク',
  'Prospects' => 'ターゲット',
  'KBContents' => 'ナレッジベース',
  'RevenueLineItems' => '商談品目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_record_type_display.php

 // created: 2016-07-25 21:54:04

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => '取引先',
  'Opportunities' => '商談',
  'Cases' => 'ケース',
  'Leads' => 'リード',
  'Contacts' => '取引先担当者',
  'Products' => '見積済商品',
  'Quotes' => '見積',
  'Bugs' => 'バグトラッカー',
  'Project' => 'プロジェクト',
  'Prospects' => 'ターゲット',
  'ProjectTask' => 'プロジェクトタスク',
  'Tasks' => 'タスク',
  'KBContents' => 'ナレッジベース',
  'RevenueLineItems' => '商談品目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = '商品';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'アサイン先';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = '新規';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'アサイン済み';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = '完了';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = '保留中';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = '拒否';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = '複製';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = '高';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = '中';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = '低';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = '受領';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = '複製';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = '完了';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = '期限切れ';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = '無効';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = '商品';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'アサイン先';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = '新規';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'アサイン済み';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = '完了';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = '保留中';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = '拒否';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = '複製';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = '高';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = '中';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = '低';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = '受領';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = '複製';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = '完了';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = '期限切れ';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = '無効';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = '商品';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'アサイン先';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = '新規';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'アサイン済み';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = '完了';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = '保留中';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = '拒否';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = '複製';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = '高';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = '中';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = '低';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = '受領';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = '複製';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = '完了';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = '期限切れ';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = '無効';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administration',
  'Product' => '商品',
  'User' => 'アサイン先',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => '新規',
  'Assigned' => 'アサイン済み',
  'Closed' => '完了',
  'Pending Input' => '保留中',
  'Rejected' => '拒否',
  'Duplicate' => '複製',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => '高',
  'P2' => '中',
  'P3' => '低',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => '受領',
  'Duplicate' => '複製',
  'Closed' => '完了',
  'Out of Date' => '期限切れ',
  'Invalid' => '無効',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.LowesCRM3500.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.LOWESCRM3.506_3.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
