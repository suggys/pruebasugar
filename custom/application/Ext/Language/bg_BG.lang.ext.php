<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:50

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Организация',
  'Contacts' => 'Контакт',
  'Tasks' => 'Задача',
  'Opportunities' => 'Възможност',
  'Products' => 'Офериран продукт',
  'Quotes' => 'Оферта',
  'Bugs' => 'Проблеми',
  'Cases' => 'Казус',
  'Leads' => 'Потенциален клиент',
  'Project' => 'Проекти',
  'ProjectTask' => 'Задача по проект',
  'Prospects' => 'Целеви клиент',
  'KBContents' => 'База от знания',
  'RevenueLineItems' => 'Приходни позиции',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:50

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Организация',
  'Contacts' => 'Контакт',
  'Opportunities' => 'Възможност',
  'Tasks' => 'Задача',
  'ProductTemplates' => 'Продуктов каталог',
  'Quotes' => 'Оферта',
  'Products' => 'Офериран продукт',
  'Contracts' => 'Договор',
  'Emails' => 'Електронна поща',
  'Bugs' => 'Проблем',
  'Project' => 'Проекти',
  'ProjectTask' => 'Задача по проект',
  'Prospects' => 'Целеви клиент',
  'Cases' => 'Казус',
  'Leads' => 'Потенциален клиент',
  'Meetings' => 'Среща',
  'Calls' => 'Обаждане',
  'KBContents' => 'База от знания',
  'RevenueLineItems' => 'Приходни позиции',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.sugar_moduleList.php

 //created: 2016-07-25 21:53:50

$app_list_strings['moduleList']['RevenueLineItems']='Приходни позиции';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.sugar_record_type_display.php

 // created: 2016-07-25 21:53:51

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Организация',
  'Opportunities' => 'Възможност',
  'Cases' => 'Казус',
  'Leads' => 'Потенциален клиент',
  'Contacts' => 'Контакти',
  'Products' => 'Офериран продукт',
  'Quotes' => 'Оферта',
  'Bugs' => 'Проблем',
  'Project' => 'Проекти',
  'Prospects' => 'Целеви клиент',
  'ProjectTask' => 'Задача по проект',
  'Tasks' => 'Задача',
  'KBContents' => 'База от знания',
  'RevenueLineItems' => 'Приходни позиции',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Продукт';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Потребител';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Нов';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Разпределена';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Затворен';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Висящ';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Отхвърлен';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Дублирай';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Висока';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Средна';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Ниска';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Приет';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Дублирай';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Затворен';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'С изтекъл срок';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Невалидни';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Продукт';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Потребител';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Нов';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Разпределена';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Затворен';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Висящ';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Отхвърлен';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Дублирай';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Висока';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Средна';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Ниска';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Приет';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Дублирай';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Затворен';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'С изтекъл срок';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Невалидни';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Продукт';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Потребител';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Нов';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Разпределена';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Затворен';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Висящ';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Отхвърлен';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Дублирай';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Висока';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Средна';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Ниска';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Приет';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Дублирай';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Затворен';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'С изтекъл срок';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Невалидни';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administration',
  'Product' => 'Продукт',
  'User' => 'Потребител',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => 'Нов',
  'Assigned' => 'Разпределена',
  'Closed' => 'Затворен',
  'Pending Input' => 'Висящ',
  'Rejected' => 'Отхвърлен',
  'Duplicate' => 'Дублирай',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => 'Висока',
  'P2' => 'Средна',
  'P3' => 'Ниска',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => 'Приет',
  'Duplicate' => 'Дублирай',
  'Closed' => 'Затворен',
  'Out of Date' => 'С изтекъл срок',
  'Invalid' => 'Невалидни',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.LowesCRM3500.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.LOWESCRM3.506_3.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
