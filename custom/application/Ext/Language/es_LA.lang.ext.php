<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Grupo_empresas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GPE_Grupo_empresas'] = 'Grupo de empresas';
$app_list_strings['moduleListSingular']['GPE_Grupo_empresas'] = 'Grupo de empresa';
$app_list_strings['gpo_empresa_estado_list']['Activo'] = 'Activo';
$app_list_strings['gpo_empresa_estado_list']['Inactivo'] = 'Inactivo';
$app_list_strings['gpo_empresa_estado_list'][''] = '';
$app_list_strings['gpo_emp_edo_list']['Activo'] = 'Activo';
$app_list_strings['gpo_emp_edo_list']['Inactivo'] = 'Inactivo';
$app_list_strings['gpo_emp_edo_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Notas_credito.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['NC_Notas_credito'] = 'Notas de crédito';
$app_list_strings['moduleListSingular']['NC_Notas_credito'] = 'Nota de crédito';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Ordenes.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['orden_Ordenes'] = 'Órdenes';
$app_list_strings['moduleListSingular']['orden_Ordenes'] = 'Orden';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.low_autorizacion_especial.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['moduleListSingular']['lowae_autorizacion_especial'] = 'Autorización Especial';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_codigo_bloqueo_manual_c_list.php

 // created: 2016-09-14 17:13:11

$app_list_strings['account_codigo_bloqueo_manual_c_list']=array (
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_document_garantia_c_list.php

 // created: 2016-09-14 17:43:28

$app_list_strings['document_garantia_c_list']=array (
  'Seleccione' => '-- Seleccione --',
  'Pagare' => 'Pagaré',
  'Contrato' => 'Contrato',
  'Cheque_Postfechado' => 'Cheque Postfechado',
  'Autorizacion' => 'Autorización',
  'Fianza' => 'Fianza',
  'Prendaria' => 'Prendaria',
  'Hipotecaria' => 'Hipotecaria',
  'Anticipo' => 'Anticipo',
  'Otro' => 'Otro (solicitar comentarios)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_estado_segun_cartera_c_list.php

 // created: 2016-09-14 16:18:35

$app_list_strings['account_estado_segun_cartera_c_list']=array (
  'CV' => 'Cartera vigente',
  'CV130D' => 'Cartera vencida 1 a 30 días',
  'CV31120D' => 'Cartera vencida 31 a 120 días',
  'CV120D' => 'Cartera vencida 120 días',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_TransactionTypeCode_list.php

 // created: 2016-10-11 22:23:20

$app_list_strings['TransactionTypeCode_list']=array (
	"0" => "Unknown",
	"1" => "Sale",
	"2" => "Return",
	"3" => "Void",
	"4" => "NoSale",
	"5" => "Exchange",
	"6" => "OpenStore",
	"7" => "CloseStore",
	"8" => "OpenRegister",
	"9" => "CloseRegister",
	"10" => "OpenTill",
	"11" => "CloseTill",
	"12" => "LoanTill",
	"13" => "PickupTill",
	"14" => "SuspendTill",
	"15" => "ResumeTill",
	"16" => "PayinTill",
	"17" => "PayoutTill",
	"18" => "HousePayment",
	"19" => "LayawayInitiate",
	"20" => "LayawayComplete",
	"21" => "LayawayPayment",
	"22" => "LayawayDelete",
	"23" => "OrderInitiate",
	"24" => "OrderComplete",
	"25" => "OrderCancel",
	"26" => "OrderPartial",
	"27" => "BankDepositStore",
	"28" => "InventoryTransferOut",
	"29" => "MandatoryInventoryCount",
	"30" => "VoluntaryInventoryCount",
	"31" => "InventoryReceive",
	"32" => "InventoryReturnSave",
	"33" => "InventoryReturnSeal",
	"34" => "InventoryReturnShip",
	"35" => "Instant Credit Enrollment",
	"36" => "Redeem",
	"37" => "Enter Training Mode",
	"38" => "Exit Training Mode",
	"39" => "Send",
	"40" => "Payroll Payout",
	"41" => "Enter Transaction Reentry",
	"42" => "Exit Transaction Reentry",
	"43" => "Inventory Control Transaction",
	"44" => "Inventory Quantity Adjustment Transaction",
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_document_template_type_dom.php

 // created: 2016-09-21 15:52:48

$app_list_strings['document_template_type_dom']=array (
  '' => '',
  'mercadotecnia' => 'Mercadotecnia',
  'garantia' => 'Garantía',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_estado_factura_list.php

 // created: 2016-11-15 23:45:49

$app_list_strings['estado_factura_list']=array (
  '' => '',
  'aclaracion' => 'Aclaración',
  'renovacion_de_cartera' => 'Renovación de cartera',
  'cobranza_extrajudicial' => 'Cobranza extrajudicial',
  'legal' => 'Legal',
  'perdida' => 'Pérdida',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_document_category_dom.php

 // created: 2016-12-08 15:33:18

$app_list_strings['document_category_dom']=array (
  '' => '',
  'pagare' => 'Pagaré',
  'contrato' => 'Contrato',
  'cheque_postfechado' => 'Cheque Postfechado',
  'autorizacion' => 'Autorización',
  'fianza' => 'Fianza',
  'prendaria' => 'Prendaria',
  'hipotecaria' => 'Hipotecaria',
  'anticipo' => 'Anticipo',
  'carta_incobrabilidad' => 'Carta de incobrabilidad',
  'otro' => 'Otro',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_type_c_list.php

 // created: 2016-09-14 17:58:24

$app_list_strings['account_type_c_list']=array (
  'Fisica' => 'Física',
  'Moral' => 'Moral',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_codigo_bloqueo_auto_c_list.php

 // created: 2016-09-14 17:12:35

$app_list_strings['account_codigo_bloqueo_auto_c_list']=array (
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_estado_bloqueo_c_list.php

 // created: 2016-09-14 17:03:37

$app_list_strings['account_estado_bloqueo_c_list']=array (
  'Desbloqueado' => 'Desbloqueado',
  'Bloqueado' => 'Bloqueado',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_genre_list.php


 // created: 2016-10-10 16:25:50

$app_list_strings['genre_list']=array (
  0 => 'No especificado',
  1 => 'Mujer',
  2 => 'Hombre',

);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_primary_address_country_c_list.php

 // created: 2016-09-29 18:46:07

$app_list_strings['primary_address_country_c_list']=array (
  'US' => 'USA',
  'CA' => 'Canada',
  'FR' => 'France',
  'MX' => 'México',
  'EU' => 'E.U.',
  'DE' => 'Germany',
  'GB' => 'Great Britain',
  'JP' => 'Japan',
  'PR' => 'Puerto Rico',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_tipo_telefono_principal_list.php

 // created: 2016-09-29 18:07:56

$app_list_strings['tipo_telefono_principal_list']=array (
  '' => '',
  1 => 'Casa',
  2 => 'Celular',
  3 => 'Fax',
  4 => 'Page',
  5 => 'Otro',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM.php

 // created: 2017-07-26 15:36:51

$app_list_strings['solicitud_credito_estado_list']=array (
  'Solicitud_Nueva' => 'Solicitud Nueva',
  'Finalizado_por_Solicitante' => 'Finalizado por Solicitante',
  'Aprobado_por_Asesor_Comercial' => 'Aprobado por Asesor Comercial',
  'Rechazado_por_Asesor_Comercial' => 'Rechazado por Asesor Comercial',
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => 'Pendiente por Gerente de Ventas Comerciales',
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => 'Aprobado por Gerente de Ventas Comerciales',
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => 'Rechazado por Gerente de Ventas Comerciales',
  'Pendiente_por_Analista_de_Credito' => 'Pendiente por Analista de Crédito',
  'Aprobado_por_Analista_de_Credito' => 'Aprobado por Analista de Crédito',
  'Rechazado_por_Analista_de_Credito' => 'Rechazado por Analista de Crédito',
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Pendiente de Envío a Agencia Externa por Administrador de Crédito',
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Aprobado Envío a Agencia Externa por Administrador de Crédito',
  'Pendiente_por_Agente_Externo' => 'Pendiente por Agente Externo',
  'Pendiente_por_Administrador_de_Credito' => 'Pendiente por Administrador de Crédito',
  'Aprobacion_por_Administrador_de_Credito' => 'Aprobación por Administrador de Crédito',
  'Rechazado_por_Administrador_de_Credito' => 'Rechazado por Administrador de Crédito',
  'Pendiente_por_Gerente_de_Credito' => 'Pendiente por Gerente de  Crédito',
  'Aprobacion_por_Gerente_de_Credito' => 'Aprobación por Gerente de Crédito',
  'Rechazado_por_Gerente_de_Credito' => 'Rechazado por Gerente de Crédito',
  'Pendiente_por_Director_de_Finanzas' => 'Pendiente por Director de Finanzas',
  'Aprobacion_por_Director_de_Finanzas' => 'Aprobación por Director de Finanzas',
  'Rechazado_por_Director_de_Finanzas' => 'Rechazado por Director de Finanzas',
  'Pendiente_por_Vicepresidencia_Comercial' => 'Pendiente por Vicepresidencia Comercial',
  'Aprobacion_por_Vicepresidencia_Comercial' => 'Aprobación por Vicepresidencia Comercial',
  'Rechazado_por_Vicepresidencia_Comercial' => 'Rechazado por Vicepresidencia Comercial',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);


?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CF_SolicitudCredito_20170726.php

 // created: 2017-07-26 15:36:51

$app_list_strings['solicitud_credito_estado_list']=array (
  'Solicitud_Nueva' => 'Solicitud Nueva',
  'Aprobado_por_Asesor_Comercial' => 'Aprobado por Asesor Comercial',
  'Rechazado_por_Asesor_Comercial' => 'Rechazado por Asesor Comercial',
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => 'Pendiente por Gerente de Ventas Comerciales',
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => 'Aprobado por Gerente de Ventas Comerciales',
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => 'Rechazado por Gerente de Ventas Comerciales',
  'Pendiente_por_Analista_de_Credito' => 'Pendiente por Analista de Crédito',
  'Aprobado_por_Analista_de_Credito' => 'Aprobado por Analista de Crédito',
  'Rechazado_por_Analista_de_Credito' => 'Rechazado por Analista de Crédito',
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Pendiente de Envío a Agencia Externa por Administrador de Crédito',
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Aprobado Envío a Agencia Externa por Administrador de Crédito',
  'Pendiente_por_Agente_Externo' => 'Pendiente por Agente Externo',
  'Pendiente_por_Administrador_de_Credito' => 'Pendiente por Administrador de Crédito',
  'Aprobacion_por_Administrador_de_Credito' => 'Aprobación por Administrador de Crédito',
  'Rechazado_por_Administrador_de_Credito' => 'Rechazado por Administrador de Crédito',
  'Pendiente_por_Gerente_de_Credito' => 'Pendiente por Gerente de  Crédito',
  'Aprobacion_por_Gerente_de_Credito' => 'Aprobación por Gerente de Crédito',
  'Rechazado_por_Gerente_de_Credito' => 'Rechazado por Gerente de Crédito',
  'Pendiente_por_Director_de_Finanzas' => 'Pendiente por Director de Finanzas',
  'Aprobacion_por_Director_de_Finanzas' => 'Aprobación por Director de Finanzas',
  'Rechazado_por_Director_de_Finanzas' => 'Rechazado por Director de Finanzas',
  'Pendiente_por_Vicepresidencia_Comercial' => 'Pendiente por Vicepresidencia Comercial',
  'Aprobacion_por_Vicepresidencia_Comercial' => 'Aprobación por Vicepresidencia Comercial',
  'Rechazado_por_Vicepresidencia_Comercial' => 'Rechazado por Vicepresidencia Comercial',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20171007.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_FRR.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_FRR.php

 // created: 2017-07-26 15:36:51

$app_list_strings['solicitud_credito_estado_list']=array (
  'Solicitud_Nueva' => 'Solicitud Nueva',
  'Finalizado_por_Solicitante' => 'Finalizado por Solicitante',
  'Aprobado_por_Asesor_Comercial' => 'Aprobado por Asesor Comercial',
  'Rechazado_por_Asesor_Comercial' => 'Rechazado por Asesor Comercial',
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => 'Pendiente por Gerente de Ventas Comerciales',
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => 'Aprobado por Gerente de Ventas Comerciales',
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => 'Rechazado por Gerente de Ventas Comerciales',
  'Pendiente_por_Analista_de_Credito' => 'Pendiente por Analista de Crédito',
  'Aprobado_por_Analista_de_Credito' => 'Aprobado por Analista de Crédito',
  'Rechazado_por_Analista_de_Credito' => 'Rechazado por Analista de Crédito',
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Pendiente de Envío a Agencia Externa por Administrador de Crédito',
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Aprobado Envío a Agencia Externa por Administrador de Crédito',
  'Pendiente_por_Agente_Externo' => 'Pendiente por Agente Externo',
  'Pendiente_por_Administrador_de_Credito' => 'Pendiente por Administrador de Crédito',
  'Aprobacion_por_Administrador_de_Credito' => 'Aprobación por Administrador de Crédito',
  'Rechazado_por_Administrador_de_Credito' => 'Rechazado por Administrador de Crédito',
  'Pendiente_por_Gerente_de_Credito' => 'Pendiente por Gerente de  Crédito',
  'Aprobacion_por_Gerente_de_Credito' => 'Aprobación por Gerente de Crédito',
  'Rechazado_por_Gerente_de_Credito' => 'Rechazado por Gerente de Crédito',
  'Pendiente_por_Director_de_Finanzas' => 'Pendiente por Director de Finanzas',
  'Aprobacion_por_Director_de_Finanzas' => 'Aprobación por Director de Finanzas',
  'Rechazado_por_Director_de_Finanzas' => 'Rechazado por Director de Finanzas',
  'Pendiente_por_Vicepresidencia_Comercial' => 'Pendiente por Vicepresidencia Comercial',
  'Aprobacion_por_Vicepresidencia_Comercial' => 'Aprobación por Vicepresidencia Comercial',
  'Rechazado_por_Vicepresidencia_Comercial' => 'Rechazado por Vicepresidencia Comercial',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);


?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_Modules_CRM.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$app_list_strings['moduleList']['LOW02_Partidas_Orden'] = 'Partidas de Orden';
$app_list_strings['moduleListSingular']['LOW02_Partidas_Orden'] = 'Partida de Orden';

$app_list_strings['lo_obras_type_dom']['Analyst'] = 'Analista';
$app_list_strings['lo_obras_type_dom']['Competitor'] = 'Competidor';
$app_list_strings['lo_obras_type_dom']['Customer'] = 'Cliente';
$app_list_strings['lo_obras_type_dom']['Integrator'] = 'Integrador';
$app_list_strings['lo_obras_type_dom']['Investor'] = 'Inversor';
$app_list_strings['lo_obras_type_dom']['Partner'] = 'Socio';
$app_list_strings['lo_obras_type_dom']['Press'] = 'Prensa';
$app_list_strings['lo_obras_type_dom']['Prospect'] = 'Prospecto';
$app_list_strings['lo_obras_type_dom']['Reseller'] = 'Revendedor';
$app_list_strings['lo_obras_type_dom']['Other'] = 'Otro';
$app_list_strings['lo_obras_type_dom'][''] = '';
$app_list_strings['moduleList']['LO_Obras'] = 'Obras';
$app_list_strings['moduleListSingular']['LO_Obras'] = 'Obra';

$app_list_strings['moduleList']['LOW01_SolicitudesCredito'] = 'Solicitudes de Crédito';
$app_list_strings['moduleListSingular']['LOW01_SolicitudesCredito'] = 'Solicitud de Crédito';
$app_list_strings['tipo_de_credito_list']['30_dias_sin_intereses'] = '30 días sin intereses';
$app_list_strings['tipo_de_credito_list']['90_dias_sin_intereses'] = '90 días sin intereses';
$app_list_strings['tipo_de_credito_list'][''] = '';
$app_list_strings['solicitud_credito_domicilio_list']['Propia'] = 'Propia';
$app_list_strings['solicitud_credito_domicilio_list']['Rentada'] = 'Rentada';
$app_list_strings['solicitud_credito_domicilio_list']['HIpotecada'] = 'HIpotecada';
$app_list_strings['solicitud_credito_domicilio_list']['De_Socios'] = 'De Socios';
$app_list_strings['solicitud_credito_domicilio_list'][''] = '';
$app_list_strings['solicitud_credito_giro_list']['Comercio'] = 'Comercio';
$app_list_strings['solicitud_credito_giro_list']['Industria'] = 'Industria';
$app_list_strings['solicitud_credito_giro_list']['Servicio'] = 'Servicio';
$app_list_strings['solicitud_credito_giro_list']['Agropecuario'] = 'Agropecuario';
$app_list_strings['solicitud_credito_giro_list']['Construccion'] = 'Construcción';
$app_list_strings['solicitud_credito_giro_list'][''] = '';
$app_list_strings['solicitud_credito_sector_list']['Publico'] = 'Público';
$app_list_strings['solicitud_credito_sector_list']['Privado'] = 'Privado';
$app_list_strings['solicitud_credito_sector_list'][''] = '';
$app_list_strings['solicitud_credito_caracter_list']['Propietario'] = 'Propietario';
$app_list_strings['solicitud_credito_caracter_list']['Asociado'] = 'Asociado';
$app_list_strings['solicitud_credito_caracter_list']['Empleado'] = 'Empleado';
$app_list_strings['solicitud_credito_caracter_list']['Otro'] = 'Otro';
$app_list_strings['solicitud_credito_caracter_list'][''] = '';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['T_de_C'] = 'T de C';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Debito'] = 'Débito';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Cheques'] = 'Cheques';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Ahorro'] = 'Ahorro';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Inversion'] = 'Inversión';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Otras'] = 'Otras';
$app_list_strings['solicitud_credito_tipo_ref_com_list'][''] = '';
$app_list_strings['recepcion_estado_cuenta_list']['Correo'] = 'Correo';
$app_list_strings['recepcion_estado_cuenta_list']['Correo_electronico'] = 'Correo electrónico';
$app_list_strings['recepcion_estado_cuenta_list'][''] = '';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list']['Correo'] = 'Correo';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list']['Correo_electronico'] = 'Correo electrónico';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list'][''] = '';
$app_list_strings['recepcion_informacion_promo_list']['Si'] = 'Si';
$app_list_strings['recepcion_informacion_promo_list']['No'] = 'No';
$app_list_strings['recepcion_informacion_promo_list'][''] = '';

$app_list_strings['moduleList']['Low03_AgentesExternos'] = 'Agente Externo';
$app_list_strings['moduleListSingular']['Low03_AgentesExternos'] = 'Agente Externo';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20170907.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20170922.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CF_Accounts_tipo_cliente_c.php

$app_list_strings['accounts_tipo_cliente_list']=array (
  '' => '',
  1 => 'Contractor',
  2 => 'Premier',
  3 => 'Reventa',  
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20170828.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CF_lowes_Pagos_konesh.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20171012.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.AgenteExterno.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['Low03_AgentesExternos'] = 'Agente Externo';
$app_list_strings['moduleListSingular']['Low03_AgentesExternos'] = 'Agente Externo';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20170817.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20171020.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20171020.php

 // created: 2017-07-26 15:36:51

$app_list_strings['solicitud_credito_estado_list']=array (
  'Solicitud_Nueva' => 'Solicitud Nueva',
  'Finalizado_por_Solicitante' => 'Finalizado por Solicitante',
  'Aprobado_por_Asesor_Comercial' => 'Aprobado por Asesor Comercial',
  'Rechazado_por_Asesor_Comercial' => 'Rechazado por Asesor Comercial',
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => 'Pendiente por Gerente de Ventas Comerciales',
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => 'Aprobado por Gerente de Ventas Comerciales',
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => 'Rechazado por Gerente de Ventas Comerciales',
  'Pendiente_por_Analista_de_Credito' => 'Pendiente por Analista de Crédito',
  'Aprobado_por_Analista_de_Credito' => 'Aprobado por Analista de Crédito',
  'Rechazado_por_Analista_de_Credito' => 'Rechazado por Analista de Crédito',
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Pendiente de Envío a Agencia Externa por Administrador de Crédito',
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Aprobado Envío a Agencia Externa por Administrador de Crédito',
  'Pendiente_por_Agente_Externo' => 'Pendiente por Agente Externo',
  'Pendiente_por_Administrador_de_Credito' => 'Pendiente por Administrador de Crédito',
  'Aprobacion_por_Administrador_de_Credito' => 'Aprobación por Administrador de Crédito',
  'Rechazado_por_Administrador_de_Credito' => 'Rechazado por Administrador de Crédito',
  'Pendiente_por_Gerente_de_Credito' => 'Pendiente por Gerente de  Crédito',
  'Aprobacion_por_Gerente_de_Credito' => 'Aprobación por Gerente de Crédito',
  'Rechazado_por_Gerente_de_Credito' => 'Rechazado por Gerente de Crédito',
  'Pendiente_por_Director_de_Finanzas' => 'Pendiente por Director de Finanzas',
  'Aprobacion_por_Director_de_Finanzas' => 'Aprobación por Director de Finanzas',
  'Rechazado_por_Director_de_Finanzas' => 'Rechazado por Director de Finanzas',
  'Pendiente_por_Vicepresidencia_Comercial' => 'Pendiente por Vicepresidencia Comercial',
  'Aprobacion_por_Vicepresidencia_Comercial' => 'Aprobación por Vicepresidencia Comercial',
  'Rechazado_por_Vicepresidencia_Comercial' => 'Rechazado por Vicepresidencia Comercial',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);


?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CF_SolicitudCredito_20170728.php

 // created: 2017-07-26 15:36:51

$app_list_strings['solicitud_credito_estado_list']=array (
  'Solicitud_Nueva' => 'Solicitud Nueva',
  'Aprobado_por_Asesor_Comercial' => 'Aprobado por Asesor Comercial',
  'Rechazado_por_Asesor_Comercial' => 'Rechazado por Asesor Comercial',
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => 'Pendiente por Gerente de Ventas Comerciales',
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => 'Aprobado por Gerente de Ventas Comerciales',
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => 'Rechazado por Gerente de Ventas Comerciales',
  'Pendiente_por_Analista_de_Credito' => 'Pendiente por Analista de Crédito',
  'Aprobado_por_Analista_de_Credito' => 'Aprobado por Analista de Crédito',
  'Rechazado_por_Analista_de_Credito' => 'Rechazado por Analista de Crédito',
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Pendiente de Envío a Agencia Externa por Administrador de Crédito',
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Aprobado Envío a Agencia Externa por Administrador de Crédito',
  'Pendiente_por_Agente_Externo' => 'Pendiente por Agente Externo',
  'Pendiente_por_Administrador_de_Credito' => 'Pendiente por Administrador de Crédito',
  'Aprobacion_por_Administrador_de_Credito' => 'Aprobación por Administrador de Crédito',
  'Rechazado_por_Administrador_de_Credito' => 'Rechazado por Administrador de Crédito',
  'Pendiente_por_Gerente_de_Credito' => 'Pendiente por Gerente de  Crédito',
  'Aprobacion_por_Gerente_de_Credito' => 'Aprobación por Gerente de Crédito',
  'Rechazado_por_Gerente_de_Credito' => 'Rechazado por Gerente de Crédito',
  'Pendiente_por_Director_de_Finanzas' => 'Pendiente por Director de Finanzas',
  'Aprobacion_por_Director_de_Finanzas' => 'Aprobación por Director de Finanzas',
  'Rechazado_por_Director_de_Finanzas' => 'Rechazado por Director de Finanzas',
  'Pendiente_por_Vicepresidencia_Comercial' => 'Pendiente por Vicepresidencia Comercial',
  'Aprobacion_por_Vicepresidencia_Comercial' => 'Aprobación por Vicepresidencia Comercial',
  'Rechazado_por_Vicepresidencia_Comercial' => 'Rechazado por Vicepresidencia Comercial',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20171010.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_201710130100.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20171009.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CF_SolicitudCredito_20170810.php

 // created: 2017-07-26 15:36:51

$app_list_strings['solicitud_credito_estado_list']=array (
  'Solicitud_Nueva' => 'Solicitud Nueva',
  'Aprobado_por_Asesor_Comercial' => 'Aprobado por Asesor Comercial',
  'Rechazado_por_Asesor_Comercial' => 'Rechazado por Asesor Comercial',
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => 'Pendiente por Gerente de Ventas Comerciales',
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => 'Aprobado por Gerente de Ventas Comerciales',
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => 'Rechazado por Gerente de Ventas Comerciales',
  'Pendiente_por_Analista_de_Credito' => 'Pendiente por Analista de Crédito',
  'Aprobado_por_Analista_de_Credito' => 'Aprobado por Analista de Crédito',
  'Rechazado_por_Analista_de_Credito' => 'Rechazado por Analista de Crédito',
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Pendiente de Envío a Agencia Externa por Administrador de Crédito',
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Aprobado Envío a Agencia Externa por Administrador de Crédito',
  'Pendiente_por_Agente_Externo' => 'Pendiente por Agente Externo',
  'Pendiente_por_Administrador_de_Credito' => 'Pendiente por Administrador de Crédito',
  'Aprobacion_por_Administrador_de_Credito' => 'Aprobación por Administrador de Crédito',
  'Rechazado_por_Administrador_de_Credito' => 'Rechazado por Administrador de Crédito',
  'Pendiente_por_Gerente_de_Credito' => 'Pendiente por Gerente de  Crédito',
  'Aprobacion_por_Gerente_de_Credito' => 'Aprobación por Gerente de Crédito',
  'Rechazado_por_Gerente_de_Credito' => 'Rechazado por Gerente de Crédito',
  'Pendiente_por_Director_de_Finanzas' => 'Pendiente por Director de Finanzas',
  'Aprobacion_por_Director_de_Finanzas' => 'Aprobación por Director de Finanzas',
  'Rechazado_por_Director_de_Finanzas' => 'Rechazado por Director de Finanzas',
  'Pendiente_por_Vicepresidencia_Comercial' => 'Pendiente por Vicepresidencia Comercial',
  'Aprobacion_por_Vicepresidencia_Comercial' => 'Aprobación por Vicepresidencia Comercial',
  'Rechazado_por_Vicepresidencia_Comercial' => 'Rechazado por Vicepresidencia Comercial',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Configuraciones.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Configuraciones.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Configuraciones.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Configuraciones.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['MRX1_Configuraciones'] = 'Configuraciones';
$app_list_strings['moduleListSingular']['MRX1_Configuraciones'] = 'Configuración';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['MRX1_Configuraciones'] = 'Configuraciones';
$app_list_strings['moduleListSingular']['MRX1_Configuraciones'] = 'Configuración';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['MRX1_Configuraciones'] = 'Configuraciones';
$app_list_strings['moduleListSingular']['MRX1_Configuraciones'] = 'Configuración';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['MRX1_Configuraciones'] = 'Configuraciones';
$app_list_strings['moduleListSingular']['MRX1_Configuraciones'] = 'Configuración';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CF_SolicitudCredito_20170804.php

 // created: 2017-07-26 15:36:51

$app_list_strings['solicitud_credito_estado_list']=array (
  'Solicitud_Nueva' => 'Solicitud Nueva',
  'Aprobado_por_Asesor_Comercial' => 'Aprobado por Asesor Comercial',
  'Rechazado_por_Asesor_Comercial' => 'Rechazado por Asesor Comercial',
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => 'Pendiente por Gerente de Ventas Comerciales',
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => 'Aprobado por Gerente de Ventas Comerciales',
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => 'Rechazado por Gerente de Ventas Comerciales',
  'Pendiente_por_Analista_de_Credito' => 'Pendiente por Analista de Crédito',
  'Aprobado_por_Analista_de_Credito' => 'Aprobado por Analista de Crédito',
  'Rechazado_por_Analista_de_Credito' => 'Rechazado por Analista de Crédito',
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Pendiente de Envío a Agencia Externa por Administrador de Crédito',
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Aprobado Envío a Agencia Externa por Administrador de Crédito',
  'Pendiente_por_Agente_Externo' => 'Pendiente por Agente Externo',
  'Pendiente_por_Administrador_de_Credito' => 'Pendiente por Administrador de Crédito',
  'Aprobacion_por_Administrador_de_Credito' => 'Aprobación por Administrador de Crédito',
  'Rechazado_por_Administrador_de_Credito' => 'Rechazado por Administrador de Crédito',
  'Pendiente_por_Gerente_de_Credito' => 'Pendiente por Gerente de  Crédito',
  'Aprobacion_por_Gerente_de_Credito' => 'Aprobación por Gerente de Crédito',
  'Rechazado_por_Gerente_de_Credito' => 'Rechazado por Gerente de Crédito',
  'Pendiente_por_Director_de_Finanzas' => 'Pendiente por Director de Finanzas',
  'Aprobacion_por_Director_de_Finanzas' => 'Aprobación por Director de Finanzas',
  'Rechazado_por_Director_de_Finanzas' => 'Rechazado por Director de Finanzas',
  'Pendiente_por_Vicepresidencia_Comercial' => 'Pendiente por Vicepresidencia Comercial',
  'Aprobacion_por_Vicepresidencia_Comercial' => 'Aprobación por Vicepresidencia Comercial',
  'Rechazado_por_Vicepresidencia_Comercial' => 'Rechazado por Vicepresidencia Comercial',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM_20170831.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_dashlet_codigo_bloqueo_c_list.php

 // created: 2017-12-08 19:45:02

$app_list_strings['dashlet_codigo_bloqueo_c_list']=array (
  '' => '',
  'CL' => 'Cliente Legal',
  'CI' => 'Cliente Inactivo',
  'DI' => 'Dictamen Incobrable',
  'Otro' => 'Otro',
  'CV' => 'Cartera vencida + x dias',
  'CD' => 'Cheque devuelto',
  'LE' => 'Limite de credito excedido',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.SolicitudCredito.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['LOW01_SolicitudesCredito'] = 'Solicitudes de Crédito';
$app_list_strings['moduleListSingular']['LOW01_SolicitudesCredito'] = 'Solicitud de Crédito';
$app_list_strings['tipo_de_credito_list']['30_dias_sin_intereses'] = '30 días sin intereses';
$app_list_strings['tipo_de_credito_list']['90_dias_sin_intereses'] = '90 días sin intereses';
$app_list_strings['tipo_de_credito_list'][''] = '';
$app_list_strings['solicitud_credito_domicilio_list']['Propia'] = 'Propia';
$app_list_strings['solicitud_credito_domicilio_list']['Rentada'] = 'Rentada';
$app_list_strings['solicitud_credito_domicilio_list']['HIpotecada'] = 'HIpotecada';
$app_list_strings['solicitud_credito_domicilio_list']['De_Socios'] = 'De Socios';
$app_list_strings['solicitud_credito_domicilio_list'][''] = '';
$app_list_strings['solicitud_credito_giro_list']['Comercio'] = 'Comercio';
$app_list_strings['solicitud_credito_giro_list']['Industria'] = 'Industria';
$app_list_strings['solicitud_credito_giro_list']['Servicio'] = 'Servicio';
$app_list_strings['solicitud_credito_giro_list']['Agropecuario'] = 'Agropecuario';
$app_list_strings['solicitud_credito_giro_list']['Construccion'] = 'Construcción';
$app_list_strings['solicitud_credito_giro_list'][''] = '';
$app_list_strings['solicitud_credito_sector_list']['Publico'] = 'Público';
$app_list_strings['solicitud_credito_sector_list']['Privado'] = 'Privado';
$app_list_strings['solicitud_credito_sector_list'][''] = '';
$app_list_strings['solicitud_credito_caracter_list']['Propietario'] = 'Propietario';
$app_list_strings['solicitud_credito_caracter_list']['Asociado'] = 'Asociado';
$app_list_strings['solicitud_credito_caracter_list']['Empleado'] = 'Empleado';
$app_list_strings['solicitud_credito_caracter_list']['Otro'] = 'Otro';
$app_list_strings['solicitud_credito_caracter_list'][''] = '';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['T_de_C'] = 'T de C';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Debito'] = 'Débito';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Cheques'] = 'Cheques';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Ahorro'] = 'Ahorro';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Inversion'] = 'Inversión';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Otras'] = 'Otras';
$app_list_strings['solicitud_credito_tipo_ref_com_list'][''] = '';
$app_list_strings['recepcion_estado_cuenta_list']['Correo'] = 'Correo';
$app_list_strings['recepcion_estado_cuenta_list']['Correo_electronico'] = 'Correo electrónico';
$app_list_strings['recepcion_estado_cuenta_list'][''] = '';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list']['Correo'] = 'Correo';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list']['Correo_electronico'] = 'Correo electrónico';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list'][''] = '';
$app_list_strings['recepcion_informacion_promo_list']['Si'] = 'Si';
$app_list_strings['recepcion_informacion_promo_list']['No'] = 'No';
$app_list_strings['recepcion_informacion_promo_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_estado_autorizacion_list.php

 // created: 2017-01-05 18:47:03

$app_list_strings['estado_autorizacion_list']=array (
  '' => '',
  'pendiente' => 'Pendiente',
  'autorizada' => 'Autorizada',
  'rechazada' => 'Rechazada',
  'finalizada' => 'Finalizada',
  'terminada_incumplida' => 'Terminada Incumplida',
  'cancelada_devolucion_cancelacion' => 'Cancelada por Devolución o Cancelación',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_estado_interfaz_list.php

 // created: 2016-10-06 11:30:03

$app_list_strings['estado_interfaz_list'] = array (
  'vacio' => '-vacio-',
  'nuevo' => 'Nuevo',
  'procesando' => 'Procesando',
  'esperandoidpos' => 'Esperando Id POS',
  'completado' => 'Completado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_pagos_estados_konesh_list.php

 // created: 2017-07-06 21:59:32

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_subtipo_list.php

 // created: 2017-07-14 17:40:09

$app_list_strings['account_subtipo_list']=array (
  '' => '',
  'mostrador' => 'Mostrador',
  'campo' => 'Campo',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_oficio_list.php

 // created: 2017-07-14 17:31:57

$app_list_strings['account_oficio_list']=array (
  'albanil' => 'Albañil',
  'carpintero' => 'Carpintero',
  'electricista' => 'Electricista',
  'estuquero' => 'Estuquero',
  'herrero' => 'Herrero',
  'impermeabilizador' => 'Impermeabilizador',
  'instalador_de_climas' => 'Instalador de climas',
  'instalador_de_piso' => 'Instalador de piso',
  'jardinero' => 'Jardinero',
  'mtto_en_general' => 'Mtto. en general',
  'pintor' => 'Pintor',
  'plomero' => 'Plomero',
  'tabla_roquero' => 'Tabla roquero',
  'yesero' => 'Yesero',
  'otro' => 'Otro (especifique)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Pagos.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['lowes_Pagos'] = 'Pagos';
$app_list_strings['moduleListSingular']['lowes_Pagos'] = 'Pago';
$app_strings['LBL_ABONO_AUTORIZACION'] = 'Abono a Autorización';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_estado_c_list.php

 // created: 2017-07-14 17:22:07

$app_list_strings['account_estado_c_list']=array (
  'Activo' => 'Activo',
  'Inactivo' => 'Inactivo',
  'Inactivo120' => 'Sin órdenes +120 días',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_contacto_estado_list.php

 // created: 2017-08-01 23:08:12

$app_list_strings['contacto_estado_list']=array (
  '' => '',
  'activo' => 'Activo',
  'inactivo' => 'Inactivo',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_accounts_giro_list.php

 // created: 2017-07-14 17:26:35

$app_list_strings['accounts_giro_list']=array (
  'arquitecto' => 'Arquitecto',
  'construccion_vertical' => 'Construcción vertical',
  'contratista' => 'Contratista',
  'decorador' => 'Decorador',
  'desarrollador_de_vivienda_residencial' => 'Desarrollador de vivienda residencial',
  'ingeniero' => 'Ingeniero',
  'mtto_industrial' => 'Mtto. industrial',
  'obra_civil' => 'Obra civil',
  'proyecto_temporal_del_hogar' => 'Proyecto temporal del hogar',
  'urbanizador' => 'Urbanizador',
  'otro' => 'Otro (especifique)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Obras.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['lo_obras_type_dom']['Analyst'] = 'Analista';
$app_list_strings['lo_obras_type_dom']['Competitor'] = 'Competidor';
$app_list_strings['lo_obras_type_dom']['Customer'] = 'Cliente';
$app_list_strings['lo_obras_type_dom']['Integrator'] = 'Integrador';
$app_list_strings['lo_obras_type_dom']['Investor'] = 'Inversor';
$app_list_strings['lo_obras_type_dom']['Partner'] = 'Socio';
$app_list_strings['lo_obras_type_dom']['Press'] = 'Prensa';
$app_list_strings['lo_obras_type_dom']['Prospect'] = 'Prospecto';
$app_list_strings['lo_obras_type_dom']['Reseller'] = 'Revendedor';
$app_list_strings['lo_obras_type_dom']['Other'] = 'Otro';
$app_list_strings['lo_obras_type_dom'][''] = '';
$app_list_strings['moduleList']['LO_Obras'] = 'Obras';
$app_list_strings['moduleListSingular']['LO_Obras'] = 'Obra';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_estado_promesa_list.php

 // created: 2016-10-18 22:46:01

$app_list_strings['estado_promesa_list']=array (
  'vigente' => 'Vigente',
  'cumplida' => 'Cumplida',
  'incumplida' => 'Incumplida',
  'pagada_con_atraso' => 'Pagada con Atraso',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_contacto_tipo_list.php

 // created: 2017-07-13 14:17:02

$app_list_strings['contacto_tipo_list']=array (
  '' => '',
  'propietario'=>'Propietario',
  'director_general'=>'Director general',
  'asistente'=>'Asistente',
  'administrador'=>'Administrador',
  'comprador'=>'Comprador',
  'pagos'=>'Pagos',
  'supervisor_de_obra'=>'Supervisor de obra',
  'representante_legal'=>'Representante legal',
  'otro'=>'Otro (Especificar)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_autorizaciones_estado_promesa_monto_list.php

 // created: 2017-03-28 23:49:27

$app_list_strings['autorizaciones_estado_promesa_monto_list']=array (
  'sin_pago' => 'Sin Pago',
  'pagada_parcial' => 'Pagada Parcial',
  'pagada_completa' => 'Pagada Completa',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Partidas_Ordenes.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['LOW02_Partidas_Orden'] = 'Partidas de Orden';
$app_list_strings['moduleListSingular']['LOW02_Partidas_Orden'] = 'Partida de Orden';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_metodo_pago_list.php

 // created: 2017-07-14 17:41:53

$app_list_strings['account_metodo_pago_list']=array (
  '' => '',
  'cheque_al_dia' => 'Cheque al día',
  'cheque_post_fechado' => 'Cheque post-fechado',
  'credito_AR' => 'Crédito AR',
  'efectivo' => 'Efectivo',
  'tarjeta_de_regalo' => 'Tarjeta de regalo',
  'tc' => 'TC',
  'otros' => 'Otros',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_facturas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['LF_Facturas'] = 'Facturas';
$app_list_strings['moduleListSingular']['LF_Facturas'] = 'Factura';
$app_list_strings['tipo_documento_list']['factura'] = 'Factura';
$app_list_strings['estado_tiempo_list']['vigente'] = 'Vigente';
$app_list_strings['estado_tiempo_list']['vencida'] = 'Vencida';
$app_list_strings['estado_tiempo_list']['pagada'] = 'Pagada';
$app_list_strings['estado_tiempo_list']['devuelta_parcial'] = 'Devuelta parcial';
$app_list_strings['estado_tiempo_list']['devuelta_completa'] = 'Devuelta completa';
$app_list_strings['estado_tiempo_list']['entregada'] = 'Entregada';
$app_list_strings['estado_factura_list']['aclaracion'] = 'Aclaración';
$app_list_strings['estado_factura_list']['renovacion_de_cartera'] = 'Renovación de cartera';
$app_list_strings['estado_factura_list']['cobranza_extrajudicial'] = 'Cobranza extrajudicial';
$app_list_strings['estado_factura_list']['legal'] = 'Legal';
$app_list_strings['estado_factura_list']['perdida'] = 'Pérdida';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_lead_estado_list.php

 // created: 2017-07-20 21:17:02

$app_list_strings['lead_estado_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Inactivo' => 'Inactivo',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_lead_status_dom.php

 // created: 2017-08-09 14:53:26

$app_list_strings['lead_status_dom']=array (
  '' => '',
  'New' => 'Nuevo',
  'Converted' => 'Convertir a Prospecto',
  'Asignado' => 'Asignado',
  'AsignadoPendiente' => 'Asignado-Pendiente de Autorización',
  'Reciclado' => 'Reciclado',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_accounts_tipo_cliente_list.php

 // created: 2017-03-01 00:07:17

$app_list_strings['accounts_tipo_cliente_list']=array (
  '' => '',
  1 => 'Contractor',
  2 => 'Premier',
  3 => 'Reventa',  
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Pagos_Facturas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['Low01_Pagos_Facturas'] = 'Pagos_Facturas';
$app_list_strings['moduleListSingular']['Low01_Pagos_Facturas'] = 'Pago Factura';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_estado_list.php

 // created: 2016-10-18 23:03:47

$app_list_strings['estado_list']=array (
  '' => '',
  'por_aplicar' => 'Por Aplicar',
  'aplicado' => 'Aplicado',
  'no_aplicado' => 'No Aplicado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_contacto_subtipo_lista.php

 // created: 2017-07-18 22:28:01

$app_list_strings['contacto_subtipo_lista']=array (
  '' => '',
  'tomador_decision' => 'Tomador de decisión',
  'influenciador' => 'Influenciador',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_codigo_bloqueo_c_list.php

 // created: 2017-11-02 05:29:29

$app_list_strings['account_codigo_bloqueo_c_list']=array (
  '' => '',
  'CL' => 'Cliente Legal',
  'CI' => 'Cliente Inactivo',
  'DI' => 'Dictamen Incobrable',
  'Otro' => 'Otro',
  'CV' => 'Cartera vencida + x dias',
  'CD' => 'Cheque devuelto',
  'LE' => 'Limite de credito excedido',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_contacto_tipo_lista.php

 // created: 2017-07-18 22:24:30

$app_list_strings['contacto_tipo_lista']=array (
  '' => '',
  'lead' => 'Lead',
  'prospecto' => 'Prospecto',
  'cliente' => 'Cliente',
  'referencia_comercial' => 'Referencia Comercial (Inversionista)',
  'referencia_proveedor' => 'Referencia Proveedor',
  'representante_legal' => 'Representante Legal',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_Motivo_Solicitud_list.php

 // created: 2016-12-08 18:50:53

$app_list_strings['Motivo_Solicitud_list']=array (
  '' => '',
  'credito_disponible_temporal' => 'Crédito Disponible Temporal',
  'promesa_de_pago' => 'Promesa de Pago',
  'otro' => 'Otro',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_record_type_display.php

 // created: 2017-07-20 10:22:43

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Cliente Creditor AR',
  'Opportunities' => 'Proyecto',
  'Cases' => 'Caso',
  'Leads' => 'Leads',
  'Contacts' => 'Contactos',
  'Products' => 'Partida Individual Cotizada',
  'Quotes' => 'Cotizacion',
  'Bugs' => 'Error',
  'Project' => 'Proyecto',
  'Prospects' => 'Prospecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Tasks' => 'Tarea',
  'KBContents' => 'Base de Conocimiento',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_parent_type_display.php

 // created: 2017-07-20 10:22:38

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Cliente Crédito AR',
  'Contacts' => 'Contacto',
  'Tasks' => 'Tarea',
  'Opportunities' => 'Proyecto',
  'Products' => 'Partida Individual Cotizada',
  'Quotes' => 'Cotizacion',
  'Bugs' => 'Errores',
  'Cases' => 'Caso',
  'Leads' => 'Lead',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Prospecto',
  'KBContents' => 'Base de Conocimiento',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_prospect_tipo_cliente_list.php

 // created: 2017-07-20 23:16:33

$app_list_strings['prospect_tipo_cliente_list']=array (
  '' => '',
  'comercial' => 'Comercial',
  'especialista' => 'Especialista',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_proyecto_etapa_inicio_list.php

 // created: 2017-07-10 22:17:04

$app_list_strings['proyecto_etapa_inicio_list']=array (
  '' => '',
  'urbanizacion' => 'Urbanización',
  'cimentacion' => 'Cimentación',
  'obra_gris' => 'Obra Gris',
  'acabados' => 'Acabados',
  'terminado' => 'Terminado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_prospect_estado_account_list.php

 // created: 2017-07-10 21:54:43

$app_list_strings['prospect_estado_account_list']=array (
  '' => '',
  'activo' => 'Activo',
  'inactivo' => 'Inactivo',  
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_state_c_list.php

 // created: 2017-07-12 21:50:10

$app_list_strings['state_c_list']=array (
  '' => '',
  'AGU' => 'Aguascalientes',
  'BCN' => 'Baja California',
  'BCS' => 'Baja California Sur',
  'CAM' => 'Campeche',
  'CHP' => 'Chiapas',
  'CHH' => 'Chihuahua',
  'CMX' => 'Ciudad de México',
  'COA' => 'Coahuila',
  'COL' => 'Colima',
  'DUR' => 'Durango',
  'MEX' => 'Estado de México',
  'GUA' => 'Guanajuato',
  'GRO' => 'Guerrero',
  'HID' => 'Hidalgo',
  'JAL' => 'Jalisco',
  'MIC' => 'Michoacán',
  'MOR' => 'Morelos',
  'NAY' => 'Nayarit',
  'NLE' => 'Nuevo León',
  'OAX' => 'Oaxaca',
  'PUE' => 'Puebla',
  'QUE' => 'Querétaro',
  'ROO' => 'Quintana Roo',
  'SLP' => 'San Luis Potosí',
  'SIN' => 'Sinaloa',
  'SON' => 'Sonora',
  'TAB' => 'Tabasco',
  'TAM' => 'Tamaulipas',
  'TLA' => 'Tlaxcala',
  'VER' => 'Veracruz',
  'YUC' => 'Yucatán',
  'ZAC' => 'Zacatecas',
  'OTRO' => 'Otro (especifique)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_proyecto_estado_seguimiento_list.php

 // created: 2017-07-10 22:26:18

$app_list_strings['proyecto_estado_seguimiento_list']=array (
  'seguimiento_ok' => 'Seguimiento OK',
  'sin_seguimiento' => 'Sin Seguimiento',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_proyecto_estatus_list.php

 // created: 2017-07-11 16:28:04

$app_list_strings['proyecto_estatus_list']=array (
  'activo' => 'Activo',
  'inactivo' => 'Inactivo',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_proyecto_clase_cat_list.php

 // created: 2017-07-10 22:10:04

$app_list_strings['proyecto_clase_cat_list']=array (
  'banos' => 'Baños',
  'pintura' => 'Pintura',
  'madera' => 'Madera',
  'electrico' => 'Eléctrico',
  'plomeria' => 'Plomería',
  'herramientas' => 'Herramientas',
  'ferreteria' => 'Ferretería',
  'puertas_y_ventanas' => 'Puertas y ventanas',
  'pisos' => 'Pisos',
  'iluminacion' => 'Iluminación',
  'material_de_construccion' => 'Material de construcción',
  'temporada' => 'Temporada',
  'linea_blanca' => 'Línea Blanca',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_tipo_cuenta_c_list.php

 // created: 2016-09-14 19:03:51

$app_list_strings['tipo_cuenta_c_list']=array (
  'valor_sin_asignar' => 'Valor sin asignar',
  'cuenta_ar' => 'Cliente Cliente AR',
  'grupo_empresas' => 'Grupo de Empresa AR',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_proyecto_tipo_list.php

 // created: 2017-07-11 16:39:16

$app_list_strings['proyecto_tipo_list']=array (
  'oportunidad_nueva' => 'Oportunidad Nueva',
  'extension' => 'Extensión',
  'prospeccion' => 'Prospección',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_proyecto_motivo_perdida_list.php

 // created: 2017-07-11 19:10:35

$app_list_strings['proyecto_motivo_perdida_list']=array (
  '' => '',
  'perdida' => 'Perdida',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_prospect_pais_list.php

 // created: 2017-07-12 11:27:22

$app_list_strings['prospect_pais_list']=array (
  '' => '',
  'US' => 'USA',
  'CA' => 'Canada',
  'FR' => 'France',
  'MX' => 'México',
  'EU' => 'E.U.',
  'DE' => 'Germany',
  'GB' => 'Great Britain',
  'JP' => 'Japan',
  'PR' => 'Puerto Rico',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_proyecto_prob_venta_list.php

 // created: 2017-07-10 22:23:19

$app_list_strings['proyecto_prob_venta_list']=array (
  '' => '',
  10 => '10%',
  25 => '25%',
  50 => '50%',
  75 => '75%',
  100 => '100%',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_record_type_display_notes.php

 // created: 2017-07-20 10:22:41

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Cliente Crédito AR',
  'Contacts' => 'Contacto',
  'Opportunities' => 'Proyecto',
  'Tasks' => 'Tarea',
  'ProductTemplates' => 'Catálogo de Productos',
  'Quotes' => 'Cotizacion',
  'Products' => 'Partida Individual Cotizada',
  'Contracts' => 'Contrato',
  'Emails' => 'Correo Electrónico',
  'Bugs' => 'Error',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Prospecto',
  'Cases' => 'Caso',
  'Leads' => 'Lead',
  'Meetings' => 'Reunión',
  'Calls' => 'Llamada',
  'KBContents' => 'Base de Conocimiento',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_proyecto_tipo_colaboracion_list.php

 // created: 2017-07-10 22:32:35

$app_list_strings['proyecto_tipo_colaboracion_list']=array (
  '' => '',
  'tienda_atiende_cliente_vende_y_otra_entrega' => 'Tienda atiende cliente vende y otra entrega',
  'tienda_atiende_cliente_vende_y_otra_entrega_por_excepcion' => 'Tienda atiende cliente vende y otra entrega por excepción',
  'tienda_atiende_cliente_vende_y_transfiere_proyecto_a_otro_vendedor_en_tienda_2_para_atencion_de_la_c' => 'Tienda atiende cliente vende y transfiere proyecto a otro vendedor en tienda 2 para atención de la cuenta',
  'tienda_atiende_cliente_vende_y_csc_define_proyecto_asignado_a_otra_tienda' => 'Tienda atiende cliente vende y CSC define proyecto asignado a otra tienda',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_users_tipo_usuario_list.php

 // created: 2017-08-25 14:54:54

$app_list_strings['users_tipo_usuario_list']=array (
  '' => '',
  'Vendedor' => 'Vendedor',
  'admincredito' => 'Administrador de Crédito',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_proyecto_moneda_list.php

 // created: 2017-07-11 16:37:31

$app_list_strings['proyecto_moneda_list']=array (
  '' => '',
  'MXP' => 'MXP',
  'USD' => 'USD',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_sucursales_series_list.php

 // created: 2017-11-10 23:52:53

$app_list_strings['sucursales_series_list']=array (
  '' => '',
  'CH' => '3255',
  'CHHB' => '3255',
  'CM' => '3227',
  'CSC' => '',
  'CSCA' => '',
  'CSCB' => '',
  'CUL' => '3236',
  'CULB' => '3236',
  'CUMB' => '3227',
  'ESC' => '3267',
  'ESCB' => '3267',
  'EST' => '',
  'GSA' => '3165',
  'GSAB' => '3165',
  'HER' => '3235',
  'HERB' => '3235',
  'LVA' => '2935',
  'LVAA' => '2935',
  'LVAB' => '2935',
  'OLN' => '',
  'PLAB' => '3389',
  'SAL' => '3233',
  'SALB' => '3233',
  'SEN' => '2936',
  'SENA' => '2936',
  'SENB' => '2936',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_proyecto_etapa_list.php

 // created: 2017-07-11 16:31:13

$app_list_strings['proyecto_etapa_list']=array (
  'prospeccion'=>'Prospección',
  'identificacion_de_necesidades'=>'Identificación de necesidades',
  'cotizacion'=>'Cotización',
  'en_negociacion'=>'En negociación',
  'ganado_ventas_pendientes'=>'Ganado (Con ventas pendientes)',
  'termiando_sin_entregas_pendientes'=>'Terminado (Cuando ya no tienes entregas pendientes)',
  'perdida'=>'Perdida',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_prospect_estado_list.php

 // created: 2017-07-17 17:44:38

$app_list_strings['prospect_estado_list']=array (
  'nuevo' => 'Nuevo',
  'convertido' => 'Convertir a Cliente',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_solicitud_credito_estado_list.php

 // created: 2017-10-12 15:35:47

$app_list_strings['solicitud_credito_estado_list']=array (
  'Solicitud_Nueva' => 'Solicitud Nueva',
  'Finalizado_por_Solicitante' => 'Finalizado por Solicitante',
  'Aprobado_por_Asesor_Comercial' => 'Aprobado por Asesor Comercial',
  'Rechazado_por_Asesor_Comercial' => 'Rechazado por Asesor Comercial',
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => 'Pendiente por Gerente de Ventas Comerciales',
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => 'Aprobado por Gerente de Ventas Comerciales',
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => 'Rechazado por Gerente de Ventas Comerciales',
  'Pendiente_por_Analista_de_Credito' => 'Pendiente por Analista de Crédito',
  'Aprobado_por_Analista_de_Credito' => 'Aprobado por Analista de Crédito',
  'Rechazado_por_Analista_de_Credito' => 'Rechazado por Analista de Crédito',
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Pendiente de Envío a Agencia Externa por Administrador de Crédito',
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Aprobado Envío a Agencia Externa por Administrador de Crédito',
  'Pendiente_por_Agente_Externo' => 'Pendiente por Agente Externo',
  'Pendiente_por_Administrador_de_Credito' => 'Pendiente por Administrador de Crédito',
  'Aprobacion_por_Administrador_de_Credito' => 'Aprobación por Administrador de Crédito',
  'Rechazado_por_Administrador_de_Credito' => 'Rechazado por Administrador de Crédito',
  'Pendiente_por_Gerente_de_Credito' => 'Pendiente por Gerente de Crédito',
  'Aprobacion_por_Gerente_de_Credito' => 'Aprobación por Gerente de Crédito',
  'Rechazado_por_Gerente_de_Credito' => 'Rechazado por Gerente de Crédito',
  'Pendiente_por_Director_de_Finanzas' => 'Pendiente por Director de Finanzas',
  'Aprobacion_por_Director_de_Finanzas' => 'Aprobación por Director de Finanzas',
  'Rechazado_por_Director_de_Finanzas' => 'Rechazado por Director de Finanzas',
  'Pendiente_por_Vicepresidencia_Comercial' => 'Pendiente por Vicepresidencia Comercial',
  'Aprobacion_por_Vicepresidencia_Comercial' => 'Aprobación por Vicepresidencia Comercial',
  'Rechazado_por_Vicepresidencia_Comercial' => 'Rechazado por Vicepresidencia Comercial',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_vigencia_autesp_rango_list.php

 // created: 2017-06-16 23:00:38

$app_list_strings['vigencia_autesp_rango_list']=array (
  '' => '',
  'vigente' => 'Vigente',
  '1a15' => '1 - 15',
  '16a30' => '16 - 30',
  '31a60' => '31 - 60',
  '61a90' => '61 - 90',
  '91a120' => '91 - 120',
  'mas120' => '+120',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Producto';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Usuario';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Nuevo';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Asignado';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Cerrado';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Pendiente de Información';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Rechazado';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplicar';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Alta';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Media';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Baja';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Aceptado';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplicar';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Cerrado';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Fuera de Fecha';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Inválido';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Producto';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Usuario';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Nuevo';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Asignado';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Cerrado';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Pendiente de Información';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Rechazado';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplicar';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Alta';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Media';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Baja';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Aceptado';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplicar';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Cerrado';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Fuera de Fecha';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Inválido';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Producto';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Usuario';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Nuevo';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Asignado';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Cerrado';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Pendiente de Información';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Rechazado';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplicar';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Alta';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Media';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Baja';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Aceptado';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplicar';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Cerrado';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Fuera de Fecha';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Inválido';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CentroDeDisenio_Personalizaciones_180201_1311.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administration',
  'Product' => 'Producto',
  'User' => 'Usuario',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => 'Nuevo',
  'Assigned' => 'Asignado',
  'Closed' => 'Cerrado',
  'Pending Input' => 'Pendiente de Información',
  'Rejected' => 'Rechazado',
  'Duplicate' => 'Duplicar',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => 'Alta',
  'P2' => 'Media',
  'P3' => 'Baja',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => 'Aceptado',
  'Duplicate' => 'Duplicar',
  'Closed' => 'Cerrado',
  'Out of Date' => 'Fuera de Fecha',
  'Invalid' => 'Inválido',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  '' => '',
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LowesCRM3500.php
 
$app_list_strings['sucursal_c_list'] = array (
  '' => '',
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['prospect_pais_list'] = array (
  '' => '',
  'US' => 'USA',
  'CA' => 'Canada',
  'FR' => 'France',
  'MX' => 'México',
  'EU' => 'E.U.',
  'DE' => 'Germany',
  'GB' => 'Great Britain',
  'JP' => 'Japan',
  'PR' => 'Puerto Rico',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_estado_segun_cartera_c_list'] = array (
  'CV' => 'Cartera vigente',
  'CV130D' => 'Cartera vencida 1 a 30 días',
  'CV31120D' => 'Cartera vencida 31 a 120 días',
  'CV120D' => 'Cartera vencida 120 días',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['accounts_tipo_cliente_list'] = array (
  1 => 'Contractor',
  2 => 'Premier',
  3 => 'Reventa',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_codigo_bloqueo_c_list'] = array (
  '' => '',
  'CL' => 'Cliente Legal',
  'CI' => 'Cliente Inactivo',
  'DI' => 'Dictamen Incobrable',
  'Otro' => 'Otro',
  'CV' => 'Cartera vencida + x dias',
  'CD' => 'Cheque devuelto',
  'LE' => 'Limite de credito excedido',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['state_c_list'] = array (
  '' => '',
  'AGU' => 'Aguascalientes',
  'BCN' => 'Baja California',
  'BCS' => 'Baja California Sur',
  'CAM' => 'Campeche',
  'CHP' => 'Chiapas',
  'CHH' => 'Chihuahua',
  'CMX' => 'Ciudad de México',
  'COA' => 'Coahuila',
  'COL' => 'Colima',
  'DUR' => 'Durango',
  'MEX' => 'Estado de México',
  'GUA' => 'Guanajuato',
  'GRO' => 'Guerrero',
  'HID' => 'Hidalgo',
  'JAL' => 'Jalisco',
  'MIC' => 'Michoacán',
  'MOR' => 'Morelos',
  'NAY' => 'Nayarit',
  'NLE' => 'Nuevo León',
  'OAX' => 'Oaxaca',
  'PUE' => 'Puebla',
  'QUE' => 'Querétaro',
  'ROO' => 'Quintana Roo',
  'SLP' => 'San Luis Potosí',
  'SIN' => 'Sinaloa',
  'SON' => 'Sonora',
  'TAB' => 'Tabasco',
  'TAM' => 'Tamaulipas',
  'TLA' => 'Tlaxcala',
  'VER' => 'Veracruz',
  'YUC' => 'Yucatán',
  'ZAC' => 'Zacatecas',
  'OTRO' => 'Otro (especifique)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['prospect_estado_account_list'] = array (
  '' => '',
  'activo' => 'Activo',
  'inactivo' => 'Inactivo',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_type_c_list'] = array (
  'Fisica' => 'Física',
  'Moral' => 'Moral',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_subtipo_list'] = array (
  '' => '',
  'mostrador' => 'Mostrador',
  'campo' => 'Campo',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['tipo_telefono_principal_list'] = array (
  '' => '',
  1 => 'Casa',
  2 => 'Celular',
  3 => 'Fax',
  4 => 'Page',
  5 => 'Otro',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['accounts_giro_list'] = array (
  'arquitecto' => 'Arquitecto',
  'construccion_vertical' => 'Construcción vertical',
  'contratista' => 'Contratista',
  'decorador' => 'Decorador',
  'desarrollador_de_vivienda_residencial' => 'Desarrollador de vivienda residencial',
  'ingeniero' => 'Ingeniero',
  'mtto_industrial' => 'Mtto. industrial',
  'obra_civil' => 'Obra civil',
  'proyecto_temporal_del_hogar' => 'Proyecto temporal del hogar',
  'urbanizador' => 'Urbanizador',
  'otro' => 'Otro (especifique)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_estado_c_list'] = array (
  'Activo' => 'Activo',
  'Inactivo' => 'Inactivo',
  'Inactivo120' => 'Sin órdenes +120 días',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['estado_interfaz_list'] = array (
  'vacio' => '-vacio-',
  'nuevo' => 'Nuevo',
  'procesando' => 'Procesando',
  'esperandoidpos' => 'Esperando Id POS',
  'completado' => 'Completado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_estado_bloqueo_c_list'] = array (
  'Desbloqueado' => 'Desbloqueado',
  'Bloqueado' => 'Bloqueado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['accounts_tipo_cliente_list'] = array (
  '' => '',
  1 => 'Contractor',
  2 => 'Premier',
  3 => 'Reventa',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['prospect_estado_list'] = array (
  'nuevo' => 'Nuevo',
  'convertido' => 'Convertir a Cliente',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['primary_address_country_c_list'] = array (
  'US' => 'USA',
  'CA' => 'Canada',
  'FR' => 'France',
  'MX' => 'México',
  'EU' => 'E.U.',
  'DE' => 'Germany',
  'GB' => 'Great Britain',
  'JP' => 'Japan',
  'PR' => 'Puerto Rico',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_metodo_pago_list'] = array (
  '' => '',
  'cheque_al_dia' => 'Cheque al día',
  'cheque_post_fechado' => 'Cheque post-fechado',
  'credito_AR' => 'Crédito AR',
  'efectivo' => 'Efectivo',
  'tarjeta_de_regalo' => 'Tarjeta de regalo',
  'tc' => 'TC',
  'otros' => 'Otros',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_oficio_list'] = array (
  'albanil' => 'Albañil',
  'carpintero' => 'Carpintero',
  'electricista' => 'Electricista',
  'estuquero' => 'Estuquero',
  'herrero' => 'Herrero',
  'impermeabilizador' => 'Impermeabilizador',
  'instalador_de_climas' => 'Instalador de climas',
  'instalador_de_piso' => 'Instalador de piso',
  'jardinero' => 'Jardinero',
  'mtto_en_general' => 'Mtto. en general',
  'pintor' => 'Pintor',
  'plomero' => 'Plomero',
  'tabla_roquero' => 'Tabla roquero',
  'yesero' => 'Yesero',
  'otro' => 'Otro (especifique)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['prospect_tipo_cliente_list'] = array (
  '' => '',
  'comercial' => 'Comercial',
  'especialista' => 'Especialista',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.LOWESCRM3.506_3.php
 
$app_list_strings['prospect_pais_list'] = array (
  '' => '',
  'US' => 'USA',
  'CA' => 'Canada',
  'FR' => 'France',
  'MX' => 'México',
  'EU' => 'E.U.',
  'DE' => 'Germany',
  'GB' => 'Great Britain',
  'JP' => 'Japan',
  'PR' => 'Puerto Rico',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  '' => '',
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['lead_estado_list'] = array (
  '' => '',
  'Activo' => 'Activo',
  'Inactivo' => 'Inactivo',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['tipo_telefono_principal_list'] = array (
  '' => '',
  1 => 'Casa',
  2 => 'Celular',
  3 => 'Fax',
  4 => 'Page',
  5 => 'Otro',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['prospect_estado_list'] = array (
  'nuevo' => 'Nuevo',
  'convertido' => 'Convertir a Cliente',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_subtipo_list'] = array (
  '' => '',
  'mostrador' => 'Mostrador',
  'campo' => 'Campo',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['accounts_tipo_cliente_list'] = array (
  '' => '',
  1 => 'Contractor',
  2 => 'Premier',
  3 => 'Reventa',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_oficio_list'] = array (
  'albanil' => 'Albañil',
  'carpintero' => 'Carpintero',
  'electricista' => 'Electricista',
  'estuquero' => 'Estuquero',
  'herrero' => 'Herrero',
  'impermeabilizador' => 'Impermeabilizador',
  'instalador_de_climas' => 'Instalador de climas',
  'instalador_de_piso' => 'Instalador de piso',
  'jardinero' => 'Jardinero',
  'mtto_en_general' => 'Mtto. en general',
  'pintor' => 'Pintor',
  'plomero' => 'Plomero',
  'tabla_roquero' => 'Tabla roquero',
  'yesero' => 'Yesero',
  'otro' => 'Otro (especifique)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_estado_bloqueo_c_list'] = array (
  'Desbloqueado' => 'Desbloqueado',
  'Bloqueado' => 'Bloqueado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['state_c_list'] = array (
  '' => '',
  'AGU' => 'Aguascalientes',
  'BCN' => 'Baja California',
  'BCS' => 'Baja California Sur',
  'CAM' => 'Campeche',
  'CHP' => 'Chiapas',
  'CHH' => 'Chihuahua',
  'CMX' => 'Ciudad de México',
  'COA' => 'Coahuila',
  'COL' => 'Colima',
  'DUR' => 'Durango',
  'MEX' => 'Estado de México',
  'GUA' => 'Guanajuato',
  'GRO' => 'Guerrero',
  'HID' => 'Hidalgo',
  'JAL' => 'Jalisco',
  'MIC' => 'Michoacán',
  'MOR' => 'Morelos',
  'NAY' => 'Nayarit',
  'NLE' => 'Nuevo León',
  'OAX' => 'Oaxaca',
  'PUE' => 'Puebla',
  'QUE' => 'Querétaro',
  'ROO' => 'Quintana Roo',
  'SLP' => 'San Luis Potosí',
  'SIN' => 'Sinaloa',
  'SON' => 'Sonora',
  'TAB' => 'Tabasco',
  'TAM' => 'Tamaulipas',
  'TLA' => 'Tlaxcala',
  'VER' => 'Veracruz',
  'YUC' => 'Yucatán',
  'ZAC' => 'Zacatecas',
  'OTRO' => 'Otro (especifique)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  '' => '',
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  3397 => 'León',
  3242 => 'Aguascalientes',
  3393 => 'Santa Catarina',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['estado_interfaz_list'] = array (
  'vacio' => '-vacio-',
  'nuevo' => 'Nuevo',
  'procesando' => 'Procesando',
  'esperandoidpos' => 'Esperando Id POS',
  'completado' => 'Completado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['prospect_tipo_cliente_list'] = array (
  '' => '',
  'comercial' => 'Comercial',
  'especialista' => 'Especialista',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_type_c_list'] = array (
  'Fisica' => 'Física',
  'Moral' => 'Moral',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['accounts_tipo_cliente_list'] = array (
  '' => '',
  1 => 'Contractor',
  2 => 'Premier',
  3 => 'Reventa',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['lead_status_dom'] = array (
  '' => '',
  'New' => 'Nuevo',
  'Converted' => 'Convertir a Prospecto',
  'Asignado' => 'Asignado',
  'AsignadoPendiente' => 'Asignado-Pendiente de Autorización',
  'Reciclado' => 'Reciclado',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_metodo_pago_list'] = array (
  '' => '',
  'cheque_al_dia' => 'Cheque al día',
  'cheque_post_fechado' => 'Cheque post-fechado',
  'credito_AR' => 'Crédito AR',
  'efectivo' => 'Efectivo',
  'tarjeta_de_regalo' => 'Tarjeta de regalo',
  'tc' => 'TC',
  'otros' => 'Otros',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_estado_c_list'] = array (
  'Activo' => 'Activo',
  'Inactivo' => 'Inactivo',
  'Inactivo120' => 'Sin órdenes +120 días',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['prospect_estado_account_list'] = array (
  '' => '',
  'activo' => 'Activo',
  'inactivo' => 'Inactivo',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['primary_address_country_c_list'] = array (
  'US' => 'USA',
  'CA' => 'Canada',
  'FR' => 'France',
  'MX' => 'México',
  'EU' => 'E.U.',
  'DE' => 'Germany',
  'GB' => 'Great Britain',
  'JP' => 'Japan',
  'PR' => 'Puerto Rico',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['accounts_giro_list'] = array (
  'arquitecto' => 'Arquitecto',
  'construccion_vertical' => 'Construcción vertical',
  'contratista' => 'Contratista',
  'decorador' => 'Decorador',
  'desarrollador_de_vivienda_residencial' => 'Desarrollador de vivienda residencial',
  'ingeniero' => 'Ingeniero',
  'mtto_industrial' => 'Mtto. industrial',
  'obra_civil' => 'Obra civil',
  'proyecto_temporal_del_hogar' => 'Proyecto temporal del hogar',
  'urbanizador' => 'Urbanizador',
  'otro' => 'Otro (especifique)',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['state_c_list'] = array (
  '' => '',
  'AGU' => 'Aguascalientes',
  'BCN' => 'Baja California',
  'BCS' => 'Baja California Sur',
  'CAM' => 'Campeche',
  'CHP' => 'Chiapas',
  'CHH' => 'Chihuahua',
  'CMX' => 'Ciudad de México',
  'COA' => 'Coahuila',
  'COL' => 'Colima',
  'DUR' => 'Durango',
  'MEX' => 'Estado de México',
  'GUA' => 'Guanajuato',
  'GRO' => 'Guerrero',
  'HID' => 'Hidalgo',
  'JAL' => 'Jalisco',
  'MIC' => 'Michoacán',
  'MOR' => 'Morelos',
  'NAY' => 'Nayarit',
  'NLE' => 'Nuevo León',
  'OAX' => 'Oaxaca',
  'PUE' => 'Puebla',
  'QUE' => 'Querétaro',
  'ROO' => 'Quintana Roo',
  'SLP' => 'San Luis Potosí',
  'SIN' => 'Sinaloa',
  'SON' => 'Sonora',
  'TAB' => 'Tabasco',
  'TAM' => 'Tamaulipas',
  'TLA' => 'Tlaxcala',
  'VER' => 'Veracruz',
  'YUC' => 'Yucatán',
  'ZAC' => 'Zacatecas',
  'OTRO' => 'Otro (especifique)',
);$app_list_strings['primary_address_country_c_list'] = array (
  'US' => 'USA',
  'CA' => 'Canada',
  'FR' => 'France',
  'MX' => 'México',
  'EU' => 'E.U.',
  'DE' => 'Germany',
  'GB' => 'Great Britain',
  'JP' => 'Japan',
  'PR' => 'Puerto Rico',
);$app_list_strings['account_type_c_list'] = array (
  'Fisica' => 'Física',
  'Moral' => 'Moral',
);$app_list_strings['account_codigo_bloqueo_c_list'] = array (
  '' => '',
  'CL' => 'Cliente Legal',
  'CI' => 'Cliente Inactivo',
  'DI' => 'Dictamen Incobrable',
  'Otro' => 'Otro',
  'CV' => 'Cartera vencida + x dias',
  'CD' => 'Cheque devuelto',
  'LE' => 'Limite de credito excedido',
);$app_list_strings['account_estado_segun_cartera_c_list'] = array (
  'CV' => 'Cartera vigente',
  'CV130D' => 'Cartera vencida 1 a 30 días',
  'CV31120D' => 'Cartera vencida 31 a 120 días',
  'CV120D' => 'Cartera vencida 120 días',
);$app_list_strings['account_estado_c_list'] = array (
  'Activo' => 'Activo',
  'Inactivo' => 'Inactivo',
  'Inactivo120' => 'Sin órdenes +120 días',
);$app_list_strings['account_estado_bloqueo_c_list'] = array (
  'Desbloqueado' => 'Desbloqueado',
  'Bloqueado' => 'Bloqueado',
);$app_list_strings['estado_interfaz_list'] = array (
  'vacio' => '-vacio-',
  'nuevo' => 'Nuevo',
  'procesando' => 'Procesando',
  'esperandoidpos' => 'Esperando Id POS',
  'completado' => 'Completado',
);$app_list_strings['accounts_giro_list'] = array (
  'arquitecto' => 'Arquitecto',
  'construccion_vertical' => 'Construcción vertical',
  'contratista' => 'Contratista',
  'decorador' => 'Decorador',
  'desarrollador_de_vivienda_residencial' => 'Desarrollador de vivienda residencial',
  'ingeniero' => 'Ingeniero',
  'mtto_industrial' => 'Mtto. industrial',
  'obra_civil' => 'Obra civil',
  'proyecto_temporal_del_hogar' => 'Proyecto temporal del hogar',
  'urbanizador' => 'Urbanizador',
  'otro' => 'Otro (especifique)',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);$app_list_strings['account_metodo_pago_list'] = array (
  '' => '',
  'cheque_al_dia' => 'Cheque al día',
  'cheque_post_fechado' => 'Cheque post-fechado',
  'credito_AR' => 'Crédito AR',
  'efectivo' => 'Efectivo',
  'tarjeta_de_regalo' => 'Tarjeta de regalo',
  'tc' => 'TC',
  'otros' => 'Otros',
);$app_list_strings['account_oficio_list'] = array (
  'albanil' => 'Albañil',
  'carpintero' => 'Carpintero',
  'electricista' => 'Electricista',
  'estuquero' => 'Estuquero',
  'herrero' => 'Herrero',
  'impermeabilizador' => 'Impermeabilizador',
  'instalador_de_climas' => 'Instalador de climas',
  'instalador_de_piso' => 'Instalador de piso',
  'jardinero' => 'Jardinero',
  'mtto_en_general' => 'Mtto. en general',
  'pintor' => 'Pintor',
  'plomero' => 'Plomero',
  'tabla_roquero' => 'Tabla roquero',
  'yesero' => 'Yesero',
  'otro' => 'Otro (especifique)',
);$app_list_strings['accounts_tipo_cliente_list'] = array (
  '' => '',
  1 => 'Contractor',
  2 => 'Premier',
  3 => 'Reventa',
);$app_list_strings['tipo_telefono_principal_list'] = array (
  '' => '',
  1 => 'Casa',
  2 => 'Celular',
  3 => 'Fax',
  4 => 'Page',
  5 => 'Otro',
);$app_list_strings['prospect_tipo_cliente_list'] = array (
  '' => '',
  'comercial' => 'Comercial',
  'especialista' => 'Especialista',
);$app_list_strings['account_subtipo_list'] = array (
  '' => '',
  'mostrador' => 'Mostrador',
  'campo' => 'Campo',
);$app_list_strings['prospect_pais_list'] = array (
  '' => '',
  'US' => 'USA',
  'CA' => 'Canada',
  'FR' => 'France',
  'MX' => 'México',
  'EU' => 'E.U.',
  'DE' => 'Germany',
  'GB' => 'Great Britain',
  'JP' => 'Japan',
  'PR' => 'Puerto Rico',
);$app_list_strings['prospect_estado_account_list'] = array (
  '' => '',
  'activo' => 'Activo',
  'inactivo' => 'Inactivo',
);$app_list_strings['prospect_estado_list'] = array (
  'nuevo' => 'Nuevo',
  'convertido' => 'Convertir a Cliente',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_estado_segun_cartera_c_list'] = array (
  'CV' => 'Cartera vigente',
  'CV130D' => 'Cartera vencida 1 a 30 días',
  'CV31120D' => 'Cartera vencida 31 a 120 días',
  'CV120D' => 'Cartera vencida 120 días',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['account_codigo_bloqueo_c_list'] = array (
  '' => '',
  'CL' => 'Cliente Legal',
  'CI' => 'Cliente Inactivo',
  'DI' => 'Dictamen Incobrable',
  'Otro' => 'Otro',
  'CV' => 'Cartera vencida + x dias',
  'CD' => 'Cheque devuelto',
  'LE' => 'Limite de credito excedido',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_sucursal_c_list.php

 // created: 2018-05-14 15:58:32

$app_list_strings['sucursal_c_list']=array (
  '' => '',
  3242 => 'Aguascalientes',
  3255 => 'Chihuahua',
  3236 => 'Culiacán',
  3227 => 'Cumbres',
  3267 => 'Escobedo',
  3165 => 'Garza Sada',
  3235 => 'Hermosillo',
  3397 => 'Leon',
  2935 => 'Linda Vista',
  3271 => 'Oferre',
  3389 => 'Pablo Livas',
  3233 => 'Saltillo',
  3393 => 'Santa Catarina',
  2936 => 'Sendero',
  3265 => 'Valle Alto',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_Orden_Estado_list.php

 // created: 2018-06-05 00:45:38

$app_list_strings['Orden_Estado_list']=array (
  '' => '',
  0 => '',
  1 => 'Iniciada',
  2 => 'Parcial',
  3 => 'Lleno',
  4 => 'Cancelada',
  5 => 'Completada',
  6 => 'Devuelta',
  7 => 'Cancelada Suspendida',
  8 => 'Cancelada Parcial',
  9 => 'Cancelada Completa',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_ordenes_estado_list.php

 // created: 2016-12-13 00:42:59

$app_list_strings['ordenes_estado_list']=array (
  '' => 'Venta',
  '0' => 'Nueva',
  '1' => 'Iniciada',
  '2' => 'Parcial',
  '3' => 'Lleno',
  '4' => 'Cancelada',
  '5' => 'Completada',
  '6' => 'Devuelta',
  '7' => 'Cancelada Suspendida',
  '8' => 'Cancelada Parcial',
  '9' => 'Cancelada Completada',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Formas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['FDP_Pago'] = 'Formas de Pago';
$app_list_strings['moduleListSingular']['FDP_Pago'] = 'Forma de Pago';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.FormasPago.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['FDP_formasPago'] = 'formaPago';
$app_list_strings['moduleListSingular']['FDP_formasPago'] = 'formapago';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_moduleList.php

// created: 2018-10-04 03:45:55
$app_list_strings['moduleList']['Home'] = 'Inicio';
$app_list_strings['moduleList']['Contacts'] = 'Contactos';
$app_list_strings['moduleList']['Accounts'] = 'Clientes';
$app_list_strings['moduleList']['Opportunities'] = 'Proyectos';
$app_list_strings['moduleList']['Cases'] = 'Casos';
$app_list_strings['moduleList']['Notes'] = 'Notas';
$app_list_strings['moduleList']['Calls'] = 'Llamadas';
$app_list_strings['moduleList']['Emails'] = 'Correos Electrónicos';
$app_list_strings['moduleList']['Meetings'] = 'Reuniones';
$app_list_strings['moduleList']['Tasks'] = 'Tareas';
$app_list_strings['moduleList']['Calendar'] = 'Calendario';
$app_list_strings['moduleList']['Leads'] = 'Leads';
$app_list_strings['moduleList']['Contracts'] = 'Contratos';
$app_list_strings['moduleList']['Quotes'] = 'Cotizaciones';
$app_list_strings['moduleList']['Products'] = 'Partidas Individuales Cotizadas';
$app_list_strings['moduleList']['Reports'] = 'Informes';
$app_list_strings['moduleList']['Forecasts'] = 'Previsiones';
$app_list_strings['moduleList']['Bugs'] = 'Errores';
$app_list_strings['moduleList']['Project'] = 'Proyectos';
$app_list_strings['moduleList']['Campaigns'] = 'Campañas';
$app_list_strings['moduleList']['Documents'] = 'Documentos';
$app_list_strings['moduleList']['pmse_Inbox'] = 'Procesos';
$app_list_strings['moduleList']['pmse_Project'] = 'Definiciones de Proceso';
$app_list_strings['moduleList']['pmse_Business_Rules'] = 'Proceso de Reglas Empresariales';
$app_list_strings['moduleList']['pmse_Emails_Templates'] = 'Proceso de Plantillas E-mail';
$app_list_strings['moduleList']['Prospects'] = 'Prospectos';
$app_list_strings['moduleList']['ProspectLists'] = 'Listas de Público Objetivo';
$app_list_strings['moduleList']['Tags'] = 'Etiquetas';
$app_list_strings['moduleList']['OutboundEmail'] = 'Opciones de Correo Electrónico';
$app_list_strings['moduleList']['DataPrivacy'] = 'Privacidad de datos';
$app_list_strings['moduleList']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['moduleList']['RevenueLineItems'] = 'Artículos de Línea de Ganancia';
$app_list_strings['moduleList']['GPE_Grupo_empresas'] = 'Grupo de empresas';
$app_list_strings['moduleList']['orden_Ordenes'] = 'Órdenes';
$app_list_strings['moduleList']['NC_Notas_credito'] = 'Notas de crédito';
$app_list_strings['moduleList']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['moduleList']['LF_Facturas'] = 'Facturas';
$app_list_strings['moduleList']['lowes_Pagos'] = 'Pagos';
$app_list_strings['moduleList']['Low01_Pagos_Facturas'] = 'Pagos_Facturas';
$app_list_strings['moduleList']['LOW02_Partidas_Orden'] = 'Partidas de Orden';
$app_list_strings['moduleList']['LO_Obras'] = 'Obras';
$app_list_strings['moduleList']['LOW01_SolicitudesCredito'] = 'Solicitudes de Crédito';
$app_list_strings['moduleList']['MRX1_Configuraciones'] = 'Configuraciones';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleList']['ops_Backups'] = 'Backups';
$app_list_strings['moduleList']['FDP_Pago'] = 'Formas de Pago';
$app_list_strings['moduleList']['FDP_formasPago'] = 'Formas de Pago';
$app_list_strings['moduleList']['Low03_AgentesExternos'] = 'Agente Externo';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_moduleListSingular.php

// created: 2018-10-04 03:46:15
$app_list_strings['moduleListSingular']['Home'] = 'Inicio';
$app_list_strings['moduleListSingular']['Contacts'] = 'Contacto';
$app_list_strings['moduleListSingular']['Accounts'] = 'Cliente';
$app_list_strings['moduleListSingular']['Opportunities'] = 'Oportunidad';
$app_list_strings['moduleListSingular']['Cases'] = 'Caso';
$app_list_strings['moduleListSingular']['Notes'] = 'Nota';
$app_list_strings['moduleListSingular']['Calls'] = 'Llamada';
$app_list_strings['moduleListSingular']['Emails'] = 'Correo Electrónico';
$app_list_strings['moduleListSingular']['Meetings'] = 'Reunión';
$app_list_strings['moduleListSingular']['Tasks'] = 'Tarea';
$app_list_strings['moduleListSingular']['Calendar'] = 'Calendario';
$app_list_strings['moduleListSingular']['Leads'] = 'Lead';
$app_list_strings['moduleListSingular']['Contracts'] = 'Contrato';
$app_list_strings['moduleListSingular']['Quotes'] = 'Cotizacion';
$app_list_strings['moduleListSingular']['Products'] = 'Partida Individual Cotizada';
$app_list_strings['moduleListSingular']['Reports'] = 'Informe';
$app_list_strings['moduleListSingular']['Forecasts'] = 'Pronóstico';
$app_list_strings['moduleListSingular']['Bugs'] = 'Error';
$app_list_strings['moduleListSingular']['Project'] = 'Proyecto';
$app_list_strings['moduleListSingular']['Campaigns'] = 'Campaña';
$app_list_strings['moduleListSingular']['Documents'] = 'Documento';
$app_list_strings['moduleListSingular']['pmse_Inbox'] = 'Proceso';
$app_list_strings['moduleListSingular']['pmse_Project'] = 'Definición de Proceso';
$app_list_strings['moduleListSingular']['pmse_Business_Rules'] = 'Proceso de Regla Empresarial';
$app_list_strings['moduleListSingular']['pmse_Emails_Templates'] = 'Plantilla de Correo Electrónico para procesos';
$app_list_strings['moduleListSingular']['Prospects'] = 'Pospecto';
$app_list_strings['moduleListSingular']['ProspectLists'] = 'Lista de Público Objetivo';
$app_list_strings['moduleListSingular']['Tags'] = 'Etiqueta';
$app_list_strings['moduleListSingular']['OutboundEmail'] = 'Opciones de Correo Electrónico';
$app_list_strings['moduleListSingular']['DataPrivacy'] = 'Privacidad de datos';
$app_list_strings['moduleListSingular']['KBContents'] = 'Artículo de Base de Conocimiento';
$app_list_strings['moduleListSingular']['RevenueLineItems'] = 'Artículo de Línea de Ganancia';
$app_list_strings['moduleListSingular']['GPE_Grupo_empresas'] = 'Grupo de empresa';
$app_list_strings['moduleListSingular']['orden_Ordenes'] = 'Orden';
$app_list_strings['moduleListSingular']['NC_Notas_credito'] = 'Nota de crédito';
$app_list_strings['moduleListSingular']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['moduleListSingular']['LF_Facturas'] = 'Factura';
$app_list_strings['moduleListSingular']['lowes_Pagos'] = 'Pago';
$app_list_strings['moduleListSingular']['Low01_Pagos_Facturas'] = 'Pago Factura';
$app_list_strings['moduleListSingular']['LOW02_Partidas_Orden'] = 'Partida de Orden';
$app_list_strings['moduleListSingular']['LO_Obras'] = 'Obra';
$app_list_strings['moduleListSingular']['LOW01_SolicitudesCredito'] = 'Solicitud de Crédito';
$app_list_strings['moduleListSingular']['MRX1_Configuraciones'] = 'Configuración';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['moduleListSingular']['ops_Backups'] = 'Backup';
$app_list_strings['moduleListSingular']['FDP_Pago'] = 'Forma de Pago';
$app_list_strings['moduleListSingular']['FDP_formasPago'] = 'Forma de Pago';
$app_list_strings['moduleListSingular']['Low03_AgentesExternos'] = 'Agente Externo';

?>
