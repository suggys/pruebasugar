<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_moduleList.php

 //created: 2016-07-25 21:53:48

$app_list_strings['moduleList']['RevenueLineItems']='Revenue Line Items';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:49

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Contact',
  'Opportunities' => 'Opportunity',
  'Tasks' => 'Task',
  'ProductTemplates' => 'Product Catalog',
  'Quotes' => 'Quote',
  'Products' => 'Quoted Line Item',
  'Contracts' => 'Contract',
  'Emails' => 'Email',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'ProjectTask' => 'Project Task',
  'Prospects' => 'Target',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Meetings' => 'Meeting',
  'Calls' => 'Call',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Revenue Line Items',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_record_type_display.php

 // created: 2016-07-25 21:53:49

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Account',
  'Opportunities' => 'Opportunity',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Contacts' => 'Contacts',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Quote',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'Prospects' => 'Target',
  'ProjectTask' => 'Project Task',
  'Tasks' => 'Task',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Revenue Line Items',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:49

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Contact',
  'Tasks' => 'Task',
  'Opportunities' => 'Opportunity',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Quote',
  'Bugs' => 'Bugs',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Project' => 'Project',
  'ProjectTask' => 'Project Task',
  'Prospects' => 'Target',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Revenue Line Items',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:50

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Организация',
  'Contacts' => 'Контакт',
  'Opportunities' => 'Възможност',
  'Tasks' => 'Задача',
  'ProductTemplates' => 'Продуктов каталог',
  'Quotes' => 'Оферта',
  'Products' => 'Офериран продукт',
  'Contracts' => 'Договор',
  'Emails' => 'Електронна поща',
  'Bugs' => 'Проблем',
  'Project' => 'Проекти',
  'ProjectTask' => 'Задача по проект',
  'Prospects' => 'Целеви клиент',
  'Cases' => 'Казус',
  'Leads' => 'Потенциален клиент',
  'Meetings' => 'Среща',
  'Calls' => 'Обаждане',
  'KBContents' => 'База от знания',
  'RevenueLineItems' => 'Приходни позиции',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:50

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Организация',
  'Contacts' => 'Контакт',
  'Tasks' => 'Задача',
  'Opportunities' => 'Възможност',
  'Products' => 'Офериран продукт',
  'Quotes' => 'Оферта',
  'Bugs' => 'Проблеми',
  'Cases' => 'Казус',
  'Leads' => 'Потенциален клиент',
  'Project' => 'Проекти',
  'ProjectTask' => 'Задача по проект',
  'Prospects' => 'Целеви клиент',
  'KBContents' => 'База от знания',
  'RevenueLineItems' => 'Приходни позиции',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.sugar_moduleList.php

 //created: 2016-07-25 21:53:50

$app_list_strings['moduleList']['RevenueLineItems']='Приходни позиции';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/cs_CZ.sugar_moduleList.php

 //created: 2016-07-25 21:53:51

$app_list_strings['moduleList']['RevenueLineItems']='Řádky obchodu';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/cs_CZ.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:51

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Účet',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Příležitost',
  'Tasks' => 'Úkol',
  'ProductTemplates' => 'Katalog produktů',
  'Quotes' => 'Nabídka',
  'Products' => 'Produkt',
  'Contracts' => 'Kontrakt',
  'Emails' => 'E-mail',
  'Bugs' => 'Chyba:',
  'Project' => 'Projekty',
  'ProjectTask' => 'Projektové úkoly',
  'Prospects' => 'Kontakt',
  'Cases' => 'Případ:',
  'Leads' => 'Příležitost',
  'Meetings' => 'Schůzka',
  'Calls' => 'Hovor',
  'KBContents' => 'Znalostní báze',
  'RevenueLineItems' => 'Řádky obchodu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/bg_BG.sugar_record_type_display.php

 // created: 2016-07-25 21:53:51

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Организация',
  'Opportunities' => 'Възможност',
  'Cases' => 'Казус',
  'Leads' => 'Потенциален клиент',
  'Contacts' => 'Контакти',
  'Products' => 'Офериран продукт',
  'Quotes' => 'Оферта',
  'Bugs' => 'Проблем',
  'Project' => 'Проекти',
  'Prospects' => 'Целеви клиент',
  'ProjectTask' => 'Задача по проект',
  'Tasks' => 'Задача',
  'KBContents' => 'База от знания',
  'RevenueLineItems' => 'Приходни позиции',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/cs_CZ.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:51

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Společnost',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Úkol',
  'Opportunities' => 'Příležitost',
  'Products' => 'Produkt',
  'Quotes' => 'Nabídka',
  'Bugs' => 'Chyby',
  'Cases' => 'Případ:',
  'Leads' => 'Příležitost',
  'Project' => 'Projekty',
  'ProjectTask' => 'Projektové úkoly',
  'Prospects' => 'Kontakt',
  'KBContents' => 'Znalostní báze',
  'RevenueLineItems' => 'Řádky obchodu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.sugar_moduleList.php

 //created: 2016-07-25 21:53:52

$app_list_strings['moduleList']['RevenueLineItems']='Revenue detaljposter';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/cs_CZ.sugar_record_type_display.php

 // created: 2016-07-25 21:53:52

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Účet',
  'Opportunities' => 'Příležitost',
  'Cases' => 'Případ:',
  'Leads' => 'Příležitost',
  'Contacts' => 'Kontakty',
  'Products' => 'Produkt',
  'Quotes' => 'Nabídka',
  'Bugs' => 'Chyba:',
  'Project' => 'Projekty',
  'Prospects' => 'Kontakt',
  'ProjectTask' => 'Projektové úkoly',
  'Tasks' => 'Úkol',
  'KBContents' => 'Znalostní báze',
  'RevenueLineItems' => 'Řádky obchodu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.sugar_record_type_display.php

 // created: 2016-07-25 21:53:53

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Virksomhed',
  'Opportunities' => 'Salgsmulighed',
  'Cases' => 'Sag',
  'Leads' => 'Kundeemne',
  'Contacts' => 'Kontakter',
  'Products' => 'Angiven linjepost',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Fejl',
  'Project' => 'Projekt',
  'Prospects' => 'Mål:',
  'ProjectTask' => 'Projektopgave',
  'Tasks' => 'Opgave',
  'KBContents' => 'Videnbase',
  'RevenueLineItems' => 'Revenue detaljposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:53

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Virksomhed',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Salgsmulighed',
  'Tasks' => 'Opgave',
  'ProductTemplates' => 'Produktkatalog',
  'Quotes' => 'Tilbud',
  'Products' => 'Angiven linjepost',
  'Contracts' => 'Kontrakt',
  'Emails' => 'E-mail',
  'Bugs' => 'Fejl',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektopgave',
  'Prospects' => 'Mål:',
  'Cases' => 'Sag',
  'Leads' => 'Kundeemne',
  'Meetings' => 'Møde',
  'Calls' => 'Opkald',
  'KBContents' => 'Videnbase',
  'RevenueLineItems' => 'Revenue detaljposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.sugar_moduleList.php

 //created: 2016-07-25 21:53:53

$app_list_strings['moduleList']['RevenueLineItems']='Umsatzposten';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/da_DK.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:53

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Virksomhed',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Opgave',
  'Opportunities' => 'Salgsmulighed',
  'Products' => 'Angiven linjepost',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Fejl',
  'Cases' => 'Sag',
  'Leads' => 'Kundeemne',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektopgave',
  'Prospects' => 'Mål:',
  'KBContents' => 'Videnbase',
  'RevenueLineItems' => 'Revenue detaljposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:54

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Firma',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Verkaufschance',
  'Tasks' => 'Aufgabe',
  'ProductTemplates' => 'Produktkatalog',
  'Quotes' => 'Angebot',
  'Products' => 'Produkt',
  'Contracts' => 'Vertrag',
  'Emails' => 'E-Mail',
  'Bugs' => 'Fehler',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektaufgabe',
  'Prospects' => 'Zielkontakt',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Meetings' => 'Meeting',
  'Calls' => 'Anruf',
  'KBContents' => 'Wissensdatenbank',
  'RevenueLineItems' => 'Umsatzposten',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:54

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Firma',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Aufgabe',
  'Opportunities' => 'Verkaufschance',
  'Products' => 'Produkt',
  'Quotes' => 'Angebot',
  'Bugs' => 'Fehler',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektaufgabe',
  'Prospects' => 'Zielkontakt',
  'KBContents' => 'Wissensdatenbank',
  'RevenueLineItems' => 'Umsatzposten',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.sugar_record_type_display.php

 // created: 2016-07-25 21:53:54

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Firma',
  'Opportunities' => 'Verkaufschance',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Contacts' => 'Kontakte',
  'Products' => 'Produkt',
  'Quotes' => 'Angebot',
  'Bugs' => 'Fehler',
  'Project' => 'Projekt',
  'Prospects' => 'Zielkontakt',
  'ProjectTask' => 'Projektaufgabe',
  'Tasks' => 'Aufgabe',
  'KBContents' => 'Wissensdatenbank',
  'RevenueLineItems' => 'Umsatzposten',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/el_EL.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:55

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Λογαριασμός',
  'Contacts' => 'Επαφή',
  'Tasks' => 'Εργασία',
  'Opportunities' => 'Ευκαιρία',
  'Products' => 'Γραμμή Εισηγμένων Ειδών',
  'Quotes' => 'Προσφορά',
  'Bugs' => 'Σφάλματα',
  'Cases' => 'Υπόθεση',
  'Leads' => 'Δυνητικός Πελάτης',
  'Project' => 'Έργο',
  'ProjectTask' => 'Εργασία Έργου',
  'Prospects' => 'Στόχος',
  'KBContents' => 'Βάση Γνώσεων',
  'RevenueLineItems' => 'Γραμμή Εσόδων',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/el_EL.sugar_moduleList.php

 //created: 2016-07-25 21:53:55

$app_list_strings['moduleList']['RevenueLineItems']='Γραμμή Εσόδων';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/el_EL.sugar_record_type_display.php

 // created: 2016-07-25 21:53:55

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Λογαριασμός',
  'Opportunities' => 'Ευκαιρία',
  'Cases' => 'Υπόθεση',
  'Leads' => 'Δυνητικός Πελάτης',
  'Contacts' => 'Επαφές',
  'Products' => 'Γραμμή Εισηγμένων Ειδών',
  'Quotes' => 'Προσφορά',
  'Bugs' => 'Σφάλμα',
  'Project' => 'Έργο',
  'Prospects' => 'Στόχος',
  'ProjectTask' => 'Εργασία Έργου',
  'Tasks' => 'Εργασία',
  'KBContents' => 'Βάση Γνώσεων',
  'RevenueLineItems' => 'Γραμμή Εσόδων',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/el_EL.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:55

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Λογαριασμός',
  'Contacts' => 'Επαφή',
  'Opportunities' => 'Ευκαιρία',
  'Tasks' => 'Εργασία',
  'ProductTemplates' => 'Κατάλογος Προϊόντος',
  'Quotes' => 'Προσφορά',
  'Products' => 'Γραμμή Εισηγμένων Ειδών',
  'Contracts' => 'Σύμβαση',
  'Emails' => 'Οποιοδήποτε Email',
  'Bugs' => 'Σφάλμα',
  'Project' => 'Έργο',
  'ProjectTask' => 'Εργασία Έργου',
  'Prospects' => 'Στόχος',
  'Cases' => 'Υπόθεση',
  'Leads' => 'Δυνητικός Πελάτης',
  'Meetings' => 'Συνάντηση',
  'Calls' => 'Κλήση',
  'KBContents' => 'Βάση Γνώσεων',
  'RevenueLineItems' => 'Γραμμή Εσόδων',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:56

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Cuenta',
  'Contacts' => 'Contacto',
  'Tasks' => 'Tarea',
  'Opportunities' => 'Oportunidad',
  'Products' => 'Línea de la Oferta',
  'Quotes' => 'Presupuesto',
  'Bugs' => 'Incidencias',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Público Objetivo',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Líneas de Ingreso',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:56

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Cuenta',
  'Contacts' => 'Contacto',
  'Opportunities' => 'Oportunidad',
  'Tasks' => 'Tarea',
  'ProductTemplates' => 'Catálogo de Productos',
  'Quotes' => 'Presupuesto',
  'Products' => 'Línea de la Oferta',
  'Contracts' => 'Contrato',
  'Emails' => 'Correo electrónico',
  'Bugs' => 'Incidencia',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Público Objetivo',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Meetings' => 'Reunión',
  'Calls' => 'Llamada',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Líneas de Ingreso',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_moduleList.php

 //created: 2016-07-25 21:53:56

$app_list_strings['moduleList']['RevenueLineItems']='Líneas de Ingreso';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/fr_FR.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:57

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contact',
  'Tasks' => 'Tâche',
  'Opportunities' => 'Affaire',
  'Products' => 'Ligne de devis',
  'Quotes' => 'Devis',
  'Bugs' => 'Bugs',
  'Cases' => 'Ticket',
  'Leads' => 'Lead',
  'Project' => 'Projet',
  'ProjectTask' => 'Tâche Projet',
  'Prospects' => 'Cible',
  'KBContents' => 'Base de connaissances',
  'RevenueLineItems' => 'Lignes de revenu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/fr_FR.sugar_moduleList.php

 //created: 2016-07-25 21:53:57

$app_list_strings['moduleList']['pmse_Emails_Templates']='Modèles d\'email des processus';
$app_list_strings['moduleList']['Queues']='Files d\'attente';
$app_list_strings['moduleList']['EmailTemplates']='Modèle d\'email';
$app_list_strings['moduleList']['RevenueLineItems']='Lignes de revenu';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_ES.sugar_record_type_display.php

 // created: 2016-07-25 21:53:57

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Cuenta',
  'Opportunities' => 'Oportunidad',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Contacts' => 'Contactos',
  'Products' => 'Línea de la Oferta',
  'Quotes' => 'Presupuesto',
  'Bugs' => 'Incidencia',
  'Project' => 'Proyecto',
  'Prospects' => 'Público Objetivo',
  'ProjectTask' => 'Tarea de Proyecto',
  'Tasks' => 'Tarea',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Líneas de Ingreso',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/fr_FR.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:58

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contact',
  'Opportunities' => 'Affaire',
  'Tasks' => 'Tâche',
  'ProductTemplates' => 'Catalogue Produits',
  'Quotes' => 'Devis',
  'Products' => 'Ligne de devis',
  'Contracts' => 'Contrat',
  'Emails' => 'Composer un email',
  'Bugs' => 'Bug',
  'Project' => 'Projet',
  'ProjectTask' => 'Tâche Projet',
  'Prospects' => 'Cible',
  'Cases' => 'Ticket',
  'Leads' => 'Lead',
  'Meetings' => 'Réunion',
  'Calls' => 'Appel',
  'KBContents' => 'Base de connaissances',
  'RevenueLineItems' => 'Lignes de revenu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/he_IL.sugar_moduleList.php

 //created: 2016-07-25 21:53:58

$app_list_strings['moduleList']['RevenueLineItems']='שורות פרטי הכנסה';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/fr_FR.sugar_record_type_display.php

 // created: 2016-07-25 21:53:58

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Compte',
  'Opportunities' => 'Affaire',
  'Cases' => 'Ticket',
  'Leads' => 'Lead',
  'Contacts' => 'Contacts',
  'Products' => 'Ligne de devis',
  'Quotes' => 'Devis',
  'Bugs' => 'Bug',
  'Project' => 'Projet',
  'Prospects' => 'Cible',
  'ProjectTask' => 'Tâche Projet',
  'Tasks' => 'Tâche',
  'KBContents' => 'Base de connaissances',
  'RevenueLineItems' => 'Lignes de revenu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/he_IL.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:59

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'חשבון',
  'Contacts' => 'איש קשר',
  'Tasks' => 'משימה',
  'Opportunities' => 'הזדמנות',
  'Products' => 'שורת פריט מצוטט',
  'Quotes' => 'הצעת מחיר',
  'Bugs' => 'באגים',
  'Cases' => 'פניית שירות',
  'Leads' => 'ליד',
  'Project' => 'פרויקט',
  'ProjectTask' => 'משימת הפרויקט',
  'Prospects' => 'מטרה',
  'KBContents' => 'מרכז מידע',
  'RevenueLineItems' => 'שורות פרטי הכנסה',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/he_IL.sugar_record_type_display.php

 // created: 2016-07-25 21:53:59

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'חשבון',
  'Opportunities' => 'הזדמנות',
  'Cases' => 'פניית שירות',
  'Leads' => 'ליד',
  'Contacts' => 'אנשי קשר',
  'Products' => 'שורת פריט מצוטט',
  'Quotes' => 'הצעת מחיר',
  'Bugs' => 'באג',
  'Project' => 'פרויקט',
  'Prospects' => 'מטרה',
  'ProjectTask' => 'משימת הפרויקט',
  'Tasks' => 'משימה',
  'KBContents' => 'מרכז מידע',
  'RevenueLineItems' => 'שורות פרטי הכנסה',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/he_IL.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:59

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'חשבון',
  'Contacts' => 'איש קשר',
  'Opportunities' => 'הזדמנות',
  'Tasks' => 'משימה',
  'ProductTemplates' => 'קטלוג מוצרים',
  'Quotes' => 'הצעת מחיר',
  'Products' => 'שורת פריט מצוטט',
  'Contracts' => 'חוזה',
  'Emails' => 'Any Email',
  'Bugs' => 'באג',
  'Project' => 'פרויקט',
  'ProjectTask' => 'משימת הפרויקט',
  'Prospects' => 'מטרה',
  'Cases' => 'פניית שירות',
  'Leads' => 'ליד',
  'Meetings' => 'פגישה',
  'Calls' => 'שיחת טלפון',
  'KBContents' => 'מרכז מידע',
  'RevenueLineItems' => 'שורות פרטי הכנסה',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:00

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Kliens',
  'Contacts' => 'Kapcsolat',
  'Tasks' => 'Feladat',
  'Opportunities' => 'Lehetőség',
  'Products' => 'Megajánlott Tétel',
  'Quotes' => 'Árajánlat',
  'Bugs' => 'Hibák',
  'Cases' => 'Eset',
  'Leads' => 'Ajánlás',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektfeladat',
  'Prospects' => 'Cél',
  'KBContents' => 'Tudásbázis',
  'RevenueLineItems' => 'Bevétel sorok',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:00

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Kliens',
  'Contacts' => 'Kapcsolat',
  'Opportunities' => 'Lehetőség',
  'Tasks' => 'Feladat',
  'ProductTemplates' => 'Termékkatalógus',
  'Quotes' => 'Árajánlat',
  'Products' => 'Megajánlott Tétel',
  'Contracts' => 'Szerződés',
  'Emails' => 'E-mail:',
  'Bugs' => 'Hiba',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektfeladat',
  'Prospects' => 'Cél',
  'Cases' => 'Eset',
  'Leads' => 'Ajánlás',
  'Meetings' => 'Találkozó',
  'Calls' => 'Hívás',
  'KBContents' => 'Tudásbázis',
  'RevenueLineItems' => 'Bevétel sorok',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.sugar_moduleList.php

 //created: 2016-07-25 21:54:00

$app_list_strings['moduleList']['RevenueLineItems']='Bevétel sorok';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/it_it.sugar_moduleList.php

 //created: 2016-07-25 21:54:01

$app_list_strings['moduleList']['RevenueLineItems']='Elementi dell´Opportunità';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.sugar_record_type_display.php

 // created: 2016-07-25 21:54:01

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Kliens',
  'Opportunities' => 'Lehetőség',
  'Cases' => 'Eset',
  'Leads' => 'Ajánlás',
  'Contacts' => 'Kapcsolatok',
  'Products' => 'Megajánlott Tétel',
  'Quotes' => 'Árajánlat',
  'Bugs' => 'Hiba',
  'Project' => 'Projekt',
  'Prospects' => 'Cél',
  'ProjectTask' => 'Projektfeladat',
  'Tasks' => 'Feladat',
  'KBContents' => 'Tudásbázis',
  'RevenueLineItems' => 'Bevétel sorok',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/it_it.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:02

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Azienda',
  'Contacts' => 'Contatto',
  'Opportunities' => 'Opportunità',
  'Tasks' => 'Compito',
  'ProductTemplates' => 'Catalogo Prodotti',
  'Quotes' => 'Offerta',
  'Products' => 'Prodotto',
  'Contracts' => 'Contratto',
  'Emails' => 'Email alternativo',
  'Bugs' => 'Bug',
  'Project' => 'Progetto',
  'ProjectTask' => 'Compiti di Progetto',
  'Prospects' => 'Obiettivo',
  'Cases' => 'Reclamo',
  'Leads' => 'Lead',
  'Meetings' => 'Riunione',
  'Calls' => 'Chiamata',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Elementi dell´Opportunità',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/it_it.sugar_record_type_display.php

 // created: 2016-07-25 21:54:02

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Azienda',
  'Opportunities' => 'Opportunità',
  'Cases' => 'Reclamo',
  'Leads' => 'Lead',
  'Contacts' => 'Contatti',
  'Products' => 'Prodotto',
  'Quotes' => 'Offerta',
  'Bugs' => 'Bug',
  'Project' => 'Progetto',
  'Prospects' => 'Obiettivo',
  'ProjectTask' => 'Compiti di Progetto',
  'Tasks' => 'Compito',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Elementi dell´Opportunità',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/it_it.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:02

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Azienda',
  'Contacts' => 'Contatto',
  'Tasks' => 'Compito',
  'Opportunities' => 'Opportunità',
  'Products' => 'Prodotto',
  'Quotes' => 'Offerta',
  'Bugs' => 'Bug',
  'Cases' => 'Reclamo',
  'Leads' => 'Lead',
  'Project' => 'Progetto',
  'ProjectTask' => 'Compiti di Progetto',
  'Prospects' => 'Obiettivo',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Elementi dell´Opportunità',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:03

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => '取引先',
  'Contacts' => '取引先担当者',
  'Opportunities' => '商談',
  'Tasks' => 'タスク',
  'ProductTemplates' => '商品カタログ',
  'Quotes' => '見積',
  'Products' => '見積済商品',
  'Contracts' => '契約',
  'Emails' => 'Eメール',
  'Bugs' => 'バグトラッカー',
  'Project' => 'プロジェクト',
  'ProjectTask' => 'プロジェクトタスク',
  'Prospects' => 'ターゲット',
  'Cases' => 'ケース',
  'Leads' => 'リード',
  'Meetings' => '会議',
  'Calls' => '電話',
  'KBContents' => 'ナレッジベース',
  'RevenueLineItems' => '商談品目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:03

$app_list_strings['parent_type_display']=array (
  'Accounts' => '取引先',
  'Contacts' => '取引先担当者',
  'Tasks' => 'タスク',
  'Opportunities' => '商談',
  'Products' => '見積済商品',
  'Quotes' => '見積',
  'Bugs' => 'バグトラッカー',
  'Cases' => 'ケース',
  'Leads' => 'リード',
  'Project' => 'プロジェクト',
  'ProjectTask' => 'プロジェクトタスク',
  'Prospects' => 'ターゲット',
  'KBContents' => 'ナレッジベース',
  'RevenueLineItems' => '商談品目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_moduleList.php

 //created: 2016-07-25 21:54:03

$app_list_strings['moduleList']['RevenueLineItems']='商談品目';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.sugar_moduleList.php

 //created: 2016-07-25 21:54:04

$app_list_strings['moduleList']['RevenueLineItems']='매출라인 품목목록';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ja_JP.sugar_record_type_display.php

 // created: 2016-07-25 21:54:04

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => '取引先',
  'Opportunities' => '商談',
  'Cases' => 'ケース',
  'Leads' => 'リード',
  'Contacts' => '取引先担当者',
  'Products' => '見積済商品',
  'Quotes' => '見積',
  'Bugs' => 'バグトラッカー',
  'Project' => 'プロジェクト',
  'Prospects' => 'ターゲット',
  'ProjectTask' => 'プロジェクトタスク',
  'Tasks' => 'タスク',
  'KBContents' => 'ナレッジベース',
  'RevenueLineItems' => '商談品目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:05

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => '거래처',
  'Contacts' => '연락처',
  'Opportunities' => '예비고객',
  'Tasks' => '과제',
  'ProductTemplates' => '상품 책자',
  'Quotes' => '견적',
  'Products' => '견적 라인아이템',
  'Contracts' => '계약',
  'Emails' => '다른 이메일:',
  'Bugs' => '버그',
  'Project' => '프로젝트',
  'ProjectTask' => '프로젝트 과제',
  'Prospects' => '목표고객',
  'Cases' => '사례',
  'Leads' => '관심고객:',
  'Meetings' => '회의',
  'Calls' => '전화상담',
  'KBContents' => '지식 기반',
  'RevenueLineItems' => '매출라인 품목목록',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.sugar_record_type_display.php

 // created: 2016-07-25 21:54:05

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => '거래처',
  'Opportunities' => '예비고객',
  'Cases' => '사례',
  'Leads' => '관심고객:',
  'Contacts' => '연락처목록',
  'Products' => '견적 라인아이템',
  'Quotes' => '견적',
  'Bugs' => '버그',
  'Project' => '프로젝트',
  'Prospects' => '목표고객',
  'ProjectTask' => '프로젝트 과제',
  'Tasks' => '과제',
  'KBContents' => '지식 기반',
  'RevenueLineItems' => '매출라인 품목목록',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:04

$app_list_strings['parent_type_display']=array (
  'Accounts' => '거래처',
  'Contacts' => '연락처',
  'Tasks' => '과제',
  'Opportunities' => '예비고객',
  'Products' => '견적 라인아이템',
  'Quotes' => '견적',
  'Bugs' => '결함',
  'Cases' => '사례',
  'Leads' => '관심고객:',
  'Project' => '프로젝트',
  'ProjectTask' => '프로젝트 과제',
  'Prospects' => '목표고객',
  'KBContents' => '지식 기반',
  'RevenueLineItems' => '매출라인 품목목록',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/lv_LV.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:06

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Uzņēmums',
  'Contacts' => 'Kontaktpersona',
  'Opportunities' => 'Iespēja',
  'Tasks' => 'Uzdevums',
  'ProductTemplates' => 'Produktu katalogs',
  'Quotes' => 'Piedāvājums',
  'Products' => 'Piedāvājuma rinda',
  'Contracts' => 'Līgums',
  'Emails' => 'Jebkāds e-pasts',
  'Bugs' => 'Kļūda',
  'Project' => 'Projekts',
  'ProjectTask' => 'Projekta uzdevums',
  'Prospects' => 'Mērķis',
  'Cases' => 'Pieteikums',
  'Leads' => 'Interesents',
  'Meetings' => 'Tikšanās',
  'Calls' => 'Zvans',
  'KBContents' => 'Zināšanu bāze',
  'RevenueLineItems' => 'Ieņēmumu posteņi',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/lv_LV.sugar_record_type_display.php

 // created: 2016-07-25 21:54:06

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Uzņēmums',
  'Opportunities' => 'Iespēja',
  'Cases' => 'Pieteikums',
  'Leads' => 'Interesents',
  'Contacts' => 'Kontaktpersonas',
  'Products' => 'Piedāvājuma rinda',
  'Quotes' => 'Piedāvājums',
  'Bugs' => 'Kļūda',
  'Project' => 'Projekts',
  'Prospects' => 'Mērķis',
  'ProjectTask' => 'Projekta uzdevums',
  'Tasks' => 'Uzdevums',
  'KBContents' => 'Zināšanu bāze',
  'RevenueLineItems' => 'Ieņēmumu posteņi',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/lv_LV.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:06

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Uzņēmums',
  'Contacts' => 'Kontaktpersona',
  'Tasks' => 'Uzdevums',
  'Opportunities' => 'Iespēja',
  'Products' => 'Piedāvājuma rinda',
  'Quotes' => 'Piedāvājums',
  'Bugs' => 'Kļūdas',
  'Cases' => 'Pieteikums',
  'Leads' => 'Interesents',
  'Project' => 'Projekts',
  'ProjectTask' => 'Projekta uzdevums',
  'Prospects' => 'Mērķis',
  'KBContents' => 'Zināšanu bāze',
  'RevenueLineItems' => 'Ieņēmumu posteņi',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/lv_LV.sugar_moduleList.php

 //created: 2016-07-25 21:54:06

$app_list_strings['moduleList']['RevenueLineItems']='Ieņēmumu posteņi';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:07

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Oppgave',
  'Opportunities' => 'Muligheter',
  'Products' => 'Produkt',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Feil',
  'Cases' => 'Sak',
  'Leads' => 'Lead',
  'Project' => 'Prosjekt',
  'ProjectTask' => 'Prosjektoppgave',
  'Prospects' => 'Mål',
  'KBContents' => 'KB-dokumenter',
  'RevenueLineItems' => 'Omsetningsposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.sugar_moduleList.php

 //created: 2016-07-25 21:54:07

$app_list_strings['moduleList']['RevenueLineItems']='Omsetningsposter';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:07

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Muligheter',
  'Tasks' => 'Oppgave',
  'ProductTemplates' => 'Produktkatalog',
  'Quotes' => 'Tilbud',
  'Products' => 'Produkt',
  'Contracts' => 'Kontrakt',
  'Emails' => 'E-postadresse',
  'Bugs' => 'Feil',
  'Project' => 'Prosjekt',
  'ProjectTask' => 'Prosjektoppgave',
  'Prospects' => 'Mål',
  'Cases' => 'Sak',
  'Leads' => 'Lead',
  'Meetings' => 'Møte',
  'Calls' => 'Opringning:',
  'KBContents' => 'KB-dokumenter',
  'RevenueLineItems' => 'Omsetningsposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:08

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Persoon',
  'Tasks' => 'Taak',
  'Opportunities' => 'Opportunity',
  'Products' => 'Geoffreerd product',
  'Quotes' => 'Offerte',
  'Bugs' => 'Bugs',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Project' => 'Project',
  'ProjectTask' => 'Projecttaak',
  'Prospects' => 'Target',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Opportunityregels',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.sugar_record_type_display.php

 // created: 2016-07-25 21:54:08

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Konto',
  'Opportunities' => 'Muligheter',
  'Cases' => 'Sak',
  'Leads' => 'Lead',
  'Contacts' => 'Kontakter',
  'Products' => 'Produkt',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Feil',
  'Project' => 'Prosjekt',
  'Prospects' => 'Mål',
  'ProjectTask' => 'Prosjektoppgave',
  'Tasks' => 'Oppgave',
  'KBContents' => 'KB-dokumenter',
  'RevenueLineItems' => 'Omsetningsposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.sugar_moduleList.php

 //created: 2016-07-25 21:54:08

$app_list_strings['moduleList']['TrackerQueries']='Tracker query\'s';
$app_list_strings['moduleList']['RevenueLineItems']='Opportunityregels';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.sugar_record_type_display.php

 // created: 2016-07-25 21:54:09

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Account',
  'Opportunities' => 'Opportunity',
  'Cases' => 'Casus',
  'Leads' => 'Lead',
  'Contacts' => 'Contactpersonen',
  'Products' => 'Geoffreerd product',
  'Quotes' => 'Offerte',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'Prospects' => 'Target',
  'ProjectTask' => 'Projecttaak',
  'Tasks' => 'Taak',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Opportunityregels',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_moduleList.php

 //created: 2016-07-25 21:54:09

$app_list_strings['moduleList']['RevenueLineItems']='Pozycje szansy';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:09

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Persoon',
  'Opportunities' => 'Opportunity',
  'Tasks' => 'Taak',
  'ProductTemplates' => 'Productcatalogus',
  'Quotes' => 'Offerte',
  'Products' => 'Geoffreerd product',
  'Contracts' => 'Contract',
  'Emails' => 'E-mailadres',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'ProjectTask' => 'Projecttaak',
  'Prospects' => 'Target',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Meetings' => 'Afspraak',
  'Calls' => 'Telefoongesprek',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Opportunityregels',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:10

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Kontrahent',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Szansa',
  'Tasks' => 'Zadanie',
  'ProductTemplates' => 'Katalog produktów',
  'Quotes' => 'Oferta',
  'Products' => 'Pozycja oferty',
  'Contracts' => 'Umowa',
  'Emails' => 'E-mail',
  'Bugs' => 'Błąd',
  'Project' => 'Projekt',
  'ProjectTask' => 'Zadanie projektowe',
  'Prospects' => 'Odbiorca',
  'Cases' => 'Zgłoszenie',
  'Leads' => 'Potencjalny klient',
  'Meetings' => 'Spotkanie',
  'Calls' => 'Rozmowa tel.',
  'KBContents' => 'Baza wiedzy',
  'RevenueLineItems' => 'Pozycje szansy',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_record_type_display.php

 // created: 2016-07-25 21:54:10

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Kontrahent',
  'Opportunities' => 'Szansa',
  'Cases' => 'Zgłoszenie',
  'Leads' => 'Potencjalny klient',
  'Contacts' => 'Kontakty',
  'Products' => 'Pozycja oferty',
  'Quotes' => 'Oferta',
  'Bugs' => 'Błąd',
  'Project' => 'Projekt',
  'Prospects' => 'Odbiorca',
  'ProjectTask' => 'Zadanie projektowe',
  'Tasks' => 'Zadanie',
  'KBContents' => 'Baza wiedzy',
  'RevenueLineItems' => 'Pozycje szansy',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pl_PL.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:10

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Kontrahent',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Zadanie',
  'Opportunities' => 'Szansa',
  'Products' => 'Pozycja oferty',
  'Quotes' => 'Oferta',
  'Bugs' => 'Błędy',
  'Cases' => 'Zgłoszenie',
  'Leads' => 'Potencjalny klient',
  'Project' => 'Projekt',
  'ProjectTask' => 'Zadanie projektowe',
  'Prospects' => 'Odbiorca',
  'KBContents' => 'Baza wiedzy',
  'RevenueLineItems' => 'Pozycje szansy',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pt_PT.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Conta',
  'Contacts' => 'Contacto',
  'Tasks' => 'Tarefa',
  'Opportunities' => 'Oportunidade',
  'Products' => 'Item de Linha Cotado',
  'Quotes' => 'Cotação',
  'Bugs' => 'Erros',
  'Cases' => 'Ocorrência',
  'Leads' => 'Cliente Potencial',
  'Project' => 'Projecto',
  'ProjectTask' => 'Tarefa de Projecto',
  'Prospects' => 'Alvo',
  'KBContents' => 'Base de Conhecimento',
  'RevenueLineItems' => 'Itens de Linha de Receita',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pt_PT.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:11

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Conta',
  'Contacts' => 'Contacto',
  'Opportunities' => 'Oportunidade',
  'Tasks' => 'Tarefa',
  'ProductTemplates' => 'Catálogo de Produtos',
  'Quotes' => 'Cotação',
  'Products' => 'Item de Linha Cotado',
  'Contracts' => 'Contrato',
  'Emails' => 'E-mail',
  'Bugs' => 'Erro',
  'Project' => 'Projecto',
  'ProjectTask' => 'Tarefa de Projecto',
  'Prospects' => 'Alvo',
  'Cases' => 'Ocorrência',
  'Leads' => 'Cliente Potencial',
  'Meetings' => 'Reunião',
  'Calls' => 'Chamada Telefónica',
  'KBContents' => 'Base de Conhecimento',
  'RevenueLineItems' => 'Itens de Linha de Receita',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pt_PT.sugar_moduleList.php

 //created: 2016-07-25 21:54:11

$app_list_strings['moduleList']['RevenueLineItems']='Itens de Linha de Receita';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pt_PT.sugar_record_type_display.php

 // created: 2016-07-25 21:54:12

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Conta',
  'Opportunities' => 'Oportunidade',
  'Cases' => 'Ocorrência',
  'Leads' => 'Cliente Potencial',
  'Contacts' => 'Contactos',
  'Products' => 'Item de Linha Cotado',
  'Quotes' => 'Cotação',
  'Bugs' => 'Erro',
  'Project' => 'Projecto',
  'Prospects' => 'Alvo',
  'ProjectTask' => 'Tarefa de Projecto',
  'Tasks' => 'Tarefa',
  'KBContents' => 'Base de Conhecimento',
  'RevenueLineItems' => 'Itens de Linha de Receita',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ro_RO.sugar_moduleList.php

 //created: 2016-07-25 21:54:12

$app_list_strings['moduleList']['RevenueLineItems']='Linii de venit';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ro_RO.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:13

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Cont:',
  'Contacts' => 'lista contact',
  'Tasks' => 'Sarcina',
  'Opportunities' => 'Oportunitate',
  'Products' => 'Element din ofertă',
  'Quotes' => 'Ofertă',
  'Bugs' => 'Probleme',
  'Cases' => 'Caz:',
  'Leads' => 'Client potențial',
  'Project' => 'Proiect',
  'ProjectTask' => 'Sarcina de proiect',
  'Prospects' => 'Tinta:',
  'KBContents' => 'Baza de cunostinte',
  'RevenueLineItems' => 'Linii de venit',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ro_RO.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:13

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Cont:',
  'Contacts' => 'lista contact',
  'Opportunities' => 'Oportunitate',
  'Tasks' => 'Sarcina',
  'ProductTemplates' => 'Catalog de produse',
  'Quotes' => 'Ofertă',
  'Products' => 'Element din ofertă',
  'Contracts' => 'Contract',
  'Emails' => 'Oricare Email:',
  'Bugs' => 'Problema:',
  'Project' => 'Proiect',
  'ProjectTask' => 'Sarcina de proiect',
  'Prospects' => 'Tinta:',
  'Cases' => 'Caz:',
  'Leads' => 'Client potențial',
  'Meetings' => 'Intalnire:',
  'Calls' => 'Apel',
  'KBContents' => 'Baza de cunostinte',
  'RevenueLineItems' => 'Linii de venit',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ro_RO.sugar_record_type_display.php

 // created: 2016-07-25 21:54:14

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Cont:',
  'Opportunities' => 'Oportunitate',
  'Cases' => 'Caz:',
  'Leads' => 'Client potențial',
  'Contacts' => 'Contacte',
  'Products' => 'Element din ofertă',
  'Quotes' => 'Ofertă',
  'Bugs' => 'Problema:',
  'Project' => 'Proiect',
  'Prospects' => 'Tinta:',
  'ProjectTask' => 'Sarcina de proiect',
  'Tasks' => 'Sarcina',
  'KBContents' => 'Baza de cunostinte',
  'RevenueLineItems' => 'Linii de venit',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ru_RU.sugar_moduleList.php

 //created: 2016-07-25 21:54:14

$app_list_strings['moduleList']['RevenueLineItems']='Доход по продуктам';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ru_RU.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:14

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Tasks' => 'Задача',
  'Opportunities' => 'Сделка',
  'Products' => 'Продукт коммерческого предложения',
  'Quotes' => 'Коммерческое предложение',
  'Bugs' => 'Ошибки',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Project' => 'Проект',
  'ProjectTask' => 'Проектная задача',
  'Prospects' => 'Адресат',
  'KBContents' => 'База знаний',
  'RevenueLineItems' => 'Доход по продуктам',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sv_SE.sugar_moduleList.php

 //created: 2016-07-25 21:54:15

$app_list_strings['moduleList']['RevenueLineItems']='Intäktsposter';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ru_RU.sugar_record_type_display.php

 // created: 2016-07-25 21:54:15

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Контрагент',
  'Opportunities' => 'Сделка',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Contacts' => 'Контакты',
  'Products' => 'Продукт коммерческого предложения',
  'Quotes' => 'Коммерческое предложение',
  'Bugs' => 'Ошибка',
  'Project' => 'Проект',
  'Prospects' => 'Адресат',
  'ProjectTask' => 'Проектная задача',
  'Tasks' => 'Задача',
  'KBContents' => 'База знаний',
  'RevenueLineItems' => 'Доход по продуктам',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ru_RU.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:15

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Opportunities' => 'Сделка',
  'Tasks' => 'Задача',
  'ProductTemplates' => 'Каталог продуктов',
  'Quotes' => 'Коммерческое предложение',
  'Products' => 'Продукт коммерческого предложения',
  'Contracts' => 'Контракт',
  'Emails' => 'E-mail',
  'Bugs' => 'Ошибка',
  'Project' => 'Проект',
  'ProjectTask' => 'Проектная задача',
  'Prospects' => 'Адресат',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Meetings' => 'Встреча',
  'Calls' => 'Звонок',
  'KBContents' => 'База знаний',
  'RevenueLineItems' => 'Доход по продуктам',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sv_SE.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:16

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Affärsmöjlighet',
  'Tasks' => 'Uppgift',
  'ProductTemplates' => 'Produktkatalog',
  'Quotes' => 'Offert',
  'Products' => 'Produkt',
  'Contracts' => 'Kontrakt',
  'Emails' => 'Email',
  'Bugs' => 'Bugg',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektuppgift',
  'Prospects' => 'Mål',
  'Cases' => 'Ärende',
  'Leads' => 'Möjlig kund',
  'Meetings' => 'Möte',
  'Calls' => 'Telefonsamtal',
  'KBContents' => 'Kunskapsbas Dokument',
  'RevenueLineItems' => 'Intäktsposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sv_SE.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:16

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Uppgift',
  'Opportunities' => 'Affärsmöjlighet',
  'Products' => 'Produkt',
  'Quotes' => 'Offert',
  'Bugs' => 'Buggar',
  'Cases' => 'Ärende',
  'Leads' => 'Möjlig kund',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektuppgift',
  'Prospects' => 'Mål',
  'KBContents' => 'Kunskapsbas Dokument',
  'RevenueLineItems' => 'Intäktsposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sv_SE.sugar_record_type_display.php

 // created: 2016-07-25 21:54:16

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Konto',
  'Opportunities' => 'Affärsmöjlighet',
  'Cases' => 'Ärende',
  'Leads' => 'Möjlig kund',
  'Contacts' => 'Kontakter',
  'Products' => 'Produkt',
  'Quotes' => 'Offert',
  'Bugs' => 'Bugg',
  'Project' => 'Projekt',
  'Prospects' => 'Mål',
  'ProjectTask' => 'Projektuppgift',
  'Tasks' => 'Uppgift',
  'KBContents' => 'Kunskapsbas Dokument',
  'RevenueLineItems' => 'Intäktsposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/tr_TR.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:17

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Müşteri',
  'Contacts' => 'Kontak',
  'Opportunities' => 'Fırsat',
  'Tasks' => 'Görev',
  'ProductTemplates' => 'Ürün Kataloğu',
  'Quotes' => 'Teklif',
  'Products' => 'Teklif Kalemi',
  'Contracts' => 'Kontrat',
  'Emails' => 'E-Posta',
  'Bugs' => 'Hata',
  'Project' => 'Proje',
  'ProjectTask' => 'Proje Görevi',
  'Prospects' => 'Hedef',
  'Cases' => 'Talep',
  'Leads' => 'Potansiyel',
  'Meetings' => 'Toplantı',
  'Calls' => 'Tel.Araması',
  'KBContents' => 'Bilgi Tabanı',
  'RevenueLineItems' => 'Gelir Kalemleri',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/tr_TR.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:17

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Müşteri',
  'Contacts' => 'Kontak',
  'Tasks' => 'Görev',
  'Opportunities' => 'Fırsat',
  'Products' => 'Teklif Kalemi',
  'Quotes' => 'Teklif',
  'Bugs' => 'Hatalar',
  'Cases' => 'Talep',
  'Leads' => 'Potansiyel',
  'Project' => 'Proje',
  'ProjectTask' => 'Proje Görevi',
  'Prospects' => 'Hedef',
  'KBContents' => 'Bilgi Tabanı',
  'RevenueLineItems' => 'Gelir Kalemleri',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/tr_TR.sugar_moduleList.php

 //created: 2016-07-25 21:54:17

$app_list_strings['moduleList']['RevenueLineItems']='Gelir Kalemleri';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_TW.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:18

$app_list_strings['parent_type_display']=array (
  'Accounts' => '帳戶',
  'Contacts' => '連絡人',
  'Tasks' => '工作',
  'Opportunities' => '商機',
  'Products' => '報價項目',
  'Quotes' => '報價',
  'Bugs' => '錯誤',
  'Cases' => '實例',
  'Leads' => '潛在客戶',
  'Project' => '專案',
  'ProjectTask' => '專案工作',
  'Prospects' => '目標',
  'KBContents' => '知識庫',
  'RevenueLineItems' => '營收項目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/tr_TR.sugar_record_type_display.php

 // created: 2016-07-25 21:54:18

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Müşteri',
  'Opportunities' => 'Fırsat',
  'Cases' => 'Talep',
  'Leads' => 'Potansiyel',
  'Contacts' => 'Kontaklar',
  'Products' => 'Teklif Kalemi',
  'Quotes' => 'Teklif',
  'Bugs' => 'Hata',
  'Project' => 'Proje',
  'Prospects' => 'Hedef',
  'ProjectTask' => 'Proje Görevi',
  'Tasks' => 'Görev',
  'KBContents' => 'Bilgi Tabanı',
  'RevenueLineItems' => 'Gelir Kalemleri',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_TW.sugar_moduleList.php

 //created: 2016-07-25 21:54:18

$app_list_strings['moduleList']['RevenueLineItems']='營收項目';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_TW.sugar_record_type_display.php

 // created: 2016-07-25 21:54:19

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => '帳戶',
  'Opportunities' => '商機',
  'Cases' => '實例',
  'Leads' => '潛在客戶',
  'Contacts' => '連絡人',
  'Products' => '報價項目',
  'Quotes' => '報價',
  'Bugs' => '錯誤',
  'Project' => '專案',
  'Prospects' => '目標',
  'ProjectTask' => '專案工作',
  'Tasks' => '工作',
  'KBContents' => '知識庫',
  'RevenueLineItems' => '營收項目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_TW.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:19

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => '帳戶',
  'Contacts' => '連絡人',
  'Opportunities' => '商機',
  'Tasks' => '工作',
  'ProductTemplates' => '產品目錄',
  'Quotes' => '報價',
  'Products' => '報價項目',
  'Contracts' => '合約',
  'Emails' => '電子郵件',
  'Bugs' => '錯誤',
  'Project' => '專案',
  'ProjectTask' => '專案工作',
  'Prospects' => '目標',
  'Cases' => '實例',
  'Leads' => '潛在客戶',
  'Meetings' => '會議',
  'Calls' => '通話',
  'KBContents' => '知識庫',
  'RevenueLineItems' => '營收項目',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_CN.sugar_moduleList.php

 //created: 2016-07-25 21:54:19

$app_list_strings['moduleList']['RevenueLineItems']='营收单项';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_CN.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:20

$app_list_strings['parent_type_display']=array (
  'Accounts' => '帐户',
  'Contacts' => '联系人',
  'Tasks' => '任务',
  'Opportunities' => '商业机会',
  'Products' => '已报价单项',
  'Quotes' => '报价',
  'Bugs' => '缺陷',
  'Cases' => '客户反馈',
  'Leads' => '潜在客户',
  'Project' => '项目',
  'ProjectTask' => '项目任务',
  'Prospects' => '目标',
  'KBContents' => '知识库',
  'RevenueLineItems' => '营收单项',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_CN.sugar_record_type_display.php

 // created: 2016-07-25 21:54:20

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => '帐户',
  'Opportunities' => '商业机会',
  'Cases' => '客户反馈',
  'Leads' => '潜在客户',
  'Contacts' => '联系人',
  'Products' => '已报价单项',
  'Quotes' => '报价',
  'Bugs' => '错误',
  'Project' => '项目',
  'Prospects' => '目标',
  'ProjectTask' => '项目任务',
  'Tasks' => '任务',
  'KBContents' => '知识库',
  'RevenueLineItems' => '营收单项',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pt_BR.sugar_moduleList.php

 //created: 2016-07-25 21:54:20

$app_list_strings['moduleList']['RevenueLineItems']='Itens da linha de receita';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/zh_CN.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:20

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => '帐户',
  'Contacts' => '联系人',
  'Opportunities' => '商业机会',
  'Tasks' => '任务',
  'ProductTemplates' => '产品目录',
  'Quotes' => '报价',
  'Products' => '已报价单项',
  'Contracts' => '合同',
  'Emails' => '电子邮件',
  'Bugs' => '错误',
  'Project' => '项目',
  'ProjectTask' => '项目任务',
  'Prospects' => '目标',
  'Cases' => '客户反馈',
  'Leads' => '潜在客户',
  'Meetings' => '会议',
  'Calls' => '电话',
  'KBContents' => '知识库',
  'RevenueLineItems' => '营收单项',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pt_BR.sugar_record_type_display.php

 // created: 2016-07-25 21:54:21

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Conta',
  'Opportunities' => 'Oportunidade',
  'Cases' => 'Ocorrência',
  'Leads' => 'Potencial',
  'Contacts' => 'Contatos',
  'Products' => 'Item de Linha de Cotação',
  'Quotes' => 'Cotação',
  'Bugs' => 'Bug',
  'Project' => 'Projeto',
  'Prospects' => 'Alvo',
  'ProjectTask' => 'Tarefa de Projeto',
  'Tasks' => 'Tarefa',
  'KBContents' => 'Base de Conhecimento',
  'RevenueLineItems' => 'Itens da linha de receita',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pt_BR.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:21

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Conta',
  'Contacts' => 'Contato',
  'Opportunities' => 'Oportunidade',
  'Tasks' => 'Tarefa',
  'ProductTemplates' => 'Catálogo de Produtos',
  'Quotes' => 'Cotação',
  'Products' => 'Item de Linha de Cotação',
  'Contracts' => 'Contrato',
  'Emails' => 'E-mail',
  'Bugs' => 'Bug',
  'Project' => 'Projeto',
  'ProjectTask' => 'Tarefa de Projeto',
  'Prospects' => 'Alvo',
  'Cases' => 'Ocorrência',
  'Leads' => 'Potencial',
  'Meetings' => 'Reunião',
  'Calls' => 'Chamada',
  'KBContents' => 'Base de Conhecimento',
  'RevenueLineItems' => 'Itens da linha de receita',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/pt_BR.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:21

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Conta',
  'Contacts' => 'Contato',
  'Tasks' => 'Tarefa',
  'Opportunities' => 'Oportunidade',
  'Products' => 'Item de Linha de Cotação',
  'Quotes' => 'Cotação',
  'Bugs' => 'Bugs',
  'Cases' => 'Ocorrência',
  'Leads' => 'Potencial',
  'Project' => 'Projeto',
  'ProjectTask' => 'Tarefa de Projeto',
  'Prospects' => 'Alvo',
  'KBContents' => 'Base de Conhecimento',
  'RevenueLineItems' => 'Itens da linha de receita',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.sugar_moduleList.php

 //created: 2016-07-25 21:54:22

$app_list_strings['moduleList']['Products']='Elements de línies d\'oferta';
$app_list_strings['moduleList']['TaxRates']='Tipus d\'impostos';
$app_list_strings['moduleList']['ProspectLists']='Llistes d\'objectius';
$app_list_strings['moduleList']['Styleguide']='Guia d\'estil';
$app_list_strings['moduleList']['RevenueLineItems']='Línia d\'impostos articles';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:22

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contacte',
  'Opportunities' => 'Oportunitat',
  'Tasks' => 'Tasca',
  'ProductTemplates' => 'Catàleg de productes',
  'Quotes' => 'Pressupost',
  'Products' => 'Element de línia d\'oferta',
  'Contracts' => 'Contracte',
  'Emails' => 'Adreça electrònica',
  'Bugs' => 'Incidència',
  'Project' => 'Projecte',
  'ProjectTask' => 'Tasca de projecte',
  'Prospects' => 'Objectiu',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Meetings' => 'Reunió',
  'Calls' => 'Trucada',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Línia d\'impostos articles',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:22

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contacte',
  'Tasks' => 'Tasca',
  'Opportunities' => 'Oportunitat',
  'Products' => 'Element de línia d\'oferta',
  'Quotes' => 'Pressupost',
  'Bugs' => 'Incidències',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Project' => 'Projecte',
  'ProjectTask' => 'Tasca de projecte',
  'Prospects' => 'Objectiu',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Línia d\'impostos articles',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ca_ES.sugar_record_type_display.php

 // created: 2016-07-25 21:54:23

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Compte',
  'Opportunities' => 'Oportunitat',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Contacts' => 'Contactes',
  'Products' => 'Element de línia d\'oferta',
  'Quotes' => 'Pressupost',
  'Bugs' => 'Incidència',
  'Project' => 'Projecte',
  'Prospects' => 'Objectiu',
  'ProjectTask' => 'Tasca de projecte',
  'Tasks' => 'Tasca',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Línia d\'impostos articles',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sr_RS.sugar_moduleList.php

 //created: 2016-07-25 21:54:23

$app_list_strings['moduleList']['RevenueLineItems']='Stavke prihoda';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sr_RS.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:23

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Kompanija',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Zadatak',
  'Opportunities' => 'Prodajna prilika',
  'Products' => 'Proizvod',
  'Quotes' => 'Ponuda',
  'Bugs' => 'Defekti',
  'Cases' => 'Slučaj:',
  'Leads' => 'Potencijalni klijent',
  'Project' => 'Projekat',
  'ProjectTask' => 'Projektni Zadatak',
  'Prospects' => 'Cilj',
  'KBContents' => 'Baza Znanja',
  'RevenueLineItems' => 'Stavke prihoda',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sr_RS.sugar_record_type_display.php

 // created: 2016-07-25 21:54:24

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Kompanija',
  'Opportunities' => 'Prodajna prilika',
  'Cases' => 'Slučaj:',
  'Leads' => 'Potencijalni klijent',
  'Contacts' => 'Kontakti',
  'Products' => 'Proizvod',
  'Quotes' => 'Ponuda',
  'Bugs' => 'Defekt:',
  'Project' => 'Projekat',
  'Prospects' => 'Cilj',
  'ProjectTask' => 'Projektni Zadatak',
  'Tasks' => 'Zadatak',
  'KBContents' => 'Baza Znanja',
  'RevenueLineItems' => 'Stavke prihoda',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sk_SK.sugar_moduleList.php

 //created: 2016-07-25 21:54:24

$app_list_strings['moduleList']['UpgradeWizard']='Sprievodca modernizáciou';
$app_list_strings['moduleList']['RevenueLineItems']='Položky krivky výnosu';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sr_RS.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:24

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Kompanija',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Prodajna prilika',
  'Tasks' => 'Zadatak',
  'ProductTemplates' => 'Katalog proizvoda',
  'Quotes' => 'Ponuda',
  'Products' => 'Proizvod',
  'Contracts' => 'Ugovor',
  'Emails' => 'Bilo koji Email:',
  'Bugs' => 'Defekt:',
  'Project' => 'Projekat',
  'ProjectTask' => 'Projektni Zadatak',
  'Prospects' => 'Cilj',
  'Cases' => 'Slučaj:',
  'Leads' => 'Potencijalni klijent',
  'Meetings' => 'Sastanak:',
  'Calls' => 'Poziv',
  'KBContents' => 'Baza Znanja',
  'RevenueLineItems' => 'Stavke prihoda',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sk_SK.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:25

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Účet',
  'Contacts' => 'Kontakt:',
  'Tasks' => 'Úloha',
  'Opportunities' => 'Obchodná príležitosť',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Ponuka',
  'Bugs' => 'Chyby',
  'Cases' => 'Prípad',
  'Leads' => 'Príležitosť',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektová úloha',
  'Prospects' => 'Cieľ',
  'KBContents' => 'Báza znalostí',
  'RevenueLineItems' => 'Položky krivky výnosu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sk_SK.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:25

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Účet',
  'Contacts' => 'Kontakt:',
  'Opportunities' => 'Obchodná príležitosť',
  'Tasks' => 'Úloha',
  'ProductTemplates' => 'Katalóg produktov',
  'Quotes' => 'Ponuka',
  'Products' => 'Quoted Line Item',
  'Contracts' => 'Zmluva',
  'Emails' => 'Email:',
  'Bugs' => 'Chyba',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektová úloha',
  'Prospects' => 'Cieľ',
  'Cases' => 'Prípad',
  'Leads' => 'Príležitosť',
  'Meetings' => 'Schôdzka',
  'Calls' => 'Volanie',
  'KBContents' => 'Báza znalostí',
  'RevenueLineItems' => 'Položky krivky výnosu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sk_SK.sugar_record_type_display.php

 // created: 2016-07-25 21:54:26

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Účet',
  'Opportunities' => 'Obchodná príležitosť',
  'Cases' => 'Prípad',
  'Leads' => 'Príležitosť',
  'Contacts' => 'Kontakty',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Ponuka',
  'Bugs' => 'Chyba',
  'Project' => 'Projekt',
  'Prospects' => 'Cieľ',
  'ProjectTask' => 'Projektová úloha',
  'Tasks' => 'Úloha',
  'KBContents' => 'Báza znalostí',
  'RevenueLineItems' => 'Položky krivky výnosu',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:26

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'llogaritë',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Detyrë',
  'Opportunities' => 'Mundësi:',
  'Products' => 'Produkti',
  'Quotes' => 'Kuota',
  'Bugs' => 'Gabimet',
  'Cases' => 'Rast',
  'Leads' => 'udhëheqje',
  'Project' => 'Projekti',
  'ProjectTask' => 'Detyrat projektuese',
  'Prospects' => 'Synim',
  'KBContents' => 'baza e njohurisë',
  'RevenueLineItems' => 'Rreshti i llojeve të të ardhurave',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.sugar_moduleList.php

 //created: 2016-07-25 21:54:26

$app_list_strings['moduleList']['RevenueLineItems']='Rreshti i llojeve të të ardhurave';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.sugar_record_type_display.php

 // created: 2016-07-25 21:54:27

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'llogaritë',
  'Opportunities' => 'Mundësi:',
  'Cases' => 'Rast',
  'Leads' => 'udhëheqje',
  'Contacts' => 'Kontaktet',
  'Products' => 'Produkti',
  'Quotes' => 'Kuota',
  'Bugs' => 'Gabim',
  'Project' => 'Projekti',
  'Prospects' => 'Synim',
  'ProjectTask' => 'Detyrat projektuese',
  'Tasks' => 'Detyrë',
  'KBContents' => 'baza e njohurisë',
  'RevenueLineItems' => 'Rreshti i llojeve të të ardhurave',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/sq_AL.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:27

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'llogaritë',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Mundësi:',
  'Tasks' => 'Detyrë',
  'ProductTemplates' => 'Katalogu i produkteve',
  'Quotes' => 'Kuota',
  'Products' => 'Produkti',
  'Contracts' => 'Kontrata',
  'Emails' => 'Email',
  'Bugs' => 'Gabim',
  'Project' => 'Projekti',
  'ProjectTask' => 'Detyrat projektuese',
  'Prospects' => 'Synim',
  'Cases' => 'Rast',
  'Leads' => 'udhëheqje',
  'Meetings' => 'Mbledhje',
  'Calls' => 'Telefoni',
  'KBContents' => 'baza e njohurisë',
  'RevenueLineItems' => 'Rreshti i llojeve të të ardhurave',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_moduleList.php

 //created: 2016-07-25 21:54:27

$app_list_strings['moduleList']['RevenueLineItems']='Artículos de Línea de Ganancia';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_record_type_display.php

 // created: 2016-07-25 21:54:28

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Cuenta',
  'Opportunities' => 'Oportunidad',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Contacts' => 'Contactos',
  'Products' => 'Partida Individual Cotizada',
  'Quotes' => 'Cotizacion',
  'Bugs' => 'Error',
  'Project' => 'Proyecto',
  'Prospects' => 'Público Objetivo',
  'ProjectTask' => 'Tarea de Proyecto',
  'Tasks' => 'Tarea',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Artículos de Línea de Ganancia',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:28

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Cuenta',
  'Contacts' => 'Contacto',
  'Opportunities' => 'Oportunidad',
  'Tasks' => 'Tarea',
  'ProductTemplates' => 'Catálogo de Productos',
  'Quotes' => 'Cotizacion',
  'Products' => 'Partida Individual Cotizada',
  'Contracts' => 'Contrato',
  'Emails' => 'Correo Electrónico',
  'Bugs' => 'Error',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Público Objetivo',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Meetings' => 'Reunión',
  'Calls' => 'Llamada',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Artículos de Línea de Ganancia',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:28

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Cuenta',
  'Contacts' => 'Contacto',
  'Tasks' => 'Tarea',
  'Opportunities' => 'Oportunidad',
  'Products' => 'Partida Individual Cotizada',
  'Quotes' => 'Cotizacion',
  'Bugs' => 'Errores',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Público Objetivo',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Artículos de Línea de Ganancia',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/fi_FI.sugar_moduleList.php

 //created: 2016-07-25 21:54:29

$app_list_strings['moduleList']['RevenueLineItems']='Tuoterivit';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/fi_FI.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:29

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Jäsen',
  'Contacts' => 'Yhteystiedot',
  'Tasks' => 'Tehtävä',
  'Opportunities' => 'Myyntimahdollisuus',
  'Products' => 'Tarjottu tuoterivi',
  'Quotes' => 'Tarjous',
  'Bugs' => 'Bugit',
  'Cases' => 'Palvelupyyntö',
  'Leads' => 'Liidi',
  'Project' => 'Projekti',
  'ProjectTask' => 'Projektitehtävä',
  'Prospects' => 'Tavoite',
  'KBContents' => 'Tietämyskanta',
  'RevenueLineItems' => 'Tuoterivit',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/fi_FI.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:30

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Jäsen',
  'Contacts' => 'Yhteystiedot',
  'Opportunities' => 'Myyntimahdollisuus',
  'Tasks' => 'Tehtävä',
  'ProductTemplates' => 'Tuotekatalogi',
  'Quotes' => 'Tarjous',
  'Products' => 'Tarjottu tuoterivi',
  'Contracts' => 'Sopimus',
  'Emails' => 'Mikä tahansa sähköposti',
  'Bugs' => 'Bugi',
  'Project' => 'Projekti',
  'ProjectTask' => 'Projektitehtävä',
  'Prospects' => 'Tavoite',
  'Cases' => 'Palvelupyyntö',
  'Leads' => 'Liidi',
  'Meetings' => 'Kokous',
  'Calls' => 'Puhelu',
  'KBContents' => 'Tietämyskanta',
  'RevenueLineItems' => 'Tuoterivit',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/fi_FI.sugar_record_type_display.php

 // created: 2016-07-25 21:54:30

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Jäsen',
  'Opportunities' => 'Myyntimahdollisuus',
  'Cases' => 'Palvelupyyntö',
  'Leads' => 'Liidi',
  'Contacts' => 'Kontaktit',
  'Products' => 'Tarjottu tuoterivi',
  'Quotes' => 'Tarjous',
  'Bugs' => 'Bugi',
  'Project' => 'Projekti',
  'Prospects' => 'Tavoite',
  'ProjectTask' => 'Projektitehtävä',
  'Tasks' => 'Tehtävä',
  'KBContents' => 'Tietämyskanta',
  'RevenueLineItems' => 'Tuoterivit',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ar_SA.sugar_moduleList.php

 //created: 2016-07-25 21:54:30

$app_list_strings['moduleList']['RevenueLineItems']='بنود العائدات';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ar_SA.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:31

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'الحساب',
  'Contacts' => 'جهة الاتصال',
  'Tasks' => 'المهمة',
  'Opportunities' => 'الفرصة',
  'Products' => 'البند المسعر',
  'Quotes' => 'عرض السعر',
  'Bugs' => 'الأخطاء',
  'Cases' => 'الحالة',
  'Leads' => 'العميل المتوقع',
  'Project' => 'المشروع',
  'ProjectTask' => 'مهمة المشروع',
  'Prospects' => 'الهدف',
  'KBContents' => 'قاعدة المعارف',
  'RevenueLineItems' => 'بنود العائدات',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ar_SA.sugar_record_type_display.php

 // created: 2016-07-25 21:54:31

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'الحساب',
  'Opportunities' => 'الفرصة',
  'Cases' => 'الحالة',
  'Leads' => 'العميل المتوقع',
  'Contacts' => 'جهات الاتصال',
  'Products' => 'البند المسعر',
  'Quotes' => 'عرض السعر',
  'Bugs' => 'الخطأ',
  'Project' => 'المشروع',
  'Prospects' => 'الهدف',
  'ProjectTask' => 'مهمة المشروع',
  'Tasks' => 'المهمة',
  'KBContents' => 'قاعدة المعارف',
  'RevenueLineItems' => 'بنود العائدات',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ar_SA.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:31

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'الحساب',
  'Contacts' => 'جهة الاتصال',
  'Opportunities' => 'الفرصة',
  'Tasks' => 'المهمة',
  'ProductTemplates' => 'كتالوج المنتج',
  'Quotes' => 'عرض السعر',
  'Products' => 'البند المسعر',
  'Contracts' => 'العقد',
  'Emails' => 'البريد الإلكتروني',
  'Bugs' => 'الخطأ',
  'Project' => 'المشروع',
  'ProjectTask' => 'مهمة المشروع',
  'Prospects' => 'الهدف',
  'Cases' => 'الحالة',
  'Leads' => 'العميل المتوقع',
  'Meetings' => 'الاجتماع',
  'Calls' => 'المكالمة',
  'KBContents' => 'قاعدة المعارف',
  'RevenueLineItems' => 'بنود العائدات',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/uk_UA.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:32

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Opportunities' => 'Угода',
  'Tasks' => 'Задача',
  'ProductTemplates' => 'Каталог продукту',
  'Quotes' => 'Комерційна пропозиція',
  'Products' => 'Продукт комерційної пропозиції',
  'Contracts' => 'Контракт',
  'Emails' => 'Email',
  'Bugs' => 'Помилка',
  'Project' => 'Проект',
  'ProjectTask' => 'Задача проекту',
  'Prospects' => 'Цільова аудиторія споживачів',
  'Cases' => 'Звернення',
  'Leads' => 'Інтерес',
  'Meetings' => 'Зустріч',
  'Calls' => 'Дзвінок',
  'KBContents' => 'База знань',
  'RevenueLineItems' => 'Доходи за продукти',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/uk_UA.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:32

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Tasks' => 'Задача',
  'Opportunities' => 'Угода',
  'Products' => 'Продукт комерційної пропозиції',
  'Quotes' => 'Комерційна пропозиція',
  'Bugs' => 'Помилки',
  'Cases' => 'Звернення',
  'Leads' => 'Інтерес',
  'Project' => 'Проект',
  'ProjectTask' => 'Задача проекту',
  'Prospects' => 'Цільова аудиторія споживачів',
  'KBContents' => 'База знань',
  'RevenueLineItems' => 'Доходи за продукти',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/uk_UA.sugar_moduleList.php

 //created: 2016-07-25 21:54:32

$app_list_strings['moduleList']['MergeRecords']='Об\'єднання записів';
$app_list_strings['moduleList']['Connectors']='З\'єднувачі';
$app_list_strings['moduleList']['Feedbacks']='Зворотній зв\'язок';
$app_list_strings['moduleList']['RevenueLineItems']='Доходи за продукти';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/uk_UA.sugar_record_type_display.php

 // created: 2016-07-25 21:54:33

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Контрагент',
  'Opportunities' => 'Угода',
  'Cases' => 'Звернення',
  'Leads' => 'Інтерес',
  'Contacts' => 'Контакти',
  'Products' => 'Продукт комерційної пропозиції',
  'Quotes' => 'Комерційна пропозиція',
  'Bugs' => 'Помилка',
  'Project' => 'Проект',
  'Prospects' => 'Цільова аудиторія споживачів',
  'ProjectTask' => 'Задача проекту',
  'Tasks' => 'Задача',
  'KBContents' => 'База знань',
  'RevenueLineItems' => 'Доходи за продукти',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.ops-modules.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['ops_Backups'] = 'Backups';
$app_list_strings['moduleListSingular']['ops_Backups'] = 'Backup';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Grupo_empresas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GPE_Grupo_empresas'] = 'Grupo de empresas';
$app_list_strings['moduleListSingular']['GPE_Grupo_empresas'] = 'Grupo de empresa';
$app_list_strings['gpo_empresa_estado_list']['Activo'] = 'Activo';
$app_list_strings['gpo_empresa_estado_list']['Inactivo'] = 'Inactivo';
$app_list_strings['gpo_empresa_estado_list'][''] = '';
$app_list_strings['gpo_emp_edo_list']['Activo'] = 'Activo';
$app_list_strings['gpo_emp_edo_list']['Inactivo'] = 'Inactivo';
$app_list_strings['gpo_emp_edo_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Grupo_empresas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GPE_Grupo_empresas'] = 'Grupo de empresas';
$app_list_strings['moduleListSingular']['GPE_Grupo_empresas'] = 'Grupo de empresa';
$app_list_strings['gpo_empresa_estado_list']['Activo'] = 'Activo';
$app_list_strings['gpo_empresa_estado_list']['Inactivo'] = 'Inactivo';
$app_list_strings['gpo_empresa_estado_list'][''] = '';
$app_list_strings['gpo_emp_edo_list']['Activo'] = 'Activo';
$app_list_strings['gpo_emp_edo_list']['Inactivo'] = 'Inactivo';
$app_list_strings['gpo_emp_edo_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_estado_c_list.php

 // created: 2016-09-14 16:06:14

$app_list_strings['account_estado_c_list']=array (
  'Activo' => 'Activo',
  'Inactivo' => 'Inactivo',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_estado_segun_cartera_c_list.php

 // created: 2016-09-14 16:18:35

$app_list_strings['account_estado_segun_cartera_c_list']=array (
  'CV' => 'Cartera vigente',
  'CV130D' => 'Cartera vencida 1 a 30 días',
  'CV31120D' => 'Cartera vencida 31 a 120 días',
  'CV120D' => 'Cartera vencida 120 días',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_estado_bloqueo_c_list.php

 // created: 2016-09-14 17:03:37

$app_list_strings['account_estado_bloqueo_c_list']=array (
  'Desbloqueado' => 'Desbloqueado',
  'Bloqueado' => 'Bloqueado',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_codigo_bloqueo_auto_c_list.php

 // created: 2016-09-14 17:12:35

$app_list_strings['account_codigo_bloqueo_auto_c_list']=array (
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_codigo_bloqueo_manual_c_list.php

 // created: 2016-09-14 17:13:11

$app_list_strings['account_codigo_bloqueo_manual_c_list']=array (
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_codigo_bloqueo_c_list.php

 // created: 2016-09-14 17:16:37

$app_list_strings['account_codigo_bloqueo_c_list']=array (
  'CL' => 'Cliente Legal',
  'CI' => 'Cliente Inactivo',
  'DI' => 'Dictamen Incobrable',
  'Otro' => 'Otro',
  'CV' => 'Cartera vencida + x días',
  'CD' => 'Cheque devuelto',
  'LE' => 'Limite de crédito excedido',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_document_garantia_c_list.php

 // created: 2016-09-14 17:43:28

$app_list_strings['document_garantia_c_list']=array (
  'Seleccione' => '-- Seleccione --',
  'Pagare' => 'Pagaré',
  'Contrato' => 'Contrato',
  'Cheque_Postfechado' => 'Cheque Postfechado',
  'Autorizacion' => 'Autorización',
  'Fianza' => 'Fianza',
  'Prendaria' => 'Prendaria',
  'Hipotecaria' => 'Hipotecaria',
  'Anticipo' => 'Anticipo',
  'Otro' => 'Otro (solicitar comentarios)',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_account_type_c_list.php

 // created: 2016-09-14 17:58:24

$app_list_strings['account_type_c_list']=array (
  'Fisica' => 'Física',
  'Moral' => 'Moral',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_state_c_list.php

 // created: 2016-09-14 18:25:34

$app_list_strings['state_c_list']=array (
  'Seleccione' => '-- Seleccione --',
  'AGU' => 'Aguascalientes',
  'BCN' => 'Baja California',
  'BCS' => 'Baja California Sur',
  'CAM' => 'Campeche',
  'CHP' => 'Chiapas',
  'CHH' => 'Chihuahua',
  'COA' => 'Coahuila',
  'COL' => 'Colima',
  'CMX' => 'Ciudad de México',
  'DUR' => 'Durango',
  'GUA' => 'Guanajuato',
  'GRO' => 'Guerrero',
  'HID' => 'Hidalgo',
  'JAL' => 'Jalisco',
  'MEX' => 'Estado de México',
  'MIC' => 'Michoacán',
  'MOR' => 'Morelos',
  'NAY' => 'Nayarit',
  'NLE' => 'Nuevo León',
  'OAX' => 'Oaxaca',
  'PUE' => 'Puebla',
  'QUE' => 'Querétaro',
  'ROO' => 'Quintana Roo',
  'SLP' => 'San Luis Potosí',
  'SIN' => 'Sinaloa',
  'SON' => 'Sonora',
  'TAB' => 'Tabasco',
  'TAM' => 'Tamaulipas',
  'TLA' => 'Tlaxcala',
  'VER' => 'Veracruz',
  'YUC' => 'Yucatán',
  'ZAC' => 'Zacatecas',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_tipo_cuenta_c_list.php

 // created: 2016-09-14 19:03:51

$app_list_strings['tipo_cuenta_c_list']=array (
  'valor_sin_asignar' => 'Valor sin asignar',
  'cuenta_ar' => 'Cuenta Cliente AR',
  'grupo_empresas' => 'Grupo de Empresa AR',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_document_template_type_dom.php

 // created: 2016-09-21 15:52:48

$app_list_strings['document_template_type_dom']=array (
  '' => '',
  'mercadotecnia' => 'Mercadotecnia',
  'garantia' => 'Garantía',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_document_category_dom.php

 // created: 2016-09-21 16:26:23

$app_list_strings['document_category_dom']=array (
  '' => '',
  'pagare' => 'Pagaré',
  'contrato' => 'Contrato',
  'cheque_postfechado' => 'Cheque Postfechado',
  'autorizacion' => 'Autorización',
  'fianza' => 'Fianza',
  'prendaria' => 'Prendaria',
  'hipotecaria' => 'Hipotecaria',
  'anticipo' => 'Anticipo',
  'otro' => 'Otro',
  'carta_incobrabilidad' => 'Carta de incibrabilidad',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_sucursal_c_list.php

 // created: 2016-09-23 06:13:31

$app_list_strings['sucursal_c_list']=array (
  '0000' => 'Valor sin asignar',
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3227 => 'Cumbres',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_tipo_telefono_principal_list.php

 // created: 2016-09-29 18:07:56

$app_list_strings['tipo_telefono_principal_list']=array (
  '' => '',
  1 => 'Casa',
  2 => 'Celular',
  3 => 'Fax',
  4 => 'Page',
  5 => 'Otro',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_primary_address_country_c_list.php

 // created: 2016-09-29 18:46:07

$app_list_strings['primary_address_country_c_list']=array (
  'US' => 'USA',
  'CA' => 'Canada',
  'FR' => 'France',
  'MX' => 'México',
  'EU' => 'E.U.',
  'DE' => 'Germany',
  'GB' => 'Great Britain',
  'JP' => 'Japan',
  'PR' => 'Puerto Rico',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_estado_interfaz_list.php

 // created: 2016-10-06 11:29:23

$app_list_strings['estado_interfaz_list']=array (
  'vacio' => '-vacio-',
  'nuevo' => 'Nuevo',
  'procesando' => 'Procesando',
  'esperandoidpos' => 'Esperando Id POS',
  'completado' => 'Completado',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_genre_list.php

 // created: 2016-10-10 16:20:36

$app_list_strings['genre_list']=array (
  0 => 'No especificado',
  1 => 'Mujer',
  2 => 'Hombre',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Ordenes.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['orden_Ordenes'] = 'Ordenes';
$app_list_strings['moduleListSingular']['orden_Ordenes'] = 'Orden';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Ordenes.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['orden_Ordenes'] = 'Ordenes';
$app_list_strings['moduleListSingular']['orden_Ordenes'] = 'Orden';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_ordenes_estado_list.php

 // created: 2016-10-11 21:53:19

$app_list_strings['ordenes_estado_list']=array (
  'iniciada' => 'Inciada',
  'parcial' => 'Parcial',
  'completada' => 'Completada',
  'devuelta' => 'Devuelta',
  'cancelada' => 'Cancelada',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.sugar_TransactionTypeCode_list.php

 // created: 2016-10-11 22:23:20

$app_list_strings['TransactionTypeCode_list']=array (
  'Transaction' => 'Transaction',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.low_autorizacion_especial.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['moduleListSingular']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['estado_promesa_list']['Por Aplicar'] = 'Por Aplicar';
$app_list_strings['estado_promesa_list']['Aplicado'] = 'Aplicado';
$app_list_strings['estado_promesa_list']['No Aplicado'] = 'No Aplicado';
$app_list_strings['estado_list']['Por Aplicar'] = 'Por Aplicar';
$app_list_strings['estado_list']['Aplicado'] = 'Aplicado';
$app_list_strings['estado_list']['No Aplicado'] = 'No Aplicado';
$app_list_strings['Motivo_Solicitud_list']['Credito Disponible Temporal'] = 'Crédito Disponible Temporal';
$app_list_strings['Motivo_Solicitud_list']['Limite de Credito Temporal'] = 'Límite de Crédito Temporal';
$app_list_strings['Motivo_Solicitud_list']['Promesa de Pago'] = 'Promesa de Pago';
$app_list_strings['Motivo_Solicitud_list']['Otro'] = 'Otro';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.low_autorizacion_especial.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['moduleListSingular']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['estado_promesa_list']['Por Aplicar'] = 'Por Aplicar';
$app_list_strings['estado_promesa_list']['Aplicado'] = 'Aplicado';
$app_list_strings['estado_promesa_list']['No Aplicado'] = 'No Aplicado';
$app_list_strings['estado_list']['Por Aplicar'] = 'Por Aplicar';
$app_list_strings['estado_list']['Aplicado'] = 'Aplicado';
$app_list_strings['estado_list']['No Aplicado'] = 'No Aplicado';
$app_list_strings['Motivo_Solicitud_list']['Credito Disponible Temporal'] = 'Crédito Disponible Temporal';
$app_list_strings['Motivo_Solicitud_list']['Limite de Credito Temporal'] = 'Límite de Crédito Temporal';
$app_list_strings['Motivo_Solicitud_list']['Promesa de Pago'] = 'Promesa de Pago';
$app_list_strings['Motivo_Solicitud_list']['Otro'] = 'Otro';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Notas_credito.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['NC_Notas_credito'] = 'Notas de crédito';
$app_list_strings['moduleListSingular']['NC_Notas_credito'] = 'Nota de crédito';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Notas_credito.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['NC_Notas_credito'] = 'Notas de crédito';
$app_list_strings['moduleListSingular']['NC_Notas_credito'] = 'Nota de crédito';

?>
