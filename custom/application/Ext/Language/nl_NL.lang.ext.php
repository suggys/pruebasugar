<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.sugar_moduleList.php

 //created: 2016-07-25 21:54:08

$app_list_strings['moduleList']['TrackerQueries']='Tracker query\'s';
$app_list_strings['moduleList']['RevenueLineItems']='Opportunityregels';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:08

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Persoon',
  'Tasks' => 'Taak',
  'Opportunities' => 'Opportunity',
  'Products' => 'Geoffreerd product',
  'Quotes' => 'Offerte',
  'Bugs' => 'Bugs',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Project' => 'Project',
  'ProjectTask' => 'Projecttaak',
  'Prospects' => 'Target',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Opportunityregels',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.sugar_record_type_display.php

 // created: 2016-07-25 21:54:09

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Account',
  'Opportunities' => 'Opportunity',
  'Cases' => 'Casus',
  'Leads' => 'Lead',
  'Contacts' => 'Contactpersonen',
  'Products' => 'Geoffreerd product',
  'Quotes' => 'Offerte',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'Prospects' => 'Target',
  'ProjectTask' => 'Projecttaak',
  'Tasks' => 'Taak',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Opportunityregels',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:09

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Persoon',
  'Opportunities' => 'Opportunity',
  'Tasks' => 'Taak',
  'ProductTemplates' => 'Productcatalogus',
  'Quotes' => 'Offerte',
  'Products' => 'Geoffreerd product',
  'Contracts' => 'Contract',
  'Emails' => 'E-mailadres',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'ProjectTask' => 'Projecttaak',
  'Prospects' => 'Target',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Meetings' => 'Afspraak',
  'Calls' => 'Telefoongesprek',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Opportunityregels',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Product';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Gebruiker';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Nieuw';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Toegewezen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Afgesloten';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Wacht op input';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Afgewezen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Dupliceren';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Hoog';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Middel';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Laag';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Geaccepteerd';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Dupliceren';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Afgesloten';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Verlopen';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Ongeldig';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Product';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Gebruiker';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Nieuw';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Toegewezen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Afgesloten';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Wacht op input';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Afgewezen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Dupliceren';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Hoog';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Middel';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Laag';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Geaccepteerd';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Dupliceren';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Afgesloten';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Verlopen';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Ongeldig';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Product';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Gebruiker';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Nieuw';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Toegewezen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Afgesloten';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Wacht op input';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Afgewezen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Dupliceren';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Hoog';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Middel';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Laag';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Geaccepteerd';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Dupliceren';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Afgesloten';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Verlopen';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Ongeldig';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administration',
  'Product' => 'Product',
  'User' => 'Gebruiker',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => 'Nieuw',
  'Assigned' => 'Toegewezen',
  'Closed' => 'Afgesloten',
  'Pending Input' => 'Wacht op input',
  'Rejected' => 'Afgewezen',
  'Duplicate' => 'Dupliceren',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => 'Hoog',
  'P2' => 'Middel',
  'P3' => 'Laag',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => 'Geaccepteerd',
  'Duplicate' => 'Dupliceren',
  'Closed' => 'Afgesloten',
  'Out of Date' => 'Verlopen',
  'Invalid' => 'Ongeldig',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.LowesCRM3500.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nl_NL.LOWESCRM3.506_3.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
