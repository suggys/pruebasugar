<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.sugar_moduleList.php

 //created: 2016-07-25 21:54:04

$app_list_strings['moduleList']['RevenueLineItems']='매출라인 품목목록';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.sugar_record_type_display.php

 // created: 2016-07-25 21:54:05

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => '거래처',
  'Opportunities' => '예비고객',
  'Cases' => '사례',
  'Leads' => '관심고객:',
  'Contacts' => '연락처목록',
  'Products' => '견적 라인아이템',
  'Quotes' => '견적',
  'Bugs' => '버그',
  'Project' => '프로젝트',
  'Prospects' => '목표고객',
  'ProjectTask' => '프로젝트 과제',
  'Tasks' => '과제',
  'KBContents' => '지식 기반',
  'RevenueLineItems' => '매출라인 품목목록',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:04

$app_list_strings['parent_type_display']=array (
  'Accounts' => '거래처',
  'Contacts' => '연락처',
  'Tasks' => '과제',
  'Opportunities' => '예비고객',
  'Products' => '견적 라인아이템',
  'Quotes' => '견적',
  'Bugs' => '결함',
  'Cases' => '사례',
  'Leads' => '관심고객:',
  'Project' => '프로젝트',
  'ProjectTask' => '프로젝트 과제',
  'Prospects' => '목표고객',
  'KBContents' => '지식 기반',
  'RevenueLineItems' => '매출라인 품목목록',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:05

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => '거래처',
  'Contacts' => '연락처',
  'Opportunities' => '예비고객',
  'Tasks' => '과제',
  'ProductTemplates' => '상품 책자',
  'Quotes' => '견적',
  'Products' => '견적 라인아이템',
  'Contracts' => '계약',
  'Emails' => '다른 이메일:',
  'Bugs' => '버그',
  'Project' => '프로젝트',
  'ProjectTask' => '프로젝트 과제',
  'Prospects' => '목표고객',
  'Cases' => '사례',
  'Leads' => '관심고객:',
  'Meetings' => '회의',
  'Calls' => '전화상담',
  'KBContents' => '지식 기반',
  'RevenueLineItems' => '매출라인 품목목록',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = '제품';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = '사용자';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = '신규';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = '배정';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = '완료';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = '응답대기';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = '거부';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = '복사하기';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = '높음';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = '보통';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = '낮음';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = '수락';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = '복사하기';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = '완료';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = '기간만료';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = '무효';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = '제품';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = '사용자';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = '신규';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = '배정';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = '완료';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = '응답대기';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = '거부';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = '복사하기';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = '높음';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = '보통';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = '낮음';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = '수락';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = '복사하기';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = '완료';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = '기간만료';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = '무효';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = '제품';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = '사용자';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = '신규';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = '배정';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = '완료';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = '응답대기';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = '거부';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = '복사하기';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = '높음';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = '보통';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = '낮음';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = '수락';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = '복사하기';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = '완료';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = '기간만료';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = '무효';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administration',
  'Product' => '제품',
  'User' => '사용자',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => '신규',
  'Assigned' => '배정',
  'Closed' => '완료',
  'Pending Input' => '응답대기',
  'Rejected' => '거부',
  'Duplicate' => '복사하기',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => '높음',
  'P2' => '보통',
  'P3' => '낮음',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => '수락',
  'Duplicate' => '복사하기',
  'Closed' => '완료',
  'Out of Date' => '기간만료',
  'Invalid' => '무효',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.LowesCRM3500.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ko_KR.LOWESCRM3.506_3.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
