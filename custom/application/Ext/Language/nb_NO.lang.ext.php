<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:07

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Muligheter',
  'Tasks' => 'Oppgave',
  'ProductTemplates' => 'Produktkatalog',
  'Quotes' => 'Tilbud',
  'Products' => 'Produkt',
  'Contracts' => 'Kontrakt',
  'Emails' => 'E-postadresse',
  'Bugs' => 'Feil',
  'Project' => 'Prosjekt',
  'ProjectTask' => 'Prosjektoppgave',
  'Prospects' => 'Mål',
  'Cases' => 'Sak',
  'Leads' => 'Lead',
  'Meetings' => 'Møte',
  'Calls' => 'Opringning:',
  'KBContents' => 'KB-dokumenter',
  'RevenueLineItems' => 'Omsetningsposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:07

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Konto',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Oppgave',
  'Opportunities' => 'Muligheter',
  'Products' => 'Produkt',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Feil',
  'Cases' => 'Sak',
  'Leads' => 'Lead',
  'Project' => 'Prosjekt',
  'ProjectTask' => 'Prosjektoppgave',
  'Prospects' => 'Mål',
  'KBContents' => 'KB-dokumenter',
  'RevenueLineItems' => 'Omsetningsposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.sugar_moduleList.php

 //created: 2016-07-25 21:54:07

$app_list_strings['moduleList']['RevenueLineItems']='Omsetningsposter';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.sugar_record_type_display.php

 // created: 2016-07-25 21:54:08

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Konto',
  'Opportunities' => 'Muligheter',
  'Cases' => 'Sak',
  'Leads' => 'Lead',
  'Contacts' => 'Kontakter',
  'Products' => 'Produkt',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Feil',
  'Project' => 'Prosjekt',
  'Prospects' => 'Mål',
  'ProjectTask' => 'Prosjektoppgave',
  'Tasks' => 'Oppgave',
  'KBContents' => 'KB-dokumenter',
  'RevenueLineItems' => 'Omsetningsposter',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Produkt';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Bruker';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Ny';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Tildelt';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Lukket';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Venter på informasjon';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Avvist';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Dupliser';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Høy';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Medium';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Lav';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Godtatt';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Dupliser';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Lukket';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Foreldet';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Ugyldig';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Produkt';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Bruker';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Ny';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Tildelt';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Lukket';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Venter på informasjon';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Avvist';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Dupliser';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Høy';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Medium';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Lav';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Godtatt';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Dupliser';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Lukket';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Foreldet';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Ugyldig';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Produkt';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Bruker';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Ny';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Tildelt';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Lukket';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Venter på informasjon';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Avvist';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Dupliser';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Høy';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Medium';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Lav';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Godtatt';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Dupliser';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Lukket';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Foreldet';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Ugyldig';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administration',
  'Product' => 'Produkt',
  'User' => 'Bruker',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => 'Ny',
  'Assigned' => 'Tildelt',
  'Closed' => 'Lukket',
  'Pending Input' => 'Venter på informasjon',
  'Rejected' => 'Avvist',
  'Duplicate' => 'Dupliser',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => 'Høy',
  'P2' => 'Medium',
  'P3' => 'Lav',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => 'Godtatt',
  'Duplicate' => 'Dupliser',
  'Closed' => 'Lukket',
  'Out of Date' => 'Foreldet',
  'Invalid' => 'Ugyldig',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.LowesCRM3500.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/nb_NO.LOWESCRM3.506_3.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
