<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_moduleList.php

 //created: 2016-07-25 21:53:48

$app_list_strings['moduleList']['RevenueLineItems']='Revenue Line Items';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:49

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Contact',
  'Tasks' => 'Task',
  'Opportunities' => 'Opportunity',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Quote',
  'Bugs' => 'Bugs',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Project' => 'Project',
  'ProjectTask' => 'Project Task',
  'Prospects' => 'Target',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Revenue Line Items',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_record_type_display.php

 // created: 2016-07-25 21:53:49

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Account',
  'Opportunities' => 'Opportunity',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Contacts' => 'Contacts',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Quote',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'Prospects' => 'Target',
  'ProjectTask' => 'Project Task',
  'Tasks' => 'Task',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Revenue Line Items',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:49

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Contact',
  'Opportunities' => 'Opportunity',
  'Tasks' => 'Task',
  'ProductTemplates' => 'Product Catalog',
  'Quotes' => 'Quote',
  'Products' => 'Quoted Line Item',
  'Contracts' => 'Contract',
  'Emails' => 'Email',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'ProjectTask' => 'Project Task',
  'Prospects' => 'Target',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Meetings' => 'Meeting',
  'Calls' => 'Call',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Revenue Line Items',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Grupo_empresas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GPE_Grupo_empresas'] = 'Grupo de empresas';
$app_list_strings['moduleListSingular']['GPE_Grupo_empresas'] = 'Grupo de empresa';
$app_list_strings['gpo_empresa_estado_list']['Activo'] = 'Activo';
$app_list_strings['gpo_empresa_estado_list']['Inactivo'] = 'Inactivo';
$app_list_strings['gpo_empresa_estado_list'][''] = '';
$app_list_strings['gpo_emp_edo_list']['Activo'] = 'Activo';
$app_list_strings['gpo_emp_edo_list']['Inactivo'] = 'Inactivo';
$app_list_strings['gpo_emp_edo_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Ordenes.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['orden_Ordenes'] = 'Ordenes';
$app_list_strings['moduleListSingular']['orden_Ordenes'] = 'Orden';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Notas_credito.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['NC_Notas_credito'] = 'Notas de crédito';
$app_list_strings['moduleListSingular']['NC_Notas_credito'] = 'Nota de crédito';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.low_autorizacion_especial.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['moduleListSingular']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['estado_promesa_list']['Por Aplicar'] = 'Por Aplicar';
$app_list_strings['estado_promesa_list']['Aplicado'] = 'Aplicado';
$app_list_strings['estado_promesa_list']['No Aplicado'] = 'No Aplicado';
$app_list_strings['estado_list']['Por Aplicar'] = 'Por Aplicar';
$app_list_strings['estado_list']['Aplicado'] = 'Aplicado';
$app_list_strings['estado_list']['No Aplicado'] = 'No Aplicado';
$app_list_strings['Motivo_Solicitud_list']['Credito Disponible Temporal'] = 'Crédito Disponible Temporal';
$app_list_strings['Motivo_Solicitud_list']['Limite de Credito Temporal'] = 'Límite de Crédito Temporal';
$app_list_strings['Motivo_Solicitud_list']['Promesa de Pago'] = 'Promesa de Pago';
$app_list_strings['Motivo_Solicitud_list']['Otro'] = 'Otro';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Lowes_facturas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['LF_Facturas'] = 'Facturas';
$app_list_strings['moduleListSingular']['LF_Facturas'] = 'Factura';
$app_list_strings['tipo_documento_list']['factura'] = 'Factura';
$app_list_strings['tipo_documento_list']['nota_de_cargo'] = 'Nota de Cargo';
$app_list_strings['estado_tiempo_list']['vigente'] = 'Vigente';
$app_list_strings['estado_tiempo_list']['vencida'] = 'Vencida';
$app_list_strings['estado_tiempo_list']['pagada'] = 'Pagada';
$app_list_strings['estado_tiempo_list']['devuelta_parcial'] = 'Devuelta parcial';
$app_list_strings['estado_tiempo_list']['devuelta_completa'] = 'Devuelta completa';
$app_list_strings['estado_tiempo_list']['entregada'] = 'Entregada';
$app_list_strings['estado_factura_list']['aclaracion'] = 'Aclaración';
$app_list_strings['estado_factura_list']['renovacion_de_cartera'] = 'Renovación de cartera';
$app_list_strings['estado_factura_list']['cobranza_extrajudicial'] = 'Cobranza extrajudicial';
$app_list_strings['estado_factura_list']['legal'] = 'Legal';
$app_list_strings['estado_factura_list']['perdida'] = 'Pérdida';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Pagos.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['lowes_Pagos'] = 'Pagos';
$app_list_strings['moduleListSingular']['lowes_Pagos'] = 'Pago';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Pagos_Facturas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['Low01_Pagos_Facturas'] = 'Pagos_Facturas';
$app_list_strings['moduleListSingular']['Low01_Pagos_Facturas'] = 'Pago Factura';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Lowes_Modules_CRM.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$app_list_strings['moduleList']['LOW02_Partidas_Orden'] = 'Partidas de Orden';
$app_list_strings['moduleListSingular']['LOW02_Partidas_Orden'] = 'Partida de Orden';

$app_list_strings['lo_obras_type_dom']['Analyst'] = 'Analista';
$app_list_strings['lo_obras_type_dom']['Competitor'] = 'Competidor';
$app_list_strings['lo_obras_type_dom']['Customer'] = 'Cliente';
$app_list_strings['lo_obras_type_dom']['Integrator'] = 'Integrador';
$app_list_strings['lo_obras_type_dom']['Investor'] = 'Inversor';
$app_list_strings['lo_obras_type_dom']['Partner'] = 'Socio';
$app_list_strings['lo_obras_type_dom']['Press'] = 'Prensa';
$app_list_strings['lo_obras_type_dom']['Prospect'] = 'Prospecto';
$app_list_strings['lo_obras_type_dom']['Reseller'] = 'Revendedor';
$app_list_strings['lo_obras_type_dom']['Other'] = 'Otro';
$app_list_strings['lo_obras_type_dom'][''] = '';
$app_list_strings['moduleList']['LO_Obras'] = 'Obras';
$app_list_strings['moduleListSingular']['LO_Obras'] = 'Obra';

$app_list_strings['moduleList']['LOW01_SolicitudesCredito'] = 'Solicitudes de Crédito';
$app_list_strings['moduleListSingular']['LOW01_SolicitudesCredito'] = 'Solicitud de Crédito';
$app_list_strings['tipo_de_credito_list']['30_dias_sin_intereses'] = '30 días sin intereses';
$app_list_strings['tipo_de_credito_list']['90_dias_sin_intereses'] = '90 días sin intereses';
$app_list_strings['tipo_de_credito_list'][''] = '';
$app_list_strings['solicitud_credito_domicilio_list']['Propia'] = 'Propia';
$app_list_strings['solicitud_credito_domicilio_list']['Rentada'] = 'Rentada';
$app_list_strings['solicitud_credito_domicilio_list']['HIpotecada'] = 'HIpotecada';
$app_list_strings['solicitud_credito_domicilio_list']['De_Socios'] = 'De Socios';
$app_list_strings['solicitud_credito_domicilio_list'][''] = '';
$app_list_strings['solicitud_credito_giro_list']['Comercio'] = 'Comercio';
$app_list_strings['solicitud_credito_giro_list']['Industria'] = 'Industria';
$app_list_strings['solicitud_credito_giro_list']['Servicio'] = 'Servicio';
$app_list_strings['solicitud_credito_giro_list']['Agropecuario'] = 'Agropecuario';
$app_list_strings['solicitud_credito_giro_list']['Construccion'] = 'Construcción';
$app_list_strings['solicitud_credito_giro_list'][''] = '';
$app_list_strings['solicitud_credito_sector_list']['Publico'] = 'Público';
$app_list_strings['solicitud_credito_sector_list']['Privado'] = 'Privado';
$app_list_strings['solicitud_credito_sector_list'][''] = '';
$app_list_strings['solicitud_credito_caracter_list']['Propietario'] = 'Propietario';
$app_list_strings['solicitud_credito_caracter_list']['Asociado'] = 'Asociado';
$app_list_strings['solicitud_credito_caracter_list']['Empleado'] = 'Empleado';
$app_list_strings['solicitud_credito_caracter_list']['Otro'] = 'Otro';
$app_list_strings['solicitud_credito_caracter_list'][''] = '';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['T_de_C'] = 'T de C';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Debito'] = 'Débito';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Cheques'] = 'Cheques';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Ahorro'] = 'Ahorro';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Inversion'] = 'Inversión';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Otras'] = 'Otras';
$app_list_strings['solicitud_credito_tipo_ref_com_list'][''] = '';
$app_list_strings['recepcion_estado_cuenta_list']['Correo'] = 'Correo';
$app_list_strings['recepcion_estado_cuenta_list']['Correo_electronico'] = 'Correo electrónico';
$app_list_strings['recepcion_estado_cuenta_list'][''] = '';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list']['Correo'] = 'Correo';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list']['Correo_electronico'] = 'Correo electrónico';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list'][''] = '';
$app_list_strings['recepcion_informacion_promo_list']['Si'] = 'Si';
$app_list_strings['recepcion_informacion_promo_list']['No'] = 'No';
$app_list_strings['recepcion_informacion_promo_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Configuraciones.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['MRX1_Configuraciones'] = 'Configuraciones';
$app_list_strings['moduleListSingular']['MRX1_Configuraciones'] = 'Configuración';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Product';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'User';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'New';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Assigned';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Closed';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Pending Input';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Rejected';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplicate';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'High';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Medium';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Low';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Accepted';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplicate';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Closed';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Out of Date';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Invalid';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Product';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'User';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'New';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Assigned';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Closed';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Pending Input';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Rejected';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplicate';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'High';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Medium';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Low';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Accepted';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplicate';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Closed';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Out of Date';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Invalid';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Product';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'User';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'New';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Assigned';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Closed';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Pending Input';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Rejected';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplicate';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'High';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Medium';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Low';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Accepted';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplicate';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Closed';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Out of Date';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Invalid';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administration',
  'Product' => 'Product',
  'User' => 'User',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => 'New',
  'Assigned' => 'Assigned',
  'Closed' => 'Closed',
  'Pending Input' => 'Pending Input',
  'Rejected' => 'Rejected',
  'Duplicate' => 'Duplicate',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => 'High',
  'P2' => 'Medium',
  'P3' => 'Low',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => 'Accepted',
  'Duplicate' => 'Duplicate',
  'Closed' => 'Closed',
  'Out of Date' => 'Out of Date',
  'Invalid' => 'Invalid',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.LowesCRM3500.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.ops-modules.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['ops_Backups'] = 'Backups';
$app_list_strings['moduleListSingular']['ops_Backups'] = 'Backup';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.LOWESCRM3.506_3.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_sucursal_c_list.php

 // created: 2018-05-14 15:58:32

$app_list_strings['sucursal_c_list']=array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  '' => '',
  3242 => 'Aguascalientes',
  3397 => 'Leon',
  3393 => 'Santa Catarina',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_ordenes_estado_list.php

 // created: 2018-06-04 21:57:10

$app_list_strings['ordenes_estado_list']=array (
  '' => '',
  0 => '',
  1 => 'Iniciada',
  2 => 'Parcial',
  3 => 'Lleno',
  4 => 'Cancelada',
  5 => 'Completada',
  6 => 'Devuelta',
  7 => 'Cancelada Suspendida',
  8 => 'Cancelada Parcial',
  9 => 'Cancelada Completada',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.sugar_Orden_Estado_list.php

 // created: 2018-06-05 00:45:37

$app_list_strings['Orden_Estado_list']=array (
  '' => '',
  0 => '',
  1 => 'Iniciada',
  2 => 'Parcial',
  3 => 'Lleno',
  4 => 'Cancelada',
  5 => 'Completada',
  6 => 'Devuelta',
  7 => 'Cancelada Suspendida',
  8 => 'Cancelada Parcial',
  9 => 'Cancelada Completa',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.Formas.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['FDP_Pago'] = 'Formas de Pago';
$app_list_strings['moduleListSingular']['FDP_Pago'] = 'Forma de Pago';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.FormasPago.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['FDP_formasPago'] = 'formaPago';
$app_list_strings['moduleListSingular']['FDP_formasPago'] = 'formapago';

?>
