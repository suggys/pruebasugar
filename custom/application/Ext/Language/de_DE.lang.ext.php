<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.sugar_moduleList.php

 //created: 2016-07-25 21:53:53

$app_list_strings['moduleList']['RevenueLineItems']='Umsatzposten';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.sugar_record_type_display.php

 // created: 2016-07-25 21:53:54

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Firma',
  'Opportunities' => 'Verkaufschance',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Contacts' => 'Kontakte',
  'Products' => 'Produkt',
  'Quotes' => 'Angebot',
  'Bugs' => 'Fehler',
  'Project' => 'Projekt',
  'Prospects' => 'Zielkontakt',
  'ProjectTask' => 'Projektaufgabe',
  'Tasks' => 'Aufgabe',
  'KBContents' => 'Wissensdatenbank',
  'RevenueLineItems' => 'Umsatzposten',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.sugar_parent_type_display.php

 // created: 2016-07-25 21:53:54

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Firma',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Aufgabe',
  'Opportunities' => 'Verkaufschance',
  'Products' => 'Produkt',
  'Quotes' => 'Angebot',
  'Bugs' => 'Fehler',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektaufgabe',
  'Prospects' => 'Zielkontakt',
  'KBContents' => 'Wissensdatenbank',
  'RevenueLineItems' => 'Umsatzposten',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:53:54

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Firma',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Verkaufschance',
  'Tasks' => 'Aufgabe',
  'ProductTemplates' => 'Produktkatalog',
  'Quotes' => 'Angebot',
  'Products' => 'Produkt',
  'Contracts' => 'Vertrag',
  'Emails' => 'E-Mail',
  'Bugs' => 'Fehler',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektaufgabe',
  'Prospects' => 'Zielkontakt',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Meetings' => 'Meeting',
  'Calls' => 'Anruf',
  'KBContents' => 'Wissensdatenbank',
  'RevenueLineItems' => 'Umsatzposten',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Produkt';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Benutzer';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Neu';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Zugewiesen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Geschlossen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Eingabe ausstehend';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Abgelehnt';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplizieren';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Hoch';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Mittel';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Niedrig';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Akzeptiert';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplizieren';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Geschlossen';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Abgelaufen';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Ungültig';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Produkt';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Benutzer';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Neu';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Zugewiesen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Geschlossen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Eingabe ausstehend';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Abgelehnt';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplizieren';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Hoch';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Mittel';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Niedrig';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Akzeptiert';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplizieren';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Geschlossen';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Abgelaufen';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Ungültig';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Produkt';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Benutzer';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Neu';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Zugewiesen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Geschlossen';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Eingabe ausstehend';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Abgelehnt';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplizieren';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Hoch';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Mittel';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Niedrig';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Akzeptiert';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplizieren';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Geschlossen';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Abgelaufen';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Ungültig';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administration',
  'Product' => 'Produkt',
  'User' => 'Benutzer',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => 'Neu',
  'Assigned' => 'Zugewiesen',
  'Closed' => 'Geschlossen',
  'Pending Input' => 'Eingabe ausstehend',
  'Rejected' => 'Abgelehnt',
  'Duplicate' => 'Duplizieren',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => 'Hoch',
  'P2' => 'Mittel',
  'P3' => 'Niedrig',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => 'Akzeptiert',
  'Duplicate' => 'Duplizieren',
  'Closed' => 'Geschlossen',
  'Out of Date' => 'Abgelaufen',
  'Invalid' => 'Ungültig',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.LowesCRM3500.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/de_DE.LOWESCRM3.506_3.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
