<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.sugar_solicitud_credito_estado_list.php

 // created: 2017-10-19 17:51:02

$app_list_strings['solicitud_credito_estado_list']=array (
  'Solicitud_Nueva' => 'Solicitud Nueva',
  'Finalizado_por_Solicitante' => 'Finalizado por Solicitante',
  'Aprobado_por_Asesor_Comercial' => 'Aprobado por Asesor Comercial',
  'Rechazado_por_Asesor_Comercial' => 'Rechazado por Asesor Comercial',
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => 'Pendiente por Gerente de Ventas Comerciales',
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => 'Aprobado por Gerente de Ventas Comerciales',
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => 'Rechazado por Gerente de Ventas Comerciales',
  'Pendiente_por_Analista_de_Credito' => 'Pendiente por Analista de Crédito',
  'Aprobado_por_Analista_de_Credito' => 'Aprobado por Analista de Crédito',
  'Rechazado_por_Analista_de_Credito' => 'Rechazado por Analista de Crédito',
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Pendiente de Envío a Agencia Externa por Administrador de Crédito',
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Aprobado Envío a Agencia Externa por Administrador de Crédito',
  'Pendiente_por_Agente_Externo' => 'Pendiente por Agente Externo',
  'Pendiente_por_Administrador_de_Credito' => 'Pendiente por Administrador de Crédito',
  'Aprobacion_por_Administrador_de_Credito' => 'Aprobación por Administrador de Crédito',
  'Rechazado_por_Administrador_de_Credito' => 'Rechazado por Administrador de Crédito',
  'Pendiente_por_Gerente_de_Credito' => 'Pendiente por Gerente de Crédito',
  'Aprobacion_por_Gerente_de_Credito' => 'Aprobación por Gerente de Crédito',
  'Rechazado_por_Gerente_de_Credito' => 'Rechazado por Gerente de Crédito',
  'Pendiente_por_Director_de_Finanzas' => 'Pendiente por Director de Finanzas',
  'Aprobacion_por_Director_de_Finanzas' => 'Aprobación por Director de Finanzas',
  'Rechazado_por_Director_de_Finanzas' => 'Rechazado por Director de Finanzas',
  'Pendiente_por_Vicepresidencia_Comercial' => 'Pendiente por Vicepresidencia Comercial',
  'Aprobacion_por_Vicepresidencia_Comercial' => 'Aprobación por Vicepresidencia Comercial',
  'Rechazado_por_Vicepresidencia_Comercial' => 'Rechazado por Vicepresidencia Comercial',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administracija';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Proizvod';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Korisnik';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Novo';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Dodijeljeno';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Zatvoreno';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Unos na čekanju';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Odbijeno';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplikat';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Visoki';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Srednji';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Niski';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Prihvaćeno';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplikat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Zatvoreno';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Zastarjelo';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Nije valjano';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administracija';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Proizvod';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Korisnik';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Novo';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Dodijeljeno';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Zatvoreno';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Unos na čekanju';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Odbijeno';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplikat';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Visoki';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Srednji';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Niski';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Prihvaćeno';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplikat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Zatvoreno';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Zastarjelo';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Nije valjano';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administracija';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Proizvod';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Korisnik';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Novo';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Dodijeljeno';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Zatvoreno';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Unos na čekanju';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Odbijeno';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplikat';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Visoki';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Srednji';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Niski';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Prihvaćeno';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplikat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Zatvoreno';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Zastarjelo';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Nije valjano';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.CentroDeDisenio_Personalizaciones_180201_1311.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['sucursal_c_list'] = array (
  '' => '',
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administracija',
  'Product' => 'Proizvod',
  'User' => 'Korisnik',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => 'Novo',
  'Assigned' => 'Dodijeljeno',
  'Closed' => 'Zatvoreno',
  'Pending Input' => 'Unos na čekanju',
  'Rejected' => 'Odbijeno',
  'Duplicate' => 'Duplikat',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => 'Visoki',
  'P2' => 'Srednji',
  'P3' => 'Niski',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => 'Prihvaćeno',
  'Duplicate' => 'Duplikat',
  'Closed' => 'Zatvoreno',
  'Out of Date' => 'Zastarjelo',
  'Invalid' => 'Nije valjano',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.LowesCRM3500.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.LowesCRM3500.php
 
$app_list_strings['accounts_tipo_cliente_list'] = array (
  1 => 'Contractor',
  2 => 'Premier',
  3 => 'Reventa',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  '' => '',
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.LOWESCRM3.506_3.php
 
$app_list_strings['sucursal_c_list'] = array (
  '' => '',
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  3397 => 'León',
  3242 => 'Aguascalientes',
  3393 => 'Santa Catarina',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  '' => '',
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
);$app_list_strings['accounts_tipo_cliente_list'] = array (
  1 => 'Contractor',
  2 => 'Premier',
  3 => 'Reventa',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.sugar_sucursal_c_list.php

 // created: 2018-05-14 15:58:32

$app_list_strings['sucursal_c_list']=array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  '' => '',
  3242 => 'Aguascalientes',
  3397 => 'Leon',
  3393 => 'Santa Catarina',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.sugar_ordenes_estado_list.php

 // created: 2018-06-04 21:57:10

$app_list_strings['ordenes_estado_list']=array (
  '' => '',
  0 => '',
  1 => 'Iniciada',
  2 => 'Parcial',
  3 => 'Lleno',
  4 => 'Cancelada',
  5 => 'Completada',
  6 => 'Devuelta',
  7 => 'Cancelada Suspendida',
  8 => 'Cancelada Parcial',
  9 => 'Cancelada Completada',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hr_HR.sugar_Orden_Estado_list.php

 // created: 2018-06-05 00:45:37

$app_list_strings['Orden_Estado_list']=array (
  '' => '',
  0 => '',
  1 => 'Iniciada',
  2 => 'Parcial',
  3 => 'Lleno',
  4 => 'Cancelada',
  5 => 'Completada',
  6 => 'Devuelta',
  7 => 'Cancelada Suspendida',
  8 => 'Cancelada Parcial',
  9 => 'Cancelada Completa',
);
?>
