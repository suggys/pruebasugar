<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.sugar_record_type_display_notes.php

 // created: 2016-07-25 21:54:00

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Kliens',
  'Contacts' => 'Kapcsolat',
  'Opportunities' => 'Lehetőség',
  'Tasks' => 'Feladat',
  'ProductTemplates' => 'Termékkatalógus',
  'Quotes' => 'Árajánlat',
  'Products' => 'Megajánlott Tétel',
  'Contracts' => 'Szerződés',
  'Emails' => 'E-mail:',
  'Bugs' => 'Hiba',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektfeladat',
  'Prospects' => 'Cél',
  'Cases' => 'Eset',
  'Leads' => 'Ajánlás',
  'Meetings' => 'Találkozó',
  'Calls' => 'Hívás',
  'KBContents' => 'Tudásbázis',
  'RevenueLineItems' => 'Bevétel sorok',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.sugar_moduleList.php

 //created: 2016-07-25 21:54:00

$app_list_strings['moduleList']['RevenueLineItems']='Bevétel sorok';
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.sugar_parent_type_display.php

 // created: 2016-07-25 21:54:00

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Kliens',
  'Contacts' => 'Kapcsolat',
  'Tasks' => 'Feladat',
  'Opportunities' => 'Lehetőség',
  'Products' => 'Megajánlott Tétel',
  'Quotes' => 'Árajánlat',
  'Bugs' => 'Hibák',
  'Cases' => 'Eset',
  'Leads' => 'Ajánlás',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektfeladat',
  'Prospects' => 'Cél',
  'KBContents' => 'Tudásbázis',
  'RevenueLineItems' => 'Bevétel sorok',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.sugar_record_type_display.php

 // created: 2016-07-25 21:54:01

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Kliens',
  'Opportunities' => 'Lehetőség',
  'Cases' => 'Eset',
  'Leads' => 'Ajánlás',
  'Contacts' => 'Kapcsolatok',
  'Products' => 'Megajánlott Tétel',
  'Quotes' => 'Árajánlat',
  'Bugs' => 'Hiba',
  'Project' => 'Projekt',
  'Prospects' => 'Cél',
  'ProjectTask' => 'Projektfeladat',
  'Tasks' => 'Feladat',
  'KBContents' => 'Tudásbázis',
  'RevenueLineItems' => 'Bevétel sorok',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Termék';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Felhasználó';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Új';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Hozzárendelve';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Lezárt';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Függőben lévő bevitel';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Elutasítva';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Kettőz';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Magas';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Közepes';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Alacsony';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Elfogadott';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Kettőz';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Lezárt';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Lejárt';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Érvénytelen';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Termék';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Felhasználó';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Új';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Hozzárendelve';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Lezárt';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Függőben lévő bevitel';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Elutasítva';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Kettőz';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Magas';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Közepes';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Alacsony';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Elfogadott';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Kettőz';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Lezárt';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Lejárt';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Érvénytelen';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Termék';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Felhasználó';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Új';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Hozzárendelve';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Lezárt';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Függőben lévő bevitel';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Elutasítva';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Kettőz';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Magas';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Közepes';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Alacsony';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Elfogadott';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Kettőz';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Lezárt';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Lejárt';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Érvénytelen';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administration',
  'Product' => 'Termék',
  'User' => 'Felhasználó',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => 'Új',
  'Assigned' => 'Hozzárendelve',
  'Closed' => 'Lezárt',
  'Pending Input' => 'Függőben lévő bevitel',
  'Rejected' => 'Elutasítva',
  'Duplicate' => 'Kettőz',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => 'Magas',
  'P2' => 'Közepes',
  'P3' => 'Alacsony',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => 'Elfogadott',
  'Duplicate' => 'Kettőz',
  'Closed' => 'Lezárt',
  'Out of Date' => 'Lejárt',
  'Invalid' => 'Érvénytelen',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.LowesCRM3500.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.LOWESCRM3.506_3.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/hu_HU.LOWESCRM3.506_3.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);

?>
