<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Include/rli_unhide.ext.php

$moduleList[] = 'RevenueLineItems';
if (isset($modInvisList) && is_array($modInvisList)) {
    foreach ($modInvisList as $key => $mod) {
        if ($mod === 'RevenueLineItems') {
            unset($modInvisList[$key]);
        }
    }
}
?>
<?php
// Merged from custom/Extension/application/Ext/Include/Grupo_empresas.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['GPE_Grupo_empresas'] = 'GPE_Grupo_empresas';
$beanFiles['GPE_Grupo_empresas'] = 'modules/GPE_Grupo_empresas/GPE_Grupo_empresas.php';
$moduleList[] = 'GPE_Grupo_empresas';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Ordenes.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['orden_Ordenes'] = 'orden_Ordenes';
$beanFiles['orden_Ordenes'] = 'modules/orden_Ordenes/orden_Ordenes.php';
$moduleList[] = 'orden_Ordenes';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Notas_credito.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['NC_Notas_credito'] = 'NC_Notas_credito';
$beanFiles['NC_Notas_credito'] = 'modules/NC_Notas_credito/NC_Notas_credito.php';
$moduleList[] = 'NC_Notas_credito';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/low_autorizacion_especial.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['lowae_autorizacion_especial'] = 'lowae_autorizacion_especial';
$beanFiles['lowae_autorizacion_especial'] = 'modules/lowae_autorizacion_especial/lowae_autorizacion_especial.php';
$moduleList[] = 'lowae_autorizacion_especial';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Lowes_facturas.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['LF_Facturas'] = 'LF_Facturas';
$beanFiles['LF_Facturas'] = 'modules/LF_Facturas/LF_Facturas.php';
$moduleList[] = 'LF_Facturas';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Pagos.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['lowes_Pagos'] = 'lowes_Pagos';
$beanFiles['lowes_Pagos'] = 'modules/lowes_Pagos/lowes_Pagos.php';
$moduleList[] = 'lowes_Pagos';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Pagos_Facturas.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['Low01_Pagos_Facturas'] = 'Low01_Pagos_Facturas';
$beanFiles['Low01_Pagos_Facturas'] = 'modules/Low01_Pagos_Facturas/Low01_Pagos_Facturas.php';
$moduleList[] = 'Low01_Pagos_Facturas';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Lowes_Modules_CRM.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['LOW02_Partidas_Orden'] = 'LOW02_Partidas_Orden';
$beanFiles['LOW02_Partidas_Orden'] = 'modules/LOW02_Partidas_Orden/LOW02_Partidas_Orden.php';
$moduleList[] = 'LOW02_Partidas_Orden';
$beanList['LO_Obras'] = 'LO_Obras';
$beanFiles['LO_Obras'] = 'modules/LO_Obras/LO_Obras.php';
$moduleList[] = 'LO_Obras';
$beanList['LOW01_SolicitudesCredito'] = 'LOW01_SolicitudesCredito';
$beanFiles['LOW01_SolicitudesCredito'] = 'modules/LOW01_SolicitudesCredito/LOW01_SolicitudesCredito.php';
$moduleList[] = 'LOW01_SolicitudesCredito';
$beanList['Low03_AgentesExternos'] = 'Low03_AgentesExternos';
$beanFiles['Low03_AgentesExternos'] = 'modules/Low03_AgentesExternos/Low03_AgentesExternos.php';
$moduleList[] = 'Low03_AgentesExternos';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Configuraciones.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['MRX1_Configuraciones'] = 'MRX1_Configuraciones';
$beanFiles['MRX1_Configuraciones'] = 'modules/MRX1_Configuraciones/MRX1_Configuraciones.php';
$moduleList[] = 'MRX1_Configuraciones';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/CentroDeDisenio.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['dise_Prospectos_cocina'] = 'dise_Prospectos_cocina';
$beanFiles['dise_Prospectos_cocina'] = 'modules/dise_Prospectos_cocina/dise_Prospectos_cocina.php';
$moduleList[] = 'dise_Prospectos_cocina';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/ops-modules.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['ops_Backups'] = 'ops_Backups';
$beanFiles['ops_Backups'] = 'modules/ops_Backups/ops_Backups.php';
$moduleList[] = 'ops_Backups';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/Formas.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['FDP_Pago'] = 'FDP_Pago';
$beanFiles['FDP_Pago'] = 'modules/FDP_Pago/FDP_Pago.php';
$moduleList[] = 'FDP_Pago';


?>
<?php
// Merged from custom/Extension/application/Ext/Include/FormasPago.php
 
 //WARNING: The contents of this file are auto-generated
$beanList['FDP_formasPago'] = 'FDP_formasPago';
$beanFiles['FDP_formasPago'] = 'modules/FDP_formasPago/FDP_formasPago.php';
$moduleList[] = 'FDP_formasPago';


?>
