<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'accounts_lowae_autorizacion_especial_1' => 
  array (
    'name' => 'accounts_lowae_autorizacion_especial_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'accounts_lowae_autorizacion_especial_1' => 
      array (
        'lhs_module' => 'Accounts',
        'lhs_table' => 'accounts',
        'lhs_key' => 'id',
        'rhs_module' => 'lowae_autorizacion_especial',
        'rhs_table' => 'lowae_autorizacion_especial',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'accounts_lowae_autorizacion_especial_1_c',
        'join_key_lhs' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
        'join_key_rhs' => 'accounts_le69cspecial_idb',
      ),
    ),
    'table' => 'accounts_lowae_autorizacion_especial_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'accounts_lowae_autorizacion_especial_1accounts_ida' => 
      array (
        'name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
        'type' => 'id',
      ),
      'accounts_le69cspecial_idb' => 
      array (
        'name' => 'accounts_le69cspecial_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'accounts_lowae_autorizacion_especial_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'accounts_lowae_autorizacion_especial_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'accounts_lowae_autorizacion_especial_1accounts_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'accounts_lowae_autorizacion_especial_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'accounts_le69cspecial_idb',
        ),
      ),
    ),
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'lowae_autorizacion_especial',
    'rhs_table' => 'lowae_autorizacion_especial',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'accounts_lowae_autorizacion_especial_1_c',
    'join_key_lhs' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
    'join_key_rhs' => 'accounts_le69cspecial_idb',
    'readonly' => true,
    'relationship_name' => 'accounts_lowae_autorizacion_especial_1',
    'rhs_subpanel' => 'ForAccountsAccounts_lowae_autorizacion_especial_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'lowae_autorizacion_especial_orden_ordenes_1' => 
  array (
    'name' => 'lowae_autorizacion_especial_orden_ordenes_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'lowae_autorizacion_especial_orden_ordenes_1' => 
      array (
        'lhs_module' => 'lowae_autorizacion_especial',
        'lhs_table' => 'lowae_autorizacion_especial',
        'lhs_key' => 'id',
        'rhs_module' => 'orden_Ordenes',
        'rhs_table' => 'orden_ordenes',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'lowae_autorizacion_especial_orden_ordenes_1_c',
        'join_key_lhs' => 'lowae_auto0644special_ida',
        'join_key_rhs' => 'lowae_autorizacion_especial_orden_ordenes_1orden_ordenes_idb',
      ),
    ),
    'table' => 'lowae_autorizacion_especial_orden_ordenes_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'lowae_auto0644special_ida' => 
      array (
        'name' => 'lowae_auto0644special_ida',
        'type' => 'id',
      ),
      'lowae_autorizacion_especial_orden_ordenes_1orden_ordenes_idb' => 
      array (
        'name' => 'lowae_autorizacion_especial_orden_ordenes_1orden_ordenes_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'lowae_autorizacion_especial_orden_ordenes_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'lowae_autorizacion_especial_orden_ordenes_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'lowae_auto0644special_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'lowae_autorizacion_especial_orden_ordenes_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'lowae_autorizacion_especial_orden_ordenes_1orden_ordenes_idb',
        ),
      ),
    ),
    'lhs_module' => 'lowae_autorizacion_especial',
    'lhs_table' => 'lowae_autorizacion_especial',
    'lhs_key' => 'id',
    'rhs_module' => 'orden_Ordenes',
    'rhs_table' => 'orden_ordenes',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'lowae_autorizacion_especial_orden_ordenes_1_c',
    'join_key_lhs' => 'lowae_auto0644special_ida',
    'join_key_rhs' => 'lowae_autorizacion_especial_orden_ordenes_1orden_ordenes_idb',
    'readonly' => true,
    'relationship_name' => 'lowae_autorizacion_especial_orden_ordenes_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'lowae_autorizacion_especial_modified_user' => 
  array (
    'name' => 'lowae_autorizacion_especial_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'lowae_autorizacion_especial',
    'rhs_table' => 'lowae_autorizacion_especial',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'lowae_autorizacion_especial_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lowae_autorizacion_especial_created_by' => 
  array (
    'name' => 'lowae_autorizacion_especial_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'lowae_autorizacion_especial',
    'rhs_table' => 'lowae_autorizacion_especial',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'lowae_autorizacion_especial_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lowae_autorizacion_especial_activities' => 
  array (
    'name' => 'lowae_autorizacion_especial_activities',
    'lhs_module' => 'lowae_autorizacion_especial',
    'lhs_table' => 'lowae_autorizacion_especial',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'lowae_autorizacion_especial',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'lowae_autorizacion_especial_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lowae_autorizacion_especial_following' => 
  array (
    'name' => 'lowae_autorizacion_especial_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'lowae_autorizacion_especial',
    'rhs_table' => 'lowae_autorizacion_especial',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'lowae_autorizacion_especial',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'lowae_autorizacion_especial_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lowae_autorizacion_especial_favorite' => 
  array (
    'name' => 'lowae_autorizacion_especial_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'lowae_autorizacion_especial',
    'rhs_table' => 'lowae_autorizacion_especial',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'lowae_autorizacion_especial',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'lowae_autorizacion_especial_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lowae_autorizacion_especial_assigned_user' => 
  array (
    'name' => 'lowae_autorizacion_especial_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'lowae_autorizacion_especial',
    'rhs_table' => 'lowae_autorizacion_especial',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'lowae_autorizacion_especial_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lowae_autorizacion_especial_lowes_pagos_1' => 
  array (
    'rhs_label' => 'Pagos',
    'lhs_label' => 'Autorización Especial',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'lowae_autorizacion_especial',
    'rhs_module' => 'lowes_Pagos',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'lowae_autorizacion_especial_lowes_pagos_1',
  ),
);