<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'nc_notas_credito_accounts' => 
  array (
    'name' => 'nc_notas_credito_accounts',
    'true_relationship_type' => 'one-to-many',
    'relationships' => 
    array (
      'nc_notas_credito_accounts' => 
      array (
        'lhs_module' => 'Accounts',
        'lhs_table' => 'accounts',
        'lhs_key' => 'id',
        'rhs_module' => 'NC_Notas_credito',
        'rhs_table' => 'nc_notas_credito',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'nc_notas_credito_accounts_c',
        'join_key_lhs' => 'nc_notas_credito_accountsaccounts_ida',
        'join_key_rhs' => 'nc_notas_credito_accountsnc_notas_credito_idb',
      ),
    ),
    'table' => 'nc_notas_credito_accounts_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'nc_notas_credito_accountsaccounts_ida' => 
      array (
        'name' => 'nc_notas_credito_accountsaccounts_ida',
        'type' => 'id',
      ),
      'nc_notas_credito_accountsnc_notas_credito_idb' => 
      array (
        'name' => 'nc_notas_credito_accountsnc_notas_credito_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'nc_notas_credito_accountsspk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'nc_notas_credito_accounts_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'nc_notas_credito_accountsaccounts_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'nc_notas_credito_accounts_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'nc_notas_credito_accountsnc_notas_credito_idb',
        ),
      ),
    ),
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'NC_Notas_credito',
    'rhs_table' => 'nc_notas_credito',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'nc_notas_credito_accounts_c',
    'join_key_lhs' => 'nc_notas_credito_accountsaccounts_ida',
    'join_key_rhs' => 'nc_notas_credito_accountsnc_notas_credito_idb',
    'readonly' => true,
    'relationship_name' => 'nc_notas_credito_accounts',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'nc_notas_credito_orden_ordenes' => 
  array (
    'name' => 'nc_notas_credito_orden_ordenes',
    'true_relationship_type' => 'one-to-one',
    'relationships' => 
    array (
      'nc_notas_credito_orden_ordenes' => 
      array (
        'lhs_module' => 'NC_Notas_credito',
        'lhs_table' => 'nc_notas_credito',
        'lhs_key' => 'id',
        'rhs_module' => 'orden_Ordenes',
        'rhs_table' => 'orden_ordenes',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'nc_notas_credito_orden_ordenes_c',
        'join_key_lhs' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
        'join_key_rhs' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
      ),
    ),
    'table' => 'nc_notas_credito_orden_ordenes_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'nc_notas_credito_orden_ordenesnc_notas_credito_ida' => 
      array (
        'name' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
        'type' => 'id',
      ),
      'nc_notas_credito_orden_ordenesorden_ordenes_idb' => 
      array (
        'name' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'nc_notas_credito_orden_ordenesspk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'nc_notas_credito_orden_ordenes_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'nc_notas_credito_orden_ordenes_idb2',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
        ),
      ),
    ),
    'lhs_module' => 'NC_Notas_credito',
    'lhs_table' => 'nc_notas_credito',
    'lhs_key' => 'id',
    'rhs_module' => 'orden_Ordenes',
    'rhs_table' => 'orden_ordenes',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-one',
    'join_table' => 'nc_notas_credito_orden_ordenes_c',
    'join_key_lhs' => 'nc_notas_credito_orden_ordenesnc_notas_credito_ida',
    'join_key_rhs' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
    'readonly' => true,
    'relationship_name' => 'nc_notas_credito_orden_ordenes',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'nc_notas_credito_modified_user' => 
  array (
    'name' => 'nc_notas_credito_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'NC_Notas_credito',
    'rhs_table' => 'nc_notas_credito',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'nc_notas_credito_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nc_notas_credito_created_by' => 
  array (
    'name' => 'nc_notas_credito_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'NC_Notas_credito',
    'rhs_table' => 'nc_notas_credito',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'nc_notas_credito_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nc_notas_credito_activities' => 
  array (
    'name' => 'nc_notas_credito_activities',
    'lhs_module' => 'NC_Notas_credito',
    'lhs_table' => 'nc_notas_credito',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'NC_Notas_credito',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'nc_notas_credito_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nc_notas_credito_following' => 
  array (
    'name' => 'nc_notas_credito_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'NC_Notas_credito',
    'rhs_table' => 'nc_notas_credito',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'NC_Notas_credito',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'nc_notas_credito_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nc_notas_credito_favorite' => 
  array (
    'name' => 'nc_notas_credito_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'NC_Notas_credito',
    'rhs_table' => 'nc_notas_credito',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'NC_Notas_credito',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'nc_notas_credito_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nc_notas_credito_assigned_user' => 
  array (
    'name' => 'nc_notas_credito_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'NC_Notas_credito',
    'rhs_table' => 'nc_notas_credito',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'nc_notas_credito_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'nc_notas_credito_lf_facturas_1' => 
  array (
    'rhs_label' => 'Facturas',
    'lhs_label' => 'Notas de crédito',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'NC_Notas_credito',
    'rhs_module' => 'LF_Facturas',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'nc_notas_credito_lf_facturas_1',
  ),
);