<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'accounts_orden_ordenes_1' => 
  array (
    'name' => 'accounts_orden_ordenes_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'accounts_orden_ordenes_1' => 
      array (
        'lhs_module' => 'Accounts',
        'lhs_table' => 'accounts',
        'lhs_key' => 'id',
        'rhs_module' => 'orden_Ordenes',
        'rhs_table' => 'orden_ordenes',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'accounts_orden_ordenes_1_c',
        'join_key_lhs' => 'accounts_orden_ordenes_1accounts_ida',
        'join_key_rhs' => 'accounts_orden_ordenes_1orden_ordenes_idb',
      ),
    ),
    'table' => 'accounts_orden_ordenes_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'accounts_orden_ordenes_1accounts_ida' => 
      array (
        'name' => 'accounts_orden_ordenes_1accounts_ida',
        'type' => 'id',
      ),
      'accounts_orden_ordenes_1orden_ordenes_idb' => 
      array (
        'name' => 'accounts_orden_ordenes_1orden_ordenes_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'accounts_orden_ordenes_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'accounts_orden_ordenes_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'accounts_orden_ordenes_1accounts_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'accounts_orden_ordenes_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'accounts_orden_ordenes_1orden_ordenes_idb',
        ),
      ),
    ),
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'orden_Ordenes',
    'rhs_table' => 'orden_ordenes',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'accounts_orden_ordenes_1_c',
    'join_key_lhs' => 'accounts_orden_ordenes_1accounts_ida',
    'join_key_rhs' => 'accounts_orden_ordenes_1orden_ordenes_idb',
    'readonly' => true,
    'relationship_name' => 'accounts_orden_ordenes_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'orden_ordenes_modified_user' => 
  array (
    'name' => 'orden_ordenes_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'orden_Ordenes',
    'rhs_table' => 'orden_ordenes',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'orden_ordenes_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'orden_ordenes_created_by' => 
  array (
    'name' => 'orden_ordenes_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'orden_Ordenes',
    'rhs_table' => 'orden_ordenes',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'orden_ordenes_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'orden_ordenes_activities' => 
  array (
    'name' => 'orden_ordenes_activities',
    'lhs_module' => 'orden_Ordenes',
    'lhs_table' => 'orden_ordenes',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'orden_Ordenes',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'orden_ordenes_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'orden_ordenes_following' => 
  array (
    'name' => 'orden_ordenes_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'orden_Ordenes',
    'rhs_table' => 'orden_ordenes',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'orden_Ordenes',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'orden_ordenes_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'orden_ordenes_favorite' => 
  array (
    'name' => 'orden_ordenes_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'orden_Ordenes',
    'rhs_table' => 'orden_ordenes',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'orden_Ordenes',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'orden_ordenes_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'orden_ordenes_assigned_user' => 
  array (
    'name' => 'orden_ordenes_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'orden_Ordenes',
    'rhs_table' => 'orden_ordenes',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'orden_ordenes_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'orden_ordenes_orden_ordenes_1' => 
  array (
    'rhs_label' => 'Ordenes',
    'lhs_label' => 'Ordenes',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'orden_Ordenes',
    'rhs_module' => 'orden_Ordenes',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'orden_ordenes_orden_ordenes_1',
  ),
);