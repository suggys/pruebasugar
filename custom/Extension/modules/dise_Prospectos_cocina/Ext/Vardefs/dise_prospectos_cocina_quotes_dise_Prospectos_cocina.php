<?php
// created: 2018-01-11 00:47:54
$dictionary["dise_Prospectos_cocina"]["fields"]["dise_prospectos_cocina_quotes"] = array (
  'name' => 'dise_prospectos_cocina_quotes',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_quotes',
  'source' => 'non-db',
  'module' => 'Quotes',
  'bean_name' => 'Quote',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'id_name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
  'link-type' => 'many',
  'side' => 'left',
);
