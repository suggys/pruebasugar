<?php
 // created: 2018-02-06 05:01:56
$dictionary['dise_Prospectos_cocina']['fields']['categoria_otra_c']['labelValue']='Otra categoría';
$dictionary['dise_Prospectos_cocina']['fields']['categoria_otra_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['dise_Prospectos_cocina']['fields']['categoria_otra_c']['enforced']='';
$dictionary['dise_Prospectos_cocina']['fields']['categoria_otra_c']['dependency']='equal($categoria,"Otros")';

 ?>