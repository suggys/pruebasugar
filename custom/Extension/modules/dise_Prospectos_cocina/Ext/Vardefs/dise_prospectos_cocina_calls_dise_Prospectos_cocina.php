<?php
// created: 2018-02-01 19:07:34
$dictionary["dise_Prospectos_cocina"]["fields"]["dise_prospectos_cocina_calls"] = array (
  'name' => 'dise_prospectos_cocina_calls',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_CALLS_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'id_name' => 'dise_prospectos_cocina_callsdise_prospectos_cocina_ida',
  'link-type' => 'many',
  'side' => 'left',
);
