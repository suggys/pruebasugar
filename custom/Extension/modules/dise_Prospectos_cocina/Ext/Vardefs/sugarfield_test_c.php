<?php
 // created: 2018-02-06 05:01:56
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['duplicate_merge_dom_value']=0;
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['labelValue']='Sucursal';
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['calculated']='1';
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['formula']='getDropdownValue("sucursal_c_list",related($created_by_link,"sucursal_c"))';
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['enforced']='1';
$dictionary['dise_Prospectos_cocina']['fields']['test_c']['dependency']='';

 ?>