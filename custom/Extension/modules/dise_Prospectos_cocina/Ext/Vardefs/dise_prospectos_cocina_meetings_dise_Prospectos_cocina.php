<?php
// created: 2018-02-01 19:07:34
$dictionary["dise_Prospectos_cocina"]["fields"]["dise_prospectos_cocina_meetings"] = array (
  'name' => 'dise_prospectos_cocina_meetings',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_MEETINGS_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'id_name' => 'dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida',
  'link-type' => 'many',
  'side' => 'left',
);
