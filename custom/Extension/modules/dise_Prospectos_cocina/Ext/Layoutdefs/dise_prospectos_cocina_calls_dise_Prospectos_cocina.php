<?php
 // created: 2018-02-01 19:07:34
$layout_defs["dise_Prospectos_cocina"]["subpanel_setup"]['dise_prospectos_cocina_calls'] = array (
  'order' => 100,
  'module' => 'Calls',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DISE_PROSPECTOS_COCINA_CALLS_FROM_CALLS_TITLE',
  'get_subpanel_data' => 'dise_prospectos_cocina_calls',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
