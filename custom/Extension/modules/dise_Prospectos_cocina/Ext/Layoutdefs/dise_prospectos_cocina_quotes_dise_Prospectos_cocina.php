<?php
 // created: 2018-01-11 00:47:54
$layout_defs["dise_Prospectos_cocina"]["subpanel_setup"]['dise_prospectos_cocina_quotes'] = array (
  'order' => 100,
  'module' => 'Quotes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_QUOTES_TITLE',
  'get_subpanel_data' => 'dise_prospectos_cocina_quotes',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
