<?php
// created: 2017-07-21 12:47:08
$dictionary["Note"]["fields"]["low01_solicitudescredito_activities_notes"] = array (
  'name' => 'low01_solicitudescredito_activities_notes',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_notes',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_NOTES_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
);
