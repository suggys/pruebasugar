<?php
 // created: 2016-09-21 15:25:57
$dictionary['Document']['fields']['doc_type']['len']=100;
$dictionary['Document']['fields']['doc_type']['audited']=false;
$dictionary['Document']['fields']['doc_type']['comments']='Document type (ex: Google, box.net, IBM SmartCloud)';
$dictionary['Document']['fields']['doc_type']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['doc_type']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['doc_type']['merge_filter']='disabled';
$dictionary['Document']['fields']['doc_type']['calculated']=false;
$dictionary['Document']['fields']['doc_type']['dependency']=false;

 ?>