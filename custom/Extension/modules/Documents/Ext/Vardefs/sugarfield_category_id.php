<?php
 // created: 2016-09-21 15:43:59
$dictionary['Document']['fields']['category_id']['audited']=false;
$dictionary['Document']['fields']['category_id']['massupdate']=true;
$dictionary['Document']['fields']['category_id']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['category_id']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['category_id']['merge_filter']='disabled';
$dictionary['Document']['fields']['category_id']['calculated']=false;
$dictionary['Document']['fields']['category_id']['dependency']=false;

 ?>