<?php
 // created: 2016-09-21 15:25:22
$dictionary['Document']['fields']['description']['audited']=false;
$dictionary['Document']['fields']['description']['massupdate']=false;
$dictionary['Document']['fields']['description']['comments']='Full text of the note';
$dictionary['Document']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Document']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Document']['fields']['description']['merge_filter']='disabled';
$dictionary['Document']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.61',
  'searchable' => true,
);
$dictionary['Document']['fields']['description']['calculated']=false;
$dictionary['Document']['fields']['description']['rows']='6';
$dictionary['Document']['fields']['description']['cols']='80';

 ?>