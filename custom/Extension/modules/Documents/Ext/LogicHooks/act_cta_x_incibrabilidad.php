<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    '1',
    'Actualiza_cta_x_incobrabilidad',
    'custom/modules/Documents/UpdateAccountIncobrabilidad.php',
    'UpdateAccountIncobrabilidad',
    'UpdateAccountIncobrabilidad'
);

$hook_array['after_relationship_add'][] = array(
  '2',
  'Actualiza cuenta y le genera el Id AR Credit',
  'custom/modules/Documents/UpdateAccountIDARCredit.php',
  'UpdateAccountIDARCredit',
  'UpdateAccountIDARCredit'
);

$hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
$hook_array['after_relationship_delete'][] = array(
    '1',
    'Actualiza_cta_x_incobrabilidad al desvincular',
    'custom/modules/Documents/UpdateAccountIncobrabilidad.php',
    'UpdateAccountUnlink',
    'UpdateAccountUnlink'
);

$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
    '1',
    'Documents logic hook manager',
    'custom/modules/Documents/DocumentsLogicHooksManager.php',
    'DocumentsLogicHooksManager',
    'beforeSave'
);

$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
    '1',
    'Actualiza cta al Guardar documento como CartaIncobrabilidad',
    'custom/modules/Documents/ActCtaxIncobrabilidad.php',
    'ActCtaxIncobrabilidad',
    'UpdateCtaIncobrabilidad'
);
$hook_array['after_save'][] = array(
    '2',
    'Documents logic hook manager',
    'custom/modules/Documents/DocumentsLogicHooksManager.php',
    'DocumentsLogicHooksManager',
    'afterSave'
);



?>
