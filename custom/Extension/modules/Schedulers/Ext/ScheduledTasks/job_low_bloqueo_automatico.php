<?php
$job_strings[] = 'job_low_bloqueo_automatico';

array_push($job_strings, 'job_low_bloqueo_automatico');

/*************************************************************
FR-11.5. Proceso de bloqueo automático
Escenario 1: Obtener cuentas activas a bloquear por Cartera vencida 1 a 30 días
Dado: Las siguientes cuentas
1. Nombre: [FR-11.5: Cuenta Inactiva], estado de la cuenta: [Inactiva]
2. Nombre: [FR-11.5: Cuenta Activa], estado de la cuenta: [Activa], Estado Según Cartera: [Vigente], Estado de bloqueo:[Desbloqueado]
3. Nombre: [FR-11.5: Cuenta Activa Vencida], estado de la cuenta: [Activa],
 Estado Según Cartera: [Cartera vencida 1 a 30 días], Estado de bloqueo:[Desbloqueado]
Cuando:  Una función filtre las cuentas para obtener las activas con catera vencida
Entonces: Debe de obtener una colección con solamente el registro con Nombre: [FR-11.5: Cuenta Activa Vencida]

Escenario 2: Obtener cuentas activas a bloquear por Cartera vencida 31 a 120 días
Dado: Las siguientes cuentas
1. Nombre: [FR-11.5: Cuenta Inactiva], estado de la cuenta: [Inactiva]
2. Nombre: [FR-11.5: Cuenta Activa], estado de la cuenta: [Activa], Estado Según Cartera: [Vigente], Estado de bloqueo:[Desbloqueado]
3. Nombre: [FR-11.5: Cuenta Activa Vencida], estado de la cuenta: [Activa],
 Estado Según Cartera: [Cartera vencida 31 a 120 días], Estado de bloqueo:[Desbloqueado]
Cuando:  Una función filtre las cuentas para obtener las activas con catera vencida
Entonces: Debe de obtener una colección con solamente el registro con Nombre: [FR-11.5: Cuenta Activa Vencida]

Escenario 3: Obtener cuentas activas a bloquear por Cartera vencida 120 días
Dado: Las siguientes cuentas
1. Nombre: [FR-11.5: Cuenta Inactiva], estado de la cuenta: [Inactiva]
2. Nombre: [FR-11.5: Cuenta Activa], estado de la cuenta: [Activa], Estado Según Cartera: [Vigente], Estado de bloqueo:[Desbloqueado]
3. Nombre: [FR-11.5: Cuenta Activa Vencida], estado de la cuenta: [Activa],
 Estado Según Cartera: [Cartera vencida 120 días], Estado de bloqueo:[Desbloqueado]
Cuando:  Una función filtre las cuentas para obtener las activas con catera vencida
Entonces: Debe de obtener una colección con solamente el registro con Nombre: [FR-11.5: Cuenta Activa Vencida]

Escenario 4: Obtener cuentas activas a bloquear por Cartera vencida 120 días omitiendo Bloquedas
Dado: Las siguientes cuentas
1. Nombre: [FR-11.5: Cuenta Inactiva], estado de la cuenta: [Inactiva]
2. Nombre: [FR-11.5: Cuenta Activa], estado de la cuenta: [Activa], Estado Según Cartera: [Vigente], Estado de bloqueo:[Desbloqueado]
3. Nombre: [FR-11.5: Cuenta Activa Vencida], estado de la cuenta: [Activa],
 Estado Según Cartera: [Cartera vencida 120 días], Estado de bloqueo:[Desbloqueado]
4. Nombre: [FR-11.5: Cuenta Activa Vencida Bloqueada], estado de la cuenta: [Activa],
 Estado Según Cartera: [Cartera vencida 120 días], Estado de bloqueo:[Bloqueado]
Cuando:  Una función filtre las cuentas para obtener las activas con catera vencida
Entonces: Debe de obtener una colección con solamente el registro con Nombre: [FR-11.5: Cuenta Activa Vencida]
**************************************************************/
function job_low_bloqueo_automatico(){
    //$cuentasBloqueadas = fn_bloqueoDeCuentasXCarteraVencida();
    // $GLOBALS['log']->fatal( 'job_low_bloqueo_automatico::cuentasBloqueadas:'. $cuentasBloqueadas);

    //$cuentasBloqueadas = fn_bloqueGpoCuentasCarteraVencida();
    // $GLOBALS['log']->fatal( 'job_low_bloqueo_automatico::cuentasBloqueadas Gpo:'. $cuentasBloqueadas);

    return true;
}

/*
Escenario 6: Bloquear cuentas con Codigo de bloqueo [CV – Cartera vencida] omitiendo Cuentas Bloqueadas
Dado: Un grupo de empresas con Nombre: [FR-11.5-E1: GE], Sumar / Administrar Límite de Crédito por Grupo[Activo]
- Y: Una colección de 3 Cuentas con Estado de bloqueo:[Desbloquedo] Relacionada al registro del modulo Grupo de empresas con Nombre: [FR-11.5-E6: GE]
- Y: Una cuenta con Nombre[FR-11.5-E6: Cuenta 4] Estado de bloqueo:[Desbloquedo]
Cuando:  Se procesen por una función para bloquear por cartera vencida
Entonces: Debe de refrescar la información de cada cuenta y validar Estado de bloqueo igual a [Desbloquedo], si es verdadero Asignar  Estado de bloqueo:[Bloqueado],  Codigo de bloqueo[CV – Cartera vencida]
*/
function fn_bloqueGpoCuentasCarteraVencida() {
  $cuentasBloqueadas = 0;
  require_once('include/SugarQuery/SugarQuery.php');

  $sq = new SugarQuery();
  $sq->select(array('id'));
  $sq->from(BeanFactory::newBean('Accounts'),array('alias'=>'a','team_security'=>false));
  $sq->joinTable('gpe_grupo_empresas_accounts_c', array('alias' => 'gpa'))->on()
  ->equalsField('gpa.gpe_grupo_empresas_accountsaccounts_idb','a.id');
  $sq->joinTable('gpe_grupo_empresas', array('alias' => 'gpe'))->on()
  ->equalsField('gpe.id','gpa.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida');
  $sq->whereRaw("gpe.administrar_credito_grupal = 1");
  $sq->where()->queryAnd()->equals('estado_bloqueo_c','Desbloqueado')
  ->equals('estado_cuenta_c','Activo')
  ->gt('saldo_pendiente_aplicar_c',0);
  $sq->oderBy("id");
  $result = $sq->execute();

  // $sqlQuery = "SELECT accounts.name, IFNULL(gpe_grupo_empresas.name, '') GPO,
  // accounts.id id_emp, gpe_grupo_empresas.id id_gpo, id_c, estado_cuenta_c, estado_bloqueo_c,
  // codigo_bloqueo_c, estado_cartera_c, saldo_pendiente_aplicar_c, administrar_credito_grupal
  // FROM accounts
  // INNER JOIN accounts_cstm ON accounts.id = id_c
  // INNER JOIN gpe_grupo_empresas_accounts_c ON gpe_grupo_empresas_accountsaccounts_idb = accounts.id
  // INNER JOIN gpe_grupo_empresas ON gpe_grupo_empresas.id = gpe_grupo_empresas_accountsgpe_grupo_empresas_ida
  // WHERE accounts.deleted <> 1 AND gpe_grupo_empresas.administrar_credito_grupal = 1
  // AND estado_bloqueo_c='Desbloqueado' AND estado_cuenta_c='Activo' AND saldo_pendiente_aplicar_c > 0
  // ORDER BY accounts.id";

  foreach ($result as $key => $row) {
    $beanAccount = BeanFactory::getBean('Accounts', $row['id_c']);
    $beanAccount->codigo_bloqueo_c = 'CV';
    $beanAccount->estado_bloqueo_c = 'Bloqueado';
    $beanAccount->save();
    $cuentasBloqueadas++;
  }
  return $cuentasBloqueadas;
}

/*
Escenario 5: Bloquear cuentas con Codigo de bloqueo [CV – Cartera vencida]
Dado: Una colección de 3 Cuentas con Estado de bloqueo:[Desbloquedo]
Cuando:  Se procesen por una función para bloquear por cartera vencida
Entonces: La cada una de las cuentas procecadas debe de tener  Estado de bloqueo:[Bloqueado],  Codigo de bloqueo[CV – Cartera vencida]
*/
function fn_bloqueoDeCuentasXCarteraVencida() {
  $cuentasBloqueadas = 0;
  require_once('include/SugarQuery/SugarQuery.php');

  $sq = new SugarQuery();
  $sq->select(array('id'));
  $sq->from(BeanFactory::newBean('Accounts'),array('alias'=>'a','team_security'=>false));
  $sq->joinTable('gpe_grupo_empresas_accounts_c', array('alias' => 'gpa'))->on()
  ->equalsField('gpa.gpe_grupo_empresas_accountsaccounts_idb','a.id');
  $sq->joinTable('gpe_grupo_empresas', array('alias' => 'gpe'))->on()
  ->equalsField('gpe.id','gpa.gpe_grupo_empresas_accountsgpe_grupo_empresas_ida');
  $sq->whereRaw("IFNULL(gpe.administrar_credito_grupal, 0) = 0");
  $sq->where()->queryAnd()->equals('estado_bloqueo_c','Desbloqueado')
  ->equals('estado_cuenta_c','Activo')
  ->gt('saldo_pendiente_aplicar_c',0);
  $sq->oderBy("id");
  $result = $sq->execute();

  // $sqlQuery = "SELECT id
  // FROM accounts
  // INNER JOIN accounts_cstm ON accounts.id = id_c
  // LEFT JOIN gpe_grupo_empresas_accounts_c ON gpe_grupo_empresas_accountsaccounts_idb = accounts.id
  // LEFT JOIN gpe_grupo_empresas ON gpe_grupo_empresas.id = gpe_grupo_empresas_accountsgpe_grupo_empresas_ida
  // WHERE accounts.deleted <> 1 AND IFNULL(gpe_grupo_empresas.administrar_credito_grupal, 0) = 0
  // AND estado_bloqueo_c='Desbloqueado' AND estado_cuenta_c='Activo' AND saldo_pendiente_aplicar_c > 0
  // ORDER BY accounts.id";

  foreach ($result as $key => $row) {
    $beanAccount = BeanFactory::getBean('Accounts', $row['id_c']);
    $beanAccount->codigo_bloqueo_c = 'CV';
    $beanAccount->estado_bloqueo_c = 'Bloqueado';
    $beanAccount->save();
    $cuentasBloqueadas++;
  }
  return $cuentasBloqueadas;
}
?>
