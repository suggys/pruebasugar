<?php
$job_strings[] = 'job_actualiza_check_sin_orden_xdias_c';
array_push($job_strings, 'job_actualiza_check_sin_orden_xdias_c');
/**
 * [job_actualiza_check_sin_orden_xdias_c Se consultan la relacion mas reciente de ordenes con cuentas y si han pasado los dias
 * configurados setea el campo sin_orden_xdias_c siempre y cuando la cuenta no tenga estatus inactivo]
 * @return [bool]
 */
function job_actualiza_check_sin_orden_xdias_c() {
    global $db;
    // obteniendo la cantidad de dias parametrizada
    $dias = 120;
    $admin = new Administration();
    $admin->retrieveSettings();
    if (array_key_exists('lowes_settings_accounts_actividad_cliente', $admin->settings))
        $dias = $admin->settings['lowes_settings_accounts_actividad_cliente'];
    // consultando cuentas activas que no tengan mas de x dias sin ordenes y que la orden relacionada mas reciente tenga mas de x dias
    $Ctas = new SugarQuery();
    $Ctas->select('id');
    $beanCuentas = BeanFactory::newBean('Accounts');
    $Ctas->from($beanCuentas, array('team_security' => false));
    $Ctas->join('accounts_orden_ordenes_1',array('alias'=>'o'));
    $Ctas->where()
         ->equals('estado_cuenta_c','Activo')
         ->equals('sin_orden_xdias_c','false');
    $Ctas->groupBy('id');
    $Ctas->havingRaw("datediff(date(now()),max(date(jt0_accounts_orden_ordenes_1_c.date_modified))) > ".$dias);
    $resCuentas = $Ctas->execute();
    actualizaCuentas($resCuentas);
    return true;
}
function actualizaCuentas($resCuentas){
    if(count($resCuentas)>0){
        // encontramos cuentas por actualizar +120 dias sin ordenes
        foreach ($resCuentas as $key => $cuenta) {
            $beanAccount = BeanFactory::getBean('Accounts', $cuenta['id']);
            if($beanAccount->id){
                $beanAccount->sin_orden_xdias_c = true;
                $beanAccount->save(false);
            }
        }
    }
}
?>
