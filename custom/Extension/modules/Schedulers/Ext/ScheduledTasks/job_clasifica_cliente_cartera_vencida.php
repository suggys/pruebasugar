<?php

$job_strings[] = 'job_clasifica_cliente_cartera_vencida';

array_push($job_strings, 'job_clasifica_cliente_cartera_vencida');


/*
NOTA:   Hay que tomar en cuenta que para ejecutar este proceso es necesario
        primero ejecutar los job de calculo de cartera vencida
*/
function job_clasifica_cliente_cartera_vencida() {
    //HSLR
    _ppl('dentro de job_cartera vencida');
    $isDebug = false;
    global $db;
  

    $sqlQuery = "SELECT MAX(fc.dias_vencidos_sin_tol_c) dias_vencimiento, a.id, a.name, ac.estado_bloqueo_c, ac.codigo_bloqueo_c
        FROM accounts a
        INNER JOIN accounts_cstm ac ON ac.id_c=a.id
        INNER JOIN accounts_lf_facturas_1_c fa ON fa.accounts_lf_facturas_1accounts_ida=a.id AND fa.deleted <>1
        INNER JOIN lf_facturas f ON f.id=fa.accounts_lf_facturas_1lf_facturas_idb
        INNER JOIN lf_facturas_cstm fc ON f.id=fc.id_c
        WHERE
         f.estado_tiempo NOT IN ('pagada', '', 'devuelta_parcial', 'devuelta_completa') 
         AND f.deleted <> 1 AND a.deleted <> 1 
         AND fc.linea_anticipo_c = 0 
         AND f.tipo_documento='factura' 
         AND f.dt_crt IS NOT NULL
         AND ac.codigo_bloqueo_c NOT IN ('CV','CL', 'Otro','DI')
        GROUP BY id, name;";

    //HSLR
    $GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida::sql:'. $sqlQuery);

    //Primero ejecutamos estea función que habilita las carteras que estaban vencidas pero ya pagaron
    //fnHabilitaCarteraCuentasPagadas();

    $res = $db->query($sqlQuery);
    $num_rows = $res->num_rows;

    $GLOBALS['log']->fatal(' --------------- '. $num_rows);

    //if($isDebug)$GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida::num_rows:'. $num_rows);
    if($num_rows) {
        $GLOBALS['log']->fatal(' -------ENTRO AL IF --------');
        while($row = $db->fetchByAssoc($res)){
            //if($isDebug)$GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida factura::'. $row['name']);
            //if($isDebug)$GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida dias_vencimiento::'. $row['dias_vencimiento']);

            $GLOBALS['log']->fatal(' -----dentro del while --- ');

            $beanAccount = BeanFactory::getBean('Accounts', $row['id']);
            $estadoCarteraPrev = $beanAccount->estado_cartera_c;
            if(intval($row['dias_vencimiento']) > 0){
              if(intval($row['dias_vencimiento']) > 0 && intval($row['dias_vencimiento']) <= 30){
                $beanAccount->estado_cartera_c = 'CV130D';
              }
              else if(intval($row['dias_vencimiento']) > 30 && intval($row['dias_vencimiento']) <= 120){
                $beanAccount->estado_cartera_c = 'CV31120D';
              }
              else if(intval($row['dias_vencimiento']) > 120){
                $beanAccount->estado_cartera_c = 'CV120D';
              }

              if(
                $beanAccount->estado_cartera_c != 'CV'
              ){
                $GLOBALS['log']->fatal('---------- CLIENTES: ------'.$beanAccount->name);
                $beanAccount->estado_bloqueo_c = 'Bloqueado';
                if (empty($beanAccount->codigo_bloqueo_c) || $beanAccount->codigo_bloqueo_c === "LE") {
                  $beanAccount->codigo_bloqueo_c = 'CV';
                }
                $beanAccount->noBloqueante = false;
              }
              //if($isDebug)$GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida account:estado_cartera_c::'. $beanAccount->estado_cartera_c );
              $beanAccount->save(false);
              //if($isDebug)$GLOBALS['log']->fatal( 'job_clasifica_cliente_cartera_vencida account:estado_cartera_c:after:'. $beanAccount->estado_cartera_c );
            }
        }
    }

    return true;
}
?>
