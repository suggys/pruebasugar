<?php
$job_strings[] = 'JOB_copia_info_Pros_Clients';
array_push($job_strings, 'JOB_copia_info_Pros_Clients');


//Actualiza el estado de bloqueo si l cliente tiene limite de credito excedido
function JOB_copia_info_Pros_Clients(){
    global $timedate, $db;

    $query='UPDATE accounts a
        INNER JOIN accounts_cstm ac ON a.id=ac.id_c
        INNER JOIN prospects_accounts_1_c pa ON pa.prospects_accounts_1accounts_idb=a.id
        INNER JOIN prospects p ON p.id=pa.prospects_accounts_1prospects_ida
        INNER JOIN prospects_cstm pc ON pc.id_c=p.id
        SET ac.primary_address_postalcode_c=p.primary_address_postalcode,
        ac.primary_address_street_c=p.primary_address_street,
        ac.primary_address_numero_c=pc.primary_address_number_c,
        ac.primary_address_city_c=p.primary_address_city,
        ac.primary_address_state_c=p.primary_address_state,
        ac.primary_address_colonia_c=pc.primary_address_colonia_c,
        ac.primary_address_country_c=p.primary_address_country,
        a.phone_office=p.phone_work
        WHERE ac.id_pos_c IS NOT NULL 
        AND ac.primary_address_postalcode_c = " "
        AND ac.primary_address_postalcode_c IS NOT NULL
        AND p.primary_address_postalcode IS NOT NULL 
        AND p.primary_address_postalcode != " "';

    $result01 = $db->query($query);

    $query2='UPDATE accounts a
        INNER JOIN accounts_cstm ac ON a.id=ac.id_c
        INNER JOIN prospects_accounts_1_c pa ON pa.prospects_accounts_1accounts_idb=a.id
        INNER JOIN prospects p ON p.id=pa.prospects_accounts_1prospects_ida
        INNER JOIN prospects_cstm pc ON pc.id_c=p.id
        SET ac.primary_address_postalcode_c=p.primary_address_postalcode,
        ac.primary_address_street_c=p.primary_address_street,
        ac.primary_address_numero_c=pc.primary_address_number_c,
        ac.primary_address_city_c=p.primary_address_city,
        ac.primary_address_state_c=p.primary_address_state,
        ac.primary_address_colonia_c=pc.primary_address_colonia_c,
        ac.primary_address_country_c=p.primary_address_country,
        a.phone_office=p.phone_work
        WHERE ac.id_pos_c IS NOT NULL 
        AND ac.primary_address_postalcode_c IS NULL 
        AND p.primary_address_postalcode IS NOT NULL  
        AND p.primary_address_postalcode != " "';

    $result02 = $db->query($query2);

    return true;
}
?>