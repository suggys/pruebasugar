<?php
$job_strings[] = 'job_incumplimiento_promesa_pago';

array_push($job_strings, 'job_incumplimiento_promesa_pago');

function job_incumplimiento_promesa_pago() {
    global $db, $timedate;

    $query ="UPDATE lowae_autorizacion_especial ae
    INNER JOIN lowae_autorizacion_especial_cstm aec on ae.id = aec.id_c
    INNER JOIN (
      SELECT id, name, fecha_cumplimiento_c, aec.dias_vencimiento_promesa_c,
      DATEDIFF(NOW(), ae.fecha_cumplimiento_c) diffnow
      FROM lowae_autorizacion_especial ae
      INNER JOIN lowae_autorizacion_especial_cstm aec ON ae.id = aec.id_c
      WHERE ae.deleted = 0
      AND ae.motivo_solicitud_c = 'promesa_de_pago'
      AND aec.estado_promesa_segun_monto_c IN ('sin_pago', 'pagada_parcial')
      AND aec.estado_autorizacion_c IN ('autorizada', 'finalizada')
      AND ae.fecha_cumplimiento_c < NOW()
    ) aei ON aei.id = ae.id
    SET ae.estado_promesa_c = 'incumplida'";
    $result1 = $db->query($query);

    $query ="UPDATE lowae_autorizacion_especial ae
    INNER JOIN lowae_autorizacion_especial_cstm aec on ae.id = aec.id_c
    INNER JOIN (
      SELECT id, name, fecha_cumplimiento_c, aec.dias_vencimiento_promesa_c,
      DATEDIFF(NOW(), ae.fecha_cumplimiento_c) diffnow
      FROM lowae_autorizacion_especial ae
      INNER JOIN lowae_autorizacion_especial_cstm aec ON ae.id = aec.id_c
      WHERE ae.deleted = 0
      AND ae.motivo_solicitud_c = 'promesa_de_pago'
      AND ae.monto_abonado_c < ae.monto_promesa_c
      AND ae.estado_promesa_c = 'incumplida'
      AND aec.estado_promesa_segun_monto_c IN ('sin_pago', 'pagada_parcial')
    ) aei ON aei.id = ae.id
    SET aec.dias_vencimiento_promesa_c = aei.diffnow";

    $result1 = $db->query($query);

    return true;
}
?>
