<?php

$job_strings[] = 'envioncMiddlewareJOB';

array_push($job_strings, 'envioncMiddlewareJOB');
function envioncMiddlewareJOB(){
    global $current_user, $timedate, $app_list_strings;
    global $estados, $tipo_telefono, $paises, $url;
    global $urlws;

    $urlws = 'http://json.lowes.com.mx/convert2json/convert2json/';
   

    $GLOBALS['log']->fatal('Entra a JOB envio de Nc al middleware');
   
    $nc1 = generaNotaCredito();
    if(count($nc1)>0){
        $data1 = json_encode($nc1);
        $respMW = llamaMiddlewareNc($data1);
        //$respMW = llamaMiddleware($suc, $data1);
        return true;
    }
    return true;
}

function generaNotaCredito(){
    global $current_user,$estados, $tipo_telefono;

    // creacion de la fecha 
    $anio=date("Y");
    $mes=date("m");
    $dia=date("d")-1;
         
    if (strlen($dia) === 1) {
        $dia = '0'.$dia;
    }

    $fecha=$anio.'-'.$mes.'-'.$dia;
    $GLOBALS["log"]->fatal("FRCHA NC: ".$fecha);
    $Nc = new SugarQuery();
    $Nc->select();
    $beanNc = BeanFactory::newBean('NC_Notas_credito');
    $Nc->from($beanNc, array('team_security' => false));
    $Nc->where()->contains('fecha_emision_c',$fecha);
    $resNC = $Nc->execute();
    $GLOBALS['log']->fatal($resNC);
    $totNc = array();

    foreach ($resNC as $r) {
        $NC['id_sugar'] = $r['id'];
        $NC['NT_NC'] = $r['no_ticket_c'];
        $NC['UUID_NC'] = $r['uuid_c'];
        $NC['FOLIO_NC'] = $r['name'];
        $NC['SERIE_NC'] = $r['series_id_c'];
        $NC['IMP_NC'] = $r['importe_total'];
        $NC['ARC_NC'] = $r['importadode_c'];
        $NC['FECHA_NC'] = $r['fecha_emision_c'];
        if($r['id']){
            array_push($totNc,$NC);
        }
    }
    return $totNc;
}


function llamaMiddlewareNc($data){
    global $current_user, $urlws;
    $dom = '';
    $configurator = new Configurator();
    $configurator->loadConfig();
    $url = $configurator->config['host_name'];
    $urlApi ='http://100.24.211.220/Middleware/json2csv.php';
    $ch = curl_init($urlApi);
    curl_setopt($ch, CURLOPT_POST, true); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, "jsonData=".$data.'#Nc_sugar_'.date('Ymd').'.csv');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    return $result;
}


