<?php
require_once 'custom/include/PagosKonesh.php';
$job_strings[] = 'JOB_envia_pagos_kones';
array_push($job_strings, 'JOB_envia_pagos_konesh');
// Reporte Promesas de pago clasifica
function JOB_envia_pagos_konesh(){
  $pagoKonesh = new PagosKonesh();
  $pagoKonesh->process();
  return true;
}
