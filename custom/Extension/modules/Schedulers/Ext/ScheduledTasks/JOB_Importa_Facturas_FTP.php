<?php

array_push($job_strings, 'importaFacturasFTP');

function importaFacturasFTP(){
	require_once('custom/modules/Schedulers/FTPGateway.php');
	require_once('custom/modules/Import/CustomImporter.php');
	require_once('include/upload_file.php');
	// descargando cuentas
	$gateway = new FTPGateway();
	try {
		$GLOBALS['log']->debug(" ==== Iniciando =====");
		$gateway->connect();
		$GLOBALS['log']->debug(" ==== listado de archivos =====");
		$response = $gateway->scanFilesystem('bdi_sugarcrm','facturas',false);
		if(count($response)){
			$files = $response;
			$localUrl = 'upload/';
			$GLOBALS['log']->debug(" ==== descargando archivos =====");
			$bean = BeanFactory::getBean('LF_Facturas');
			$stream = new UploadStream();
			foreach ($files as $key => $file) {
				$gateway->downloadFile('bdi_sugarcrm/'.$file,$localUrl.$file);
				importaArchivo($localUrl.$file,$bean);
				// eliminando archivo local
				$stream->unlink('upload://'.$file);
				// moviendo archivo remoto
				$gateway->moveFile('bdi_sugarcrm/'.$file,'bdi_sugarcrm_procesado/'.$file);
			}
		}
	}
	catch (Exception $e){
	    $GLOBALS['log']->debug(" ** Error *** importaFacturasFTP ***** ".$e->getMessage());
	}
	return true;
}
function importaArchivo($localFile,$bean) {
	if( $localFile && $bean ){
		// leyendo archivos
        $_REQUEST = array(
            'colnum_0'    => 'name',
            'colnum_1'    => 'description',
            'colnum_2'    => 'tipo_documento',
            'colnum_3'    => 'estado_tiempo',
            'colnum_4'    => 'no_ticket',
            'colnum_5'    => 'sub_total',
            'colnum_6'    => 'iva',
            'colnum_7'    => 'importe',
            'colnum_8'    => 'refactura',
            'colnum_9'    => 'fecha_entrega',
            'colnum_10'    => 'dt_crt',
            'colnum_11'    => 'fecha_vencimiento',
            'colnum_12'    => 'dias_vencimiento',
            'colnum_13'    => 'abono',
            'colnum_14'    => 'saldo',
            'colnum_15'    => 'estado',
            'colnum_16'    => 'uuid',
            'colnum_17'    => 'serie',
            'colnum_18'    => 'rfc_emisor',
            'colnum_19'    => 'nombre_emisor',
            'colnum_20'    => 'rfc_receptor',
            'colnum_21'    => 'name_receptor',
            'columncount' => '21',
            'importlocale_charset' => 'UTF-8',
            'importlocale_currency' => '-99',
            'importlocale_dateformat' => 'Y-m-d',
            'importlocale_dec_sep' => '.',
            'importlocale_default_currency_significant_digits' => '2',
            //'importlocale_default_locale_name_format' => 's f l',
            'importlocale_num_grp_sep' => ',',
            'importlocale_timeformat' => 'H:i',
            'importlocale_timezone' => 'America/Mexico_City',
            'import_module' => 'LF_Facturas',
        );
		$importFile = new ImportFile( $localFile );
		if($importFile->fileExists()){
			$autoDetectOk = $importFile->autoDetectCSVProperties();
			$importFile->setHeaderRow(true); // indicando que tiene header row
			$totalRecords = $importFile->getTotalRecordCount(); // indicando que tiene header row
			//$GLOBALS['log']->fatal(" ==== Total de registros a importar  =====".$totalRecords);
			$importer = new CustomImporter($importFile, $bean);
    		$importer->import();
    		//$GLOBALS['log']->fatal(" ==== Archivo ===== ".$localFile." importado :) ");
		}
	}
}