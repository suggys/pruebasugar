<?php

$job_strings[] = 'envioCtasMiddlewareJOB';

array_push($job_strings, 'envioCtasMiddlewareJOB');
function envioCtasMiddlewareJOB(){
    global $current_user, $timedate, $app_list_strings;
    global $estados, $tipo_telefono, $paises, $url;
    $estados = array('AGU' => 'AGU','BCN' => 'BCN','BCS' => 'BCS','CAM' => 'CAM','CHP' => 'CHP','CHH' => 'CHH','COA' => 'COA','COL' => 'COL','DIF' => 'DIF','CMX' => 'DIF','DUR' => 'DUR','GUA' => 'GUA','GRO' => 'GRO','HID' => 'HID','JAL' => 'JAL','MEX' => 'MEX','MIC' => 'MIC','MOR' => 'MOR','NAY' => 'NAY','NLE' => 'NLE','OAX' => 'OAX','PUE' => 'PUE','QUE' => 'QUE','ROO' => 'ROO','SLP' => 'SLP','SIN' => 'SIN','SON' => 'SON','TAB' => 'TAB','TAM' => 'TAM','TLA' => 'TLA','VER' => 'VER','YUC' => 'YUC','ZAC' => 'ZAC');
    $tipo_telefono = array('' => '-1','0' => '0','1' => '1','2' => '2','3' => '3','4' => '4','5' => '5');
    $estados = array('AGU' => 'AGU','BCN' => 'BCN','BCS' => 'BCS','CAM' => 'CAM','CHP' => 'CHP','CHH' => 'CHH','COA' => 'COA','COL' => 'COL','DIF' => 'DIF','CMX' => 'DIF','DUR' => 'DUR','GUA' => 'GUA','GRO' => 'GRO','HID' => 'HID','JAL' => 'JAL','MEX' => 'MEX','MIC' => 'MIC','MOR' => 'MOR','NAY' => 'NAY','NLE' => 'NLE','OAX' => 'OAX','PUE' => 'PUE','QUE' => 'QUE','ROO' => 'ROO','SLP' => 'SLP','SIN' => 'SIN','SON' => 'SON','TAB' => 'TAB','TAM' => 'TAM','TLA' => 'TLA','VER' => 'VER','YUC' => 'YUC','ZAC' => 'ZAC');
    $tipo_telefono = array('' => '-1','0' => '0','1' => '1','2' => '2','3' => '3','4' => '4','5' => '5');

    $GLOBALS['log']->fatal('Entra a JOB envio de ctas al middleware');
    $sucursales = $app_list_strings['sucursal_c_list'];
    foreach ($sucursales as $suc => $val) {
        if($suc != '0000'){
            $ctas1 = generaCtasxSuc($suc);
            if(count($ctas1)>0){
                $data1 = json_encode($ctas1);
                $respMW = llamaMiddleware($suc, $data1);
                marcaCtasProcesadas($ctas1);
                $vresp = json_decode($respMW);
                // $GLOBALS['log']->fatal($respMW);
                $var1 = $vresp->code;
                if($var1 == '200'){
                    marcaCtasEsperando_IDpos($ctas1);
                }
            }
        }
    }
    return true;
}

function generaCtasxSuc($sucursal){
    global $current_user,$estados, $tipo_telefono;
    $Ctas = new SugarQuery();
    $Ctas->select();
    $beanCuentas = BeanFactory::newBean('Accounts');
    $Ctas->from($beanCuentas, array('team_security' => false));
    $Ctas->where()
         ->equals('deleted',0)
         ->equals('estado_interfaz_pos_c','nuevo')
         ->equals('id_sucursal_c',$sucursal);
    $resCuentas = $Ctas->execute();
    $totCtas = array();

    foreach ($resCuentas as $r) {
        $sea = new SugarEmailAddress;
        $Acc = new Account();
        $Acc->retrieve($r['id']);
        $mail = $sea->getPrimaryAddress($Acc);
        $Con = new Contact();
        $Con->retrieve($r['contact_id_c']);
        $cta['id_sugar'] = $r['id'];
        $cta['ID_CT'] = $r['id_pos_c'];
        $cta['NM_CT'] = $r['name'];
        $cta['ID_LN_ANTC'] = $r['id_linea_anticipo_c'];
        $cta['ID_AR_CRD'] = $r['id_ar_credit_c'];
        $cta['EM_ADS'] = $mail;
        $cta['A1_CNCT'] = $r['primary_address_street_c'] . ", " .$r['primary_address_numero_c'];
        $cta['A2_CNCT'] = $r['primary_address_colonia_c'];
        $cta['CI_CNCT'] = $r['primary_address_city_c'];
        $cta['PC_CNCT'] = $r['primary_address_postalcode_c'];
        $cta['ST_CNCT'] = $estados[$r['primary_address_state_c']]; // Array $estados
        $cta['CO_CNCT'] = $r['primary_address_country_c'];
        $cta['TL_CNCT'] = $r['phone_office'];
        $cta['TY_PHN'] = $tipo_telefono[$r['tipo_telefono_principal_c']]; // Array $tipo_telefono
        $cta['ID_TAX'] = $r['rfc_c'];
        $cta['LU_CNCT_SLN'] = $Con->salutation;
        $nombre_rsocial = [];
        $nombre_rsocial = separa_razon_social($r['name']);
        $cta['FN_CNCT'] = $nombre_rsocial[0];
        $cta['LN_CNCT'] = $nombre_rsocial[1];
        // $cta['FN_CNCT'] = $Con->first_name;
        // $cta['LN_CNCT'] = $Con->last_name;
        $cta['DC_CNCT'] = $Con->birthdate;
        $cta['GNDR_CNCT'] = $Con->genre_c;
        $cta['ID_PRCGP'] = $Acc->tipo_cliente_c ? $Acc->tipo_cliente_c : "1";

        if($r['id']){
            array_push($totCtas,$cta);
        }
    }
    return $totCtas;
}

function separa_razon_social($rsocial){
    $result = [];
    $long = strlen($rsocial);
    if($long > 30){
        $result[0] = substr($rsocial, 0, 29);
        $result[1] = substr($rsocial, 29, $long);
    }
    else{
        $result[0] = $rsocial;
        $result[1] = ".";
    }
    return $result;
}

function llamaMiddleware($suc, $data){
    global $current_user;
    $dom = '';
    $configurator = new Configurator();
    $configurator->loadConfig();
    $urlApi = "http://100.24.211.220/Middleware/json2csv.php";
    $ch = curl_init($urlApi);
    curl_setopt($ch, CURLOPT_POST, true); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, "jsonData=".$data.'#cliente_credito_ar_'.$suc.'_'.date('dmY_Gis').'.csv');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    return $result;
}


function marcaCtasProcesadas($aprocesar){
    global $current_user, $timedate;
    if($aprocesar){
        foreach($aprocesar as $cta){
            $Acc = BeanFactory::getBean('Accounts', $cta['id_sugar']);
            if($Acc->id){
              $Acc->estado_interfaz_pos_c = 'procesando';
              $Acc->save(false);
            }
        }
    }
}

function validaRespuestaMiddleware($jsonResp){
    global $current_user, $timedate;
    $rmw = json_decode($jsonResp,true);
    $resp = $rmw[0]['code'];
    if($resp == '200'){
        return true;
    }
    return false;
}

function marcaCtasEsperando_IDpos($aprocesar){
    global $current_user, $timedate;
    if($aprocesar){
        foreach($aprocesar as $cta){
            $Acc = BeanFactory::getBean('Accounts', $cta['id_sugar']);
            if($Acc->id){
              $Acc->estado_interfaz_pos_c = 'esperandoidpos';
              $Acc->save();
            }
        }
    }
}
