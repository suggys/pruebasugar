<?php
$job_strings[] = 'job_actualiza_estado_autorizacion_especial';
array_push($job_strings, 'job_actualiza_estado_autorizacion_especial');

function job_actualiza_estado_autorizacion_especial() {
  $whereRaw = "(lae.fecha_fin_c < NOW() AND lae.motivo_solicitud_c IN ('credito_disponible_temporal','otro')) OR (lae.fecha_cumplimiento_c < NOW() AND lae.motivo_solicitud_c IN ('promesa_de_pago'))";
  $sq = new SugarQuery();
	$sq->select(array('id'));
  $sq->from(BeanFactory::newBean('lowae_autorizacion_especial'),array('alias'=>'lae','team_security'=>false));
  $sq->where()->equals('lae_c.estado_autorizacion_c','autorizada');
	$sq->whereRaw($whereRaw);
	$result = $sq->execute();
	foreach ($result as $a) {
		$beanAutEsp = BeanFactory::getBean('lowae_autorizacion_especial',$a['id']);
    $beanAutEsp->estado_c = 'no_aplicado';
    $beanAutEsp->estado_autorizacion_c = 'finalizada';
		$beanAutEsp->save(false);
	}
  return true;
}
