<?php
$job_strings[] = 'JOB_clasifica_facturas_vigencia';
array_push($job_strings, 'JOB_clasifica_facturas_vigencia');
// Reporte Promesas de pago clasifica
function JOB_clasifica_facturas_vigencia(){

	global $timedate, $db;
	// $GLOBALS["log"]->fatal("antes de UPDATE");
	$query1 ="
	UPDATE lf_facturas_cstm fc
	LEFT JOIN lf_facturas f ON f.id = fc.id_c
	LEFT JOIN (
	 SELECT f0.id, f0.dt_crt, accounts_cstm.plazo_pago_autorizado_c, accounts_cstm.dias_tolerancia_c,
	 accounts_cstm.dias_tolerancia_c + accounts_cstm.plazo_pago_autorizado_c dias,
	 DATEDIFF(NOW(), f0.dt_crt) diffnow,
	 DATEDIFF(NOW(), f0.dt_crt) - (accounts_cstm.dias_tolerancia_c + accounts_cstm.plazo_pago_autorizado_c) dias_vencidos
	 FROM lf_facturas f0
	 INNER JOIN accounts_lf_facturas_1_c ON f0.id = accounts_lf_facturas_1_c.accounts_lf_facturas_1lf_facturas_idb and accounts_lf_facturas_1_c.deleted = 0
	 INNER JOIN accounts_cstm ON accounts_cstm.id_c = accounts_lf_facturas_1_c.accounts_lf_facturas_1accounts_ida and accounts_lf_facturas_1_c.deleted = 0
	 INNER JOIN accounts a ON accounts_cstm.id_c = a.id
	 LEFT JOIN lf_facturas_lf_facturas_c ff ON ff.lf_facturas_lf_facturaslf_facturas_ida = f0.id
	 WHERE f0.deleted = 0
	 AND f0.refactura = 0
	 AND ff.lf_facturas_lf_facturaslf_facturas_ida IS NULL

	 UNION

	 SELECT afc.id_c id, afc.fecha_emision_padre_c dt_crt, accounts_cstm.plazo_pago_autorizado_c, accounts_cstm.dias_tolerancia_c,
	 accounts_cstm.dias_tolerancia_c + accounts_cstm.plazo_pago_autorizado_c dias,
	 DATEDIFF(NOW(), IFNULL(afc.fecha_emision_padre_c,0)) diffnow,
	 DATEDIFF(NOW(), afc.fecha_emision_padre_c) - (accounts_cstm.dias_tolerancia_c + accounts_cstm.plazo_pago_autorizado_c) dias_vencidos
   FROM lf_facturas_cstm afc
	 INNER JOIN lf_facturas f0 ON f0.id = afc.id_c
	 INNER JOIN accounts_lf_facturas_1_c ON f0.id = accounts_lf_facturas_1_c.accounts_lf_facturas_1lf_facturas_idb AND accounts_lf_facturas_1_c.deleted = 0
	 INNER JOIN accounts_cstm ON accounts_cstm.id_c = accounts_lf_facturas_1_c.accounts_lf_facturas_1accounts_ida
	 INNER JOIN accounts a ON accounts_cstm.id_c = a.id
	 INNER JOIN lf_facturas_lf_facturas_c ff ON ff.lf_facturas_lf_facturaslf_facturas_idb = afc.id_c
	 WHERE f0.deleted = 0
	 AND f0.deleted = 0
	) faux ON faux.id = f.id
	SET fc.vigencia_c =
		CASE
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) <= 0 THEN 'vigente'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) BETWEEN 1 AND 15 THEN '1a15'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) BETWEEN 16 AND 30 THEN '16a30'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) BETWEEN 31 AND 60 THEN '31a60'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) BETWEEN 61 AND 90 THEN '61a90'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) BETWEEN 91 AND 120 THEN '91a120'
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) > 120 THEN 'mas120'
		END,
	fc.dias_vencidos_sin_tol_c =
		CASE
		WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) > 0 THEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY))
		WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) <= 0 THEN 0
		END,
	f.estado_tiempo =
		CASE
			WHEN f.estado_tiempo = 'devuelta_parcial' THEN 'devuelta_parcial'
			ELSE
				CASE
					WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) > 0 THEN 'vencida'
					WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY)) <= 0 THEN 'vigente'
				END
		END,
	f.dias_vencimiento =
		CASE
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0) + IFNULL(faux.dias_tolerancia_c, 0) ) DAY)) > 0 THEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0) + IFNULL(faux.dias_tolerancia_c, 0) ) DAY))
			WHEN DATEDIFF(curdate(), DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0) + IFNULL(faux.dias_tolerancia_c, 0) ) DAY)) <= 0 THEN 0
		END,
	f.fecha_vencimiento = DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0)) DAY),
	fc.fecha_vencimiento_flexible_c = DATE_ADD(faux.dt_crt, INTERVAL (IFNULL(faux.plazo_pago_autorizado_c, 0) + IFNULL(faux.dias_tolerancia_c, 0) ) DAY)
	WHERE f.tipo_documento = 'factura'
	AND fc.linea_anticipo_c = 0
	AND f.deleted = 0
	AND (f.estado_tiempo IN ('vigente', 'vencida', 'devuelta_parcial' , '') OR f.estado_tiempo IS NULL)
	";
	$result1 = $db->query($query1);

  $query00 = "UPDATE lf_facturas_cstm fc
	    LEFT JOIN lf_facturas f ON f.id = fc.id_c
	    LEFT JOIN lowes_pagos_lf_facturas_c p ON p.lowes_pagos_lf_facturaslf_facturas_idb = f.id
	    LEFT JOIN lowes_pagos lp ON lp.id = p.lowes_pagos_lf_facturaslowes_pagos_ida
	    LEFT JOIN lowes_pagos_accounts_c pa ON pa.lowes_pagos_accountslowes_pagos_idb = lp.id
	    LEFT JOIN accounts_cstm ac ON pa.lowes_pagos_accountsaccounts_ida = ac.id_c
	    SET fc.dias_vencido_pagada_c = DATEDIFF(lp.fecha_transaccion,f.dt_crt)-ac.plazo_pago_autorizado_c-ac.dias_tolerancia_c
	    WHERE f.tipo_documento = 'factura'
	    AND f.deleted = 0
			AND fc.linea_anticipo_c = 0
	    AND dt_crt IS NOT NULL
	    AND p.lowes_pagos_lf_facturaslowes_pagos_ida IS NOT NULL;  ";
    $result00 = $db->query($query00);

	$query01 = "UPDATE lf_facturas_cstm fc
				LEFT JOIN lf_facturas f ON f.id = fc.id_c
				SET fc.dias_trascurridos_c = DATEDIFF(NOW(),f.dt_crt)
				WHERE f.saldo > 0
				AND fc.linea_anticipo_c = 0
				AND f.tipo_documento = 'factura';  ";
    $result01 = $db->query($query01);
    return true;
}
