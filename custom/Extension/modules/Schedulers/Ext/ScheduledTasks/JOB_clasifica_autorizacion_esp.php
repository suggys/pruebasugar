<?php
$job_strings[] = 'JOB_clasifica_autorizacion_esp';
array_push($job_strings, 'JOB_clasifica_autorizacion_esp');
// Reporte Promesas de pago clasifica
function JOB_clasifica_autorizacion_esp(){

	global $timedate, $current_user, $db;

    $query1 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = 'vigente'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) = 0
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result1 = $db->query($query1);

    $query2 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = '1a30'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) BETWEEN 1 AND 30
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result2 = $db->query($query2);

    $query3 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = '31a60'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) BETWEEN 31 AND 60
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result3 = $db->query($query3);

    $query4 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = '61a90'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) BETWEEN 61 AND 90
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result4 = $db->query($query4);

    $query5 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = '91a120'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) BETWEEN 91 AND 120
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result5 = $db->query($query5);

    $query6 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = 'mas120'
            WHERE DATEDIFF(NOW(),a.fecha_cumplimiento_c) > 120
            AND a.monto_abonado_c  < a.monto_promesa_c
            AND a.deleted = 0; ";
    $result6 = $db->query($query6);

    $query7 = " UPDATE lowae_autorizacion_especial_cstm ac
            LEFT JOIN lowae_autorizacion_especial a ON a.id = ac.id_c
            SET ac.vigencia_c = ''
            WHERE a.monto_promesa_c = a.monto_abonado_c
            AND a.deleted = 0;  ";
    $result7 = $db->query($query7);


    //$GLOBALS['log']->fatal("Se ejecutó calculo de vigencia de autorización especial.");
    return true;
}