<?php

$job_strings[] = 'envioFacturasMiddlewareJOB';

array_push($job_strings, 'envioFacturasMiddlewareJOB');
function envioFacturasMiddlewareJOB(){
    global $current_user, $timedate, $app_list_strings;
    global $estados, $tipo_telefono, $paises, $url;
    global $urlws;

    $urlws = 'http://json.lowes.com.mx/convert2json/convert2json/';
   

    $GLOBALS['log']->fatal('Entra a JOB envio de Fact al middleware');
   
    $Fact1 = generaFacturas();
    if(count($Fact1)>0){
        $data1 = json_encode($Fact1);
        $respMW = llamaMiddlewareFac($data1);
        //$respMW = llamaMiddleware($suc, $data1);
        return true;
    }
    return true;
}

function generaFacturas(){
    global $current_user,$estados, $tipo_telefono;

    // creacion de la fecha 
    $anio=date("Y");
    $mes=date("m");
    $dia=date("d")-1;
         
    if (strlen($dia) === 1) {
        $dia = '0'.$dia;
    }

    $fecha=$anio.'-'.$mes.'-'.$dia;
    //$GLOBALS['log']->fatal("Fecha: ".$fecha);
    
    $Fact = new SugarQuery();
    $Fact->select();
    $beanFact = BeanFactory::newBean('LF_Facturas');
    $Fact->from($beanFact, array('team_security' => false));
    $Fact->where()->contains('dt_crt',$fecha);
    $resFacturas=$Fact->execute();
    //$GLOBALS['log']->fatal($resFacturas);
    $totFact = array();

    foreach ($resFacturas as $r) {
        $fac['id_sugar'] = $r['id'];
        $fac['NT_CDT'] = $r['no_ticket'];
        $fac['UUID_FAC'] = $r['uuid'];
        $fac['NAME_FAC'] = $r['name'];
        $fac['SERIE_FAC'] = $r['serie'];
        $fac['IMP_FAC'] = $r['importe'];
        $fac['ARC_FAC'] = $r['importadode_c'];
        $fac['FECHA_FAC'] = $r['dt_crt'];
        if($r['id']){
            array_push($totFact,$fac);
        }
    }
    return $totFact;
}


function llamaMiddlewareFac($data){
    global $current_user, $urlws;
    $dom = '';
    $configurator = new Configurator();
    $configurator->loadConfig();
    $url = $configurator->config['host_name'];
    $urlApi ='http://100.24.211.220/Middleware/json2csv.php';
    $ch = curl_init($urlApi);
    curl_setopt($ch, CURLOPT_POST, true); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, "jsonData=".$data.'#fact_sugar_'.date('Ymd').'.csv');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    return $result;
}


