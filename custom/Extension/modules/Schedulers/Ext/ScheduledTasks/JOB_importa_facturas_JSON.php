<?php
require_once 'custom/include/HealthCheckImport.php';
$job_strings[] = 'importaFacturasJSON';
array_push($job_strings, 'importaFacturasJSON');

function importaFacturasJSON()
{
	$saludDeImportaciones = new HealthCheckImport();
	$result = $saludDeImportaciones->obtenerRegistrosJSON("facturas");

	if (!empty($result))
	{
		if ($result['code'] == "200")
		{
			$registrados = 0;
			$filename = explode('.', $result['filename']);
			$saludDeFacturas = $saludDeImportaciones->facturas($result);

			if (empty($saludDeFacturas['facturasConError']))
			{
				$registrados = $saludDeImportaciones->guardarBeans($saludDeFacturas['facturasAGuardar'],$result['filename']);
				if ($registrados == count($saludDeFacturas['facturasAGuardar']))
				{
					$esProcesado = $saludDeImportaciones->esProcesadoConExito($filename[0]);
				}
			} else {
				if (in_array(5,$saludDeFacturas['typeErrors']) || in_array(6,$saludDeFacturas['typeErrors'])){
					$saludDeImportaciones->crearNotaDeError("facturas",$saludDeFacturas['typeErrors']);
				} else {
					if(!empty($saludDeFacturas['facturasAGuardar'])){
						$registrados = $saludDeImportaciones->guardarBeans($saludDeFacturas['facturasAGuardar'],$result['filename']);
					}
					$saludDeImportaciones->crearNotaDeError("facturas",$saludDeFacturas['typeErrors'],$saludDeFacturas['facturasConError'],$filename[0],$registrados);
				}
				$esProcesado = $saludDeImportaciones->esProcesadoConErrores($filename[0]);
			}
			return $saludDeImportaciones->logProcesados("Facturas",$esProcesado);
		} else {
			$GLOBALS['log']->fatal($result['message']);
			return false;
		}
	} else {
		$GLOBALS['log']->fatal('Error con el middleware');
		return false;
	}
}
