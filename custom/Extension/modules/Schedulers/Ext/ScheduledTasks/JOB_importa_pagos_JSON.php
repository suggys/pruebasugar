<?php
require_once 'custom/include/HealthCheckImport.php';
$job_strings[] = 'importaPagosJSON';
array_push($job_strings, 'importaPagosJSON');

function importaPagosJSON()
{
	$saludDeImportaciones = new HealthCheckImport();
	$result = $saludDeImportaciones->obtenerRegistrosJSON("pagos");

	if (!empty($result))
	{
		if ($result['code'] == "200")
		{
			$registrados = 0;
			$filename = explode('.', $result['filename']);
			$saludDePagos = $saludDeImportaciones->pagos($result);

			if (empty($saludDePagos['pagosConError']))
			{
				$registrados = $saludDeImportaciones->guardarBeans($saludDePagos['pagosAGuardar'],$result['filename']);
				if ($registrados == count($saludDePagos['pagosAGuardar']))
				{
					$esProcesado = $saludDeImportaciones->esProcesadoConExito($filename[0]);
				}
			} else {
				if (in_array(5,$saludDePagos['typeErrors']) || in_array(6,$saludDePagos['typeErrors'])){
					$saludDeImportaciones->crearNotaDeError("pagos",$saludDePagos['typeErrors']);
				} else {
					if(!empty($saludDePagos['pagosAGuardar'])){
						$registrados = $saludDeImportaciones->guardarBeans($saludDePagos['pagosAGuardar'],$result['filename']);
					}
					$saludDeImportaciones->crearNotaDeError("pagos",$saludDePagos['typeErrors'],$saludDePagos['pagosConError'],$filename[0],$registrados);
				}
				$esProcesado = $saludDeImportaciones->esProcesadoConErrores($filename[0]);
			}
			return $saludDeImportaciones->logProcesados("Pagos",$esProcesado);
		} else {
			$GLOBALS['log']->fatal($result['message']);
			return false;
		}
	} else {
		$GLOBALS['log']->fatal('Error con el middleware');
		return false;
	}
}
