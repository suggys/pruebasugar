<?php
require_once 'custom/include/HealthCheckImport.php';
$job_strings[] = 'importaNotasDeCreditoJSON';
array_push($job_strings, 'importaNotasDeCreditoJSON');

function importaNotasDeCreditoJSON()
{
	$saludDeImportaciones = new HealthCheckImport();
	$result = $saludDeImportaciones->obtenerRegistrosJSON("n_credito");

	if (!empty($result))
	{
		if ($result['code'] == "200")
		{
			$registrados = 0;
			$filename = explode('.', $result['filename']);
			$saludDeNotasCredito = $saludDeImportaciones->notasDeCredito($result);

			if (empty($saludDeNotasCredito['n_CreditoConError']))
			{
				$registrados = $saludDeImportaciones->guardarBeans($saludDeNotasCredito['n_CreditoAGuardar'],$result['filename']);
				if ($registrados == count($saludDeNotasCredito['n_CreditoAGuardar']))
				{
					$esProcesado = $saludDeImportaciones->esProcesadoConExito($filename[0]);
				}
			} else {
				if (in_array(5,$saludDeNotasCredito['typeErrors']) || in_array(6,$saludDeNotasCredito['typeErrors'])){
					$saludDeImportaciones->crearNotaDeError("n_credito",$saludDeNotasCredito['typeErrors']);
				} else {
					if(!empty($saludDeNotasCredito['n_CreditoAGuardar'])){
						$registrados = $saludDeImportaciones->guardarBeans($saludDeNotasCredito['n_CreditoAGuardar'],$result['filename']);
					}
					$saludDeImportaciones->crearNotaDeError("n_credito",$saludDeNotasCredito['typeErrors'],$saludDeNotasCredito['n_CreditoConError'],$filename[0],$registrados);
				}
				$esProcesado = $saludDeImportaciones->esProcesadoConErrores($filename[0]);
			}
			return $saludDeImportaciones->logProcesados("NotasDeCredito",$esProcesado);
		} else {
			$GLOBALS['log']->fatal($result['message']);
			return false;
		}
	} else {
		$GLOBALS['log']->fatal('Error con el middleware');
		return false;
	}
}
