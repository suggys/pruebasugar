<?php
 // created: 2017-01-18 23:14:35
$dictionary['lowes_Pagos']['fields']['name']['len']='255';
$dictionary['lowes_Pagos']['fields']['name']['required']=false;
$dictionary['lowes_Pagos']['fields']['name']['audited']=true;
$dictionary['lowes_Pagos']['fields']['name']['massupdate']=false;
$dictionary['lowes_Pagos']['fields']['name']['unified_search']=false;
$dictionary['lowes_Pagos']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['lowes_Pagos']['fields']['name']['calculated']=false;
$dictionary['lowes_Pagos']['fields']['name']['importable']='false';

 ?>