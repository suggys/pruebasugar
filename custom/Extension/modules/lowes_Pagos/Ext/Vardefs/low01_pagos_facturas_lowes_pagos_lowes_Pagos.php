<?php
// created: 2017-04-03 22:41:21
$dictionary["lowes_Pagos"]["fields"]["low01_pagos_facturas_lowes_pagos"] = array (
  'name' => 'low01_pagos_facturas_lowes_pagos',
  'type' => 'link',
  'relationship' => 'low01_pagos_facturas_lowes_pagos',
  'source' => 'non-db',
  'module' => 'Low01_Pagos_Facturas',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'low01_pagos_facturas_lowes_pagoslowes_pagos_ida',
  'link-type' => 'many',
  'side' => 'left',
);
