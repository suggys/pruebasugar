<?php
// created: 2017-11-06 23:18:19
$dictionary["lowes_Pagos"]["fields"]["lowae_autorizacion_especial_lowes_pagos_2"] = array (
  'name' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'type' => 'link',
  'relationship' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'source' => 'non-db',
  'module' => 'lowae_autorizacion_especial',
  'bean_name' => 'lowae_autorizacion_especial',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'id_name' => 'lowae_auto4a14special_ida',
);
