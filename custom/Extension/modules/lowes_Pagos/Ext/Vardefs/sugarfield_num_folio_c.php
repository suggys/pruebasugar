<?php
 // created: 2017-01-18 23:14:55
$dictionary['lowes_Pagos']['fields']['num_folio_c']['help']='Consecutivo';
$dictionary['lowes_Pagos']['fields']['num_folio_c']['auto_increment']=true;
$dictionary['lowes_Pagos']['fields']['num_folio_c']['duplicate_on_record_copy']='no';
$dictionary['lowes_Pagos']['fields']['num_folio_c']['disable_num_format']='1';
$dictionary['lowes_Pagos']['fields']['num_folio_c']['no_default']=true;
$dictionary['lowes_Pagos']['fields']['num_folio_c']['len']='11';
$dictionary['lowes_Pagos']['fields']['num_folio_c']['autoinc_next']='57';
$dictionary['lowes_Pagos']['fields']['num_folio_c']['audited']=true;

 ?>