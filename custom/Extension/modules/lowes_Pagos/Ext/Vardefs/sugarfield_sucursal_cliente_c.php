<?php
 // created: 2017-09-14 14:48:43
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['duplicate_merge_dom_value']=0;
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['labelValue']='Sucursal de Cliente';
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['calculated']='1';
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['formula']='ifElse(not(equal($sucursal_cliente_c,"")),related($lowes_pagos_accounts,"id_sucursal_c"),"")';
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['enforced']='1';
$dictionary['lowes_Pagos']['fields']['sucursal_cliente_c']['dependency']='';

 ?>
