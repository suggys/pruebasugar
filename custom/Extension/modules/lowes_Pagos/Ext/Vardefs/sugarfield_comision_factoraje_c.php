<?php
 // created: 2017-10-18 05:35:29
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['labelValue']='Comisión de Factoraje';
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['enforced']='';
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['default']=null;
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['required']=1;
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['dependency']='equal(related($lowes_pagos_accounts,"con_factoraje_c"),true)';
$dictionary['lowes_Pagos']['fields']['comision_factoraje_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>
