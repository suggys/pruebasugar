<?php
// created: 2016-10-22 15:12:51
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_lf_facturas_1"] = array (
  'name' => 'lowes_pagos_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'lowes_pagos_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'lowes_pagos_lf_facturas_1lf_facturas_idb',
);
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_lf_facturas_1_name"] = array (
  'name' => 'lowes_pagos_lf_facturas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'save' => true,
  'id_name' => 'lowes_pagos_lf_facturas_1lf_facturas_idb',
  'link' => 'lowes_pagos_lf_facturas_1',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'name',
);
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_lf_facturas_1lf_facturas_idb"] = array (
  'name' => 'lowes_pagos_lf_facturas_1lf_facturas_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID',
  'id_name' => 'lowes_pagos_lf_facturas_1lf_facturas_idb',
  'link' => 'lowes_pagos_lf_facturas_1',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
