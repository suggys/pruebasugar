<?php
    $hook_version = 1;
    $hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
    $hook_array['before_save'][] = array(
        1,
        'FR-27.5. Liberar Crédito Disponible',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'antesDeliberarCreditoDisponible'
    );
    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        1,
        'FR-27.5. Liberar Crédito Disponible',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'liberaCreditoDisponible'
    );
?>