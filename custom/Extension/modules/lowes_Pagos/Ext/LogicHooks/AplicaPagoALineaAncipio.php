<?php
	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
    $hook_array['before_save'][] = array(
        1,
        'Aplica pago a linea de anticipo',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'aplicaPagoALineaAncipio',
    );

    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        1,
        'Calcula saldo disponible cliente',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'calculaSaldoDisponibleCuenta',
    );

    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        2,
        'Calcula saldo disponible cliente',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'calculaSaldoFavorCtaPorPagoAticipo',
    );
?>
