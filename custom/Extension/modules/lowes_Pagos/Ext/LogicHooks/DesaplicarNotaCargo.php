<?php
	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
    $hook_array['after_relationship_delete'][] = array(
        1,
        'Desaplicar nota de cargo',
        'custom/modules/lowes_Pagos/PagosLogicHooksManager.php',
        'PagosLogicHooksManager',
        'desaplicarNotaCargo'
    );
?>
