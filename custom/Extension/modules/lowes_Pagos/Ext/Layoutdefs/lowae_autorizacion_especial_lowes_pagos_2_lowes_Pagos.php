<?php
 // created: 2017-11-06 23:18:19
$layout_defs["lowes_Pagos"]["subpanel_setup"]['lowae_autorizacion_especial_lowes_pagos_2'] = array (
  'order' => 100,
  'module' => 'lowae_autorizacion_especial',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'get_subpanel_data' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
