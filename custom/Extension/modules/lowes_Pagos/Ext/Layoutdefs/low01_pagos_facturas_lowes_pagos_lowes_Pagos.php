<?php
 // created: 2017-04-03 22:41:21
$layout_defs["lowes_Pagos"]["subpanel_setup"]['low01_pagos_facturas_lowes_pagos'] = array (
  'order' => 100,
  'module' => 'Low01_Pagos_Facturas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOW01_PAGOS_FACTURAS_TITLE',
  'get_subpanel_data' => 'low01_pagos_facturas_lowes_pagos',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
