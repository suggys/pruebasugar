<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    1,
    'FR-23.2. Calcular saldos de la factura por aplicación de nota de crédito',
    'custom/modules/LF_Facturas/CalSaldoFactAplicaNC.php',
    'CalSaldoFactAplicaNC',
    'CalSaldoFactAplicaNC'
);
?>