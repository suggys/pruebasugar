<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    2,
    'FR-28.1. Aplicar pagos a facturas',
    'custom/modules/LF_Facturas/LHCalculaSaldoPendientePago.php',
    'LHCalculaSaldoPendientePago',
    'fnCalculaSaldoPendienteAplicar'
);

$hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
$hook_array['after_relationship_delete'][] = array(
    2,
    'FR-28.1. Aplicar pagos a facturas',
    'custom/modules/LF_Facturas/LHCalculaSaldoPendientePago.php',
    'LHCalculaSaldoPendientePago',
    'fnCalculaSaldoPendienteAplicarDesvincular'
);
?>