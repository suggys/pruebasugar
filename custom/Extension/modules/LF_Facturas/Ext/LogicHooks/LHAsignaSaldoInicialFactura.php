<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : [];
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : [];
$hook_array['before_save'][] = array(
    1,
    'Copia importe a saldo cuando se crea',
    'custom/modules/LF_Facturas/LHAsignaSaldoInicialFactura.php',
    'LHAsignaSaldoInicialFactura',
    'fnCopiaSaldoFactura'
);
?>