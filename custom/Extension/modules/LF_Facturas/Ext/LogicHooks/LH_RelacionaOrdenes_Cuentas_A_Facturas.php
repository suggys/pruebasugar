<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : [];
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : [];
$hook_array['before_save'][] = array(
    1,
    'Relaciona Ordenes y cuentas a Factura a partir del num de ticket',
    'custom/modules/LF_Facturas/Rel_Orden_Cuenta_NumTicket.php',
    'Rel_Orden_Cuenta_NumTicket',
    'previousRow'
);
$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : [];
$hook_array['after_save'][] = array(
    1,
    'Relaciona Ordenes y cuentas a Factura a partir del num de ticket',
    'custom/modules/LF_Facturas/Rel_Orden_Cuenta_NumTicket.php',
    'Rel_Orden_Cuenta_NumTicket',
    'relacionaDatos'
);
?>