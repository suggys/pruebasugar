<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
    1,
    'Calcula saldo deudor',
    'custom/modules/LF_Facturas/FacturasLogicHooksManager.php',
    'FacturasLogicHooksManager',
    'afterSave'
);

$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
    2,
    'Guarda fetched_row',
    'custom/modules/LF_Facturas/FacturasLogicHooksManager.php',
    'FacturasLogicHooksManager',
    'beforeSave'
);

$hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
$hook_array['after_relationship_delete'][] = array(
    3,
    'Relaciona Facturas',
    'custom/modules/LF_Facturas/Relacionafacturas.php',
    'Relacionafacturas',
    'fnQuitaRefactura'
);

$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    3,
    'Relaciona Facturas',
    'custom/modules/LF_Facturas/Relacionafacturas.php',
    'Relacionafacturas',
    'fnAgregaRefactura'
);
?>
