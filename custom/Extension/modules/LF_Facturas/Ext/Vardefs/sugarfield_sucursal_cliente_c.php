<?php
 // created: 2017-08-24 22:53:10
$dictionary['LF_Facturas']['fields']['sucursal_cliente_c']['labelValue']='Sucursal de Cliente';
$dictionary['LF_Facturas']['fields']['sucursal_cliente_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LF_Facturas']['fields']['sucursal_cliente_c']['formula']='related($accounts_lf_facturas_1,"id_sucursal_c")';
$dictionary['LF_Facturas']['fields']['sucursal_cliente_c']['enforced']='false';
$dictionary['LF_Facturas']['fields']['sucursal_cliente_c']['dependency']='';

 ?>