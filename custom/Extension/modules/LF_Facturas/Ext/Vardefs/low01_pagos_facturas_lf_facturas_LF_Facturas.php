<?php
// created: 2017-04-03 22:41:21
$dictionary["LF_Facturas"]["fields"]["low01_pagos_facturas_lf_facturas"] = array (
  'name' => 'low01_pagos_facturas_lf_facturas',
  'type' => 'link',
  'relationship' => 'low01_pagos_facturas_lf_facturas',
  'source' => 'non-db',
  'module' => 'Low01_Pagos_Facturas',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
  'link-type' => 'many',
  'side' => 'left',
);
