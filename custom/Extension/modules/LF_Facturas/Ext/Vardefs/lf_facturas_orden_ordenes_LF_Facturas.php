<?php
// created: 2016-10-19 00:31:50
$dictionary["LF_Facturas"]["fields"]["lf_facturas_orden_ordenes"] = array (
  'name' => 'lf_facturas_orden_ordenes',
  'type' => 'link',
  'relationship' => 'lf_facturas_orden_ordenes',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'side' => 'right',
  'vname' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
  'link-type' => 'one',
);
$dictionary["LF_Facturas"]["fields"]["lf_facturas_orden_ordenes_name"] = array (
  'name' => 'lf_facturas_orden_ordenes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'save' => true,
  'id_name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
  'link' => 'lf_facturas_orden_ordenes',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["lf_facturas_orden_ordenesorden_ordenes_ida"] = array (
  'name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_LF_FACTURAS_TITLE_ID',
  'id_name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
  'link' => 'lf_facturas_orden_ordenes',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
