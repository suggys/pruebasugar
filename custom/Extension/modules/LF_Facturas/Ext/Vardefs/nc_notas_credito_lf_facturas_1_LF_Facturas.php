<?php
// created: 2016-10-20 16:38:00
$dictionary["LF_Facturas"]["fields"]["nc_notas_credito_lf_facturas_1"] = array (
  'name' => 'nc_notas_credito_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'NC_Notas_credito',
  'bean_name' => 'NC_Notas_credito',
  'side' => 'right',
  'vname' => 'LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
  'link-type' => 'one',
);
$dictionary["LF_Facturas"]["fields"]["nc_notas_credito_lf_facturas_1_name"] = array (
  'name' => 'nc_notas_credito_lf_facturas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE',
  'save' => true,
  'id_name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
  'link' => 'nc_notas_credito_lf_facturas_1',
  'table' => 'nc_notas_credito',
  'module' => 'NC_Notas_credito',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["nc_notas_credito_lf_facturas_1nc_notas_credito_ida"] = array (
  'name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID',
  'id_name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
  'link' => 'nc_notas_credito_lf_facturas_1',
  'table' => 'nc_notas_credito',
  'module' => 'NC_Notas_credito',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
