<?php
// created: 2016-10-19 00:31:50
$dictionary["LF_Facturas"]["fields"]["lf_facturas_lf_facturas"] = array (
  'name' => 'lf_facturas_lf_facturas',
  'type' => 'link',
  'relationship' => 'lf_facturas_lf_facturas',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_L_TITLE',
  'id_name' => 'lf_facturas_lf_facturaslf_facturas_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["LF_Facturas"]["fields"]["lf_facturas_lf_facturas_right"] = array (
  'name' => 'lf_facturas_lf_facturas_right',
  'type' => 'link',
  'relationship' => 'lf_facturas_lf_facturas',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'side' => 'right',
  'vname' => 'LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_R_TITLE',
  'id_name' => 'lf_facturas_lf_facturaslf_facturas_ida',
  'link-type' => 'one',
);
$dictionary["LF_Facturas"]["fields"]["lf_facturas_lf_facturas_name"] = array (
  'name' => 'lf_facturas_lf_facturas_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_L_TITLE',
  'save' => true,
  'id_name' => 'lf_facturas_lf_facturaslf_facturas_ida',
  'link' => 'lf_facturas_lf_facturas_right',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["lf_facturas_lf_facturaslf_facturas_ida"] = array (
  'name' => 'lf_facturas_lf_facturaslf_facturas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_R_TITLE_ID',
  'id_name' => 'lf_facturas_lf_facturaslf_facturas_ida',
  'link' => 'lf_facturas_lf_facturas_right',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
