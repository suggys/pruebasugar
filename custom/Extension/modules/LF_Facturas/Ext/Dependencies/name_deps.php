<?php
$dependencies['LF_Facturas']['name'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('tipo_documento'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(

      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'name',
              'label'  => 'name_label', //normally <field>_label
              'value' => 'equal($tipo_documento, "factura")', //Formula
          ),
      ),
    ),
);
