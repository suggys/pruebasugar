<?php
$dependencies['LF_Facturas']['lf_facturas_orden_ordenes_name'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'true',
    'triggerFields' => array('tipo_documento'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(

      array(
          'name' => 'SetVisibility', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'lf_facturas_orden_ordenes_name',
              'label'  => 'lf_facturas_orden_ordenes_name_label', //normally <field>_label
              'value' => 'equal($tipo_documento, "factura")', //Formula
          ),
      ),
    ),
);
