<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Saldos';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Información General';
$mod_strings['LBL_RECORDVIEW_PANEL5'] = 'Saldos Credito AR';
$mod_strings['LBL_RECORDVIEW_PANEL7'] = 'Documento Garantía';
$mod_strings['LBL_RECORDVIEW_PANEL8'] = 'Información Adicional';
$mod_strings['LBL_RECORDVIEW_PANEL9'] = 'Condiciones Créditicias';
$mod_strings['LBL_RECORDVIEW_PANEL10'] = 'Datos de Identifiación';
$mod_strings['LBL_RECORDVIEW_PANEL11'] = 'Datos del Sistema';
$mod_strings['LBL_RECORDVIEW_PANEL12'] = 'Datos de Identifiación';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Datos del Sistema';
$mod_strings['LBL_BILLING_ADDRESS'] = 'Dirección de facturación';
$mod_strings['LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Nota de cargo';
$mod_strings['LBL_LINEA_ANTICIPO'] = 'Linea Anticipo';
$mod_strings['LBL_FOLIO_C'] = 'Folio';
$mod_strings['LBL_ORDEN_COMPRA_C'] = 'Orde de compra';
$mod_strings['LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_L_TITLE'] = 'Orden Padre';
$mod_strings['LBL_SHOW_MORE'] = 'Información del sistema';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE'] = 'Cliente Crédito AR';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Nota de Crédito';
$mod_strings['LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Órden';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE'] = 'Cliente Crédito AR';
$mod_strings['LBL_LF_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_L_TITLE'] = 'Factura Padre';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE'] = 'Nota de Crédito';
