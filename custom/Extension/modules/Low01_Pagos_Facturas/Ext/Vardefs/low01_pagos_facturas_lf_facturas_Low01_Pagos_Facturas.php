<?php
// created: 2017-04-03 22:41:21
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lf_facturas"] = array (
  'name' => 'low01_pagos_facturas_lf_facturas',
  'type' => 'link',
  'relationship' => 'low01_pagos_facturas_lf_facturas',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'side' => 'right',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LOW01_PAGOS_FACTURAS_TITLE',
  'id_name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
  'link-type' => 'one',
);
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lf_facturas_name"] = array (
  'name' => 'low01_pagos_facturas_lf_facturas_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LF_FACTURAS_TITLE',
  'save' => true,
  'id_name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
  'link' => 'low01_pagos_facturas_lf_facturas',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'name',
);
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lf_facturaslf_facturas_ida"] = array (
  'name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LF_FACTURAS_FROM_LOW01_PAGOS_FACTURAS_TITLE_ID',
  'id_name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
  'link' => 'low01_pagos_facturas_lf_facturas',
  'table' => 'lf_facturas',
  'module' => 'LF_Facturas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
