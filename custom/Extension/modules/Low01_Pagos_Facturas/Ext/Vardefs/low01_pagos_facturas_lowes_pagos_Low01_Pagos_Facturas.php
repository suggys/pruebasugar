<?php
// created: 2017-04-03 22:41:21
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lowes_pagos"] = array (
  'name' => 'low01_pagos_facturas_lowes_pagos',
  'type' => 'link',
  'relationship' => 'low01_pagos_facturas_lowes_pagos',
  'source' => 'non-db',
  'module' => 'lowes_Pagos',
  'bean_name' => 'lowes_Pagos',
  'side' => 'right',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOW01_PAGOS_FACTURAS_TITLE',
  'id_name' => 'low01_pagos_facturas_lowes_pagoslowes_pagos_ida',
  'link-type' => 'one',
);
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lowes_pagos_name"] = array (
  'name' => 'low01_pagos_facturas_lowes_pagos_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOWES_PAGOS_TITLE',
  'save' => true,
  'id_name' => 'low01_pagos_facturas_lowes_pagoslowes_pagos_ida',
  'link' => 'low01_pagos_facturas_lowes_pagos',
  'table' => 'lowes_pagos',
  'module' => 'lowes_Pagos',
  'rname' => 'name',
);
$dictionary["Low01_Pagos_Facturas"]["fields"]["low01_pagos_facturas_lowes_pagoslowes_pagos_ida"] = array (
  'name' => 'low01_pagos_facturas_lowes_pagoslowes_pagos_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_PAGOS_FACTURAS_LOWES_PAGOS_FROM_LOW01_PAGOS_FACTURAS_TITLE_ID',
  'id_name' => 'low01_pagos_facturas_lowes_pagoslowes_pagos_ida',
  'link' => 'low01_pagos_facturas_lowes_pagos',
  'table' => 'lowes_pagos',
  'module' => 'lowes_Pagos',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
