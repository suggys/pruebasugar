<?php
// created: 2018-02-01 19:07:34
$dictionary["Meeting"]["fields"]["dise_prospectos_cocina_meetings"] = array (
  'name' => 'dise_prospectos_cocina_meetings',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_meetings',
  'source' => 'non-db',
  'module' => 'dise_Prospectos_cocina',
  'bean_name' => 'dise_Prospectos_cocina',
  'side' => 'right',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_MEETINGS_FROM_MEETINGS_TITLE',
  'id_name' => 'dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida',
  'link-type' => 'one',
);
$dictionary["Meeting"]["fields"]["dise_prospectos_cocina_meetings_name"] = array (
  'name' => 'dise_prospectos_cocina_meetings_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_MEETINGS_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'save' => true,
  'id_name' => 'dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_meetings',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'name',
);
$dictionary["Meeting"]["fields"]["dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida"] = array (
  'name' => 'dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_MEETINGS_FROM_MEETINGS_TITLE_ID',
  'id_name' => 'dise_prospectos_cocina_meetingsdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_meetings',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
