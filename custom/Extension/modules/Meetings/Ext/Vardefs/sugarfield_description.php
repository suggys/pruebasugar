<?php
 // created: 2018-01-25 15:08:52
$dictionary['Meeting']['fields']['description']['audited']=false;
$dictionary['Meeting']['fields']['description']['massupdate']=false;
$dictionary['Meeting']['fields']['description']['comments']='Full text of the note';
$dictionary['Meeting']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Meeting']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Meeting']['fields']['description']['merge_filter']='disabled';
$dictionary['Meeting']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.55',
  'searchable' => true,
);
$dictionary['Meeting']['fields']['description']['calculated']=false;
$dictionary['Meeting']['fields']['description']['rows']='6';
$dictionary['Meeting']['fields']['description']['cols']='80';

 ?>