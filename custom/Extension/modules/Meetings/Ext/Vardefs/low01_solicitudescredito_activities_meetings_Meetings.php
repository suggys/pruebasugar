<?php
// created: 2017-07-21 12:47:08
$dictionary["Meeting"]["fields"]["low01_solicitudescredito_activities_meetings"] = array (
  'name' => 'low01_solicitudescredito_activities_meetings',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_meetings',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_MEETINGS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
);
