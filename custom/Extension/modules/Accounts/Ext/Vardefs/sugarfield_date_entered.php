<?php
 // created: 2016-09-23 09:41:18
$dictionary['Account']['fields']['date_entered']['audited']=false;
$dictionary['Account']['fields']['date_entered']['comments']='Date record created';
$dictionary['Account']['fields']['date_entered']['importable']='false';
$dictionary['Account']['fields']['date_entered']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['date_entered']['duplicate_merge_dom_value']=1;
$dictionary['Account']['fields']['date_entered']['merge_filter']='disabled';
$dictionary['Account']['fields']['date_entered']['calculated']=false;
$dictionary['Account']['fields']['date_entered']['enable_range_search']='1';

 ?>