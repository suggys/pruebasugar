<?php
 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['credito_diponible_grupo_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['credito_diponible_grupo_c']['labelValue']='Crédito Disponible de Grupo :';
$dictionary['Account']['fields']['credito_diponible_grupo_c']['calculated']='1';
$dictionary['Account']['fields']['credito_diponible_grupo_c']['formula']='related($gpe_grupo_empresas_accounts,"credito_diponible_grupo_c")';
$dictionary['Account']['fields']['credito_diponible_grupo_c']['enforced']='1';
$dictionary['Account']['fields']['credito_diponible_grupo_c']['dependency']='greaterThan(strlen(related($gpe_grupo_empresas_accounts,"id")),1)';
$dictionary['Account']['fields']['credito_diponible_grupo_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>