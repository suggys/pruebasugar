<?php
// created: 2016-09-14 15:26:16
$dictionary["Account"]["fields"]["gpe_grupo_empresas_accounts"] = array (
  'name' => 'gpe_grupo_empresas_accounts',
  'type' => 'link',
  'relationship' => 'gpe_grupo_empresas_accounts',
  'source' => 'non-db',
  'module' => 'GPE_Grupo_empresas',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["gpe_grupo_empresas_accounts_name"] = array (
  'name' => 'gpe_grupo_empresas_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE',
  'save' => true,
  'id_name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
  'link' => 'gpe_grupo_empresas_accounts',
  'table' => 'gpe_grupo_empresas',
  'module' => 'GPE_Grupo_empresas',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["gpe_grupo_empresas_accountsgpe_grupo_empresas_ida"] = array (
  'name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
  'link' => 'gpe_grupo_empresas_accounts',
  'table' => 'gpe_grupo_empresas',
  'module' => 'GPE_Grupo_empresas',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
