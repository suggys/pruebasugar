<?php
// created: 2016-10-12 00:13:37
$dictionary["Account"]["fields"]["nc_notas_credito_accounts"] = array (
  'name' => 'nc_notas_credito_accounts',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_accounts',
  'source' => 'non-db',
  'module' => 'NC_Notas_credito',
  'bean_name' => false,
  'vname' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'nc_notas_credito_accountsaccounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
