<?php
// created: 2016-10-24 13:40:01
$dictionary["Account"]["fields"]["accounts_lf_facturas_1"] = array (
  'name' => 'accounts_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'accounts_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_lf_facturas_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
