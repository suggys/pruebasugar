<?php
 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['primary_address_other_state_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['primary_address_other_state_c']['labelValue']='Otro Estado';
$dictionary['Account']['fields']['primary_address_other_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['primary_address_other_state_c']['enforced']='';
$dictionary['Account']['fields']['primary_address_other_state_c']['dependency']='equal($primary_address_state_c,"OTRO")';

 ?>