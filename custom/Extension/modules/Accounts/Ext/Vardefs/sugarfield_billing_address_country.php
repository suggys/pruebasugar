<?php
 // created: 2017-07-14 19:13:07
$dictionary['Account']['fields']['billing_address_country']['audited']=false;
$dictionary['Account']['fields']['billing_address_country']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_country']['comments']='The country used for the billing address';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_country']['calculated']=false;
$dictionary['Account']['fields']['billing_address_country']['type']='enum';
$dictionary['Account']['fields']['billing_address_country']['options']='primary_address_country_c_list';

 ?>
