<?php
 // created: 2017-01-22 16:20:24
$dictionary['Account']['fields']['date_modified']['audited']=false;
$dictionary['Account']['fields']['date_modified']['comments']='Date record last modified';
$dictionary['Account']['fields']['date_modified']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['date_modified']['duplicate_merge_dom_value']=1;
$dictionary['Account']['fields']['date_modified']['merge_filter']='disabled';
$dictionary['Account']['fields']['date_modified']['calculated']=false;
$dictionary['Account']['fields']['date_modified']['enable_range_search']='1';

 ?>