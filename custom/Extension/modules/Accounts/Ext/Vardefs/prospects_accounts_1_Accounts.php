<?php
// created: 2017-07-20 22:48:10
$dictionary["Account"]["fields"]["prospects_accounts_1"] = array (
  'name' => 'prospects_accounts_1',
  'type' => 'link',
  'relationship' => 'prospects_accounts_1',
  'source' => 'non-db',
  'module' => 'Prospects',
  'bean_name' => 'Prospect',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE',
  'id_name' => 'prospects_accounts_1prospects_ida',
);
$dictionary["Account"]["fields"]["prospects_accounts_1_name"] = array (
  'name' => 'prospects_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE',
  'save' => true,
  'id_name' => 'prospects_accounts_1prospects_ida',
  'link' => 'prospects_accounts_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["prospects_accounts_1prospects_ida"] = array (
  'name' => 'prospects_accounts_1prospects_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_PROSPECTS_TITLE_ID',
  'id_name' => 'prospects_accounts_1prospects_ida',
  'link' => 'prospects_accounts_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
