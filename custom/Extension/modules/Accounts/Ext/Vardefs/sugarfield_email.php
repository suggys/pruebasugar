<?php
 // created: 2016-09-26 11:57:33
$dictionary['Account']['fields']['email']['len']='100';
$dictionary['Account']['fields']['email']['audited']=true;
$dictionary['Account']['fields']['email']['massupdate']=true;
$dictionary['Account']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['email']['merge_filter']='disabled';
$dictionary['Account']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.89',
  'searchable' => true,
);
$dictionary['Account']['fields']['email']['calculated']=false;
$dictionary['Account']['fields']['email']['required']=true;

 ?>
