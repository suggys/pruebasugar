<?php
 // created: 2017-07-14 22:55:42
$dictionary['Account']['fields']['billing_address_state']['audited']=false;
$dictionary['Account']['fields']['billing_address_state']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_state']['comments']='The state used for billing address';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_state']['calculated']=false;
$dictionary['Account']['fields']['billing_address_state']['type']='enum';
$dictionary['Account']['fields']['billing_address_state']['options']='state_c_list';
$dictionary['Account']['fields']['billing_address_state']['visibility_grid']=array (
  'trigger' => 'billing_address_country',
  'values' =>
  array (
    'US' =>
    array (
      0 => 'OTRO',
    ),
    'CA' =>
    array (
      0 => 'OTRO',
    ),
    'FR' =>
    array (
      0 => 'OTRO',
    ),
    'MX' =>
    array (
      0 => '',
      1 => 'AGU',
      2 => 'BCN',
      3 => 'BCS',
      4 => 'CAM',
      5 => 'CHP',
      6 => 'CHH',
      7 => 'CMX',
      8 => 'COA',
      9 => 'COL',
      10 => 'DUR',
      11 => 'MEX',
      12 => 'GUA',
      13 => 'GRO',
      14 => 'HID',
      15 => 'JAL',
      16 => 'MIC',
      17 => 'MOR',
      18 => 'NAY',
      19 => 'NLE',
      20 => 'OAX',
      21 => 'PUE',
      22 => 'QUE',
      23 => 'ROO',
      24 => 'SLP',
      25 => 'SIN',
      26 => 'SON',
      27 => 'TAB',
      28 => 'TAM',
      29 => 'TLA',
      30 => 'VER',
      31 => 'YUC',
      32 => 'ZAC',
    ),
    'EU' =>
    array (
      0 => 'OTRO',
    ),
    'DE' =>
    array (
      0 => 'OTRO',
    ),
    'GB' =>
    array (
      0 => 'OTRO',
    ),
    'JP' =>
    array (
      0 => 'OTRO',
    ),
    'PR' =>
    array (
      0 => 'OTRO',
    ),
  ),
);
$dictionary['Account']['fields']['billing_address_state']['len']=100;
$dictionary['Account']['fields']['billing_address_state']['dependency']=false;

 ?>
