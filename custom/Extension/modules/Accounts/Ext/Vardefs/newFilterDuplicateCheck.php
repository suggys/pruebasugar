<?php
$dictionary['Account']['duplicate_check']['FilterDuplicateCheck'] = array(
    'filter_template' => array(
        array(
            '$or' => array(
                array('id_ar_credit_c' => array('$equals' => '$id_ar_credit_c')),
                array('id_pos_c' => array('$equals' => '$id_pos_c')),
                array('id_linea_anticipo_c' => array('$equals' => '$id_linea_anticipo_c')),
            )
        ),
    ),
    'ranking_fields' => array(
        array('in_field_name' => 'id_ar_credit_c', 'dupe_field_name' => 'id_ar_credit_c'),
        array('in_field_name' => 'id_pos_c', 'dupe_field_name' => 'id_pos_c'),
        array('in_field_name' => 'id_linea_anticipo_c', 'dupe_field_name' => 'id_linea_anticipo_c'),
    )
);