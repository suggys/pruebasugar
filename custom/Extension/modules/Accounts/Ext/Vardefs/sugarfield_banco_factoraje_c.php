<?php
 // created: 2018-05-14 04:05:37
$dictionary['Account']['fields']['banco_factoraje_c']['labelValue']='Banco de Factoraje';
$dictionary['Account']['fields']['banco_factoraje_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['banco_factoraje_c']['enforced']='';
$dictionary['Account']['fields']['banco_factoraje_c']['dependency']='equal($con_factoraje_c,true)';

 ?>