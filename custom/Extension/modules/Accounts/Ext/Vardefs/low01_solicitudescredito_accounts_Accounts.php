<?php
// created: 2017-07-21 12:47:08
$dictionary["Account"]["fields"]["low01_solicitudescredito_accounts"] = array (
  'name' => 'low01_solicitudescredito_accounts',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_accounts',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'low01_solicitudescredito_accountsaccounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
