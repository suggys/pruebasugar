<?php
 // created: 2016-09-27 21:52:05
$dictionary['Account']['fields']['phone_alternate']['len']='100';
$dictionary['Account']['fields']['phone_alternate']['audited']=true;
$dictionary['Account']['fields']['phone_alternate']['massupdate']=false;
$dictionary['Account']['fields']['phone_alternate']['comments']='An alternate phone number';
$dictionary['Account']['fields']['phone_alternate']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['phone_alternate']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['phone_alternate']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_alternate']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.03',
  'searchable' => true,
);
$dictionary['Account']['fields']['phone_alternate']['calculated']=false;

 ?>