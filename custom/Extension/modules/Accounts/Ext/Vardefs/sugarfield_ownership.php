<?php
 // created: 2018-03-06 00:27:44
$dictionary['Account']['fields']['ownership']['len']='100';
$dictionary['Account']['fields']['ownership']['audited']=true;
$dictionary['Account']['fields']['ownership']['massupdate']=false;
$dictionary['Account']['fields']['ownership']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['ownership']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['ownership']['merge_filter']='disabled';
$dictionary['Account']['fields']['ownership']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['ownership']['calculated']=false;
$dictionary['Account']['fields']['ownership']['required']=false;

 ?>