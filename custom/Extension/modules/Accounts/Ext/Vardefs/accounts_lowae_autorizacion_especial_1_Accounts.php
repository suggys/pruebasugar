<?php
// created: 2016-10-13 23:48:34
$dictionary["Account"]["fields"]["accounts_lowae_autorizacion_especial_1"] = array (
  'name' => 'accounts_lowae_autorizacion_especial_1',
  'type' => 'link',
  'relationship' => 'accounts_lowae_autorizacion_especial_1',
  'source' => 'non-db',
  'module' => 'lowae_autorizacion_especial',
  'bean_name' => 'lowae_autorizacion_especial',
  'vname' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
