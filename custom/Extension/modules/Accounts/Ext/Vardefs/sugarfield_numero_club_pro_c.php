<?php
 // created: 2018-05-14 04:05:40
$dictionary['Account']['fields']['numero_club_pro_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['numero_club_pro_c']['labelValue']='Número de Tarjeta Club PRO';
$dictionary['Account']['fields']['numero_club_pro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['numero_club_pro_c']['enforced']='';
$dictionary['Account']['fields']['numero_club_pro_c']['dependency']='equal($club_pro_c,true)';

 ?>