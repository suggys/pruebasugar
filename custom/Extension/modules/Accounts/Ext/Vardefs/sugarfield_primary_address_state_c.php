<?php
 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['primary_address_state_c']['labelValue']='Estado:';
$dictionary['Account']['fields']['primary_address_state_c']['dependency']='';
$dictionary['Account']['fields']['primary_address_state_c']['visibility_grid']=array (
  'trigger' => 'primary_address_country_c',
  'values' => 
  array (
    'US' => 
    array (
      0 => 'OTRO',
    ),
    'CA' => 
    array (
      0 => 'OTRO',
    ),
    'FR' => 
    array (
      0 => 'OTRO',
    ),
    'MX' => 
    array (
      0 => '',
      1 => 'AGU',
      2 => 'BCN',
      3 => 'BCS',
      4 => 'CAM',
      5 => 'CHP',
      6 => 'CHH',
      7 => 'CMX',
      8 => 'COA',
      9 => 'COL',
      10 => 'DUR',
      11 => 'MEX',
      12 => 'GUA',
      13 => 'GRO',
      14 => 'HID',
      15 => 'JAL',
      16 => 'MIC',
      17 => 'MOR',
      18 => 'NAY',
      19 => 'NLE',
      20 => 'OAX',
      21 => 'PUE',
      22 => 'QUE',
      23 => 'ROO',
      24 => 'SLP',
      25 => 'SIN',
      26 => 'SON',
      27 => 'TAB',
      28 => 'TAM',
      29 => 'TLA',
      30 => 'VER',
      31 => 'YUC',
      32 => 'ZAC',
    ),
    'EU' => 
    array (
      0 => 'OTRO',
    ),
    'DE' => 
    array (
      0 => 'OTRO',
    ),
    'GB' => 
    array (
      0 => 'OTRO',
    ),
    'JP' => 
    array (
      0 => 'OTRO',
    ),
    'PR' => 
    array (
      0 => 'OTRO',
    ),
  ),
);

 ?>