<?php
 // created: 2017-08-02 22:46:38
$dictionary['Account']['fields']['description']['audited']=true;
$dictionary['Account']['fields']['description']['massupdate']=false;
$dictionary['Account']['fields']['description']['comments']='Full text of the note';
$dictionary['Account']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['description']['merge_filter']='disabled';
$dictionary['Account']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.72',
  'searchable' => true,
);
$dictionary['Account']['fields']['description']['calculated']=false;
$dictionary['Account']['fields']['description']['rows']='6';
$dictionary['Account']['fields']['description']['cols']='80';
$dictionary['Account']['fields']['description']['dependency']='and(equal($codigo_bloqueo_c,"Otro"), equal($estado_bloqueo_c,"Bloqueado"))';
$dictionary['Account']['fields']['description']['required']=true;

 ?>
