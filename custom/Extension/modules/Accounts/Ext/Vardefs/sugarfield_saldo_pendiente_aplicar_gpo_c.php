<?php
 // created: 2018-05-14 04:05:42
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['labelValue']='Saldo Pendiente por Aplicar de Grupo';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['calculated']='1';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['formula']='related($gpe_grupo_empresas_accounts,"saldo_pendiente_aplicar_c")';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['enforced']='1';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['dependency']='greaterThan(strlen(related($gpe_grupo_empresas_accounts,"id")),1)';
$dictionary['Account']['fields']['saldo_pendiente_aplicar_gpo_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>