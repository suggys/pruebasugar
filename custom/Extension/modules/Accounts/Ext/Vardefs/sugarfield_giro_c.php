<?php
 // created: 2018-05-14 04:05:39
$dictionary['Account']['fields']['giro_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['giro_c']['labelValue']='Giro / Industria';
$dictionary['Account']['fields']['giro_c']['dependency']='';
$dictionary['Account']['fields']['giro_c']['visibility_grid']=array (
  'trigger' => 'tipo_cliente2_c',
  'values' => 
  array (
    1 => 
    array (
    ),
    2 => 
    array (
    ),
    3 => 
    array (
    ),
    '' => 
    array (
    ),
    'comercial' => 
    array (
      0 => 'arquitecto',
      1 => 'construccion_vertical',
      2 => 'contratista',
      3 => 'decorador',
      4 => 'desarrollador_de_vivienda_residencial',
      5 => 'ingeniero',
      6 => 'mtto_industrial',
      7 => 'obra_civil',
      8 => 'proyecto_temporal_del_hogar',
      9 => 'urbanizador',
      10 => 'otro',
    ),
    'especialista' => 
    array (
    ),
  ),
);

 ?>