<?php
 // created: 2018-05-14 04:05:41
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['labelValue']='Saldo Deudor de Grupo';
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['calculated']='1';
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['formula']='related($gpe_grupo_empresas_accounts,"saldo_deudor_c")';
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['enforced']='1';
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['dependency']='greaterThan(strlen(related($gpe_grupo_empresas_accounts,"id")),1)';
$dictionary['Account']['fields']['saldo_deudor_gpe_empresas_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>