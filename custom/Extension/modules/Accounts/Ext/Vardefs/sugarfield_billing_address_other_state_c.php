<?php
 // created: 2018-05-14 04:05:38
$dictionary['Account']['fields']['billing_address_other_state_c']['duplicate_merge_dom_value']=0;
$dictionary['Account']['fields']['billing_address_other_state_c']['labelValue']='Otro Estado Dirección Fiscal';
$dictionary['Account']['fields']['billing_address_other_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['billing_address_other_state_c']['enforced']='';
$dictionary['Account']['fields']['billing_address_other_state_c']['dependency']='equal($billing_address_state,"OTRO")';

 ?>