<?php
$mod_strings['LBL_SIN_ORDEN_XDIAS'] = "Sin Ordenes por 120 días";
$mod_strings['LBL_NAME'] = 'Nombre o Razón Social:';
$mod_strings['LBL_ID_AR_CREDIT'] = 'ID AR Credit';
$mod_strings['LBL_FACTURACION_AUTOMATICA_C'] = 'Facturación Automática:';
$mod_strings['LBL_TYPE'] = 'Tipo:';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_BILLING_ADDRESS_STREET'] = 'Calle Dirección Fiscal';
$mod_strings['LBL_BILLING_ADDRESS_POSTALCODE'] = 'C.P. Dirección Fiscal';
$mod_strings['LBL_BILLING_ADDRESS_CITY'] = 'Municipio Dirección Fiscal';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Estado Dirección Fiscal';
$mod_strings['LBL_BILLING_ADDRESS_COUNTRY'] = 'País Dirección Fiscal';
$mod_strings['LBL_PHONE_ALT'] = 'Teléfono Celular:';
$mod_strings['LBL_PHONE_OFFICE'] = 'Teléfono de Oficina:';
$mod_strings['LBL_ANY_EMAIL'] = 'Email Comercial:';
$mod_strings['LBL_WEBSITE'] = 'Página de Internet:';
$mod_strings['LBL_ID_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_ID_VENDEDOR_C'] = 'Vendedor';
$mod_strings['LBL_LIMITE_CREDITO_AUTORIZADO_C'] = 'Límite de Crédito Autorizado:';
$mod_strings['LBL_PLAZO_PAGO_AUTORIZADO_C'] = 'Plazo de Pago Autorizado:';
$mod_strings['LBL_DIAS_TOLERANCIA_C'] = 'Días de Tolerancia:';
$mod_strings['LBL_ID_LINEA_ANTICIPO_C'] = 'ID Línea de Anticipo:';
$mod_strings['LBL_NUMERO_CUENTA_CLABE_C'] = 'Número de Cuenta Clabe Banamex:';
$mod_strings['LBL_ESTADO_CUENTA_C'] = 'Estado de la Cuenta:';
$mod_strings['LBL_ESTADO_BLOQUEO_C'] = 'Estado de Bloqueo:';
$mod_strings['LBL_CODIGO_BLOQUEO_C'] = 'Código de Bloqueo:';
$mod_strings['LBL_ESTADO_CARTERA_C'] = 'Estado según Cartera:';
$mod_strings['LBL_CREDITO_DISPONIBLE_C'] = 'Crédito Disponible:';
$mod_strings['LBL_SALDO_DEUDOR_C'] = 'Saldo Deudor:';
$mod_strings['LBL_SALDO_PENDIENTE_APLICAR_C'] = 'Saldo Pendiente por Aplicar:';
$mod_strings['LBL_SALDO_FAVOR_C'] = 'Saldo a Favor Línea de Anticipo:';
$mod_strings['LBL_ID_POS_C'] = 'ID POS:';
$mod_strings['LBL_OWNERSHIP'] = 'Nombre del Aval:';
$mod_strings['LBL_NOMBRE_AVALA_C'] = 'Nombre del Aval:';
$mod_strings['LBL_NOMBRE_DOCUMENTO_GARANTIA_C'] = 'Nombre Documento Garantía:';
$mod_strings['LBL_MONTO_DOCUMENTO_GARANTIA_C'] = 'Monto de Documento Garantía:';
$mod_strings['LBL_GRUPO_EMPRESAS_ID'] = 'Grupo de Empresas:';
$mod_strings['LBL_ADMINISTRAR_CREDITO_GRUPAL_C'] = 'Sumar / Administrar Límite de Crédito por Grupo:';
$mod_strings['LBL_ESTADO_GRUPO_C'] = 'Estado del Grupo';
$mod_strings['LBL_TIPO_CUENTA_C'] = 'Tipo de Cuenta';
$mod_strings['LBL_DATE_ENTERED'] = 'Fecha de Creación:';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_0'] = 'LBL_CURRENCY';
$mod_strings['LBL_COMENTADIOS_C'] = 'Comentarios';
$mod_strings['LBL_TIPO_DOCUMENTO_C'] = 'Catálogo Documento Garantía:';
$mod_strings['LBL_CURRENCY_1'] = 'LBL_CURRENCY';
$mod_strings['LBL_ACCOUNT_TYPE_C'] = 'Tipo de Persona:';
$mod_strings['LBL_BILLING_ADDRESS_NUMBER_C'] = 'Número';
$mod_strings['LBL_BILLING_ADDRESS_COL_C'] = 'Colonia:';
$mod_strings['LBL_BILLING_ADDRESS_STATE_C'] = 'Estado Facturación c :';
$mod_strings['LBL_OWN_TEL_CEL_C'] = 'Teléfono Celular:';
$mod_strings['LBL_OWN_EMAIL_C'] = 'Email:';
$mod_strings['LBL_OWN_RFC_C'] = 'RFC de Representante Legal:';
$mod_strings['LBL_CURRENCY_2'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_3'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_4'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_5'] = 'LBL_CURRENCY';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Saldos Crédito AR';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Documento Garantía';
$mod_strings['LBL_RECORDVIEW_PANEL3'] = 'Información Adicional';
$mod_strings['LBL_RECORDVIEW_PANEL4'] = 'Condiciones Créditicias';
$mod_strings['LBL_RECORDVIEW_PANEL10'] = '';
$mod_strings['LBL_RECORDVIEW_PANEL11'] = '';
$mod_strings['LBL_RECORDVIEW_PANEL12'] = '';
$mod_strings['LBL_RECORDVIEW_PANEL5'] = 'Perfil del Cliente';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORD_SHOWMORE'] = 'Información de Sistema';
$mod_strings['LBL_RECORDVIEW_PANEL7'] = 'Saldo Línea de anticipo';
$mod_strings['LBL_RECORDVIEW_PANEL8'] = 'Dirección Fiscal';
$mod_strings['LBL_RECORDVIEW_PANEL9'] = 'Dirección Alterna';
$mod_strings['LBL_ACCOUNT'] = 'Cliente Crédito AR:';
$mod_strings['LBL_ACCOUNTS_SUBPANEL_TITLE'] = 'Clientes Crédito AR';
$mod_strings['LBL_ACCOUNT_TYPE'] = 'Tipo de Cliente Crédito AR';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mis Cuentas';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia:';
$mod_strings['LBL_ID_CAJERO_C'] = 'Cajero:';
$mod_strings['LBL_GRUPO_EMPRESAS_ID_C'] = 'Grupo de Gmpresas:';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número:';
$mod_strings['LBL_TIPO_TELEFONO_PRINCIPAL_C'] = 'Tipo de Teléfono:';
$mod_strings['LBL_ISBUSINESSCUSTOMER_C'] = 'Cliente de Negocios :';
$mod_strings['LBL_DATE_MODIFIED'] = 'Fecha de Modificación:';
$mod_strings['LBL_DESCRIPTION'] = 'Comentarios:';
$mod_strings['LBL_NOMBRE_REPRESENTANTE_C'] = 'Nombre del Representante Legal:';
$mod_strings['LBL_PRIMARY_ADDRESS_STREET_C'] = 'Calle:';
$mod_strings['LBL_PRIMARY_ADDRESS_POSTALCODE_C'] = 'C. P.';
$mod_strings['LBL_PRIMARY_ADDRESS_CITY_C'] = 'Municipio:';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY_C'] = 'País:';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE_C'] = 'Estado:';
$mod_strings['LBL_ID_AR_CREDIT_C'] = 'ID AR Credit';
$mod_strings['LBL_ESTADO_INTERFAZ_POS_C'] = 'Estado de Envío a POS :';
$mod_strings['LBL_CURRENCY_6'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_7'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_8'] = 'LBL_CURRENCY';
$mod_strings['LBL_CURRENCY_9'] = 'LBL_CURRENCY';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE'] = 'Grupo de Empresas';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE_ID'] = 'Grupo de Empresas ID';
$mod_strings['LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Grupo de Empresas';
$mod_strings['LBL_CALCULA_ID_LINEA_CREDITO'] = 'Calcula ID Linea de Anticipo';
$mod_strings['LBL_ENVIAR_POS_C'] = 'Enviar a POS :';
$mod_strings['LBL_TIPO_TELEFONO_PRINCIPAL'] = 'Tipo de Teléfono :';
$mod_strings['LBL_CREDITO_DISPONIBLE_FLEXIBLE_'] = 'Crédito Disponible Flexible :';
$mod_strings['LBL_CREDITO_DISPONIBLE_FLEXIBLE'] = 'Crédito Disponible Flexible :';
$mod_strings['LBL_TOTAL_ORDENES_C'] = 'total ordenes c';
$mod_strings['LBL_RECORDVIEW_PANEL6'] = 'Saldo de grupo';
$mod_strings['LBL_CREDITO_DIPONIBLE_GRUPO_C'] = 'Crédito Disponible de Grupo :';
$mod_strings['LBL_LIMITE_CREDITO_GRUPO_C'] = 'Límite de Crédito de Grupo :';
$mod_strings['LBL_SALDO_DEUDOR_GPE_EMPRESAS_C'] = 'Saldo Deudor de Grupo';
$mod_strings['LBL_TOTAL_ORDENES'] = 'total ordenes';
$mod_strings['LBL_CURRENCY_10'] = 'LBL_CURRENCY';
$mod_strings['LBL_IMPORTADO_C'] = 'Registro Importado';
$mod_strings['LBL_IMPORTADO'] = 'Registro Importado';
$mod_strings['LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE'] = 'Órdenes';
$mod_strings['LBL_MEMBER_ORG_SUBPANEL_TITLE'] = 'Organizaciones Miembro';
$mod_strings['LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Facturas / Notas de Cargo';
$mod_strings['LBL_SALDO_PENDIENTE_APLICAR_GPO'] = 'Saldo Pendiente por Aplicar de Grupo';
$mod_strings['LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE'] = 'Autorización Especial';
$mod_strings['LBL_COD_BLOQUEAO_AUX'] = 'LBL_COD_BLOQUEAO_AUX';
$mod_strings['LBL_USER_VENDEDOR'] = 'Vendedor';
$mod_strings['LBL_CARTERA_PENDIENTE_FACTURAR'] = 'Pendiente de Facturar';
$mod_strings['LBL_CARTERA_VIGENTE'] = 'Vigente';
$mod_strings['LBL_CARTERA_1A15'] = '1 a 15';
$mod_strings['LBL_CARTERA_16A30'] = '16 a 30';
$mod_strings['LBL_CARTERA_31A60'] = '31 a 60';
$mod_strings['LBL_CARTERA_61A90'] = '61 a 90';
$mod_strings['LBL_CARTERA_91A120'] = '91 a 120';
$mod_strings['LBL_CARTERA_MAS120'] = 'Mas 120';
$mod_strings['LBL_CARTERA_TOTAL'] = 'Total';
$mod_strings['LBL_CARTERA_VENCIDO'] = 'Vencido';
$mod_strings['LBL_CARTERA_PORCENTAJE_VENCIDO'] = 'Porcentaje Vencido';
$mod_strings['LBL_CARTERA_IMPACTO_CV'] = 'Impacto CV';
$mod_strings['LBL_CARTERA_IMPACTO_CV_120'] = 'Impacto CV 120';
$mod_strings['LBL_NAME_TIENDA'] = 'Tienda';
$mod_strings['LBL_CARTERA_PORCENTAJE_PENDIENTE_FACTURAR'] = 'Porcentaje Pendiente Facturar';
$mod_strings['LBL_CARTERA_PORCENTAJE_VIGENTE'] = 'Porcentaje Vigente';
$mod_strings['LBL_CARTERA_DISPONIBLE_EXCEDIDO'] = 'Disponible Excedido';
$mod_strings['LBL_USER_VENDEDOR_C'] = 'LBL_USER_VENDEDOR_C';
$mod_strings['LBL_TIPO_CLIENTE'] = 'Tipo de Cliente';
$mod_strings['LBL_CONTACTO_PAGOS_KONESH'] = 'contacto pagos konesh';
$mod_strings['LBL_LEADS_SUBPANEL_TITLE'] = 'Leads';
$mod_strings['LBL_GIRO'] = 'Giro / Industria';
$mod_strings['LBL_OTRO_GIRO'] = 'Otro Giro / Industria';
$mod_strings['LBL_OFICIO'] = 'Oficio';
$mod_strings['LBL_OTRO_OFICIO'] = 'Otro Oficio';
$mod_strings['LBL_SUBTIPO'] = 'Subtipo';
$mod_strings['LBL_METODO_PAGO'] = 'Método de Pago';
$mod_strings['LBL_CLUB_PRO'] = 'Cuenta con Tarjeta Club PRO';
$mod_strings['LBL_NUMERO_CLUB_PRO'] = 'Número de Tarjeta Club PRO';
$mod_strings['LBL_COMPETIDORES_DIRECTOS'] = 'Competidores Directos';
$mod_strings['LBL_CUENTA_PATRON'] = 'Cuenta del Patrón';
$mod_strings['LBL_VOL_VENTAS_ANUAL_EST'] = 'Volumen de Ventas Anuales Estimadas';
$mod_strings['LBL_PRIMARY_ADDRESS_OTHER_STATE'] = 'Otro Estado';
$mod_strings['LBL_BILLING_ADDRESS_OTHER_STATE'] = 'Otro Estado Dirección Fiscal';
$mod_strings['LBL_BILLING_ADDRESS_COLONIA'] = 'Colonia Dirección Fiscal';
$mod_strings['LBL_BILLING_ADDRESS_NUMBER'] = 'Número Dirección Fiscal';
$mod_strings['LBL_ALT_ADDRESS_POSTALCODE'] = 'C. P. Dirección Alterna';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'País Dirección Alterna';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Estado Dirección Alterna';
$mod_strings['LBL_ALT_ADDRESS_OTHER_STATE'] = 'Otro Estado Dirección Alterna';
$mod_strings['LBL_ALT_ADDRESS_CITY'] = 'Municipio Dirección Alterna';
$mod_strings['LBL_ALT_ADDRESS_COLONIA'] = 'Colonia Dirección Alterna';
$mod_strings['LBL_ALT_ADDRESS_STREET'] = 'Calle Dirección Alterna';
$mod_strings['LBL_ALT_ADDRESS_NUMERO'] = 'Número Dirección Alterna';
$mod_strings['LBL_FOLIO'] = 'Folio';
$mod_strings['LBL_TIPO_CLIENTE2'] = 'Tipo Cliente';
$mod_strings['LBL_ASSIGNED_TO'] = 'Vendedor (Asignado a)';
$mod_strings['LBL_CONTACTO_PAGOS_KONESH'] = 'Contacto Para Pagos Konesh';
$mod_strings['LBL_CONTACT_ID1'] = 'Contacto Para Pagos Konesh';
$mod_strings['LBL_APRUEBA_OC_O_COT'] = 'Aprueba OC o Cotización';
$mod_strings['LBL_NUM_CTA_CLABE_BANAMEX'] = 'Número de Cuenta Banamex';
$mod_strings['LBL_CON_FACTORAJE'] = "Con Factoraje";
$mod_strings['LBL_BANCO_FACTORAJE'] = "Banco de Factoraje";
$mod_strings['LBL_RFC_FACTORAJE'] = "RFC de Factoraje";
