<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
    5,
    'FR-11.7. Bloqueo por LE – Limite de crédito excedido',
    'custom/modules/Accounts/BloqueoCuentaAR_por_LE.php',
    'BloqueoCuentaAR_por_LE',
    'BloqueoCuentaAR_por_LE'
);
?>