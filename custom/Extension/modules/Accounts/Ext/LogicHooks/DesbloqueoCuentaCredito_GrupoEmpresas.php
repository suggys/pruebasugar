<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : [];
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : [];
$hook_array['before_save'][] = array(
    7,
    'Desbloqueo de Clientes Crédito AR pertenecientes a un Grupo de Empresas',
    'custom/modules/Accounts/DesbloqueoCuentaCredito_GrupoEmpresas.php',
    'DesbloqueoCuentaCredito_GrupoEmpresas',
    'desbloqueoCuentaCreditoGrupoEmpresas'
);
?>
