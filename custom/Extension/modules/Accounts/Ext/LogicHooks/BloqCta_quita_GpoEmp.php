<?php
	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
    $hook_array['after_relationship_delete'][] = array(
        1,
        'Bloqueo de Clientes Crédito AR al quitar una cta al Grupo de empresas',
        'custom/modules/Accounts/BloqCta_quita_GpoEmp.php',
        'BloqCta_quita_GpoEmpClass',
        'BloqCta_quita_GpoEmp'
        );
?>
