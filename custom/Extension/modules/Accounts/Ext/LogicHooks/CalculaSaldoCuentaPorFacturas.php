<?php
	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
    $hook_array['after_relationship_add'][] = array(
        2,
        'Calculo de saldo deudor de Empresas a partir de facturas',
        'custom/modules/Accounts/CalculaSaldoCuentaPorFacturas.php',
        'CalculaSaldoCuentaPorFacturas',
        'calculaSaldoDeudor'
    );
    $hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
    $hook_array['after_relationship_delete'][] = array(
        2,
        'Calculo de saldo deudor de Empresas a partir de facturas',
        'custom/modules/Accounts/CalculaSaldoCuentaPorFacturas.php',
        'CalculaSaldoCuentaPorFacturas',
        'calculaSaldoDeudor'
    );
?>