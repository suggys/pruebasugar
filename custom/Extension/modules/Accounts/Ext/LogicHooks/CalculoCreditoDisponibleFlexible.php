<?php
	$hook_version = 1;
	$hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
    $hook_array['before_save'][] = array(
        4,
        'Calculo credito disponible flexible',
        'custom/modules/Accounts/AccountsLogicHooksManager.php',
        'AccountsLogicHooksManager',
        'calculaCreditDisponibleFlexible'
    );
?>