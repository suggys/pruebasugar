<?php
	$hook_version = 1;
    $hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        1,
        'Bloqueo de Clientes Crédito AR pertenecientes a un Grupo de Empresas',
        'custom/modules/Accounts/BloqueoCuentasCreditoGrupoEmpresas.php',
        'BloqueoCuentasCreditoGrupoEmpresas',
        'bloqueoCuentaCreditoGrupoEmpresas'
    );
    $hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
    $hook_array['before_save'][] = array(
        1,
        'save fetched row',
        'custom/modules/Accounts/BloqueoCuentasCreditoGrupoEmpresas.php',
        'BloqueoCuentasCreditoGrupoEmpresas',
        'saveFetchedRow'
    );
?>