<?php
$dependencies['Accounts']['gpe_grupo_empresas_accounts_name'] = array(
  'hooks' => array("edit","view"),
  'triggerFields' => array('id_ar_credit_c'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'gpe_grupo_empresas_accounts_name',
        'value' => 'not(equal($id_ar_credit_c,""))',
      ),
    ),
  ),
);
