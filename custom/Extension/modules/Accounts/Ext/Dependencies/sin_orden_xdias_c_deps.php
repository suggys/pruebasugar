<?php
// Convirtiendo campo sin_orden_xdias_c como solo lectura siempre
$dependencies['Accounts']['sin_orden_xdias_c'] = array(
    'hooks' => array("edit"),
    'trigger' => 'true',
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'sin_orden_xdias_c',
              'value' => 'true',
          ),
      ),
    ),
);
