<?php
$dependencies['Accounts']['panel_10_visibility'] = array(
        'hooks' => array("edit","view"),
        'trigger' => 'not(equal($id_ar_credit_c, ""))',
        'triggerFields' => array('id_ar_credit_c'),
        'onload' => true,
        //Actions is a list of actions to fire when the trigger is true
        'actions' => array(
            array(
                'name' => 'SetPanelVisibility',
                'params' => array(
                    'target' => 'LBL_RECORDVIEW_PANEL10',
                    'value' => 'true',
                ),
            )
        ),
        //notActions is a list of actions to fire when the trigger is false
        'notActions' => array(
            array(
                'name' => 'SetPanelVisibility',
                'params' => array(
                    'target' => 'LBL_RECORDVIEW_PANEL10',
                    'value' => 'false',
                ),
            ),
        ),
    );
