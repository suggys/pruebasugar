<?php
 // created: 2017-11-02 17:23:51
$dictionary['lowae_autorizacion_especial']['fields']['numero_veces_aplicacion_c']['labelValue']='Número de Veces de Aplicación';
$dictionary['lowae_autorizacion_especial']['fields']['numero_veces_aplicacion_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowae_autorizacion_especial']['fields']['numero_veces_aplicacion_c']['enforced']='';
$dictionary['lowae_autorizacion_especial']['fields']['numero_veces_aplicacion_c']['dependency']='equal($motivo_solicitud_c,"credito_disponible_temporal")';

 ?>