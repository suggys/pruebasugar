<?php
 // created: 2017-11-02 19:17:54
$dictionary['lowae_autorizacion_especial']['fields']['saldo_pendiente_aplicar_c']['labelValue']='Monto Pendiente por Aplicar';
$dictionary['lowae_autorizacion_especial']['fields']['saldo_pendiente_aplicar_c']['formula']='ifElse(and(equal($motivo_solicitud_c,"otro"),equal($estado_autorizacion_c,"pendiente")),$monto_venta_c,$saldo_pendiente_aplicar_c)';
$dictionary['lowae_autorizacion_especial']['fields']['saldo_pendiente_aplicar_c']['enforced']='false';
$dictionary['lowae_autorizacion_especial']['fields']['saldo_pendiente_aplicar_c']['dependency']='or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"promesa_de_pago"),equal($motivo_solicitud_c,"otro"))';
$dictionary['lowae_autorizacion_especial']['fields']['saldo_pendiente_aplicar_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>