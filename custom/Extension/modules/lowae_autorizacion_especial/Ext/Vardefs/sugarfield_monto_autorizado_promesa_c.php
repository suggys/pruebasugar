<?php
 // created: 2017-04-28 17:49:54
$dictionary['lowae_autorizacion_especial']['fields']['monto_autorizado_promesa_c']['labelValue']='Monto Autorizado de Promesa de Pago';
$dictionary['lowae_autorizacion_especial']['fields']['monto_autorizado_promesa_c']['formula']='ifElse(and(equal($estado_autorizacion_c,"pendiente"),equal($motivo_solicitud_c,"promesa_de_pago")),subtract($monto_venta_c,related($accounts_lowae_autorizacion_especial_1,"credito_disponible_flexible_c")),$monto_autorizado_promesa_c)';
$dictionary['lowae_autorizacion_especial']['fields']['monto_autorizado_promesa_c']['enforced']='false';
$dictionary['lowae_autorizacion_especial']['fields']['monto_autorizado_promesa_c']['dependency']='equal($motivo_solicitud_c,"promesa_de_pago")';
$dictionary['lowae_autorizacion_especial']['fields']['monto_autorizado_promesa_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>