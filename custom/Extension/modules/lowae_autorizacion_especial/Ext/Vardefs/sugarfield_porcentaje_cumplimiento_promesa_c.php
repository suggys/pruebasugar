<?php
 // created: 2017-04-03 18:17:39
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['duplicate_merge_dom_value']=0;
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['labelValue']='% de Cumplimiento';
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['calculated']='1';
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['formula']='ifElse(greaterThan($monto_promesa_c, 0),round(divide($monto_abonado_c,$monto_promesa_c),2),0)';
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['enforced']='1';
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['dependency']='equal($motivo_solicitud_c,"promesa_de_pago")';
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['len']=5;
$dictionary['lowae_autorizacion_especial']['fields']['porcentaje_cumplimiento_promesa_c']['precision']=2;

 ?>
