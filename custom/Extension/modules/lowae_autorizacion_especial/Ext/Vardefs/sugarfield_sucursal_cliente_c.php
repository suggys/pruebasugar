<?php
 // created: 2017-07-06 12:22:24
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['duplicate_merge_dom_value']=0;
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['labelValue']='Sucursal de Cliente';
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['calculated']='true';
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['formula']='related($accounts_lowae_autorizacion_especial_1,"id_sucursal_c")';
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['enforced']='true';
$dictionary['lowae_autorizacion_especial']['fields']['sucursal_cliente_c']['dependency']='';

 ?>