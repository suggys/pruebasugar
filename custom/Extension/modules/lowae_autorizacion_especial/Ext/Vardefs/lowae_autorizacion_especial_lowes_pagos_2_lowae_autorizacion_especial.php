<?php
// created: 2017-11-06 23:18:19
$dictionary["lowae_autorizacion_especial"]["fields"]["lowae_autorizacion_especial_lowes_pagos_2"] = array (
  'name' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'type' => 'link',
  'relationship' => 'lowae_autorizacion_especial_lowes_pagos_2',
  'source' => 'non-db',
  'module' => 'lowes_Pagos',
  'bean_name' => 'lowes_Pagos',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_2_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'lowae_autorizacion_especial_lowes_pagos_2lowes_pagos_idb',
);
