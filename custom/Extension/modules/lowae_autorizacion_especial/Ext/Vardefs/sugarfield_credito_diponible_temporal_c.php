<?php
 // created: 2017-11-02 17:18:41
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['dependency']='equal($motivo_solicitud_c,"credito_disponible_temporal")';
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['importable']='false';
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['duplicate_merge']='disabled';
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['duplicate_merge_dom_value']=0;
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['calculated']='1';
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['formula']='ifElse(and(equal($estado_autorizacion_c,"pendiente"),equal($motivo_solicitud_c,"credito_disponible_temporal")),subtract($monto_venta_c,related($accounts_lowae_autorizacion_especial_1,"credito_disponible_flexible_c")),$credito_diponible_temporal_c)';
$dictionary['lowae_autorizacion_especial']['fields']['credito_diponible_temporal_c']['enforced']=true;

 ?>