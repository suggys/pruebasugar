<?php
 // created: 2017-01-30 16:19:55
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['duplicate_merge_dom_value']=0;
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['labelValue']='Fecha de Autorización';
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['calculated']='1';
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['formula']='ifElse(equal($estado_autorizacion_c,"autorizada"),now(),$fecha_autorizacion_c)';
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['enforced']='1';
$dictionary['lowae_autorizacion_especial']['fields']['fecha_autorizacion_c']['dependency']='or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"promesa_de_pago"),equal($motivo_solicitud_c,"otro"))';

 ?>
