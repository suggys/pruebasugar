<?php
 // created: 2017-01-12 00:09:08
$dictionary['lowae_autorizacion_especial']['fields']['estado_c']['default']='';
$dictionary['lowae_autorizacion_especial']['fields']['estado_c']['calculated']='true';
$dictionary['lowae_autorizacion_especial']['fields']['estado_c']['enforced']='true';
$dictionary['lowae_autorizacion_especial']['fields']['estado_c']['dependency']='and(or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"otro")), equal($aplica_por_unica_ocacion_c,1))';
?>
