<?php
// created: 2016-10-13 23:48:34
$dictionary["lowae_autorizacion_especial"]["fields"]["accounts_lowae_autorizacion_especial_1"] = array (
  'name' => 'accounts_lowae_autorizacion_especial_1',
  'type' => 'link',
  'relationship' => 'accounts_lowae_autorizacion_especial_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'id_name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["lowae_autorizacion_especial"]["fields"]["accounts_lowae_autorizacion_especial_1_name"] = array (
  'name' => 'accounts_lowae_autorizacion_especial_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
  'link' => 'accounts_lowae_autorizacion_especial_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["lowae_autorizacion_especial"]["fields"]["accounts_lowae_autorizacion_especial_1accounts_ida"] = array (
  'name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE_ID',
  'id_name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
  'link' => 'accounts_lowae_autorizacion_especial_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
