<?php
 // created: 2017-03-29 00:03:52
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_segun_monto_c']['labelValue']='Estado de la Promesa según el Monto';
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_segun_monto_c']['dependency']='equal($motivo_solicitud_c,"promesa_de_pago")';
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_segun_monto_c']['visibility_grid']='';
$dictionary['lowae_autorizacion_especial']['fields']['estado_promesa_segun_monto_c']['default']='sin_pago';

 ?>
