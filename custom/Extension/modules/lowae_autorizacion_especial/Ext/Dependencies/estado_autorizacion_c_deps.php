<?php
$dependencies['lowae_autorizacion_especial']['estado_autorizacion_c_deps'] = array(
    'hooks' => array("edit"),
    'trigger' => 'or(equal($estado_autorizacion_c,"rechazada"),equal($estado_autorizacion_c,"finalizada"),equal($estado_autorizacion_c,"terminada_incumplida"),equal($estado_autorizacion_c,"cancelada_devolucion_cancelacion"))',
    'triggerFields' => array('estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'estado_autorizacion_c',
              'value' => 'true',
          ),
      ),
    ),
);
