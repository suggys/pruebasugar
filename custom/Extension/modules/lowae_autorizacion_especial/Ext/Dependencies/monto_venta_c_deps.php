<?php

$dependencies['lowae_autorizacion_especial']['monto_venta_c_deps'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'isInList($estado_autorizacion_c,createList("rechazada","finalizada","terminada_incumplida","cancelada_devolucion_cancelacion"))',
  'triggerFields' => array('estado_autorizacion_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'monto_venta_c',
        'value' => 'true',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'monto_venta_c',
        'value' => 'false',
      ),
    ),
  )
);

?>
