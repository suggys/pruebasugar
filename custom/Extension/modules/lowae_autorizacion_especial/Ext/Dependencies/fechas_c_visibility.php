<?php

$dependencies['lowae_autorizacion_especial']['fechas_c_visibility'] = array(
  'hooks' => array("edit","view"),
  'triggerFields' => array('motivo_solicitud_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'fecha_inicio_c',
        'value' => 'or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"promesa_de_pago"),equal($motivo_solicitud_c,"otro"))',
      ),
    ),
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'fecha_fin_c',
        'value' => 'or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"otro"))',
      ),
    ),
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'fecha_cumplimiento_c',
        'value' => 'equal($motivo_solicitud_c,"promesa_de_pago")',
      ),
    ),
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'monto_promesa_c',
        'value' => 'equal($motivo_solicitud_c,"promesa_de_pago")',
      ),
    ),
  ),
);

?>
