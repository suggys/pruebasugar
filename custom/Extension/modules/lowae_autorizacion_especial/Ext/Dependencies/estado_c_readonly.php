<?php
$dependencies['lowae_autorizacion_especial']['estado_c_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'equal($aplica_por_unica_ocacion_c,true)',
    'triggerFields' => array('aplica_por_unica_ocacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'estado_c',
              'value' => 'true',
          ),
      ),
    ),
);
