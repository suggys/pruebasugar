<?php
$dependencies['lowae_autorizacion_especial']['estado_autorizacion_c_visibility'] = array(
  'hooks' => array("edit","view"),
  'triggerFields' => array('motivo_solicitud_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'estado_autorizacion_c',
        'value' => 'or(equal($motivo_solicitud_c,"credito_disponible_temporal"),equal($motivo_solicitud_c,"promesa_de_pago"),equal($motivo_solicitud_c,"otro"))',
      ),
    )
  ),
);
