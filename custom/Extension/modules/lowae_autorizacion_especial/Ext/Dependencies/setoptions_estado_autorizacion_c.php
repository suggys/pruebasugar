<?php

$dependencies['lowae_autorizacion_especial']['setoptions_estado_autorizacion_c'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'or(and(equal($estado_autorizacion_c,"autorizada"),equal($motivo_solicitud_c,"otro")),and(equal($estado_autorizacion_c,"autorizada"),equal($motivo_solicitud_c,"credito_disponible_temporal")))',
   'triggerFields' => array('motivo_solicitud_c','estado_autorizacion_c'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'estado_autorizacion_c',
         'keys' => 'createList("autorizada","finalizada")',
         'labels' => 'createList("Autorizada","Finalizada")'
       ),
     ),
   ),
);

?>
