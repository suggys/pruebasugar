<?
$dependencies['lowae_autorizacion_especial']['monto_promesa_c_readonly'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'and(equal($motivo_solicitud_c,"promesa_de_pago"),not(equal($estado_autorizacion_c,"pendiente")))',
    'triggerFields' => array('motivo_solicitud_c','estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'monto_promesa_c',
                'value' => 'true',
            ),
        ),
    ),
);

?>
