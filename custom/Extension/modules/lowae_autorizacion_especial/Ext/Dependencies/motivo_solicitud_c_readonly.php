<?php
$dependencies['lowae_autorizacion_especial']['motivo_solicitud_c_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'not(equal($estado_autorizacion_c,"pendiente"))',
    'triggerFields' => array('estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'motivo_solicitud_c',
              'value' => 'true',
          ),
      ),
    ),
);
