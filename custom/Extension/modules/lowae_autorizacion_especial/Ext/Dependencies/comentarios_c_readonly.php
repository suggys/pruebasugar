<?php
$dependencies['lowae_autorizacion_especial']['comentarios_c_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'not(equal($estado_autorizacion_c,"pendiente"))',
    'triggerFields' => array('estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'comentarios_c',
              'value' => 'true',
          ),
      ),
    ),
);
