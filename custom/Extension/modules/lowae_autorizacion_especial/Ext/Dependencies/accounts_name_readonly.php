<?php
$dependencies['lowae_autorizacion_especial']['accounts_name_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'or(equal($estado_autorizacion_c,"autorizada"),equal($estado_autorizacion_c,"finalizada"),equal($estado_autorizacion_c,"rechazada"),equal($estado_autorizacion_c,"terminada_incumplida"))',
    'triggerFields' => array('estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'accounts_lowae_autorizacion_especial_1_name',
              'value' => 'true',
          ),
      ),
    ),
);
