<?php

$dependencies['lowae_autorizacion_especial']['setoptions_estado_autorizacion2_c'] = array(
   'hooks' => array("edit","save"),
   'trigger' => 'and(equal($estado_autorizacion_c,"autorizada"),equal($motivo_solicitud_c,"promesa_de_pago"))',
   'triggerFields' => array('motivo_solicitud_c','estado_autorizacion_c'),
   'onload' => true,
   'actions' => array(
     array(
       'name' => 'SetOptions',
       'params' => array(
         'target' => 'estado_autorizacion_c',
         'keys' => 'createList("autorizada","terminada_incumplida")',
         'labels' => 'createList("Autorizada","Terminada Incumplida")'
       ),
     ),
   ),
);

?>
