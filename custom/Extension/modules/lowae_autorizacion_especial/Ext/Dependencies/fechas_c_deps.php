<?php

$dependencies['lowae_autorizacion_especial']['fechas_c_deps'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'not(equal($estado_autorizacion_c,"pendiente"))',
    'triggerFields' => array('estado_autorizacion_c'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'fecha_inicio_c',
                'value' => 'true',
            ),
        ),
        array(
            'name' => 'ReadOnly',
            'params' => array(
                'target' => 'fecha_fin_c',
                'value' => 'true',
            ),
        ),
    ),
    'notActions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'fecha_inicio_c',
              'value' => 'false',
          ),
      ),
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'fecha_fin_c',
              'value' => 'false',
          ),
      ),
    )
);

?>
