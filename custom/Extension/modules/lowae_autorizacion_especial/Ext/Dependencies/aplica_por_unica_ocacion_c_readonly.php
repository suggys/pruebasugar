<?php
$dependencies['lowae_autorizacion_especial']['aplica_por_unica_ocacion_c_readonly'] = array(
    'hooks' => array("edit"),
    'trigger' => 'or(equal($estado_autorizacion_c,"rechazada"),equal($estado_autorizacion_c,"finalizada"),equal($estado_autorizacion_c,"terminada_incumplida"),equal($estado_autorizacion_c,"cancelada_devolucion_cancelacion"),not(equal($numero_veces_aplicacion_c,0)))',
    'triggerFields' => array('estado_autorizacion_c','numero_veces_aplicacion_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'aplica_por_unica_ocacion_c',
              'value' => 'true',
          ),
      ),
    ),
);
