<?php
$dependencies['lowae_autorizacion_especial']['required_fields_promesa_pago_dep'] = array(
  'hooks' => array("edit"),
  'trigger' => 'true',
  'triggerFields' => array('motivo_solicitud_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'monto_promesa_c',
        'label' => 'monto_promesa_c_label',
        'value' => 'equal($motivo_solicitud_c,"promesa_de_pago")',
      ),
    ),
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'fecha_cumplimiento_c',
        'label' => 'fecha_cumplimiento_c_label',
        'value' => 'equal($motivo_solicitud_c,"promesa_de_pago")',
      ),
    ),
  ),
);
