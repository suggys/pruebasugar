<?php
// created: 2017-07-21 12:47:08
$dictionary["Email"]["fields"]["low01_solicitudescredito_activities_emails"] = array (
  'name' => 'low01_solicitudescredito_activities_emails',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_emails',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_EMAILS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
);
