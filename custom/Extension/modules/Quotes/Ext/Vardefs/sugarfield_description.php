<?php
 // created: 2018-01-16 06:06:19
$dictionary['Quote']['fields']['description']['required']=true;
$dictionary['Quote']['fields']['description']['audited']=true;
$dictionary['Quote']['fields']['description']['massupdate']=false;
$dictionary['Quote']['fields']['description']['comments']='Full text of the note';
$dictionary['Quote']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Quote']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Quote']['fields']['description']['merge_filter']='disabled';
$dictionary['Quote']['fields']['description']['unified_search']=false;
$dictionary['Quote']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.57',
  'searchable' => true,
);
$dictionary['Quote']['fields']['description']['calculated']=false;
$dictionary['Quote']['fields']['description']['rows']='6';
$dictionary['Quote']['fields']['description']['cols']='80';

 ?>