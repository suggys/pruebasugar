<?php
// created: 2018-01-11 00:47:54
$dictionary["Quote"]["fields"]["dise_prospectos_cocina_quotes"] = array (
  'name' => 'dise_prospectos_cocina_quotes',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_quotes',
  'source' => 'non-db',
  'module' => 'dise_Prospectos_cocina',
  'bean_name' => 'dise_Prospectos_cocina',
  'side' => 'right',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_QUOTES_TITLE',
  'id_name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
  'link-type' => 'one',
);
$dictionary["Quote"]["fields"]["dise_prospectos_cocina_quotes_name"] = array (
  'name' => 'dise_prospectos_cocina_quotes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'save' => true,
  'id_name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_quotes',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'name',
);
$dictionary["Quote"]["fields"]["dise_prospectos_cocina_quotesdise_prospectos_cocina_ida"] = array (
  'name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_QUOTES_FROM_QUOTES_TITLE_ID',
  'id_name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_quotes',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
