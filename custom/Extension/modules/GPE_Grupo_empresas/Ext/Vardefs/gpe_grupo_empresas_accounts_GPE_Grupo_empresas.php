<?php
// created: 2016-09-14 15:26:16
$dictionary["GPE_Grupo_empresas"]["fields"]["gpe_grupo_empresas_accounts"] = array (
  'name' => 'gpe_grupo_empresas_accounts',
  'type' => 'link',
  'relationship' => 'gpe_grupo_empresas_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_GPE_GRUPO_EMPRESAS_ACCOUNTS_FROM_GPE_GRUPO_EMPRESAS_TITLE',
  'id_name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
  'link-type' => 'many',
  'side' => 'left',
);
