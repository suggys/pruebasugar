<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
$hook_array['after_relationship_delete'][] = array(
    1,
    'Calcular Saldo Deudor, Crédito Disponible y Límite de Crédito al desvincular cta Cliente Crédito AR a Gpo de Empresas',
    'custom/modules/GPE_Grupo_empresas/Calcula_Saldos_Al_Vincular_Desvincular.php',
    'Calcula_Saldos_Al_Vincular_Desvincular',
    'fnCalculaSaldoAlDesvincular'
);
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    1,
    'Calcular Saldo Deudor, Crédito Disponible y Límite de Crédito al vincular cta Cliente Crédito AR a Gpo de Empresas',
    'custom/modules/GPE_Grupo_empresas/Calcula_Saldos_Al_Vincular_Desvincular.php',
    'Calcula_Saldos_Al_Vincular_Desvincular',
    'fnCalculaSaldoAlVincular'
);

$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
    1,
    'Before save',
    'custom/modules/GPE_Grupo_empresas/GrupoEmpresasLogicHooksMananger.php',
    'GrupoEmpresasLogicHooksMananger',
    'beforeSave'
);
?>