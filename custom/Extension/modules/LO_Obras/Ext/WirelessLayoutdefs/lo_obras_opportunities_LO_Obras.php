<?php
 // created: 2017-07-20 15:21:47
$layout_defs["LO_Obras"]["subpanel_setup"]['lo_obras_opportunities'] = array (
  'order' => 100,
  'module' => 'Opportunities',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_OPPORTUNITIES_TITLE',
  'get_subpanel_data' => 'lo_obras_opportunities',
);
