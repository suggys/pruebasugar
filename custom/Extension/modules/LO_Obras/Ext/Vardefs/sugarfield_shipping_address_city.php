<?php
 // created: 2017-07-21 22:03:06
$dictionary['LO_Obras']['fields']['shipping_address_city']['len']='100';
$dictionary['LO_Obras']['fields']['shipping_address_city']['audited']=false;
$dictionary['LO_Obras']['fields']['shipping_address_city']['massupdate']=false;
$dictionary['LO_Obras']['fields']['shipping_address_city']['comments']='The city used for the shipping address';
$dictionary['LO_Obras']['fields']['shipping_address_city']['duplicate_merge']='enabled';
$dictionary['LO_Obras']['fields']['shipping_address_city']['duplicate_merge_dom_value']='1';
$dictionary['LO_Obras']['fields']['shipping_address_city']['merge_filter']='disabled';
$dictionary['LO_Obras']['fields']['shipping_address_city']['unified_search']=false;
$dictionary['LO_Obras']['fields']['shipping_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LO_Obras']['fields']['shipping_address_city']['calculated']=false;

 ?>