<?php
 // created: 2017-07-21 21:56:27
$dictionary['LO_Obras']['fields']['shipping_address_street']['audited']=false;
$dictionary['LO_Obras']['fields']['shipping_address_street']['massupdate']=false;
$dictionary['LO_Obras']['fields']['shipping_address_street']['comments']='The street address used for for shipping purposes';
$dictionary['LO_Obras']['fields']['shipping_address_street']['duplicate_merge']='enabled';
$dictionary['LO_Obras']['fields']['shipping_address_street']['duplicate_merge_dom_value']='1';
$dictionary['LO_Obras']['fields']['shipping_address_street']['merge_filter']='disabled';
$dictionary['LO_Obras']['fields']['shipping_address_street']['unified_search']=false;
$dictionary['LO_Obras']['fields']['shipping_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.25',
  'searchable' => true,
);
$dictionary['LO_Obras']['fields']['shipping_address_street']['calculated']=false;
$dictionary['LO_Obras']['fields']['shipping_address_street']['rows']='4';
$dictionary['LO_Obras']['fields']['shipping_address_street']['cols']='20';

 ?>