<?php
// created: 2017-07-20 15:21:47
$dictionary["LO_Obras"]["fields"]["lo_obras_opportunities"] = array (
  'name' => 'lo_obras_opportunities',
  'type' => 'link',
  'relationship' => 'lo_obras_opportunities',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_LO_OBRAS_TITLE',
  'id_name' => 'lo_obras_opportunitieslo_obras_ida',
  'link-type' => 'many',
  'side' => 'left',
);
