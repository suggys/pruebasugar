<?php

$dictionary['LO_Obras']['duplicate_check']['FilterDuplicateCheck'] = array(
    'filter_template' => array(
        array(
            '$or' => array(
                array('name' => array('$equals' => '$name')),
                //array('duns_num' => array('$equals' => '$duns_num')),
                // array(
                //     '$and' => array(
                //         array('name' => array('$starts' => '$name')),
                //         array(
                //             '$or' => array(
                //                 array('billing_address_city' => array('$starts' => '$billing_address_city')),
                //             )
                //         ),
                //     )
                // ),
            )
        ),
    ),
    'ranking_fields' => array(
        array('in_field_name' => 'name', 'dupe_field_name' => 'name'),
        //array('in_field_name' => 'billing_address_city', 'dupe_field_name' => 'billing_address_city'),
    )
);
