<?php
 // created: 2017-07-16 22:51:12
$dictionary['LO_Obras']['fields']['email']['len']='100';
$dictionary['LO_Obras']['fields']['email']['required']=true;
$dictionary['LO_Obras']['fields']['email']['audited']=false;
$dictionary['LO_Obras']['fields']['email']['massupdate']=true;
$dictionary['LO_Obras']['fields']['email']['duplicate_merge']='enabled';
$dictionary['LO_Obras']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['LO_Obras']['fields']['email']['merge_filter']='disabled';
$dictionary['LO_Obras']['fields']['email']['unified_search']=false;
$dictionary['LO_Obras']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.5',
  'searchable' => true,
);
$dictionary['LO_Obras']['fields']['email']['calculated']=false;

 ?>