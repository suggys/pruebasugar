<?php
 // created: 2017-09-06 00:30:33
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['len']='5';
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['audited']=false;
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['massupdate']=false;
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['comments']='The zip code used for the shipping address';
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['duplicate_merge']='enabled';
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['merge_filter']='disabled';
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['unified_search']=false;
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LO_Obras']['fields']['shipping_address_postalcode']['calculated']=false;

 ?>