<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Dirección';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Nuevo Panel 2';
$mod_strings['LBL_SHIPPING_ADDRESS_POSTALCODE'] = 'Código Postal';
$mod_strings['LBL_SHIPPING_ADDRESS_STREET'] = 'Calle';
$mod_strings['LBL_SHIPPING_ADDRESS_COUNTRY'] = 'País';
$mod_strings['LBL_SHIPPING_ADDRESS_CITY'] = 'Municipio';
$mod_strings['LBL_BILLING_ADDRESS_STATE'] = 'Otro estado';
$mod_strings['LBL_SHIPPING_ADDRESS_STATE'] = 'Estado';
$mod_strings['LBL_BILLING_ADDRESS_NUMERO'] = 'Número';
$mod_strings['LBL_SHOW_MORE'] = 'Información de Sistema';
$mod_strings['LBL_LO_OBRAS_OPPORTUNITIES_FROM_OPPORTUNITIES_TITLE'] = 'Proyectos';
