<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_RECORD'] = 'Crear Forma de Pago';
$mod_strings['LNK_LIST'] = 'Vista Formas de Pago';
$mod_strings['LBL_MODULE_NAME'] = 'Formas de Pago';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Forma de Pago';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nuevo Forma de Pago';
$mod_strings['LNK_IMPORT_VCARD'] = 'Importar Forma de Pago vCard';
$mod_strings['LNK_IMPORT_FDP_FORMASPAGO'] = 'Importar Formas de Pago';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Buscar Forma de Pago';
$mod_strings['LBL_FDP_FORMASPAGO_SUBPANEL_TITLE'] = 'Formas de Pago';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mi Formas de Pago';
$mod_strings['LBL_VALU'] = 'Forma de Pago para el SAT';
$mod_strings['LBL_CODE'] = 'Código SAT';
$mod_strings['LBL_NAME'] = 'Forma de Pago Entrante';
