<?php
 // created: 2018-10-04 03:50:11
$dictionary['FDP_formasPago']['fields']['name']['len']='255';
$dictionary['FDP_formasPago']['fields']['name']['audited']=false;
$dictionary['FDP_formasPago']['fields']['name']['massupdate']=false;
$dictionary['FDP_formasPago']['fields']['name']['unified_search']=false;
$dictionary['FDP_formasPago']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['FDP_formasPago']['fields']['name']['calculated']=false;

 ?>