<?php
 // created: 2016-10-13 23:48:33
$layout_defs["Accounts"]["subpanel_setup"]['accounts_lowae_autorizacion_especial_1'] = array (
  'order' => 100,
  'module' => 'lowae_autorizacion_especial',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_LOWAE_AUTORIZACION_ESPECIAL_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'get_subpanel_data' => 'accounts_lowae_autorizacion_especial_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
