<?php
 // created: 2016-10-21 13:50:29
$layout_defs["lowes_Pagos"]["subpanel_setup"]['lowes_pagos_lf_facturas_1'] = array (
  'order' => 100,
  'module' => 'LF_Facturas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'get_subpanel_data' => 'lowes_pagos_lf_facturas_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
