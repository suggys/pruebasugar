<?php

 // created: 2017-02-23 18:24:39
$layout_defs["lowae_autorizacion_especial"]["subpanel_setup"]['lowae_autorizacion_especial_orden_ordenes_1'] = array (
  'order' => 100,
  'module' => 'orden_Ordenes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE',
  'get_subpanel_data' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'top_buttons' =>
  array (
    0 =>
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 =>
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
