<?php
 // created: 2017-10-17 01:51:44
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_low03_agentesexternos_1'] = array (
  'order' => 100,
  'module' => 'Low03_AgentesExternos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW03_AGENTESEXTERNOS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_low03_agentesexternos_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
