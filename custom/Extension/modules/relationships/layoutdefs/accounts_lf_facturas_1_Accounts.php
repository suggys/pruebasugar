<?php
 // created: 2016-10-24 13:40:01
$layout_defs["Accounts"]["subpanel_setup"]['accounts_lf_facturas_1'] = array (
  'order' => 100,
  'module' => 'LF_Facturas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'get_subpanel_data' => 'accounts_lf_facturas_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
