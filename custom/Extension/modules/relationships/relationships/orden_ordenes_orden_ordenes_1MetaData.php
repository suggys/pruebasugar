<?php
// created: 2016-10-11 21:43:30
$dictionary["orden_ordenes_orden_ordenes_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'orden_ordenes_orden_ordenes_1' => 
    array (
      'lhs_module' => 'orden_Ordenes',
      'lhs_table' => 'orden_ordenes',
      'lhs_key' => 'id',
      'rhs_module' => 'orden_Ordenes',
      'rhs_table' => 'orden_ordenes',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'orden_ordenes_orden_ordenes_1_c',
      'join_key_lhs' => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
      'join_key_rhs' => 'orden_ordenes_orden_ordenes_1orden_ordenes_idb',
    ),
  ),
  'table' => 'orden_ordenes_orden_ordenes_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'orden_ordenes_orden_ordenes_1orden_ordenes_ida' => 
    array (
      'name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
      'type' => 'id',
    ),
    'orden_ordenes_orden_ordenes_1orden_ordenes_idb' => 
    array (
      'name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'orden_ordenes_orden_ordenes_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'orden_ordenes_orden_ordenes_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'orden_ordenes_orden_ordenes_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'orden_ordenes_orden_ordenes_1orden_ordenes_idb',
      ),
    ),
  ),
);