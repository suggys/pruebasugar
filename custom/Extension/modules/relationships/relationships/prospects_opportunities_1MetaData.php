<?php
// created: 2017-07-14 22:54:49
$dictionary["prospects_opportunities_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' =>
  array (
    'prospects_opportunities_1' =>
    array (
      'lhs_module' => 'Prospects',
      'lhs_table' => 'prospects',
      'lhs_key' => 'id',
      'rhs_module' => 'Opportunities',
      'rhs_table' => 'opportunities',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'prospects_opportunities_1_c',
      'join_key_lhs' => 'prospects_opportunities_1prospects_ida',
      'join_key_rhs' => 'prospects_opportunities_1opportunities_idb',
    ),
  ),
  'table' => 'prospects_opportunities_1_c',
  'fields' =>
  array (
    'id' =>
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' =>
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' =>
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'prospects_opportunities_1prospects_ida' =>
    array (
      'name' => 'prospects_opportunities_1prospects_ida',
      'type' => 'id',
    ),
    'prospects_opportunities_1opportunities_idb' =>
    array (
      'name' => 'prospects_opportunities_1opportunities_idb',
      'type' => 'id',
    ),
  ),
  'indices' =>
  array (
    0 =>
    array (
      'name' => 'prospects_opportunities_1spk',
      'type' => 'primary',
      'fields' =>
      array (
        0 => 'id',
      ),
    ),
    1 =>
    array (
      'name' => 'prospects_opportunities_1_ida1',
      'type' => 'index',
      'fields' =>
      array (
        0 => 'prospects_opportunities_1prospects_ida',
      ),
    ),
    2 =>
    array (
      'name' => 'prospects_opportunities_1_alt',
      'type' => 'alternate_key',
      'fields' =>
      array (
        0 => 'prospects_opportunities_1opportunities_idb',
      ),
    ),
  ),
);
