<?php
// created: 2017-11-06 23:18:19
$dictionary["lowae_autorizacion_especial_lowes_pagos_2"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'lowae_autorizacion_especial_lowes_pagos_2' => 
    array (
      'lhs_module' => 'lowae_autorizacion_especial',
      'lhs_table' => 'lowae_autorizacion_especial',
      'lhs_key' => 'id',
      'rhs_module' => 'lowes_Pagos',
      'rhs_table' => 'lowes_pagos',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'lowae_autorizacion_especial_lowes_pagos_2_c',
      'join_key_lhs' => 'lowae_auto4a14special_ida',
      'join_key_rhs' => 'lowae_autorizacion_especial_lowes_pagos_2lowes_pagos_idb',
    ),
  ),
  'table' => 'lowae_autorizacion_especial_lowes_pagos_2_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'lowae_auto4a14special_ida' => 
    array (
      'name' => 'lowae_auto4a14special_ida',
      'type' => 'id',
    ),
    'lowae_autorizacion_especial_lowes_pagos_2lowes_pagos_idb' => 
    array (
      'name' => 'lowae_autorizacion_especial_lowes_pagos_2lowes_pagos_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_lowae_autorizacion_especial_lowes_pagos_2_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_lowae_autorizacion_especial_lowes_pagos_2_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lowae_auto4a14special_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_lowae_autorizacion_especial_lowes_pagos_2_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lowae_autorizacion_especial_lowes_pagos_2lowes_pagos_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'lowae_autorizacion_especial_lowes_pagos_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'lowae_auto4a14special_ida',
        1 => 'lowae_autorizacion_especial_lowes_pagos_2lowes_pagos_idb',
      ),
    ),
  ),
);