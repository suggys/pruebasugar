<?php
// created: 2017-02-23 18:24:39
$dictionary["lowae_autorizacion_especial_orden_ordenes_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'lowae_autorizacion_especial_orden_ordenes_1' => 
    array (
      'lhs_module' => 'lowae_autorizacion_especial',
      'lhs_table' => 'lowae_autorizacion_especial',
      'lhs_key' => 'id',
      'rhs_module' => 'orden_Ordenes',
      'rhs_table' => 'orden_ordenes',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'lowae_autorizacion_especial_orden_ordenes_1_c',
      'join_key_lhs' => 'lowae_auto0644special_ida',
      'join_key_rhs' => 'lowae_autorizacion_especial_orden_ordenes_1orden_ordenes_idb',
    ),
  ),
  'table' => 'lowae_autorizacion_especial_orden_ordenes_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'lowae_auto0644special_ida' => 
    array (
      'name' => 'lowae_auto0644special_ida',
      'type' => 'id',
    ),
    'lowae_autorizacion_especial_orden_ordenes_1orden_ordenes_idb' => 
    array (
      'name' => 'lowae_autorizacion_especial_orden_ordenes_1orden_ordenes_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'lowae_autorizacion_especial_orden_ordenes_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'lowae_autorizacion_especial_orden_ordenes_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lowae_auto0644special_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'lowae_autorizacion_especial_orden_ordenes_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'lowae_autorizacion_especial_orden_ordenes_1orden_ordenes_idb',
      ),
    ),
  ),
);