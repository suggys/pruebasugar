<?php
// created: 2016-10-20 16:38:00
$dictionary["nc_notas_credito_lf_facturas_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'nc_notas_credito_lf_facturas_1' => 
    array (
      'lhs_module' => 'NC_Notas_credito',
      'lhs_table' => 'nc_notas_credito',
      'lhs_key' => 'id',
      'rhs_module' => 'LF_Facturas',
      'rhs_table' => 'lf_facturas',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'nc_notas_credito_lf_facturas_1_c',
      'join_key_lhs' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
      'join_key_rhs' => 'nc_notas_credito_lf_facturas_1lf_facturas_idb',
    ),
  ),
  'table' => 'nc_notas_credito_lf_facturas_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'nc_notas_credito_lf_facturas_1nc_notas_credito_ida' => 
    array (
      'name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
      'type' => 'id',
    ),
    'nc_notas_credito_lf_facturas_1lf_facturas_idb' => 
    array (
      'name' => 'nc_notas_credito_lf_facturas_1lf_facturas_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'nc_notas_credito_lf_facturas_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'nc_notas_credito_lf_facturas_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'nc_notas_credito_lf_facturas_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'nc_notas_credito_lf_facturas_1lf_facturas_idb',
      ),
    ),
  ),
);