<?php
// created: 2016-10-24 13:40:01
$dictionary["accounts_lf_facturas_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'accounts_lf_facturas_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'LF_Facturas',
      'rhs_table' => 'lf_facturas',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_lf_facturas_1_c',
      'join_key_lhs' => 'accounts_lf_facturas_1accounts_ida',
      'join_key_rhs' => 'accounts_lf_facturas_1lf_facturas_idb',
    ),
  ),
  'table' => 'accounts_lf_facturas_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'accounts_lf_facturas_1accounts_ida' => 
    array (
      'name' => 'accounts_lf_facturas_1accounts_ida',
      'type' => 'id',
    ),
    'accounts_lf_facturas_1lf_facturas_idb' => 
    array (
      'name' => 'accounts_lf_facturas_1lf_facturas_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'accounts_lf_facturas_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'accounts_lf_facturas_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_lf_facturas_1accounts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'accounts_lf_facturas_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_lf_facturas_1lf_facturas_idb',
      ),
    ),
  ),
);