<?php
// created: 2017-07-23 02:41:44
$dictionary["orden_Ordenes"]["fields"]["prospects_orden_ordenes_1"] = array (
  'name' => 'prospects_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'prospects_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'Prospects',
  'bean_name' => 'Prospect',
  'side' => 'right',
  'vname' => 'LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'prospects_orden_ordenes_1prospects_ida',
  'link-type' => 'one',
);
$dictionary["orden_Ordenes"]["fields"]["prospects_orden_ordenes_1_name"] = array (
  'name' => 'prospects_orden_ordenes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_PROSPECTS_TITLE',
  'save' => true,
  'id_name' => 'prospects_orden_ordenes_1prospects_ida',
  'link' => 'prospects_orden_ordenes_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'name',
);
$dictionary["orden_Ordenes"]["fields"]["prospects_orden_ordenes_1prospects_ida"] = array (
  'name' => 'prospects_orden_ordenes_1prospects_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID',
  'id_name' => 'prospects_orden_ordenes_1prospects_ida',
  'link' => 'prospects_orden_ordenes_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
