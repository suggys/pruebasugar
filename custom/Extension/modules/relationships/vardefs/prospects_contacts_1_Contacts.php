<?php
// created: 2017-07-20 21:33:08
$dictionary["Contact"]["fields"]["prospects_contacts_1"] = array (
  'name' => 'prospects_contacts_1',
  'type' => 'link',
  'relationship' => 'prospects_contacts_1',
  'source' => 'non-db',
  'module' => 'Prospects',
  'bean_name' => 'Prospect',
  'side' => 'right',
  'vname' => 'LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'prospects_contacts_1prospects_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["prospects_contacts_1_name"] = array (
  'name' => 'prospects_contacts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE',
  'save' => true,
  'id_name' => 'prospects_contacts_1prospects_ida',
  'link' => 'prospects_contacts_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["prospects_contacts_1prospects_ida"] = array (
  'name' => 'prospects_contacts_1prospects_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'prospects_contacts_1prospects_ida',
  'link' => 'prospects_contacts_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
