<?php
// created: 2016-10-24 13:40:01
$dictionary["LF_Facturas"]["fields"]["accounts_lf_facturas_1"] = array (
  'name' => 'accounts_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'accounts_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE',
  'id_name' => 'accounts_lf_facturas_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["LF_Facturas"]["fields"]["accounts_lf_facturas_1_name"] = array (
  'name' => 'accounts_lf_facturas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_lf_facturas_1accounts_ida',
  'link' => 'accounts_lf_facturas_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["LF_Facturas"]["fields"]["accounts_lf_facturas_1accounts_ida"] = array (
  'name' => 'accounts_lf_facturas_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE_ID',
  'id_name' => 'accounts_lf_facturas_1accounts_ida',
  'link' => 'accounts_lf_facturas_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
