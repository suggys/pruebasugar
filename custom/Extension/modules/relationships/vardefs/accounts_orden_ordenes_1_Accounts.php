<?php
// created: 2016-10-11 21:41:46
$dictionary["Account"]["fields"]["accounts_orden_ordenes_1"] = array (
  'name' => 'accounts_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'accounts_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'vname' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_orden_ordenes_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
