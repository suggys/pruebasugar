<?php
// created: 2017-10-17 01:51:44
$dictionary["Low03_AgentesExternos"]["fields"]["low01_solicitudescredito_low03_agentesexternos_1"] = array (
  'name' => 'low01_solicitudescredito_low03_agentesexternos_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_low03_agentesexternos_1',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => 'LOW01_SolicitudesCredito',
  'side' => 'right',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW03_AGENTESEXTERNOS_TITLE',
  'id_name' => 'low01_soliaf8bcredito_ida',
  'link-type' => 'one',
);
$dictionary["Low03_AgentesExternos"]["fields"]["low01_solicitudescredito_low03_agentesexternos_1_name"] = array (
  'name' => 'low01_solicitudescredito_low03_agentesexternos_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'save' => true,
  'id_name' => 'low01_soliaf8bcredito_ida',
  'link' => 'low01_solicitudescredito_low03_agentesexternos_1',
  'table' => 'low01_solicitudescredito',
  'module' => 'LOW01_SolicitudesCredito',
  'rname' => 'name',
);
$dictionary["Low03_AgentesExternos"]["fields"]["low01_soliaf8bcredito_ida"] = array (
  'name' => 'low01_soliaf8bcredito_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW03_AGENTESEXTERNOS_TITLE_ID',
  'id_name' => 'low01_soliaf8bcredito_ida',
  'link' => 'low01_solicitudescredito_low03_agentesexternos_1',
  'table' => 'low01_solicitudescredito',
  'module' => 'LOW01_SolicitudesCredito',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
