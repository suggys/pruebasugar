<?php
// created: 2017-02-23 18:33:18
$dictionary["Account"]["fields"]["lowae_autorizacion_especial_accounts_1"] = array (
  'name' => 'lowae_autorizacion_especial_accounts_1',
  'type' => 'link',
  'relationship' => 'lowae_autorizacion_especial_accounts_1',
  'source' => 'non-db',
  'module' => 'lowae_autorizacion_especial',
  'bean_name' => 'lowae_autorizacion_especial',
  'side' => 'right',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'lowae_auto6c11special_ida',
  'link-type' => 'one',
);
$dictionary["Account"]["fields"]["lowae_autorizacion_especial_accounts_1_name"] = array (
  'name' => 'lowae_autorizacion_especial_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'save' => true,
  'id_name' => 'lowae_auto6c11special_ida',
  'link' => 'lowae_autorizacion_especial_accounts_1',
  'table' => 'lowae_autorizacion_especial',
  'module' => 'lowae_autorizacion_especial',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["lowae_auto6c11special_ida"] = array (
  'name' => 'lowae_auto6c11special_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'lowae_auto6c11special_ida',
  'link' => 'lowae_autorizacion_especial_accounts_1',
  'table' => 'lowae_autorizacion_especial',
  'module' => 'lowae_autorizacion_especial',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
