<?php
// created: 2017-03-29 15:36:09
// $dictionary["lowes_Pagos"]["fields"]["lowae_autorizacion_especial_lowes_pagos_1"] = array (
//   'name' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'type' => 'link',
//   'relationship' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'source' => 'non-db',
//   'module' => 'lowae_autorizacion_especial',
//   'bean_name' => 'lowae_autorizacion_especial',
//   'side' => 'right',
//   'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE',
//   'id_name' => 'lowae_auto3868special_ida',
//   'link-type' => 'one',
// );
// $dictionary["lowes_Pagos"]["fields"]["lowae_autorizacion_especial_lowes_pagos_1_name"] = array (
//   'name' => 'lowae_autorizacion_especial_lowes_pagos_1_name',
//   'type' => 'relate',
//   'source' => 'non-db',
//   'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
//   'save' => true,
//   'id_name' => 'lowae_auto3868special_ida',
//   'link' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'table' => 'lowae_autorizacion_especial',
//   'module' => 'lowae_autorizacion_especial',
//   'rname' => 'name',
// );
// $dictionary["lowes_Pagos"]["fields"]["lowae_auto3868special_ida"] = array (
//   'name' => 'lowae_auto3868special_ida',
//   'type' => 'id',
//   'source' => 'non-db',
//   'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_LOWES_PAGOS_1_FROM_LOWES_PAGOS_TITLE_ID',
//   'id_name' => 'lowae_auto3868special_ida',
//   'link' => 'lowae_autorizacion_especial_lowes_pagos_1',
//   'table' => 'lowae_autorizacion_especial',
//   'module' => 'lowae_autorizacion_especial',
//   'rname' => 'id',
//   'reportable' => false,
//   'side' => 'right',
//   'massupdate' => false,
//   'duplicate_merge' => 'disabled',
//   'hideacl' => true,
// );
