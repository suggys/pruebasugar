<?php
// created: 2017-07-26 16:30:16
$dictionary["Document"]["fields"]["low01_solicitudescredito_documents_1"] = array (
  'name' => 'low01_solicitudescredito_documents_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_documents_1',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => 'LOW01_SolicitudesCredito',
  'side' => 'right',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'id_name' => 'low01_solicitudescredito_documents_1low01_solicitudescredito_ida',
  'link-type' => 'one',
);
$dictionary["Document"]["fields"]["low01_solicitudescredito_documents_1_name"] = array (
  'name' => 'low01_solicitudescredito_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'save' => true,
  'id_name' => 'low01_solicitudescredito_documents_1low01_solicitudescredito_ida',
  'link' => 'low01_solicitudescredito_documents_1',
  'table' => 'low01_solicitudescredito',
  'module' => 'LOW01_SolicitudesCredito',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["low01_solicitudescredito_documents_1low01_solicitudescredito_ida"] = array (
  'name' => 'low01_solicitudescredito_documents_1low01_solicitudescredito_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_DOCUMENTS_TITLE_ID',
  'id_name' => 'low01_solicitudescredito_documents_1low01_solicitudescredito_ida',
  'link' => 'low01_solicitudescredito_documents_1',
  'table' => 'low01_solicitudescredito',
  'module' => 'LOW01_SolicitudesCredito',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
