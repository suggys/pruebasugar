<?php
// created: 2016-10-11 21:43:30
$dictionary["orden_Ordenes"]["fields"]["orden_ordenes_orden_ordenes_1"] = array (
  'name' => 'orden_ordenes_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'orden_ordenes_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'vname' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_L_TITLE',
  'id_name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_idb',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["orden_Ordenes"]["fields"]["orden_ordenes_orden_ordenes_1_right"] = array (
  'name' => 'orden_ordenes_orden_ordenes_1_right',
  'type' => 'link',
  'relationship' => 'orden_ordenes_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'side' => 'right',
  'vname' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE',
  'id_name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
  'link-type' => 'one',
);
$dictionary["orden_Ordenes"]["fields"]["orden_ordenes_orden_ordenes_1_name"] = array (
  'name' => 'orden_ordenes_orden_ordenes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_L_TITLE',
  'save' => true,
  'id_name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
  'link' => 'orden_ordenes_orden_ordenes_1_right',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'name',
);
$dictionary["orden_Ordenes"]["fields"]["orden_ordenes_orden_ordenes_1orden_ordenes_ida"] = array (
  'name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ORDEN_ORDENES_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_R_TITLE_ID',
  'id_name' => 'orden_ordenes_orden_ordenes_1orden_ordenes_ida',
  'link' => 'orden_ordenes_orden_ordenes_1_right',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
