<?php
// created: 2017-07-20 22:48:10
$dictionary["Prospect"]["fields"]["prospects_accounts_1"] = array (
  'name' => 'prospects_accounts_1',
  'type' => 'link',
  'relationship' => 'prospects_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'prospects_accounts_1accounts_idb',
);
$dictionary["Prospect"]["fields"]["prospects_accounts_1_name"] = array (
  'name' => 'prospects_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'prospects_accounts_1accounts_idb',
  'link' => 'prospects_accounts_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["Prospect"]["fields"]["prospects_accounts_1accounts_idb"] = array (
  'name' => 'prospects_accounts_1accounts_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE_ID',
  'id_name' => 'prospects_accounts_1accounts_idb',
  'link' => 'prospects_accounts_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
