<?php
// created: 2017-02-23 18:33:18
$dictionary["lowae_autorizacion_especial"]["fields"]["lowae_autorizacion_especial_accounts_1"] = array (
  'name' => 'lowae_autorizacion_especial_accounts_1',
  'type' => 'link',
  'relationship' => 'lowae_autorizacion_especial_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ACCOUNTS_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'id_name' => 'lowae_auto6c11special_ida',
  'link-type' => 'many',
  'side' => 'left',
);
