<?php
// created: 2016-10-21 13:50:29
$dictionary["lowes_Pagos"]["fields"]["lowes_pagos_lf_facturas_1"] = array (
  'name' => 'lowes_pagos_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'lowes_pagos_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_LOWES_PAGOS_LF_FACTURAS_1_FROM_LOWES_PAGOS_TITLE',
  'id_name' => 'lowes_pagos_lf_facturas_1lowes_pagos_ida',
  'link-type' => 'many',
  'side' => 'left',
);
