<?php

// created: 2017-02-23 18:24:39
$dictionary["lowae_autorizacion_especial"]["fields"]["lowae_autorizacion_especial_orden_ordenes_1"] = array (
  'name' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'id_name' => 'lowae_auto0644special_ida',
  'link-type' => 'many',
  'side' => 'left',
);
