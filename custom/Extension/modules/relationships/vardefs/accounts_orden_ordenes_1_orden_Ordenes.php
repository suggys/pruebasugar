<?php
// created: 2016-10-11 21:41:46
$dictionary["orden_Ordenes"]["fields"]["accounts_orden_ordenes_1"] = array (
  'name' => 'accounts_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'accounts_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'accounts_orden_ordenes_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["orden_Ordenes"]["fields"]["accounts_orden_ordenes_1_name"] = array (
  'name' => 'accounts_orden_ordenes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_orden_ordenes_1accounts_ida',
  'link' => 'accounts_orden_ordenes_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["orden_Ordenes"]["fields"]["accounts_orden_ordenes_1accounts_ida"] = array (
  'name' => 'accounts_orden_ordenes_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID',
  'id_name' => 'accounts_orden_ordenes_1accounts_ida',
  'link' => 'accounts_orden_ordenes_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
