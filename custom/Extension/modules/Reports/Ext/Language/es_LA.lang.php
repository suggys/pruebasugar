<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente';
$mod_strings['LBL_ACCOUNTS'] = 'Clientes';
$mod_strings['LBL_ACCOUNT'] = 'Cliente';
$mod_strings['LBL_ACCOUNT_REPORTS'] = 'Informes de Clientes';
$mod_strings['LBL_MY_TEAM_ACCOUNT_REPORTS'] = 'Informes de Clientes de Mi Equipo';
$mod_strings['LBL_MY_ACCOUNT_REPORTS'] = 'Informes de Mis Clientes';
$mod_strings['LBL_PUBLISHED_ACCOUNT_REPORTS'] = 'Informes de Clientes Publicados';
$mod_strings['DEFAULT_REPORT_TITLE_3'] = 'Lista de Clientes de Socios';
$mod_strings['DEFAULT_REPORT_TITLE_4'] = 'Lista de Clientes de Clientes';
$mod_strings['DEFAULT_REPORT_TITLE_16'] = 'Clientes por Tipo por Industria';
$mod_strings['DEFAULT_REPORT_TITLE_43'] = 'Propietarios de Clientes Cliente';
$mod_strings['DEFAULT_REPORT_TITLE_44'] = 'Mis Nuevas Clientes Cliente';
$mod_strings['LBL_MY_TEAM_PROSPECT_REPORTS'] = 'Informes de Prospecto de Mi Equipo';
$mod_strings['LBL_PUBLISHED_PROSPECT_REPORTS'] = 'Informes de Prospecto Publicados';
$mod_strings['LBL_LEADS'] = 'Leads';
$mod_strings['LBL_LEAD'] = 'Lead';
$mod_strings['DEFAULT_REPORT_TITLE_18'] = 'Leads por Toma de Contacto';
