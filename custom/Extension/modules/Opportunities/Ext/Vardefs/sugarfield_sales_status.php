<?php
 // created: 2017-07-20 10:22:15
$dictionary['Opportunity']['fields']['sales_status']['audited']=false;
$dictionary['Opportunity']['fields']['sales_status']['massupdate']=false;
$dictionary['Opportunity']['fields']['sales_status']['importable']=false;
$dictionary['Opportunity']['fields']['sales_status']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sales_status']['reportable']=false;
$dictionary['Opportunity']['fields']['sales_status']['calculated']=false;
$dictionary['Opportunity']['fields']['sales_status']['dependency']=false;
$dictionary['Opportunity']['fields']['sales_status']['studio']=false;
$dictionary['Opportunity']['fields']['sales_status']['default']='activo';
$dictionary['Opportunity']['fields']['sales_status']['len']=100;
$dictionary['Opportunity']['fields']['sales_status']['options']='proyecto_estatus_list';

 ?>