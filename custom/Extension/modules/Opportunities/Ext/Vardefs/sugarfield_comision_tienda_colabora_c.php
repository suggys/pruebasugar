<?php
 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['comision_tienda_colabora_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['comision_tienda_colabora_c']['labelValue']='Comisión Tienda Colaboradora (%)';
$dictionary['Opportunity']['fields']['comision_tienda_colabora_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['comision_tienda_colabora_c']['enforced']='';
$dictionary['Opportunity']['fields']['comision_tienda_colabora_c']['dependency']='equal(true,false)';

 ?>