<?php
// created: 2017-07-20 15:21:47
$dictionary["Opportunity"]["fields"]["lo_obras_opportunities"] = array (
  'name' => 'lo_obras_opportunities',
  'type' => 'link',
  'relationship' => 'lo_obras_opportunities',
  'source' => 'non-db',
  'module' => 'LO_Obras',
  'bean_name' => 'LO_Obras',
  'side' => 'right',
  'vname' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'lo_obras_opportunitieslo_obras_ida',
  'link-type' => 'one',
);
$dictionary["Opportunity"]["fields"]["lo_obras_opportunities_name"] = array (
  'name' => 'lo_obras_opportunities_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_LO_OBRAS_TITLE',
  'save' => true,
  'id_name' => 'lo_obras_opportunitieslo_obras_ida',
  'link' => 'lo_obras_opportunities',
  'table' => 'lo_obras',
  'module' => 'LO_Obras',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["lo_obras_opportunitieslo_obras_ida"] = array (
  'name' => 'lo_obras_opportunitieslo_obras_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_OPPORTUNITIES_TITLE_ID',
  'id_name' => 'lo_obras_opportunitieslo_obras_ida',
  'link' => 'lo_obras_opportunities',
  'table' => 'lo_obras',
  'module' => 'LO_Obras',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
