<?php
 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['motivo_de_perdida_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['motivo_de_perdida_c']['labelValue']='Motivo de Pérdida';
$dictionary['Opportunity']['fields']['motivo_de_perdida_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['motivo_de_perdida_c']['enforced']='';
$dictionary['Opportunity']['fields']['motivo_de_perdida_c']['dependency']='equal("perdida",$etapa_c)';

 ?>