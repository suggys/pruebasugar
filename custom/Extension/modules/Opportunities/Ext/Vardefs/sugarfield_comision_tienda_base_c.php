<?php
 // created: 2017-08-23 18:56:05
$dictionary['Opportunity']['fields']['comision_tienda_base_c']['duplicate_merge_dom_value']=0;
$dictionary['Opportunity']['fields']['comision_tienda_base_c']['labelValue']='Comisión Tienda Base (%)';
$dictionary['Opportunity']['fields']['comision_tienda_base_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['comision_tienda_base_c']['enforced']='false';
$dictionary['Opportunity']['fields']['comision_tienda_base_c']['dependency']='equal(false,true)';

 ?>