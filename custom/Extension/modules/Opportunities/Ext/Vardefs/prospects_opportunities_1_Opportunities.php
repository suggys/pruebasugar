<?php
// created: 2017-07-14 22:54:49
$dictionary["Opportunity"]["fields"]["prospects_opportunities_1"] = array (
  'name' => 'prospects_opportunities_1',
  'type' => 'link',
  'relationship' => 'prospects_opportunities_1',
  'source' => 'non-db',
  'module' => 'Prospects',
  'bean_name' => 'Prospect',
  'side' => 'right',
  'vname' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'prospects_opportunities_1prospects_ida',
  'link-type' => 'one',
);
$dictionary["Opportunity"]["fields"]["prospects_opportunities_1_name"] = array (
  'name' => 'prospects_opportunities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_PROSPECTS_TITLE',
  'save' => true,
  'id_name' => 'prospects_opportunities_1prospects_ida',
  'link' => 'prospects_opportunities_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["prospects_opportunities_1prospects_ida"] = array (
  'name' => 'prospects_opportunities_1prospects_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE_ID',
  'id_name' => 'prospects_opportunities_1prospects_ida',
  'link' => 'prospects_opportunities_1',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
