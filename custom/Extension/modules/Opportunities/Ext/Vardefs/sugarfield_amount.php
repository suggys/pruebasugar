<?php
 // created: 2017-07-20 10:22:11
$dictionary['Opportunity']['fields']['amount']['required']=true;
$dictionary['Opportunity']['fields']['amount']['audited']=true;
$dictionary['Opportunity']['fields']['amount']['massupdate']=true;
$dictionary['Opportunity']['fields']['amount']['comments']='Unconverted amount of the opportunity';
$dictionary['Opportunity']['fields']['amount']['duplicate_merge']='enabled';
$dictionary['Opportunity']['fields']['amount']['duplicate_merge_dom_value']='1';
$dictionary['Opportunity']['fields']['amount']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['amount']['calculated']=false;
$dictionary['Opportunity']['fields']['amount']['default']='';

 ?>