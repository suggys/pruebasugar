<?php
 // created: 2017-07-17 21:02:43
$layout_defs["Opportunities"]["subpanel_setup"]['lo_obras_opportunities'] = array (
  'order' => 100,
  'module' => 'LO_Obras',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LO_OBRAS_OPPORTUNITIES_FROM_LO_OBRAS_TITLE',
  'get_subpanel_data' => 'lo_obras_opportunities',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
