<?php

$dependencies['Opportunities']['lo_obras_opportunities_name_required'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('lo_obras_opportunities_name'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'lo_obras_opportunities_name',
        'label' => 'lo_obras_opportunities_name_label',
        'value' => 'equal($lo_obras_opportunities_name,"")',
      ),
    ),
  ),
);
