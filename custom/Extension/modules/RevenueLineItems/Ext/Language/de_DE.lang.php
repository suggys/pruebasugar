<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_REVENUELINEITEM'] = 'Neuer Umsatzposten';
$mod_strings['LBL_MODULE_NAME'] = 'Umsatzposten';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Umsatzposten';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Neuer Umsatzposten';
$mod_strings['LNK_REVENUELINEITEM_LIST'] = 'Umsatzposten anzeigen';
$mod_strings['LNK_IMPORT_REVENUELINEITEMS'] = 'Umsatzposten importieren';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Umsatzposten-Liste';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Umsatzposten-Suche';
