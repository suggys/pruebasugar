<?php
 // created: 2017-07-18 00:29:44
$dictionary['LOW02_Partidas_Orden']['fields']['name']['len']='100';
$dictionary['LOW02_Partidas_Orden']['fields']['name']['audited']=false;
$dictionary['LOW02_Partidas_Orden']['fields']['name']['massupdate']=false;
$dictionary['LOW02_Partidas_Orden']['fields']['name']['unified_search']=false;
$dictionary['LOW02_Partidas_Orden']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['LOW02_Partidas_Orden']['fields']['name']['calculated']=false;

 ?>