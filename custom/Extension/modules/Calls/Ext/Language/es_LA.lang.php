<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['ERR_DELETE_RECORD'] = 'Debe especificar un número de registro para eliminar la Cliente.';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Cliente';
$mod_strings['LNK_SELECT_ACCOUNT'] = 'Seleccionar Cliente';
$mod_strings['LNK_NEW_ACCOUNT'] = 'Nueva Cliente';
$mod_strings['LBL_CREATE_LEAD'] = 'Nuevo Lead';
$mod_strings['LNK_NEW_OPPORTUNITY'] = 'Nuevo Proyecto';
$mod_strings['LBL_DESCRIPTION'] = 'Conclusión:';
