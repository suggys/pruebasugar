<?php
 // created: 2018-01-25 15:06:58
$dictionary['Call']['fields']['description']['audited']=false;
$dictionary['Call']['fields']['description']['massupdate']=false;
$dictionary['Call']['fields']['description']['comments']='Full text of the note';
$dictionary['Call']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Call']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Call']['fields']['description']['merge_filter']='disabled';
$dictionary['Call']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.54',
  'searchable' => true,
);
$dictionary['Call']['fields']['description']['calculated']=false;
$dictionary['Call']['fields']['description']['rows']='6';
$dictionary['Call']['fields']['description']['cols']='80';

 ?>