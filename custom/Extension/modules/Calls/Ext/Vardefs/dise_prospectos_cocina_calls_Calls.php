<?php
// created: 2018-02-01 19:07:34
$dictionary["Call"]["fields"]["dise_prospectos_cocina_calls"] = array (
  'name' => 'dise_prospectos_cocina_calls',
  'type' => 'link',
  'relationship' => 'dise_prospectos_cocina_calls',
  'source' => 'non-db',
  'module' => 'dise_Prospectos_cocina',
  'bean_name' => 'dise_Prospectos_cocina',
  'side' => 'right',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_CALLS_FROM_CALLS_TITLE',
  'id_name' => 'dise_prospectos_cocina_callsdise_prospectos_cocina_ida',
  'link-type' => 'one',
);
$dictionary["Call"]["fields"]["dise_prospectos_cocina_calls_name"] = array (
  'name' => 'dise_prospectos_cocina_calls_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_CALLS_FROM_DISE_PROSPECTOS_COCINA_TITLE',
  'save' => true,
  'id_name' => 'dise_prospectos_cocina_callsdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_calls',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'name',
);
$dictionary["Call"]["fields"]["dise_prospectos_cocina_callsdise_prospectos_cocina_ida"] = array (
  'name' => 'dise_prospectos_cocina_callsdise_prospectos_cocina_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_DISE_PROSPECTOS_COCINA_CALLS_FROM_CALLS_TITLE_ID',
  'id_name' => 'dise_prospectos_cocina_callsdise_prospectos_cocina_ida',
  'link' => 'dise_prospectos_cocina_calls',
  'table' => 'dise_prospectos_cocina',
  'module' => 'dise_Prospectos_cocina',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
