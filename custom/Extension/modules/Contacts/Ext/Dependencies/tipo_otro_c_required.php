<?php
$dependencies['Contacts']['tipo_otro_c_required'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'equal($tipo_c,"otro")',
  'triggerFields' => array('tipo_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'tipo_otro_c',
        'label'  => 'tipo_otro_c_label',
        'value' => 'true',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'tipo_otro_c',
        'label'  => 'tipo_otro_c_label',
        'value' => 'false',
      ),
    ),
  ),
);
