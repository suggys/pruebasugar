<?php
$dependencies['Contacts']['prospects_contacts_1_name_readonly'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'not(equal($account_name,""))',
  'triggerFields' => array('account_name'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'prospects_contacts_1_name',
        'label'  => 'prospects_contacts_1_name_label',
        'value' => 'true',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'prospects_contacts_1_name',
        'label'  => 'prospects_contacts_1_name_label',
        'value' => 'false',
      ),
    ),
  ),
);
