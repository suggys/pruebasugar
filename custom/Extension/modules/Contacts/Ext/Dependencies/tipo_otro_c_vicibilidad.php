<?php
$dependencies['Contacts']['tipo_otro_c_visibility'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'equal($tipo_c,"otro")',
  'triggerFields' => array('tipo_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'tipo_otro_c',
        'label'  => 'tipo_otro_c_label',
        'value' => 'true',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'tipo_otro_c',
        'label'  => 'tipo_otro_c_label',
        'value' => 'false',
      ),
    ),
  ),
);
