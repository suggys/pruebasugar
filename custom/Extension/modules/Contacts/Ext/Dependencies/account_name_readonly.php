<?php
$dependencies['Contacts']['account_name_readonly'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'not(equal($prospects_contacts_1_name,""))',
  'triggerFields' => array('prospects_contacts_1_name'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'account_name',
        'label'  => 'account_name_label',
        'value' => 'true',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'ReadOnly',
      'params' => array(
        'target' => 'account_name',
        'label'  => 'account_name_label',
        'value' => 'false',
      ),
    ),
  ),
);
