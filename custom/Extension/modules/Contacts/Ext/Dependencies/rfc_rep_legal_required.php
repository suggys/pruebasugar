<?php
$dependencies['Contacts']['rfc_rep_legal_c_required'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'equal($tipo_c,"representante_legal")',
  'triggerFields' => array('tipo_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'rfc_representante_legal_c',
        'label'  => 'rfc_representante_legal_c_label',
        'value' => 'true',
        'label' => 'Requerido',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'rfc_representante_legal_c',
        'label'  => 'rfc_representante_legal_c_label',
        'value' => 'false',
      ),
    ),
  ),
);
