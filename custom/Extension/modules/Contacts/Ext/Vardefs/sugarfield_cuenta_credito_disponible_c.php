<?php
 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_credito_disponible_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_credito_disponible_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_credito_disponible_c']['formula']='ifElse(equal(related($accounts,"estado_bloqueo_c"),"Bloqueado"),0,related($accounts,"credito_disponible_c"))';
$dictionary['Contact']['fields']['cuenta_credito_disponible_c']['enforced']='true';
$dictionary['Contact']['fields']['cuenta_credito_disponible_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>