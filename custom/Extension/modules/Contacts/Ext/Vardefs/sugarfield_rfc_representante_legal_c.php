<?php
 // created: 2017-08-11 21:47:21
$dictionary['Contact']['fields']['rfc_representante_legal_c']['labelValue']='RFC Representante Legal';
$dictionary['Contact']['fields']['rfc_representante_legal_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['rfc_representante_legal_c']['enforced']='';
$dictionary['Contact']['fields']['rfc_representante_legal_c']['dependency']='';
$dictionary['Contact']['fields']['rfc_representante_legal_c']['len']=13;

 ?>
