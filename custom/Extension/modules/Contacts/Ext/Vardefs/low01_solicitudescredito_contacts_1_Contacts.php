<?php
// created: 2017-10-12 22:45:07
$dictionary["Contact"]["fields"]["low01_solicitudescredito_contacts_1"] = array (
  'name' => 'low01_solicitudescredito_contacts_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_contacts_1',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => 'LOW01_SolicitudesCredito',
  'side' => 'right',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["low01_solicitudescredito_contacts_1_name"] = array (
  'name' => 'low01_solicitudescredito_contacts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'save' => true,
  'id_name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
  'link' => 'low01_solicitudescredito_contacts_1',
  'table' => 'low01_solicitudescredito',
  'module' => 'LOW01_SolicitudesCredito',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["low01_solicitudescredito_contacts_1low01_solicitudescredito_ida"] = array (
  'name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
  'link' => 'low01_solicitudescredito_contacts_1',
  'table' => 'low01_solicitudescredito',
  'module' => 'LOW01_SolicitudesCredito',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
