<?php
 // created: 2017-08-25 16:04:03
$dictionary['Contact']['fields']['portal_active']['default']=false;
$dictionary['Contact']['fields']['portal_active']['audited']=false;
$dictionary['Contact']['fields']['portal_active']['massupdate']=false;
$dictionary['Contact']['fields']['portal_active']['comments']='Indicator whether this contact is a portal user';
$dictionary['Contact']['fields']['portal_active']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['portal_active']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['portal_active']['merge_filter']='disabled';
$dictionary['Contact']['fields']['portal_active']['unified_search']=false;
$dictionary['Contact']['fields']['portal_active']['calculated']=false;
$dictionary['Contact']['fields']['portal_active']['dependency']='not(equal(related($accounts,"id_ar_credit_c"),""))';

 ?>