<?php
$dictionary['Contact']['duplicate_check']['FilterDuplicateCheck'] = array(
    'filter_template' => array(
        array(
            '$and' => array(
                array('first_name' => array('$equals' => '$first_name')),
                array('last_name' => array('$equals' => '$last_name')),
                array('apellido_materno_c' => array('$equals' => '$apellido_materno_c'))
            )
        ),
    ),
    'ranking_fields' => array(
        array('in_field_name' => 'first_name', 'dupe_field_name' => 'first_name'),
        array('in_field_name' => 'last_name', 'dupe_field_name' => 'last_name'),
        array('in_field_name' => 'apellido_materno_c', 'dupe_field_name' => 'apellido_materno_c'),
    )
);
