<?php
 // created: 2017-08-23 18:55:59
$dictionary['Contact']['fields']['cuenta_limite_credito_autorizado_c']['duplicate_merge_dom_value']=0;
$dictionary['Contact']['fields']['cuenta_limite_credito_autorizado_c']['calculated']=true;
$dictionary['Contact']['fields']['cuenta_limite_credito_autorizado_c']['formula']='related($accounts,"limite_credito_autorizado_c")';
$dictionary['Contact']['fields']['cuenta_limite_credito_autorizado_c']['enforced']='true';
$dictionary['Contact']['fields']['cuenta_limite_credito_autorizado_c']['related_fields']=array (
  0 => 'currency_id',
  1 => 'base_rate',
);

 ?>