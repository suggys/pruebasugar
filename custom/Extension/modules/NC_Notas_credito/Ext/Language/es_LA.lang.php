<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_FOLIO_FACTURA_C'] = 'Folio Factura';
$mod_strings['LBL_UUID'] = 'Identificador Único (UUID)';
$mod_strings['LBL_SERIES_ID'] = 'Serie';
$mod_strings['LBL_RFC_E'] = 'RFC Emisor';
$mod_strings['LBL_NOMBRE_E'] = 'Nombre Emisor';
$mod_strings['LBL_RFC_R'] = 'RFC Receptor';
$mod_strings['LBL_NAME_R'] = 'Nombre Receptor';
$mod_strings['LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_LF_FACTURAS_TITLE'] = 'Facturas / Notas de cargo';
$mod_strings['LBL_NO_TICKET_C'] = 'Número de Ticket';
$mod_strings['LBL_NAME'] = 'Folio';
$mod_strings['LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE'] = 'Cliente';
$mod_strings['LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE'] = 'Orden';
$mod_strings['LBL_SUB_TOTAL_C'] = 'Subtotal';
$mod_strings['LBL_IMPORTADO'] = 'Registro Importado';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'Saldos';
$mod_strings['LBL_RECORD_BODY'] = 'Información General';
$mod_strings['LBL_SHOW_MORE'] = 'Información del Sistema';
$mod_strings['LBL_DESCRIPTION'] = 'Comentarios';
$mod_strings['LBL_MODULE_NAME'] = 'Notas de Crédito';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Nota de Crédito';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Información de Sistema';
$mod_strings['LBL_IMPORTADODE'] = 'Archivo de origen';
