<?php
 // created: 2016-12-14 01:17:29
$dictionary['NC_Notas_credito']['fields']['name']['len']='255';
$dictionary['NC_Notas_credito']['fields']['name']['audited']=false;
$dictionary['NC_Notas_credito']['fields']['name']['massupdate']=false;
$dictionary['NC_Notas_credito']['fields']['name']['unified_search']=false;
$dictionary['NC_Notas_credito']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['NC_Notas_credito']['fields']['name']['calculated']=false;

 ?>