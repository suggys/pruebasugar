<?php
// created: 2016-10-12 00:13:37
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_accounts"] = array (
  'name' => 'nc_notas_credito_accounts',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE',
  'id_name' => 'nc_notas_credito_accountsaccounts_ida',
  'link-type' => 'one',
);
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_accounts_name"] = array (
  'name' => 'nc_notas_credito_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'nc_notas_credito_accountsaccounts_ida',
  'link' => 'nc_notas_credito_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_accountsaccounts_ida"] = array (
  'name' => 'nc_notas_credito_accountsaccounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ACCOUNTS_FROM_NC_NOTAS_CREDITO_TITLE_ID',
  'id_name' => 'nc_notas_credito_accountsaccounts_ida',
  'link' => 'nc_notas_credito_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
