<?php
// created: 2016-10-12 00:13:37
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_orden_ordenes"] = array (
  'name' => 'nc_notas_credito_orden_ordenes',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_orden_ordenes',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
);
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_orden_ordenes_name"] = array (
  'name' => 'nc_notas_credito_orden_ordenes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'save' => true,
  'id_name' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
  'link' => 'nc_notas_credito_orden_ordenes',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'name',
);
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_orden_ordenesorden_ordenes_idb"] = array (
  'name' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_NC_NOTAS_CREDITO_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE_ID',
  'id_name' => 'nc_notas_credito_orden_ordenesorden_ordenes_idb',
  'link' => 'nc_notas_credito_orden_ordenes',
  'table' => 'orden_ordenes',
  'module' => 'orden_Ordenes',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
