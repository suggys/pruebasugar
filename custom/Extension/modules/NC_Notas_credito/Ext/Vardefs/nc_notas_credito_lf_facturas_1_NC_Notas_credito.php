<?php
// created: 2016-10-20 16:38:00
$dictionary["NC_Notas_credito"]["fields"]["nc_notas_credito_lf_facturas_1"] = array (
  'name' => 'nc_notas_credito_lf_facturas_1',
  'type' => 'link',
  'relationship' => 'nc_notas_credito_lf_facturas_1',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_NC_NOTAS_CREDITO_LF_FACTURAS_1_FROM_NC_NOTAS_CREDITO_TITLE',
  'id_name' => 'nc_notas_credito_lf_facturas_1nc_notas_credito_ida',
  'link-type' => 'many',
  'side' => 'left',
);
