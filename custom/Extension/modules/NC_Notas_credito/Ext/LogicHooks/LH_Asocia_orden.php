<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();

$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(1, 'LH asocia Nota Credito con Orden', 'custom/modules/NC_Notas_credito/LHAsociaNCOrdenFacturaCta.php', 'LHAsociaNCOrdenFacturaCta','asociaNCOrdenFacturaCta');


$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(1, 'Relaciona Factura con Nota de credito', 'custom/modules/NC_Notas_credito/LHAsociaNCOrdenFacturaCta.php', 'LHAsociaNCOrdenFacturaCta','asociaNCFactura');

$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(1, 'Ejecuta tareas despues de relacionar', 'custom/modules/NC_Notas_credito/LHAsociaNCOrdenFacturaCta.php', 'LHAsociaNCOrdenFacturaCta','afterRelationshipAdd');


?>
