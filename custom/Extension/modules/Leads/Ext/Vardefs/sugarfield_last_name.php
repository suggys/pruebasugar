<?php
 // created: 2018-05-11 13:59:06
$dictionary['Lead']['fields']['last_name']['required']=true;
$dictionary['Lead']['fields']['last_name']['audited']=false;
$dictionary['Lead']['fields']['last_name']['massupdate']=false;
$dictionary['Lead']['fields']['last_name']['comments']='';
$dictionary['Lead']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['last_name']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.85',
  'searchable' => true,
);
$dictionary['Lead']['fields']['last_name']['calculated']=false;
$dictionary['Lead']['fields']['last_name']['len']='80';

 ?>