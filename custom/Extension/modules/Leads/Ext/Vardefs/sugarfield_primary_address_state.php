<?php
 // created: 2017-08-06 00:18:00
$dictionary['Lead']['fields']['primary_address_state']['audited']=false;
$dictionary['Lead']['fields']['primary_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_state']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_state']['len']=100;
$dictionary['Lead']['fields']['primary_address_state']['dependency']=false;
$dictionary['Lead']['fields']['primary_address_state']['visibility_grid']=array (
  'trigger' => 'primary_address_country',
  'values' => 
  array (
    '' => 
    array (
      0 => '',
    ),
    'US' => 
    array (
      0 => 'OTRO',
    ),
    'CA' => 
    array (
      0 => 'OTRO',
    ),
    'FR' => 
    array (
      0 => 'OTRO',
    ),
    'MX' => 
    array (
      0 => 'AGU',
      1 => 'OTRO',
      2 => 'BCN',
      3 => 'BCS',
      4 => 'CAM',
      5 => 'CHP',
      6 => 'CHH',
      7 => 'CMX',
      8 => 'COL',
      9 => 'DUR',
      10 => 'MEX',
      11 => 'GUA',
      12 => 'GRO',
      13 => 'HID',
      14 => 'JAL',
      15 => 'MIC',
      16 => 'MOR',
      17 => 'NAY',
      18 => 'NLE',
      19 => 'OAX',
      20 => 'PUE',
      21 => 'QUE',
      22 => 'ROO',
      23 => 'SLP',
      24 => 'SIN',
      25 => 'SON',
      26 => 'TAB',
      27 => 'TAM',
      28 => 'TLA',
      29 => 'VER',
      30 => 'YUC',
      31 => 'ZAC',
    ),
    'EU' => 
    array (
      0 => 'OTRO',
    ),
    'DE' => 
    array (
      0 => 'OTRO',
    ),
    'GB' => 
    array (
      0 => 'OTRO',
    ),
    'JP' => 
    array (
      0 => 'OTRO',
    ),
    'PR' => 
    array (
      0 => 'OTRO',
    ),
  ),
);

 ?>