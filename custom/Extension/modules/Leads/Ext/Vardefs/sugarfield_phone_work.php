<?php
 // created: 2017-08-02 21:21:08
$dictionary['Lead']['fields']['phone_work']['type']='varchar';
$dictionary['Lead']['fields']['phone_work']['len']='13';
$dictionary['Lead']['fields']['phone_work']['massupdate']=false;
$dictionary['Lead']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_work']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_work']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_work']['calculated']=false;
$dictionary['Lead']['fields']['phone_work']['enable_range_search']=false;
$dictionary['Lead']['fields']['phone_work']['min']=false;
$dictionary['Lead']['fields']['phone_work']['max']=false;
$dictionary['Lead']['fields']['phone_work']['disable_num_format']='1';

 ?>
