<?php
 // created: 2017-07-20 21:34:16
$dictionary['Lead']['fields']['website']['len']='255';
$dictionary['Lead']['fields']['website']['audited']=false;
$dictionary['Lead']['fields']['website']['massupdate']=false;
$dictionary['Lead']['fields']['website']['comments']='URL of website for the company';
$dictionary['Lead']['fields']['website']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['website']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['website']['merge_filter']='disabled';
$dictionary['Lead']['fields']['website']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['website']['calculated']=false;
$dictionary['Lead']['fields']['website']['gen']='';

 ?>