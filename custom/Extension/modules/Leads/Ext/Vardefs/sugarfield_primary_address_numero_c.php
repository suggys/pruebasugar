<?php
 // created: 2018-06-05 04:25:46
$dictionary['Lead']['fields']['primary_address_numero_c']['labelValue']='Número';
$dictionary['Lead']['fields']['primary_address_numero_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_numero_c']['enforced']='';
$dictionary['Lead']['fields']['primary_address_numero_c']['dependency']='equal($status,"Converted")';

 ?>