<?php
 // created: 2017-08-04 14:45:22
$dictionary['Lead']['fields']['email']['len']='100';
$dictionary['Lead']['fields']['email']['required']=false;
$dictionary['Lead']['fields']['email']['audited']=false;
$dictionary['Lead']['fields']['email']['massupdate']=true;
$dictionary['Lead']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['email']['merge_filter']='disabled';
$dictionary['Lead']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.83',
  'searchable' => true,
);
$dictionary['Lead']['fields']['email']['calculated']=false;

 ?>