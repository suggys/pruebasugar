<?php
 // created: 2018-06-05 04:25:12
$dictionary['Lead']['fields']['primary_address_colonia_c']['labelValue']='Colonia';
$dictionary['Lead']['fields']['primary_address_colonia_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_colonia_c']['enforced']='';
$dictionary['Lead']['fields']['primary_address_colonia_c']['dependency']='equal($status,"Converted")';

 ?>