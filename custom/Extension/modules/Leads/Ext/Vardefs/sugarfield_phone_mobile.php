<?php
 // created: 2017-08-04 14:44:26
$dictionary['Lead']['fields']['phone_mobile']['type']='varchar';
$dictionary['Lead']['fields']['phone_mobile']['len']='10';
$dictionary['Lead']['fields']['phone_mobile']['required']=false;
$dictionary['Lead']['fields']['phone_mobile']['audited']=false;
$dictionary['Lead']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Lead']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Lead']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.01',
  'searchable' => true,
);
$dictionary['Lead']['fields']['phone_mobile']['calculated']=false;
$dictionary['Lead']['fields']['phone_mobile']['enable_range_search']=false;
$dictionary['Lead']['fields']['phone_mobile']['min']=false;
$dictionary['Lead']['fields']['phone_mobile']['max']=false;
$dictionary['Lead']['fields']['phone_mobile']['disable_num_format']='1';

 ?>
