<?php
 // created: 2018-06-05 04:23:10
$dictionary['Lead']['fields']['primary_address_street']['required']=true;
$dictionary['Lead']['fields']['primary_address_street']['audited']=false;
$dictionary['Lead']['fields']['primary_address_street']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_street']['comments']='The street address used for primary address';
$dictionary['Lead']['fields']['primary_address_street']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.31',
  'searchable' => true,
);
$dictionary['Lead']['fields']['primary_address_street']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_street']['dependency']='equal($status,"Converted")';
$dictionary['Lead']['fields']['primary_address_street']['rows']='4';
$dictionary['Lead']['fields']['primary_address_street']['cols']='20';

 ?>