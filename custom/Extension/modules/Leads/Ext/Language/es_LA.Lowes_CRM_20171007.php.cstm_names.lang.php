<?php
$mod_strings['LBL_CONTACTO_C'] = 'Contacto';
$mod_strings['LBL_LEAD_STATUS_C'] = 'Estado de la cuenta';
$mod_strings['LBL_LEADS_APELLIDO_MATERNO_C'] = 'Apellido Materno';
$mod_strings['LBL_LEADS_FOLIO_C'] = 'Folio';
$mod_strings['LBL_PROYECTO_C'] = 'Proyecto';
$mod_strings['LBL_PROYECTO_ID_C'] = 'ID Proyecto';
$mod_strings['LBL_SUCURSAL_C'] = 'Sucursal';
$mod_strings['LBL_PRIMARY_ADDRESS_COLONIA_C'] = 'Colonia';
$mod_strings['LBL_PRIMARY_ADDRESS_NUMERO_C'] = 'Número';
$mod_strings['LBL_RFC_C'] = 'RFC';
$mod_strings['LBL_TIPO_PERSONA_C'] = 'Tipo de persona';
