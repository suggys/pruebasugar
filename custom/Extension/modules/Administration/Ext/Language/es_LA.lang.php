<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_OPPORTUNITIES_DESC'] = 'Configurar el administrador para el módulo de Proyectos. Los Ajustes de Proyectos incluyen vista de Proyectos sólo por la Oportunidad, o por Proyectos más los Elementos de la línea de Ingresos que se adjuntan.';
$mod_strings['LBL_MANAGE_OPPORTUNITIES_TITLE'] = 'Proyectos';
$mod_strings['LBL_MANAGE_OPPORTUNITIES_DESC'] = 'Configurar Módulo de Proyectos';
