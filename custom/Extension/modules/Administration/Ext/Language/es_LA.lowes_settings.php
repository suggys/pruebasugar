<?php


  // Authentication Manager
  $mod_strings['LBL_LOWES_SETTINGS'] = 'Configuración Lowes';
  $mod_strings['LBL_LOWES_SETTINGS_DESCRIPTION'] = 'Valores de configuración para Lowes';
  $mod_strings['LBL_LOWES_SETTINGS_MANAGEMENT'] = 'Lowes information management';
  $mod_strings['LBL_CANCEL_BUTTON_TITLE'] = 'Cancelar';
  $mod_strings['LBL_SAVE_BUTTON_TITLE'] = 'Guardar';
  $mod_strings['LBL_FOLIO_KONESH'] = 'Folio Konesh';
  $mod_strings['LBL_MONTO_MAXIMO_PROYECTOS'] = 'Monto máximo proyectos';
  $mod_strings['LBL_LEADS_CONSECUTIVO'] = 'Folio Leads';
  $mod_strings['LBL_PROSPECTS_CONSECUTIVO'] = 'Folio Prospectos';
  $mod_strings['LBL_CLIENTES_CONSECUTIVO'] = 'Folio Clientes';
  $mod_strings['LBL_PDF_MANAGER_SOLICITUD_CREDITO'] = 'PDF para descarga de Solicitud de Crédito';
  $mod_strings['LBL_ACTIVIDAD_CLIENTE'] = 'Actividad Cliente';
  $mod_strings['LBL_EMAILTEMPLATE_MONTO_MAXIMO_PROYE'] = 'EmailTemplate para Monto Máximo de Proyectos';
  $mod_strings['LBL_EMAILTEMPLATE_NOTIFICA_EXTERNO'] = 'EmailTemplate para notificar al agente externo';
  $mod_strings['LBL_EMAILTEMPLATE_NOTIFICA_RECHAZO'] = 'EmailTemplate para notificar rechazo de solicitud';
  $mod_strings['LBL_MONTO_MAXIMO_GC'] = 'Monto de aprobación de Gerente de Crédito';
  $mod_strings['LBL_MONTO_MAXIMO_DF'] = 'Monto de aprobación de Director de Finanzas';
  $mod_strings['LBL_EMAILTEMP_NOTIFICA_APROB'] = 'EmailTemplate para notificar aprobación de solicitud';
  $mod_strings['LBL_PDF_MANAGER_ESTADO_CUENTA_PORTAL'] = 'PDF de estado de cuenta del portal de clientes';
  $mod_strings['LBL_EMAIL_BIENVENIDA_CLI'] = 'EmailTemplate para bienvenida de clientes';
  $mod_strings['LBL_EMAIL_FORMULARIO_SOL_CRED'] = 'Email para Formulario de Crédito';
  $mod_strings['LBL_EMAIL_EVALUA_SOL_CRED'] = 'Email para Evaluación de Solicitud de Crédito por Asesor Comercial';
  $mod_strings['LBL_ID_POS'] = 'Secuencia de ID POS';
  // $mod_strings['AUTHENTICATION_SETTINGS'] = 'Authentication information';
  // $mod_strings['USER'] = 'User';
  // $mod_strings['PASSWORD'] = 'Password';
