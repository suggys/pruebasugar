<?php
 // created: 2017-07-16 19:22:10
$layout_defs["orden_Ordenes"]["subpanel_setup"]['low02_partidas_orden_orden_ordenes'] = array (
  'order' => 100,
  'module' => 'LOW02_Partidas_Orden',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_LOW02_PARTIDAS_ORDEN_TITLE',
  'get_subpanel_data' => 'low02_partidas_orden_orden_ordenes',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
