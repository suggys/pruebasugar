<?php
// created: 2017-02-23 18:24:39
$dictionary["orden_Ordenes"]["fields"]["lowae_autorizacion_especial_orden_ordenes_1"] = array (
  'name' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'lowae_autorizacion_especial',
  'bean_name' => 'lowae_autorizacion_especial',
  'side' => 'right',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'lowae_auto0644special_ida',
  'link-type' => 'one',
);
$dictionary["orden_Ordenes"]["fields"]["lowae_autorizacion_especial_orden_ordenes_1_name"] = array (
  'name' => 'lowae_autorizacion_especial_orden_ordenes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_LOWAE_AUTORIZACION_ESPECIAL_TITLE',
  'save' => true,
  'id_name' => 'lowae_auto0644special_ida',
  'link' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'table' => 'lowae_autorizacion_especial',
  'module' => 'lowae_autorizacion_especial',
  'rname' => 'name',
);
$dictionary["orden_Ordenes"]["fields"]["lowae_auto0644special_ida"] = array (
  'name' => 'lowae_auto0644special_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOWAE_AUTORIZACION_ESPECIAL_ORDEN_ORDENES_1_FROM_ORDEN_ORDENES_TITLE_ID',
  'id_name' => 'lowae_auto0644special_ida',
  'link' => 'lowae_autorizacion_especial_orden_ordenes_1',
  'table' => 'lowae_autorizacion_especial',
  'module' => 'lowae_autorizacion_especial',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
