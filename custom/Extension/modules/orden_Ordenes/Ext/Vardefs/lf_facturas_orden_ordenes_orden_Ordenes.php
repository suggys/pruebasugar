<?php
// created: 2016-10-19 00:31:50
$dictionary["orden_Ordenes"]["fields"]["lf_facturas_orden_ordenes"] = array (
  'name' => 'lf_facturas_orden_ordenes',
  'type' => 'link',
  'relationship' => 'lf_facturas_orden_ordenes',
  'source' => 'non-db',
  'module' => 'LF_Facturas',
  'bean_name' => 'LF_Facturas',
  'vname' => 'LBL_LF_FACTURAS_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
  'link-type' => 'many',
  'side' => 'left',
);
