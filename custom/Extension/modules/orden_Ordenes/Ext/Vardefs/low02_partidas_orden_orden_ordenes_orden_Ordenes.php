<?php
// created: 2017-07-16 19:22:10
$dictionary["orden_Ordenes"]["fields"]["low02_partidas_orden_orden_ordenes"] = array (
  'name' => 'low02_partidas_orden_orden_ordenes',
  'type' => 'link',
  'relationship' => 'low02_partidas_orden_orden_ordenes',
  'source' => 'non-db',
  'module' => 'LOW02_Partidas_Orden',
  'bean_name' => false,
  'vname' => 'LBL_LOW02_PARTIDAS_ORDEN_ORDEN_ORDENES_FROM_ORDEN_ORDENES_TITLE',
  'id_name' => 'low02_partidas_orden_orden_ordenesorden_ordenes_ida',
  'link-type' => 'many',
  'side' => 'left',
);
