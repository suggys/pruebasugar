<?php
    $hook_version = 1;
    $hook_array = isset($hook_array) ? $hook_array : array();
    $hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
    $hook_array['after_save'][] = array(
        1,
        'FR-20.2. Vinculación de órdenes automática',
        'custom/modules/orden_Ordenes/VinculacionAutomaticaDeOrdenes.php',
        'VinculacionAutomaticaDeOrdenes',
        'vinculaAutomaticaDeOrdenes'
    );
?>