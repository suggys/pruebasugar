<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
    2,
    'Calcular el crédito disponible de la cuenta Cliente Crédito AR',
    'custom/modules/orden_Ordenes/CalcularCreditoDisponibleDeClienteCreditoAR.php',
    'CalcularCreditoDisponibleDeClienteCreditoAR',
    'CalcularCreditoDisponibleDeClienteCreditoAR'
);
?>