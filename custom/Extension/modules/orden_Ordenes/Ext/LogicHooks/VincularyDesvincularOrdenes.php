<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
    1,
    'Vincular una Orden',
    'custom/modules/orden_Ordenes/VincularyDesvincularOrdenes.php',
    'VincularyDesvincularOrdenes',
    'vincularOrdenes'
);
$hook_array['after_relationship_delete'] = isset($hook_array['after_relationship_delete']) ? $hook_array['after_relationship_delete'] : array();
$hook_array['after_relationship_delete'][] = array(
    1,
    'Desvincular una Orden',
    'custom/modules/orden_Ordenes/VincularyDesvincularOrdenes.php',
    'VincularyDesvincularOrdenes',
    'desvincularOrdenes'
);
?>