<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
	1, 
	'Establece monto por entrega de una Orden Iniciada o Completada', 
	'custom/modules/orden_Ordenes/EstableceMontoPorEntregarDeOrden.php',
    'EstableceMontoPorEntregarDeOrden',
    'estableceMontoPorEntregar'
);

$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
	3, 
	'After save', 
	'custom/modules/orden_Ordenes/EstableceMontoPorEntregarDeOrden.php',
    'EstableceMontoPorEntregarDeOrden',
    'afterSave'
);
?>