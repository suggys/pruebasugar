<?php
 // created: 2017-08-15 23:00:31
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['importable']='false';
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['duplicate_merge']='disabled';
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['calculated']='true';
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['formula']='related($created_by_link,"full_name")';
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['enforced']=true;
$dictionary['LOW01_SolicitudesCredito']['fields']['promotor']['len']='50';
 ?>
