<?php
// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_activities_meetings"] = array (
  'name' => 'low01_solicitudescredito_activities_meetings',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_meetings',
  'source' => 'non-db',
  'module' => 'Meetings',
  'bean_name' => 'Meeting',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_MEETINGS_FROM_MEETINGS_TITLE',
);
