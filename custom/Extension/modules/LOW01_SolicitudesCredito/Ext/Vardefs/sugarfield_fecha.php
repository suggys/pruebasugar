<?php
 // created: 2017-08-11 16:45:56
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['massupdate']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['importable']='false';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['duplicate_merge']='disabled';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['calculated']='true';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['formula']='now()';
$dictionary['LOW01_SolicitudesCredito']['fields']['fecha']['enforced']=true;

 ?>