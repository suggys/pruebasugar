<?php
 // created: 2017-08-08 03:36:46
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['len']='85';
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['required']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['audited']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['massupdate']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['unified_search']=false;
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['name']['calculated']=false;

 ?>
