<?php
// created: 2017-10-17 01:51:44
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_low03_agentesexternos_1"] = array (
  'name' => 'low01_solicitudescredito_low03_agentesexternos_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_low03_agentesexternos_1',
  'source' => 'non-db',
  'module' => 'Low03_AgentesExternos',
  'bean_name' => 'Low03_AgentesExternos',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_LOW03_AGENTESEXTERNOS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'id_name' => 'low01_soliaf8bcredito_ida',
  'link-type' => 'many',
  'side' => 'left',
);
