<?php
 // created: 2017-08-23 18:56:02
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['labelValue']='Comentarios Gerente Ventas Comercial';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobado_por_Gerente_de_Ventas_Comerciales","Rechazado_por_Gerente_de_Ventas_Comerciales","Pendiente_por_Analista_de_Credito","Aprobado_por_Analista_de_Credito","Rechazado_por_Analista_de_Credito","Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Pendiente_por_Agente_Externo","Pendiente_por_Administrador_de_Credito","Aprobacion_por_Administrador_de_Credito","Rechazado_por_Administrador_de_Credito","Pendiente_por_Gerente_de_Credito","Aprobacion_por_Gerente_de_Credito","Rechazado_por_Gerente_de_Credito","Pendiente_por_Director_de_Finanzas","Aprobacion_por_Director_de_Finanzas","Rechazado_por_Director_de_Finanzas","Pendiente_por_Vicepresidencia_Comercial","Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['required']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_vtas_com_c']['audited']=1;

 ?>
