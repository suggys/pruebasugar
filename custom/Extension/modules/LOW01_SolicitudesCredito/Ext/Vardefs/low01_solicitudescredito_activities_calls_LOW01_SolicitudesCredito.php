<?php
// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_activities_calls"] = array (
  'name' => 'low01_solicitudescredito_activities_calls',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_calls',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_CALLS_FROM_CALLS_TITLE',
);
