<?php
// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_prospects"] = array (
  'name' => 'low01_solicitudescredito_prospects',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_prospects',
  'source' => 'non-db',
  'module' => 'Prospects',
  'bean_name' => 'Prospect',
  'side' => 'right',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'id_name' => 'low01_solicitudescredito_prospectsprospects_ida',
  'link-type' => 'one',
);
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_prospects_name"] = array (
  'name' => 'low01_solicitudescredito_prospects_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE',
  'save' => true,
  'id_name' => 'low01_solicitudescredito_prospectsprospects_ida',
  'link' => 'low01_solicitudescredito_prospects',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'account_name',
);
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_prospectsprospects_ida"] = array (
  'name' => 'low01_solicitudescredito_prospectsprospects_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE_ID',
  'id_name' => 'low01_solicitudescredito_prospectsprospects_ida',
  'link' => 'low01_solicitudescredito_prospects',
  'table' => 'prospects',
  'module' => 'Prospects',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
