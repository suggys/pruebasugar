<?php
// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_activities_emails"] = array (
  'name' => 'low01_solicitudescredito_activities_emails',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_emails',
  'source' => 'non-db',
  'module' => 'Emails',
  'bean_name' => 'Email',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_EMAILS_FROM_EMAILS_TITLE',
);
