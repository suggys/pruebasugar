<?php
// created: 2017-10-12 22:45:07
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_contacts_1"] = array (
  'name' => 'low01_solicitudescredito_contacts_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'id_name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
  'link-type' => 'many',
  'side' => 'left',
);
