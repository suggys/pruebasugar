<?php
 // created: 2017-08-21 16:12:53
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['importable']='false';
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['duplicate_merge']='disabled';
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['calculated']='1';
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['formula']='getDropdownValue("sucursal_c_list",related($created_by_link,"sucursal_c"))';
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['enforced']=true;
$dictionary['LOW01_SolicitudesCredito']['fields']['tienda']['len']='50';
 ?>
