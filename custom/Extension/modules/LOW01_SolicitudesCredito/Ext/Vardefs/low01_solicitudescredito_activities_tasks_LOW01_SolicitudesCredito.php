<?php
// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_activities_tasks"] = array (
  'name' => 'low01_solicitudescredito_activities_tasks',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_activities_tasks',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACTIVITIES_TASKS_FROM_TASKS_TITLE',
);
