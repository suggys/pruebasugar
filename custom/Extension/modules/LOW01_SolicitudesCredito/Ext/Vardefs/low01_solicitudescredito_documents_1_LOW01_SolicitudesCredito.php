<?php
// created: 2017-07-26 16:30:16
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_documents_1"] = array (
  'name' => 'low01_solicitudescredito_documents_1',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_DOCUMENTS_1_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'id_name' => 'low01_solicitudescredito_documents_1low01_solicitudescredito_ida',
  'link-type' => 'many',
  'side' => 'left',
);
