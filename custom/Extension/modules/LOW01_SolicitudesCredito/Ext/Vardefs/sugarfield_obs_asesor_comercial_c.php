<?php
 // created: 2018-09-17 21:19:42
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_asesor_comercial_c']['labelValue']='Comentario Asesor Comercial';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_asesor_comercial_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_asesor_comercial_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_asesor_comercial_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobado_por_Asesor_Comercial","Rechazado_por_Asesor_Comercial","Pendiente_por_Gerente_de_Ventas_Comerciales","Aprobado_por_Gerente_de_Ventas_Comerciales","Rechazado_por_Gerente_de_Ventas_Comerciales","Pendiente_por_Analista_de_Credito","Aprobado_por_Analista_de_Credito","Rechazado_por_Analista_de_Credito","Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito","Pendiente_por_Agente_Externo","Pendiente_por_Administrador_de_Credito","Aprobacion_por_Administrador_de_Credito","Rechazado_por_Administrador_de_Credito","Pendiente_por_Gerente_de_Credito","Aprobacion_por_Gerente_de_Credito","Rechazado_por_Gerente_de_Credito","Pendiente_por_Director_de_Finanzas","Aprobacion_por_Director_de_Finanzas","Rechazado_por_Director_de_Finanzas","Pendiente_por_Vicepresidencia_Comercial","Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';

 ?>