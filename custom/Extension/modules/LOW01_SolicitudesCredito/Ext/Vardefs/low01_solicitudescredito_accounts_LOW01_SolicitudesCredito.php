<?php
// created: 2017-07-21 12:47:08
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_accounts"] = array (
  'name' => 'low01_solicitudescredito_accounts',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'id_name' => 'low01_solicitudescredito_accountsaccounts_ida',
  'link-type' => 'one',
);
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_accounts_name"] = array (
  'name' => 'low01_solicitudescredito_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'low01_solicitudescredito_accountsaccounts_ida',
  'link' => 'low01_solicitudescredito_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["LOW01_SolicitudesCredito"]["fields"]["low01_solicitudescredito_accountsaccounts_ida"] = array (
  'name' => 'low01_solicitudescredito_accountsaccounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_ACCOUNTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE_ID',
  'id_name' => 'low01_solicitudescredito_accountsaccounts_ida',
  'link' => 'low01_solicitudescredito_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
