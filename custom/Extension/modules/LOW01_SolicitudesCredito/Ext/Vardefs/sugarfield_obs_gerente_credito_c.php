<?php
 // created: 2017-08-23 18:56:03
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['duplicate_merge_dom_value']=0;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['labelValue']='Comentarios Gerente de Crédito';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['enforced']='';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['dependency']='isInList($estado_solicitud_c,createList("Aprobacion_por_Gerente_de_Credito","Rechazado_por_Gerente_de_Credito","Pendiente_por_Director_de_Finanzas","Aprobacion_por_Director_de_Finanzas","Rechazado_por_Director_de_Finanzas","Pendiente_por_Vicepresidencia_Comercial","Aprobacion_por_Vicepresidencia_Comercial","Rechazado_por_Vicepresidencia_Comercial"))';
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['required']=1;
$dictionary['LOW01_SolicitudesCredito']['fields']['obs_gerente_credito_c']['audited']=1;

 ?>
