<?php
 // created: 2017-10-12 22:45:07
$layout_defs["LOW01_SolicitudesCredito"]["subpanel_setup"]['low01_solicitudescredito_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_contacts_1',
  'top_buttons' =>
  array (
    0 =>
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 =>
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
