<?php
$dependencies['LOW01_SolicitudesCredito']['folio_bc'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal("1", "1")',
    'triggerFields' => array(''),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'folio_bc',
              'value' => 'true', //Formula
          ),
      ),
    ),
);
