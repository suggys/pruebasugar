<?php
$dependencies['LOW01_SolicitudesCredito']['low01_solicitudescredito_accounts_name_deps'] = array(
  'hooks' => array("edit","view"),
  'triggerFields' => array('low01_solicitudescredito_accounts_name'),
  'onload' => true,
  //Actions is a list of actions to fire when the trigger is true
  'actions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'low01_solicitudescredito_accounts_name',
        'value' => 'not(equal($low01_solicitudescredito_accounts_name,""))',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetVisibility',
      'params' => array(
        'target' => 'low01_solicitudescredito_accounts_name',
        'value' => 'false',
      ),
    ),
  ),
);
