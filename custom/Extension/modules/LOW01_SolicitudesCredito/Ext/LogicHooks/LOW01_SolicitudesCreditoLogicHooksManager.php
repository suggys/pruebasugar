<?php
//$hook_array['after_save'][] = array(1,'AfterSave LH','custom/modules/LOW01_SolicitudesCredito/LOW01_SolicitudesCreditoLogicHooksManager.php','LOW01_SolicitudesCreditoLogicHooksManager','AfterSave',);

$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();

$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
    1,
    'BeforeSave LH',
    'custom/modules/LOW01_SolicitudesCredito/LOW01_SolicitudesCreditoLogicHooksManager.php',
    'LOW01_SolicitudesCreditoLogicHooksManager',
    'beforesave'
);

$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
    1,
    'AfterSave LH',
    'custom/modules/LOW01_SolicitudesCredito/LOW01_SolicitudesCreditoLogicHooksManager.php',
    'LOW01_SolicitudesCreditoLogicHooksManager',
    'AfterSave'
);
