<?php
$hook_version = 1;
$hook_array = isset($hook_array) ? $hook_array : array();
$hook_array['before_save'] = isset($hook_array['before_save']) ? $hook_array['before_save'] : array();
$hook_array['before_save'][] = array(
  1,
  'Genera Folio al guardar Prospecto',
  'custom/modules/Prospects/ProspectsLogicHooksManager.php',
  'ProspectsLogicHooksManager',
  'generaFolio'
);

$hook_array['after_save'] = isset($hook_array['after_save']) ? $hook_array['after_save'] : array();
$hook_array['after_save'][] = array(
  1,
  'AfterSave Functions',
  'custom/modules/Prospects/ProspectsLogicHooksManager.php',
  'ProspectsLogicHooksManager',
  'AfterSave'
);

$hook_array['after_relationship_add'] = isset($hook_array['after_relationship_add']) ? $hook_array['after_relationship_add'] : array();
$hook_array['after_relationship_add'][] = array(
  8,
  'Convercion automatica de prospecto a cliente',
  'custom/modules/Prospects/ProspectsLogicHooksManager.php',
  'ProspectsLogicHooksManager',
  'ConvercionAuto'
);
?>
