<?php
 // created: 2017-07-20 21:33:08
$layout_defs["Prospects"]["subpanel_setup"]['prospects_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_PROSPECTS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'prospects_contacts_1',
);
