<?php
$dependencies['Prospects']['estado_envio_pos_c'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'true',
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'estado_envio_pos_c',
              'value' => 'true',
          ),
      ),
    ),
);
