<?php
$dependencies['Prospects']['aprueba_oc_o_cot_c_visible'] = array(
    'hooks' => array("edit","view"),
    //Trigger formula for the dependency. Defaults to 'true'.
    // 'trigger' => 'not(equal($name, ""))',
    'triggerFields' => [''],
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'SetVisibility', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'aprueba_oc_o_cot_c',
              'value' => 'not(equal($name, ""))', //Formula
              // 'label' => 'aprueba_oc_o_cot_c_label',
          ),
      ),
    ),
);
