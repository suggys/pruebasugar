<?php
// Id AR credi visibility
$dependencies['Prospects']['id_ar_credit_c_visibility'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'equal($id_ar_credit_c,"")',
    'triggerFields' => array(''),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'SetVisibility',
          'params' => array(
              'target' => 'id_ar_credit_c',
              'value' => 'not(equal($id_ar_credit_c,""))',
          ),
      ),
    ),
    // 'notActions' => array(
    //   array(
    //       'name' => 'SetVisibility',
    //       'params' => array(
    //           'target' => 'id_ar_credit_c',
    //           'value' => 'true',
    //       ),
    //   ),
    // ),
);
