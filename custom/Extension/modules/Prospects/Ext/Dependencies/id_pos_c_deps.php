<?php
$dependencies['Prospects']['id_pos_c_deps'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'true',
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'ReadOnly',
          'params' => array(
              'target' => 'id_pos_c',
              'value' => 'true',
          ),
      ),
    ),
);
// Id ar credito visibility.
$dependencies['Prospects']['id_pos_c_visibility'] = array(
    'hooks' => array("edit","view"),
    'trigger' => 'equal($id_pos_c,"")',
    // 'triggerFields' => array('id_ar_credit_c'),
    'onload' => true,
    'actions' => array(
      array(
          'name' => 'SetVisibility',
          'params' => array(
              'target' => 'id_pos_c',
              'value' => 'false',
          ),
      ),
    ),
    'notActions' => array(
      array(
          'name' => 'SetVisibility',
          'params' => array(
              'target' => 'id_pos_c',
              'value' => 'true',
          ),
      ),
    ),
);
