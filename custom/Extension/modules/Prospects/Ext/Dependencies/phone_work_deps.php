<?php
$dependencies['Prospects']['phone_work_required'] = array(
    'hooks' => array("edit","view"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal($estado_c, "convertido")',
    'triggerFields' => array('estado_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'SetRequired', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'phone_work',
              'value' => 'true', //Formula
              'label' => 'Requerido',
          ),
      ),
    ),
);
