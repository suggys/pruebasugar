<?php

$dependencies['Prospects']['subtipo_c_required'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('estado_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'subtipo_c',
        'label' => 'subtipo_c_label',
        'value' => 'equal($estado_c,"convertido")',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'subtipo_c',
        'label' => 'subtipo_c_label',
        'value' => 'false',
      ),
    ),
  ),
);
