<?php
$dependencies['Prospects']['alt_address_state_actions'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal($estado_c, "convertido")',
    'triggerFields' => array('estado_c'),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'alt_address_state',
              'value' => true, //Formula
          ),
      ),
    ),
);
