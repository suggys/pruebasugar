<?php
$dependencies['Prospects']['hide_panel_3_deps'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'equal($estado_c, "nuevo" )',
  'triggerFields' => array('estado_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetPanelVisibility',
      'params' => array(
        'target' => 'LBL_RECORDVIEW_PANEL3',
        'value' => 'true',
      ),
    )
  ),
  'notActions' => array(
    array(
      'name' => 'SetPanelVisibility',
      'params' => array(
        'target' => 'LBL_RECORDVIEW_PANEL3',
        'value' => 'true',
      ),
    ),
  ),
);
