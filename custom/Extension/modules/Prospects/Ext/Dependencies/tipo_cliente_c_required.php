<?php

$dependencies['Prospects']['tipo_cliente_c_required'] = array(
  'hooks' => array("edit","view"),
  'trigger' => 'true',
  'triggerFields' => array('estado_c'),
  'onload' => true,
  'actions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'tipo_cliente_c',
        'label' => 'tipo_cliente_c_label',
        'value' => 'equal($estado_c,"convertido")',
      ),
    ),
  ),
  'notActions' => array(
    array(
      'name' => 'SetRequired',
      'params' => array(
        'target' => 'tipo_cliente_c',
        'label' => 'tipo_cliente_c_label',
        'value' => 'false',
      ),
    ),
  ),
);
