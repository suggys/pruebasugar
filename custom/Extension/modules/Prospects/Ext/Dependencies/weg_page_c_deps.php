<?php
$dependencies['Prospects']['web_page_c_readonly'] = array(
    'hooks' => array("edit"),
    //Trigger formula for the dependency. Defaults to 'true'.
    'trigger' => 'equal($estado_c, "convertido")',
    'triggerFields' => array(''),
    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    'actions' => array(
      array(
          'name' => 'ReadOnly', //Action type
          //The parameters passed in depend on the action type
          'params' => array(
              'target' => 'web_page_c',
              'value' => 'true', //Formula
          ),
      ),
    ),
);
