<?php
 // created: 2017-07-21 12:47:07
$layout_defs["Prospects"]["subpanel_setup"]['low01_solicitudescredito_prospects'] = array (
  'order' => 100,
  'module' => 'LOW01_SolicitudesCredito',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_LOW01_SOLICITUDESCREDITO_TITLE',
  'get_subpanel_data' => 'low01_solicitudescredito_prospects',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
