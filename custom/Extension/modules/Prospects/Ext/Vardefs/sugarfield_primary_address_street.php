<?php
 // created: 2018-03-07 15:38:54
$dictionary['Prospect']['fields']['primary_address_street']['required']=true;
$dictionary['Prospect']['fields']['primary_address_street']['audited']=false;
$dictionary['Prospect']['fields']['primary_address_street']['massupdate']=false;
$dictionary['Prospect']['fields']['primary_address_street']['comments']='The street address used for primary address';
$dictionary['Prospect']['fields']['primary_address_street']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['primary_address_street']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_street']['unified_search']=false;
$dictionary['Prospect']['fields']['primary_address_street']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.22',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['primary_address_street']['calculated']=false;
$dictionary['Prospect']['fields']['primary_address_street']['rows']='4';
$dictionary['Prospect']['fields']['primary_address_street']['cols']='20';

 ?>