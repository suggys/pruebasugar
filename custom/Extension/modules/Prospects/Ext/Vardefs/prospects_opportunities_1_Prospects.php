<?php
// created: 2017-07-14 22:54:49
$dictionary["Prospect"]["fields"]["prospects_opportunities_1"] = array (
  'name' => 'prospects_opportunities_1',
  'type' => 'link',
  'relationship' => 'prospects_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_PROSPECTS_OPPORTUNITIES_1_FROM_PROSPECTS_TITLE',
  'id_name' => 'prospects_opportunities_1prospects_ida',
  'link-type' => 'many',
  'side' => 'left',
);
