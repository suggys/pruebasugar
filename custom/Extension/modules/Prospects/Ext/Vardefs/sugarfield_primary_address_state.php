<?php
 // created: 2018-03-07 15:40:02
$dictionary['Prospect']['fields']['primary_address_state']['type']='enum';
$dictionary['Prospect']['fields']['primary_address_state']['options']='state_c_list';
$dictionary['Prospect']['fields']['primary_address_state']['len']=100;
$dictionary['Prospect']['fields']['primary_address_state']['audited']=false;
$dictionary['Prospect']['fields']['primary_address_state']['massupdate']=true;
$dictionary['Prospect']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Prospect']['fields']['primary_address_state']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['primary_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_state']['unified_search']=false;
$dictionary['Prospect']['fields']['primary_address_state']['calculated']=false;
$dictionary['Prospect']['fields']['primary_address_state']['dependency']=false;
$dictionary['Prospect']['fields']['primary_address_state']['visibility_grid']=array (
  'trigger' => 'primary_address_country',
  'values' => 
  array (
    '' => 
    array (
      0 => '',
    ),
    'US' => 
    array (
      0 => 'OTRO',
    ),
    'CA' => 
    array (
      0 => 'OTRO',
    ),
    'FR' => 
    array (
      0 => 'OTRO',
    ),
    'MX' => 
    array (
      0 => 'AGU',
      1 => 'BCN',
      2 => 'BCS',
      3 => 'CAM',
      4 => 'CHP',
      5 => 'CHH',
      6 => 'CMX',
      7 => 'COA',
      8 => 'COL',
      9 => 'DUR',
      10 => 'MEX',
      11 => 'GRO',
      12 => 'GUA',
      13 => 'HID',
      14 => 'JAL',
      15 => 'MIC',
      16 => 'MOR',
      17 => 'NAY',
      18 => 'NLE',
      19 => 'OAX',
      20 => 'PUE',
      21 => 'QUE',
      22 => 'ROO',
      23 => 'SLP',
      24 => 'SIN',
      25 => 'SON',
      26 => 'TAB',
      27 => 'TAM',
      28 => 'TLA',
      29 => 'VER',
      30 => 'YUC',
      31 => 'ZAC',
    ),
    'EU' => 
    array (
      0 => 'OTRO',
    ),
    'DE' => 
    array (
      0 => 'OTRO',
    ),
    'GB' => 
    array (
      0 => 'OTRO',
    ),
    'JP' => 
    array (
      0 => 'OTRO',
    ),
    'PR' => 
    array (
      0 => 'OTRO',
    ),
  ),
);
$dictionary['Prospect']['fields']['primary_address_state']['required']=true;

 ?>