<?php
 // created: 2017-07-13 22:29:32
$dictionary['Prospect']['fields']['tracker_key']['required']=false;
$dictionary['Prospect']['fields']['tracker_key']['audited']=false;
$dictionary['Prospect']['fields']['tracker_key']['massupdate']=false;
$dictionary['Prospect']['fields']['tracker_key']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['tracker_key']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['tracker_key']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['tracker_key']['unified_search']=false;
$dictionary['Prospect']['fields']['tracker_key']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['tracker_key']['calculated']=false;
$dictionary['Prospect']['fields']['tracker_key']['enable_range_search']=false;
$dictionary['Prospect']['fields']['tracker_key']['autoinc_next']='1';
$dictionary['Prospect']['fields']['tracker_key']['min']=false;
$dictionary['Prospect']['fields']['tracker_key']['max']=false;
$dictionary['Prospect']['fields']['tracker_key']['disable_num_format']='';

 ?>