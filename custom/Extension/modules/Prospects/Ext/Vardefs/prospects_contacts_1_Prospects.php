<?php
// created: 2017-07-20 21:33:08
$dictionary["Prospect"]["fields"]["prospects_contacts_1"] = array (
  'name' => 'prospects_contacts_1',
  'type' => 'link',
  'relationship' => 'prospects_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_PROSPECTS_CONTACTS_1_FROM_PROSPECTS_TITLE',
  'id_name' => 'prospects_contacts_1prospects_ida',
  'link-type' => 'many',
  'side' => 'left',
);
