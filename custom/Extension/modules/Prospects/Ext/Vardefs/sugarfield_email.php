<?php
 // created: 2017-08-01 07:45:51
$dictionary['Prospect']['fields']['email']['len']='100';
$dictionary['Prospect']['fields']['email']['audited']=false;
$dictionary['Prospect']['fields']['email']['massupdate']=true;
$dictionary['Prospect']['fields']['email']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['email']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['email']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['email']['unified_search']=false;
$dictionary['Prospect']['fields']['email']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.35',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['email']['calculated']=false;
$dictionary['Prospect']['fields']['email']['required']=true;

 ?>