<?php
 // created: 2018-03-07 15:39:33
$dictionary['Prospect']['fields']['primary_address_city']['audited']=false;
$dictionary['Prospect']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Prospect']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Prospect']['fields']['primary_address_city']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['primary_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_city']['unified_search']=false;
$dictionary['Prospect']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['primary_address_city']['calculated']=false;
$dictionary['Prospect']['fields']['primary_address_city']['required']=true;

 ?>