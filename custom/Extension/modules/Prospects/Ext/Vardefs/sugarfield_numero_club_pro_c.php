<?php
 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['numero_club_pro_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['numero_club_pro_c']['labelValue']='Número de Tarjeta Club PRO';
$dictionary['Prospect']['fields']['numero_club_pro_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['numero_club_pro_c']['enforced']='';
$dictionary['Prospect']['fields']['numero_club_pro_c']['dependency']='equal($club_pro_c,true)';

 ?>