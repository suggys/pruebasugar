<?php
// created: 2017-07-21 12:47:08
$dictionary["Prospect"]["fields"]["low01_solicitudescredito_prospects"] = array (
  'name' => 'low01_solicitudescredito_prospects',
  'type' => 'link',
  'relationship' => 'low01_solicitudescredito_prospects',
  'source' => 'non-db',
  'module' => 'LOW01_SolicitudesCredito',
  'bean_name' => false,
  'vname' => 'LBL_LOW01_SOLICITUDESCREDITO_PROSPECTS_FROM_PROSPECTS_TITLE',
  'id_name' => 'low01_solicitudescredito_prospectsprospects_ida',
  'link-type' => 'many',
  'side' => 'left',
);
