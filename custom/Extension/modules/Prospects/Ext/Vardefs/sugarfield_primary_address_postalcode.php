<?php
 // created: 2018-03-07 15:40:44
$dictionary['Prospect']['fields']['primary_address_postalcode']['len']='5';
$dictionary['Prospect']['fields']['primary_address_postalcode']['audited']=false;
$dictionary['Prospect']['fields']['primary_address_postalcode']['massupdate']=false;
$dictionary['Prospect']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Prospect']['fields']['primary_address_postalcode']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['primary_address_postalcode']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_postalcode']['unified_search']=false;
$dictionary['Prospect']['fields']['primary_address_postalcode']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['primary_address_postalcode']['calculated']=false;
$dictionary['Prospect']['fields']['primary_address_postalcode']['required']=true;

 ?>