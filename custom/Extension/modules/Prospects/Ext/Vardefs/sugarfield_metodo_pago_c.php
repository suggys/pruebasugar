<?php
 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['metodo_pago_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['metodo_pago_c']['labelValue']='Método de Pago';
$dictionary['Prospect']['fields']['metodo_pago_c']['visibility_grid']=array (
  'trigger' => 'estado_c',
  'values' => 
  array (
    'nuevo' => 
    array (
      0 => '',
    ),
    'convertido' => 
    array (
      0 => '',
      1 => 'cheque_al_dia',
      2 => 'cheque_post-fechado',
      3 => 'credito_AR',
      4 => 'efectivo',
      5 => 'tarjeta_de_regalo',
      6 => 'tc',
      7 => 'otros',
    ),
  ),
);

 ?>