<?php
 // created: 2018-03-07 15:42:38
$dictionary['Prospect']['fields']['phone_work']['len']='13';
$dictionary['Prospect']['fields']['phone_work']['massupdate']=false;
$dictionary['Prospect']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Prospect']['fields']['phone_work']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['phone_work']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['phone_work']['unified_search']=false;
$dictionary['Prospect']['fields']['phone_work']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.87',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['phone_work']['calculated']=false;
$dictionary['Prospect']['fields']['phone_work']['required']=true;

 ?>