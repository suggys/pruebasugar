<?php
 // created: 2017-08-06 00:11:28
$dictionary['Prospect']['fields']['alt_address_state']['audited']=false;
$dictionary['Prospect']['fields']['alt_address_state']['massupdate']=false;
$dictionary['Prospect']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Prospect']['fields']['alt_address_state']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['alt_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['alt_address_state']['unified_search']=false;
$dictionary['Prospect']['fields']['alt_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Prospect']['fields']['alt_address_state']['calculated']=false;

 ?>