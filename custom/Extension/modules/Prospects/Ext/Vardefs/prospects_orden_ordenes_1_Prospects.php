<?php
// created: 2017-07-23 02:41:44
$dictionary["Prospect"]["fields"]["prospects_orden_ordenes_1"] = array (
  'name' => 'prospects_orden_ordenes_1',
  'type' => 'link',
  'relationship' => 'prospects_orden_ordenes_1',
  'source' => 'non-db',
  'module' => 'orden_Ordenes',
  'bean_name' => 'orden_Ordenes',
  'vname' => 'LBL_PROSPECTS_ORDEN_ORDENES_1_FROM_PROSPECTS_TITLE',
  'id_name' => 'prospects_orden_ordenes_1prospects_ida',
  'link-type' => 'many',
  'side' => 'left',
);
