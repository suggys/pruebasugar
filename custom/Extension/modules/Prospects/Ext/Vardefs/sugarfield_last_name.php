<?php
 // created: 2018-05-11 14:01:26
$dictionary['Prospect']['fields']['last_name']['audited']=false;
$dictionary['Prospect']['fields']['last_name']['massupdate']=false;
$dictionary['Prospect']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Prospect']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['last_name']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['last_name']['unified_search']=false;
$dictionary['Prospect']['fields']['last_name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.36',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['last_name']['calculated']=false;
$dictionary['Prospect']['fields']['last_name']['len']='80';
$dictionary['Prospect']['fields']['last_name']['required']=true;

 ?>