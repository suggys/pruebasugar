<?php
 // created: 2018-05-14 04:05:44
$dictionary['Prospect']['fields']['giro_c']['duplicate_merge_dom_value']=0;
$dictionary['Prospect']['fields']['giro_c']['labelValue']='Giro / Industria';
$dictionary['Prospect']['fields']['giro_c']['visibility_grid']=array (
  'trigger' => 'tipo_cliente_c',
  'values' => 
  array (
    '' => 
    array (
    ),
    'comercial' => 
    array (
      0 => 'arquitecto',
      1 => 'construccion_vertical',
      2 => 'contratista',
      3 => 'decorador',
      4 => 'desarrollador_de_vivienda_residencial',
      5 => 'ingeniero',
      6 => 'mtto_industrial',
      7 => 'obra_civil',
      8 => 'proyecto_temporal_del_hogar',
      9 => 'urbanizador',
      10 => 'otro',
    ),
    'especialista' => 
    array (
    ),
  ),
);

 ?>