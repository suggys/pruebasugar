<?php
 // created: 2018-03-07 15:41:20
$dictionary['Prospect']['fields']['primary_address_country']['type']='enum';
$dictionary['Prospect']['fields']['primary_address_country']['options']='prospect_pais_list';
$dictionary['Prospect']['fields']['primary_address_country']['audited']=false;
$dictionary['Prospect']['fields']['primary_address_country']['massupdate']=true;
$dictionary['Prospect']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Prospect']['fields']['primary_address_country']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['primary_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_country']['unified_search']=false;
$dictionary['Prospect']['fields']['primary_address_country']['calculated']=false;
$dictionary['Prospect']['fields']['primary_address_country']['dependency']=false;
$dictionary['Prospect']['fields']['primary_address_country']['required']=true;

 ?>