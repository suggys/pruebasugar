<?php
 // created: 2017-02-08 18:46:51
$dictionary['User']['fields']['last_name']['required']=false;
$dictionary['User']['fields']['last_name']['audited']=false;
$dictionary['User']['fields']['last_name']['massupdate']=false;
$dictionary['User']['fields']['last_name']['duplicate_merge']='enabled';
$dictionary['User']['fields']['last_name']['duplicate_merge_dom_value']='1';
$dictionary['User']['fields']['last_name']['merge_filter']='disabled';
$dictionary['User']['fields']['last_name']['unified_search']=false;
$dictionary['User']['fields']['last_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['User']['fields']['last_name']['calculated']=false;

 ?>