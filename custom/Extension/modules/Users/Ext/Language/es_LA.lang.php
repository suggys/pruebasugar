<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_TIPO_USUARIO'] = 'Tipo de Usuario';
$mod_strings['LBL_SUCURSAL'] = 'Sucursal';
$mod_strings['LBL_OWN_OPPS'] = 'No hay Proyectos';
$mod_strings['LBL_OWN_OPPS_DESC'] = 'Marque esta opción si a este usuario no se le asignarán proyectos. Utilice esta opción para usuarios que sean gerentes y no estén involucrados en actividades comerciales. Esta opción se utiliza en el módulo de previsión.';
