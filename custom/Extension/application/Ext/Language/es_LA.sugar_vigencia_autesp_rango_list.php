<?php
 // created: 2017-06-16 23:00:38

$app_list_strings['vigencia_autesp_rango_list']=array (
  '' => '',
  'vigente' => 'Vigente',
  '1a15' => '1 - 15',
  '16a30' => '16 - 30',
  '31a60' => '31 - 60',
  '61a90' => '61 - 90',
  '91a120' => '91 - 120',
  'mas120' => '+120',
);