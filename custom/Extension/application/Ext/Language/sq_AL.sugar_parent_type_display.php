<?php
 // created: 2016-07-25 21:54:26

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'llogaritë',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Detyrë',
  'Opportunities' => 'Mundësi:',
  'Products' => 'Produkti',
  'Quotes' => 'Kuota',
  'Bugs' => 'Gabimet',
  'Cases' => 'Rast',
  'Leads' => 'udhëheqje',
  'Project' => 'Projekti',
  'ProjectTask' => 'Detyrat projektuese',
  'Prospects' => 'Synim',
  'KBContents' => 'baza e njohurisë',
  'RevenueLineItems' => 'Rreshti i llojeve të të ardhurave',
);