<?php
 // created: 2016-07-25 21:54:08

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Konto',
  'Opportunities' => 'Muligheter',
  'Cases' => 'Sak',
  'Leads' => 'Lead',
  'Contacts' => 'Kontakter',
  'Products' => 'Produkt',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Feil',
  'Project' => 'Prosjekt',
  'Prospects' => 'Mål',
  'ProjectTask' => 'Prosjektoppgave',
  'Tasks' => 'Oppgave',
  'KBContents' => 'KB-dokumenter',
  'RevenueLineItems' => 'Omsetningsposter',
);