<?php
 // created: 2016-07-25 21:53:53

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Virksomhed',
  'Opportunities' => 'Salgsmulighed',
  'Cases' => 'Sag',
  'Leads' => 'Kundeemne',
  'Contacts' => 'Kontakter',
  'Products' => 'Angiven linjepost',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Fejl',
  'Project' => 'Projekt',
  'Prospects' => 'Mål:',
  'ProjectTask' => 'Projektopgave',
  'Tasks' => 'Opgave',
  'KBContents' => 'Videnbase',
  'RevenueLineItems' => 'Revenue detaljposter',
);