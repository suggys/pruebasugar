<?php
 // created: 2017-11-02 05:29:29

$app_list_strings['account_codigo_bloqueo_c_list']=array (
  '' => '',
  'CL' => 'Cliente Legal',
  'CI' => 'Cliente Inactivo',
  'DI' => 'Dictamen Incobrable',
  'Otro' => 'Otro',
  'CV' => 'Cartera vencida + x dias',
  'CD' => 'Cheque devuelto',
  'LE' => 'Limite de credito excedido',
);