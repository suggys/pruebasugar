<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$app_list_strings['moduleList']['LOW02_Partidas_Orden'] = 'Partidas de Orden';
$app_list_strings['moduleListSingular']['LOW02_Partidas_Orden'] = 'Partida de Orden';

$app_list_strings['lo_obras_type_dom']['Analyst'] = 'Analista';
$app_list_strings['lo_obras_type_dom']['Competitor'] = 'Competidor';
$app_list_strings['lo_obras_type_dom']['Customer'] = 'Cliente';
$app_list_strings['lo_obras_type_dom']['Integrator'] = 'Integrador';
$app_list_strings['lo_obras_type_dom']['Investor'] = 'Inversor';
$app_list_strings['lo_obras_type_dom']['Partner'] = 'Socio';
$app_list_strings['lo_obras_type_dom']['Press'] = 'Prensa';
$app_list_strings['lo_obras_type_dom']['Prospect'] = 'Prospecto';
$app_list_strings['lo_obras_type_dom']['Reseller'] = 'Revendedor';
$app_list_strings['lo_obras_type_dom']['Other'] = 'Otro';
$app_list_strings['lo_obras_type_dom'][''] = '';
$app_list_strings['moduleList']['LO_Obras'] = 'Obras';
$app_list_strings['moduleListSingular']['LO_Obras'] = 'Obra';

$app_list_strings['moduleList']['LOW01_SolicitudesCredito'] = 'Solicitudes de Crédito';
$app_list_strings['moduleListSingular']['LOW01_SolicitudesCredito'] = 'Solicitud de Crédito';
$app_list_strings['tipo_de_credito_list']['30_dias_sin_intereses'] = '30 días sin intereses';
$app_list_strings['tipo_de_credito_list']['90_dias_sin_intereses'] = '90 días sin intereses';
$app_list_strings['tipo_de_credito_list'][''] = '';
$app_list_strings['solicitud_credito_domicilio_list']['Propia'] = 'Propia';
$app_list_strings['solicitud_credito_domicilio_list']['Rentada'] = 'Rentada';
$app_list_strings['solicitud_credito_domicilio_list']['HIpotecada'] = 'HIpotecada';
$app_list_strings['solicitud_credito_domicilio_list']['De_Socios'] = 'De Socios';
$app_list_strings['solicitud_credito_domicilio_list'][''] = '';
$app_list_strings['solicitud_credito_giro_list']['Comercio'] = 'Comercio';
$app_list_strings['solicitud_credito_giro_list']['Industria'] = 'Industria';
$app_list_strings['solicitud_credito_giro_list']['Servicio'] = 'Servicio';
$app_list_strings['solicitud_credito_giro_list']['Agropecuario'] = 'Agropecuario';
$app_list_strings['solicitud_credito_giro_list']['Construccion'] = 'Construcción';
$app_list_strings['solicitud_credito_giro_list'][''] = '';
$app_list_strings['solicitud_credito_sector_list']['Publico'] = 'Público';
$app_list_strings['solicitud_credito_sector_list']['Privado'] = 'Privado';
$app_list_strings['solicitud_credito_sector_list'][''] = '';
$app_list_strings['solicitud_credito_caracter_list']['Propietario'] = 'Propietario';
$app_list_strings['solicitud_credito_caracter_list']['Asociado'] = 'Asociado';
$app_list_strings['solicitud_credito_caracter_list']['Empleado'] = 'Empleado';
$app_list_strings['solicitud_credito_caracter_list']['Otro'] = 'Otro';
$app_list_strings['solicitud_credito_caracter_list'][''] = '';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['T_de_C'] = 'T de C';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Debito'] = 'Débito';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Cheques'] = 'Cheques';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Ahorro'] = 'Ahorro';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Inversion'] = 'Inversión';
$app_list_strings['solicitud_credito_tipo_ref_com_list']['Otras'] = 'Otras';
$app_list_strings['solicitud_credito_tipo_ref_com_list'][''] = '';
$app_list_strings['recepcion_estado_cuenta_list']['Correo'] = 'Correo';
$app_list_strings['recepcion_estado_cuenta_list']['Correo_electronico'] = 'Correo electrónico';
$app_list_strings['recepcion_estado_cuenta_list'][''] = '';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list']['Correo'] = 'Correo';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list']['Correo_electronico'] = 'Correo electrónico';
$app_list_strings['solicitud_credito_recepcion_estado_cuenta_list'][''] = '';
$app_list_strings['recepcion_informacion_promo_list']['Si'] = 'Si';
$app_list_strings['recepcion_informacion_promo_list']['No'] = 'No';
$app_list_strings['recepcion_informacion_promo_list'][''] = '';
