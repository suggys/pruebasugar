<?php
 // created: 2017-07-14 17:26:35

$app_list_strings['accounts_giro_list']=array (
  'arquitecto' => 'Arquitecto',
  'construccion_vertical' => 'Construcción vertical',
  'contratista' => 'Contratista',
  'decorador' => 'Decorador',
  'desarrollador_de_vivienda_residencial' => 'Desarrollador de vivienda residencial',
  'ingeniero' => 'Ingeniero',
  'mtto_industrial' => 'Mtto. industrial',
  'obra_civil' => 'Obra civil',
  'proyecto_temporal_del_hogar' => 'Proyecto temporal del hogar',
  'urbanizador' => 'Urbanizador',
  'otro' => 'Otro (especifique)',
);
