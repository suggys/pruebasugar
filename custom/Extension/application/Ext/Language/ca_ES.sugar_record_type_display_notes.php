<?php
 // created: 2016-07-25 21:54:22

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contacte',
  'Opportunities' => 'Oportunitat',
  'Tasks' => 'Tasca',
  'ProductTemplates' => 'Catàleg de productes',
  'Quotes' => 'Pressupost',
  'Products' => 'Element de línia d\'oferta',
  'Contracts' => 'Contracte',
  'Emails' => 'Adreça electrònica',
  'Bugs' => 'Incidència',
  'Project' => 'Projecte',
  'ProjectTask' => 'Tasca de projecte',
  'Prospects' => 'Objectiu',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Meetings' => 'Reunió',
  'Calls' => 'Trucada',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Línia d\'impostos articles',
);