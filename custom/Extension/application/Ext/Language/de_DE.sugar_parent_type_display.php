<?php
 // created: 2016-07-25 21:53:54

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Firma',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Aufgabe',
  'Opportunities' => 'Verkaufschance',
  'Products' => 'Produkt',
  'Quotes' => 'Angebot',
  'Bugs' => 'Fehler',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektaufgabe',
  'Prospects' => 'Zielkontakt',
  'KBContents' => 'Wissensdatenbank',
  'RevenueLineItems' => 'Umsatzposten',
);