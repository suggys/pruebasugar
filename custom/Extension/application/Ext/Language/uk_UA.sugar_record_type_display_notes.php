<?php
 // created: 2016-07-25 21:54:32

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Opportunities' => 'Угода',
  'Tasks' => 'Задача',
  'ProductTemplates' => 'Каталог продукту',
  'Quotes' => 'Комерційна пропозиція',
  'Products' => 'Продукт комерційної пропозиції',
  'Contracts' => 'Контракт',
  'Emails' => 'Email',
  'Bugs' => 'Помилка',
  'Project' => 'Проект',
  'ProjectTask' => 'Задача проекту',
  'Prospects' => 'Цільова аудиторія споживачів',
  'Cases' => 'Звернення',
  'Leads' => 'Інтерес',
  'Meetings' => 'Зустріч',
  'Calls' => 'Дзвінок',
  'KBContents' => 'База знань',
  'RevenueLineItems' => 'Доходи за продукти',
);