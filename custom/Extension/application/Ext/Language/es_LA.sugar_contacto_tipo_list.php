<?php
 // created: 2017-07-13 14:17:02

$app_list_strings['contacto_tipo_list']=array (
  '' => '',
  'propietario'=>'Propietario',
  'director_general'=>'Director general',
  'asistente'=>'Asistente',
  'administrador'=>'Administrador',
  'comprador'=>'Comprador',
  'pagos'=>'Pagos',
  'supervisor_de_obra'=>'Supervisor de obra',
  'representante_legal'=>'Representante legal',
  'otro'=>'Otro (Especificar)',
);
