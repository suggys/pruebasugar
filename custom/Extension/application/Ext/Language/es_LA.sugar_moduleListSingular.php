<?php
// created: 2018-10-04 03:46:15
$app_list_strings['moduleListSingular']['Home'] = 'Inicio';
$app_list_strings['moduleListSingular']['Contacts'] = 'Contacto';
$app_list_strings['moduleListSingular']['Accounts'] = 'Cliente';
$app_list_strings['moduleListSingular']['Opportunities'] = 'Oportunidad';
$app_list_strings['moduleListSingular']['Cases'] = 'Caso';
$app_list_strings['moduleListSingular']['Notes'] = 'Nota';
$app_list_strings['moduleListSingular']['Calls'] = 'Llamada';
$app_list_strings['moduleListSingular']['Emails'] = 'Correo Electrónico';
$app_list_strings['moduleListSingular']['Meetings'] = 'Reunión';
$app_list_strings['moduleListSingular']['Tasks'] = 'Tarea';
$app_list_strings['moduleListSingular']['Calendar'] = 'Calendario';
$app_list_strings['moduleListSingular']['Leads'] = 'Lead';
$app_list_strings['moduleListSingular']['Contracts'] = 'Contrato';
$app_list_strings['moduleListSingular']['Quotes'] = 'Cotizacion';
$app_list_strings['moduleListSingular']['Products'] = 'Partida Individual Cotizada';
$app_list_strings['moduleListSingular']['Reports'] = 'Informe';
$app_list_strings['moduleListSingular']['Forecasts'] = 'Pronóstico';
$app_list_strings['moduleListSingular']['Bugs'] = 'Error';
$app_list_strings['moduleListSingular']['Project'] = 'Proyecto';
$app_list_strings['moduleListSingular']['Campaigns'] = 'Campaña';
$app_list_strings['moduleListSingular']['Documents'] = 'Documento';
$app_list_strings['moduleListSingular']['pmse_Inbox'] = 'Proceso';
$app_list_strings['moduleListSingular']['pmse_Project'] = 'Definición de Proceso';
$app_list_strings['moduleListSingular']['pmse_Business_Rules'] = 'Proceso de Regla Empresarial';
$app_list_strings['moduleListSingular']['pmse_Emails_Templates'] = 'Plantilla de Correo Electrónico para procesos';
$app_list_strings['moduleListSingular']['Prospects'] = 'Pospecto';
$app_list_strings['moduleListSingular']['ProspectLists'] = 'Lista de Público Objetivo';
$app_list_strings['moduleListSingular']['Tags'] = 'Etiqueta';
$app_list_strings['moduleListSingular']['OutboundEmail'] = 'Opciones de Correo Electrónico';
$app_list_strings['moduleListSingular']['DataPrivacy'] = 'Privacidad de datos';
$app_list_strings['moduleListSingular']['KBContents'] = 'Artículo de Base de Conocimiento';
$app_list_strings['moduleListSingular']['RevenueLineItems'] = 'Artículo de Línea de Ganancia';
$app_list_strings['moduleListSingular']['GPE_Grupo_empresas'] = 'Grupo de empresa';
$app_list_strings['moduleListSingular']['orden_Ordenes'] = 'Orden';
$app_list_strings['moduleListSingular']['NC_Notas_credito'] = 'Nota de crédito';
$app_list_strings['moduleListSingular']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['moduleListSingular']['LF_Facturas'] = 'Factura';
$app_list_strings['moduleListSingular']['lowes_Pagos'] = 'Pago';
$app_list_strings['moduleListSingular']['Low01_Pagos_Facturas'] = 'Pago Factura';
$app_list_strings['moduleListSingular']['LOW02_Partidas_Orden'] = 'Partida de Orden';
$app_list_strings['moduleListSingular']['LO_Obras'] = 'Obra';
$app_list_strings['moduleListSingular']['LOW01_SolicitudesCredito'] = 'Solicitud de Crédito';
$app_list_strings['moduleListSingular']['MRX1_Configuraciones'] = 'Configuración';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['moduleListSingular']['ops_Backups'] = 'Backup';
$app_list_strings['moduleListSingular']['FDP_Pago'] = 'Forma de Pago';
$app_list_strings['moduleListSingular']['FDP_formasPago'] = 'Forma de Pago';
$app_list_strings['moduleListSingular']['Low03_AgentesExternos'] = 'Agente Externo';
