<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/th_TH.CentroDeDisenio_Personalizaciones_180201_1311.php
 
$app_list_strings['sucursal_c_list'] = array (
  '' => '',
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
);
?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php
 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'การดูแลระบบ',
  'Product' => 'ผลิตภัณฑ์',
  'User' => 'ผู้ใช้',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => 'ใหม่',
  'Assigned' => 'ระบุแล้ว',
  'Closed' => 'ปิดแล้ว',
  'Pending Input' => 'รออินพุต',
  'Rejected' => 'ปฏิเสธ',
  'Duplicate' => 'ซ้ำ',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => 'สูง',
  'P2' => 'ปานกลาง',
  'P3' => 'ต่ำ',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => 'ยอมรับแล้ว',
  'Duplicate' => 'ซ้ำ',
  'Closed' => 'ปิดแล้ว',
  'Out of Date' => 'เก่าเกินไป',
  'Invalid' => 'ไม่ถูกต้อง',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);
?>
