<?php
 // created: 2017-12-08 19:45:02

$app_list_strings['dashlet_codigo_bloqueo_c_list']=array (
  '' => '',
  'CL' => 'Cliente Legal',
  'CI' => 'Cliente Inactivo',
  'DI' => 'Dictamen Incobrable',
  'Otro' => 'Otro',
  'CV' => 'Cartera vencida + x dias',
  'CD' => 'Cheque devuelto',
  'LE' => 'Limite de credito excedido',
);
