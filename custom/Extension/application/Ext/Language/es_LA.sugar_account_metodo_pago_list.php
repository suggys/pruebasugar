<?php
 // created: 2017-07-14 17:41:53

$app_list_strings['account_metodo_pago_list']=array (
  '' => '',
  'cheque_al_dia' => 'Cheque al día',
  'cheque_post_fechado' => 'Cheque post-fechado',
  'credito_AR' => 'Crédito AR',
  'efectivo' => 'Efectivo',
  'tarjeta_de_regalo' => 'Tarjeta de regalo',
  'tc' => 'TC',
  'otros' => 'Otros',
);
