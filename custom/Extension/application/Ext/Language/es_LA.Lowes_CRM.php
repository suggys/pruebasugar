<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/es_LA.Lowes_CRM.php

 // created: 2017-07-26 15:36:51

$app_list_strings['solicitud_credito_estado_list']=array (
  'Solicitud_Nueva' => 'Solicitud Nueva',
  'Finalizado_por_Solicitante' => 'Finalizado por Solicitante',
  'Aprobado_por_Asesor_Comercial' => 'Aprobado por Asesor Comercial',
  'Rechazado_por_Asesor_Comercial' => 'Rechazado por Asesor Comercial',
  'Pendiente_por_Gerente_de_Ventas_Comerciales' => 'Pendiente por Gerente de Ventas Comerciales',
  'Aprobado_por_Gerente_de_Ventas_Comerciales' => 'Aprobado por Gerente de Ventas Comerciales',
  'Rechazado_por_Gerente_de_Ventas_Comerciales' => 'Rechazado por Gerente de Ventas Comerciales',
  'Pendiente_por_Analista_de_Credito' => 'Pendiente por Analista de Crédito',
  'Aprobado_por_Analista_de_Credito' => 'Aprobado por Analista de Crédito',
  'Rechazado_por_Analista_de_Credito' => 'Rechazado por Analista de Crédito',
  'Pendiente_de_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Pendiente de Envío a Agencia Externa por Administrador de Crédito',
  'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito' => 'Aprobado Envío a Agencia Externa por Administrador de Crédito',
  'Pendiente_por_Agente_Externo' => 'Pendiente por Agente Externo',
  'Pendiente_por_Administrador_de_Credito' => 'Pendiente por Administrador de Crédito',
  'Aprobacion_por_Administrador_de_Credito' => 'Aprobación por Administrador de Crédito',
  'Rechazado_por_Administrador_de_Credito' => 'Rechazado por Administrador de Crédito',
  'Pendiente_por_Gerente_de_Credito' => 'Pendiente por Gerente de  Crédito',
  'Aprobacion_por_Gerente_de_Credito' => 'Aprobación por Gerente de Crédito',
  'Rechazado_por_Gerente_de_Credito' => 'Rechazado por Gerente de Crédito',
  'Pendiente_por_Director_de_Finanzas' => 'Pendiente por Director de Finanzas',
  'Aprobacion_por_Director_de_Finanzas' => 'Aprobación por Director de Finanzas',
  'Rechazado_por_Director_de_Finanzas' => 'Rechazado por Director de Finanzas',
  'Pendiente_por_Vicepresidencia_Comercial' => 'Pendiente por Vicepresidencia Comercial',
  'Aprobacion_por_Vicepresidencia_Comercial' => 'Aprobación por Vicepresidencia Comercial',
  'Rechazado_por_Vicepresidencia_Comercial' => 'Rechazado por Vicepresidencia Comercial',
);

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

$app_list_strings['pagos_estados_konesh_list']=array (
  '' => '',
  'Activo' => 'Activo',
  'Aplicado' => 'Aplicado',
  'Reaplicado' => 'Reaplicado',
  'Enviado' => 'Enviado',
);

?>
