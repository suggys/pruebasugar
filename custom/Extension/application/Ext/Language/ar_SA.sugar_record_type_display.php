<?php
 // created: 2016-07-25 21:54:31

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'الحساب',
  'Opportunities' => 'الفرصة',
  'Cases' => 'الحالة',
  'Leads' => 'العميل المتوقع',
  'Contacts' => 'جهات الاتصال',
  'Products' => 'البند المسعر',
  'Quotes' => 'عرض السعر',
  'Bugs' => 'الخطأ',
  'Project' => 'المشروع',
  'Prospects' => 'الهدف',
  'ProjectTask' => 'مهمة المشروع',
  'Tasks' => 'المهمة',
  'KBContents' => 'قاعدة المعارف',
  'RevenueLineItems' => 'بنود العائدات',
);