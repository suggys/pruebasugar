<?php
 // created: 2016-07-25 21:54:10

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Kontrahent',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Szansa',
  'Tasks' => 'Zadanie',
  'ProductTemplates' => 'Katalog produktów',
  'Quotes' => 'Oferta',
  'Products' => 'Pozycja oferty',
  'Contracts' => 'Umowa',
  'Emails' => 'E-mail',
  'Bugs' => 'Błąd',
  'Project' => 'Projekt',
  'ProjectTask' => 'Zadanie projektowe',
  'Prospects' => 'Odbiorca',
  'Cases' => 'Zgłoszenie',
  'Leads' => 'Potencjalny klient',
  'Meetings' => 'Spotkanie',
  'Calls' => 'Rozmowa tel.',
  'KBContents' => 'Baza wiedzy',
  'RevenueLineItems' => 'Pozycje szansy',
);