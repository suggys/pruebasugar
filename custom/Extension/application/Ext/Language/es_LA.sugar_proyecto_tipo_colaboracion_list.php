<?php
 // created: 2017-07-10 22:32:35

$app_list_strings['proyecto_tipo_colaboracion_list']=array (
  '' => '',
  'tienda_atiende_cliente_vende_y_otra_entrega' => 'Tienda atiende cliente vende y otra entrega',
  'tienda_atiende_cliente_vende_y_otra_entrega_por_excepcion' => 'Tienda atiende cliente vende y otra entrega por excepción',
  'tienda_atiende_cliente_vende_y_transfiere_proyecto_a_otro_vendedor_en_tienda_2_para_atencion_de_la_c' => 'Tienda atiende cliente vende y transfiere proyecto a otro vendedor en tienda 2 para atención de la cuenta',
  'tienda_atiende_cliente_vende_y_csc_define_proyecto_asignado_a_otra_tienda' => 'Tienda atiende cliente vende y CSC define proyecto asignado a otra tienda',
);
