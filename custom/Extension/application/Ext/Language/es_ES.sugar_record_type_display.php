<?php
 // created: 2016-07-25 21:53:57

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Cuenta',
  'Opportunities' => 'Oportunidad',
  'Cases' => 'Caso',
  'Leads' => 'Cliente Potencial',
  'Contacts' => 'Contactos',
  'Products' => 'Línea de la Oferta',
  'Quotes' => 'Presupuesto',
  'Bugs' => 'Incidencia',
  'Project' => 'Proyecto',
  'Prospects' => 'Público Objetivo',
  'ProjectTask' => 'Tarea de Proyecto',
  'Tasks' => 'Tarea',
  'KBContents' => 'Base de Conocimiento',
  'RevenueLineItems' => 'Líneas de Ingreso',
);