<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['GPE_Grupo_empresas'] = 'Grupo de empresas';
$app_list_strings['moduleListSingular']['GPE_Grupo_empresas'] = 'Grupo de empresa';
$app_list_strings['gpo_empresa_estado_list']['Activo'] = 'Activo';
$app_list_strings['gpo_empresa_estado_list']['Inactivo'] = 'Inactivo';
$app_list_strings['gpo_empresa_estado_list'][''] = '';
$app_list_strings['gpo_emp_edo_list']['Activo'] = 'Activo';
$app_list_strings['gpo_emp_edo_list']['Inactivo'] = 'Inactivo';
$app_list_strings['gpo_emp_edo_list'][''] = '';
