<?php 
$app_list_strings['dise_prospectos_cocina_type_dom'] = array (
  'Administration' => 'Administration',
  'Product' => '產品',
  'User' => '使用者',
);$app_list_strings['dise_prospectos_cocina_status_dom'] = array (
  'New' => '新',
  'Assigned' => '已指派',
  'Closed' => '已關閉',
  'Pending Input' => '暫止的輸入',
  'Rejected' => '已拒絕',
  'Duplicate' => '複製',
);$app_list_strings['dise_prospectos_cocina_priority_dom'] = array (
  'P1' => '高',
  'P2' => '媒體',
  'P3' => '低',
);$app_list_strings['dise_prospectos_cocina_resolution_dom'] = array (
  'Accepted' => '已接受',
  'Duplicate' => '複製',
  'Closed' => '已關閉',
  'Out of Date' => '已過期',
  'Invalid' => '無效',
  '' => '',
);$app_list_strings['tipo_persona_list'] = array (
  'Persona_fisica' => 'Persona física',
  'Persona_moral' => 'Persona_moral',
);$app_list_strings['categoria_list'] = array (
  'Accesorios' => 'Accesorios',
  'Banio' => 'Baño',
  'Centro_de_entretenimiento' => 'Centro de entretenimiento',
  'Closet' => 'Closet',
  'Cocina' => 'Cocina',
  'Cubiertas' => 'Cubiertas',
  'Otros' => 'Otros',
);$app_list_strings['etapa_de_venta_list'] = array (
  'Prospeccion' => 'Prospección',
  'Analisis' => 'Análisis',
  'Disenio' => 'Diseño',
  'Negociacion' => 'Negociación',
  'Venta_gana' => 'Venta ganada',
  'Venta_perdida' => 'Venta perdida',
);$app_list_strings['sucursal_c_list'] = array (
  2935 => 'Linda Vista',
  2936 => 'Sendero',
  3233 => 'Saltillo',
  3235 => 'Hermosillo',
  3236 => 'Culiacán',
  3255 => 'Chihuahua',
  3227 => 'Cumbres',
  3265 => 'Valle Alto',
  3267 => 'Escobedo',
  3271 => 'Oferre',
  3165 => 'Garza Sada',
  3389 => 'Pablo Livas',
  4241 => 'DEV STORE',
  7861 => 'PERF STORE',
  '' => '',
);