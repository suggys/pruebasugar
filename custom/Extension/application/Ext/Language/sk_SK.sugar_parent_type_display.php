<?php
 // created: 2016-07-25 21:54:25

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Účet',
  'Contacts' => 'Kontakt:',
  'Tasks' => 'Úloha',
  'Opportunities' => 'Obchodná príležitosť',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Ponuka',
  'Bugs' => 'Chyby',
  'Cases' => 'Prípad',
  'Leads' => 'Príležitosť',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektová úloha',
  'Prospects' => 'Cieľ',
  'KBContents' => 'Báza znalostí',
  'RevenueLineItems' => 'Položky krivky výnosu',
);