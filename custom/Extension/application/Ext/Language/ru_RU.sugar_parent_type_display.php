<?php
 // created: 2016-07-25 21:54:14

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Tasks' => 'Задача',
  'Opportunities' => 'Сделка',
  'Products' => 'Продукт коммерческого предложения',
  'Quotes' => 'Коммерческое предложение',
  'Bugs' => 'Ошибки',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Project' => 'Проект',
  'ProjectTask' => 'Проектная задача',
  'Prospects' => 'Адресат',
  'KBContents' => 'База знаний',
  'RevenueLineItems' => 'Доход по продуктам',
);