<?php
 // created: 2016-09-14 16:18:35

$app_list_strings['account_estado_segun_cartera_c_list']=array (
  'CV' => 'Cartera vigente',
  'CV130D' => 'Cartera vencida 1 a 30 días',
  'CV31120D' => 'Cartera vencida 31 a 120 días',
  'CV120D' => 'Cartera vencida 120 días',
);