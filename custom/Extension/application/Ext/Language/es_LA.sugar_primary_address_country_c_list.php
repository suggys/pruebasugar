<?php
 // created: 2016-09-29 18:46:07

$app_list_strings['primary_address_country_c_list']=array (
  'US' => 'USA',
  'CA' => 'Canada',
  'FR' => 'France',
  'MX' => 'México',
  'EU' => 'E.U.',
  'DE' => 'Germany',
  'GB' => 'Great Britain',
  'JP' => 'Japan',
  'PR' => 'Puerto Rico',
);