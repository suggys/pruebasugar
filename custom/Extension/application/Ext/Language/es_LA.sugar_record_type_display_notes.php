<?php
 // created: 2017-07-20 10:22:41

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Cliente Crédito AR',
  'Contacts' => 'Contacto',
  'Opportunities' => 'Proyecto',
  'Tasks' => 'Tarea',
  'ProductTemplates' => 'Catálogo de Productos',
  'Quotes' => 'Cotizacion',
  'Products' => 'Partida Individual Cotizada',
  'Contracts' => 'Contrato',
  'Emails' => 'Correo Electrónico',
  'Bugs' => 'Error',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Prospecto',
  'Cases' => 'Caso',
  'Leads' => 'Lead',
  'Meetings' => 'Reunión',
  'Calls' => 'Llamada',
  'KBContents' => 'Base de Conocimiento',
);