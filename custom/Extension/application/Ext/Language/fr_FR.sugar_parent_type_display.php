<?php
 // created: 2016-07-25 21:53:57

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contact',
  'Tasks' => 'Tâche',
  'Opportunities' => 'Affaire',
  'Products' => 'Ligne de devis',
  'Quotes' => 'Devis',
  'Bugs' => 'Bugs',
  'Cases' => 'Ticket',
  'Leads' => 'Lead',
  'Project' => 'Projet',
  'ProjectTask' => 'Tâche Projet',
  'Prospects' => 'Cible',
  'KBContents' => 'Base de connaissances',
  'RevenueLineItems' => 'Lignes de revenu',
);