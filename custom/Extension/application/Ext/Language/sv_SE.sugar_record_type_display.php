<?php
 // created: 2016-07-25 21:54:16

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Konto',
  'Opportunities' => 'Affärsmöjlighet',
  'Cases' => 'Ärende',
  'Leads' => 'Möjlig kund',
  'Contacts' => 'Kontakter',
  'Products' => 'Produkt',
  'Quotes' => 'Offert',
  'Bugs' => 'Bugg',
  'Project' => 'Projekt',
  'Prospects' => 'Mål',
  'ProjectTask' => 'Projektuppgift',
  'Tasks' => 'Uppgift',
  'KBContents' => 'Kunskapsbas Dokument',
  'RevenueLineItems' => 'Intäktsposter',
);