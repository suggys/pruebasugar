<?php
 // created: 2017-07-11 16:31:13

$app_list_strings['proyecto_etapa_list']=array (
  'prospeccion'=>'Prospección',
  'identificacion_de_necesidades'=>'Identificación de necesidades',
  'cotizacion'=>'Cotización',
  'en_negociacion'=>'En negociación',
  'ganado_ventas_pendientes'=>'Ganado (Con ventas pendientes)',
  'termiando_sin_entregas_pendientes'=>'Terminado (Cuando ya no tienes entregas pendientes)',
  'perdida'=>'Perdida',
);
