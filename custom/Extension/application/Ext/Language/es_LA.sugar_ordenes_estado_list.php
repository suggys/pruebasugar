<?php
 // created: 2016-12-13 00:42:59

$app_list_strings['ordenes_estado_list']=array (
  '' => 'Venta',
  '0' => 'Nueva',
  '1' => 'Iniciada',
  '2' => 'Parcial',
  '3' => 'Lleno',
  '4' => 'Cancelada',
  '5' => 'Completada',
  '6' => 'Devuelta',
  '7' => 'Cancelada Suspendida',
  '8' => 'Cancelada Parcial',
  '9' => 'Cancelada Completada',
);
