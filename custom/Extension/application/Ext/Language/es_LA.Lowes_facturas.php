<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['LF_Facturas'] = 'Facturas';
$app_list_strings['moduleListSingular']['LF_Facturas'] = 'Factura';
$app_list_strings['tipo_documento_list']['factura'] = 'Factura';
$app_list_strings['estado_tiempo_list']['vigente'] = 'Vigente';
$app_list_strings['estado_tiempo_list']['vencida'] = 'Vencida';
$app_list_strings['estado_tiempo_list']['pagada'] = 'Pagada';
$app_list_strings['estado_tiempo_list']['devuelta_parcial'] = 'Devuelta parcial';
$app_list_strings['estado_tiempo_list']['devuelta_completa'] = 'Devuelta completa';
$app_list_strings['estado_tiempo_list']['entregada'] = 'Entregada';
$app_list_strings['estado_factura_list']['aclaracion'] = 'Aclaración';
$app_list_strings['estado_factura_list']['renovacion_de_cartera'] = 'Renovación de cartera';
$app_list_strings['estado_factura_list']['cobranza_extrajudicial'] = 'Cobranza extrajudicial';
$app_list_strings['estado_factura_list']['legal'] = 'Legal';
$app_list_strings['estado_factura_list']['perdida'] = 'Pérdida';
