<?php
 // created: 2016-12-08 15:33:18

$app_list_strings['document_category_dom']=array (
  '' => '',
  'pagare' => 'Pagaré',
  'contrato' => 'Contrato',
  'cheque_postfechado' => 'Cheque Postfechado',
  'autorizacion' => 'Autorización',
  'fianza' => 'Fianza',
  'prendaria' => 'Prendaria',
  'hipotecaria' => 'Hipotecaria',
  'anticipo' => 'Anticipo',
  'carta_incobrabilidad' => 'Carta de incobrabilidad',
  'otro' => 'Otro',
);