<?php
 // created: 2016-07-25 21:54:22

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contacte',
  'Tasks' => 'Tasca',
  'Opportunities' => 'Oportunitat',
  'Products' => 'Element de línia d\'oferta',
  'Quotes' => 'Pressupost',
  'Bugs' => 'Incidències',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Project' => 'Projecte',
  'ProjectTask' => 'Tasca de projecte',
  'Prospects' => 'Objectiu',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Línia d\'impostos articles',
);