<?php
 // created: 2017-07-20 10:22:43

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Cliente Creditor AR',
  'Opportunities' => 'Proyecto',
  'Cases' => 'Caso',
  'Leads' => 'Leads',
  'Contacts' => 'Contactos',
  'Products' => 'Partida Individual Cotizada',
  'Quotes' => 'Cotizacion',
  'Bugs' => 'Error',
  'Project' => 'Proyecto',
  'Prospects' => 'Prospecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Tasks' => 'Tarea',
  'KBContents' => 'Base de Conocimiento',
);