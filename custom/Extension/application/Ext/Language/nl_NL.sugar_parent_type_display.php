<?php
 // created: 2016-07-25 21:54:08

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Persoon',
  'Tasks' => 'Taak',
  'Opportunities' => 'Opportunity',
  'Products' => 'Geoffreerd product',
  'Quotes' => 'Offerte',
  'Bugs' => 'Bugs',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Project' => 'Project',
  'ProjectTask' => 'Projecttaak',
  'Prospects' => 'Target',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Opportunityregels',
);