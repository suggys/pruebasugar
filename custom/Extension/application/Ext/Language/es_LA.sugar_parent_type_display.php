<?php
 // created: 2017-07-20 10:22:38

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Cliente Crédito AR',
  'Contacts' => 'Contacto',
  'Tasks' => 'Tarea',
  'Opportunities' => 'Proyecto',
  'Products' => 'Partida Individual Cotizada',
  'Quotes' => 'Cotizacion',
  'Bugs' => 'Errores',
  'Cases' => 'Caso',
  'Leads' => 'Lead',
  'Project' => 'Proyecto',
  'ProjectTask' => 'Tarea de Proyecto',
  'Prospects' => 'Prospecto',
  'KBContents' => 'Base de Conocimiento',
);