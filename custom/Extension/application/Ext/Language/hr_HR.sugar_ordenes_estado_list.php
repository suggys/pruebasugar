<?php
 // created: 2018-06-04 21:57:10

$app_list_strings['ordenes_estado_list']=array (
  '' => '',
  0 => '',
  1 => 'Iniciada',
  2 => 'Parcial',
  3 => 'Lleno',
  4 => 'Cancelada',
  5 => 'Completada',
  6 => 'Devuelta',
  7 => 'Cancelada Suspendida',
  8 => 'Cancelada Parcial',
  9 => 'Cancelada Completada',
);