<?php
 // created: 2017-07-10 22:10:04

$app_list_strings['proyecto_clase_cat_list']=array (
  'banos' => 'Baños',
  'pintura' => 'Pintura',
  'madera' => 'Madera',
  'electrico' => 'Eléctrico',
  'plomeria' => 'Plomería',
  'herramientas' => 'Herramientas',
  'ferreteria' => 'Ferretería',
  'puertas_y_ventanas' => 'Puertas y ventanas',
  'pisos' => 'Pisos',
  'iluminacion' => 'Iluminación',
  'material_de_construccion' => 'Material de construcción',
  'temporada' => 'Temporada',
  'linea_blanca' => 'Línea Blanca',
);