<?php
 // created: 2017-01-05 18:47:03

$app_list_strings['estado_autorizacion_list']=array (
  '' => '',
  'pendiente' => 'Pendiente',
  'autorizada' => 'Autorizada',
  'rechazada' => 'Rechazada',
  'finalizada' => 'Finalizada',
  'terminada_incumplida' => 'Terminada Incumplida',
  'cancelada_devolucion_cancelacion' => 'Cancelada por Devolución o Cancelación',
);
