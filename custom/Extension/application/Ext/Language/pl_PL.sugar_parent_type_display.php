<?php
 // created: 2016-07-25 21:54:10

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Kontrahent',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Zadanie',
  'Opportunities' => 'Szansa',
  'Products' => 'Pozycja oferty',
  'Quotes' => 'Oferta',
  'Bugs' => 'Błędy',
  'Cases' => 'Zgłoszenie',
  'Leads' => 'Potencjalny klient',
  'Project' => 'Projekt',
  'ProjectTask' => 'Zadanie projektowe',
  'Prospects' => 'Odbiorca',
  'KBContents' => 'Baza wiedzy',
  'RevenueLineItems' => 'Pozycje szansy',
);