<?php
 // created: 2017-08-09 14:53:26

$app_list_strings['lead_status_dom']=array (
  '' => '',
  'New' => 'Nuevo',
  'Converted' => 'Convertir a Prospecto',
  'Asignado' => 'Asignado',
  'AsignadoPendiente' => 'Asignado-Pendiente de Autorización',
  'Reciclado' => 'Reciclado',
);