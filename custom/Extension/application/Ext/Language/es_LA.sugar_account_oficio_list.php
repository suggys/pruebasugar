<?php
 // created: 2017-07-14 17:31:57

$app_list_strings['account_oficio_list']=array (
  'albanil' => 'Albañil',
  'carpintero' => 'Carpintero',
  'electricista' => 'Electricista',
  'estuquero' => 'Estuquero',
  'herrero' => 'Herrero',
  'impermeabilizador' => 'Impermeabilizador',
  'instalador_de_climas' => 'Instalador de climas',
  'instalador_de_piso' => 'Instalador de piso',
  'jardinero' => 'Jardinero',
  'mtto_en_general' => 'Mtto. en general',
  'pintor' => 'Pintor',
  'plomero' => 'Plomero',
  'tabla_roquero' => 'Tabla roquero',
  'yesero' => 'Yesero',
  'otro' => 'Otro (especifique)',
);
