<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['moduleListSingular']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['estado_promesa_list']['Por Aplicar'] = 'Por Aplicar';
$app_list_strings['estado_promesa_list']['Aplicado'] = 'Aplicado';
$app_list_strings['estado_promesa_list']['No Aplicado'] = 'No Aplicado';
$app_list_strings['estado_list']['Por Aplicar'] = 'Por Aplicar';
$app_list_strings['estado_list']['Aplicado'] = 'Aplicado';
$app_list_strings['estado_list']['No Aplicado'] = 'No Aplicado';
$app_list_strings['Motivo_Solicitud_list']['Credito Disponible Temporal'] = 'Crédito Disponible Temporal';
$app_list_strings['Motivo_Solicitud_list']['Limite de Credito Temporal'] = 'Límite de Crédito Temporal';
$app_list_strings['Motivo_Solicitud_list']['Promesa de Pago'] = 'Promesa de Pago';
$app_list_strings['Motivo_Solicitud_list']['Otro'] = 'Otro';
