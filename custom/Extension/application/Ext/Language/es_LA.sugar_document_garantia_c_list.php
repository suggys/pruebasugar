<?php
 // created: 2016-09-14 17:43:28

$app_list_strings['document_garantia_c_list']=array (
  'Seleccione' => '-- Seleccione --',
  'Pagare' => 'Pagaré',
  'Contrato' => 'Contrato',
  'Cheque_Postfechado' => 'Cheque Postfechado',
  'Autorizacion' => 'Autorización',
  'Fianza' => 'Fianza',
  'Prendaria' => 'Prendaria',
  'Hipotecaria' => 'Hipotecaria',
  'Anticipo' => 'Anticipo',
  'Otro' => 'Otro (solicitar comentarios)',
);