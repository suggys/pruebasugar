<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ro_RO.CentroDeDisenio.php

// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/ro_RO.CentroDeDisenio.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Produs';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Utilizator';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Nou';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Alocat';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Închis';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Asteapta intrari';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Respins';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplicat';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Ridicat';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Mediu';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Scazut';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Acceptat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplicat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Închis';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Expirat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Nevalid';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Produs';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Utilizator';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Nou';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Alocat';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Închis';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Asteapta intrari';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Respins';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplicat';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Ridicat';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Mediu';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Scazut';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Acceptat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplicat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Închis';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Expirat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Nevalid';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';


?>
<?php
// Merged from custom/Extension/application/Ext/Language/temp.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['dise_prospectos_cocina_type_dom']['Administration'] = 'Administration';
$app_list_strings['dise_prospectos_cocina_type_dom']['Product'] = 'Produs';
$app_list_strings['dise_prospectos_cocina_type_dom']['User'] = 'Utilizator';
$app_list_strings['dise_prospectos_cocina_status_dom']['New'] = 'Nou';
$app_list_strings['dise_prospectos_cocina_status_dom']['Assigned'] = 'Alocat';
$app_list_strings['dise_prospectos_cocina_status_dom']['Closed'] = 'Închis';
$app_list_strings['dise_prospectos_cocina_status_dom']['Pending Input'] = 'Asteapta intrari';
$app_list_strings['dise_prospectos_cocina_status_dom']['Rejected'] = 'Respins';
$app_list_strings['dise_prospectos_cocina_status_dom']['Duplicate'] = 'Duplicat';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P1'] = 'Ridicat';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P2'] = 'Mediu';
$app_list_strings['dise_prospectos_cocina_priority_dom']['P3'] = 'Scazut';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Accepted'] = 'Acceptat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Duplicate'] = 'Duplicat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Closed'] = 'Închis';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Out of Date'] = 'Expirat';
$app_list_strings['dise_prospectos_cocina_resolution_dom']['Invalid'] = 'Nevalid';
$app_list_strings['dise_prospectos_cocina_resolution_dom'][''] = '';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleListSingular']['dise_Prospectos_cocina'] = 'Prospecto de Centro de Diseño';
$app_list_strings['tipo_persona_list']['Persona_fisica'] = 'Persona física';
$app_list_strings['tipo_persona_list']['Persona_moral'] = 'Persona_moral';
$app_list_strings['categoria_list']['Accesorios'] = 'Accesorios';
$app_list_strings['categoria_list']['Banio'] = 'Baño';
$app_list_strings['categoria_list']['Centro_de_entretenimiento'] = 'Centro de entretenimiento';
$app_list_strings['categoria_list']['Closet'] = 'Closet';
$app_list_strings['categoria_list']['Cocina'] = 'Cocina';
$app_list_strings['categoria_list']['Cubiertas'] = 'Cubiertas';
$app_list_strings['categoria_list']['Otros'] = 'Otros';
$app_list_strings['etapa_de_venta_list']['Prospeccion'] = 'Prospección';
$app_list_strings['etapa_de_venta_list']['Analisis'] = 'Análisis';
$app_list_strings['etapa_de_venta_list']['Disenio'] = 'Diseño';
$app_list_strings['etapa_de_venta_list']['Negociacion'] = 'Negociación';
$app_list_strings['etapa_de_venta_list']['Venta_gana'] = 'Venta ganada';
$app_list_strings['etapa_de_venta_list']['Venta_perdida'] = 'Venta perdida';
$app_list_strings['sucursal_c_list'][2935] = 'Linda Vista';
$app_list_strings['sucursal_c_list'][2936] = 'Sendero';
$app_list_strings['sucursal_c_list'][3233] = 'Saltillo';
$app_list_strings['sucursal_c_list'][3235] = 'Hermosillo';
$app_list_strings['sucursal_c_list'][3236] = 'Culiacán';
$app_list_strings['sucursal_c_list'][3255] = 'Chihuahua';
$app_list_strings['sucursal_c_list'][3227] = 'Cumbres';
$app_list_strings['sucursal_c_list'][3265] = 'Valle Alto';
$app_list_strings['sucursal_c_list'][3267] = 'Escobedo';
$app_list_strings['sucursal_c_list'][3271] = 'Oferre';
$app_list_strings['sucursal_c_list'][3165] = 'Garza Sada';
$app_list_strings['sucursal_c_list'][3389] = 'Pablo Livas';
$app_list_strings['sucursal_c_list'][4241] = 'DEV STORE';
$app_list_strings['sucursal_c_list'][7861] = 'PERF STORE';
$app_list_strings['sucursal_c_list'][''] = '';

?>
