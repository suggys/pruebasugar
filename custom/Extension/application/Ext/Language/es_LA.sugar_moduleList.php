<?php
// created: 2018-10-04 03:45:55
$app_list_strings['moduleList']['Home'] = 'Inicio';
$app_list_strings['moduleList']['Contacts'] = 'Contactos';
$app_list_strings['moduleList']['Accounts'] = 'Clientes';
$app_list_strings['moduleList']['Opportunities'] = 'Proyectos';
$app_list_strings['moduleList']['Cases'] = 'Casos';
$app_list_strings['moduleList']['Notes'] = 'Notas';
$app_list_strings['moduleList']['Calls'] = 'Llamadas';
$app_list_strings['moduleList']['Emails'] = 'Correos Electrónicos';
$app_list_strings['moduleList']['Meetings'] = 'Reuniones';
$app_list_strings['moduleList']['Tasks'] = 'Tareas';
$app_list_strings['moduleList']['Calendar'] = 'Calendario';
$app_list_strings['moduleList']['Leads'] = 'Leads';
$app_list_strings['moduleList']['Contracts'] = 'Contratos';
$app_list_strings['moduleList']['Quotes'] = 'Cotizaciones';
$app_list_strings['moduleList']['Products'] = 'Partidas Individuales Cotizadas';
$app_list_strings['moduleList']['Reports'] = 'Informes';
$app_list_strings['moduleList']['Forecasts'] = 'Previsiones';
$app_list_strings['moduleList']['Bugs'] = 'Errores';
$app_list_strings['moduleList']['Project'] = 'Proyectos';
$app_list_strings['moduleList']['Campaigns'] = 'Campañas';
$app_list_strings['moduleList']['Documents'] = 'Documentos';
$app_list_strings['moduleList']['pmse_Inbox'] = 'Procesos';
$app_list_strings['moduleList']['pmse_Project'] = 'Definiciones de Proceso';
$app_list_strings['moduleList']['pmse_Business_Rules'] = 'Proceso de Reglas Empresariales';
$app_list_strings['moduleList']['pmse_Emails_Templates'] = 'Proceso de Plantillas E-mail';
$app_list_strings['moduleList']['Prospects'] = 'Prospectos';
$app_list_strings['moduleList']['ProspectLists'] = 'Listas de Público Objetivo';
$app_list_strings['moduleList']['Tags'] = 'Etiquetas';
$app_list_strings['moduleList']['OutboundEmail'] = 'Opciones de Correo Electrónico';
$app_list_strings['moduleList']['DataPrivacy'] = 'Privacidad de datos';
$app_list_strings['moduleList']['KBContents'] = 'Base de Conocimiento';
$app_list_strings['moduleList']['RevenueLineItems'] = 'Artículos de Línea de Ganancia';
$app_list_strings['moduleList']['GPE_Grupo_empresas'] = 'Grupo de empresas';
$app_list_strings['moduleList']['orden_Ordenes'] = 'Órdenes';
$app_list_strings['moduleList']['NC_Notas_credito'] = 'Notas de crédito';
$app_list_strings['moduleList']['lowae_autorizacion_especial'] = 'Autorización Especial';
$app_list_strings['moduleList']['LF_Facturas'] = 'Facturas';
$app_list_strings['moduleList']['lowes_Pagos'] = 'Pagos';
$app_list_strings['moduleList']['Low01_Pagos_Facturas'] = 'Pagos_Facturas';
$app_list_strings['moduleList']['LOW02_Partidas_Orden'] = 'Partidas de Orden';
$app_list_strings['moduleList']['LO_Obras'] = 'Obras';
$app_list_strings['moduleList']['LOW01_SolicitudesCredito'] = 'Solicitudes de Crédito';
$app_list_strings['moduleList']['MRX1_Configuraciones'] = 'Configuraciones';
$app_list_strings['moduleList']['dise_Prospectos_cocina'] = 'Prospectos de Centro de Diseño';
$app_list_strings['moduleList']['ops_Backups'] = 'Backups';
$app_list_strings['moduleList']['FDP_Pago'] = 'Formas de Pago';
$app_list_strings['moduleList']['FDP_formasPago'] = 'Formas de Pago';
$app_list_strings['moduleList']['Low03_AgentesExternos'] = 'Agente Externo';
