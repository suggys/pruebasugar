<?php
// created: 2017-11-09 00:36:34
$role_dropdown_filters['estado_autorizacion_list'] = array (
  '' => true,
  'pendiente' => true,
  'autorizada' => true,
  'rechazada' => true,
  'finalizada' => false,
  'terminada_incumplida' => false,
  'cancelada_devolucion_cancelacion' => false,
);
