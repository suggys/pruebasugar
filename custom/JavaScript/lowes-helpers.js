/**
 * Handlebars helpers.
 *
 * These functions are to be used in handlebars templates.
 * @class Handlebars.helpers
 * @singleton
 */
(function(app) {
    app.events.on("app:init", function() {

        /**
         * convert a string to upper case
         */
        Handlebars.registerHelper("formatNumber", function (text)
        {
            return app.utils.formatNumberLocale(text)
        });

    });
})(SUGAR.App);
