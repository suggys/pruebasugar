(function(app) {
  app.events.on("app:sync:complete", function(){
    /*call to the custom api to disallow create for Standard Users.*/
    console.log('saludos . estamos en :: '+Backbone.history.fragment);
    if(Backbone.history.fragment == 'LO_Obras'){
      var acls = app.user.getAcls();
      acls.LO_Obras.create = 'no';
      app.user.set("acls", acls);
    }
    if(Backbone.history.fragment == 'Accounts'){
      var acls = app.user.getAcls();
      acls.Accounts.create = 'no';
      app.user.set("acls", acls);
    }
    if(Backbone.history.fragment == 'Contacts'){
      var acls = app.user.getAcls();
      acls.Contacts.create = 'no';
      app.user.set("acls", acls);
    }
  });
})(SUGAR.App);
