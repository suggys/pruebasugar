<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class customEmailsVisibility extends SugarVisibility {

  public function addVisibilityQuery(SugarQuery $query)
  {
    global $current_user, $timedate;
    $roleGerenteVentasCom = BeanFactory::getBean("ACLRoles", 'b96cf8f0-7159-11e7-872a-060b37ffb723');
    $isUserValidGerenteVentasCom = $current_user->check_role_membership($roleGerenteVentasCom->name, $current_user->id);
    if($isUserValidGerenteVentasCom){
      $query->joinTable('users_cstm', array('alias' => 'usrs'))->on()->equalsField('emails.assigned_user_id','usrs.id_c');
      $query->where()->equals("id_sucursal_c", $current_user->sucursal_c);
      $query->where()->equals("usrs.sucursal_c", $current_user->sucursal_c);
    }
    return $query;
  }
}
?>
