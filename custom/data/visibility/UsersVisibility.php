<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class UsersVisibility extends SugarVisibility {

  public function addVisibilityQuery(SugarQuery $query)
  {
    global $current_user, $timedate;
    $roleGerenteVentas = BeanFactory::getBean("ACLRoles", 'b96cf8f0-7159-11e7-872a-060b37ffb723');
    $isUserValidGerenteVentas = $current_user->check_role_membership($roleGerenteVentas->name, $current_user->id);
    if($isUserValidGerenteVentas){
      $query->where()->equals("sucursal_c", $current_user->sucursal_c);
    }
    return $query;
  }
}
?>
