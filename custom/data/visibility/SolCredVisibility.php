<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class SolCredVisibility extends SugarVisibility {

  public function addVisibilityQuery(SugarQuery $query)
  {
    global $current_user, $timedate;
    $roleAsesorComercial = BeanFactory::getBean("ACLRoles", 'c954058c-715a-11e7-b290-02a6691319f3');
    $isUserAsesorComercial = $current_user->check_role_membership($roleAsesorComercial->name, $current_user->id);

    $roleGerenteVentas = BeanFactory::getBean("ACLRoles", 'b96cf8f0-7159-11e7-872a-060b37ffb723');
    $isUserValidGerenteVentas = $current_user->check_role_membership($roleGerenteVentas->name, $current_user->id);
    //Analista
    $roleAnalistaCredito = BeanFactory::getBean("ACLRoles", '69b8f6d6-7aa3-11e6-91c0-027a430c0995');
    $isUserValidAnalistaCredito = $current_user->check_role_membership($roleAnalistaCredito->name, $current_user->id);
    if($isUserAsesorComercial){
      $query->joinTable("(
        SELECT sp.low01_solicitudescredito_prospectslow01_solicitudescredito_idb id
        FROM prospects p
        JOIN low01_solicitudescredito_prospects_c sp on sp.low01_solicitudescredito_prospectsprospects_ida = p.id and sp.deleted = 0
        WHERE p.assigned_user_id = '$current_user->id'
        AND p.deleted = 0
        UNION
        SELECT sa.low01_solicitudescredito_accountslow01_solicitudescredito_idb id
        FROM accounts a
        JOIN low01_solicitudescredito_accounts_c sa on sa.low01_solicitudescredito_accountsaccounts_ida = a.id and sa.deleted = 0
        WHERE a.assigned_user_id = '$current_user->id'
        AND a.deleted = 0)", ["alias" => "solicitudes_asesor"])
        ->on()->equalsField("solicitudes_asesor.id",  "low01_solicitudescredito.id");
      // cambio -compileSql- por -execute- por upgrade 7.8 a 7.9
      // SugarQuery::compileSql() is deprecated. Use SugarQuery::execute() instead.
      $GLOBALS["log"]->fatal("query:".$query->execute());
      //$GLOBALS["log"]->fatal("query:".$query->compileSql());
      // $this->getSolicitudes();

    }
    if($isUserValidGerenteVentas || $isUserValidAnalistaCredito){
      $query->joinTable("(
        SELECT sp.low01_solicitudescredito_prospectslow01_solicitudescredito_idb id
        FROM prospects p
        INNER JOIN prospects_cstm pc on pc.id_c = p.id
        JOIN low01_solicitudescredito_prospects_c sp on sp.low01_solicitudescredito_prospectsprospects_ida = p.id and sp.deleted = 0
        WHERE p.deleted = 0
        AND pc.sucursal_c = '$current_user->sucursal_c'
        UNION
        SELECT sa.low01_solicitudescredito_accountslow01_solicitudescredito_idb id
        FROM accounts a
        INNER JOIN accounts_cstm ac on ac.id_c = a.id
        JOIN low01_solicitudescredito_accounts_c sa on sa.low01_solicitudescredito_accountsaccounts_ida = a.id and sa.deleted = 0
        WHERE ac.id_sucursal_c = '$current_user->sucursal_c'
        AND a.deleted = 0
        )", ["alias" => "solicitudes_sucursal"])
        ->on()->equalsField("solicitudes_sucursal.id",  "low01_solicitudescredito.id");
      // $query->where()->equals("created_by", $current_user->id);
      // $this->getSolicitudes();
    }
    return $query;
  }

  public function getSolicitudes()
  {
    $bean = BeanFactory::newBean('Users');
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $sugarQuery->select(array('id', 'user_name', 'sucursal_c'));

    $records = $sugarQuery->execute();
  }
}
?>
