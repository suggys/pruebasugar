<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class AccountsVisibility extends SugarVisibility {

  public function addVisibilityQuery(SugarQuery $query)
  {
    global $current_user, $timedate;
    

    // @Laimu AMC 18/04/26
    // Se comentarizo por la situacion de problemas con el upgrade de Sugar 7.9 
    // Aun se esta trabajando en la solucion
 

     // $GLOBALS["log"]->setLevel('debug');


    $preparedStmtAdd = $query->compile();
    $sqlPrev = $preparedStmtAdd->getSQL();

    $txtAccounts = '';


    /*
      @Laimu AMC 180507

      Se verifica si dentro del query, es de tipo Clientes, o si en el query
      viene con el prefijo "jt0_", de ser asi, le cambia el nombre de la variable
      para que no falle el query.
    
      @Laimu AMC 180520

      Se hacen ajustes para que el sinonimo de la tabla pueda ser cualquiera,
      ya que al parecer puede estar cambiando aleatoreamente.

    */ 

    $idxStr = stripos($sqlPrev, "LEFT JOIN accounts ");

    if ( stripos($sqlPrev, "accounts record") !== false ){
      $txtAccounts = 'record.id';
    }
    else{

      if ( $idxStr == FALSE ){

        // INNER JOIN, se agrego esta seccion para la parte de grupos de empresas
        // en lugar de LEFT, se agrega la misma logica pero con el INNER

        $idxStr1 = stripos($sqlPrev, "INNER JOIN accounts ");

        if ( $idxStr1 == FALSE ){
          $txtAccounts = 'accounts.id';
        }
        else{

            $idxStrLEFT1 =  stripos($sqlPrev, "ON", $idxStr1 + 19 );

            if ( $idxStrLEFT1 !== false ){
            
              // Se busca el sinonimo
              $txtAccounts =substr ($sqlPrev, $idxStr1 + 19 , ( $idxStrLEFT1 - 1 ) - ( $idxStr1 + 19 )  );
              $txtAccounts = trim( $txtAccounts );
    
              // se agrega el ID
              $txtAccounts .= '.id';
            }
            else{
              $txtAccounts = 'accounts.id';
            }
        }


      } 
      else
      {

        $idxStrLEFT =  stripos($sqlPrev, "ON", $idxStr + 18 );

        if ( $idxStrLEFT !== false ){
         
          // Se busca el sinonimo
          $txtAccounts =substr ($sqlPrev, $idxStr + 18 , ( $idxStrLEFT - 1 ) - ( $idxStr + 18 )  );
          $txtAccounts = trim( $txtAccounts );

          // se agrega el ID
          $txtAccounts .= '.id';
        }
        else{
          $txtAccounts = 'accounts.id';
        }

      }
     
    }

  // $GLOBALS["log"]->fatal("Query Accts Vis (txtAccounts):". $txtAccounts );  


   if(!$current_user->is_admin){

      $query->joinTable("
        (
        SELECT pac.prospects_accounts_1accounts_idb id FROM prospects_accounts_1_c pac
        JOIN prospects p ON pac.prospects_accounts_1prospects_ida = p.id
        LEFT JOIN prospects_cstm pc ON pc.id_c = p.id
        WHERE pc.estado_c = 'convertido' AND pac.deleted = 0 AND p.deleted = 0
        UNION
        SELECT a.id FROM accounts a
        LEFT JOIN prospects_accounts_1_c pac ON pac.prospects_accounts_1accounts_idb = a.id
        WHERE pac.prospects_accounts_1accounts_idb IS NULL AND a.deleted = 0
        )", ['alias' => 'clientes_reales'])->on()->equalsField('clientes_reales.id', $txtAccounts ) ;
         
  }

  /*
  
   $preparedStmtAdd = $query->compile();
   $GLOBALS["log"]->fatal("Query Accts Vis:". $preparedStmtAdd->getSQL() );  

    $paramsQry = $preparedStmtAdd->getParameters();
    $i = 0;
    $txt = "";
    foreach ($paramsQry as $v) {
      $txt .= "[" . $i ."," . $v . "]";
      $i++;
    }

   $GLOBALS["log"]->fatal("Query Accts Vis: ParamsQry:". $txt  );
*/
   
    return $query;
  }
}
?>
