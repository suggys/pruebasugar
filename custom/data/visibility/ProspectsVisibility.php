<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class ProspectsVisibility extends SugarVisibility {

  public function addVisibilityQuery(SugarQuery $query)
  {
    global $current_user, $timedate;
    $roleGerenteVentas = BeanFactory::getBean("ACLRoles", 'b96cf8f0-7159-11e7-872a-060b37ffb723');
    $isUserValidGerenteVentas = $current_user->check_role_membership($roleGerenteVentas->name, $current_user->id);
    $roleAnalistaCredito = BeanFactory::getBean("ACLRoles", '69b8f6d6-7aa3-11e6-91c0-027a430c0995');
    $isUserValidAnalistaCredito = $current_user->check_role_membership($roleAnalistaCredito->name, $current_user->id);
    $roleAsesorComercial = BeanFactory::getBean("ACLRoles", 'c954058c-715a-11e7-b290-02a6691319f3');
    $isUserAsesorComercial = $current_user->check_role_membership($roleAsesorComercial->name, $current_user->id);
    if($isUserValidGerenteVentas || $isUserValidAnalistaCredito){
      $query->where()->equals("sucursal_c", $current_user->sucursal_c);
    }
    $query->where()->notEquals("estado_c", "convertido");

    return $query;
  }
}
?>
