<?php

namespace Sugarcrm\Sugarcrm\custom\Console\Command;

use Sugarcrm\Sugarcrm\Console\CommandRegistry\Mode\InstanceModeInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

require_once 'tests/custom/CustomSugarTestAccountsUtilities.php';
require_once 'data/BeanFactory.php';
require_once 'include/SugarDateTime.php';

use CustomSugarTestAccountsUtilities;
use SugarDateTime;
use BeanFactory;
use DateInterval;

class LowesTestDataPopulateCommand extends Command implements InstanceModeInterface
{
    private $accountByIds = [];
    protected function configure()
    {
        $this
            ->setName('lowes:populateqa')
            ->setDescription('Llena la instancia con escenarios de prueba')
            ->setHelp('Este comando no acepta parametros.')
            ->addOption(
                'mode',
                null,
                InputOption::VALUE_OPTIONAL,
                'Comma separated list of modules to be purged from queue. If no module(s) are specified ' .
                'all records from any disabled modules will be purged.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Merx Populate QA Scenarios...");
        $accountsUtils = new CustomSugarTestAccountsUtilities();

        $mode = $input->getOption('mode') ? $input->getOption('mode') : "escenarios";

        if($mode === 'escenarios'){
          $accountsUtils->createEscenarios();
        }
        else{
          $this->createDemoData($output);
        }
        $output->writeln("Listo para probar :D");
    }

    private function createDemoData($output)
    {
      $output->writeln("clearDataBase");
      $this->clearDataBase();
      $output->writeln("createGruposEmpresa");
      $this->createGruposEmpresa();
      $output->writeln("createAccounts");
      $this->createAccounts();
      $output->writeln("createOrdenes");
      $this->createOrdenes();
      $output->writeln("createFacturas");
      $this->createFacturas();
      $output->writeln("createPagos");
      $this->createPagos();
      $output->writeln("createNotasCreditos");
      $this->createNotasCreditos();

    }

    public function createGruposEmpresa()
    {
      $this->grupos = [];
      $data = [
        ['name' => 'Grupo de construcción de Sureste', 'administrar_credito_grupal' => false],
        ['name' => 'Construcciones el tigre', 'administrar_credito_grupal' => false],
        ['name' => 'Altavista inbiliaria', 'administrar_credito_grupal' => false],
        ['name' => 'Grupo Inmobiliario y Constructor Olimpo', 'administrar_credito_grupal' => false],
        ['name' => 'Constructora Romatsa, S.A. De C.V.', 'administrar_credito_grupal' => false],
        ['name' => 'Gurrieb & Asociados, S.A. De C.V.', 'administrar_credito_grupal' => true],
        ['name' => 'PTS INGENIERIA', 'administrar_credito_grupal' => true],
        //['name' => 'Empresa Constructora Orizabeña, S.A. De C.V.', 'administrar_credito_grupal' => true],['name' => 'Barmac Construcción', 'administrar_credito_grupal' => true],['name' => 'Grupo Constructo, S.A. De C.V.', 'administrar_credito_grupal' => true],['name' => 'Ingeniería Y Procesos De Construcción, S.A. De C.V.', 'administrar_credito_grupal' => true],['name' => 'GRUPO CCEIC', 'administrar_credito_grupal' => true],['name' => 'Gutsa Construcciones, S.A. De C.V.', 'administrar_credito_grupal' => true],['name' => 'ICA S.A. de C.V.', 'administrar_credito_grupal' => true],['name' => 'AMERIC', 'administrar_credito_grupal' => true],['name' => 'CYR Construcciones, S.A. De C.V.', 'administrar_credito_grupal' => true],['name' => 'Arendal, S. De R.L. De C.V.', 'administrar_credito_grupal' => true],['name' => 'Estuco Recubrimientos, S.A. De C.V.', 'administrar_credito_grupal' => true],['name' => 'Carso', 'administrar_credito_grupal' => true],
      ];
      foreach($data as $grupoData){
        $grupo = BeanFactory::newBean('GPE_Grupo_empresas');
        $grupo->name = $grupoData['name'];
        $grupo->administrar_credito_grupal = $grupoData['administrar_credito_grupal'];
        $grupo->save(false);
        $this->grupos[] = $grupo;
      }
    }

    private function createAccounts()
    {
      $this->accounts = [];
      $cps = ["83175","83188","40000","36560","36568","91110","23000","47400","37545","37440","81200","81200","84160","28239","87370","82018","97288","21290","07700","25750",];
      $calles = ["Diego de Montemayor","Capitán Emilio Carranza","Isaac Garza","Prof. Serafín Peña","Valentín Gómez Farías","General Francisco Naranjo","Hermenegildo Galeana","Ignacio Zaragoza","América","15 de mayo","General Juan Zuazua","Venustiano Carranza","Vicente Guerrero","José Ma. Arteaga","Luis Carvajal y de la Cueva","Avenida Benito Juárez","Avenida José Ma. Pino Suárez","Calzada Francisco I. Madero","Modesto Arreola","Jalisco, Querétaro, 16 de Septiembre","Hilario Martínez","General Julián Villagrán","Porfirio Díaz","Cristobal Colón","Reforma","Guillermo Prieto","Dr. José María Coss(Dr. Coss)","Nicolás Bravo","Ignacio López Rayón","Juan Aldama",];
      $ciudades = ["Acapulco","Aguascalientes","Cabo San Lucas","Caborca","Campeche","Cancun","Celaya","Chihuahua","Chilpancingo De Los Bravo","Ciudad Del Carmen","Ciudad Guzman","Cd. Juarez","Ciudad Obregon","Cd. Victoria","Coatzacoalcos","Colima","Colima","Cordoba","Cuauhtemoc","Jiutepec","Culiacan","Culiacan","Delicias","Durango","Ensenada","Guadalajara","Tlaquepaque","Tonala","Guamuchil","Guanajuato","Guasave","Guaymas","Hermosillo","Hermosillo","Iguala","Irapuato","Irapuato","Xalapa","La Paz","Lagos De Moreno","Leon","Leon","Los Mochis","Los Mochis","Magdalena De Kino Ciudad","Manzanillo","Matamoros","Mazatlan","Merida","Mexicali","Ciudad De Mexico","Monclova","San Nicolas De Los Garza","San Nicolas De Los Garza","Monterrey","Morelia","Uriangato","Navojoa","Nogales","Nuevo Laredo","Oaxaca De Juarez","Orizaba","Mineral De La Reforma","Puebla","Puerto Peñasco","Puerto Vallarta","Queretaro","Queretaro","Reynosa","Saltillo","San Jose Del Cabo","San Juan Del Rio","San Luis Potosi","San Luis Rio Colorado","San Miguel De Allende","Altamira","Tecate","Tehuacan","Tepic","Tijuana","Tlalnepantla De Baz","Tlaxcala","Toluca","Torreon","Tuxtla Gutierrez","Uruapan","Veracruz","Villahermosa","Zacatecas","Zapopan","Zapotlanejo",];
      $estados = ["AGU","BCN","BCS","CAM","CHP","CHH","COA","COL","CMX","DUR","GUA","GRO","HID","JAL","MEX","MIC","MOR","NAY","NLE","OAX","PUE","QUE","ROO","SLP","SIN","SON","TAB","TAM","TLA","VER","YUC","ZAC",];
      $data = [];
      $count = 1;
      foreach($this->grupos as $grupo){

        $grupo->load_relationship('gpe_grupo_empresas_accounts');
        $account = BeanFactory::newBean('Accounts');
        $indexStr = str_pad($count, 2, "0", STR_PAD_LEFT);
        $account->name = $grupo->name . " " . $indexStr;
        $account->limite_credito_autorizado_c = 20000;
        $account->email = "a1@gmail.com";
    		$account->id_cajero_c = "1234".$count;
    		$account->id_sucursal_c = "2935";
    		$account->ownership = "Juan Perez";
        $account->rfc = "AAAA0000$indexStr"."A$indexStr";
        $account->phone_office = "55555555$indexStr";
        $account->primary_address_numero_c = $count;
        $account->primary_address_postalcode_c = $cps[rand (1 , 19)];
        $account->primary_address_street_c = $calles[rand (1 , 29)];
        $account->primary_address_city_c = $ciudades[rand (1 , 9)];
        $account->primary_address_state_c = $estados[rand (1 , 31)];
        $account->save(false);
        $this->accounts[] = $account;
        $grupo->gpe_grupo_empresas_accounts->add($account);
        $this->accountByIds[$account->id] = $account;
        $count++;

        $indexStr = str_pad($count, 2, "0", STR_PAD_LEFT);
        $account = BeanFactory::newBean('Accounts');
        $account->name = $grupo->name . " " . $indexStr;
        $account->limite_credito_autorizado_c = 10000;
        $account->email = "a1@gmail.com";
    		$account->id_cajero_c = "1234".$count;
    		$account->id_sucursal_c = "2935";
    		$account->ownership = "Juan Perez";
        $account->rfc = "AAAA0000$indexStr"."A$indexStr";
        $account->phone_office = "55555555$indexStr";
        $account->primary_address_numero_c = $count;
        $account->primary_address_postalcode_c = $cps[rand (1 , 19)];
        $account->primary_address_street_c = $calles[rand (1 , 29)];
        $account->primary_address_city_c = $ciudades[rand (1 , 9)];
        $account->primary_address_state_c = $estados[rand (1 , 31)];
        $account->save(false);
        $this->accounts[] = $account;
        $grupo->gpe_grupo_empresas_accounts->add($account);
        $this->accountByIds[$account->id] = $account;
        $count++;
      }
    }

    public function createOrdenes()
    {
      $this->ordenesCompletadas = [];
      $this->ordenesCompletadasCanceladas = [];
      $this->ordenesCanceladas = [];
      $count = 1;
      foreach ($this->accounts as $account) {
        $indexStr = str_pad($count, 3, "0", STR_PAD_LEFT);
        $orden1 = BeanFactory::newBean('orden_Ordenes');
        $orden1->name = '029351762016120'.$indexStr;
        $orden1->no_ticket_c = '2935'.$indexStr;
        $orden1->id_sucursal_c = '2935';
        $orden1->estado_c = '1';
        $orden1->amount_c = rand (1000 , 4000);
        $orden1->id_customer_c = $account->id_ar_credit_c;
        $orden1->save(false);
        $count++;

        $indexStr = str_pad($count, 3, "0", STR_PAD_LEFT);
        $orden2 = BeanFactory::newBean('orden_Ordenes');
        $orden2->name = '029351762016120'.$indexStr;
        $orden2->no_ticket_c = '2935'.$indexStr;
        $orden2->id_sucursal_c = '2935';
        $orden2->estado_c = '5';
        $orden2->amount_c = $orden1->amount_c;
        $orden2->folio_orden_inicial_c = $orden1->no_ticket_c;
        $orden2->id_customer_c = $account->id_ar_credit_c;
        $orden2->save(false);
        $this->ordenesCompletadas[] = $orden2;
        $count++;

      }
    }

    public function createFacturas()
    {
      global $current_user, $timedate;
      $this->facturas = [];
      $count = 1;
      foreach ($this->ordenesCompletadas as $orden) {
        $indexStr = str_pad($count, 3, "0", STR_PAD_LEFT);
        $fecha = new SugarDateTime('now');
        $fecha = $fecha->sub(new DateInterval('P'.rand(0,30).'D'));
        $fechaEmision = $timedate->tzUser($fecha, $current_user)->setTime(0,0)->asDb();
        $fechaEntrega = $timedate->tzUser($fecha->add(new DateInterval('P'.rand(5,10).'D')), $current_user)->setTime(0,0)->asDb();

        $factura = BeanFactory::newBean('LF_Facturas');
        $factura->name = '6'.$indexStr;
        $factura->importe = $orden->amount_c;
        $factura->no_ticket = $orden->no_ticket_c;
        $factura->dt_crt = $fechaEmision;
        $factura->fecha_entrega = $fechaEntrega;
        $factura->save(false);
        $this->facturas[] = $factura;
        $count++;
      }
    }

    public function createPagos($value='')
    {
      global $current_user, $timedate;
      $this->pagos = [];
      $count = 0;
      foreach ($this->facturas as $factura) {
        $account = $this->accountByIds[$factura->accounts_lf_facturas_1accounts_ida];
        $fecha = new SugarDateTime('now');
        $fecha = $fecha->sub(new DateInterval('P'.rand(1,30).'D'));
        $fechaTransaccion = $timedate->tzUser($fecha, $current_user)->setTime(0,0)->asDbDate();
        $pago = BeanFactory::newBean('lowes_Pagos');
        $pago->forma_pago = $count%2 ? 'Efectivo' : 'Cheque';
        $pago->monto = $factura->importe;
        $pago->id_customer_c = $account->id_ar_credit_c;
        $pago->fecha_transaccion = $fechaTransaccion;
        $pago->save(false);
        $this->pagos[] = $pago;
        $count++;
      }
    }

    public function createNotasCreditos($value='')
    {
      global $current_user, $timedate;
      $this->notasCreditos = [];
      $count = 1;
      foreach ($this->accounts as $account) {
        $indexStr = str_pad($count, 3, "0", STR_PAD_LEFT);
        $fecha = new SugarDateTime('now');
        $fecha = $fecha->sub(new DateInterval('P'.rand(1,30).'D'));
        $fechaEmision = $timedate->tzUser($fecha, $current_user)->setTime(0,0)->asDbDate();
        $notaCredito = BeanFactory::newBean('NC_Notas_credito');
        $notaCredito->name = '7'.$indexStr;
        $notaCredito->importe_total = rand(1000, 4000);
        $notaCredito->fecha_emision_c = $fechaEmision;
        $notaCredito->save(false);
        $account->load_relationship('nc_notas_credito_accounts');
        $account->nc_notas_credito_accounts->add($notaCredito);
        $this->notasCreditos[] = $notaCredito;
        $count++;
      }
    }

    public function clearDataBase($value='')
    {
      $GLOBALS['db']->query("DELETE FROM accounts");
      $GLOBALS['db']->query("DELETE FROM accounts_cstm");

      $GLOBALS['db']->query("DELETE FROM lowes_pagos");
      $GLOBALS['db']->query("DELETE FROM lowes_pagos_cstm");

      $GLOBALS['db']->query("DELETE FROM lf_facturas");
      $GLOBALS['db']->query("DELETE FROM lf_facturas_cstm");

      $GLOBALS['db']->query("DELETE FROM nc_notas_credito");
      $GLOBALS['db']->query("DELETE FROM nc_notas_credito_cstm");

      $GLOBALS['db']->query("DELETE FROM orden_ordenes");
      $GLOBALS['db']->query("DELETE FROM orden_ordenes_cstm");

      $GLOBALS['db']->query("DELETE FROM gpe_grupo_empresas");
      $GLOBALS['db']->query("DELETE FROM gpe_grupo_empresas_cstm");

      $GLOBALS['db']->query("DELETE FROM lowae_autorizacion_especial");
      $GLOBALS['db']->query("DELETE FROM lowae_autorizacion_especial_cstm");

      $GLOBALS['db']->query("DELETE FROM nc_notas_credito");
      $GLOBALS['db']->query("DELETE FROM nc_notas_credito_cstm");

      $GLOBALS['db']->query("DELETE FROM lowae_autorizacion_especial");
      $GLOBALS['db']->query("DELETE FROM lowae_autorizacion_especial_cstm");

      $GLOBALS['db']->query("DELETE FROM gpe_grupo_empresas");

    }
}
