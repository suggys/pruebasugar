<?php
require_once 'custom/modules/MRX1_Configuraciones/CustomMRX1Configuraciones.php';
require_once 'custom/include/CustomHttpRequest.php';
/**
 *
 */
class PagosKonesh
{
  public function __construct()
  {
    $configurator = new Configurator();
    $configurator->loadConfig();
    $this->hostname = $configurator->config['host_name'];
    $this->urlApi = "convert2json/lowes/PagoKonesh";

    $admin = new CustomMRX1Configuraciones();
    $admin->retrieveSettings();
    if(empty($admin->settings['kones:limit_pagos'])){
      $admin->saveSetting("kones:limit_pagos", 100);
      $admin->retrieveSettings();
    }
    $this->limitPagos = $admin->settings['kones:limit_pagos'];
  }

  public function isFactoraje($id_pago){
    $GLOBALS["log"]->fatal("Llego aqui factoraje");
    global $current_user,$db;
    $bean = BeanFactory::newBean('Low01_Pagos_Facturas');
    $sugarQuery = new SugarQuery();
    $sugarQuery->select(array('estatus_fac_c'));
    $sugarQuery->from($bean, array('team_security' => false));
    $sugarQuery->where()
               ->notEquals("id_c", "")
               ->equals("low01_pagos_facturas_lowes_pagoslowes_pagos_ida", $id_pago);
    $compiled = $sugarQuery->compile();
    
    $pagos_fac = $sugarQuery->execute();
    $band=false;
    foreach ($pagos_fac as $ren) {
      if($ren[estatus_fac_c]=="Con factoraje"){
        $band=true;
      }
    }
    return $band;
  }

  public function getMontoFactoraje($id_pago){
    global $current_user,$db;
    $GLOBALS['log']->fatal("Llego a sacar el monto");
    $bean = BeanFactory::newBean('Low01_Pagos_Facturas');
    $sugarQuery = new SugarQuery();
    $sugarQuery->select(array('aforo_c', 'intereses_c', 'honorarios_c'));
    $sugarQuery->from($bean, array('team_security' => false));
    $sugarQuery->where()
               ->notEquals("id_c", "")
               ->equals("low01_pagos_facturas_lowes_pagoslowes_pagos_ida", $id_pago);
    $compiled = $sugarQuery->compile();
    $pago_fac = $sugarQuery->execute();
    
    foreach ($pago_fac as $ren) {
      if($ren[honorarios_c]!=" "){
        $GLOBALS["log"]->fatal("Entro al if del monto");
        $monto = $ren[aforo_c] + $ren[honorarios_c] + $ren[intereses_c];
      }
    }

    return $monto;
  }

  public function send(CustomHttpRequest $req, $pago, $data)
  {
    $jsonData = json_encode($data);
    $req->setOption(CURLOPT_CUSTOMREQUEST, "POST");
    $req->setOption(CURLOPT_POSTFIELDS, $jsonData);
    $req->setOption(CURLOPT_RETURNTRANSFER, true);
    $req->setOption(CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData))
    );
    $result = $req->execute();

    $statusCode = $req->getInfo(CURLINFO_HTTP_CODE);
    // $GLOBALS["log"]->fatal("send:statusCode:".$statusCode);
    if($statusCode === 200){
      $pago->folio_konesh_c = $data["COMPROBANTE"]["Folio"];
      $pago->estado_konesh_c = "Enviado";
      $pago->save(false);
      $admin = new CustomMRX1Configuraciones();
      $admin->retrieveSettings();
      $admin->saveSetting("folio_konesh", $data["COMPROBANTE"]["Folio"]);
      return true;
    }
    else {
      $note = BeanFactory::newBean('Notes');
      $note->name = "ERROR INTERFACE KONESH - ". $statusCode;
      $note->description = "ERROR INTERFACE KONESH - ". $statusCode;
      $note->assigned_user_id = '1';
      $note->save();
      return false;
    }
  }

  public function sendComplemento($url, $cheque ,$data){
    $GLOBALS["log"]->fatal("-----------------------Complemento de pago");
    $GLOBALS["log"]->fatal($data);
    $jsonData = json_encode($data);
    $req = curl_init($url);
    curl_setopt($req, CURLOPT_POST, true); 
    curl_setopt($req, CURLOPT_POSTFIELDS, "jsonData=".$jsonData);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($req);
    
    $statusCode =json_decode($result, true);
    //$GLOBALS["log"]->fatal("send:statusCode:".$statusCode["code"]);
    if($statusCode["code"] === 200){
      $pago->folio_konesh_c = $data["COMPROBANTE"]["Folio"];
      $pago->estado_konesh_c = "Enviado";
      $pago->save(false);
      $admin = new CustomMRX1Configuraciones();
      $admin->retrieveSettings();
      $admin->saveSetting("folio_konesh", $data["COMPROBANTE"]["Folio"]);
      return true;
    }
    else {
      $note = BeanFactory::newBean('Notes');
      $note->name = "ERROR INTERFACE KONESH - ". $statusCode;
      $note->description = "ERROR INTERFACE KONESH - ". $statusCode;
      $note->assigned_user_id = '1';
      $note->save();
      return false;
    }
  }
  
  public function sendCheques($url, $cheque ,$data ){
    $jsonData=json_encode($data);
    $req = curl_init($url);
    curl_setopt($req, CURLOPT_POST, true); 
    curl_setopt($req, CURLOPT_POSTFIELDS, "jsonData=".$jsonData);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($req);
    $GLOBALS['log']->fatal('Cheques... '.json_decode($result));
    $GLOBALS["log"]->fatal("send:statusCode: ".$result);

    if($statusCode === 200){
      $cheque->estatus_envio="Enviado";
      $cheque->save;
      return true;
    }else{
      return false;
    }
  }

  public function getPagos()
  {
    global $current_user,$db;
    $usertimezone = $current_user->getPreference('timezone');
    // $date_bean = new SugarDateTime("2017-10-19", new DateTimeZone($usertimezone));
	  $date_bean = new SugarDateTime("2016-01-01", new DateTimeZone($usertimezone));
    $dtcrt = $date_bean->asDb(true);
    $bean = BeanFactory::newBean('lowes_Pagos');
    $sugarQuery = new SugarQuery();   
    $sugarQuery->from($bean, ['team_security' => false]);    
    $sugarQuery->join("lowes_pagos_accounts", ['team_security' => false]);
    // $sugarQuery->join("accounts", ['team_security' => false]);
    $sugarQuery->joinTable("accounts",['alias' => "jt0_lowes_pagos_accounts"])->on()->
      equalsField("jt0_lowes_pagos_accounts.id","lowes_pagos_accounts.lowes_pagos_accountsaccounts_ida");  
    // $accountJoin = $sugarQuery->join("accounts", ['team_security' => false])->joinName(); 
    $sugarQuery->joinTable("accounts_cstm",['alias' => "jt0_accounts_cstm"])->on()->equalsField("jt0_accounts_cstm.id_c","jt0_lowes_pagos_accounts.id"); 
    $sugarQuery->joinTable("(
      SELECT low01_pagos_facturas_lowes_pagos_c.low01_pagos_facturas_lowes_pagoslowes_pagos_ida pago_id
      FROM low01_pagos_facturas
      INNER JOIN low01_pagos_facturas_lf_facturas_c ON low01_pagos_facturas_lf_facturas_c.low01_pagos_facturas_lf_facturaslow01_pagos_facturas_idb = low01_pagos_facturas.id AND low01_pagos_facturas_lf_facturas_c.deleted = 0
      INNER JOIN low01_pagos_facturas_lowes_pagos_c ON low01_pagos_facturas_lowes_pagos_c.low01_pagos_facturas_lowes_pagoslow01_pagos_facturas_idb = low01_pagos_facturas.id AND low01_pagos_facturas_lowes_pagos_c.deleted = 0
      INNER JOIN lf_facturas ON lf_facturas.id = low01_pagos_facturas_lf_facturas_c.low01_pagos_facturas_lf_facturaslf_facturas_ida AND lf_facturas.deleted = 0
      INNER JOIN lowes_pagos ON lowes_pagos.id = low01_pagos_facturas_lowes_pagos_c.low01_pagos_facturas_lowes_pagoslowes_pagos_ida AND low01_pagos_facturas_lowes_pagos_c.deleted = 0
      INNER JOIN lowes_pagos_cstm ON lowes_pagos_cstm.id_c = lowes_pagos.id
      WHERE lf_facturas.dt_crt > '$dtcrt'
	  AND lowes_pagos.fecha_transaccion >= '2018-09-01'
      AND lowes_pagos_cstm.estado_konesh_c IN ('Aplicado','Reaplicado')
      GROUP BY 1
      )", ["alias" => "pagos_facturas"])
      ->on()->equalsField("pagos_facturas.pago_id",  "lowes_pagos.id");
    $sugarQuery->whereRaw(" (jt0_accounts_cstm.con_factoraje_c = 0) OR (jt0_accounts_cstm.con_factoraje_c = 1 AND lowes_pagos_cstm.comision_factoraje_c IS NOT NULL)");
    
    $sugarQuery->limit($this->limitPagos);
    $sugarQuery->select(array('id', 'name','estado_konesh_c','comision_factoraje_c'));
    $compiled = $sugarQuery->compile();

    $query=$compiled->getSQL();
    $parameters = $compiled->getParameters();
    $GLOBALS['log']->fatal(".......::Query PAGOS::...... ".$query);
    $GLOBALS['log']->fatal(".......::Parametros::...... ".json_encode($parameters));


    $pagos = $sugarQuery->execute(); 
    
    return $pagos;
  }

  public function getFacturas($pago)
  {
    global $current_user;
    $usertimezone = $current_user->getPreference('timezone');
    $date_bean = new SugarDateTime("2017-10-19", new DateTimeZone($usertimezone));
    $dtcrt = $date_bean->asDb(true);
    // $GLOBALS["log"]->fatal("dtcrt:". $dtcrt);
    $bean = BeanFactory::newBean('Low01_Pagos_Facturas');
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, ['team_security' => false]);
    $sugarQuery->join("low01_pagos_facturas_lf_facturas", ['team_security' => false, 'alias' => 'facturas']);
    $sugarQuery->where()
      ->gt('facturas.dt_crt', $dtcrt)
      ->equals('low01_pagos_facturas_lowes_pagoslowes_pagos_ida', $pago->id)
      ;
    $sugarQuery->select(array('id','low01_pagos_facturas_lf_facturaslf_facturas_ida','facturas.dt_crt'));
    $facturas = $sugarQuery->execute();
    $compiled = $sugarQuery->compile();

    return $facturas;
  }

  public function getDocsRelacionados($facturas, $pagosFacturas, $factorajeIndex = "", $pago){
    global $db;

    $result = [];
    $index = 1;

    $importePagoTotal = 0.0;
    $iCountTotal = 2;

    $namePagoBase = "";
    $namePagoFactura = "";
    $namePagoBase = $pago->name;
    
    $sumaTotalSaldoAnterior = 0.0;
	
	 $GLOBALS["log"]->fatal(">>>>PAGO A procesar --> [". $pago->name ."]" );
    
    foreach ($facturas as $factura) {

      $GLOBALS["log"]->fatal(">>>>FACTURA a procesar --> [". $factura->name ."]" . " Monto: [" . $factura->importe . "]" );

      $par="1";
      $qry="select @row_number:=@row_number+1 AS row_number, 
      id, rel, serie, pago, num_pago_c, importe 
   from (SELECT @row_number:=0) AS t
     inner join (select lf_ori.id 'id', lf_ori.name 'rel', lf_ori.serie 'serie',  lp.name 'pago',  lfc.num_pago_c,  sum(lf.importe) 'importe'
             from low01_pagos_facturas_lowes_pagos_c lpf
             inner join low01_pagos_facturas lf on lf.id = lpf.low01_pagos_facturas_lowes_pagoslow01_pagos_facturas_idb and lf.deleted = 0
             left join low01_pagos_facturas_cstm lfc on lfc.id_c = lf.id   
             inner join low01_pagos_facturas_lf_facturas_c lpf_f on lpf_f.low01_pagos_facturas_lf_facturaslow01_pagos_facturas_idb = lf.id
             inner join lf_facturas lf_ori on lf_ori.id = lpf_f.low01_pagos_facturas_lf_facturaslf_facturas_ida
             inner join lowes_pagos lp on lp.id = lpf.low01_pagos_facturas_lowes_pagoslowes_pagos_ida and lp.deleted = 0
             left join lowes_pagos_cstm lpc on lpc.id_c = lp.id and lp.deleted = 0                       
             where lf_ori.id = '".$factura->id."' 
             group by  lf_ori.id, lf_ori.name, lf_ori.serie, lp.name, lfc.num_pago_c 
             order by lf.date_entered asc, lfc.num_pago_c asc ) as A;";

      $qs = $db->query($qry);


      $GLOBALS['log']->fatal(".......::Query PAGOS de FACTURAS:: (factura_id)...... [". $factura->id . "]"); 

      $importeAPagar = 0.0;

      $pagoFactura = $pagosFacturas[$factura->id];

      //$GLOBALS["log"]->fatal("--> Pago Factura Importe ".$pagoFactura->importe);

      $sumaTotalSaldoAnterior = 0.0;
      $idxContPag = 1;
      $IndicePar = 0;
      $iCountTotal = 2;

      $boolENTRA = FALSE;

	  		 // $GLOBALS["log"]->fatal("Pago Factura   [" . $namePagoFactura . "]");

      while($rest=$db->fetchByAssoc($qs)){

        $namePagoFactura = $rest['pago'];

          //$GLOBALS["log"]->fatal("Pago Factura   [" . $namePagoFactura . "]");

       // $GLOBALS["log"]->fatal("Saldo importe pago-factura : ".$rest['importe'] ); 

        // Obtenemos el valor del importe
        if ( strcmp( $namePagoBase, $namePagoFactura) == 0  ){

           //  $GLOBALS["log"]->fatal("Dentro del importe (condicion)");
			    //$GLOBALS["log"]->fatal("Indice: ".$par);
			
            $importeAPagar = $rest['importe'];

            $par = $rest['row_number'];
            

            //$IndicePar = intval($par);
            //$sumaTotalSaldoAnterior += $rest['importe'];

           // $iCountTotal = 3;
           $boolENTRA = TRUE;
        }

        // Se hace la suma para tratar de obtener el saldo anterior
        // sumando los pagos que se hacen despues

        if ( $boolENTRA == TRUE  ){
          $sumaTotalSaldoAnterior += $rest['importe'];
        }

        // $GLOBALS["log"]->fatal("Saldo Anterior acumulado: ".$sumaTotalSaldoAnterior); 

        $idxContPag++;

       // $GLOBALS["log"]->fatal("idxContPag   [" . $idxContPag . "]");
       // $GLOBALS["log"]->fatal("IndicePar   [" . $IndicePar . "]");

       // $GLOBALS["log"]->fatal("--> Row Number: ".  $rest['row_number'] );
       // $GLOBALS["log"]->fatal("--> Importa Saldo Anterior: ". $sumaTotalSaldoAnterior);
    
      }

      //$GLOBALS["log"]->fatal("--> Importa Pago Total: ". $importeAPagar);
    
      if ( $importeAPagar <= 0 ){
		  
		//$GLOBALS["log"]->fatal("Importa a pagar es 0 ");
        continue;
      }
    
     // $GLOBALS["log"]->fatal("Importe a pagar: ".$importeAPagar);
     // $importePagoTotal -= $importeAPagar;

      $importe = ($factorajeIndex == "2." && $pago->comision_factoraje_c === "0.000000") ? 0 : $factura->importe;
      // $saldoInsoluto = ($factorajeIndex == "2." && $pago->comision_factoraje_c === "0.000000") ? 0 : ($factura->importe - $pagoFactura->importe);
      
      //$saldoInsoluto = ($factorajeIndex == "2." && $pago->comision_factoraje_c === "0.000000") ? 0 : ($factura->importe -$importeAPagar);
      /*
        El saldo insoluto es lo que esta pendiente por liquidar de la factura,
        en este caso, es el importe de la factura, menos lo abonado hasta ese pago
      */ 
      $saldoInsoluto = ($factorajeIndex == "2." && $pago->comision_factoraje_c === "0.000000") ? 0 : (  $factura->importe - ( $factura->abono - $sumaTotalSaldoAnterior ) - $importeAPagar);

      $ImpSaldoAnte = 0.0;

      //$GLOBALS["log"]->fatal("Factura Abono: ".$factura->abono);   
      //$GLOBALS["log"]->fatal("Factura Importe: ".$factura->importe); 
    
      // $ImpSaldoAnte = ( ( $factura->abono == 0 ) ? $factura->importe : $factura->abono ) - $importeAPagar;

      // En caso de que el saldo anterior, se un valor minimo, entonces
      // lo hacemos 0
      if ( $sumaTotalSaldoAnterior < 0.001 ){
        $sumaTotalSaldoAnterior = 0;
      }

      if ( $saldoInsoluto < 0.001 ){
        $saldoInsoluto = 0;
      }



      // El saldo anterior es el importe de la factura
      // menos lo abonado
      // menos lo pagado despues del pago que estamos enviando
      $ImpSaldoAnte =  $factura->importe - ($factura->abono - $sumaTotalSaldoAnterior);

	  
      //$GLOBALS["log"]->fatal("Saldo Anterior acumulado Final: ".$sumaTotalSaldoAnterior); 


      //_ppl( "Importe importe: {" . $importe . "}");
      //_ppl("Importe saldoInsoluto: {" . $saldoInsoluto . "}");

      //$GLOBALS["log"]->fatal("Factura Abono: ".$factura->abono);
      //$GLOBALS["log"]->fatal("Saldo Insoluto: ".$saldoInsoluto);   
       //$GLOBALS["log"]->fatal("Saldo Anterior: ".$ImpSaldoAnte);   
      //$GLOBALS["log"]->fatal("Importe a pagar: ".$importeAPagar);

      // $GLOBALS["log"]->fatal(">>>>Monto Factura: [" . $factura->importe . "]" );
	  
 

      $result["IdDocumento".$factorajeIndex.$index] = $factura->uuid;
      $result["Serie".$factorajeIndex.$index] = $factura->serie;
      $result["Folio".$factorajeIndex.$index] = $factura->name;
      $result["MonedaDR".$factorajeIndex.$index] = "MXN";
      $result["TipoCambioDR".$factorajeIndex.$index] = "";
      $result["MetodoDePagoDR".$factorajeIndex.$index] = "PPD";
      $result["NumParcialidad".$factorajeIndex.$index] = $par;
     // $result["ImpSaldoAnt".$factorajeIndex.$index] = number_format($importe, 2, '.', '');
      $result["ImpSaldoAnt".$factorajeIndex.$index] = number_format($ImpSaldoAnte, 2, '.', '');
      $result["ImpPagado".$factorajeIndex.$index] = number_format( $importeAPagar, 2, '.', ''); //importe a $pagoFactura->importe
      $result["ImpSaldoInsoluto".$factorajeIndex.$index] = number_format($saldoInsoluto, 2, '.', '');
    
      $index++;
    }
   
    return $result;
  }

  public function getPagoKonesData($pago, $facturas, $pagosFacturas, $account, $contact)
  {
    global $timedate, $current_user;
    $pago->fetch($pago->id);

    $admin = new CustomMRX1Configuraciones();
    $admin->retrieveSettings();
    $folioKonesh = (empty($admin->settings['folio_konesh']) ? 0 : $admin->settings['folio_konesh']) + 1;

    $correoElectronico = "";
    if($contact && $contact->id){
      $sea = new SugarEmailAddress;
      $emailAddresses = $sea->getAddressesByGUID($contact->id, "Contacts");
      $correos = [];
      foreach ($emailAddresses as $emailAddress) {
        $correos[] = $emailAddress['email_address'];
      }
      $correoElectronico = implode(";", $correos);
    }

    $fecha = new SugarDateTime('now');
    $fecha = $timedate->tzUser($fecha, $current_user);
    //$GLOBALS["log"]->fatal("pagos:". print_r($facturas, true));
    $complementoPago = $this->getComplementoPago($pago, $facturas, $pagosFacturas, $account);
    $receptorNombre = $this->isFactoraje($pago->id) ? $account->banco_factoraje_c : $facturas[0]->name_receptor;
    $receptorRfc = $this->isFactoraje($pago->id) ? $account->rfc_factoraje_c : $facturas[0]->rfc_receptor;

    $data = [
      "file_name" => "LCM061222S74_$folioKonesh.txt",
      "COMPROBANTE" => [
        "Version" => "3.3",
        "Folio" => $folioKonesh,
        "Fecha" => $fecha->format("Y-m-d\TH:i:s"),
        "SubTotal" => "0",
        "Total" => "0",
        "Moneda" => "XXX",
        "TipoDeComprobante" => "P",
        "LugarExpedicion" => "66279",
      ],
      "CFDIRELACIONADO" => [
        "TipoRelacion" => ($pago->folio_konesh_c ? "04" : ""),
        "UUID" => ($pago->folio_konesh_c ? $pago->folio_konesh_c : ""),
      ],
      "EMISOR" => [
        "Nombre" => "Lowe's Companies Mexico S. de R.L. de C.V.",
        "Rfc" => "LCM061222S74",
        "RegimenFiscal" => "601",
      ],
      "RECEPTOR" => [
        "NOMBRERE" => $receptorNombre,
        "RFCRE" => $receptorRfc,
        "UsoCFDI" => "P01",
      ],
      "CONCEPTOS" => [
         "Cantidad" =>  "1",
         "CveProdServ" =>  "84111506",
         "UnidadClave" =>  "ACT",
         "Descripcion" =>  "Pago",
         "ValorUnitario" =>  "0.00",
         "Importe" =>  "0.00",
       ],
      "IMPUESTOS" => [],
      "EXTRAS" => [
         "correoElectronico" => $correoElectronico,
      ],
      "COMPLEMENTO PAGO" => $complementoPago,
    ];

    /*
    $json=json_encode($data);
    
    $jsonDecode = json_decode($json, true);
    foreach ($jsonDecode as $ren1 => $val1) {
      if(is_array($val1)){
        $cad.="#$ren1 \n";
        foreach ($val1 as $ren2 => $val2) {
          if(is_array($val2)){
            $cad.="#$ren2 \n";
            foreach ($val2 as $ren3 => $val3) {
              $cad.="$ren3 : $val3 \n";
            }
            $cad.="#FIN DE $ren2 \n";
          }else{
            $cad.="$ren2 : $val2 \n";
          }
        }
        $cad.="#FIN DE $ren1 \n";
      }else{
        $cad.="$ren1 : $val1 \n";
      }
    }
    */
    $json=json_encode($data);
    //_ppl("Pago Konesh Data");
    //$GLOBALS["log"]->fatal("Complemento: ". print_r($data, true));
    $noti = BeanFactory::newBean('Notes');
    $noti->name = "Datos Complemento-LCM061222S74_" . $folioKonesh . ".txt";
    $jsonDecode = json_decode($json, true);
    foreach ($jsonDecode as $ren1 => $val1) {
      if(is_array($val1)){
        $cad.="#$ren1 \n";
        foreach ($val1 as $ren2 => $val2) {
          if(is_array($val2)){
            $cad.="#$ren2 \n";
            foreach ($val2 as $ren3 => $val3) {
              $cad.="$ren3 : $val3 \n";
            }
            $cad.="#FIN DE $ren2 \n";
          }else{
            $cad.="$ren2 : $val2 \n";
          }
        }
        $cad.="#FIN DE $ren1 \n";
      }else{
        $cad.="$ren1 : $val1 \n";
      }
    }
    $noti->description = $cad;
    $noti->assigned_user_id = '1';
    $noti->save();
    
    // $GLOBALS["log"]->fatal("Complemento: ". print_r($cad, true));
    return $data;
  }

  public function getCheques(){
    global $current_user,$db;
    $bean = BeanFactory::newBean('pchck_Cheques_posfechados');
    $sugarQuery = new SugarQuery();
    $sugarQuery->select(array('id', 'name', 'banco'));
    $sugarQuery->from($bean, array('team_security' => false));
    $sugarQuery->where()
               ->equals("cheque_cobrado", "1")
               ->equals("rebotado", "0")
               ->equals("estatus_envio", "Activo");
    $compiled = $sugarQuery->compile();
    /*$query=$compiled->getSQL();
    $parameters = $compiled->getParameters();
    $GLOBALS['log']->fatal(".......::Query Cheques::...... ".$query); */
    
    $cheques = $sugarQuery->execute(); 
    
    return $cheques;
  }

  public function getComplementoCheques($cheque){
    $tienda=$cheque->tienda;
    $puesto=$cheque->puesto;
    $transaccion=$cheque->transaccion;
    $fecha=$cheque->fecha_recibido;
    $fecha=str_replace("-", "", $fecha);

    $ticket=$tienda."-".$puesto."-".$transaccion."-".$fecha;
    $GLOBALS["log"]->fatal("Ticket: ".$ticket);

    global $timedate, $current_user;
    $admin = new CustomMRX1Configuraciones();
    $admin->retrieveSettings();
    $folioKonesh = (empty($admin->settings['folio_konesh']) ? 0 : $admin->settings['folio_konesh']) + 1;
    $fecha = new SugarDateTime('now');
    $fecha = $timedate->tzUser($fecha, $current_user);
    $data = [
      "file_name" => "CHEQUEPOSFECHADO_$cheque->rfc"."_$folioKonesh"."_$cheque->transaccion.txt",
      "COMPROBANTE" => [
        "Version" => "3.3",
        "Folio" => $folioKonesh,
        "Fecha" => $fecha->format("Y-m-d\TH:i:s"),
        "SubTotal" => "0",
        "Total" => "0",
        "Moneda" => "XXX",
        "TipoDeComprobante" => "P",
        "LugarExpedicion" => "66279",
      ],
      "CFDIRELACIONADO" => [
        "TipoRelacion" => "",
        "UUID" => "",
      ],
      "EMISOR" => [
        "Nombre" => "Lowe's Companies Mexico S. de R.L. de C.V.",
        "Rfc" => "LCM061222S74",
        "RegimenFiscal" => "601",
      ],
      "RECEPTOR" => [
        "NOMBRERE" => $cheque->banco,
        "RFCRE" => $chque->rfc,
        "UsoCFDI" => "P01",
      ],
      "CONCEPTOS" => [
         "Cantidad" =>  "1",
         "CveProdServ" =>  "84111506",
         "UnidadClave" =>  "ACT",
         "Descripcion" =>  "Pago",
         "ValorUnitario" =>  "0.00",
         "Importe" =>  "0.00",
       ],
      "IMPUESTOS" => [],
      "EXTRAS" => [
         "correoElectronico" => "",
      ],
      "COMPLEMENTO PAGO" => [
        "PAGO" => [
          "FechaPago1" => $cheque->fecha_recibido,
          "FormaDePagoP1" => 03,
          "MonedaP1" => "MXN",
          "TipoCambioP1" => "",
          "Monto1" => number_format($cheque->monto, 2, '.', ''),
          "NumOperacion1" => "",
          "RfcEmisorCtaOrd1" => "",
          "NomBancoOrdExt1" => "",
          "CtaOrdenante1" => "",
          "RfcEmisorCtaBen1" => "",
          "CtaBeneficiario1" => "",
          "TipoCadPago1" => "",
          "CertPago1" => "",
          "CadPago1" => "",
          "SelloPago1" => "",
        ],
        "DOCTORELACIONADO" => [
           "IdDocumento1.1" => "",
           "Ticket1.1" => "$ticket",
           "Serie1.1" => "",
           "Folio1.1" => "",
           "MonedaDR1.1" => "",
           "TipoCambioDR1.1" => "",
           "MetodoDePagoDR1.1" => "",
           "NumParcialidad1.1" => "",
           "ImpSaldoAnt1.1" => "",
           "ImpPagado1.1" => "",
           "ImpSaldoInsoluto1.1" => "",
        ],
        "IMPUESTOS" => [
          "TotalImpuestosRetenidos1" => "",
          "TotalImpuestosTrasladados1" => "",
        ],
        "RETENCIONES" => [
          "Impuesto1.1" => "",
          "Importe1.1" => "",
        ],
        "TRASLADOS" => [
          "Impuesto1.1" => "",
          "TipoFactor1.1" => "",
          "TasaOCuota1.1" => "",
          "Importe1.1" => "",
        ]
      ],
    ];
    return $data;
  }

  public function getComplementoPago($pago, $facturas, $pagosFacturas, $account)
  {
    global $timedate, $current_user;

    $horas=0;
    $fechaC = new SugarDateTime('now');
    $horas = $fechaC->getHour();

    $fechaPago = new SugarDateTime($pago->fecha_transaccion);

    $fechaPago = $timedate->tzUser($fechaPago, $current_user);
    $horas = $fechaPago->getHour();
   //_ppl("tzUser ". $timedate->tzUser($fechaPago, $current_user));
    $fechaPago = $fechaPago->setTime(12,0);
    $fechaPago->modify('+'.$horas.' hours');
    $horas = $fechaPago->getHour();

    if($horas<12){
      $fechaPago->modify('+'.(12-$horas).' hours');
    }
    if($horas>12){
      $fechaPago->modify('-'.(12-$horas).' hours');
    }

    $forma="";
    $pagoFacturasAux = [];
    foreach ($pagosFacturas as $pagoFactura) {
      $pagoFacturasAux[$pagoFactura->low01_pagos_facturas_lf_facturaslf_facturas_ida] = $pagoFactura;
    }
    $complementoPago = [];
    //_ppl('P a g o  c o m p l e m e n t o : ');
    if($this->isFactoraje($pago->id)){
      $pago->comision_factoraje_c;
      for ($item=1; $item <= 2; $item++) {
        //$monto =  number_format(($item == 1 ? $pago->monto : $pago->comision_factoraje_c), 2, '.', '');
        $monto = number_format(($item == 1 ? $pago->monto : $this->getMontoFactoraje($pago->id)), 2, '.', '');
        $tipoPago =  $item == 1 ? "02" : "17";
        //_ppl('F a c t o r a j e');
        $docsRelacionados = $this->getDocsRelacionados($facturas, $pagoFacturasAux, "$item.", $pago);
        $complementoPago = array_merge($complementoPago, [
          "PAGO$item" => [
            "FechaPago$item" => $fechaPago->format("Y-m-d\TH:i:s"),
            "FormaDePagoP$item" => $tipoPago,
            "MonedaP$item" => "MXN",
            "TipoCambioP$item" => "",
            "Monto$item" => $monto,
            "NumOperacion$item" => "",
            "RfcEmisorCtaOrd$item" => "",
            "NomBancoOrdExt$item" => "",
            "CtaOrdenante$item" => "",
            "RfcEmisorCtaBen$item" => "",
            "CtaBeneficiario$item" => "",
            "TipoCadPago$item" => "",
            "CertPago$item" => "",
            "CadPago$item" => "",
            "SelloPago$item" => "",
          ],
          "DOCTORELACIONADO$item" => $docsRelacionados,
          "IMPUESTOS$item" => [
            "TotalImpuestosRetenidos$item" => "",
            "TotalImpuestosTrasladados$item" => "",
          ],
          "RETENCIONES$item" => [
            "Impuesto$item.1" => "",
            "Importe$item.1" => "",
          ],
          "TRASLADOS$item" => [
            "Impuesto$item.1" => "",
            "TipoFactor$item.1" => "",
            "TasaOCuota$item.1" => "",
            "Importe$item.1" => "",
          ]
        ]);
      }
    }
    else{
      $GLOBALS["log"]->fatal("No Factoraje");
      $docsRelacionados = $this->getDocsRelacionados($facturas, $pagoFacturasAux, "1.", $pago);
      //_ppl('F o r m a   d e   P a g o - - - > '.$pago->forma_pago);
      $forma=$this->traductor($pago->forma_pago);
      //_ppl("Forma Pago \(n.n)/ ".$forma);
      $complementoPago = [
        "PAGO" => [
          "FechaPago1" => $fechaPago->format("Y-m-d\TH:i:s"),
          "FormaDePagoP1" => $forma,
          "MonedaP1" => "MXN",
          "TipoCambioP1" => "",
          "Monto1" => number_formatgetDocsRelacionados($pago->monto, 2, '.', ''),
          "NumOperacion1" => "",
          "RfcEmisorCtaOrd1" => "",
          "NomBancoOrdExt1" => "",
          "CtaOrdenante1" => "",
          "RfcEmisorCtaBen1" => "",
          "CtaBeneficiario1" => "",
          "TipoCadPago1" => "",
          "CertPago1" => "",
          "CadPago1" => "",
          "SelloPago1" => "",
        ],
        "DOCTORELACIONADO" => $docsRelacionados,
        "IMPUESTOS" => [
          "TotalImpuestosRetenidos1" => "",
          "TotalImpuestosTrasladados1" => "",
        ],
        "RETENCIONES" => [
          "Impuesto1.1" => "",
          "Importe1.1" => "",
        ],
        "TRASLADOS" => [
          "Impuesto1.1" => "",
          "TipoFactor1.1" => "",
          "TasaOCuota1.1" => "",
          "Importe1.1" => "",
        ]
      ];
    }
    return $complementoPago;
  }

  public function process()
  {
    $GLOBALS["log"]->fatal("< - - - - - Inicio envio pagos KONESH - - - - > ".$url);
     
    $chequeData = $this->getCheques();
    foreach ($chequeData as $cheque) {
      $cheque_bn =  BeanFactory::getBean('pchck_Cheques_posfechados', $cheque['id']);
      $data_cheque = $this->getComplementoCheques($cheque_bn);
      //$GLOBALS["log"]->fatal("info de getComplementoCheques |m| ".json_encode($data_cheque));
      //$url =  $this->hostname === 'lowes.sugarondemand.com' ? 'http://sugarconecta.lowes.com.mx/' : 'http://json.lowes.com.mx/';
      $url = "http://100.24.211.220/Middleware/json2txt.php";
        
      // $GLOBALS["log"]->fatal("< - - - - - U R L - - - - > ".$url);

      //$req = new CurlRequest($url);
      $this->sendCheques($url, $cheque_bn ,$data_cheque);     
    }
    

    $pagosData = $this->getPagos();
    foreach ($pagosData as $pagoData) 
	  {
		$GLOBALS["log"]->fatal("#$()/-> Procesando pago [". $pagoData['id'] ."]"); 
				
        $pago = BeanFactory::getBean('lowes_Pagos', $pagoData['id']);
        $account = BeanFactory::getBean('Accounts', $pago->lowes_pagos_accountsaccounts_ida);
        $contact = null;
        if($account->contact_id1_c){
          $contact = BeanFactory::getBean('Contacts', $account->contact_id1_c);
        }
        $pagosFacturasData = $this->getFacturas($pago);
        if(count($pagosFacturasData))
		{
          $facturas = [];
          $pagosFacturas = [];
          foreach ($pagosFacturasData as $pagoFacturaData) 
		  {
            $pagoFactura = BeanFactory::getBean('Low01_Pagos_Facturas', $pagoFacturaData['id']);
            $factura = BeanFactory::getBean('LF_Facturas', $pagoFacturaData['low01_pagos_facturas_lf_facturaslf_facturas_ida']);
            $pagosFacturas[$factura->id] = $pagoFactura;
            $facturas[] = $factura;
          }
          $data = $this->getPagoKonesData($pago, $facturas, $pagosFacturas, $account, $contact);
         // $url =  $this->hostname === 'lowes.sugarondemand.com' ? 'http://json.lowes.com.mx/' : 'http://json.lowes.com.mx/';
		  $url =  $this->hostname === 'lowes.sugarondemand.com' ? 'http://sugarconecta.lowes.com.mx/' : 'http://json.lowes.com.mx/';
          $url .= $this->urlApi;
          // $GLOBALS["log"]->fatal("< - - - - - U R L - - - - > ".$url);
		  
          //$req = new CurlRequest($url);
          //$result = $this->send($req, $pago, $data);
          $url = "http://100.24.211.220/Middleware/json2txt.php";
          $result = $this->sendComplemento($url, $pago, $data);
        }
      }
	  
	  $GLOBALS["log"]->fatal("< - - - - - FIN DE PAGOS KONESH - - - - > ");
	  
    //$GLOBALS["log"]->setLevel('debug');  
  }

   public function traductor($tipo)
   {
    global $db;
    
    $sql = "SELECT id, name, description, valu, code from fdp_formaspago"; 
    $result = $db->query($sql);
    
    while ($formas = $db->fetchByAssoc($result) ) {
      if($formas['name'] == $tipo)
      {
        //$GLOBALS['log']->fatal("tipo ".$formas['valu']);
        return $formas['valu'];
      }else{
        if($formas['valu'] == $tipo){
          return $formas['valu'];
        }
        continue;
      }
    }
  }

}

?>
