<?php
require_once 'custom/include/HealthCheckImport.php';
class ImportadorOrdenes
{
  public function process()
  {
    $saludDeImportaciones = new HealthCheckImport();
  	$result = $saludDeImportaciones->obtenerRegistrosJSON("ordenes");

  	if (!empty($result))
  	{
  		if ($result['code'] == "200")
  		{
  			$registrados = 0;
  			$filename = explode('.', $result['filename']);
  			$saludDeOrdenes = $saludDeImportaciones->ordenes($result);

  			if (empty($saludDeOrdenes['ordenesConError']))
  			{
  				$registrados = $saludDeImportaciones->guardarBeans($saludDeOrdenes['ordenesAGuardar'],$result['filename']);
  				if ($registrados == count($saludDeOrdenes['ordenesAGuardar']))
  				{
  					$esProcesado = $saludDeImportaciones->esProcesadoConExito($filename[0]);
  				}
  			} else {
  				if (in_array(5,$saludDeOrdenes['typeErrors']) || in_array(6,$saludDeOrdenes['typeErrors'])){
  					$saludDeImportaciones->crearNotaDeError("ordenes",$saludDeOrdenes['typeErrors']);
  				} else {
  					if(!empty($saludDeOrdenes['ordenesAGuardar'])){
  						$registrados = $saludDeImportaciones->guardarBeans($saludDeOrdenes['ordenesAGuardar'],$result['filename']);
  					}
  					$saludDeImportaciones->crearNotaDeError("ordenes",$saludDeOrdenes['typeErrors'],$saludDeOrdenes['ordenesConError'],$filename[0],$registrados);
  				}
  				$esProcesado = $saludDeImportaciones->esProcesadoConErrores($filename[0]);
  			}
  			return $saludDeImportaciones->logProcesados("Ordenes",$esProcesado);
  		} else {
  			$GLOBALS['log']->fatal($result['message']);
  			return false;
  		}
  	} else {
  		$GLOBALS['log']->fatal('Error con el middleware');
  		return false;
  	}
  }
}
