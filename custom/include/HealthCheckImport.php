<?php

require_once('include/SugarQuery/SugarQuery.php');
require_once('include/SugarDateTime.php');
require_once('include/TimeDate.php');
require_once('custom/include/CustomHttpRequest.php');

class HealthCheckImport
{
  public function __construct() {
    $configurator = new Configurator();
    $configurator->loadConfig();
    $this->hotsName = $configurator->config['host_name'];
    $this->baseUrl = "http://100.24.211.220/Middleware/";
  }

  public function obtenerRegistrosJSON($modulo)
  {
    $url = $this->baseUrl."csv2json.php";
    $GLOBALS['log']->fatal('----- URL: '.$url.'----- modulo:'.$modulo);

    $req = curl_init($url);
    curl_setopt($req, CURLOPT_POST, true); 
    curl_setopt($req, CURLOPT_POSTFIELDS, "modulo=".$modulo);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($req);
    $GLOBALS["log"]->fatal("Regreso con: ".$result);
    return json_decode($result, true);
  }

  public function esProcesadoConExito($name){
    $url = $this->baseUrl."funcionessh.php";
    $GLOBALS['log']->fatal('----- URL: '.$url.'----- name: '.$name);
    
    $req = curl_init($url);
    curl_setopt($req, CURLOPT_POST, true); 
    curl_setopt($req, CURLOPT_POSTFIELDS, "procesado=".$name.".csv");
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($req);
    $GLOBALS["log"]->fatal("Regreso con Exito: ".$result);
    return json_decode($result, true);
  }

  public function esProcesadoConErrores($name)
  {
    $url = $this->baseUrl."funcionessh.php";
    $GLOBALS['log']->fatal('----- URL: '.$url.'----- name: '.$name);
   
    $req = curl_init($url);
    curl_setopt($req, CURLOPT_POST, true); 
    curl_setopt($req, CURLOPT_POSTFIELDS, "error=".$name.".csv");
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($req);
    $GLOBALS["log"]->fatal("Regreso con Errores: ".$result);
    return json_decode($result, true);
  }

  public function crearNotaDeError($module,$typeErrors,$beanErrors = null,$filename = null,$registrados = 0)
  {
    $nota = BeanFactory::newBean("Notes");
    // $nota->name = "Error - Importación en el módulo de $module";
    $nota->name = "Error - Importación de registros por JOB";
    if(is_null($beanErrors)){
      switch ($typeErrors[0]) {
        case 5: $msg = "El archivo no cuenta con todas las columnas necesarias para que sus registros sean procesados.\n"; break;
        case 6: $msg = "El archivo no tiene registros.\n"; break;
        default: break;
      }
    } else {
      if ($registrados > 0){
        $msg = "Del archivo $filename.csv, se dieron de alta: $registrados registros, pero los siguientes presentan errores:\n";
      } else {
        $msg = "Del archivo $filename.csv, los siguientes registros presentan errores:\n";
      }
      foreach ($typeErrors as $key => $value) {
        switch ($value) {
          case 1:
            if ($module === "facturas")
            {
              $msg .= "La factura " . $beanErrors[$key]->name . " tiene una orden pero, no cuenta con un cliente relacionado.\n";
            }
            if ($module === "n_credito")
            {
              $msg .= "La nota de crédito " . $beanErrors[$key]->name . " no tiene número de ticket.\n";
            }
            if ($module === "pagos")
            {
              $msg .= "El pago con folio_ebs " . $beanErrors[$key]->folio_ebs_c . " no tiene número de cuenta clabe.\n";
            }
            if ($module === "ordenes")
            {
              $msg .= "La orden con ticket " . $beanErrors[$key]->no_ticket_c . " no tiene una cuenta válida.\n";
            }
          break;
          case 2: $msg .= "La factura " . $beanErrors[$key]->name . " no tiene una orden.\n"; break;
          case 3: $msg .= "La factura " . $beanErrors[$key]->name . " no tiene número de ticket.\n"; break;
          case 4:
            if ($module === "facturas" || $module === "n_credito")
            {
              $msg .= "El registro " . $beanErrors[$key]->name . " le faltan una o más columnas para ser procesado.\n";
            }
            if ($module === "pagos")
            {
              $msg .= "El registro " . $beanErrors[$key]->folio_ebs_c . " le faltan una o más columnas para ser procesado.\n";
            }
            if ($module === "ordenes")
            {
              $msg .= "El registro " . $beanErrors[$key]->folio_c . " le faltan una o más columnas para ser procesado.\n";
            }
            break;
          default: break;
        }
      }
    }
    $msg .= "Se moverá al directorio de errores. Revisar, corregir y volver a procesar los registros con errores.";
    $nota->description = $msg;
    $nota->assigned_user_id = 1;
    $nota->team_id = 1;
    $nota->save(false);
    return $nota;
  }

  private function getFactura($factura,$result,$i){
    global $current_user, $timedate;
    foreach ($result['records'][$i] as $key => $value) {
      switch ($key) {
        case 'FOLIO_ID': $factura->name = $value; break;
        case 'TICKET_NUM':$factura->no_ticket = $value;break;
        case 'SERIES_ID': $factura->serie = $value; break;
        case 'UUID': $factura->uuid = $value; break;
        case 'RFC_E': $factura->rfc_emisor = $value; break;
        case 'NAME_E': $factura->nombre_emisor = $value; break;
        case 'RFC_R': $factura->rfc_receptor = $value; break;
        case 'NAME_R': $factura->name_receptor = $value; break;
        case 'DT_CRT':
        $usertimezone = $current_user->getPreference('timezone');
        $date_bean = new SugarDateTime($value, new DateTimeZone($usertimezone));
        $factura->dt_crt = $date_bean->asDb(true);
        break; //dt_crt
        case 'SUBTOTAL': $factura->sub_total = $value; break;
        case 'IVA': $factura->iva = $value; break;
        case 'IMP_TOT': $factura->importe = $value; break;
        default: break;
      }
    }
    return $factura;
  }

  // return array($facturasAGuardar,$facturasConError,$typeErrors)
  public function facturas($result)
  {
    $facturasAGuardar = array();
    $facturasConError = array();
    $typeErrors = array();

    if (isset($result['records'][0]))
    {
      if(count($result['records'][0]) == 12)// es el número correcto de campos esperados
      {
        $totaldeRegistros = count($result['records']);
        $i = 0;
        while ($i < $totaldeRegistros) {
          $factura = BeanFactory::newBean('LF_Facturas');
          $factura->assigned_user_id = "55335026-99fc-11e6-850c-060b37ffb723";
          if (count($result['records'][$i]) == 12) {
            $factura = $this->getFactura($factura,$result,$i);
            if(!$this->getFacturaExist($factura->name, $factura->serie, $factura->no_ticket)){//nuevo parametro No_tick HSLR
              if(!empty($factura->no_ticket)){
                $orden = $this->getOrdenExist($factura->no_ticket,true);
                if(isset($orden[0]['id'])){
                  if($this->getAccountExist($orden[0]['id'])){
                    array_push($facturasAGuardar, $factura); // Tiene una orden y esa orden tiene cliente relacionado
                  }
                  else {
                    array_push($facturasConError, $factura); // Tiene una orden pero No tiene un cliente relacionado
                    array_push($typeErrors, 1);
                  }
                }else {
                  array_push($facturasConError, $factura); // No tiene una orden
                  array_push($typeErrors, 2);
                }
              } else {
                array_push($facturasConError, $factura); // No tiene número de ticket
                array_push($typeErrors, 3);
              }
            } else {
              $GLOBALS['log']->fatal('importaFacturasJSON Factura Existente: ' . $factura->name . ", fecha de emisión: " . $factura->dt_crt . " no_ticket:" . $factura->no_ticket);
            }
          } else {
            $factura = $this->getFactura($factura,$result,$i);
            array_push($facturasConError, $factura); // A un registro le falta una columna
            array_push($typeErrors, 4);
          }
          $i++;
        }
      }else{
        array_push($typeErrors, 5); // El archivo no tiene todas las columnas esperadas o viene vacio
      }
    }else {
      array_push($typeErrors, 6); // El archivo viene vacio
    }
    return array('facturasAGuardar'=>$facturasAGuardar,'facturasConError'=>$facturasConError,'typeErrors'=>$typeErrors);
  }
  //modificacion en la funcion getFacturaExist se incrementa otro 
  private function getFacturaExist($name, $serie, $no_ticket)
  {
    $bean = BeanFactory::newBean("LF_Facturas");
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $sugarQuery->where()
    ->equals('name', $name)
    ->equals('serie', $serie)
    ->equals('no_ticket', $no_ticket);
    $sugarQuery->limit(1);
    $result = $sugarQuery->execute();
    return count($result) > 0;
  }

  private function getAccountExist($idOrden)
  {
    $bean = BeanFactory::getBean('orden_Ordenes',$idOrden);
    $bean->load_relationship('accounts_orden_ordenes_1');
    $cuenta = $bean->accounts_orden_ordenes_1->getBeans();
    return count($cuenta) > 0;
  }

  private function getAccountOrdenesExist($id_pos_c){
    $bean = BeanFactory::newBean('Accounts');
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $sugarQuery->where()
    ->equals('id_pos_c', $id_pos_c);
    $sugarQuery->limit(1);
    $result = $sugarQuery->execute();
    return count($result) > 0;
  }

  private function getNotasDeCredito($nota,$result,$i)
  {
    global $current_user, $timedate;
    foreach ($result['records'][$i] as $key => $value) {
      switch ($key) {
        case 'FOLIO_ID': $nota->name = $value; break;
        case 'TICKET_NUM':
        if(!empty($value)){$nota->no_ticket_c = $value;}
        break;
        case 'DT_CRT':
        $usertimezone = $current_user->getPreference('timezone');
        $date_bean = new SugarDateTime($value, new DateTimeZone($usertimezone));
        $nota->fecha_emision_c = $date_bean->asDb(true);
        break;
        case 'SUBTOTAL' : $nota->sub_total_c = $value; break;
        case 'IVA' : $nota->iva_c = $value; break;
        case 'IMP_TOT' : $nota->importe_total = $value; break;
        case 'SERIES_ID': $nota->series_id_c = $value; break;
        case 'UUID': $nota->uuid_c = $value; break;
        case 'RFC_E': $nota->rfc_e_c = $value; break;
        case 'NAME_E': $nota->nombre_e_c = $value; break;
        case 'RFC_R': $nota->rfc_r_c = $value; break;
        case 'NAME_R': $nota->name_r_c = $value; break;
        default: break;
      }
    }
    return $nota;
  }

  // return array($n_CreditoAGuardar,$n_CreditoConError,$typeErrors)
  public function notasDeCredito($result)
  {
    $n_CreditoAGuardar = array();
    $n_CreditoConError = array();
    $typeErrors = array();

    if (isset($result['records'][0]))
    {
      if(count($result['records'][0]) == 12)// es el número correcto de campos esperados
      {
        $totaldeRegistros = count($result['records']);
        $i = 0;
        while ($i < $totaldeRegistros) {
          $nota = BeanFactory::newBean('NC_Notas_credito');
          if (count($result['records'][$i]) == 12) {
            $nota = $this->getNotasDeCredito($nota,$result,$i);
            if(!$this->getNotaCreditoExist($nota->name)){
              if(!empty($nota->no_ticket_c)){
                array_push($n_CreditoAGuardar, $nota);
              } else {
                array_push($n_CreditoConError, $nota); // La nota no tiene número de ticket
                array_push($typeErrors, 1);
              }
            } else {
              $GLOBALS['log']->fatal('importaNotasDeCreditoJSON Nota de Crédito Existente: ' . $nota->name . ", fecha de emisión: " . $nota->fecha_emision_c . " no_ticket:" .  $nota->no_ticket_c);
            }
          } else {
            $nota = $this->getNotasDeCredito($nota,$result,$i);
            array_push($n_CreditoConError, $nota); // La nota no tiene todas las columnas
            array_push($typeErrors, 4);
          }
          $i++;
        }
      } else {
        array_push($typeErrors, 5);
      }
    } else {
      array_push($typeErrors, 6);
    }
    return array('n_CreditoAGuardar'=>$n_CreditoAGuardar,'n_CreditoConError'=>$n_CreditoConError,'typeErrors'=>$typeErrors);
  }

  private function getNotaCreditoExist($name)
  {
    $bean = BeanFactory::newBean("NC_Notas_credito");
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $sugarQuery->where()
    ->equals('name', $name);
    $sugarQuery->limit(1);
    $result = $sugarQuery->execute();
    return count($result) > 0;
  }

  private function getPago($pago,$result,$i)
  {
    global $timedate, $current_user;
    foreach ($result['records'][$i] as $key => $value) {
      switch ($key) {
        case 'folio': $pago->folio_ebs_c = $value; break;
        case 'monto': $pago->monto = str_replace(",", "", $value) ; break;
        case 'forma_de_pago': $pago->forma_pago = $value; break;
        case 'cuenta_virtual' :
        if(!empty($value)) { $pago->numero_cuenta_clabe_c = $value;}
        break;
        case 'fecha':
        $fechaTransaccion = $timedate->fromDbFormat($value, "d-m-Y");
        $timedate->asUser($fechaTransaccion, $current_user);
        $pago->fecha_transaccion = $fechaTransaccion->asDbDate();
        break;
        default: break;
      }
    }
    return $pago;
  }
  // return array($pagosAGuardar,$pagosConError,$typeErrors)
  public function pagos($result)
  {
    $pagosAGuardar = array();
    $pagosConError = array();
    $typeErrors = array();

    if (isset($result['records'][0]))
    {
      if(count($result['records'][0]) == 5)// es el número correcto de campos esperados
      {
        $totaldeRegistros = count($result['records']);
        $i = 0;
        while ($i < $totaldeRegistros) {
          $pago = BeanFactory::newBean('lowes_Pagos');
          if (count($result['records'][$i]) == 5) {
            $pago = $this->getPago($pago,$result,$i);
            if(!$this->getPagoExist($pago->folio_ebs_c)){
              if(!empty($pago->numero_cuenta_clabe_c)) //Existe la Cuenta con el número de cuenta clabe
              {
                array_push($pagosAGuardar, $pago);
              } else {
                array_push($pagosConError, $pago); // El pago no tiene número de cuenta clabe
                array_push($typeErrors, 1);
              }
            } else {
              $GLOBALS['log']->fatal('importaPagosJSON Pago Existente: ' . $pago->folio_ebs_c . ", fecha de transacción: " . $pago->fecha_transaccion . " numero_cuenta_clabe_c:" . $pago->numero_cuenta_clabe_c);
            }
          } else {
            $pago = $this->getPago($pago,$result,$i);
            array_push($pagosConError, $pago); // A un registro le hace falta alguna columna
            array_push($typeErrors, 4);
          }
          $i++;
        }
      } else {
        array_push($typeErrors, 5);
      }
    } else {
      array_push($typeErrors, 6);
    }
    return array('pagosAGuardar'=>$pagosAGuardar,'pagosConError'=>$pagosConError,'typeErrors'=>$typeErrors);
  }

  private function getPagoExist($folioEbs)
  {
    $bean = BeanFactory::newBean("lowes_Pagos");
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $sugarQuery->where()
    ->equals('folio_ebs_c', $folioEbs);
    $sugarQuery->limit(1);
    $result = $sugarQuery->execute();
    return count($result) > 0;
  }

  private function getOrden($orden,$result,$i)
  {
    global $current_user, $timedate;
    foreach ($result['records'][$i] as $key => $value) {
      switch ($key) {
        case 'name': $orden->name = $value; break;
        case 'folio_c': $orden->folio_c = $value; break;
        case 'estado_c': $orden->estado_c = $value; break;
        case 'no_ticket_c': $orden->no_ticket_c = $value; break;
        case 'amount_c': $orden->amount_c = $value; break;
        case 'sub_total_c': $orden->sub_total_c = $value; break;
        case 'iva_c': $orden->iva_c = $value; break;
        case 'id_pos_c': $orden->id_pos_c = $value; break;
        case 'folio_orden_inicial_c': $orden->folio_orden_inicial_c = $value; break;
        case 'id_caja_c': $orden->id_caja_c = $value; break;
        case 'id_cajero_c': $orden->id_cajero_c = $value; break;
        case 'id_sucursal_c': $orden->id_sucursal_c = $value; break;
        case 'transactiontypecode_c':$orden->transactiontypecode_c = $value; break;
        case 'fecha_transaccion_c':
        $usertimezone = $current_user->getPreference('timezone');
        $dateTransaccion = new SugarDateTime($value, new DateTimeZone($usertimezone));
        $orden->fecha_transaccion_c = $dateTransaccion->asDb(true);
        break;
        default:break;
      }
    }
    return $orden;
  }

  // return array($ordenesAGuardar,$ordenesConError,$typeErrors)
  public function ordenes($result){
    $ordenesAGuardar = array();
    $ordenesConError = array();
    $typeErrors = array();

    if (isset($result['records'][0]))
    {
      if(count($result['records'][0]) == 14)// es el número correcto de campos esperados
      {
        $totaldeRegistros = count($result['records']);
        $i = 0;
        while ($i < $totaldeRegistros){
          $orden = BeanFactory::newBean('orden_Ordenes');
          if (count($result['records'][$i]) == 14) {
            $orden = $this->getOrden($orden,$result,$i);
            if (!$this->getOrdenExist($orden->no_ticket_c)) {
              
              //HSLR checa si la sucursal viene con un cero al inicio, si si se lo quita
              if (strlen($orden->id_sucursal_c) >= 5) {
                $orden->id_sucursal_c=$this->checkSucursal($orden->id_sucursal_c);
              }

              if($this->getAccountOrdenesExist($orden->id_pos_c)){
                array_push($ordenesAGuardar, $orden);
              } else {
                array_push($ordenesConError,$orden); // La cuenta no es valida para esa orden
                array_push($typeErrors, 1);
              }
            } else {
              $GLOBALS['log']->fatal('importaOrdenJSON Orden Existente: ' . $orden->folio_c . ", fecha de transacción: " . $orden->fecha_transaccion_c . " no_ticket_c:" . $orden->no_ticket_c);
            }
          } else {
            $orden = $this->getOrden($orden,$result,$i);
            array_push($ordenesConError,$orden); // La orden no tiene todas las columnas
            array_push($typeErrors, 4);
          }
          $i++;
        }
      }else {
        array_push($typeErrors, 5);
      }
    } else {
      array_push($typeErrors, 6);
    }
    return array('ordenesAGuardar'=>$ordenesAGuardar,'ordenesConError'=>$ordenesConError,'typeErrors'=>$typeErrors);
  }

//metodo para quitar el cero inicial del id de la sucursal
  private function checkSucursal($id_sucursal_c){
    return substr($id_sucursal_c, -4);
  }


  private function getOrdenExist($no_ticket_c, $flag = false){
    $bean = BeanFactory::newBean("orden_Ordenes");
    $sugarQuery = new SugarQuery();
    $sugarQuery->from($bean, array('team_security'=>false));
    $sugarQuery->where()
    ->equals('no_ticket_c', $no_ticket_c);
    $sugarQuery->limit(1);
    $result = $sugarQuery->execute();
    if(!$flag){
      return count($result) > 0;
    } else {
      if (!empty($result)){
        return $result;
      } else {
        return false;
      }
    }
  }

  public function guardarBeans($result,$filename=''){
    $totaldeRegistrados = 0;
    foreach ($result as $bean) {
      if(!empty($filename)){
        $bean->importadode_c = $filename;
      }
      $bean->save(false);
      $totaldeRegistrados++;
    }
    return $totaldeRegistrados;
  }

  public function logProcesados($module,$result)
  {
  	if($result['code'] == 200)
  	{
  		return true;
  	} else {
  		$GLOBALS['log']->fatal('Error al mover el archivo en importa'.$module.'JSON: '.print_r($result['message'],1));
  		return false;
  	}
  }

}
