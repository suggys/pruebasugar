<?php
require_once 'custom/include/CustomHttpRequest.php';
/**
 *
 */
class ImportadorPartidas
{
  // $configurator = new Configurator();
  // $configurator->loadConfig();
  // $hotsName = $configurator->config['host_name'];
  //
  // $url = getURLApiFactura()."/getDataModule/pedidos_crm";
  // if($hotsName != 'lowes.sugarondemand.com'){
  //   $url .=  '?tests=1';
  // }
  public function getPartidas(CustomHttpRequest $req)
  {
    $req->setOption(CURLOPT_RETURNTRANSFER, true);
    $result = $req->execute();
    $req->close();
    return json_decode($result, true);
  }
}
