<?php
require_once('custom/modules/Accounts/sugarpdf/sugarpdf.pdfmanager.php');

class PdfFromPortalController{
  private $module = "Accounts";
  public function __construct()
  {
    global $current_user;
    $current_user = BeanFactory::getBean("Users", "1");
  }

  public function getEdoCuenta()
  {
    if(!empty($_GET['pdf'])){
      $this->pdf();
      return;
    }
  }
  public function pdf()
  {
    global $current_language, $current_user;
    $current_user = BeanFactory::getBean("Users", "1");
    $admin = new Administration();
    $admin->retrieveSettings();
    $templateId = '';
    if(array_key_exists('lowes_settings_pdf_manager_estado_cuenta_portal', $admin->settings)){
      $templateId = $admin->settings['lowes_settings_pdf_manager_estado_cuenta_portal'];   
    }
    //'cba2b224-7557-11e7-bbb3-080027b5ccaa';//
    // recuperando contacto
    $contacto = BeanFactory::retrieveBean('Contacts', $_GET['idContacto']);
    if($contacto->account_id && !empty($templateId)){
      $bean = BeanFactory::getBean($this->module, $contacto->account_id);  
      $pdfManager = new AccountsSugarpdfPdfmanager($bean);
      $_REQUEST["sugarpdf"] = "pdfmanager";
      $_REQUEST['pdf_template_id'] = $templateId;
      $_REQUEST['fromPortal'] = 1;
      $pdfManager->process();
      $pdfManager->Output($pdfManager->fileName,'I');
    }
  }

}


 ?>
