<?php
require_once('include/Sugarpdf/sugarpdf/sugarpdf.pdfmanager.php');

class SolicitudCreditoController{
  private $module = "LOW01_SolicitudesCredito";

  private $fileFields = [
    "doc_formato_solicitud_d", "doc_aval_d", "doc_acta_constitutiva_d", "doc_copia_estado_finan_rec_d", "copia_estado_financiero_anterd", "doc_copia_id_aval_d", "doc_copia_id_solicitante_d", "doc_copia_estado_cuenta_d", "doc_comprobante_domicilio_d", "doc_copia_acta_representante_d", "doc_copia_pago_impuestos_d", "doc_copia_rfc_d", "doc_formato_buro_d", "reporte_analisis_agente_ext_c",
  ];

  private $hideFields = [
    "doc_formato_solicitud", "doc_aval", "doc_acta_constitutiva", "doc_copia_estado_finan_rec", "copia_estado_financiero_anter", "doc_copia_id_aval", "doc_copia_id_solicitante", "doc_copia_estado_cuenta", "doc_comprobante_domicilio", "doc_copia_acta_representante", "doc_copia_pago_impuestos", "doc_copia_rfc", "doc_formato_buro"
  ];

  private $estadosPermitidos = [
    'Solicitud_Nueva',
    'Aprobado_Envio_a_Agencia_Externa_por_Administrador_de_Credito',
    'Pendiente_por_Agente_Externo',
  ];

  private $hide_panels = [
    "panel_header",
    "panel_body",
    "panel_hidden",
    "LBL_RECORDVIEW_PANEL15",
  ];

  private $fileFieldsData = [];

  public function __construct()
  {
    global $current_user;
    $current_user = BeanFactory::getBean("Users", "1");
    $metadata = new MetaDataManager();
    $moduleData = $metadata->getModuleData($this->module);
    $this->fields = $moduleData["fields"];
  }

  public function postReq($postData)
  {
    global $current_language, $current_user;
    $current_user = BeanFactory::getBean("Users", "1");
    $bean = BeanFactory::getBean($this->module, $_GET['id']);
    $field_name = "";

    $beanData = $this->getBeanData($postData);

    $sugarApi = new FileApiWrapper;
    $api = new RestService();
    $api->user = $current_user;

    $documents = $this->saveDocuments($postData, $beanData);
    $sugarApi->updateBeanWrapper($bean,$api, $beanData);
  }

  public function getReq()
  {
    if(!empty($_GET['pdf'])){
      $this->pdf();
      return;
    }

    if(!empty($_GET['download_field_file'])){
      $this->downloadFieldFile();
      return;
    }
    global $current_language, $current_user;
    $current_user = BeanFactory::getBean("Users", "1");
    $beanFormated = [];
    $metadata = new MetaDataManager();
    $moduleData = $metadata->getModuleData($this->module);

    $view = $metadata->getModuleView($this->module, "record");
    // $GLOBALS["log"]->fatal("view:". print_r($view, true));

    $fieldList = $metadata->getModuleViewFields($this->module, "record");
    $fieldList[] = 'id';
    if(isset($_GET['id']) && !empty($_GET['id'])){
      $bean = BeanFactory::getBean($this->module, $_GET['id']);
      if($bean->id && in_array($bean->estado_solicitud_c, $this->estadosPermitidos)){

        $panels = $this->filerPanelsToDisplay($bean, $view['meta']['panels']);
        $panels = $this->formatFields($bean, $panels);

        $api = new RestService();
        $api->user = $current_user;
        $beanFormated = $data = ApiHelper::getHelper($api,$bean)->formatForApi($bean,$fieldList, []);
        $module_strings = return_module_language($current_language, $this->module);
        $fields = $this->filterFieldsToDisplay($moduleData['fields'], $bean->estado_solicitud_c);

        $app_list_strings_aux = $this->getModuleAppListStrings($moduleData['fields']);
        $smarty = new Sugar_Smarty;
        $smarty->assign('panels', json_encode($panels));
        $smarty->assign('fields', json_encode($fields));
        $smarty->assign('language', json_encode($module_strings));
        $smarty->assign('bean', json_encode($beanFormated));
        $smarty->assign('app_list_strings', json_encode($app_list_strings_aux));

        $smarty->display('custom/include/SolicitudCredito/form.tpl');
      }
    }
  }

  public function getModuleAppListStrings($fields)
  {
    global $app_list_strings;
    $app_list_strings_aux = [];
    foreach ($fields as $field) {
      if(!empty($field['type']) && $field['type'] === 'enum'){
        $app_list_strings_aux[$field['options']] = $app_list_strings[$field['options']];
      }
    }
    return $app_list_strings_aux;
  }

  public function process()
  {
    $uploadFile = new UploadFile;
    $uploadFile->temp_file_location = 'php://input';
    $postData = $uploadFile->get_file_contents();
    $postData = json_decode($postData, true);
    if(!empty($postData)){
      $this->postReq($postData);
    }
    else{
      $this->getReq();
    }

  }

  public function saveDocument($field_name, $data)
  {
    global $current_user;
    $current_user = BeanFactory::getBean("Users", "1");
    list($type, $nameData, $fileBase64) = explode(';', $data);
    list(, $name) = explode('=', $nameData);
    list(, $fileBase64) = explode(',', $fileBase64);

    $document = BeanFactory::newBean("Documents");
    $document->name = $name;
    $document->filename = $name;
    $document->category_id = "otro";
    $document->assigned_user_id = "1";
    $document->revision = 1;
    $document->save();

    $revision = new DocumentRevision;
    $revision->document_id = $document->id;
    $revision->filename = $document->filename;
    $revision->revision = 1;
    $revision->doc_type = 'Sugar';
    $revision->save();

    $uploadFile = new UploadStream;
    $fileApi = new FileApiWrapper;
    $destination = $GLOBALS['sugar_config']['upload_dir'] . $revision->id;

    $fp = $fileApi->getFileHandleWrapper($destination, 'wb');
    $uploadFile->fp = $fp;
    $data = base64_decode($fileBase64);
    $uploadFile->stream_write($data);
    $fileApi->closeFileHandleWrapper($fp);

    return $document;
  }

  public function getBeanData($formData)
  {
    $beanData = [];
    foreach ($formData as $panel) {
      foreach ($panel as $field_name => $value) {
        if(!in_array($field_name, $this->fileFields)){
          $beanData[$field_name] = $value;
        }
        else{
          $this->fileFieldsData[$field_name] = $value;
        }
      }
    }
    $GLOBALS["log"]->fatal("getBeanData:". print_r($beanData, true));
    return $beanData;
  }

  public function saveDocuments($postData, &$beanData)
  {
    $documents = [];
    foreach ($this->fileFields as $field_name) {
      if(!empty($this->fileFieldsData[$field_name])){
        $documents[$field_name] = $this->saveDocument($field_name, $this->fileFieldsData[$field_name]);
        $beanData[$this->fields[$field_name]['id_name']] = $documents[$field_name]->id;
        $beanData[$field_name] = $documents[$field_name]->name;
      }
    }
    return $documents;
  }

  public function filterFieldsToDisplay($fields, $estadoSolicitud){
    $filteredFields = [];
    foreach ($fields as $key => $def) {
      if(in_array($key, $this->hideFields)) continue;
      $filteredFields[$key] = $def;
    }
    return $filteredFields;
  }

  public function formatFields($bean, $panels)
  {
    $resultPanels = [];
    if($bean->estado_solicitud_c == 'Solicitud_Nueva'){
      $resultPanels = $panels;
    }

    if($bean->estado_solicitud_c == 'Pendiente_por_Agente_Externo'){
      foreach ($panels as $panel) {
        $panelAux = $panel;
        if($panel['name'] != "LBL_RECORDVIEW_PANEL14"){
          $panelAux['fields'] = [];
          foreach ($panel['fields'] as $field) {
            $field['readonly_portal'] = true;

            if($panel['name'] === "LBL_RECORDVIEW_PANEL6"){
              $field['type'] = "download_file";
            }

            $panelAux['fields'][] = $field;
          }
        }
        $resultPanels[] = $panelAux;
      }
    }
    return $resultPanels;
  }

  public function filerPanelsToDisplay($bean, $panels){
    $panelsResult = [];
    $black_list = $this->hide_panels;

    if($bean->estado_solicitud_c === 'Solicitud_Nueva'){
      $black_list[] = "LBL_RECORDVIEW_PANEL14";
    }
    for ($item=0; $item < count($panels); $item++) {
      if(!in_array($panels[$item]['name'], $black_list)){
        $panelsResult[] = $panels[$item];
      }
    }
    return $panelsResult;
  }

  public function pdf()
  {
    global $current_language, $current_user;
    $current_user = BeanFactory::getBean("Users", "1");
    $admin = new Administration();
    $admin->retrieveSettings();
    $templateId = $admin->settings['lowes_settings_pdf_manager_solicitud_credito'];

    $bean = BeanFactory::getBean($this->module, $_GET['id']);
    $pdfManager = new SugarpdfPdfmanager($bean);
    $_REQUEST["sugarpdf"] = "pdfmanager";
    $_REQUEST['pdf_template_id'] = $templateId;
    $pdfManager->process();
    $pdfManager->Output($pdfManager->fileName,'I');
  }

  public function downloadFieldFile()
  {

    if(isset($_GET['id']) && !empty($_GET['id'])){
      $bean = BeanFactory::getBean($this->module, $_GET['id']);
      if($bean->id){
        $beanArray = (array)$bean;
        $fileField = $this->fields[$_GET['download_field_file']]['id_name'];
        $documentId = $beanArray[$fileField];
        if(!empty($beanArray[$fileField])){
          $document = BeanFactory::getBean("Documents", $documentId);
          $downloadFile = new DownloadFile;
          $downloadFile->getFile($document, "filename", true);
        }
      }
    }
  }
}

class FileApiWrapper extends FileApi{

  public function getFileHandleWrapper($path, $mode = 'r'){
    return $this->getFileHandle($path, $mode);
  }

  public function closeFileHandleWrapper($handle)
  {
    return $this->closeFileHandle($handle);
  }

  public function updateBeanWrapper(SugarBean $bean, ServiceBase $api, $args)
  {
    $this->updateBean($bean, $api, $args);
    return $bean->id;
  }
}
 ?>
