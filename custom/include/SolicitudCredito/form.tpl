<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="icon" type="image/vnd.microsoft.icon" href="https://www.lowes.com.mx/img/favicon.ico?1502119280">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <title>Lowes - Solicitud de Crédito</title>
  {literal}
  <style media="screen">
    header{
      border-bottom: 1em solid #004990;
      margin-bottom: 1em;
    }
    header img{
      width:100%;
    }
    header .title span{
      font-size: 3em;
      font-weight: bold;
      line-height: 3.5em;
    }
    body{
      padding:1em;
    }
    label{
      font-size: 11px;
      color: #747474;
      margin-bottom: 0;
    }
    legend{
      border: none;
      background-color: #ccc;
      padding: 0.5em;
    }
    .form-control{
        height: 28px;
    }
  </style>
  {/literal}
  <script type="text/javascript">
    var panels = {$panels};
    var fields = {$fields};
    var language = {$language};
    var bean = {$bean};
    var app_list_strings = {$app_list_strings};
    // console.warn("bean",bean);
  </script>
</head>
<body>
  <header class="container">
    <div class="col-md-3">
      <img src="/custom/themes/default/images/company_logo.png" alt="" />
    </div>
    <div class="col-md-7 title">
      <span>Solicitud de Crédito</span>
    </div>
  </header>
  <div id="app" class="container">
  </div>

  <script src="/cache/include/javascript/solicitud_credito.js"></script>
  <iframe name="pdfmanager" src=" " class="hide"></iframe>
</body>
</html>
