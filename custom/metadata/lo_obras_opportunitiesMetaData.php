<?php
// created: 2017-07-20 15:21:47
$dictionary["lo_obras_opportunities"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'lo_obras_opportunities' => 
    array (
      'lhs_module' => 'LO_Obras',
      'lhs_table' => 'lo_obras',
      'lhs_key' => 'id',
      'rhs_module' => 'Opportunities',
      'rhs_table' => 'opportunities',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'lo_obras_opportunities_c',
      'join_key_lhs' => 'lo_obras_opportunitieslo_obras_ida',
      'join_key_rhs' => 'lo_obras_opportunitiesopportunities_idb',
    ),
  ),
  'table' => 'lo_obras_opportunities_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'lo_obras_opportunitieslo_obras_ida' => 
    array (
      'name' => 'lo_obras_opportunitieslo_obras_ida',
      'type' => 'id',
    ),
    'lo_obras_opportunitiesopportunities_idb' => 
    array (
      'name' => 'lo_obras_opportunitiesopportunities_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'lo_obras_opportunitiesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'lo_obras_opportunities_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lo_obras_opportunitieslo_obras_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'lo_obras_opportunities_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'lo_obras_opportunitiesopportunities_idb',
      ),
    ),
  ),
);