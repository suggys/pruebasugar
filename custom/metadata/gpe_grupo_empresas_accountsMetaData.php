<?php
// created: 2018-04-14 04:17:56
$dictionary['gpe_grupo_empresas_accounts'] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'gpe_grupo_empresas_accounts' => 
    array (
      'lhs_module' => 'GPE_Grupo_empresas',
      'lhs_table' => 'gpe_grupo_empresas',
      'lhs_key' => 'id',
      'rhs_module' => 'Accounts',
      'rhs_table' => 'accounts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'gpe_grupo_empresas_accounts_c',
      'join_key_lhs' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
      'join_key_rhs' => 'gpe_grupo_empresas_accountsaccounts_idb',
    ),
  ),
  'table' => 'gpe_grupo_empresas_accounts_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida' => 
    array (
      'name' => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    'gpe_grupo_empresas_accountsaccounts_idb' => 
    array (
      'name' => 'gpe_grupo_empresas_accountsaccounts_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'gpe_grupo_empresas_accountsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'gpe_grupo_empresas_accounts_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'gpe_grupo_empresas_accountsgpe_grupo_empresas_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'gpe_grupo_empresas_accounts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'gpe_grupo_empresas_accountsaccounts_idb',
      ),
    ),
  ),
);