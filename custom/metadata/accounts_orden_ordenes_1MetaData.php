<?php
// created: 2016-10-11 21:41:46
$dictionary["accounts_orden_ordenes_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_orden_ordenes_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'orden_Ordenes',
      'rhs_table' => 'orden_ordenes',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_orden_ordenes_1_c',
      'join_key_lhs' => 'accounts_orden_ordenes_1accounts_ida',
      'join_key_rhs' => 'accounts_orden_ordenes_1orden_ordenes_idb',
    ),
  ),
  'table' => 'accounts_orden_ordenes_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'accounts_orden_ordenes_1accounts_ida' => 
    array (
      'name' => 'accounts_orden_ordenes_1accounts_ida',
      'type' => 'id',
    ),
    'accounts_orden_ordenes_1orden_ordenes_idb' => 
    array (
      'name' => 'accounts_orden_ordenes_1orden_ordenes_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'accounts_orden_ordenes_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'accounts_orden_ordenes_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_orden_ordenes_1accounts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'accounts_orden_ordenes_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_orden_ordenes_1orden_ordenes_idb',
      ),
    ),
  ),
);