<?php
// created: 2018-02-01 19:07:34
$dictionary["dise_prospectos_cocina_quotes"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'dise_prospectos_cocina_quotes' => 
    array (
      'lhs_module' => 'dise_Prospectos_cocina',
      'lhs_table' => 'dise_prospectos_cocina',
      'lhs_key' => 'id',
      'rhs_module' => 'Quotes',
      'rhs_table' => 'quotes',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'dise_prospectos_cocina_quotes_c',
      'join_key_lhs' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
      'join_key_rhs' => 'dise_prospectos_cocina_quotesquotes_idb',
    ),
  ),
  'table' => 'dise_prospectos_cocina_quotes_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida' => 
    array (
      'name' => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
      'type' => 'id',
    ),
    'dise_prospectos_cocina_quotesquotes_idb' => 
    array (
      'name' => 'dise_prospectos_cocina_quotesquotes_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_dise_prospectos_cocina_quotes_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_dise_prospectos_cocina_quotes_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'dise_prospectos_cocina_quotesdise_prospectos_cocina_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_dise_prospectos_cocina_quotes_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'dise_prospectos_cocina_quotesquotes_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'dise_prospectos_cocina_quotes_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'dise_prospectos_cocina_quotesquotes_idb',
      ),
    ),
  ),
);