<?php
// created: 2016-10-13 22:21:31
$dictionary["lowae_autorizacion_especial_accounts_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'lowae_autorizacion_especial_accounts_1' => 
    array (
      'lhs_module' => 'lowae_autorizacion_especial',
      'lhs_table' => 'lowae_autorizacion_especial',
      'lhs_key' => 'id',
      'rhs_module' => 'Accounts',
      'rhs_table' => 'accounts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'lowae_autorizacion_especial_accounts_1_c',
      'join_key_lhs' => 'lowae_auto6c11special_ida',
      'join_key_rhs' => 'lowae_autorizacion_especial_accounts_1accounts_idb',
    ),
  ),
  'table' => 'lowae_autorizacion_especial_accounts_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'lowae_auto6c11special_ida' => 
    array (
      'name' => 'lowae_auto6c11special_ida',
      'type' => 'id',
    ),
    'lowae_autorizacion_especial_accounts_1accounts_idb' => 
    array (
      'name' => 'lowae_autorizacion_especial_accounts_1accounts_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'lowae_autorizacion_especial_accounts_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'lowae_autorizacion_especial_accounts_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lowae_auto6c11special_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'lowae_autorizacion_especial_accounts_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'lowae_autorizacion_especial_accounts_1accounts_idb',
      ),
    ),
  ),
);