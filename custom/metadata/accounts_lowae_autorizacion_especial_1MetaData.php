<?php
// created: 2016-10-13 23:48:33
$dictionary["accounts_lowae_autorizacion_especial_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'accounts_lowae_autorizacion_especial_1' => 
    array (
      'lhs_module' => 'Accounts',
      'lhs_table' => 'accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'lowae_autorizacion_especial',
      'rhs_table' => 'lowae_autorizacion_especial',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'accounts_lowae_autorizacion_especial_1_c',
      'join_key_lhs' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
      'join_key_rhs' => 'accounts_le69cspecial_idb',
    ),
  ),
  'table' => 'accounts_lowae_autorizacion_especial_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'accounts_lowae_autorizacion_especial_1accounts_ida' => 
    array (
      'name' => 'accounts_lowae_autorizacion_especial_1accounts_ida',
      'type' => 'id',
    ),
    'accounts_le69cspecial_idb' => 
    array (
      'name' => 'accounts_le69cspecial_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'accounts_lowae_autorizacion_especial_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'accounts_lowae_autorizacion_especial_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'accounts_lowae_autorizacion_especial_1accounts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'accounts_lowae_autorizacion_especial_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'accounts_le69cspecial_idb',
      ),
    ),
  ),
);