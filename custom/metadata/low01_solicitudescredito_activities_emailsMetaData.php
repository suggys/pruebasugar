<?php
// created: 2017-07-21 12:47:07
$dictionary["low01_solicitudescredito_activities_emails"] = array (
  'relationships' => 
  array (
    'low01_solicitudescredito_activities_emails' => 
    array (
      'lhs_module' => 'LOW01_SolicitudesCredito',
      'lhs_table' => 'low01_solicitudescredito',
      'lhs_key' => 'id',
      'rhs_module' => 'Emails',
      'rhs_table' => 'emails',
      'relationship_role_column_value' => 'LOW01_SolicitudesCredito',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'emails_beans',
      'join_key_rhs' => 'email_id',
      'join_key_lhs' => 'bean_id',
      'relationship_role_column' => 'bean_module',
    ),
  ),
  'fields' => '',
  'indices' => '',
  'table' => '',
);