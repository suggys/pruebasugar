<?php
// created: 2016-10-19 00:31:50
$dictionary["lf_facturas_lf_facturas"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'lf_facturas_lf_facturas' => 
    array (
      'lhs_module' => 'LF_Facturas',
      'lhs_table' => 'lf_facturas',
      'lhs_key' => 'id',
      'rhs_module' => 'LF_Facturas',
      'rhs_table' => 'lf_facturas',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'lf_facturas_lf_facturas_c',
      'join_key_lhs' => 'lf_facturas_lf_facturaslf_facturas_ida',
      'join_key_rhs' => 'lf_facturas_lf_facturaslf_facturas_idb',
    ),
  ),
  'table' => 'lf_facturas_lf_facturas_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'lf_facturas_lf_facturaslf_facturas_ida' => 
    array (
      'name' => 'lf_facturas_lf_facturaslf_facturas_ida',
      'type' => 'id',
    ),
    'lf_facturas_lf_facturaslf_facturas_idb' => 
    array (
      'name' => 'lf_facturas_lf_facturaslf_facturas_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'lf_facturas_lf_facturasspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'lf_facturas_lf_facturas_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lf_facturas_lf_facturaslf_facturas_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'lf_facturas_lf_facturas_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'lf_facturas_lf_facturaslf_facturas_idb',
      ),
    ),
  ),
);