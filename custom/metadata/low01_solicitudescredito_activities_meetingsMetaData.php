<?php
// created: 2017-07-21 12:47:07
$dictionary["low01_solicitudescredito_activities_meetings"] = array (
  'relationships' => 
  array (
    'low01_solicitudescredito_activities_meetings' => 
    array (
      'lhs_module' => 'LOW01_SolicitudesCredito',
      'lhs_table' => 'low01_solicitudescredito',
      'lhs_key' => 'id',
      'rhs_module' => 'Meetings',
      'rhs_table' => 'meetings',
      'relationship_role_column_value' => 'LOW01_SolicitudesCredito',
      'rhs_key' => 'parent_id',
      'relationship_type' => 'one-to-many',
      'relationship_role_column' => 'parent_type',
    ),
  ),
  'fields' => '',
  'indices' => '',
  'table' => '',
);