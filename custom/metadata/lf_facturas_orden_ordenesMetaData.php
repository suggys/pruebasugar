<?php
// created: 2016-10-19 00:31:50
$dictionary["lf_facturas_orden_ordenes"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'lf_facturas_orden_ordenes' => 
    array (
      'lhs_module' => 'orden_Ordenes',
      'lhs_table' => 'orden_ordenes',
      'lhs_key' => 'id',
      'rhs_module' => 'LF_Facturas',
      'rhs_table' => 'lf_facturas',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'lf_facturas_orden_ordenes_c',
      'join_key_lhs' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
      'join_key_rhs' => 'lf_facturas_orden_ordeneslf_facturas_idb',
    ),
  ),
  'table' => 'lf_facturas_orden_ordenes_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'lf_facturas_orden_ordenesorden_ordenes_ida' => 
    array (
      'name' => 'lf_facturas_orden_ordenesorden_ordenes_ida',
      'type' => 'id',
    ),
    'lf_facturas_orden_ordeneslf_facturas_idb' => 
    array (
      'name' => 'lf_facturas_orden_ordeneslf_facturas_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'lf_facturas_orden_ordenesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'lf_facturas_orden_ordenes_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lf_facturas_orden_ordenesorden_ordenes_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'lf_facturas_orden_ordenes_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'lf_facturas_orden_ordeneslf_facturas_idb',
      ),
    ),
  ),
);