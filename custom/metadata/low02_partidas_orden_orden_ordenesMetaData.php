<?php
// created: 2017-07-16 19:22:10
$dictionary["low02_partidas_orden_orden_ordenes"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'low02_partidas_orden_orden_ordenes' => 
    array (
      'lhs_module' => 'orden_Ordenes',
      'lhs_table' => 'orden_ordenes',
      'lhs_key' => 'id',
      'rhs_module' => 'LOW02_Partidas_Orden',
      'rhs_table' => 'low02_partidas_orden',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'low02_partidas_orden_orden_ordenes_c',
      'join_key_lhs' => 'low02_partidas_orden_orden_ordenesorden_ordenes_ida',
      'join_key_rhs' => 'low02_partidas_orden_orden_ordeneslow02_partidas_orden_idb',
    ),
  ),
  'table' => 'low02_partidas_orden_orden_ordenes_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'low02_partidas_orden_orden_ordenesorden_ordenes_ida' => 
    array (
      'name' => 'low02_partidas_orden_orden_ordenesorden_ordenes_ida',
      'type' => 'id',
    ),
    'low02_partidas_orden_orden_ordeneslow02_partidas_orden_idb' => 
    array (
      'name' => 'low02_partidas_orden_orden_ordeneslow02_partidas_orden_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'low02_partidas_orden_orden_ordenesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'low02_partidas_orden_orden_ordenes_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'low02_partidas_orden_orden_ordenesorden_ordenes_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'low02_partidas_orden_orden_ordenes_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'low02_partidas_orden_orden_ordeneslow02_partidas_orden_idb',
      ),
    ),
  ),
);