<?php
// created: 2017-04-03 22:41:21
$dictionary["low01_pagos_facturas_lf_facturas"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'low01_pagos_facturas_lf_facturas' => 
    array (
      'lhs_module' => 'LF_Facturas',
      'lhs_table' => 'lf_facturas',
      'lhs_key' => 'id',
      'rhs_module' => 'Low01_Pagos_Facturas',
      'rhs_table' => 'low01_pagos_facturas',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'low01_pagos_facturas_lf_facturas_c',
      'join_key_lhs' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
      'join_key_rhs' => 'low01_pagos_facturas_lf_facturaslow01_pagos_facturas_idb',
    ),
  ),
  'table' => 'low01_pagos_facturas_lf_facturas_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'low01_pagos_facturas_lf_facturaslf_facturas_ida' => 
    array (
      'name' => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
      'type' => 'id',
    ),
    'low01_pagos_facturas_lf_facturaslow01_pagos_facturas_idb' => 
    array (
      'name' => 'low01_pagos_facturas_lf_facturaslow01_pagos_facturas_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_low01_pagos_facturas_lf_facturas_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_low01_pagos_facturas_lf_facturas_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'low01_pagos_facturas_lf_facturaslf_facturas_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_low01_pagos_facturas_lf_facturas_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'low01_pagos_facturas_lf_facturaslow01_pagos_facturas_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'low01_pagos_facturas_lf_facturas_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'low01_pagos_facturas_lf_facturaslow01_pagos_facturas_idb',
      ),
    ),
  ),
);