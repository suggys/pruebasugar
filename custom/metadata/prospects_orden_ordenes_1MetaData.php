<?php
// created: 2017-07-20 22:21:03
$dictionary["prospects_orden_ordenes_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'prospects_orden_ordenes_1' => 
    array (
      'lhs_module' => 'Prospects',
      'lhs_table' => 'prospects',
      'lhs_key' => 'id',
      'rhs_module' => 'orden_Ordenes',
      'rhs_table' => 'orden_ordenes',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'prospects_orden_ordenes_1_c',
      'join_key_lhs' => 'prospects_orden_ordenes_1prospects_ida',
      'join_key_rhs' => 'prospects_orden_ordenes_1orden_ordenes_idb',
    ),
  ),
  'table' => 'prospects_orden_ordenes_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'prospects_orden_ordenes_1prospects_ida' => 
    array (
      'name' => 'prospects_orden_ordenes_1prospects_ida',
      'type' => 'id',
    ),
    'prospects_orden_ordenes_1orden_ordenes_idb' => 
    array (
      'name' => 'prospects_orden_ordenes_1orden_ordenes_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'prospects_orden_ordenes_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'prospects_orden_ordenes_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'prospects_orden_ordenes_1prospects_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'prospects_orden_ordenes_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'prospects_orden_ordenes_1orden_ordenes_idb',
      ),
    ),
  ),
);