<?php
// created: 2017-10-10 16:08:11
$dictionary["low01_solicitudescredito_contacts_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'low01_solicitudescredito_contacts_1' => 
    array (
      'lhs_module' => 'LOW01_SolicitudesCredito',
      'lhs_table' => 'low01_solicitudescredito',
      'lhs_key' => 'id',
      'rhs_module' => 'Contacts',
      'rhs_table' => 'contacts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'low01_solicitudescredito_contacts_1_c',
      'join_key_lhs' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
      'join_key_rhs' => 'low01_solicitudescredito_contacts_1contacts_idb',
    ),
  ),
  'table' => 'low01_solicitudescredito_contacts_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida' => 
    array (
      'name' => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
      'type' => 'id',
    ),
    'low01_solicitudescredito_contacts_1contacts_idb' => 
    array (
      'name' => 'low01_solicitudescredito_contacts_1contacts_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'low01_solicitudescredito_contacts_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'low01_solicitudescredito_contacts_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'low01_solicitudescredito_contacts_1low01_solicitudescredito_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'low01_solicitudescredito_contacts_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'low01_solicitudescredito_contacts_1contacts_idb',
      ),
    ),
  ),
);