<?php
// created: 2017-07-21 12:47:07
$dictionary["low01_solicitudescredito_prospects"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'low01_solicitudescredito_prospects' => 
    array (
      'lhs_module' => 'Prospects',
      'lhs_table' => 'prospects',
      'lhs_key' => 'id',
      'rhs_module' => 'LOW01_SolicitudesCredito',
      'rhs_table' => 'low01_solicitudescredito',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'low01_solicitudescredito_prospects_c',
      'join_key_lhs' => 'low01_solicitudescredito_prospectsprospects_ida',
      'join_key_rhs' => 'low01_solicitudescredito_prospectslow01_solicitudescredito_idb',
    ),
  ),
  'table' => 'low01_solicitudescredito_prospects_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'low01_solicitudescredito_prospectsprospects_ida' => 
    array (
      'name' => 'low01_solicitudescredito_prospectsprospects_ida',
      'type' => 'id',
    ),
    'low01_solicitudescredito_prospectslow01_solicitudescredito_idb' => 
    array (
      'name' => 'low01_solicitudescredito_prospectslow01_solicitudescredito_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'low01_solicitudescredito_prospectsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'low01_solicitudescredito_prospects_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'low01_solicitudescredito_prospectsprospects_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'low01_solicitudescredito_prospects_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'low01_solicitudescredito_prospectslow01_solicitudescredito_idb',
      ),
    ),
  ),
);